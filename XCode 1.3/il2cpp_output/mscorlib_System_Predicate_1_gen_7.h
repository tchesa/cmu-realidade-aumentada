﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnionAssets.FLE.DataEventHandlerFunction
struct DataEventHandlerFunction_t459;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Predicate`1<UnionAssets.FLE.DataEventHandlerFunction>
struct  Predicate_1_t2678  : public MulticastDelegate_t10
{
};
