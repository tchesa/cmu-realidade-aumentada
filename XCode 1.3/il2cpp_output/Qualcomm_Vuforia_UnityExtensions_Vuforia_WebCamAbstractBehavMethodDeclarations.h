﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t453;
// System.String
struct String_t;
// Vuforia.WebCamImpl
struct WebCamImpl_t939;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
extern "C" bool WebCamAbstractBehaviour_get_PlayModeRenderVideo_m6392 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_PlayModeRenderVideo_m6393 (WebCamAbstractBehaviour_t453 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
extern "C" String_t* WebCamAbstractBehaviour_get_DeviceName_m6394 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
extern "C" void WebCamAbstractBehaviour_set_DeviceName_m6395 (WebCamAbstractBehaviour_t453 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
extern "C" bool WebCamAbstractBehaviour_get_FlipHorizontally_m6396 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_FlipHorizontally_m6397 (WebCamAbstractBehaviour_t453 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
extern "C" bool WebCamAbstractBehaviour_get_TurnOffWebCam_m6398 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_TurnOffWebCam_m6399 (WebCamAbstractBehaviour_t453 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
extern "C" bool WebCamAbstractBehaviour_get_IsPlaying_m6400 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
extern "C" bool WebCamAbstractBehaviour_IsWebCamUsed_m6401 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
extern "C" WebCamImpl_t939 * WebCamAbstractBehaviour_get_ImplementationClass_m6402 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
extern "C" void WebCamAbstractBehaviour_InitCamera_m6403 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
extern "C" bool WebCamAbstractBehaviour_CheckNativePluginSupport_m6404 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
extern "C" void WebCamAbstractBehaviour_OnLevelWasLoaded_m6405 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
extern "C" void WebCamAbstractBehaviour_OnDestroy_m6406 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
extern "C" void WebCamAbstractBehaviour_Update_m6407 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
extern "C" int32_t WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m6408 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern "C" void WebCamAbstractBehaviour__ctor_m2773 (WebCamAbstractBehaviour_t453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
