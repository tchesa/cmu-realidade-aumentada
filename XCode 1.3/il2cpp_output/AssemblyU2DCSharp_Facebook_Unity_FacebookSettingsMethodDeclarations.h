﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FacebookSettings
struct FacebookSettings_t246;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>
struct List_1_t247;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.FacebookSettings::.ctor()
extern "C" void FacebookSettings__ctor_m1371 (FacebookSettings_t246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Facebook.Unity.FacebookSettings::get_SelectedAppIndex()
extern "C" int32_t FacebookSettings_get_SelectedAppIndex_m1372 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_SelectedAppIndex(System.Int32)
extern "C" void FacebookSettings_set_SelectedAppIndex_m1373 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Facebook.Unity.FacebookSettings::get_AppIds()
extern "C" List_1_t78 * FacebookSettings_get_AppIds_m1374 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_AppIds(System.Collections.Generic.List`1<System.String>)
extern "C" void FacebookSettings_set_AppIds_m1375 (Object_t * __this /* static, unused */, List_1_t78 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Facebook.Unity.FacebookSettings::get_AppLabels()
extern "C" List_1_t78 * FacebookSettings_get_AppLabels_m1376 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_AppLabels(System.Collections.Generic.List`1<System.String>)
extern "C" void FacebookSettings_set_AppLabels_m1377 (Object_t * __this /* static, unused */, List_1_t78 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.FacebookSettings::get_AppId()
extern "C" String_t* FacebookSettings_get_AppId_m1378 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.FacebookSettings::get_IsValidAppId()
extern "C" bool FacebookSettings_get_IsValidAppId_m1379 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.FacebookSettings::get_Cookie()
extern "C" bool FacebookSettings_get_Cookie_m1380 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_Cookie(System.Boolean)
extern "C" void FacebookSettings_set_Cookie_m1381 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.FacebookSettings::get_Logging()
extern "C" bool FacebookSettings_get_Logging_m1382 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_Logging(System.Boolean)
extern "C" void FacebookSettings_set_Logging_m1383 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.FacebookSettings::get_Status()
extern "C" bool FacebookSettings_get_Status_m1384 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_Status(System.Boolean)
extern "C" void FacebookSettings_set_Status_m1385 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.FacebookSettings::get_Xfbml()
extern "C" bool FacebookSettings_get_Xfbml_m1386 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_Xfbml(System.Boolean)
extern "C" void FacebookSettings_set_Xfbml_m1387 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.FacebookSettings::get_IosURLSuffix()
extern "C" String_t* FacebookSettings_get_IosURLSuffix_m1388 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_IosURLSuffix(System.String)
extern "C" void FacebookSettings_set_IosURLSuffix_m1389 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.FacebookSettings::get_ChannelUrl()
extern "C" String_t* FacebookSettings_get_ChannelUrl_m1390 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.FacebookSettings::get_FrictionlessRequests()
extern "C" bool FacebookSettings_get_FrictionlessRequests_m1391 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_FrictionlessRequests(System.Boolean)
extern "C" void FacebookSettings_set_FrictionlessRequests_m1392 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes> Facebook.Unity.FacebookSettings::get_AppLinkSchemes()
extern "C" List_1_t247 * FacebookSettings_get_AppLinkSchemes_m1393 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::set_AppLinkSchemes(System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>)
extern "C" void FacebookSettings_set_AppLinkSchemes_m1394 (Object_t * __this /* static, unused */, List_1_t247 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.FacebookSettings Facebook.Unity.FacebookSettings::get_Instance()
extern "C" FacebookSettings_t246 * FacebookSettings_get_Instance_m1395 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::SettingsChanged()
extern "C" void FacebookSettings_SettingsChanged_m1396 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings::DirtyEditor()
extern "C" void FacebookSettings_DirtyEditor_m1397 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
