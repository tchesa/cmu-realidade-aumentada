﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_IOSTransactionErrorCode.h"

// IOSStoreKitError
struct  IOSStoreKitError_t136  : public Object_t
{
	// System.String IOSStoreKitError::description
	String_t* ___description_0;
	// IOSTransactionErrorCode IOSStoreKitError::code
	int32_t ___code_1;
};
