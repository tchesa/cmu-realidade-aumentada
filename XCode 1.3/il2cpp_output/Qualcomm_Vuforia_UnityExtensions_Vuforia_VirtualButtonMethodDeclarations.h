﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VirtualButton
struct VirtualButton_t1061;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VirtualButton::.ctor()
extern "C" void VirtualButton__ctor_m6113 (VirtualButton_t1061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
