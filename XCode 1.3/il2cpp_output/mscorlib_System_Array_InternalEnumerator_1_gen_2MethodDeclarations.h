﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_2.h"

// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15651_gshared (InternalEnumerator_1_t2589 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15651(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2589 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15651_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15652_gshared (InternalEnumerator_1_t2589 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15652(__this, method) (( void (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15652_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15653_gshared (InternalEnumerator_1_t2589 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15653(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15653_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15654_gshared (InternalEnumerator_1_t2589 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15654(__this, method) (( void (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15654_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15655_gshared (InternalEnumerator_1_t2589 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15655(__this, method) (( bool (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15655_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C" double InternalEnumerator_1_get_Current_m15656_gshared (InternalEnumerator_1_t2589 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15656(__this, method) (( double (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15656_gshared)(__this, method)
