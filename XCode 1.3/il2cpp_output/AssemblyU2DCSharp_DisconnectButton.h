﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"

// DisconnectButton
struct  DisconnectButton_t193  : public MonoBehaviour_t18
{
	// System.Single DisconnectButton::w
	float ___w_1;
	// System.Single DisconnectButton::h
	float ___h_2;
	// UnityEngine.Rect DisconnectButton::r
	Rect_t30  ___r_3;
};
