﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t33;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// IOSSocialUseExample/<PostFBScreenshot>c__Iterator4
struct  U3CPostFBScreenshotU3Ec__Iterator4_t206  : public Object_t
{
	// System.Int32 IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::<height>__1
	int32_t ___U3CheightU3E__1_1;
	// UnityEngine.Texture2D IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::<tex>__2
	Texture2D_t33 * ___U3CtexU3E__2_2;
	// System.Int32 IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::$PC
	int32_t ___U24PC_3;
	// System.Object IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::$current
	Object_t * ___U24current_4;
};
