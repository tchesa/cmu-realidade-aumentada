﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_PlayerSignatureResult
struct ISN_PlayerSignatureResult_t118;
// ISN_Error
struct ISN_Error_t85;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_PlayerSignatureResult::.ctor(ISN_Error)
extern "C" void ISN_PlayerSignatureResult__ctor_m672 (ISN_PlayerSignatureResult_t118 * __this, ISN_Error_t85 * ___er, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_PlayerSignatureResult::.ctor(System.String,System.String,System.String,System.String)
extern "C" void ISN_PlayerSignatureResult__ctor_m673 (ISN_PlayerSignatureResult_t118 * __this, String_t* ___publicKeyUrl, String_t* ___signature, String_t* ___salt, String_t* ___timestamp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_PlayerSignatureResult::get_PublicKeyUrl()
extern "C" String_t* ISN_PlayerSignatureResult_get_PublicKeyUrl_m674 (ISN_PlayerSignatureResult_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ISN_PlayerSignatureResult::get_Signature()
extern "C" ByteU5BU5D_t119* ISN_PlayerSignatureResult_get_Signature_m675 (ISN_PlayerSignatureResult_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ISN_PlayerSignatureResult::get_Salt()
extern "C" ByteU5BU5D_t119* ISN_PlayerSignatureResult_get_Salt_m676 (ISN_PlayerSignatureResult_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ISN_PlayerSignatureResult::get_Timestamp()
extern "C" int64_t ISN_PlayerSignatureResult_get_Timestamp_m677 (ISN_PlayerSignatureResult_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
