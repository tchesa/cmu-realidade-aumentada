﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.InitDelegate
struct InitDelegate_t241;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t242;
// Facebook.Unity.CallbackManager
struct CallbackManager_t224;

#include "mscorlib_System_Object.h"

// Facebook.Unity.FacebookBase
struct  FacebookBase_t227  : public Object_t
{
	// Facebook.Unity.InitDelegate Facebook.Unity.FacebookBase::onInitCompleteDelegate
	InitDelegate_t241 * ___onInitCompleteDelegate_0;
	// Facebook.Unity.HideUnityDelegate Facebook.Unity.FacebookBase::onHideUnityDelegate
	HideUnityDelegate_t242 * ___onHideUnityDelegate_1;
	// Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::<CallbackManager>k__BackingField
	CallbackManager_t224 * ___U3CCallbackManagerU3Ek__BackingField_2;
};
