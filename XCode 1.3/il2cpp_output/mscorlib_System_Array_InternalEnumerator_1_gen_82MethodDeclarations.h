﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Terrain>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m28461(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3439 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15544_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Terrain>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m28462(__this, method) (( void (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15545_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Terrain>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28463(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15546_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Terrain>::Dispose()
#define InternalEnumerator_1_Dispose_m28464(__this, method) (( void (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15547_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Terrain>::MoveNext()
#define InternalEnumerator_1_MoveNext_m28465(__this, method) (( bool (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15548_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Terrain>::get_Current()
#define InternalEnumerator_1_get_Current_m28466(__this, method) (( Terrain_t1334 * (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15549_gshared)(__this, method)
