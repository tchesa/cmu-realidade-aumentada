﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// AchievementTemplate
struct  AchievementTemplate_t115  : public Object_t
{
	// System.String AchievementTemplate::id
	String_t* ___id_0;
	// System.Single AchievementTemplate::_progress
	float ____progress_1;
};
