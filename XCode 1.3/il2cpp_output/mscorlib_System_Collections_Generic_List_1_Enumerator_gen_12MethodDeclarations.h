﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1045;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m23835_gshared (Enumerator_t1150 * __this, List_1_t1045 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m23835(__this, ___l, method) (( void (*) (Enumerator_t1150 *, List_1_t1045 *, const MethodInfo*))Enumerator__ctor_m23835_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m23836_gshared (Enumerator_t1150 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m23836(__this, method) (( void (*) (Enumerator_t1150 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m23836_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23837_gshared (Enumerator_t1150 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23837(__this, method) (( Object_t * (*) (Enumerator_t1150 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23837_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m6579_gshared (Enumerator_t1150 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m6579(__this, method) (( void (*) (Enumerator_t1150 *, const MethodInfo*))Enumerator_Dispose_m6579_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m23838_gshared (Enumerator_t1150 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m23838(__this, method) (( void (*) (Enumerator_t1150 *, const MethodInfo*))Enumerator_VerifyState_m23838_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m6578_gshared (Enumerator_t1150 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m6578(__this, method) (( bool (*) (Enumerator_t1150 *, const MethodInfo*))Enumerator_MoveNext_m6578_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m6575_gshared (Enumerator_t1150 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m6575(__this, method) (( int32_t (*) (Enumerator_t1150 *, const MethodInfo*))Enumerator_get_Current_m6575_gshared)(__this, method)
