﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t27;
// System.Object
struct Object_t;
// CFX_Demo
struct CFX_Demo_t319;

#include "mscorlib_System_Object.h"

// CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA
struct  U3CRandomSpawnsCoroutineU3Ec__IteratorA_t318  : public Object_t
{
	// UnityEngine.GameObject CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::<particles>__0
	GameObject_t27 * ___U3CparticlesU3E__0_0;
	// System.Int32 CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::$PC
	int32_t ___U24PC_1;
	// System.Object CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::$current
	Object_t * ___U24current_2;
	// CFX_Demo CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::<>f__this
	CFX_Demo_t319 * ___U3CU3Ef__this_3;
};
