﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m27795(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3387 *, Event_t725 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m27694_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m27796(__this, method) (( Event_t725 * (*) (KeyValuePair_2_t3387 *, const MethodInfo*))KeyValuePair_2_get_Key_m27695_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m27797(__this, ___value, method) (( void (*) (KeyValuePair_2_t3387 *, Event_t725 *, const MethodInfo*))KeyValuePair_2_set_Key_m27696_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m27798(__this, method) (( int32_t (*) (KeyValuePair_2_t3387 *, const MethodInfo*))KeyValuePair_2_get_Value_m27697_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m27799(__this, ___value, method) (( void (*) (KeyValuePair_2_t3387 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m27698_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m27800(__this, method) (( String_t* (*) (KeyValuePair_2_t3387 *, const MethodInfo*))KeyValuePair_2_ToString_m27699_gshared)(__this, method)
