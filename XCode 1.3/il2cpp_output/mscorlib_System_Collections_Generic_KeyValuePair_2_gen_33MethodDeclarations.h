﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m24343(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3166 *, Type_t *, uint16_t, const MethodInfo*))KeyValuePair_2__ctor_m24239_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Key()
#define KeyValuePair_2_get_Key_m24344(__this, method) (( Type_t * (*) (KeyValuePair_2_t3166 *, const MethodInfo*))KeyValuePair_2_get_Key_m24240_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m24345(__this, ___value, method) (( void (*) (KeyValuePair_2_t3166 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m24241_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Value()
#define KeyValuePair_2_get_Value_m24346(__this, method) (( uint16_t (*) (KeyValuePair_2_t3166 *, const MethodInfo*))KeyValuePair_2_get_Value_m24242_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m24347(__this, ___value, method) (( void (*) (KeyValuePair_2_t3166 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Value_m24243_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::ToString()
#define KeyValuePair_2_ToString_m24348(__this, method) (( String_t* (*) (KeyValuePair_2_t3166 *, const MethodInfo*))KeyValuePair_2_ToString_m24244_gshared)(__this, method)
