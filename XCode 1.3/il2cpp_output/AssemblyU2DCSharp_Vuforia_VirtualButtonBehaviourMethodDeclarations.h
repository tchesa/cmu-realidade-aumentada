﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t448;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C" void VirtualButtonBehaviour__ctor_m2117 (VirtualButtonBehaviour_t448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
