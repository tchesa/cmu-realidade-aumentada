﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.MobileFacebookGameObject
struct MobileFacebookGameObject_t253;
// Facebook.Unity.Mobile.IMobileFacebookImplementation
struct IMobileFacebookImplementation_t482;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::.ctor()
extern "C" void MobileFacebookGameObject__ctor_m1510 (MobileFacebookGameObject_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.Mobile.IMobileFacebookImplementation Facebook.Unity.Mobile.MobileFacebookGameObject::get_MobileFacebook()
extern "C" Object_t * MobileFacebookGameObject_get_MobileFacebook_m1511 (MobileFacebookGameObject_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnAppInviteComplete(System.String)
extern "C" void MobileFacebookGameObject_OnAppInviteComplete_m1512 (MobileFacebookGameObject_t253 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnFetchDeferredAppLinkComplete(System.String)
extern "C" void MobileFacebookGameObject_OnFetchDeferredAppLinkComplete_m1513 (MobileFacebookGameObject_t253 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
