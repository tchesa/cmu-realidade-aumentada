﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_35MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m20460(__this, ___host, method) (( void (*) (Enumerator_t831 *, Dictionary_2_t659 *, const MethodInfo*))Enumerator__ctor_m16451_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20461(__this, method) (( Object_t * (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16452_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m20462(__this, method) (( void (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16453_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m20463(__this, method) (( void (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_Dispose_m16454_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m4051(__this, method) (( bool (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_MoveNext_m16455_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m4050(__this, method) (( PointerEventData_t652 * (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_get_Current_m16456_gshared)(__this, method)
