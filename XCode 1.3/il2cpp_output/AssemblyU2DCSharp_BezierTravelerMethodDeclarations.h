﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BezierTraveler
struct BezierTraveler_t342;

#include "codegen/il2cpp-codegen.h"

// System.Void BezierTraveler::.ctor()
extern "C" void BezierTraveler__ctor_m1934 (BezierTraveler_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BezierTraveler::get_normalizedProgress()
extern "C" float BezierTraveler_get_normalizedProgress_m1935 (BezierTraveler_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BezierTraveler::get_isPlaying()
extern "C" bool BezierTraveler_get_isPlaying_m1936 (BezierTraveler_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BezierTraveler::Update()
extern "C" void BezierTraveler_Update_m1937 (BezierTraveler_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BezierTraveler::Play()
extern "C" void BezierTraveler_Play_m1938 (BezierTraveler_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
