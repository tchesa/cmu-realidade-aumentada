﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger>
struct List_1_t347;
// ARAnimation/TransparencyTrigger
struct TransparencyTrigger_t339;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<ARAnimation/TransparencyTrigger>
struct  Enumerator_t2827 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<ARAnimation/TransparencyTrigger>::l
	List_1_t347 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<ARAnimation/TransparencyTrigger>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<ARAnimation/TransparencyTrigger>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<ARAnimation/TransparencyTrigger>::current
	TransparencyTrigger_t339 * ___current_3;
};
