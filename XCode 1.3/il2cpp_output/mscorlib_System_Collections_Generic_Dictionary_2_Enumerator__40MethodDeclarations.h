﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1210;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__40.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_40.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m26145_gshared (Enumerator_t3260 * __this, Dictionary_2_t1210 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m26145(__this, ___dictionary, method) (( void (*) (Enumerator_t3260 *, Dictionary_2_t1210 *, const MethodInfo*))Enumerator__ctor_m26145_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26146_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26146(__this, method) (( Object_t * (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26146_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26147_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m26147(__this, method) (( void (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m26147_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t58  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26148_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26148(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26148_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26149_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26149(__this, method) (( Object_t * (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26149_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26150_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26150(__this, method) (( Object_t * (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26151_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26151(__this, method) (( bool (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_MoveNext_m26151_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" KeyValuePair_2_t3255  Enumerator_get_Current_m26152_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26152(__this, method) (( KeyValuePair_2_t3255  (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_get_Current_m26152_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m26153_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m26153(__this, method) (( int32_t (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_get_CurrentKey_m26153_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_CurrentValue()
extern "C" VirtualButtonData_t970  Enumerator_get_CurrentValue_m26154_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m26154(__this, method) (( VirtualButtonData_t970  (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_get_CurrentValue_m26154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Reset()
extern "C" void Enumerator_Reset_m26155_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_Reset_m26155(__this, method) (( void (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_Reset_m26155_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::VerifyState()
extern "C" void Enumerator_VerifyState_m26156_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m26156(__this, method) (( void (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_VerifyState_m26156_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m26157_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m26157(__this, method) (( void (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_VerifyCurrent_m26157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m26158_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26158(__this, method) (( void (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_Dispose_m26158_gshared)(__this, method)
