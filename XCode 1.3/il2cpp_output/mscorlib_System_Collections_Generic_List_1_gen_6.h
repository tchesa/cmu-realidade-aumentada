﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// iAdBanner[]
struct iAdBannerU5BU5D_t2738;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<iAdBanner>
struct  List_1_t462  : public Object_t
{
	// T[] System.Collections.Generic.List`1<iAdBanner>::_items
	iAdBannerU5BU5D_t2738* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<iAdBanner>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<iAdBanner>::_version
	int32_t ____version_3;
};
struct List_1_t462_StaticFields{
	// T[] System.Collections.Generic.List`1<iAdBanner>::EmptyArray
	iAdBannerU5BU5D_t2738* ___EmptyArray_4;
};
