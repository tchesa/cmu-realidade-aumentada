﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Bezier[]
struct BezierU5BU5D_t377;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// BezierTraveler
struct  BezierTraveler_t342  : public MonoBehaviour_t18
{
	// Bezier[] BezierTraveler::beziers
	BezierU5BU5D_t377* ___beziers_1;
	// System.Single BezierTraveler::speed
	float ___speed_2;
	// System.Single BezierTraveler::progress
	float ___progress_3;
	// System.Boolean BezierTraveler::teste
	bool ___teste_4;
	// System.Boolean BezierTraveler::_isPlaying
	bool ____isPlaying_5;
};
