﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickManager
struct ClickManager_t195;
// UnionAssets.FLE.CEvent
struct CEvent_t74;

#include "codegen/il2cpp-codegen.h"

// System.Void ClickManager::.ctor()
extern "C" void ClickManager__ctor_m1104 (ClickManager_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickManager::Awake()
extern "C" void ClickManager_Awake_m1105 (ClickManager_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickManager::Update()
extern "C" void ClickManager_Update_m1106 (ClickManager_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickManager::OnData(UnionAssets.FLE.CEvent)
extern "C" void ClickManager_OnData_m1107 (ClickManager_t195 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
