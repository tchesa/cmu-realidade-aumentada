﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.AndroidFacebookLoader
struct AndroidFacebookLoader_t254;
// Facebook.Unity.FacebookGameObject
struct FacebookGameObject_t229;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Mobile.Android.AndroidFacebookLoader::.ctor()
extern "C" void AndroidFacebookLoader__ctor_m1434 (AndroidFacebookLoader_t254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.FacebookGameObject Facebook.Unity.Mobile.Android.AndroidFacebookLoader::get_FBGameObject()
extern "C" FacebookGameObject_t229 * AndroidFacebookLoader_get_FBGameObject_m1435 (AndroidFacebookLoader_t254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
