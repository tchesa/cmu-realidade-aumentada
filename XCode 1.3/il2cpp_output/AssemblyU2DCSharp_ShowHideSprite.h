﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t387;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Color.h"

// ShowHideSprite
struct  ShowHideSprite_t386  : public MonoBehaviour_t18
{
	// System.Boolean ShowHideSprite::showing
	bool ___showing_1;
	// System.Single ShowHideSprite::showSpeed
	float ___showSpeed_2;
	// UnityEngine.Color ShowHideSprite::color
	Color_t5  ___color_3;
	// UnityEngine.Color ShowHideSprite::transparent
	Color_t5  ___transparent_4;
	// UnityEngine.UI.Image ShowHideSprite::image
	Image_t387 * ___image_5;
};
