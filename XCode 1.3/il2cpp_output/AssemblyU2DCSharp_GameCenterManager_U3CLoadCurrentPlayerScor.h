﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Object_t;
// GameCenterManager
struct GameCenterManager_t98;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_GCBoardTimeSpan.h"
#include "AssemblyU2DCSharp_GCCollectionType.h"

// GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0
struct  U3CLoadCurrentPlayerScoreLocalU3Ec__Iterator0_t97  : public Object_t
{
	// System.String GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::leaderboardId
	String_t* ___leaderboardId_0;
	// GCBoardTimeSpan GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::timeSpan
	int32_t ___timeSpan_1;
	// GCCollectionType GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::collection
	int32_t ___collection_2;
	// System.Int32 GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::$PC
	int32_t ___U24PC_3;
	// System.Object GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::$current
	Object_t * ___U24current_4;
	// System.String GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::<$>leaderboardId
	String_t* ___U3CU24U3EleaderboardId_5;
	// GCBoardTimeSpan GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::<$>timeSpan
	int32_t ___U3CU24U3EtimeSpan_6;
	// GCCollectionType GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::<$>collection
	int32_t ___U3CU24U3Ecollection_7;
	// GameCenterManager GameCenterManager/<LoadCurrentPlayerScoreLocal>c__Iterator0::<>f__this
	GameCenterManager_t98 * ___U3CU3Ef__this_8;
};
