﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX3_Demo/<RandomSpawnsCoroutine>c__Iterator9
struct U3CRandomSpawnsCoroutineU3Ec__Iterator9_t315;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX3_Demo/<RandomSpawnsCoroutine>c__Iterator9::.ctor()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__Iterator9__ctor_m1723 (U3CRandomSpawnsCoroutineU3Ec__Iterator9_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX3_Demo/<RandomSpawnsCoroutine>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CRandomSpawnsCoroutineU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1724 (U3CRandomSpawnsCoroutineU3Ec__Iterator9_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX3_Demo/<RandomSpawnsCoroutine>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CRandomSpawnsCoroutineU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1725 (U3CRandomSpawnsCoroutineU3Ec__Iterator9_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX3_Demo/<RandomSpawnsCoroutine>c__Iterator9::MoveNext()
extern "C" bool U3CRandomSpawnsCoroutineU3Ec__Iterator9_MoveNext_m1726 (U3CRandomSpawnsCoroutineU3Ec__Iterator9_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo/<RandomSpawnsCoroutine>c__Iterator9::Dispose()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__Iterator9_Dispose_m1727 (U3CRandomSpawnsCoroutineU3Ec__Iterator9_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo/<RandomSpawnsCoroutine>c__Iterator9::Reset()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__Iterator9_Reset_m1728 (U3CRandomSpawnsCoroutineU3Ec__Iterator9_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
