﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>
struct ValueCollection_t2793;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t2785;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3652;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Single[]
struct SingleU5BU5D_t23;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_47.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m18660_gshared (ValueCollection_t2793 * __this, Dictionary_2_t2785 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m18660(__this, ___dictionary, method) (( void (*) (ValueCollection_t2793 *, Dictionary_2_t2785 *, const MethodInfo*))ValueCollection__ctor_m18660_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18661_gshared (ValueCollection_t2793 * __this, float ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18661(__this, ___item, method) (( void (*) (ValueCollection_t2793 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18661_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18662_gshared (ValueCollection_t2793 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18662(__this, method) (( void (*) (ValueCollection_t2793 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18662_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18663_gshared (ValueCollection_t2793 * __this, float ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18663(__this, ___item, method) (( bool (*) (ValueCollection_t2793 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18663_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18664_gshared (ValueCollection_t2793 * __this, float ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18664(__this, ___item, method) (( bool (*) (ValueCollection_t2793 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18664_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18665_gshared (ValueCollection_t2793 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18665(__this, method) (( Object_t* (*) (ValueCollection_t2793 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18665_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m18666_gshared (ValueCollection_t2793 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m18666(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2793 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m18666_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18667_gshared (ValueCollection_t2793 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18667(__this, method) (( Object_t * (*) (ValueCollection_t2793 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18667_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18668_gshared (ValueCollection_t2793 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18668(__this, method) (( bool (*) (ValueCollection_t2793 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18668_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18669_gshared (ValueCollection_t2793 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18669(__this, method) (( bool (*) (ValueCollection_t2793 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18669_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m18670_gshared (ValueCollection_t2793 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m18670(__this, method) (( Object_t * (*) (ValueCollection_t2793 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m18670_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m18671_gshared (ValueCollection_t2793 * __this, SingleU5BU5D_t23* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m18671(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2793 *, SingleU5BU5D_t23*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m18671_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::GetEnumerator()
extern "C" Enumerator_t2794  ValueCollection_GetEnumerator_m18672_gshared (ValueCollection_t2793 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m18672(__this, method) (( Enumerator_t2794  (*) (ValueCollection_t2793 *, const MethodInfo*))ValueCollection_GetEnumerator_m18672_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m18673_gshared (ValueCollection_t2793 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m18673(__this, method) (( int32_t (*) (ValueCollection_t2793 *, const MethodInfo*))ValueCollection_get_Count_m18673_gshared)(__this, method)
