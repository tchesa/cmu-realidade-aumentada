﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t119;
// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// ISN_DeviceGUID
struct  ISN_DeviceGUID_t170  : public Object_t
{
	// System.Byte[] ISN_DeviceGUID::_Bytes
	ByteU5BU5D_t119* ____Bytes_0;
	// System.String ISN_DeviceGUID::_Base64
	String_t* ____Base64_1;
};
