﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Contexts.ContextCallbackObject
struct ContextCallbackObject_t2183;
// System.Runtime.Remoting.Contexts.CrossContextDelegate
struct CrossContextDelegate_t2481;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Contexts.ContextCallbackObject::.ctor()
extern "C" void ContextCallbackObject__ctor_m13261 (ContextCallbackObject_t2183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Contexts.ContextCallbackObject::DoCallBack(System.Runtime.Remoting.Contexts.CrossContextDelegate)
extern "C" void ContextCallbackObject_DoCallBack_m13262 (ContextCallbackObject_t2183 * __this, CrossContextDelegate_t2481 * ___deleg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
