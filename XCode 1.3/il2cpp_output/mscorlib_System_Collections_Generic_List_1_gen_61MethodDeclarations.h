﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1288;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t3749;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3750;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t3751;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t3392;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1440;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t3396;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t3399;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m27843_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1__ctor_m27843(__this, method) (( void (*) (List_1_t1288 *, const MethodInfo*))List_1__ctor_m27843_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m27844_gshared (List_1_t1288 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m27844(__this, ___collection, method) (( void (*) (List_1_t1288 *, Object_t*, const MethodInfo*))List_1__ctor_m27844_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m8142_gshared (List_1_t1288 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m8142(__this, ___capacity, method) (( void (*) (List_1_t1288 *, int32_t, const MethodInfo*))List_1__ctor_m8142_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m27845_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m27845(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m27845_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27846_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27846(__this, method) (( Object_t* (*) (List_1_t1288 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27846_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m27847_gshared (List_1_t1288 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m27847(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1288 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m27847_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m27848_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m27848(__this, method) (( Object_t * (*) (List_1_t1288 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m27848_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m27849_gshared (List_1_t1288 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m27849(__this, ___item, method) (( int32_t (*) (List_1_t1288 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m27849_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m27850_gshared (List_1_t1288 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m27850(__this, ___item, method) (( bool (*) (List_1_t1288 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m27850_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m27851_gshared (List_1_t1288 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m27851(__this, ___item, method) (( int32_t (*) (List_1_t1288 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m27851_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m27852_gshared (List_1_t1288 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m27852(__this, ___index, ___item, method) (( void (*) (List_1_t1288 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m27852_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m27853_gshared (List_1_t1288 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m27853(__this, ___item, method) (( void (*) (List_1_t1288 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m27853_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27854_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27854(__this, method) (( bool (*) (List_1_t1288 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27854_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m27855_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m27855(__this, method) (( bool (*) (List_1_t1288 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m27855_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m27856_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m27856(__this, method) (( Object_t * (*) (List_1_t1288 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m27856_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m27857_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m27857(__this, method) (( bool (*) (List_1_t1288 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m27857_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m27858_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m27858(__this, method) (( bool (*) (List_1_t1288 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m27858_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m27859_gshared (List_1_t1288 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m27859(__this, ___index, method) (( Object_t * (*) (List_1_t1288 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m27859_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m27860_gshared (List_1_t1288 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m27860(__this, ___index, ___value, method) (( void (*) (List_1_t1288 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m27860_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m27861_gshared (List_1_t1288 * __this, UICharInfo_t859  ___item, const MethodInfo* method);
#define List_1_Add_m27861(__this, ___item, method) (( void (*) (List_1_t1288 *, UICharInfo_t859 , const MethodInfo*))List_1_Add_m27861_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m27862_gshared (List_1_t1288 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m27862(__this, ___newCount, method) (( void (*) (List_1_t1288 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m27862_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m27863_gshared (List_1_t1288 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m27863(__this, ___collection, method) (( void (*) (List_1_t1288 *, Object_t*, const MethodInfo*))List_1_AddCollection_m27863_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m27864_gshared (List_1_t1288 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m27864(__this, ___enumerable, method) (( void (*) (List_1_t1288 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m27864_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m27865_gshared (List_1_t1288 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m27865(__this, ___collection, method) (( void (*) (List_1_t1288 *, Object_t*, const MethodInfo*))List_1_AddRange_m27865_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3392 * List_1_AsReadOnly_m27866_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m27866(__this, method) (( ReadOnlyCollection_1_t3392 * (*) (List_1_t1288 *, const MethodInfo*))List_1_AsReadOnly_m27866_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m27867_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_Clear_m27867(__this, method) (( void (*) (List_1_t1288 *, const MethodInfo*))List_1_Clear_m27867_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m27868_gshared (List_1_t1288 * __this, UICharInfo_t859  ___item, const MethodInfo* method);
#define List_1_Contains_m27868(__this, ___item, method) (( bool (*) (List_1_t1288 *, UICharInfo_t859 , const MethodInfo*))List_1_Contains_m27868_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m27869_gshared (List_1_t1288 * __this, UICharInfoU5BU5D_t1440* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m27869(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1288 *, UICharInfoU5BU5D_t1440*, int32_t, const MethodInfo*))List_1_CopyTo_m27869_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Find(System.Predicate`1<T>)
extern "C" UICharInfo_t859  List_1_Find_m27870_gshared (List_1_t1288 * __this, Predicate_1_t3396 * ___match, const MethodInfo* method);
#define List_1_Find_m27870(__this, ___match, method) (( UICharInfo_t859  (*) (List_1_t1288 *, Predicate_1_t3396 *, const MethodInfo*))List_1_Find_m27870_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m27871_gshared (Object_t * __this /* static, unused */, Predicate_1_t3396 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m27871(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3396 *, const MethodInfo*))List_1_CheckMatch_m27871_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m27872_gshared (List_1_t1288 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3396 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m27872(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1288 *, int32_t, int32_t, Predicate_1_t3396 *, const MethodInfo*))List_1_GetIndex_m27872_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t3391  List_1_GetEnumerator_m27873_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m27873(__this, method) (( Enumerator_t3391  (*) (List_1_t1288 *, const MethodInfo*))List_1_GetEnumerator_m27873_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m27874_gshared (List_1_t1288 * __this, UICharInfo_t859  ___item, const MethodInfo* method);
#define List_1_IndexOf_m27874(__this, ___item, method) (( int32_t (*) (List_1_t1288 *, UICharInfo_t859 , const MethodInfo*))List_1_IndexOf_m27874_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m27875_gshared (List_1_t1288 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m27875(__this, ___start, ___delta, method) (( void (*) (List_1_t1288 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m27875_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m27876_gshared (List_1_t1288 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m27876(__this, ___index, method) (( void (*) (List_1_t1288 *, int32_t, const MethodInfo*))List_1_CheckIndex_m27876_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m27877_gshared (List_1_t1288 * __this, int32_t ___index, UICharInfo_t859  ___item, const MethodInfo* method);
#define List_1_Insert_m27877(__this, ___index, ___item, method) (( void (*) (List_1_t1288 *, int32_t, UICharInfo_t859 , const MethodInfo*))List_1_Insert_m27877_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m27878_gshared (List_1_t1288 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m27878(__this, ___collection, method) (( void (*) (List_1_t1288 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m27878_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m27879_gshared (List_1_t1288 * __this, UICharInfo_t859  ___item, const MethodInfo* method);
#define List_1_Remove_m27879(__this, ___item, method) (( bool (*) (List_1_t1288 *, UICharInfo_t859 , const MethodInfo*))List_1_Remove_m27879_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m27880_gshared (List_1_t1288 * __this, Predicate_1_t3396 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m27880(__this, ___match, method) (( int32_t (*) (List_1_t1288 *, Predicate_1_t3396 *, const MethodInfo*))List_1_RemoveAll_m27880_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m27881_gshared (List_1_t1288 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m27881(__this, ___index, method) (( void (*) (List_1_t1288 *, int32_t, const MethodInfo*))List_1_RemoveAt_m27881_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse()
extern "C" void List_1_Reverse_m27882_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_Reverse_m27882(__this, method) (( void (*) (List_1_t1288 *, const MethodInfo*))List_1_Reverse_m27882_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort()
extern "C" void List_1_Sort_m27883_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_Sort_m27883(__this, method) (( void (*) (List_1_t1288 *, const MethodInfo*))List_1_Sort_m27883_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m27884_gshared (List_1_t1288 * __this, Comparison_1_t3399 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m27884(__this, ___comparison, method) (( void (*) (List_1_t1288 *, Comparison_1_t3399 *, const MethodInfo*))List_1_Sort_m27884_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t1440* List_1_ToArray_m27885_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_ToArray_m27885(__this, method) (( UICharInfoU5BU5D_t1440* (*) (List_1_t1288 *, const MethodInfo*))List_1_ToArray_m27885_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m27886_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m27886(__this, method) (( void (*) (List_1_t1288 *, const MethodInfo*))List_1_TrimExcess_m27886_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m27887_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m27887(__this, method) (( int32_t (*) (List_1_t1288 *, const MethodInfo*))List_1_get_Capacity_m27887_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m27888_gshared (List_1_t1288 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m27888(__this, ___value, method) (( void (*) (List_1_t1288 *, int32_t, const MethodInfo*))List_1_set_Capacity_m27888_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m27889_gshared (List_1_t1288 * __this, const MethodInfo* method);
#define List_1_get_Count_m27889(__this, method) (( int32_t (*) (List_1_t1288 *, const MethodInfo*))List_1_get_Count_m27889_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t859  List_1_get_Item_m27890_gshared (List_1_t1288 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m27890(__this, ___index, method) (( UICharInfo_t859  (*) (List_1_t1288 *, int32_t, const MethodInfo*))List_1_get_Item_m27890_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m27891_gshared (List_1_t1288 * __this, int32_t ___index, UICharInfo_t859  ___value, const MethodInfo* method);
#define List_1_set_Item_m27891(__this, ___index, ___value, method) (( void (*) (List_1_t1288 *, int32_t, UICharInfo_t859 , const MethodInfo*))List_1_set_Item_m27891_gshared)(__this, ___index, ___value, method)
