﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t28;

#include "mscorlib_System_Object.h"

// PaymentManagerExample
struct  PaymentManagerExample_t186  : public Object_t
{
};
struct PaymentManagerExample_t186_StaticFields{
	// System.Boolean PaymentManagerExample::IsInitialized
	bool ___IsInitialized_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PaymentManagerExample::<>f__switch$map0
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map0_3;
};
