﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.AndroidFacebookGameObject
struct AndroidFacebookGameObject_t252;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::.ctor()
extern "C" void AndroidFacebookGameObject__ctor_m1432 (AndroidFacebookGameObject_t252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnAwake()
extern "C" void AndroidFacebookGameObject_OnAwake_m1433 (AndroidFacebookGameObject_t252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
