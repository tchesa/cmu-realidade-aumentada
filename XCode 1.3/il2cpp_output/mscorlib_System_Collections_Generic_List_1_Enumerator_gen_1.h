﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<AchievementTemplate>
struct List_1_t104;
// AchievementTemplate
struct AchievementTemplate_t115;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<AchievementTemplate>
struct  Enumerator_t496 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<AchievementTemplate>::l
	List_1_t104 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<AchievementTemplate>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<AchievementTemplate>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<AchievementTemplate>::current
	AchievementTemplate_t115 * ___current_3;
};
