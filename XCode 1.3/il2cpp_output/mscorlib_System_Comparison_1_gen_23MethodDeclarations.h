﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen_7MethodDeclarations.h"

// System.Void System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m20207(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2899 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m15663_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>::Invoke(T,T)
#define Comparison_1_Invoke_m20208(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2899 *, Entry_t619 *, Entry_t619 *, const MethodInfo*))Comparison_1_Invoke_m15664_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m20209(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2899 *, Entry_t619 *, Entry_t619 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m15665_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m20210(__this, ___result, method) (( int32_t (*) (Comparison_1_t2899 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m15666_gshared)(__this, ___result, method)
