﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t8  : public Array_t { };
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t21  : public Array_t { };
// UnityEngine.Color[,]
// UnityEngine.Color[,]
struct ColorU5BU2CU5D_t22  : public Array_t { };
// UnityEngine.Color[]
// UnityEngine.Color[]
struct ColorU5BU5D_t52  : public Array_t { };
// UnityEngine.Rect[]
// UnityEngine.Rect[]
struct RectU5BU5D_t24  : public Array_t { };
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct TransformU5BU5D_t32  : public Array_t { };
// UnityEngine.Component[]
// UnityEngine.Component[]
struct ComponentU5BU5D_t54  : public Array_t { };
// UnityEngine.Object[]
// UnityEngine.Object[]
struct ObjectU5BU5D_t1153  : public Array_t { };
// UnityEngine.Material[]
// UnityEngine.Material[]
struct MaterialU5BU5D_t49  : public Array_t { };
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t311  : public Array_t { };
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t550  : public Array_t { };
// UnityEngine.ParticleSystem[]
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t331  : public Array_t { };
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t3832  : public Array_t { };
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t3833  : public Array_t { };
// UnityEngine.WebCamDevice[]
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t575  : public Array_t { };
// UnityEngine.Shader[]
// UnityEngine.Shader[]
struct ShaderU5BU5D_t395  : public Array_t { };
// UnityEngine.Renderer[]
// UnityEngine.Renderer[]
struct RendererU5BU5D_t587  : public Array_t { };
// UnityEngine.Collider[]
// UnityEngine.Collider[]
struct ColliderU5BU5D_t588  : public Array_t { };
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct CameraU5BU5D_t600  : public Array_t { };
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t835  : public Array_t { };
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t840  : public Array_t { };
// UnityEngine.Font[]
// UnityEngine.Font[]
struct FontU5BU5D_t2924  : public Array_t { };
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t722  : public Array_t { };
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t2950  : public Array_t { };
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1441  : public Array_t { };
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1440  : public Array_t { };
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t2990  : public Array_t { };
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3010  : public Array_t { };
// UnityEngine.Color32[]
// UnityEngine.Color32[]
struct Color32U5BU5D_t945  : public Array_t { };
// UnityEngine.MeshFilter[]
// UnityEngine.MeshFilter[]
struct MeshFilterU5BU5D_t1198  : public Array_t { };
// UnityEngine.MeshRenderer[]
// UnityEngine.MeshRenderer[]
struct MeshRendererU5BU5D_t3335  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t1252  : public Array_t { };
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t1263  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t1451  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t1453  : public Array_t { };
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t1268  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t1251  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t3356  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t1432  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_t1452  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t1433  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct ScoreU5BU5D_t1454  : public Array_t { };
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1273  : public Array_t { };
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t3368  : public Array_t { };
// UnityEngine.Event[]
// UnityEngine.Event[]
struct EventU5BU5D_t3367  : public Array_t { };
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t3419  : public Array_t { };
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t3424  : public Array_t { };
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t3432  : public Array_t { };
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1443  : public Array_t { };
// UnityEngine.Terrain[]
// UnityEngine.Terrain[]
struct TerrainU5BU5D_t1444  : public Array_t { };
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct LayoutCacheU5BU5D_t3444  : public Array_t { };
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t3450  : public Array_t { };
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t1396  : public Array_t { };
// UnityEngine.Display[]
// UnityEngine.Display[]
struct DisplayU5BU5D_t1422  : public Array_t { };
