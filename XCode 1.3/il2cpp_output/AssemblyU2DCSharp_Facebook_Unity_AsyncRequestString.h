﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Uri
struct Uri_t292;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t295;
// UnityEngine.WWWForm
struct WWWForm_t293;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t294;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_Facebook_Unity_HttpMethod.h"

// Facebook.Unity.AsyncRequestString
struct  AsyncRequestString_t290  : public MonoBehaviour_t18
{
	// System.Uri Facebook.Unity.AsyncRequestString::url
	Uri_t292 * ___url_1;
	// Facebook.Unity.HttpMethod Facebook.Unity.AsyncRequestString::method
	int32_t ___method_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.AsyncRequestString::formData
	Object_t* ___formData_3;
	// UnityEngine.WWWForm Facebook.Unity.AsyncRequestString::query
	WWWForm_t293 * ___query_4;
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult> Facebook.Unity.AsyncRequestString::callback
	FacebookDelegate_1_t294 * ___callback_5;
};
