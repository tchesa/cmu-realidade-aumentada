﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// CFX3_Demo
struct CFX3_Demo_t314;

#include "mscorlib_System_Object.h"

// CFX3_Demo/<CheckForDeletedParticles>c__Iterator8
struct  U3CCheckForDeletedParticlesU3Ec__Iterator8_t313  : public Object_t
{
	// System.Int32 CFX3_Demo/<CheckForDeletedParticles>c__Iterator8::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 CFX3_Demo/<CheckForDeletedParticles>c__Iterator8::$PC
	int32_t ___U24PC_1;
	// System.Object CFX3_Demo/<CheckForDeletedParticles>c__Iterator8::$current
	Object_t * ___U24current_2;
	// CFX3_Demo CFX3_Demo/<CheckForDeletedParticles>c__Iterator8::<>f__this
	CFX3_Demo_t314 * ___U3CU3Ef__this_3;
};
