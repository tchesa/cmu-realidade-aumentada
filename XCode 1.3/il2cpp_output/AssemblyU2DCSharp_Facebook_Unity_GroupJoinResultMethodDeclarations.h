﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.GroupJoinResult
struct GroupJoinResult_t281;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.GroupJoinResult::.ctor(System.String)
extern "C" void GroupJoinResult__ctor_m1601 (GroupJoinResult_t281 * __this, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
