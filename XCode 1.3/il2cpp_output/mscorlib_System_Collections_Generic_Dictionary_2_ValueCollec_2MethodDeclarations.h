﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m22194(__this, ___dictionary, method) (( void (*) (ValueCollection_t1125 *, Dictionary_2_t885 *, const MethodInfo*))ValueCollection__ctor_m16163_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22195(__this, ___item, method) (( void (*) (ValueCollection_t1125 *, VideoBackgroundAbstractBehaviour_t445 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22196(__this, method) (( void (*) (ValueCollection_t1125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16165_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22197(__this, ___item, method) (( bool (*) (ValueCollection_t1125 *, VideoBackgroundAbstractBehaviour_t445 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16166_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22198(__this, ___item, method) (( bool (*) (ValueCollection_t1125 *, VideoBackgroundAbstractBehaviour_t445 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16167_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22199(__this, method) (( Object_t* (*) (ValueCollection_t1125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16168_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m22200(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1125 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16169_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22201(__this, method) (( Object_t * (*) (ValueCollection_t1125 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16170_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22202(__this, method) (( bool (*) (ValueCollection_t1125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16171_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22203(__this, method) (( bool (*) (ValueCollection_t1125 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16172_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m22204(__this, method) (( Object_t * (*) (ValueCollection_t1125 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16173_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m22205(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1125 *, VideoBackgroundAbstractBehaviourU5BU5D_t3022*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m16174_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::GetEnumerator()
#define ValueCollection_GetEnumerator_m6453(__this, method) (( Enumerator_t1124  (*) (ValueCollection_t1125 *, const MethodInfo*))ValueCollection_GetEnumerator_m16175_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Count()
#define ValueCollection_get_Count_m22206(__this, method) (( int32_t (*) (ValueCollection_t1125 *, const MethodInfo*))ValueCollection_get_Count_m16176_gshared)(__this, method)
