﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo
struct CFX_Demo_t319;
// UnityEngine.GameObject
struct GameObject_t27;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo::.ctor()
extern "C" void CFX_Demo__ctor_m1748 (CFX_Demo_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::OnMouseDown()
extern "C" void CFX_Demo_OnMouseDown_m1749 (CFX_Demo_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX_Demo::spawnParticle()
extern "C" GameObject_t27 * CFX_Demo_spawnParticle_m1750 (CFX_Demo_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::OnGUI()
extern "C" void CFX_Demo_OnGUI_m1751 (CFX_Demo_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX_Demo::RandomSpawnsCoroutine()
extern "C" Object_t * CFX_Demo_RandomSpawnsCoroutine_m1752 (CFX_Demo_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::Update()
extern "C" void CFX_Demo_Update_m1753 (CFX_Demo_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::prevParticle()
extern "C" void CFX_Demo_prevParticle_m1754 (CFX_Demo_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::nextParticle()
extern "C" void CFX_Demo_nextParticle_m1755 (CFX_Demo_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
