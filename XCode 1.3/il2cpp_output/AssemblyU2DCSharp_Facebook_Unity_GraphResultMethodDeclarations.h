﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.GraphResult
struct GraphResult_t278;
// UnityEngine.WWW
struct WWW_t289;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t279;
// UnityEngine.Texture2D
struct Texture2D_t33;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.GraphResult::.ctor(UnityEngine.WWW)
extern "C" void GraphResult__ctor_m1592 (GraphResult_t278 * __this, WWW_t289 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Object> Facebook.Unity.GraphResult::get_ResultList()
extern "C" Object_t* GraphResult_get_ResultList_m1593 (GraphResult_t278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.GraphResult::set_ResultList(System.Collections.Generic.IList`1<System.Object>)
extern "C" void GraphResult_set_ResultList_m1594 (GraphResult_t278 * __this, Object_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Facebook.Unity.GraphResult::get_Texture()
extern "C" Texture2D_t33 * GraphResult_get_Texture_m1595 (GraphResult_t278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.GraphResult::set_Texture(UnityEngine.Texture2D)
extern "C" void GraphResult_set_Texture_m1596 (GraphResult_t278 * __this, Texture2D_t33 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.GraphResult::Init(System.String)
extern "C" void GraphResult_Init_m1597 (GraphResult_t278 * __this, String_t* ___rawResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
