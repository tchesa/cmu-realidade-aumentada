﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t27;

#include "mscorlib_System_Object.h"

// Facebook.Unity.ComponentFactory
struct  ComponentFactory_t235  : public Object_t
{
};
struct ComponentFactory_t235_StaticFields{
	// UnityEngine.GameObject Facebook.Unity.ComponentFactory::facebookGameObject
	GameObject_t27 * ___facebookGameObject_1;
};
