﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIStyle
struct GUIStyle_t184;
// UnityEngine.Texture2D
struct Texture2D_t33;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// IOSSocialUseExample
struct  IOSSocialUseExample_t207  : public MonoBehaviour_t18
{
	// UnityEngine.GUIStyle IOSSocialUseExample::style
	GUIStyle_t184 * ___style_1;
	// UnityEngine.GUIStyle IOSSocialUseExample::style2
	GUIStyle_t184 * ___style2_2;
	// UnityEngine.Texture2D IOSSocialUseExample::textureForPost
	Texture2D_t33 * ___textureForPost_3;
};
