﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceTokenListener
struct DeviceTokenListener_t139;

#include "codegen/il2cpp-codegen.h"

// System.Void DeviceTokenListener::.ctor()
extern "C" void DeviceTokenListener__ctor_m784 (DeviceTokenListener_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceTokenListener::Create()
extern "C" void DeviceTokenListener_Create_m785 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceTokenListener::Awake()
extern "C" void DeviceTokenListener_Awake_m786 (DeviceTokenListener_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
