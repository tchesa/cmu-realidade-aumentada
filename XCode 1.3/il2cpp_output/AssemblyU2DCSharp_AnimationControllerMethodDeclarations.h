﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationController
struct AnimationController_t357;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// UnityEngine.AudioClip
struct AudioClip_t31;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void AnimationController::.ctor()
extern "C" void AnimationController__ctor_m1830 (AnimationController_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController::Start()
extern "C" void AnimationController_Start_m1831 (AnimationController_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController::Update()
extern "C" void AnimationController_Update_m1832 (AnimationController_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController::Play()
extern "C" void AnimationController_Play_m1833 (AnimationController_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController::Play(System.Boolean)
extern "C" void AnimationController_Play_m1834 (AnimationController_t357 * __this, bool ___playSound, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController::Stop()
extern "C" void AnimationController_Stop_m1835 (AnimationController_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationController::IPlay()
extern "C" Object_t * AnimationController_IPlay_m1836 (AnimationController_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController::PlayClipWithDelay(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern "C" void AnimationController_PlayClipWithDelay_m1837 (AnimationController_t357 * __this, AudioClip_t31 * ___clip, Vector3_t6  ___position, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationController::IPlayClipWithDelay(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern "C" Object_t * AnimationController_IPlayClipWithDelay_m1838 (AnimationController_t357 * __this, AudioClip_t31 * ___clip, Vector3_t6  ___position, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
