﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_DeviceGUID
struct ISN_DeviceGUID_t170;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_DeviceGUID::.ctor(System.String)
extern "C" void ISN_DeviceGUID__ctor_m936 (ISN_DeviceGUID_t170 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_DeviceGUID::GetBase64String()
extern "C" String_t* ISN_DeviceGUID_GetBase64String_m937 (ISN_DeviceGUID_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ISN_DeviceGUID::GetBytes()
extern "C" ByteU5BU5D_t119* ISN_DeviceGUID_GetBytes_m938 (ISN_DeviceGUID_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
