﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_Editor_EditorFacebookMockDi_0.h"

// Facebook.Unity.Editor.Dialogs.EmptyMockDialog
struct  EmptyMockDialog_t271  : public EditorFacebookMockDialog_t270
{
	// System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::<EmptyDialogTitle>k__BackingField
	String_t* ___U3CEmptyDialogTitleU3Ek__BackingField_3;
};
