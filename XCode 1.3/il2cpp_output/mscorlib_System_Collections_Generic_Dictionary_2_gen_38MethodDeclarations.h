﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1261;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2650;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3615;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t3616;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t2660;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t2664;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m8129_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m8129(__this, method) (( void (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2__ctor_m8129_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16251_gshared (Dictionary_2_t1261 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16251(__this, ___comparer, method) (( void (*) (Dictionary_2_t1261 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16251_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m16253_gshared (Dictionary_2_t1261 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m16253(__this, ___capacity, method) (( void (*) (Dictionary_2_t1261 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16253_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m16255_gshared (Dictionary_2_t1261 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m16255(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1261 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m16255_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m16257_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m16257(__this, method) (( Object_t * (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16257_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m16259_gshared (Dictionary_2_t1261 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16259(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1261 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16259_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16261_gshared (Dictionary_2_t1261 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16261(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1261 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16261_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16263_gshared (Dictionary_2_t1261 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m16263(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1261 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16263_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m16265_gshared (Dictionary_2_t1261 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m16265(__this, ___key, method) (( bool (*) (Dictionary_2_t1261 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16265_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16267_gshared (Dictionary_2_t1261 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m16267(__this, ___key, method) (( void (*) (Dictionary_2_t1261 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16267_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16269_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16269(__this, method) (( bool (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16269_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16271_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16271(__this, method) (( Object_t * (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16271_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16273_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16273(__this, method) (( bool (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16273_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16275_gshared (Dictionary_2_t1261 * __this, KeyValuePair_2_t2658  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16275(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1261 *, KeyValuePair_2_t2658 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16275_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16277_gshared (Dictionary_2_t1261 * __this, KeyValuePair_2_t2658  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16277(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1261 *, KeyValuePair_2_t2658 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16277_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16279_gshared (Dictionary_2_t1261 * __this, KeyValuePair_2U5BU5D_t3615* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16279(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1261 *, KeyValuePair_2U5BU5D_t3615*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16279_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16281_gshared (Dictionary_2_t1261 * __this, KeyValuePair_2_t2658  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16281(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1261 *, KeyValuePair_2_t2658 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16281_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16283_gshared (Dictionary_2_t1261 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16283(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1261 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16283_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16285_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16285(__this, method) (( Object_t * (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16285_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16287_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16287(__this, method) (( Object_t* (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16287_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16289_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16289(__this, method) (( Object_t * (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16289_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m16291_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m16291(__this, method) (( int32_t (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_get_Count_m16291_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m16293_gshared (Dictionary_2_t1261 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m16293(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1261 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m16293_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m16295_gshared (Dictionary_2_t1261 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m16295(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1261 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m16295_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m16297_gshared (Dictionary_2_t1261 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m16297(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1261 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16297_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m16299_gshared (Dictionary_2_t1261 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m16299(__this, ___size, method) (( void (*) (Dictionary_2_t1261 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16299_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m16301_gshared (Dictionary_2_t1261 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m16301(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1261 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16301_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2658  Dictionary_2_make_pair_m16303_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m16303(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2658  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m16303_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m16305_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m16305(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m16305_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m16307_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m16307(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m16307_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m16309_gshared (Dictionary_2_t1261 * __this, KeyValuePair_2U5BU5D_t3615* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m16309(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1261 *, KeyValuePair_2U5BU5D_t3615*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16309_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m16311_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m16311(__this, method) (( void (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_Resize_m16311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m16313_gshared (Dictionary_2_t1261 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m16313(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1261 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m16313_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m16315_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m16315(__this, method) (( void (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_Clear_m16315_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m16317_gshared (Dictionary_2_t1261 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m16317(__this, ___key, method) (( bool (*) (Dictionary_2_t1261 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m16317_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m16319_gshared (Dictionary_2_t1261 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m16319(__this, ___value, method) (( bool (*) (Dictionary_2_t1261 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m16319_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m16321_gshared (Dictionary_2_t1261 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m16321(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1261 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m16321_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m16323_gshared (Dictionary_2_t1261 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m16323(__this, ___sender, method) (( void (*) (Dictionary_2_t1261 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16323_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m16325_gshared (Dictionary_2_t1261 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m16325(__this, ___key, method) (( bool (*) (Dictionary_2_t1261 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m16325_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m16327_gshared (Dictionary_2_t1261 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m16327(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1261 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m16327_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Keys()
extern "C" KeyCollection_t2660 * Dictionary_2_get_Keys_m16329_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m16329(__this, method) (( KeyCollection_t2660 * (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_get_Keys_m16329_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C" ValueCollection_t2664 * Dictionary_2_get_Values_m16331_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m16331(__this, method) (( ValueCollection_t2664 * (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_get_Values_m16331_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m16333_gshared (Dictionary_2_t1261 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m16333(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1261 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16333_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m16335_gshared (Dictionary_2_t1261 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m16335(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t1261 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16335_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m16337_gshared (Dictionary_2_t1261 * __this, KeyValuePair_2_t2658  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m16337(__this, ___pair, method) (( bool (*) (Dictionary_2_t1261 *, KeyValuePair_2_t2658 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16337_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C" Enumerator_t2662  Dictionary_2_GetEnumerator_m16339_gshared (Dictionary_2_t1261 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m16339(__this, method) (( Enumerator_t2662  (*) (Dictionary_2_t1261 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16339_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m16341_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m16341(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16341_gshared)(__this /* static, unused */, ___key, ___value, method)
