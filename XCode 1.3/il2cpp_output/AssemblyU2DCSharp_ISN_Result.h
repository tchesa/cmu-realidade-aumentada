﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ISN_Error
struct ISN_Error_t85;

#include "mscorlib_System_Object.h"

// ISN_Result
struct  ISN_Result_t114  : public Object_t
{
	// ISN_Error ISN_Result::error
	ISN_Error_t85 * ___error_0;
	// System.Boolean ISN_Result::_IsSucceeded
	bool ____IsSucceeded_1;
};
