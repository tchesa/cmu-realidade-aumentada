﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ComponentTracker
struct ComponentTracker_t67;
// System.Collections.Generic.Dictionary`2<System.String,ComponentTrack>
struct Dictionary_2_t68;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// ComponentTracker
struct  ComponentTracker_t67  : public MonoBehaviour_t18
{
	// System.Collections.Generic.Dictionary`2<System.String,ComponentTrack> ComponentTracker::dict
	Dictionary_2_t68 * ___dict_2;
};
struct ComponentTracker_t67_StaticFields{
	// ComponentTracker ComponentTracker::instance
	ComponentTracker_t67 * ___instance_1;
};
