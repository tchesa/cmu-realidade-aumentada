﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t3279;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void DefaultComparer__ctor_m26429_gshared (DefaultComparer_t3279 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m26429(__this, method) (( void (*) (DefaultComparer_t3279 *, const MethodInfo*))DefaultComparer__ctor_m26429_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26430_gshared (DefaultComparer_t3279 * __this, TargetSearchResult_t1050  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m26430(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3279 *, TargetSearchResult_t1050 , const MethodInfo*))DefaultComparer_GetHashCode_m26430_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26431_gshared (DefaultComparer_t3279 * __this, TargetSearchResult_t1050  ___x, TargetSearchResult_t1050  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m26431(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3279 *, TargetSearchResult_t1050 , TargetSearchResult_t1050 , const MethodInfo*))DefaultComparer_Equals_m26431_gshared)(__this, ___x, ___y, method)
