﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__42MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m26780(__this, ___dictionary, method) (( void (*) (Enumerator_t3309 *, Dictionary_2_t1066 *, const MethodInfo*))Enumerator__ctor_m26677_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26781(__this, method) (( Object_t * (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26678_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m26782(__this, method) (( void (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m26679_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26783(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26680_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26784(__this, method) (( Object_t * (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26681_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26785(__this, method) (( Object_t * (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26682_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::MoveNext()
#define Enumerator_MoveNext_m26786(__this, method) (( bool (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_MoveNext_m26683_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_Current()
#define Enumerator_get_Current_m26787(__this, method) (( KeyValuePair_2_t3306  (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_get_Current_m26684_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m26788(__this, method) (( String_t* (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_get_CurrentKey_m26685_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m26789(__this, method) (( ProfileData_t1064  (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_get_CurrentValue_m26686_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::Reset()
#define Enumerator_Reset_m26790(__this, method) (( void (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_Reset_m26687_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::VerifyState()
#define Enumerator_VerifyState_m26791(__this, method) (( void (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_VerifyState_m26688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m26792(__this, method) (( void (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_VerifyCurrent_m26689_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::Dispose()
#define Enumerator_Dispose_m26793(__this, method) (( void (*) (Enumerator_t3309 *, const MethodInfo*))Enumerator_Dispose_m26690_gshared)(__this, method)
