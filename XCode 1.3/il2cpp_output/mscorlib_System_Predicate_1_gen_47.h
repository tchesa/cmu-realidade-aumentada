﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t431;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Predicate`1<Vuforia.ReconstructionAbstractBehaviour>
struct  Predicate_1_t3144  : public MulticastDelegate_t10
{
};
