﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t27;
// DisconnectButton
struct DisconnectButton_t193;
// ConnectionButton
struct ConnectionButton_t192;
// ClickManager
struct ClickManager_t195;
// PTPGameController
struct PTPGameController_t197;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t198;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// PTPGameController
struct  PTPGameController_t197  : public MonoBehaviour_t18
{
	// UnityEngine.GameObject PTPGameController::pref
	GameObject_t27 * ___pref_1;
	// DisconnectButton PTPGameController::d
	DisconnectButton_t193 * ___d_2;
	// ConnectionButton PTPGameController::b
	ConnectionButton_t192 * ___b_3;
	// ClickManager PTPGameController::m
	ClickManager_t195 * ___m_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PTPGameController::spheres
	List_1_t198 * ___spheres_6;
};
struct PTPGameController_t197_StaticFields{
	// PTPGameController PTPGameController::instance
	PTPGameController_t197 * ___instance_5;
};
