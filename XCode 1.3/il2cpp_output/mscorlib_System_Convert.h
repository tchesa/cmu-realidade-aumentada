﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Type[]
struct TypeU5BU5D_t1218;

#include "mscorlib_System_Object.h"

// System.Convert
struct  Convert_t494  : public Object_t
{
};
struct Convert_t494_StaticFields{
	// System.Object System.Convert::DBNull
	Object_t * ___DBNull_0;
	// System.Type[] System.Convert::conversionTable
	TypeU5BU5D_t1218* ___conversionTable_1;
};
