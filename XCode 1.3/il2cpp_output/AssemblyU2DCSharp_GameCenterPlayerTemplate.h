﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t33;

#include "mscorlib_System_Object.h"

// GameCenterPlayerTemplate
struct  GameCenterPlayerTemplate_t107  : public Object_t
{
	// System.String GameCenterPlayerTemplate::_playerId
	String_t* ____playerId_0;
	// System.String GameCenterPlayerTemplate::_displayName
	String_t* ____displayName_1;
	// System.String GameCenterPlayerTemplate::_alias
	String_t* ____alias_2;
	// UnityEngine.Texture2D GameCenterPlayerTemplate::_avatar
	Texture2D_t33 * ____avatar_3;
};
