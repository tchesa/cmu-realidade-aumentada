﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_7MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m17907(__this, ___dictionary, method) (( void (*) (KeyCollection_t2749 *, Dictionary_2_t177 *, const MethodInfo*))KeyCollection__ctor_m16125_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17908(__this, ___item, method) (( void (*) (KeyCollection_t2749 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16126_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17909(__this, method) (( void (*) (KeyCollection_t2749 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16127_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17910(__this, ___item, method) (( bool (*) (KeyCollection_t2749 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16128_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17911(__this, ___item, method) (( bool (*) (KeyCollection_t2749 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16129_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17912(__this, method) (( Object_t* (*) (KeyCollection_t2749 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16130_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m17913(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2749 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m16131_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17914(__this, method) (( Object_t * (*) (KeyCollection_t2749 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16132_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17915(__this, method) (( bool (*) (KeyCollection_t2749 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16133_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17916(__this, method) (( bool (*) (KeyCollection_t2749 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16134_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m17917(__this, method) (( Object_t * (*) (KeyCollection_t2749 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m16135_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m17918(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2749 *, StringU5BU5D_t260*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m16136_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::GetEnumerator()
#define KeyCollection_GetEnumerator_m17919(__this, method) (( Enumerator_t3641  (*) (KeyCollection_t2749 *, const MethodInfo*))KeyCollection_GetEnumerator_m16137_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iAdBanner>::get_Count()
#define KeyCollection_get_Count_m17920(__this, method) (( int32_t (*) (KeyCollection_t2749 *, const MethodInfo*))KeyCollection_get_Count_m16138_gshared)(__this, method)
