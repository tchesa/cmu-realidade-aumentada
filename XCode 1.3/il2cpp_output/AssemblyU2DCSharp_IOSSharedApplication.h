﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<ISN_CheckUrlResult>
struct Action_1_t155;

#include "AssemblyU2DCSharp_ISN_Singleton_1_gen_2.h"

// IOSSharedApplication
struct  IOSSharedApplication_t153  : public ISN_Singleton_1_t154
{
};
struct IOSSharedApplication_t153_StaticFields{
	// System.Action`1<ISN_CheckUrlResult> IOSSharedApplication::OnUrlCheckResultAction
	Action_1_t155 * ___OnUrlCheckResultAction_7;
	// System.Action`1<ISN_CheckUrlResult> IOSSharedApplication::<>f__am$cache1
	Action_1_t155 * ___U3CU3Ef__amU24cache1_8;
};
