﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102.h"

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29425_gshared (InternalEnumerator_1_t3512 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29425(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3512 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29425_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29426_gshared (InternalEnumerator_1_t3512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29426(__this, method) (( void (*) (InternalEnumerator_1_t3512 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29426_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_gshared (InternalEnumerator_1_t3512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3512 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29428_gshared (InternalEnumerator_1_t3512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29428(__this, method) (( void (*) (InternalEnumerator_1_t3512 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29428_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29429_gshared (InternalEnumerator_1_t3512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29429(__this, method) (( bool (*) (InternalEnumerator_1_t3512 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29429_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C" int64_t InternalEnumerator_1_get_Current_m29430_gshared (InternalEnumerator_1_t3512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29430(__this, method) (( int64_t (*) (InternalEnumerator_1_t3512 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29430_gshared)(__this, method)
