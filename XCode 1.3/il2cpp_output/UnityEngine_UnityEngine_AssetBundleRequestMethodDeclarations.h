﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t1341;
// UnityEngine.Object
struct Object_t53;
struct Object_t53_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m7369 (AssetBundleRequest_t1341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t53 * AssetBundleRequest_get_asset_m7370 (AssetBundleRequest_t1341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
