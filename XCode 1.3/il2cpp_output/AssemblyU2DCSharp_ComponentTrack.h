﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// ComponentTrack
struct  ComponentTrack_t66  : public MonoBehaviour_t18
{
	// System.String ComponentTrack::identifier
	String_t* ___identifier_1;
};
