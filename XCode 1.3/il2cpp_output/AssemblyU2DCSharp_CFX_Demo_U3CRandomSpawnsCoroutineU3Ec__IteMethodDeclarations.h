﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA
struct U3CRandomSpawnsCoroutineU3Ec__IteratorA_t318;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::.ctor()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__IteratorA__ctor_m1742 (U3CRandomSpawnsCoroutineU3Ec__IteratorA_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CRandomSpawnsCoroutineU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1743 (U3CRandomSpawnsCoroutineU3Ec__IteratorA_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CRandomSpawnsCoroutineU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1744 (U3CRandomSpawnsCoroutineU3Ec__IteratorA_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::MoveNext()
extern "C" bool U3CRandomSpawnsCoroutineU3Ec__IteratorA_MoveNext_m1745 (U3CRandomSpawnsCoroutineU3Ec__IteratorA_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::Dispose()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__IteratorA_Dispose_m1746 (U3CRandomSpawnsCoroutineU3Ec__IteratorA_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo/<RandomSpawnsCoroutine>c__IteratorA::Reset()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__IteratorA_Reset_m1747 (U3CRandomSpawnsCoroutineU3Ec__IteratorA_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
