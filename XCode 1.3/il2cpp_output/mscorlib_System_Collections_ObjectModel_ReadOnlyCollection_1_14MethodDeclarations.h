﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m19156(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2829 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15555_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19157(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2829 *, TrailTrigger_t341 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15556_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19158(__this, method) (( void (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15557_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19159(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2829 *, int32_t, TrailTrigger_t341 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15558_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19160(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2829 *, TrailTrigger_t341 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15559_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19161(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2829 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15560_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19162(__this, ___index, method) (( TrailTrigger_t341 * (*) (ReadOnlyCollection_1_t2829 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15561_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19163(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2829 *, int32_t, TrailTrigger_t341 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15562_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19164(__this, method) (( bool (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15563_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19165(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2829 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15564_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19166(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15565_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m19167(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2829 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15566_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m19168(__this, method) (( void (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15567_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m19169(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2829 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15568_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19170(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2829 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15569_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m19171(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2829 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15570_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m19172(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2829 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15571_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19173(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2829 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15572_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19174(__this, method) (( bool (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15573_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19175(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15574_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19176(__this, method) (( bool (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15575_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19177(__this, method) (( bool (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15576_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m19178(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2829 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15577_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m19179(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2829 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15578_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::Contains(T)
#define ReadOnlyCollection_1_Contains_m19180(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2829 *, TrailTrigger_t341 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15579_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m19181(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2829 *, TrailTriggerU5BU5D_t348*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15580_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m19182(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m19183(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2829 *, TrailTrigger_t341 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15582_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::get_Count()
#define ReadOnlyCollection_1_get_Count_m19184(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2829 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15583_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ARAnimation/TrailTrigger>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m19185(__this, ___index, method) (( TrailTrigger_t341 * (*) (ReadOnlyCollection_1_t2829 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15584_gshared)(__this, ___index, method)
