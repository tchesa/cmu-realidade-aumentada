﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t3514;
// System.Object[]
struct ObjectU5BU5D_t34;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t554;
// System.Exception
struct Exception_t40;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m29435_gshared (ArrayReadOnlyList_1_t3514 * __this, ObjectU5BU5D_t34* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m29435(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t3514 *, ObjectU5BU5D_t34*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m29435_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29436_gshared (ArrayReadOnlyList_1_t3514 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29436(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3514 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29436_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m29437_gshared (ArrayReadOnlyList_1_t3514 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m29437(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3514 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m29437_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m29438_gshared (ArrayReadOnlyList_1_t3514 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m29438(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t3514 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m29438_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m29439_gshared (ArrayReadOnlyList_1_t3514 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m29439(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t3514 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m29439_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m29440_gshared (ArrayReadOnlyList_1_t3514 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m29440(__this, method) (( bool (*) (ArrayReadOnlyList_1_t3514 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m29440_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m29441_gshared (ArrayReadOnlyList_1_t3514 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m29441(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3514 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m29441_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m29442_gshared (ArrayReadOnlyList_1_t3514 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m29442(__this, method) (( void (*) (ArrayReadOnlyList_1_t3514 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m29442_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m29443_gshared (ArrayReadOnlyList_1_t3514 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m29443(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3514 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m29443_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m29444_gshared (ArrayReadOnlyList_1_t3514 * __this, ObjectU5BU5D_t34* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m29444(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3514 *, ObjectU5BU5D_t34*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m29444_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m29445_gshared (ArrayReadOnlyList_1_t3514 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m29445(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t3514 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m29445_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m29446_gshared (ArrayReadOnlyList_1_t3514 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m29446(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t3514 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m29446_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m29447_gshared (ArrayReadOnlyList_1_t3514 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m29447(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3514 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m29447_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m29448_gshared (ArrayReadOnlyList_1_t3514 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m29448(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3514 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m29448_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m29449_gshared (ArrayReadOnlyList_1_t3514 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m29449(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3514 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m29449_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t40 * ArrayReadOnlyList_1_ReadOnlyError_m29450_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m29450(__this /* static, unused */, method) (( Exception_t40 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m29450_gshared)(__this /* static, unused */, method)
