﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t3058;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_57.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22851_gshared (Enumerator_t3068 * __this, Dictionary_2_t3058 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22851(__this, ___host, method) (( void (*) (Enumerator_t3068 *, Dictionary_2_t3058 *, const MethodInfo*))Enumerator__ctor_m22851_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22852_gshared (Enumerator_t3068 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22852(__this, method) (( Object_t * (*) (Enumerator_t3068 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22852_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m22853_gshared (Enumerator_t3068 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m22853(__this, method) (( void (*) (Enumerator_t3068 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m22853_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m22854_gshared (Enumerator_t3068 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22854(__this, method) (( void (*) (Enumerator_t3068 *, const MethodInfo*))Enumerator_Dispose_m22854_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22855_gshared (Enumerator_t3068 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22855(__this, method) (( bool (*) (Enumerator_t3068 *, const MethodInfo*))Enumerator_MoveNext_m22855_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m22856_gshared (Enumerator_t3068 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22856(__this, method) (( Object_t * (*) (Enumerator_t3068 *, const MethodInfo*))Enumerator_get_Current_m22856_gshared)(__this, method)
