﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// SinoidalAnimation
struct  SinoidalAnimation_t388  : public MonoBehaviour_t18
{
	// System.Single SinoidalAnimation::range
	float ___range_1;
	// System.Single SinoidalAnimation::speed
	float ___speed_2;
	// UnityEngine.Vector3 SinoidalAnimation::initialPosition
	Vector3_t6  ___initialPosition_3;
};
