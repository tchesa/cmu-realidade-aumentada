﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,GameCenterPlayerTemplate,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m17002(__this, ___object, ___method, method) (( void (*) (Transform_1_t2696 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m16183_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,GameCenterPlayerTemplate,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m17003(__this, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Transform_1_t2696 *, String_t*, GameCenterPlayerTemplate_t107 *, const MethodInfo*))Transform_1_Invoke_m16184_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,GameCenterPlayerTemplate,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m17004(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2696 *, String_t*, GameCenterPlayerTemplate_t107 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m16185_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,GameCenterPlayerTemplate,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m17005(__this, ___result, method) (( DictionaryEntry_t58  (*) (Transform_1_t2696 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m16186_gshared)(__this, ___result, method)
