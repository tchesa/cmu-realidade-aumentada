﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_38MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor()
#define Dictionary_2__ctor_m6731(__this, method) (( void (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2__ctor_m8129_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m25319(__this, ___comparer, method) (( void (*) (Dictionary_2_t1037 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16251_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Int32)
#define Dictionary_2__ctor_m25320(__this, ___capacity, method) (( void (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16253_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m25321(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1037 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m16255_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m25322(__this, method) (( Object_t * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16257_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25323(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16259_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25324(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1037 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16261_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m25325(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1037 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16263_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m25326(__this, ___key, method) (( bool (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16265_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m25327(__this, ___key, method) (( void (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16267_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25328(__this, method) (( bool (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16269_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25329(__this, method) (( Object_t * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16271_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25330(__this, method) (( bool (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16273_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25331(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1037 *, KeyValuePair_2_t3213 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16275_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25332(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1037 *, KeyValuePair_2_t3213 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16277_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25333(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1037 *, KeyValuePair_2U5BU5D_t3717*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16279_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25334(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1037 *, KeyValuePair_2_t3213 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16281_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25335(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1037 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16283_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25336(__this, method) (( Object_t * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16285_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25337(__this, method) (( Object_t* (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16287_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25338(__this, method) (( Object_t * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16289_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Count()
#define Dictionary_2_get_Count_m25339(__this, method) (( int32_t (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_get_Count_m16291_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Item(TKey)
#define Dictionary_2_get_Item_m25340(__this, ___key, method) (( SurfaceAbstractBehaviour_t436 * (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m16293_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m25341(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1037 *, int32_t, SurfaceAbstractBehaviour_t436 *, const MethodInfo*))Dictionary_2_set_Item_m16295_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m25342(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1037 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16297_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m25343(__this, ___size, method) (( void (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16299_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m25344(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1037 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16301_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m25345(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3213  (*) (Object_t * /* static, unused */, int32_t, SurfaceAbstractBehaviour_t436 *, const MethodInfo*))Dictionary_2_make_pair_m16303_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m25346(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, SurfaceAbstractBehaviour_t436 *, const MethodInfo*))Dictionary_2_pick_key_m16305_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m25347(__this /* static, unused */, ___key, ___value, method) (( SurfaceAbstractBehaviour_t436 * (*) (Object_t * /* static, unused */, int32_t, SurfaceAbstractBehaviour_t436 *, const MethodInfo*))Dictionary_2_pick_value_m16307_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m25348(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1037 *, KeyValuePair_2U5BU5D_t3717*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16309_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Resize()
#define Dictionary_2_Resize_m25349(__this, method) (( void (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_Resize_m16311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Add(TKey,TValue)
#define Dictionary_2_Add_m25350(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1037 *, int32_t, SurfaceAbstractBehaviour_t436 *, const MethodInfo*))Dictionary_2_Add_m16313_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Clear()
#define Dictionary_2_Clear_m25351(__this, method) (( void (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_Clear_m16315_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m25352(__this, ___key, method) (( bool (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m16317_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m25353(__this, ___value, method) (( bool (*) (Dictionary_2_t1037 *, SurfaceAbstractBehaviour_t436 *, const MethodInfo*))Dictionary_2_ContainsValue_m16319_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m25354(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1037 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m16321_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m25355(__this, ___sender, method) (( void (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16323_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Remove(TKey)
#define Dictionary_2_Remove_m25356(__this, ___key, method) (( bool (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m16325_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m25357(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1037 *, int32_t, SurfaceAbstractBehaviour_t436 **, const MethodInfo*))Dictionary_2_TryGetValue_m16327_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Keys()
#define Dictionary_2_get_Keys_m25358(__this, method) (( KeyCollection_t3214 * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_get_Keys_m16329_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Values()
#define Dictionary_2_get_Values_m6689(__this, method) (( ValueCollection_t1185 * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_get_Values_m16331_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m25359(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16333_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m25360(__this, ___value, method) (( SurfaceAbstractBehaviour_t436 * (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16335_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m25361(__this, ___pair, method) (( bool (*) (Dictionary_2_t1037 *, KeyValuePair_2_t3213 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16337_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m25362(__this, method) (( Enumerator_t3215  (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16339_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m25363(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, int32_t, SurfaceAbstractBehaviour_t436 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16341_gshared)(__this /* static, unused */, ___key, ___value, method)
