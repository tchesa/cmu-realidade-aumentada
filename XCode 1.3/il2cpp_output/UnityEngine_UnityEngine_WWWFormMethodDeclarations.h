﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WWWForm
struct WWWForm_t293;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1442;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.Collections.Hashtable
struct Hashtable_t19;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.WWWForm::.ctor()
extern "C" void WWWForm__ctor_m2406 (WWWForm_t293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern "C" void WWWForm_AddField_m2407 (WWWForm_t293 * __this, String_t* ___fieldName, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String,System.Text.Encoding)
extern "C" void WWWForm_AddField_m7974 (WWWForm_t293 * __this, String_t* ___fieldName, String_t* ___value, Encoding_t1442 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String)
extern "C" void WWWForm_AddBinaryData_m2671 (WWWForm_t293 * __this, String_t* ___fieldName, ByteU5BU5D_t119* ___contents, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String,System.String)
extern "C" void WWWForm_AddBinaryData_m7975 (WWWForm_t293 * __this, String_t* ___fieldName, ByteU5BU5D_t119* ___contents, String_t* ___fileName, String_t* ___mimeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable UnityEngine.WWWForm::get_headers()
extern "C" Hashtable_t19 * WWWForm_get_headers_m2517 (WWWForm_t293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWForm::get_data()
extern "C" ByteU5BU5D_t119* WWWForm_get_data_m7976 (WWWForm_t293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
