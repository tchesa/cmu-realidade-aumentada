﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Delegate
struct Delegate_t506;

#include "mscorlib_System_Object.h"

// System.DelegateSerializationHolder
struct  DelegateSerializationHolder_t2427  : public Object_t
{
	// System.Delegate System.DelegateSerializationHolder::_delegate
	Delegate_t506 * ____delegate_0;
};
