﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSInAppPurchaseManager
struct IOSInAppPurchaseManager_t127;
// System.String
struct String_t;
// IOSProductTemplate
struct IOSProductTemplate_t135;
// IOSStoreProductView
struct IOSStoreProductView_t134;
// System.Collections.Generic.List`1<IOSProductTemplate>
struct List_1_t131;
// IOSStoreKitResponse
struct IOSStoreKitResponse_t137;
// ISN_Result
struct ISN_Result_t114;
// IOSStoreKitVerificationResponse
struct IOSStoreKitVerificationResponse_t138;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_IOSTransactionErrorCode.h"

// System.Void IOSInAppPurchaseManager::.ctor()
extern "C" void IOSInAppPurchaseManager__ctor_m719 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::.cctor()
extern "C" void IOSInAppPurchaseManager__cctor_m720 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSInAppPurchaseManager IOSInAppPurchaseManager::get_instance()
extern "C" IOSInAppPurchaseManager_t127 * IOSInAppPurchaseManager_get_instance_m721 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::loadStore()
extern "C" void IOSInAppPurchaseManager_loadStore_m722 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::RequestInAppSettingState()
extern "C" void IOSInAppPurchaseManager_RequestInAppSettingState_m723 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::buyProduct(System.String)
extern "C" void IOSInAppPurchaseManager_buyProduct_m724 (IOSInAppPurchaseManager_t127 * __this, String_t* ___productId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::addProductId(System.String)
extern "C" void IOSInAppPurchaseManager_addProductId_m725 (IOSInAppPurchaseManager_t127 * __this, String_t* ___productId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSProductTemplate IOSInAppPurchaseManager::GetProductById(System.String)
extern "C" IOSProductTemplate_t135 * IOSInAppPurchaseManager_GetProductById_m726 (IOSInAppPurchaseManager_t127 * __this, String_t* ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::restorePurchases()
extern "C" void IOSInAppPurchaseManager_restorePurchases_m727 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::verifyLastPurchase(System.String)
extern "C" void IOSInAppPurchaseManager_verifyLastPurchase_m728 (IOSInAppPurchaseManager_t127 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::RegisterProductView(IOSStoreProductView)
extern "C" void IOSInAppPurchaseManager_RegisterProductView_m729 (IOSInAppPurchaseManager_t127 * __this, IOSStoreProductView_t134 * ___view, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<IOSProductTemplate> IOSInAppPurchaseManager::get_products()
extern "C" List_1_t131 * IOSInAppPurchaseManager_get_products_m730 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSInAppPurchaseManager::get_IsStoreLoaded()
extern "C" bool IOSInAppPurchaseManager_get_IsStoreLoaded_m731 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSInAppPurchaseManager::get_IsInAppPurchasesEnabled()
extern "C" bool IOSInAppPurchaseManager_get_IsInAppPurchasesEnabled_m732 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSInAppPurchaseManager::get_IsWaitingLoadResult()
extern "C" bool IOSInAppPurchaseManager_get_IsWaitingLoadResult_m733 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSInAppPurchaseManager::get_nextId()
extern "C" int32_t IOSInAppPurchaseManager_get_nextId_m734 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onStoreKitStart(System.String)
extern "C" void IOSInAppPurchaseManager_onStoreKitStart_m735 (IOSInAppPurchaseManager_t127 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::OnStoreKitInitFailed(System.String)
extern "C" void IOSInAppPurchaseManager_OnStoreKitInitFailed_m736 (IOSInAppPurchaseManager_t127 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onStoreDataReceived(System.String)
extern "C" void IOSInAppPurchaseManager_onStoreDataReceived_m737 (IOSInAppPurchaseManager_t127 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onProductBought(System.String)
extern "C" void IOSInAppPurchaseManager_onProductBought_m738 (IOSInAppPurchaseManager_t127 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onProductStateDeferred(System.String)
extern "C" void IOSInAppPurchaseManager_onProductStateDeferred_m739 (IOSInAppPurchaseManager_t127 * __this, String_t* ___productIdentifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onTransactionFailed(System.String)
extern "C" void IOSInAppPurchaseManager_onTransactionFailed_m740 (IOSInAppPurchaseManager_t127 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onVerificationResult(System.String)
extern "C" void IOSInAppPurchaseManager_onVerificationResult_m741 (IOSInAppPurchaseManager_t127 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onRestoreTransactionFailed(System.String)
extern "C" void IOSInAppPurchaseManager_onRestoreTransactionFailed_m742 (IOSInAppPurchaseManager_t127 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onRestoreTransactionComplete(System.String)
extern "C" void IOSInAppPurchaseManager_onRestoreTransactionComplete_m743 (IOSInAppPurchaseManager_t127 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::OnProductViewLoaded(System.String)
extern "C" void IOSInAppPurchaseManager_OnProductViewLoaded_m744 (IOSInAppPurchaseManager_t127 * __this, String_t* ___viewId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::OnProductViewLoadedFailed(System.String)
extern "C" void IOSInAppPurchaseManager_OnProductViewLoadedFailed_m745 (IOSInAppPurchaseManager_t127 * __this, String_t* ___viewId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::OnProductViewDismissed(System.String)
extern "C" void IOSInAppPurchaseManager_OnProductViewDismissed_m746 (IOSInAppPurchaseManager_t127 * __this, String_t* ___viewId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::FireSuccessInitEvent()
extern "C" void IOSInAppPurchaseManager_FireSuccessInitEvent_m747 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::FireRestoreCompleteEvent()
extern "C" void IOSInAppPurchaseManager_FireRestoreCompleteEvent_m748 (IOSInAppPurchaseManager_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::FireProductBoughtEvent(System.String,System.String,System.String,System.Boolean)
extern "C" void IOSInAppPurchaseManager_FireProductBoughtEvent_m749 (IOSInAppPurchaseManager_t127 * __this, String_t* ___productIdentifier, String_t* ___receipt, String_t* ___transactionIdentifier, bool ___IsRestored, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::SendTransactionFailEvent(System.String,System.String,IOSTransactionErrorCode)
extern "C" void IOSInAppPurchaseManager_SendTransactionFailEvent_m750 (IOSInAppPurchaseManager_t127 * __this, String_t* ___productIdentifier, String_t* ___errorDescribtion, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnTransactionComplete>m__14(IOSStoreKitResponse)
extern "C" void IOSInAppPurchaseManager_U3COnTransactionCompleteU3Em__14_m751 (Object_t * __this /* static, unused */, IOSStoreKitResponse_t137 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnRestoreComplete>m__15(ISN_Result)
extern "C" void IOSInAppPurchaseManager_U3COnRestoreCompleteU3Em__15_m752 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnStoreKitInitComplete>m__16(ISN_Result)
extern "C" void IOSInAppPurchaseManager_U3COnStoreKitInitCompleteU3Em__16_m753 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnPurchasesStateSettingsLoaded>m__17(System.Boolean)
extern "C" void IOSInAppPurchaseManager_U3COnPurchasesStateSettingsLoadedU3Em__17_m754 (Object_t * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnVerificationComplete>m__18(IOSStoreKitVerificationResponse)
extern "C" void IOSInAppPurchaseManager_U3COnVerificationCompleteU3Em__18_m755 (Object_t * __this /* static, unused */, IOSStoreKitVerificationResponse_t138 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
