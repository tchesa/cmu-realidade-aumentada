﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterManager
struct GameCenterManager_t98;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.String[]
struct StringU5BU5D_t260;
// GCLeaderboard
struct GCLeaderboard_t121;
// GameCenterPlayerTemplate
struct GameCenterPlayerTemplate_t107;
// System.Collections.Generic.List`1<AchievementTemplate>
struct List_1_t104;
// System.Collections.Generic.Dictionary`2<System.String,GameCenterPlayerTemplate>
struct Dictionary_2_t106;
// UnionAssets.FLE.EventDispatcherBase
struct EventDispatcherBase_t72;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;
// AchievementTemplate
struct AchievementTemplate_t115;
// ISN_Result
struct ISN_Result_t114;
// ISN_PlayerScoreLoadedResult
struct ISN_PlayerScoreLoadedResult_t116;
// ISN_AchievementProgressResult
struct ISN_AchievementProgressResult_t113;
// ISN_UserInfoLoadResult
struct ISN_UserInfoLoadResult_t120;
// ISN_PlayerSignatureResult
struct ISN_PlayerSignatureResult_t118;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GCBoardTimeSpan.h"
#include "AssemblyU2DCSharp_GCCollectionType.h"

// System.Void GameCenterManager::.ctor()
extern "C" void GameCenterManager__ctor_m558 (GameCenterManager_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::.cctor()
extern "C" void GameCenterManager__cctor_m559 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_initGameCenter()
extern "C" void GameCenterManager__initGameCenter_m560 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_showLeaderboard(System.String,System.Int32)
extern "C" void GameCenterManager__showLeaderboard_m561 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, int32_t ___timeSpan, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_reportScore(System.String,System.String)
extern "C" void GameCenterManager__reportScore_m562 (Object_t * __this /* static, unused */, String_t* ___score, String_t* ___leaderboardId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_showLeaderboards()
extern "C" void GameCenterManager__showLeaderboards_m563 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_getLeaderboardScore(System.String,System.Int32,System.Int32)
extern "C" void GameCenterManager__getLeaderboardScore_m564 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, int32_t ___timeSpan, int32_t ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_loadLeaderboardScore(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void GameCenterManager__loadLeaderboardScore_m565 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, int32_t ___timeSpan, int32_t ___collection, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_showAchievements()
extern "C" void GameCenterManager__showAchievements_m566 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_resetAchievements()
extern "C" void GameCenterManager__resetAchievements_m567 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_submitAchievement(System.Single,System.String,System.Boolean)
extern "C" void GameCenterManager__submitAchievement_m568 (Object_t * __this /* static, unused */, float ___percent, String_t* ___achievementId, bool ___isCompleteNotification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_loadGCUserData(System.String)
extern "C" void GameCenterManager__loadGCUserData_m569 (Object_t * __this /* static, unused */, String_t* ___uid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_ISN_issueLeaderboardChallenge(System.String,System.String,System.String)
extern "C" void GameCenterManager__ISN_issueLeaderboardChallenge_m570 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, String_t* ___message, String_t* ___playerIds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_ISN_issueLeaderboardChallengeWithFriendsPicker(System.String,System.String)
extern "C" void GameCenterManager__ISN_issueLeaderboardChallengeWithFriendsPicker_m571 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_ISN_issueAchievementChallenge(System.String,System.String,System.String)
extern "C" void GameCenterManager__ISN_issueAchievementChallenge_m572 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, String_t* ___message, String_t* ___playerIds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_ISN_issueAchievementChallengeWithFriendsPicker(System.String,System.String)
extern "C" void GameCenterManager__ISN_issueAchievementChallengeWithFriendsPicker_m573 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_gcRetrieveFriends()
extern "C" void GameCenterManager__gcRetrieveFriends_m574 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::_ISN_getSignature()
extern "C" void GameCenterManager__ISN_getSignature_m575 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::init()
extern "C" void GameCenterManager_init_m576 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::Awake()
extern "C" void GameCenterManager_Awake_m577 (GameCenterManager_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::RetrievePlayerSignature()
extern "C" void GameCenterManager_RetrievePlayerSignature_m578 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::RegisterAchievement(System.String)
extern "C" void GameCenterManager_RegisterAchievement_m579 (Object_t * __this /* static, unused */, String_t* ___achievementId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowLeaderboard(System.String)
extern "C" void GameCenterManager_ShowLeaderboard_m580 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowLeaderboard(System.String,GCBoardTimeSpan)
extern "C" void GameCenterManager_ShowLeaderboard_m581 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, int32_t ___timeSpan, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowLeaderboards()
extern "C" void GameCenterManager_ShowLeaderboards_m582 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ReportScore(System.Int64,System.String)
extern "C" void GameCenterManager_ReportScore_m583 (Object_t * __this /* static, unused */, int64_t ___score, String_t* ___leaderboardId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ReportScore(System.Double,System.String)
extern "C" void GameCenterManager_ReportScore_m584 (Object_t * __this /* static, unused */, double ___score, String_t* ___leaderboardId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::RetrieveFriends()
extern "C" void GameCenterManager_RetrieveFriends_m585 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadCurrentPlayerScore(System.String,GCBoardTimeSpan,GCCollectionType)
extern "C" void GameCenterManager_LoadCurrentPlayerScore_m586 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, int32_t ___timeSpan, int32_t ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameCenterManager::LoadCurrentPlayerScoreLocal(System.String,GCBoardTimeSpan,GCCollectionType)
extern "C" Object_t * GameCenterManager_LoadCurrentPlayerScoreLocal_m587 (GameCenterManager_t98 * __this, String_t* ___leaderboardId, int32_t ___timeSpan, int32_t ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadScore(System.String,System.Int32,System.Int32,GCBoardTimeSpan,GCCollectionType)
extern "C" void GameCenterManager_LoadScore_m588 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, int32_t ___from, int32_t ___to, int32_t ___timeSpan, int32_t ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueLeaderboardChallenge(System.String,System.String,System.String)
extern "C" void GameCenterManager_IssueLeaderboardChallenge_m589 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, String_t* ___message, String_t* ___playerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueLeaderboardChallenge(System.String,System.String,System.String[])
extern "C" void GameCenterManager_IssueLeaderboardChallenge_m590 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, String_t* ___message, StringU5BU5D_t260* ___playerIds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueLeaderboardChallenge(System.String,System.String)
extern "C" void GameCenterManager_IssueLeaderboardChallenge_m591 (Object_t * __this /* static, unused */, String_t* ___leaderboardId, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueAchievementChallenge(System.String,System.String,System.String)
extern "C" void GameCenterManager_IssueAchievementChallenge_m592 (Object_t * __this /* static, unused */, String_t* ___achievementId, String_t* ___message, String_t* ___playerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueAchievementChallenge(System.String,System.String,System.String[])
extern "C" void GameCenterManager_IssueAchievementChallenge_m593 (Object_t * __this /* static, unused */, String_t* ___achievementId, String_t* ___message, StringU5BU5D_t260* ___playerIds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueAchievementChallenge(System.String,System.String)
extern "C" void GameCenterManager_IssueAchievementChallenge_m594 (Object_t * __this /* static, unused */, String_t* ___achievementId, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowAchievements()
extern "C" void GameCenterManager_ShowAchievements_m595 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ResetAchievements()
extern "C" void GameCenterManager_ResetAchievements_m596 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SubmitAchievement(System.Single,System.String)
extern "C" void GameCenterManager_SubmitAchievement_m597 (Object_t * __this /* static, unused */, float ___percent, String_t* ___achievementId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SubmitAchievementNoCache(System.Single,System.String)
extern "C" void GameCenterManager_SubmitAchievementNoCache_m598 (Object_t * __this /* static, unused */, float ___percent, String_t* ___achievementId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SubmitAchievement(System.Single,System.String,System.Boolean)
extern "C" void GameCenterManager_SubmitAchievement_m599 (Object_t * __this /* static, unused */, float ___percent, String_t* ___achievementId, bool ___isCompleteNotification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadUsersData(System.String[])
extern "C" void GameCenterManager_LoadUsersData_m600 (Object_t * __this /* static, unused */, StringU5BU5D_t260* ___UIDs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameCenterManager::GetAchievementProgress(System.String)
extern "C" float GameCenterManager_GetAchievementProgress_m601 (Object_t * __this /* static, unused */, String_t* ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GCLeaderboard GameCenterManager::GetLeaderboard(System.String)
extern "C" GCLeaderboard_t121 * GameCenterManager_GetLeaderboard_m602 (Object_t * __this /* static, unused */, String_t* ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameCenterPlayerTemplate GameCenterManager::GetPlayerById(System.String)
extern "C" GameCenterPlayerTemplate_t107 * GameCenterManager_GetPlayerById_m603 (Object_t * __this /* static, unused */, String_t* ___playerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AchievementTemplate> GameCenterManager::get_Achievements()
extern "C" List_1_t104 * GameCenterManager_get_Achievements_m604 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,GameCenterPlayerTemplate> GameCenterManager::get_Players()
extern "C" Dictionary_2_t106 * GameCenterManager_get_Players_m605 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnionAssets.FLE.EventDispatcherBase GameCenterManager::get_Dispatcher()
extern "C" EventDispatcherBase_t72 * GameCenterManager_get_Dispatcher_m606 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameCenterPlayerTemplate GameCenterManager::get_Player()
extern "C" GameCenterPlayerTemplate_t107 * GameCenterManager_get_Player_m607 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterManager::get_IsInitialized()
extern "C" bool GameCenterManager_get_IsInitialized_m608 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterManager::get_IsPlayerAuthenticated()
extern "C" bool GameCenterManager_get_IsPlayerAuthenticated_m609 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterManager::get_IsAchievementsInfoLoaded()
extern "C" bool GameCenterManager_get_IsAchievementsInfoLoaded_m610 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GameCenterManager::get_FriendsList()
extern "C" List_1_t78 * GameCenterManager_get_FriendsList_m611 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onLeaderboardScoreFailed(System.String)
extern "C" void GameCenterManager_onLeaderboardScoreFailed_m612 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onLeaderboardScore(System.String)
extern "C" void GameCenterManager_onLeaderboardScore_m613 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onScoreSubmittedEvent(System.String)
extern "C" void GameCenterManager_onScoreSubmittedEvent_m614 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onScoreSubmittedFailed(System.String)
extern "C" void GameCenterManager_onScoreSubmittedFailed_m615 (GameCenterManager_t98 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onLeaderboardScoreListLoaded(System.String)
extern "C" void GameCenterManager_onLeaderboardScoreListLoaded_m616 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onLeaderboardScoreListLoadFailed(System.String)
extern "C" void GameCenterManager_onLeaderboardScoreListLoadFailed_m617 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementsReset(System.String)
extern "C" void GameCenterManager_onAchievementsReset_m618 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementsResetFailed(System.String)
extern "C" void GameCenterManager_onAchievementsResetFailed_m619 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementProgressChanged(System.String)
extern "C" void GameCenterManager_onAchievementProgressChanged_m620 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementsLoaded(System.String)
extern "C" void GameCenterManager_onAchievementsLoaded_m621 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementsLoadedFailed()
extern "C" void GameCenterManager_onAchievementsLoadedFailed_m622 (GameCenterManager_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAuthenticateLocalPlayer(System.String)
extern "C" void GameCenterManager_onAuthenticateLocalPlayer_m623 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAuthenticationFailed(System.String)
extern "C" void GameCenterManager_onAuthenticationFailed_m624 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onUserInfoLoaded(System.String)
extern "C" void GameCenterManager_onUserInfoLoaded_m625 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onUserInfoLoadFailed(System.String)
extern "C" void GameCenterManager_onUserInfoLoadFailed_m626 (GameCenterManager_t98 * __this, String_t* ___playerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnGameCenterViewDismissed(System.String)
extern "C" void GameCenterManager_OnGameCenterViewDismissed_m627 (GameCenterManager_t98 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onFriendListLoaded(System.String)
extern "C" void GameCenterManager_onFriendListLoaded_m628 (GameCenterManager_t98 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onFriendListFailedToLoad(System.String)
extern "C" void GameCenterManager_onFriendListFailedToLoad_m629 (GameCenterManager_t98 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::VerificationSignatureRetrieveFailed(System.String)
extern "C" void GameCenterManager_VerificationSignatureRetrieveFailed_m630 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::VerificationSignatureRetrieved(System.String)
extern "C" void GameCenterManager_VerificationSignatureRetrieved_m631 (GameCenterManager_t98 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::submitAchievement(AchievementTemplate)
extern "C" void GameCenterManager_submitAchievement_m632 (GameCenterManager_t98 * __this, AchievementTemplate_t115 * ___tpl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ResetStoredProgress()
extern "C" void GameCenterManager_ResetStoredProgress_m633 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SaveAchievementProgress(System.String,System.Single)
extern "C" void GameCenterManager_SaveAchievementProgress_m634 (Object_t * __this /* static, unused */, String_t* ___achievementId, float ___progress, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameCenterManager::GetStoredAchievementProgress(System.String)
extern "C" float GameCenterManager_GetStoredAchievementProgress_m635 (Object_t * __this /* static, unused */, String_t* ___achievementId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnAuthFinished>m__5(ISN_Result)
extern "C" void GameCenterManager_U3COnAuthFinishedU3Em__5_m636 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnScoreSubmitted>m__6(ISN_Result)
extern "C" void GameCenterManager_U3COnScoreSubmittedU3Em__6_m637 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnPlayerScoreLoaded>m__7(ISN_PlayerScoreLoadedResult)
extern "C" void GameCenterManager_U3COnPlayerScoreLoadedU3Em__7_m638 (Object_t * __this /* static, unused */, ISN_PlayerScoreLoadedResult_t116 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnScoresListLoaded>m__8(ISN_Result)
extern "C" void GameCenterManager_U3COnScoresListLoadedU3Em__8_m639 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnAchievementsReset>m__9(ISN_Result)
extern "C" void GameCenterManager_U3COnAchievementsResetU3Em__9_m640 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnAchievementsLoaded>m__A(ISN_Result)
extern "C" void GameCenterManager_U3COnAchievementsLoadedU3Em__A_m641 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnAchievementsProgress>m__B(ISN_AchievementProgressResult)
extern "C" void GameCenterManager_U3COnAchievementsProgressU3Em__B_m642 (Object_t * __this /* static, unused */, ISN_AchievementProgressResult_t113 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnGameCenterViewDismissedAction>m__C()
extern "C" void GameCenterManager_U3COnGameCenterViewDismissedActionU3Em__C_m643 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnFriendsListLoaded>m__D(ISN_Result)
extern "C" void GameCenterManager_U3COnFriendsListLoadedU3Em__D_m644 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnUserInfoLoaded>m__E(ISN_UserInfoLoadResult)
extern "C" void GameCenterManager_U3COnUserInfoLoadedU3Em__E_m645 (Object_t * __this /* static, unused */, ISN_UserInfoLoadResult_t120 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnPlayerSignatureRetrieveResult>m__F(ISN_PlayerSignatureResult)
extern "C" void GameCenterManager_U3COnPlayerSignatureRetrieveResultU3Em__F_m646 (Object_t * __this /* static, unused */, ISN_PlayerSignatureResult_t118 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
