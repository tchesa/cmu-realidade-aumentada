﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Object>
struct GenericEqualityComparer_1_t3526;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Object>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m29501_gshared (GenericEqualityComparer_1_t3526 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m29501(__this, method) (( void (*) (GenericEqualityComparer_1_t3526 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m29501_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Object>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m29502_gshared (GenericEqualityComparer_1_t3526 * __this, Object_t * ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m29502(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3526 *, Object_t *, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m29502_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Object>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m29503_gshared (GenericEqualityComparer_1_t3526 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m29503(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3526 *, Object_t *, Object_t *, const MethodInfo*))GenericEqualityComparer_1_Equals_m29503_gshared)(__this, ___x, ___y, method)
