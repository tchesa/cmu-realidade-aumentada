﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t45;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t311;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t320;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// CFX_Demo
struct  CFX_Demo_t319  : public MonoBehaviour_t18
{
	// System.Boolean CFX_Demo::orderedSpawns
	bool ___orderedSpawns_1;
	// System.Single CFX_Demo::step
	float ___step_2;
	// System.Single CFX_Demo::range
	float ___range_3;
	// System.Single CFX_Demo::order
	float ___order_4;
	// UnityEngine.Material CFX_Demo::groundMat
	Material_t45 * ___groundMat_5;
	// UnityEngine.Material CFX_Demo::waterMat
	Material_t45 * ___waterMat_6;
	// UnityEngine.GameObject[] CFX_Demo::ParticleExamples
	GameObjectU5BU5D_t311* ___ParticleExamples_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> CFX_Demo::ParticlesYOffsetD
	Dictionary_2_t320 * ___ParticlesYOffsetD_8;
	// System.Int32 CFX_Demo::exampleIndex
	int32_t ___exampleIndex_9;
	// System.String CFX_Demo::randomSpawnsDelay
	String_t* ___randomSpawnsDelay_10;
	// System.Boolean CFX_Demo::randomSpawns
	bool ___randomSpawns_11;
	// System.Boolean CFX_Demo::slowMo
	bool ___slowMo_12;
};
