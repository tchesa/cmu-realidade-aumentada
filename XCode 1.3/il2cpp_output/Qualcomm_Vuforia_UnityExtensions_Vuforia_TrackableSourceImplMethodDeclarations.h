﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.TrackableSourceImpl
struct TrackableSourceImpl_t1056;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.IntPtr Vuforia.TrackableSourceImpl::get_TrackableSourcePtr()
extern "C" IntPtr_t TrackableSourceImpl_get_TrackableSourcePtr_m6100 (TrackableSourceImpl_t1056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::set_TrackableSourcePtr(System.IntPtr)
extern "C" void TrackableSourceImpl_set_TrackableSourcePtr_m6101 (TrackableSourceImpl_t1056 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::.ctor(System.IntPtr)
extern "C" void TrackableSourceImpl__ctor_m6102 (TrackableSourceImpl_t1056 * __this, IntPtr_t ___trackableSourcePtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
