﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Component
struct Component_t56;
// UnityEngine.Transform
struct Transform_t25;
// UnityEngine.Rigidbody
struct Rigidbody_t50;
// UnityEngine.Camera
struct Camera_t510;
// UnityEngine.Light
struct Light_t47;
// UnityEngine.Renderer
struct Renderer_t46;
// UnityEngine.AudioSource
struct AudioSource_t20;
// UnityEngine.GUIText
struct GUIText_t44;
// UnityEngine.GUITexture
struct GUITexture_t43;
// UnityEngine.GameObject
struct GameObject_t27;
// System.Type
struct Type_t;
// UnityEngine.Component[]
struct ComponentU5BU5D_t54;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t826;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"

// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" Transform_t25 * Component_get_transform_m416 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::InternalGetTransform()
extern "C" Transform_t25 * Component_InternalGetTransform_m8065 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Component::get_rigidbody()
extern "C" Rigidbody_t50 * Component_get_rigidbody_m377 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Component::get_camera()
extern "C" Camera_t510 * Component_get_camera_m4029 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Light UnityEngine.Component::get_light()
extern "C" Light_t47 * Component_get_light_m352 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer UnityEngine.Component::get_renderer()
extern "C" Renderer_t46 * Component_get_renderer_m349 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource UnityEngine.Component::get_audio()
extern "C" AudioSource_t20 * Component_get_audio_m355 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIText UnityEngine.Component::get_guiText()
extern "C" GUIText_t44 * Component_get_guiText_m348 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUITexture UnityEngine.Component::get_guiTexture()
extern "C" GUITexture_t43 * Component_get_guiTexture_m346 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" GameObject_t27 * Component_get_gameObject_m306 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::InternalGetGameObject()
extern "C" GameObject_t27 * Component_InternalGetGameObject_m8066 (Component_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C" Component_t56 * Component_GetComponent_m4416 (Component_t56 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C" Component_t56 * Component_GetComponentInChildren_m8067 (Component_t56 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component[] UnityEngine.Component::GetComponentsWithCorrectReturnType(System.Type)
extern "C" ComponentU5BU5D_t54* Component_GetComponentsWithCorrectReturnType_m8068 (Component_t56 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Type,System.Boolean,System.Boolean,System.Object)
extern "C" void Component_GetComponentsForListInternal_m8069 (Component_t56 * __this, Type_t * ___searchType, Type_t * ___listElementType, bool ___recursive, bool ___includeInactive, Object_t * ___resultList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C" void Component_GetComponents_m4135 (Component_t56 * __this, Type_t * ___type, List_1_t826 * ___results, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void Component_SendMessage_m8070 (Component_t56 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object)
extern "C" void Component_SendMessage_m2805 (Component_t56 * __this, String_t* ___methodName, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::SendMessage(System.String)
extern "C" void Component_SendMessage_m2804 (Component_t56 * __this, String_t* ___methodName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
