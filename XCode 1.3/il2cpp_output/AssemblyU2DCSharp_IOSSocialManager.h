﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// IOSSocialManager
struct IOSSocialManager_t169;
// System.Action`1<ISN_Result>
struct Action_1_t99;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// IOSSocialManager
struct  IOSSocialManager_t169  : public EventDispatcher_t69
{
	// System.Action`1<ISN_Result> IOSSocialManager::OnFacebookPostResult
	Action_1_t99 * ___OnFacebookPostResult_10;
	// System.Action`1<ISN_Result> IOSSocialManager::OnTwitterPostResult
	Action_1_t99 * ___OnTwitterPostResult_11;
	// System.Action`1<ISN_Result> IOSSocialManager::OnMailResult
	Action_1_t99 * ___OnMailResult_12;
};
struct IOSSocialManager_t169_StaticFields{
	// IOSSocialManager IOSSocialManager::_instance
	IOSSocialManager_t169 * ____instance_9;
};
