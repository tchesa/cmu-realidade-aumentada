﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.EditorFacebookMockDialog
struct EditorFacebookMockDialog_t270;
// Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete
struct OnComplete_t269;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::.ctor()
extern "C" void EditorFacebookMockDialog__ctor_m1555 (EditorFacebookMockDialog_t270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete Facebook.Unity.Editor.EditorFacebookMockDialog::get_Callback()
extern "C" OnComplete_t269 * EditorFacebookMockDialog_get_Callback_m1556 (EditorFacebookMockDialog_t270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_Callback(Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete)
extern "C" void EditorFacebookMockDialog_set_Callback_m1557 (EditorFacebookMockDialog_t270 * __this, OnComplete_t269 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Editor.EditorFacebookMockDialog::get_CallbackID()
extern "C" String_t* EditorFacebookMockDialog_get_CallbackID_m1558 (EditorFacebookMockDialog_t270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_CallbackID(System.String)
extern "C" void EditorFacebookMockDialog_set_CallbackID_m1559 (EditorFacebookMockDialog_t270 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUI()
extern "C" void EditorFacebookMockDialog_OnGUI_m1560 (EditorFacebookMockDialog_t270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendCancelResult()
extern "C" void EditorFacebookMockDialog_SendCancelResult_m1561 (EditorFacebookMockDialog_t270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendErrorResult(System.String)
extern "C" void EditorFacebookMockDialog_SendErrorResult_m1562 (EditorFacebookMockDialog_t270 * __this, String_t* ___errorMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUIDialog(System.Int32)
extern "C" void EditorFacebookMockDialog_OnGUIDialog_m1563 (EditorFacebookMockDialog_t270 * __this, int32_t ___windowId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
