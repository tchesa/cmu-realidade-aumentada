﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1764;

#include "mscorlib_System_Object.h"

// System.IO.Stream
struct  Stream_t1764  : public Object_t
{
};
struct Stream_t1764_StaticFields{
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1764 * ___Null_0;
};
