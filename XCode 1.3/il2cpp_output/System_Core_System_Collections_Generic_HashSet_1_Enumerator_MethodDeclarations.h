﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m27311(__this, ___hashset, method) (( void (*) (Enumerator_t1232 *, HashSet_1_t1085 *, const MethodInfo*))Enumerator__ctor_m27300_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27312(__this, method) (( Object_t * (*) (Enumerator_t1232 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27301_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m27313(__this, method) (( void (*) (Enumerator_t1232 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m27302_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::MoveNext()
#define Enumerator_MoveNext_m6838(__this, method) (( bool (*) (Enumerator_t1232 *, const MethodInfo*))Enumerator_MoveNext_m27303_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::get_Current()
#define Enumerator_get_Current_m6837(__this, method) (( MeshRenderer_t596 * (*) (Enumerator_t1232 *, const MethodInfo*))Enumerator_get_Current_m27304_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::Dispose()
#define Enumerator_Dispose_m6839(__this, method) (( void (*) (Enumerator_t1232 *, const MethodInfo*))Enumerator_Dispose_m27305_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::CheckState()
#define Enumerator_CheckState_m27314(__this, method) (( void (*) (Enumerator_t1232 *, const MethodInfo*))Enumerator_CheckState_m27306_gshared)(__this, method)
