﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_80.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27700_gshared (InternalEnumerator_1_t3375 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27700(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3375 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27700_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27701_gshared (InternalEnumerator_1_t3375 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27701(__this, method) (( void (*) (InternalEnumerator_1_t3375 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27701_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27702_gshared (InternalEnumerator_1_t3375 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27702(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3375 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27702_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27703_gshared (InternalEnumerator_1_t3375 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27703(__this, method) (( void (*) (InternalEnumerator_1_t3375 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27703_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27704_gshared (InternalEnumerator_1_t3375 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27704(__this, method) (( bool (*) (InternalEnumerator_1_t3375 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27704_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m27705_gshared (InternalEnumerator_1_t3375 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27705(__this, method) (( int32_t (*) (InternalEnumerator_1_t3375 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27705_gshared)(__this, method)
