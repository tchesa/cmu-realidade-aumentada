﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.Dialogs.EmptyMockDialog
struct EmptyMockDialog_t271;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::.ctor()
extern "C" void EmptyMockDialog__ctor_m1564 (EmptyMockDialog_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::get_EmptyDialogTitle()
extern "C" String_t* EmptyMockDialog_get_EmptyDialogTitle_m1565 (EmptyMockDialog_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::set_EmptyDialogTitle(System.String)
extern "C" void EmptyMockDialog_set_EmptyDialogTitle_m1566 (EmptyMockDialog_t271 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::get_DialogTitle()
extern "C" String_t* EmptyMockDialog_get_DialogTitle_m1567 (EmptyMockDialog_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::DoGui()
extern "C" void EmptyMockDialog_DoGui_m1568 (EmptyMockDialog_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::SendSuccessResult()
extern "C" void EmptyMockDialog_SendSuccessResult_m1569 (EmptyMockDialog_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
