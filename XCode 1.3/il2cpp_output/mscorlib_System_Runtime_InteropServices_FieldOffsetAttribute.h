﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.FieldOffsetAttribute
struct  FieldOffsetAttribute_t1877  : public Attribute_t592
{
	// System.Int32 System.Runtime.InteropServices.FieldOffsetAttribute::val
	int32_t ___val_0;
};
