﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ISN_Security
struct ISN_Security_t157;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// ISN_Singleton`1<ISN_Security>
struct  ISN_Singleton_1_t158  : public EventDispatcher_t69
{
};
struct ISN_Singleton_1_t158_StaticFields{
	// T ISN_Singleton`1<ISN_Security>::_instance
	ISN_Security_t157 * ____instance_3;
	// System.Boolean ISN_Singleton`1<ISN_Security>::applicationIsQuitting
	bool ___applicationIsQuitting_4;
};
