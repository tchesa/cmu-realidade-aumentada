﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSProductTemplate[]
struct IOSProductTemplateU5BU5D_t2727;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<IOSProductTemplate>
struct  List_1_t131  : public Object_t
{
	// T[] System.Collections.Generic.List`1<IOSProductTemplate>::_items
	IOSProductTemplateU5BU5D_t2727* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<IOSProductTemplate>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<IOSProductTemplate>::_version
	int32_t ____version_3;
};
struct List_1_t131_StaticFields{
	// T[] System.Collections.Generic.List`1<IOSProductTemplate>::EmptyArray
	IOSProductTemplateU5BU5D_t2727* ___EmptyArray_4;
};
