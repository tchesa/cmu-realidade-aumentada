﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3472;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C" void Enumerator__ctor_m29080_gshared (Enumerator_t3474 * __this, LinkedList_1_t3472 * ___parent, const MethodInfo* method);
#define Enumerator__ctor_m29080(__this, ___parent, method) (( void (*) (Enumerator_t3474 *, LinkedList_1_t3472 *, const MethodInfo*))Enumerator__ctor_m29080_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m29081_gshared (Enumerator_t3474 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m29081(__this, method) (( Object_t * (*) (Enumerator_t3474 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m29081_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m29082_gshared (Enumerator_t3474 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m29082(__this, method) (( void (*) (Enumerator_t3474 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m29082_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m29083_gshared (Enumerator_t3474 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m29083(__this, method) (( Object_t * (*) (Enumerator_t3474 *, const MethodInfo*))Enumerator_get_Current_m29083_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m29084_gshared (Enumerator_t3474 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m29084(__this, method) (( bool (*) (Enumerator_t3474 *, const MethodInfo*))Enumerator_MoveNext_m29084_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m29085_gshared (Enumerator_t3474 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m29085(__this, method) (( void (*) (Enumerator_t3474 *, const MethodInfo*))Enumerator_Dispose_m29085_gshared)(__this, method)
