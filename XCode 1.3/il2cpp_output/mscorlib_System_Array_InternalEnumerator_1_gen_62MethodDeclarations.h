﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_62.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_40.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26107_gshared (InternalEnumerator_1_t3256 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26107(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3256 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26107_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26108_gshared (InternalEnumerator_1_t3256 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26108(__this, method) (( void (*) (InternalEnumerator_1_t3256 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26108_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26109_gshared (InternalEnumerator_1_t3256 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26109(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3256 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26109_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26110_gshared (InternalEnumerator_1_t3256 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26110(__this, method) (( void (*) (InternalEnumerator_1_t3256 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26110_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26111_gshared (InternalEnumerator_1_t3256 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26111(__this, method) (( bool (*) (InternalEnumerator_1_t3256 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26111_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::get_Current()
extern "C" KeyValuePair_2_t3255  InternalEnumerator_1_get_Current_m26112_gshared (InternalEnumerator_1_t3256 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26112(__this, method) (( KeyValuePair_2_t3255  (*) (InternalEnumerator_1_t3256 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26112_gshared)(__this, method)
