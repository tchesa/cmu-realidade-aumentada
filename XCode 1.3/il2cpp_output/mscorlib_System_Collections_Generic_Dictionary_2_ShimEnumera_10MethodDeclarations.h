﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t3489;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3476;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m29267_gshared (ShimEnumerator_t3489 * __this, Dictionary_2_t3476 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m29267(__this, ___host, method) (( void (*) (ShimEnumerator_t3489 *, Dictionary_2_t3476 *, const MethodInfo*))ShimEnumerator__ctor_m29267_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m29268_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m29268(__this, method) (( bool (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_MoveNext_m29268_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern "C" DictionaryEntry_t58  ShimEnumerator_get_Entry_m29269_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m29269(__this, method) (( DictionaryEntry_t58  (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_get_Entry_m29269_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m29270_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m29270(__this, method) (( Object_t * (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_get_Key_m29270_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m29271_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m29271(__this, method) (( Object_t * (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_get_Value_m29271_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m29272_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m29272(__this, method) (( Object_t * (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_get_Current_m29272_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C" void ShimEnumerator_Reset_m29273_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m29273(__this, method) (( void (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_Reset_m29273_gshared)(__this, method)
