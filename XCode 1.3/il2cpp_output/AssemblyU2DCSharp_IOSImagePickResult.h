﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t33;

#include "AssemblyU2DCSharp_ISN_Result.h"

// IOSImagePickResult
struct  IOSImagePickResult_t149  : public ISN_Result_t114
{
	// UnityEngine.Texture2D IOSImagePickResult::_image
	Texture2D_t33 * ____image_2;
};
