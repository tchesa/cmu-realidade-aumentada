﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// CFX2_AutoRotate
struct  CFX2_AutoRotate_t324  : public MonoBehaviour_t18
{
	// UnityEngine.Vector3 CFX2_AutoRotate::speed
	Vector3_t6  ___speed_1;
};
