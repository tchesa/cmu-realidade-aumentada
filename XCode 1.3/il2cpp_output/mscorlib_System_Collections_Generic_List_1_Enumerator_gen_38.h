﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ARAnimation/TrailTrigger>
struct List_1_t349;
// ARAnimation/TrailTrigger
struct TrailTrigger_t341;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<ARAnimation/TrailTrigger>
struct  Enumerator_t2832 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<ARAnimation/TrailTrigger>::l
	List_1_t349 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<ARAnimation/TrailTrigger>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<ARAnimation/TrailTrigger>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<ARAnimation/TrailTrigger>::current
	TrailTrigger_t341 * ___current_3;
};
