﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m4022(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t643 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m19763_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Invoke(T0)
#define UnityAction_1_Invoke_m19778(__this, ___arg0, method) (( void (*) (UnityAction_1_t643 *, List_1_t799 *, const MethodInfo*))UnityAction_1_Invoke_m19764_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m19779(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t643 *, List_1_t799 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m19765_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m19780(__this, ___result, method) (( void (*) (UnityAction_1_t643 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m19766_gshared)(__this, ___result, method)
