﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARAnimation/TrailTrigger[]
struct TrailTriggerU5BU5D_t348;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<ARAnimation/TrailTrigger>
struct  List_1_t349  : public Object_t
{
	// T[] System.Collections.Generic.List`1<ARAnimation/TrailTrigger>::_items
	TrailTriggerU5BU5D_t348* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<ARAnimation/TrailTrigger>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<ARAnimation/TrailTrigger>::_version
	int32_t ____version_3;
};
struct List_1_t349_StaticFields{
	// T[] System.Collections.Generic.List`1<ARAnimation/TrailTrigger>::EmptyArray
	TrailTriggerU5BU5D_t348* ___EmptyArray_4;
};
