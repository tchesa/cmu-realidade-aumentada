﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t1442;

#include "mscorlib_System_Text_Decoder.h"

// System.Text.Encoding/ForwardingDecoder
struct  ForwardingDecoder_t2380  : public Decoder_t2005
{
	// System.Text.Encoding System.Text.Encoding/ForwardingDecoder::encoding
	Encoding_t1442 * ___encoding_2;
};
