﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GCLeaderboard
struct GCLeaderboard_t121;
// System.String
struct String_t;
// GCScore
struct GCScore_t117;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GCBoardTimeSpan.h"
#include "AssemblyU2DCSharp_GCCollectionType.h"

// System.Void GCLeaderboard::.ctor(System.String)
extern "C" void GCLeaderboard__ctor_m685 (GCLeaderboard_t121 * __this, String_t* ___leaderboardId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GCLeaderboard::get_id()
extern "C" String_t* GCLeaderboard_get_id_m686 (GCLeaderboard_t121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GCScore GCLeaderboard::GetCurrentPlayerScore(GCBoardTimeSpan,GCCollectionType)
extern "C" GCScore_t117 * GCLeaderboard_GetCurrentPlayerScore_m687 (GCLeaderboard_t121 * __this, int32_t ___timeSpan, int32_t ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GCLeaderboard::UpdateCurrentPlayerRank(System.Int32,GCBoardTimeSpan,GCCollectionType)
extern "C" void GCLeaderboard_UpdateCurrentPlayerRank_m688 (GCLeaderboard_t121 * __this, int32_t ___rank, int32_t ___timeSpan, int32_t ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GCScore GCLeaderboard::GetScore(System.Int32,GCBoardTimeSpan,GCCollectionType)
extern "C" GCScore_t117 * GCLeaderboard_GetScore_m689 (GCLeaderboard_t121 * __this, int32_t ___rank, int32_t ___scope, int32_t ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GCLeaderboard::UpdateScore(GCScore)
extern "C" void GCLeaderboard_UpdateScore_m690 (GCLeaderboard_t121 * __this, GCScore_t117 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
