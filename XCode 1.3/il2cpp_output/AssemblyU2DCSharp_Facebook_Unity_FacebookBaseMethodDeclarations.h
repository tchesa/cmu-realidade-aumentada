﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FacebookBase
struct FacebookBase_t227;
// Facebook.Unity.CallbackManager
struct CallbackManager_t224;
// System.String
struct String_t;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t242;
// Facebook.Unity.InitDelegate
struct InitDelegate_t241;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t221;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t476;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>
struct FacebookDelegate_1_t468;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t295;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t294;
// UnityEngine.WWWForm
struct WWWForm_t293;
// Facebook.Unity.LoginResult
struct LoginResult_t282;
// System.Uri
struct Uri_t292;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_1.h"
#include "AssemblyU2DCSharp_Facebook_Unity_HttpMethod.h"
#include "mscorlib_System_Nullable_1_gen_0.h"

// System.Void Facebook.Unity.FacebookBase::.ctor(Facebook.Unity.CallbackManager)
extern "C" void FacebookBase__ctor_m1334 (FacebookBase_t227 * __this, CallbackManager_t224 * ___callbackManager, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.FacebookBase::get_SDKUserAgent()
extern "C" String_t* FacebookBase_get_SDKUserAgent_m1335 (FacebookBase_t227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.FacebookBase::get_LoggedIn()
extern "C" bool FacebookBase_get_LoggedIn_m1336 (FacebookBase_t227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::get_CallbackManager()
extern "C" CallbackManager_t224 * FacebookBase_get_CallbackManager_m1337 (FacebookBase_t227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::set_CallbackManager(Facebook.Unity.CallbackManager)
extern "C" void FacebookBase_set_CallbackManager_m1338 (FacebookBase_t227 * __this, CallbackManager_t224 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::Init(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.Boolean,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern "C" void FacebookBase_Init_m1339 (FacebookBase_t227 * __this, String_t* ___appId, bool ___cookie, bool ___logging, bool ___status, bool ___xfbml, String_t* ___channelUrl, String_t* ___authResponse, bool ___frictionlessRequests, HideUnityDelegate_t242 * ___hideUnityDelegate, InitDelegate_t241 * ___onInitComplete, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::LogOut()
extern "C" void FacebookBase_LogOut_m1340 (FacebookBase_t227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::AppRequest(System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern "C" void FacebookBase_AppRequest_m1341 (FacebookBase_t227 * __this, String_t* ___message, Object_t* ___to, Object_t* ___filters, Object_t* ___excludeIds, Nullable_1_t467  ___maxRecipients, String_t* ___data, String_t* ___title, FacebookDelegate_1_t468 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::API(System.String,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern "C" void FacebookBase_API_m1342 (FacebookBase_t227 * __this, String_t* ___query, int32_t ___method, Object_t* ___formData, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::API(System.String,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern "C" void FacebookBase_API_m1343 (FacebookBase_t227 * __this, String_t* ___query, int32_t ___method, WWWForm_t293 * ___formData, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::OnHideUnity(System.Boolean)
extern "C" void FacebookBase_OnHideUnity_m1344 (FacebookBase_t227 * __this, bool ___isGameShown, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::OnInitComplete(System.String)
extern "C" void FacebookBase_OnInitComplete_m1345 (FacebookBase_t227 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::OnLogoutComplete(System.String)
extern "C" void FacebookBase_OnLogoutComplete_m1346 (FacebookBase_t227 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::ValidateAppRequestArgs(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern "C" void FacebookBase_ValidateAppRequestArgs_m1347 (FacebookBase_t227 * __this, String_t* ___message, Nullable_1_t466  ___actionType, String_t* ___objectId, Object_t* ___to, Object_t* ___filters, Object_t* ___excludeIds, Nullable_1_t467  ___maxRecipients, String_t* ___data, String_t* ___title, FacebookDelegate_1_t468 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookBase::OnAuthResponse(Facebook.Unity.LoginResult)
extern "C" void FacebookBase_OnAuthResponse_m1348 (FacebookBase_t227 * __this, LoginResult_t282 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.FacebookBase::CopyByValue(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C" Object_t* FacebookBase_CopyByValue_m1349 (FacebookBase_t227 * __this, Object_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Facebook.Unity.FacebookBase::GetGraphUrl(System.String)
extern "C" Uri_t292 * FacebookBase_GetGraphUrl_m1350 (FacebookBase_t227 * __this, String_t* ___query, const MethodInfo* method) IL2CPP_METHOD_ATTR;
