﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1288;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m27892_gshared (Enumerator_t3391 * __this, List_1_t1288 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m27892(__this, ___l, method) (( void (*) (Enumerator_t3391 *, List_1_t1288 *, const MethodInfo*))Enumerator__ctor_m27892_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m27893_gshared (Enumerator_t3391 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m27893(__this, method) (( void (*) (Enumerator_t3391 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m27893_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27894_gshared (Enumerator_t3391 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27894(__this, method) (( Object_t * (*) (Enumerator_t3391 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27894_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m27895_gshared (Enumerator_t3391 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27895(__this, method) (( void (*) (Enumerator_t3391 *, const MethodInfo*))Enumerator_Dispose_m27895_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m27896_gshared (Enumerator_t3391 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m27896(__this, method) (( void (*) (Enumerator_t3391 *, const MethodInfo*))Enumerator_VerifyState_m27896_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m27897_gshared (Enumerator_t3391 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m27897(__this, method) (( bool (*) (Enumerator_t3391 *, const MethodInfo*))Enumerator_MoveNext_m27897_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t859  Enumerator_get_Current_m27898_gshared (Enumerator_t3391 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m27898(__this, method) (( UICharInfo_t859  (*) (Enumerator_t3391 *, const MethodInfo*))Enumerator_get_Current_m27898_gshared)(__this, method)
