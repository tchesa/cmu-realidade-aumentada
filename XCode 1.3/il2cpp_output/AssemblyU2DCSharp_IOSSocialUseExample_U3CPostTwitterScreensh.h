﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t33;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator3
struct  U3CPostTwitterScreenshotU3Ec__Iterator3_t205  : public Object_t
{
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator3::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator3::<height>__1
	int32_t ___U3CheightU3E__1_1;
	// UnityEngine.Texture2D IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator3::<tex>__2
	Texture2D_t33 * ___U3CtexU3E__2_2;
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator3::$PC
	int32_t ___U24PC_3;
	// System.Object IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator3::$current
	Object_t * ___U24current_4;
};
