﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasFacebook_Canva_4MethodDeclarations.h"

// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IGroupJoinResult>::.ctor(Facebook.Unity.Canvas.CanvasFacebook,System.String,System.String)
#define CanvasUIMethodCall_1__ctor_m2387(__this, ___canvasImpl, ___methodName, ___callbackMethod, method) (( void (*) (CanvasUIMethodCall_1_t527 *, CanvasFacebook_t226 *, String_t*, String_t*, const MethodInfo*))CanvasUIMethodCall_1__ctor_m18261_gshared)(__this, ___canvasImpl, ___methodName, ___callbackMethod, method)
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IGroupJoinResult>::Call(Facebook.Unity.MethodArguments)
#define CanvasUIMethodCall_1_Call_m18329(__this, ___args, method) (( void (*) (CanvasUIMethodCall_1_t527 *, MethodArguments_t248 *, const MethodInfo*))CanvasUIMethodCall_1_Call_m18263_gshared)(__this, ___args, method)
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IGroupJoinResult>::UI(System.String,Facebook.Unity.MethodArguments,Facebook.Unity.FacebookDelegate`1<T>)
#define CanvasUIMethodCall_1_UI_m18330(__this, ___method, ___args, ___callback, method) (( void (*) (CanvasUIMethodCall_1_t527 *, String_t*, MethodArguments_t248 *, FacebookDelegate_1_t472 *, const MethodInfo*))CanvasUIMethodCall_1_UI_m18265_gshared)(__this, ___method, ___args, ___callback, method)
