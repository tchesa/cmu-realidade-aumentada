﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3149;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_41.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m24265_gshared (Enumerator_t3155 * __this, Dictionary_2_t3149 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m24265(__this, ___host, method) (( void (*) (Enumerator_t3155 *, Dictionary_2_t3149 *, const MethodInfo*))Enumerator__ctor_m24265_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24266_gshared (Enumerator_t3155 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24266(__this, method) (( Object_t * (*) (Enumerator_t3155 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24266_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m24267_gshared (Enumerator_t3155 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m24267(__this, method) (( void (*) (Enumerator_t3155 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m24268_gshared (Enumerator_t3155 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m24268(__this, method) (( void (*) (Enumerator_t3155 *, const MethodInfo*))Enumerator_Dispose_m24268_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24269_gshared (Enumerator_t3155 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24269(__this, method) (( bool (*) (Enumerator_t3155 *, const MethodInfo*))Enumerator_MoveNext_m24269_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m24270_gshared (Enumerator_t3155 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m24270(__this, method) (( Object_t * (*) (Enumerator_t3155 *, const MethodInfo*))Enumerator_get_Current_m24270_gshared)(__this, method)
