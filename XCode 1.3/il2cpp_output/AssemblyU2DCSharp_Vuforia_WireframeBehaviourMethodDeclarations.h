﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t454;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C" void WireframeBehaviour__ctor_m2123 (WireframeBehaviour_t454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern "C" void WireframeBehaviour_CreateLineMaterial_m2124 (WireframeBehaviour_t454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern "C" void WireframeBehaviour_OnRenderObject_m2125 (WireframeBehaviour_t454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern "C" void WireframeBehaviour_OnDrawGizmos_m2126 (WireframeBehaviour_t454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
