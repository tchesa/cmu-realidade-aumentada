﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t486;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15550_gshared (Enumerator_t552 * __this, List_1_t486 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15550(__this, ___l, method) (( void (*) (Enumerator_t552 *, List_1_t486 *, const MethodInfo*))Enumerator__ctor_m15550_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15551_gshared (Enumerator_t552 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15551(__this, method) (( void (*) (Enumerator_t552 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15551_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15552_gshared (Enumerator_t552 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15552(__this, method) (( Object_t * (*) (Enumerator_t552 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15552_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15553_gshared (Enumerator_t552 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15553(__this, method) (( void (*) (Enumerator_t552 *, const MethodInfo*))Enumerator_Dispose_m15553_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m15554_gshared (Enumerator_t552 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15554(__this, method) (( void (*) (Enumerator_t552 *, const MethodInfo*))Enumerator_VerifyState_m15554_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m2489_gshared (Enumerator_t552 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2489(__this, method) (( bool (*) (Enumerator_t552 *, const MethodInfo*))Enumerator_MoveNext_m2489_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m2488_gshared (Enumerator_t552 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2488(__this, method) (( Object_t * (*) (Enumerator_t552 *, const MethodInfo*))Enumerator_get_Current_m2488_gshared)(__this, method)
