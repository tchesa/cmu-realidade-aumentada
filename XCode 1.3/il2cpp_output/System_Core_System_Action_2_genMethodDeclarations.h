﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen_0MethodDeclarations.h"

// System.Void System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2665(__this, ___object, ___method, method) (( void (*) (Action_2_t380 *, Object_t *, IntPtr_t, const MethodInfo*))Action_2__ctor_m19399_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>::Invoke(T1,T2)
#define Action_2_Invoke_m2668(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t380 *, bool, Object_t *, const MethodInfo*))Action_2_Invoke_m19400_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m19401(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t380 *, bool, Object_t *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Action_2_BeginInvoke_m19402_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m19403(__this, ___result, method) (( void (*) (Action_2_t380 *, Object_t *, const MethodInfo*))Action_2_EndInvoke_m19404_gshared)(__this, ___result, method)
