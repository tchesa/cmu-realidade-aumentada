﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// CFX_Demo_RandomDir
struct  CFX_Demo_RandomDir_t321  : public MonoBehaviour_t18
{
	// UnityEngine.Vector3 CFX_Demo_RandomDir::min
	Vector3_t6  ___min_1;
	// UnityEngine.Vector3 CFX_Demo_RandomDir::max
	Vector3_t6  ___max_2;
};
