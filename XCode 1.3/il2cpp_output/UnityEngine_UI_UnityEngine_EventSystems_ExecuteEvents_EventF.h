﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.IDeselectHandler
struct IDeselectHandler_t813;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t615;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct  EventFunction_1_t637  : public MulticastDelegate_t10
{
};
