﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// IOSNotificationController
struct IOSNotificationController_t140;
// System.Action`1<IOSNotificationDeviceToken>
struct Action_1_t142;
// System.Action`1<ISN_Result>
struct Action_1_t99;

#include "AssemblyU2DCSharp_ISN_Singleton_1_gen.h"

// IOSNotificationController
struct  IOSNotificationController_t140  : public ISN_Singleton_1_t141
{
	// System.Action`1<IOSNotificationDeviceToken> IOSNotificationController::OnDeviceTokenReceived
	Action_1_t142 * ___OnDeviceTokenReceived_11;
	// System.Action`1<ISN_Result> IOSNotificationController::OnNotificationScheduleResult
	Action_1_t99 * ___OnNotificationScheduleResult_12;
};
struct IOSNotificationController_t140_StaticFields{
	// IOSNotificationController IOSNotificationController::_instance
	IOSNotificationController_t140 * ____instance_9;
	// System.Int32 IOSNotificationController::_AllowedNotificationsType
	int32_t ____AllowedNotificationsType_10;
	// System.Action`1<IOSNotificationDeviceToken> IOSNotificationController::<>f__am$cache4
	Action_1_t142 * ___U3CU3Ef__amU24cache4_13;
	// System.Action`1<ISN_Result> IOSNotificationController::<>f__am$cache5
	Action_1_t99 * ___U3CU3Ef__amU24cache5_14;
};
