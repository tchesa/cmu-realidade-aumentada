﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m18714(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t569 *, String_t*, float, const MethodInfo*))KeyValuePair_2__ctor_m18616_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m2571(__this, method) (( String_t* (*) (KeyValuePair_2_t569 *, const MethodInfo*))KeyValuePair_2_get_Key_m18617_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18715(__this, ___value, method) (( void (*) (KeyValuePair_2_t569 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m18618_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m2572(__this, method) (( float (*) (KeyValuePair_2_t569 *, const MethodInfo*))KeyValuePair_2_get_Value_m18619_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18716(__this, ___value, method) (( void (*) (KeyValuePair_2_t569 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m18620_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::ToString()
#define KeyValuePair_2_ToString_m18717(__this, method) (( String_t* (*) (KeyValuePair_2_t569 *, const MethodInfo*))KeyValuePair_2_ToString_m18621_gshared)(__this, method)
