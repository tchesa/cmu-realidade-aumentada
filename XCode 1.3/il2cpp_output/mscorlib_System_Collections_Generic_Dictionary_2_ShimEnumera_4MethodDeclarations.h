﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct ShimEnumerator_t3071;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t3058;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22869_gshared (ShimEnumerator_t3071 * __this, Dictionary_2_t3058 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22869(__this, ___host, method) (( void (*) (ShimEnumerator_t3071 *, Dictionary_2_t3058 *, const MethodInfo*))ShimEnumerator__ctor_m22869_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22870_gshared (ShimEnumerator_t3071 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22870(__this, method) (( bool (*) (ShimEnumerator_t3071 *, const MethodInfo*))ShimEnumerator_MoveNext_m22870_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Entry()
extern "C" DictionaryEntry_t58  ShimEnumerator_get_Entry_m22871_gshared (ShimEnumerator_t3071 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22871(__this, method) (( DictionaryEntry_t58  (*) (ShimEnumerator_t3071 *, const MethodInfo*))ShimEnumerator_get_Entry_m22871_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22872_gshared (ShimEnumerator_t3071 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22872(__this, method) (( Object_t * (*) (ShimEnumerator_t3071 *, const MethodInfo*))ShimEnumerator_get_Key_m22872_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22873_gshared (ShimEnumerator_t3071 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22873(__this, method) (( Object_t * (*) (ShimEnumerator_t3071 *, const MethodInfo*))ShimEnumerator_get_Value_m22873_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22874_gshared (ShimEnumerator_t3071 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22874(__this, method) (( Object_t * (*) (ShimEnumerator_t3071 *, const MethodInfo*))ShimEnumerator_get_Current_m22874_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m22875_gshared (ShimEnumerator_t3071 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m22875(__this, method) (( void (*) (ShimEnumerator_t3071 *, const MethodInfo*))ShimEnumerator_Reset_m22875_gshared)(__this, method)
