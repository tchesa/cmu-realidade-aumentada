﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.Byte>
struct GenericComparer_1_t2718;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Byte>::.ctor()
extern "C" void GenericComparer_1__ctor_m17318_gshared (GenericComparer_1_t2718 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m17318(__this, method) (( void (*) (GenericComparer_1_t2718 *, const MethodInfo*))GenericComparer_1__ctor_m17318_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Byte>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m17319_gshared (GenericComparer_1_t2718 * __this, uint8_t ___x, uint8_t ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m17319(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2718 *, uint8_t, uint8_t, const MethodInfo*))GenericComparer_1_Compare_m17319_gshared)(__this, ___x, ___y, method)
