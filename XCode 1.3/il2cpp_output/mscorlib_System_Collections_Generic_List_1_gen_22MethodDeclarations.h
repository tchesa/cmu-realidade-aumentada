﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.ctor()
#define List_1__ctor_m4122(__this, method) (( void (*) (List_1_t842 *, const MethodInfo*))List_1__ctor_m2529_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20813(__this, ___collection, method) (( void (*) (List_1_t842 *, Object_t*, const MethodInfo*))List_1__ctor_m15449_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.ctor(System.Int32)
#define List_1__ctor_m20814(__this, ___capacity, method) (( void (*) (List_1_t842 *, int32_t, const MethodInfo*))List_1__ctor_m15451_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.cctor()
#define List_1__cctor_m20815(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15453_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20816(__this, method) (( Object_t* (*) (List_1_t842 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20817(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t842 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15457_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20818(__this, method) (( Object_t * (*) (List_1_t842 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15459_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20819(__this, ___item, method) (( int32_t (*) (List_1_t842 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m15461_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20820(__this, ___item, method) (( bool (*) (List_1_t842 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m15463_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20821(__this, ___item, method) (( int32_t (*) (List_1_t842 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m15465_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20822(__this, ___index, ___item, method) (( void (*) (List_1_t842 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m15467_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20823(__this, ___item, method) (( void (*) (List_1_t842 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m15469_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20824(__this, method) (( bool (*) (List_1_t842 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15471_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20825(__this, method) (( bool (*) (List_1_t842 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15473_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20826(__this, method) (( Object_t * (*) (List_1_t842 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20827(__this, method) (( bool (*) (List_1_t842 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15477_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20828(__this, method) (( bool (*) (List_1_t842 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15479_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20829(__this, ___index, method) (( Object_t * (*) (List_1_t842 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m15481_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20830(__this, ___index, ___value, method) (( void (*) (List_1_t842 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m15483_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Add(T)
#define List_1_Add_m20831(__this, ___item, method) (( void (*) (List_1_t842 *, Text_t580 *, const MethodInfo*))List_1_Add_m15485_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20832(__this, ___newCount, method) (( void (*) (List_1_t842 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15487_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20833(__this, ___collection, method) (( void (*) (List_1_t842 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15489_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20834(__this, ___enumerable, method) (( void (*) (List_1_t842 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15491_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20835(__this, ___collection, method) (( void (*) (List_1_t842 *, Object_t*, const MethodInfo*))List_1_AddRange_m15493_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::AsReadOnly()
#define List_1_AsReadOnly_m20836(__this, method) (( ReadOnlyCollection_1_t2929 * (*) (List_1_t842 *, const MethodInfo*))List_1_AsReadOnly_m15495_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Clear()
#define List_1_Clear_m20837(__this, method) (( void (*) (List_1_t842 *, const MethodInfo*))List_1_Clear_m15497_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::Contains(T)
#define List_1_Contains_m20838(__this, ___item, method) (( bool (*) (List_1_t842 *, Text_t580 *, const MethodInfo*))List_1_Contains_m15499_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20839(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t842 *, TextU5BU5D_t2928*, int32_t, const MethodInfo*))List_1_CopyTo_m15501_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Text>::Find(System.Predicate`1<T>)
#define List_1_Find_m20840(__this, ___match, method) (( Text_t580 * (*) (List_1_t842 *, Predicate_1_t2931 *, const MethodInfo*))List_1_Find_m15503_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20841(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2931 *, const MethodInfo*))List_1_CheckMatch_m15505_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20842(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t842 *, int32_t, int32_t, Predicate_1_t2931 *, const MethodInfo*))List_1_GetIndex_m15507_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::GetEnumerator()
#define List_1_GetEnumerator_m20843(__this, method) (( Enumerator_t2932  (*) (List_1_t842 *, const MethodInfo*))List_1_GetEnumerator_m2487_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::IndexOf(T)
#define List_1_IndexOf_m20844(__this, ___item, method) (( int32_t (*) (List_1_t842 *, Text_t580 *, const MethodInfo*))List_1_IndexOf_m15509_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20845(__this, ___start, ___delta, method) (( void (*) (List_1_t842 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15511_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20846(__this, ___index, method) (( void (*) (List_1_t842 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15513_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Insert(System.Int32,T)
#define List_1_Insert_m20847(__this, ___index, ___item, method) (( void (*) (List_1_t842 *, int32_t, Text_t580 *, const MethodInfo*))List_1_Insert_m15515_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20848(__this, ___collection, method) (( void (*) (List_1_t842 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15517_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::Remove(T)
#define List_1_Remove_m20849(__this, ___item, method) (( bool (*) (List_1_t842 *, Text_t580 *, const MethodInfo*))List_1_Remove_m15519_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20850(__this, ___match, method) (( int32_t (*) (List_1_t842 *, Predicate_1_t2931 *, const MethodInfo*))List_1_RemoveAll_m15521_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20851(__this, ___index, method) (( void (*) (List_1_t842 *, int32_t, const MethodInfo*))List_1_RemoveAt_m15523_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Reverse()
#define List_1_Reverse_m20852(__this, method) (( void (*) (List_1_t842 *, const MethodInfo*))List_1_Reverse_m15525_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Sort()
#define List_1_Sort_m20853(__this, method) (( void (*) (List_1_t842 *, const MethodInfo*))List_1_Sort_m15527_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20854(__this, ___comparison, method) (( void (*) (List_1_t842 *, Comparison_1_t2933 *, const MethodInfo*))List_1_Sort_m15529_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Text>::ToArray()
#define List_1_ToArray_m20855(__this, method) (( TextU5BU5D_t2928* (*) (List_1_t842 *, const MethodInfo*))List_1_ToArray_m15531_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::TrimExcess()
#define List_1_TrimExcess_m20856(__this, method) (( void (*) (List_1_t842 *, const MethodInfo*))List_1_TrimExcess_m15533_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Capacity()
#define List_1_get_Capacity_m20857(__this, method) (( int32_t (*) (List_1_t842 *, const MethodInfo*))List_1_get_Capacity_m15535_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20858(__this, ___value, method) (( void (*) (List_1_t842 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15537_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Count()
#define List_1_get_Count_m20859(__this, method) (( int32_t (*) (List_1_t842 *, const MethodInfo*))List_1_get_Count_m15539_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Item(System.Int32)
#define List_1_get_Item_m20860(__this, ___index, method) (( Text_t580 * (*) (List_1_t842 *, int32_t, const MethodInfo*))List_1_get_Item_m15541_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::set_Item(System.Int32,T)
#define List_1_set_Item_m20861(__this, ___index, ___value, method) (( void (*) (List_1_t842 *, int32_t, Text_t580 *, const MethodInfo*))List_1_set_Item_m15543_gshared)(__this, ___index, ___value, method)
