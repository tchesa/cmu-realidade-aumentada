﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t31;

#include "mscorlib_System_ValueType.h"

// AnimationController/Audios
struct  Audios_t355 
{
	// UnityEngine.AudioClip AnimationController/Audios::Abertura
	AudioClip_t31 * ___Abertura_0;
	// UnityEngine.AudioClip AnimationController/Audios::SaiDaLogo
	AudioClip_t31 * ___SaiDaLogo_1;
	// UnityEngine.AudioClip AnimationController/Audios::BaseInicial
	AudioClip_t31 * ___BaseInicial_2;
	// UnityEngine.AudioClip AnimationController/Audios::Acendendo
	AudioClip_t31 * ___Acendendo_3;
	// UnityEngine.AudioClip AnimationController/Audios::Passeio
	AudioClip_t31 * ___Passeio_4;
	// UnityEngine.AudioClip AnimationController/Audios::Subida
	AudioClip_t31 * ___Subida_5;
	// UnityEngine.AudioClip AnimationController/Audios::ToqueNaTela
	AudioClip_t31 * ___ToqueNaTela_6;
	// UnityEngine.AudioClip AnimationController/Audios::Agradecimento
	AudioClip_t31 * ___Agradecimento_7;
};
