﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartUIController/<StartTransiction>c__Iterator18
struct U3CStartTransictionU3Ec__Iterator18_t389;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void StartUIController/<StartTransiction>c__Iterator18::.ctor()
extern "C" void U3CStartTransictionU3Ec__Iterator18__ctor_m1983 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StartUIController/<StartTransiction>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartTransictionU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1984 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StartUIController/<StartTransiction>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartTransictionU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m1985 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StartUIController/<StartTransiction>c__Iterator18::MoveNext()
extern "C" bool U3CStartTransictionU3Ec__Iterator18_MoveNext_m1986 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController/<StartTransiction>c__Iterator18::Dispose()
extern "C" void U3CStartTransictionU3Ec__Iterator18_Dispose_m1987 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController/<StartTransiction>c__Iterator18::Reset()
extern "C" void U3CStartTransictionU3Ec__Iterator18_Reset_m1988 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
