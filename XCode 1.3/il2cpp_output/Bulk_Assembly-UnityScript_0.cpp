﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// JSCall
struct JSCall_t607;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E.h"
#include "AssemblyU2DUnityScript_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DUnityScript_JSCall.h"
#include "AssemblyU2DUnityScript_JSCallMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_ArrayMethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_Array.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Object.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JSCall::.ctor()
extern "C" void JSCall__ctor_m2800 (JSCall_t607 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSCall::Start()
extern Il2CppCodeGenString* _stringLiteral832;
extern "C" void JSCall_Start_m2801 (JSCall_t607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral832 = il2cpp_codegen_string_literal_from_index(832);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_SendMessage_m2804(__this, _stringLiteral832, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSCall::OnGUI()
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* Array_t608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral833;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral834;
extern Il2CppCodeGenString* _stringLiteral835;
extern Il2CppCodeGenString* _stringLiteral239;
extern Il2CppCodeGenString* _stringLiteral836;
extern Il2CppCodeGenString* _stringLiteral99;
extern "C" void JSCall_OnGUI_m2802 (JSCall_t607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		Array_t608_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(431);
		_stringLiteral833 = il2cpp_codegen_string_literal_from_index(833);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral834 = il2cpp_codegen_string_literal_from_index(834);
		_stringLiteral835 = il2cpp_codegen_string_literal_from_index(835);
		_stringLiteral239 = il2cpp_codegen_string_literal_from_index(239);
		_stringLiteral836 = il2cpp_codegen_string_literal_from_index(836);
		_stringLiteral99 = il2cpp_codegen_string_literal_from_index(99);
		s_Il2CppMethodIntialized = true;
	}
	Array_t608 * V_0 = {0};
	{
		Rect_t30  L_0 = {0};
		Rect__ctor_m387(&L_0, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)150)))), (((float)((float)((int32_t)40)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m2298(NULL /*static, unused*/, L_0, _stringLiteral833, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_2 = ((int32_t)100);
		Object_t * L_3 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_2);
		Component_SendMessage_m2805(__this, _stringLiteral313, L_3, /*hidden argument*/NULL);
	}

IL_0035:
	{
		Rect_t30  L_4 = {0};
		Rect__ctor_m387(&L_4, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)60)))), (((float)((float)((int32_t)150)))), (((float)((float)((int32_t)40)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		bool L_5 = GUI_Button_m2298(NULL /*static, unused*/, L_4, _stringLiteral834, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_008c;
		}
	}
	{
		Array_t608 * L_6 = (Array_t608 *)il2cpp_codegen_object_new (Array_t608_il2cpp_TypeInfo_var);
		Array__ctor_m2806(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		Array_t608 * L_7 = V_0;
		NullCheck(L_7);
		Array_push_m2807(L_7, _stringLiteral835, /*hidden argument*/NULL);
		Array_t608 * L_8 = V_0;
		NullCheck(L_8);
		Array_push_m2807(L_8, _stringLiteral239, /*hidden argument*/NULL);
		Array_t608 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = Array_join_m2808(L_9, _stringLiteral99, /*hidden argument*/NULL);
		Component_SendMessage_m2805(__this, _stringLiteral836, L_10, /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// System.Void JSCall::Main()
extern "C" void JSCall_Main_m2803 (JSCall_t607 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
