﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t2785;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18636_gshared (Enumerator_t2790 * __this, Dictionary_2_t2785 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m18636(__this, ___host, method) (( void (*) (Enumerator_t2790 *, Dictionary_2_t2785 *, const MethodInfo*))Enumerator__ctor_m18636_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18637_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18637(__this, method) (( Object_t * (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18637_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18638_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m18638(__this, method) (( void (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m18638_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::Dispose()
extern "C" void Enumerator_Dispose_m18639_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18639(__this, method) (( void (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_Dispose_m18639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18640_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18640(__this, method) (( bool (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_MoveNext_m18640_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m18641_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18641(__this, method) (( Object_t * (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_get_Current_m18641_gshared)(__this, method)
