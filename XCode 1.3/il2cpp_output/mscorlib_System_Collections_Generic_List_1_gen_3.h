﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AchievementTemplate[]
struct AchievementTemplateU5BU5D_t2690;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<AchievementTemplate>
struct  List_1_t104  : public Object_t
{
	// T[] System.Collections.Generic.List`1<AchievementTemplate>::_items
	AchievementTemplateU5BU5D_t2690* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<AchievementTemplate>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<AchievementTemplate>::_version
	int32_t ____version_3;
};
struct List_1_t104_StaticFields{
	// T[] System.Collections.Generic.List`1<AchievementTemplate>::EmptyArray
	AchievementTemplateU5BU5D_t2690* ___EmptyArray_4;
};
