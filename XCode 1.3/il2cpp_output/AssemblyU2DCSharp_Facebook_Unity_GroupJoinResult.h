﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase.h"

// Facebook.Unity.GroupJoinResult
struct  GroupJoinResult_t281  : public ResultBase_t275
{
};
