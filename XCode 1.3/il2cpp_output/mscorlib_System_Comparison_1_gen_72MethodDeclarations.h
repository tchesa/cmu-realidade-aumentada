﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t3553;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m29753_gshared (Comparison_1_t3553 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m29753(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3553 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m29753_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m29754_gshared (Comparison_1_t3553 * __this, CustomAttributeTypedArgument_t2101  ___x, CustomAttributeTypedArgument_t2101  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m29754(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3553 *, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101 , const MethodInfo*))Comparison_1_Invoke_m29754_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m29755_gshared (Comparison_1_t3553 * __this, CustomAttributeTypedArgument_t2101  ___x, CustomAttributeTypedArgument_t2101  ___y, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m29755(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3553 *, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101 , AsyncCallback_t12 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m29755_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m29756_gshared (Comparison_1_t3553 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m29756(__this, ___result, method) (( int32_t (*) (Comparison_1_t3553 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m29756_gshared)(__this, ___result, method)
