﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
#define Dictionary_2__ctor_m2350(__this, method) (( void (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2__ctor_m16020_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m18055(__this, ___comparer, method) (( void (*) (Dictionary_2_t288 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16022_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2409(__this, ___capacity, method) (( void (*) (Dictionary_2_t288 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16024_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m18056(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t288 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m16026_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m18057(__this, method) (( Object_t * (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16028_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m18058(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t288 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16030_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m18059(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t288 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16032_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m18060(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t288 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16034_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m18061(__this, ___key, method) (( bool (*) (Dictionary_2_t288 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16036_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m18062(__this, ___key, method) (( void (*) (Dictionary_2_t288 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16038_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18063(__this, method) (( bool (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16040_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18064(__this, method) (( Object_t * (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16042_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18065(__this, method) (( bool (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16044_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18066(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t288 *, KeyValuePair_2_t287 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16046_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18067(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t288 *, KeyValuePair_2_t287 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16048_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18068(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t288 *, KeyValuePair_2U5BU5D_t3643*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16050_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18069(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t288 *, KeyValuePair_2_t287 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16052_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m18070(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t288 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16054_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18071(__this, method) (( Object_t * (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16056_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18072(__this, method) (( Object_t* (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16058_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18073(__this, method) (( Object_t * (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16060_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count()
#define Dictionary_2_get_Count_m18074(__this, method) (( int32_t (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_get_Count_m16062_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(TKey)
#define Dictionary_2_get_Item_m18075(__this, ___key, method) (( String_t* (*) (Dictionary_2_t288 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m16064_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m18076(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t288 *, String_t*, String_t*, const MethodInfo*))Dictionary_2_set_Item_m16066_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m18077(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t288 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16068_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m18078(__this, ___size, method) (( void (*) (Dictionary_2_t288 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16070_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m18079(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t288 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16072_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m18080(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t287  (*) (Object_t * /* static, unused */, String_t*, String_t*, const MethodInfo*))Dictionary_2_make_pair_m16074_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.String>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m18081(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, String_t*, const MethodInfo*))Dictionary_2_pick_key_m16076_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.String>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m18082(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, String_t*, const MethodInfo*))Dictionary_2_pick_value_m16078_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m18083(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t288 *, KeyValuePair_2U5BU5D_t3643*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16080_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Resize()
#define Dictionary_2_Resize_m18084(__this, method) (( void (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_Resize_m16082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(TKey,TValue)
#define Dictionary_2_Add_m18085(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t288 *, String_t*, String_t*, const MethodInfo*))Dictionary_2_Add_m16084_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Clear()
#define Dictionary_2_Clear_m18086(__this, method) (( void (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_Clear_m16086_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m18087(__this, ___key, method) (( bool (*) (Dictionary_2_t288 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m16088_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m18088(__this, ___value, method) (( bool (*) (Dictionary_2_t288 *, String_t*, const MethodInfo*))Dictionary_2_ContainsValue_m16090_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m18089(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t288 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m16092_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m18090(__this, ___sender, method) (( void (*) (Dictionary_2_t288 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16094_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::Remove(TKey)
#define Dictionary_2_Remove_m18091(__this, ___key, method) (( bool (*) (Dictionary_2_t288 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m16096_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m18092(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t288 *, String_t*, String_t**, const MethodInfo*))Dictionary_2_TryGetValue_m16098_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Keys()
#define Dictionary_2_get_Keys_m18093(__this, method) (( KeyCollection_t2758 * (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_get_Keys_m16100_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Values()
#define Dictionary_2_get_Values_m18094(__this, method) (( ValueCollection_t2759 * (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_get_Values_m16102_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.String>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m18095(__this, ___key, method) (( String_t* (*) (Dictionary_2_t288 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16104_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.String>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m18096(__this, ___value, method) (( String_t* (*) (Dictionary_2_t288 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16106_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m18097(__this, ___pair, method) (( bool (*) (Dictionary_2_t288 *, KeyValuePair_2_t287 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16108_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2456(__this, method) (( Enumerator_t545  (*) (Dictionary_2_t288 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16110_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.String>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m18098(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, String_t*, String_t*, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16112_gshared)(__this /* static, unused */, ___key, ___value, method)
