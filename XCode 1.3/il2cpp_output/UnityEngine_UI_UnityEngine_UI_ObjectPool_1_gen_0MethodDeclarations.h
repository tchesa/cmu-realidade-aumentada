﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m4128(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t693 *, UnityAction_1_t694 *, UnityAction_1_t694 *, const MethodInfo*))ObjectPool_1__ctor_m19733_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m20912(__this, method) (( int32_t (*) (ObjectPool_1_t693 *, const MethodInfo*))ObjectPool_1_get_countAll_m19735_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m20913(__this, ___value, method) (( void (*) (ObjectPool_1_t693 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m19737_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m20914(__this, method) (( int32_t (*) (ObjectPool_1_t693 *, const MethodInfo*))ObjectPool_1_get_countActive_m19739_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m20915(__this, method) (( int32_t (*) (ObjectPool_1_t693 *, const MethodInfo*))ObjectPool_1_get_countInactive_m19741_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m4138(__this, method) (( List_1_t724 * (*) (ObjectPool_1_t693 *, const MethodInfo*))ObjectPool_1_Get_m19743_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m4141(__this, ___element, method) (( void (*) (ObjectPool_1_t693 *, List_1_t724 *, const MethodInfo*))ObjectPool_1_Release_m19745_gshared)(__this, ___element, method)
