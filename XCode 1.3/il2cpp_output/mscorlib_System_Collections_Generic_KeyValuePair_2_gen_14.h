﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GameCenterPlayerTemplate
struct GameCenterPlayerTemplate_t107;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.String,GameCenterPlayerTemplate>
struct  KeyValuePair_2_t2697 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,GameCenterPlayerTemplate>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,GameCenterPlayerTemplate>::value
	GameCenterPlayerTemplate_t107 * ___value_1;
};
