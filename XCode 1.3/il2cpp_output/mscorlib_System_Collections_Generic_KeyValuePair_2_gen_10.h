﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ComponentTrack
struct ComponentTrack_t66;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.String,ComponentTrack>
struct  KeyValuePair_2_t2644 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,ComponentTrack>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,ComponentTrack>::value
	ComponentTrack_t66 * ___value_1;
};
