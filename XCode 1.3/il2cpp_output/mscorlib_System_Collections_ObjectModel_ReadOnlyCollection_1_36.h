﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<Vuforia.ICloudRecoEventHandler>
struct IList_1_t3035;

#include "mscorlib_System_Object.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ICloudRecoEventHandler>
struct  ReadOnlyCollection_1_t3034  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ICloudRecoEventHandler>::list
	Object_t* ___list_0;
};
