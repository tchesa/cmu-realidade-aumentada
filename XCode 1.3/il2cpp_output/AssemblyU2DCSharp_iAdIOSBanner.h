﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,iAdBanner>
struct Dictionary_2_t177;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"

// iAdIOSBanner
struct  iAdIOSBanner_t176  : public MonoBehaviour_t18
{
	// UnityEngine.TextAnchor iAdIOSBanner::anchor
	int32_t ___anchor_1;
};
struct iAdIOSBanner_t176_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,iAdBanner> iAdIOSBanner::_registeredBanners
	Dictionary_2_t177 * ____registeredBanners_2;
};
