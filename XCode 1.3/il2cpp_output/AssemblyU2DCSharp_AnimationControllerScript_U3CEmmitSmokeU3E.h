﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// AnimationControllerScript
struct AnimationControllerScript_t364;

#include "mscorlib_System_Object.h"

// AnimationControllerScript/<EmmitSmoke>c__Iterator15
struct  U3CEmmitSmokeU3Ec__Iterator15_t370  : public Object_t
{
	// System.Int32 AnimationControllerScript/<EmmitSmoke>c__Iterator15::$PC
	int32_t ___U24PC_0;
	// System.Object AnimationControllerScript/<EmmitSmoke>c__Iterator15::$current
	Object_t * ___U24current_1;
	// AnimationControllerScript AnimationControllerScript/<EmmitSmoke>c__Iterator15::<>f__this
	AnimationControllerScript_t364 * ___U3CU3Ef__this_2;
};
