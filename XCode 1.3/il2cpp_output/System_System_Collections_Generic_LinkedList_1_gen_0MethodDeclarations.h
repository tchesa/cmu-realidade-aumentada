﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3472;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t554;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3473;
// System.Object[]
struct ObjectU5BU5D_t34;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C" void LinkedList_1__ctor_m29052_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m29052(__this, method) (( void (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1__ctor_m29052_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m29053_gshared (LinkedList_1_t3472 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m29053(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3472 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))LinkedList_1__ctor_m29053_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29054_gshared (LinkedList_1_t3472 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29054(__this, ___value, method) (( void (*) (LinkedList_1_t3472 *, Object_t *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29054_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m29055_gshared (LinkedList_1_t3472 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m29055(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3472 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m29055_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29056_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29056(__this, method) (( Object_t* (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29056_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29057_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29057(__this, method) (( Object_t * (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29057_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29058_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29058(__this, method) (( bool (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29058_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29059_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29059(__this, method) (( bool (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29059_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29060_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29060(__this, method) (( Object_t * (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29060_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m29061_gshared (LinkedList_1_t3472 * __this, LinkedListNode_1_t3473 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m29061(__this, ___node, method) (( void (*) (LinkedList_1_t3472 *, LinkedListNode_1_t3473 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m29061_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C" LinkedListNode_1_t3473 * LinkedList_1_AddLast_m29062_gshared (LinkedList_1_t3472 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m29062(__this, ___value, method) (( LinkedListNode_1_t3473 * (*) (LinkedList_1_t3472 *, Object_t *, const MethodInfo*))LinkedList_1_AddLast_m29062_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C" void LinkedList_1_Clear_m29063_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m29063(__this, method) (( void (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_Clear_m29063_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C" bool LinkedList_1_Contains_m29064_gshared (LinkedList_1_t3472 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m29064(__this, ___value, method) (( bool (*) (LinkedList_1_t3472 *, Object_t *, const MethodInfo*))LinkedList_1_Contains_m29064_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m29065_gshared (LinkedList_1_t3472 * __this, ObjectU5BU5D_t34* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m29065(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3472 *, ObjectU5BU5D_t34*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m29065_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C" LinkedListNode_1_t3473 * LinkedList_1_Find_m29066_gshared (LinkedList_1_t3472 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Find_m29066(__this, ___value, method) (( LinkedListNode_1_t3473 * (*) (LinkedList_1_t3472 *, Object_t *, const MethodInfo*))LinkedList_1_Find_m29066_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3474  LinkedList_1_GetEnumerator_m29067_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m29067(__this, method) (( Enumerator_t3474  (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_GetEnumerator_m29067_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m29068_gshared (LinkedList_1_t3472 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m29068(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3472 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))LinkedList_1_GetObjectData_m29068_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m29069_gshared (LinkedList_1_t3472 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m29069(__this, ___sender, method) (( void (*) (LinkedList_1_t3472 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m29069_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C" bool LinkedList_1_Remove_m29070_gshared (LinkedList_1_t3472 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m29070(__this, ___value, method) (( bool (*) (LinkedList_1_t3472 *, Object_t *, const MethodInfo*))LinkedList_1_Remove_m29070_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m29071_gshared (LinkedList_1_t3472 * __this, LinkedListNode_1_t3473 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m29071(__this, ___node, method) (( void (*) (LinkedList_1_t3472 *, LinkedListNode_1_t3473 *, const MethodInfo*))LinkedList_1_Remove_m29071_gshared)(__this, ___node, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m29072_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m29072(__this, method) (( int32_t (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_get_Count_m29072_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C" LinkedListNode_1_t3473 * LinkedList_1_get_First_m29073_gshared (LinkedList_1_t3472 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m29073(__this, method) (( LinkedListNode_1_t3473 * (*) (LinkedList_1_t3472 *, const MethodInfo*))LinkedList_1_get_First_m29073_gshared)(__this, method)
