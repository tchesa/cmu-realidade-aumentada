﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_75.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27570_gshared (InternalEnumerator_1_t3363 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27570(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3363 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27570_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27571_gshared (InternalEnumerator_1_t3363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27571(__this, method) (( void (*) (InternalEnumerator_1_t3363 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27571_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27572_gshared (InternalEnumerator_1_t3363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27572(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3363 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27572_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27573_gshared (InternalEnumerator_1_t3363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27573(__this, method) (( void (*) (InternalEnumerator_1_t3363 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27573_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27574_gshared (InternalEnumerator_1_t3363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27574(__this, method) (( bool (*) (InternalEnumerator_1_t3363 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27574_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C" GcScoreData_t1245  InternalEnumerator_1_get_Current_m27575_gshared (InternalEnumerator_1_t3363 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27575(__this, method) (( GcScoreData_t1245  (*) (InternalEnumerator_1_t3363 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27575_gshared)(__this, method)
