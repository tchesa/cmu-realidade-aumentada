﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct DefaultComparer_t3563;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void DefaultComparer__ctor_m29918_gshared (DefaultComparer_t3563 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m29918(__this, method) (( void (*) (DefaultComparer_t3563 *, const MethodInfo*))DefaultComparer__ctor_m29918_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m29919_gshared (DefaultComparer_t3563 * __this, CustomAttributeNamedArgument_t2100  ___x, CustomAttributeNamedArgument_t2100  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m29919(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3563 *, CustomAttributeNamedArgument_t2100 , CustomAttributeNamedArgument_t2100 , const MethodInfo*))DefaultComparer_Compare_m29919_gshared)(__this, ___x, ___y, method)
