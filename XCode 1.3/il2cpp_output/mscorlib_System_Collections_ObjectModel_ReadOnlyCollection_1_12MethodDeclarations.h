﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m18403(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2771 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15555_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18404(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2771 *, UrlSchemes_t245 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15556_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18405(__this, method) (( void (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15557_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18406(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2771 *, int32_t, UrlSchemes_t245 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15558_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18407(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2771 *, UrlSchemes_t245 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15559_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18408(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2771 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15560_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18409(__this, ___index, method) (( UrlSchemes_t245 * (*) (ReadOnlyCollection_1_t2771 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15561_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18410(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2771 *, int32_t, UrlSchemes_t245 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15562_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18411(__this, method) (( bool (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15563_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18412(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2771 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15564_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18413(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15565_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m18414(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2771 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15566_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m18415(__this, method) (( void (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15567_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m18416(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2771 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15568_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18417(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2771 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15569_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m18418(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2771 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15570_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m18419(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2771 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15571_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18420(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2771 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15572_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18421(__this, method) (( bool (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15573_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18422(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15574_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18423(__this, method) (( bool (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15575_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18424(__this, method) (( bool (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15576_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m18425(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2771 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15577_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m18426(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2771 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15578_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::Contains(T)
#define ReadOnlyCollection_1_Contains_m18427(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2771 *, UrlSchemes_t245 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15579_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m18428(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2771 *, UrlSchemesU5BU5D_t2770*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15580_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m18429(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m18430(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2771 *, UrlSchemes_t245 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15582_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::get_Count()
#define ReadOnlyCollection_1_get_Count_m18431(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2771 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15583_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Facebook.Unity.FacebookSettings/UrlSchemes>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m18432(__this, ___index, method) (( UrlSchemes_t245 * (*) (ReadOnlyCollection_1_t2771 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15584_gshared)(__this, ___index, method)
