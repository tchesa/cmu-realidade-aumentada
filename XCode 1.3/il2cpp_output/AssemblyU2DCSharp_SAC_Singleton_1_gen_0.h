﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// SAC_Singleton`1<System.Object>
struct  SAC_Singleton_1_t2756  : public EventDispatcher_t69
{
};
struct SAC_Singleton_1_t2756_StaticFields{
	// T SAC_Singleton`1<System.Object>::_instance
	Object_t * ____instance_3;
};
