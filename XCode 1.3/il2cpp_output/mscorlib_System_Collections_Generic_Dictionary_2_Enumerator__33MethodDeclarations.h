﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m24377(__this, ___dictionary, method) (( void (*) (Enumerator_t3169 *, Dictionary_2_t1010 *, const MethodInfo*))Enumerator__ctor_m24271_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24378(__this, method) (( Object_t * (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24272_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m24379(__this, method) (( void (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24273_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24380(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24274_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24381(__this, method) (( Object_t * (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24275_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24382(__this, method) (( Object_t * (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24276_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::MoveNext()
#define Enumerator_MoveNext_m24383(__this, method) (( bool (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_MoveNext_m24277_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_Current()
#define Enumerator_get_Current_m24384(__this, method) (( KeyValuePair_2_t3166  (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_get_Current_m24278_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24385(__this, method) (( Type_t * (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_get_CurrentKey_m24279_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24386(__this, method) (( uint16_t (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_get_CurrentValue_m24280_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::Reset()
#define Enumerator_Reset_m24387(__this, method) (( void (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_Reset_m24281_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyState()
#define Enumerator_VerifyState_m24388(__this, method) (( void (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_VerifyState_m24282_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24389(__this, method) (( void (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_VerifyCurrent_m24283_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::Dispose()
#define Enumerator_Dispose_m24390(__this, method) (( void (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_Dispose_m24284_gshared)(__this, method)
