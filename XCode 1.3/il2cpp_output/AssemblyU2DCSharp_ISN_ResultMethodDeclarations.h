﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_Result::.ctor(System.Boolean)
extern "C" void ISN_Result__ctor_m933 (ISN_Result_t114 * __this, bool ___IsResultSucceeded, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ISN_Result::get_IsSucceeded()
extern "C" bool ISN_Result_get_IsSucceeded_m934 (ISN_Result_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ISN_Result::get_IsFailed()
extern "C" bool ISN_Result_get_IsFailed_m935 (ISN_Result_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
