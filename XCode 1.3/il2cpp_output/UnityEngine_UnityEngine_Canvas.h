﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t841;

#include "UnityEngine_UnityEngine_Behaviour.h"

// UnityEngine.Canvas
struct  Canvas_t690  : public Behaviour_t875
{
};
struct Canvas_t690_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t841 * ___willRenderCanvases_1;
};
