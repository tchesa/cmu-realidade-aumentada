﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_127.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29606_gshared (InternalEnumerator_1_t3544 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29606(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3544 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29606_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29607_gshared (InternalEnumerator_1_t3544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29607(__this, method) (( void (*) (InternalEnumerator_1_t3544 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29607_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29608_gshared (InternalEnumerator_1_t3544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29608(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3544 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29608_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29609_gshared (InternalEnumerator_1_t3544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29609(__this, method) (( void (*) (InternalEnumerator_1_t3544 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29609_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29610_gshared (InternalEnumerator_1_t3544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29610(__this, method) (( bool (*) (InternalEnumerator_1_t3544 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29610_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C" CustomAttributeNamedArgument_t2100  InternalEnumerator_1_get_Current_m29611_gshared (InternalEnumerator_1_t3544 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29611(__this, method) (( CustomAttributeNamedArgument_t2100  (*) (InternalEnumerator_1_t3544 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29611_gshared)(__this, method)
