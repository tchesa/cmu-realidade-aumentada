﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<System.DateTime>
struct Action_1_t152;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Action`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m2255_gshared (Action_1_t152 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m2255(__this, ___object, ___method, method) (( void (*) (Action_1_t152 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2255_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<System.DateTime>::Invoke(T)
extern "C" void Action_1_Invoke_m2258_gshared (Action_1_t152 * __this, DateTime_t220  ___obj, const MethodInfo* method);
#define Action_1_Invoke_m2258(__this, ___obj, method) (( void (*) (Action_1_t152 *, DateTime_t220 , const MethodInfo*))Action_1_Invoke_m2258_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<System.DateTime>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m17638_gshared (Action_1_t152 * __this, DateTime_t220  ___obj, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m17638(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t152 *, DateTime_t220 , AsyncCallback_t12 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m17638_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m17639_gshared (Action_1_t152 * __this, Object_t * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m17639(__this, ___result, method) (( void (*) (Action_1_t152 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m17639_gshared)(__this, ___result, method)
