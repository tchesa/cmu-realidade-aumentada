﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.Prop[]
struct PropU5BU5D_t3216;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<Vuforia.Prop>
struct  List_1_t1108  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Prop>::_items
	PropU5BU5D_t3216* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::_version
	int32_t ____version_3;
};
struct List_1_t1108_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.Prop>::EmptyArray
	PropU5BU5D_t3216* ___EmptyArray_4;
};
