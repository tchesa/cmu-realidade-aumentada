﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnionAssets.FLE.EventHandlerFunction[]
struct EventHandlerFunctionU5BU5D_t2651;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>
struct  List_1_t460  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>::_items
	EventHandlerFunctionU5BU5D_t2651* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>::_version
	int32_t ____version_3;
};
struct List_1_t460_StaticFields{
	// T[] System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>::EmptyArray
	EventHandlerFunctionU5BU5D_t2651* ___EmptyArray_4;
};
