﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_3MethodDeclarations.h"

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m8148(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1462 *, Object_t53 *, MethodInfo_t *, String_t*, const MethodInfo*))CachedInvokableCall_1__ctor_m28152_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m28164(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1462 *, ObjectU5BU5D_t34*, const MethodInfo*))CachedInvokableCall_1_Invoke_m28153_gshared)(__this, ___args, method)
