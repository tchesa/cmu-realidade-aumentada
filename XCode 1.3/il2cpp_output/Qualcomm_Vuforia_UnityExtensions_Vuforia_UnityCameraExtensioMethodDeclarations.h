﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Camera
struct Camera_t510;

#include "codegen/il2cpp-codegen.h"

// System.Int32 Vuforia.UnityCameraExtensions::GetPixelHeightInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelHeightInt_m4530 (Object_t * __this /* static, unused */, Camera_t510 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.UnityCameraExtensions::GetPixelWidthInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelWidthInt_m4531 (Object_t * __this /* static, unused */, Camera_t510 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMaxDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMaxDepthForVideoBackground_m4532 (Object_t * __this /* static, unused */, Camera_t510 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMinDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMinDepthForVideoBackground_m4533 (Object_t * __this /* static, unused */, Camera_t510 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
