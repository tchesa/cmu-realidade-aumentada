﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_41.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22404_gshared (InternalEnumerator_1_t3038 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22404(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3038 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22404_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22405_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22405(__this, method) (( void (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22405_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22406_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22406(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22406_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22407_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22407(__this, method) (( void (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22407_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22408_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22408(__this, method) (( bool (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22408_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t1050  InternalEnumerator_1_get_Current_m22409_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22409(__this, method) (( TargetSearchResult_t1050  (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22409_gshared)(__this, method)
