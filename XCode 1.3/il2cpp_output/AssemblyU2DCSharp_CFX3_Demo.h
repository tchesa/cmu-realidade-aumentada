﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Renderer
struct Renderer_t46;
// UnityEngine.Collider
struct Collider_t316;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t311;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t198;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// CFX3_Demo
struct  CFX3_Demo_t314  : public MonoBehaviour_t18
{
	// System.Boolean CFX3_Demo::orderedSpawns
	bool ___orderedSpawns_1;
	// System.Single CFX3_Demo::step
	float ___step_2;
	// System.Single CFX3_Demo::range
	float ___range_3;
	// System.Single CFX3_Demo::order
	float ___order_4;
	// UnityEngine.Renderer CFX3_Demo::groundRenderer
	Renderer_t46 * ___groundRenderer_5;
	// UnityEngine.Collider CFX3_Demo::groundCollider
	Collider_t316 * ___groundCollider_6;
	// UnityEngine.GameObject[] CFX3_Demo::ParticleExamples
	GameObjectU5BU5D_t311* ___ParticleExamples_7;
	// System.Int32 CFX3_Demo::exampleIndex
	int32_t ___exampleIndex_8;
	// System.String CFX3_Demo::randomSpawnsDelay
	String_t* ___randomSpawnsDelay_9;
	// System.Boolean CFX3_Demo::randomSpawns
	bool ___randomSpawns_10;
	// System.Boolean CFX3_Demo::slowMo
	bool ___slowMo_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CFX3_Demo::onScreenParticles
	List_1_t198 * ___onScreenParticles_12;
};
