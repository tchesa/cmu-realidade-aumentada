﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m22174(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3025 *, Camera_t510 *, VideoBackgroundAbstractBehaviour_t445 *, const MethodInfo*))KeyValuePair_2__ctor_m16119_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m22175(__this, method) (( Camera_t510 * (*) (KeyValuePair_2_t3025 *, const MethodInfo*))KeyValuePair_2_get_Key_m16120_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22176(__this, ___value, method) (( void (*) (KeyValuePair_2_t3025 *, Camera_t510 *, const MethodInfo*))KeyValuePair_2_set_Key_m16121_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m22177(__this, method) (( VideoBackgroundAbstractBehaviour_t445 * (*) (KeyValuePair_2_t3025 *, const MethodInfo*))KeyValuePair_2_get_Value_m16122_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22178(__this, ___value, method) (( void (*) (KeyValuePair_2_t3025 *, VideoBackgroundAbstractBehaviour_t445 *, const MethodInfo*))KeyValuePair_2_set_Value_m16123_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m22179(__this, method) (( String_t* (*) (KeyValuePair_2_t3025 *, const MethodInfo*))KeyValuePair_2_ToString_m16124_gshared)(__this, method)
