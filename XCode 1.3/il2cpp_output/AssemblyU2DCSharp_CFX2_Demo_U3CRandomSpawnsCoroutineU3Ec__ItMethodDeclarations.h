﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7
struct U3CRandomSpawnsCoroutineU3Ec__Iterator7_t309;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::.ctor()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__Iterator7__ctor_m1700 (U3CRandomSpawnsCoroutineU3Ec__Iterator7_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CRandomSpawnsCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1701 (U3CRandomSpawnsCoroutineU3Ec__Iterator7_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CRandomSpawnsCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1702 (U3CRandomSpawnsCoroutineU3Ec__Iterator7_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::MoveNext()
extern "C" bool U3CRandomSpawnsCoroutineU3Ec__Iterator7_MoveNext_m1703 (U3CRandomSpawnsCoroutineU3Ec__Iterator7_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::Dispose()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__Iterator7_Dispose_m1704 (U3CRandomSpawnsCoroutineU3Ec__Iterator7_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::Reset()
extern "C" void U3CRandomSpawnsCoroutineU3Ec__Iterator7_Reset_m1705 (U3CRandomSpawnsCoroutineU3Ec__Iterator7_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
