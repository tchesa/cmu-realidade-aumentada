﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Renderer.h"

// UnityEngine.SkinnedMeshRenderer
struct  SkinnedMeshRenderer_t359  : public Renderer_t46
{
};
