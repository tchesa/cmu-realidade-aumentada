﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5
struct U3CSaveScreenshotU3Ec__Iterator5_t215;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::.ctor()
extern "C" void U3CSaveScreenshotU3Ec__Iterator5__ctor_m1203 (U3CSaveScreenshotU3Ec__Iterator5_t215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSaveScreenshotU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1204 (U3CSaveScreenshotU3Ec__Iterator5_t215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSaveScreenshotU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1205 (U3CSaveScreenshotU3Ec__Iterator5_t215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::MoveNext()
extern "C" bool U3CSaveScreenshotU3Ec__Iterator5_MoveNext_m1206 (U3CSaveScreenshotU3Ec__Iterator5_t215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::Dispose()
extern "C" void U3CSaveScreenshotU3Ec__Iterator5_Dispose_m1207 (U3CSaveScreenshotU3Ec__Iterator5_t215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::Reset()
extern "C" void U3CSaveScreenshotU3Ec__Iterator5_Reset_m1208 (U3CSaveScreenshotU3Ec__Iterator5_t215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
