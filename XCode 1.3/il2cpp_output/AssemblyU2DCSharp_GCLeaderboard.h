﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScoreCollection
struct ScoreCollection_t122;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t28;
// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// GCLeaderboard
struct  GCLeaderboard_t121  : public Object_t
{
	// ScoreCollection GCLeaderboard::SocsialCollection
	ScoreCollection_t122 * ___SocsialCollection_0;
	// ScoreCollection GCLeaderboard::GlobalCollection
	ScoreCollection_t122 * ___GlobalCollection_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> GCLeaderboard::currentPlayerRank
	Dictionary_2_t28 * ___currentPlayerRank_2;
	// System.String GCLeaderboard::_id
	String_t* ____id_3;
};
