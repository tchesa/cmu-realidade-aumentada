﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<AnimationControllerScript/TimeEvent>
struct IList_1_t2841;

#include "mscorlib_System_Object.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<AnimationControllerScript/TimeEvent>
struct  ReadOnlyCollection_1_t2840  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AnimationControllerScript/TimeEvent>::list
	Object_t* ___list_0;
};
