﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_Error
struct ISN_Error_t85;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_Error::.ctor()
extern "C" void ISN_Error__ctor_m548 (ISN_Error_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
