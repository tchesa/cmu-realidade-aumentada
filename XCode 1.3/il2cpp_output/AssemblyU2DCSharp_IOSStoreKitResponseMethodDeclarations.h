﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSStoreKitResponse
struct IOSStoreKitResponse_t137;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSStoreKitResponse::.ctor()
extern "C" void IOSStoreKitResponse__ctor_m782 (IOSStoreKitResponse_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
