﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ArUiController/<ShareAfterLogin>c__Iterator16
struct U3CShareAfterLoginU3Ec__Iterator16_t373;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ArUiController/<ShareAfterLogin>c__Iterator16::.ctor()
extern "C" void U3CShareAfterLoginU3Ec__Iterator16__ctor_m1896 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ArUiController/<ShareAfterLogin>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CShareAfterLoginU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1897 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ArUiController/<ShareAfterLogin>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CShareAfterLoginU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m1898 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ArUiController/<ShareAfterLogin>c__Iterator16::MoveNext()
extern "C" bool U3CShareAfterLoginU3Ec__Iterator16_MoveNext_m1899 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController/<ShareAfterLogin>c__Iterator16::Dispose()
extern "C" void U3CShareAfterLoginU3Ec__Iterator16_Dispose_m1900 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController/<ShareAfterLogin>c__Iterator16::Reset()
extern "C" void U3CShareAfterLoginU3Ec__Iterator16_Reset_m1901 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
