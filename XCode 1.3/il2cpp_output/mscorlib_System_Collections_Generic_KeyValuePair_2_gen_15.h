﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GCLeaderboard
struct GCLeaderboard_t121;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>
struct  KeyValuePair_2_t2704 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>::value
	GCLeaderboard_t121 * ___value_1;
};
