﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t1388;
// UnityEngine.GUIStyle
struct GUIStyle_t184;
// UnityEngine.GUIContent
struct GUIContent_t549;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t550;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C" void GUIWordWrapSizer__ctor_m7598 (GUIWordWrapSizer_t1388 * __this, GUIStyle_t184 * ____style, GUIContent_t549 * ____content, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m7599 (GUIWordWrapSizer_t1388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m7600 (GUIWordWrapSizer_t1388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
