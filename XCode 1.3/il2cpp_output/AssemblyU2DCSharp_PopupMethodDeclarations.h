﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Popup
struct Popup_t384;

#include "codegen/il2cpp-codegen.h"

// System.Void Popup::.ctor()
extern "C" void Popup__ctor_m1964 (Popup_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::Start()
extern "C" void Popup_Start_m1965 (Popup_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::Update()
extern "C" void Popup_Update_m1966 (Popup_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::Activate(System.Single)
extern "C" void Popup_Activate_m1967 (Popup_t384 * __this, float ___deactivate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::Deactivate()
extern "C" void Popup_Deactivate_m1968 (Popup_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::SetActivetFalse()
extern "C" void Popup_SetActivetFalse_m1969 (Popup_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
