﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iCloudData
struct iCloudData_t179;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "codegen/il2cpp-codegen.h"

// System.Void iCloudData::.ctor(System.String,System.String)
extern "C" void iCloudData__ctor_m1009 (iCloudData_t179 * __this, String_t* ___k, String_t* ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String iCloudData::get_key()
extern "C" String_t* iCloudData_get_key_m1010 (iCloudData_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String iCloudData::get_stringValue()
extern "C" String_t* iCloudData_get_stringValue_m1011 (iCloudData_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single iCloudData::get_floatValue()
extern "C" float iCloudData_get_floatValue_m1012 (iCloudData_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] iCloudData::get_bytesValue()
extern "C" ByteU5BU5D_t119* iCloudData_get_bytesValue_m1013 (iCloudData_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iCloudData::get_IsEmpty()
extern "C" bool iCloudData_get_IsEmpty_m1014 (iCloudData_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
