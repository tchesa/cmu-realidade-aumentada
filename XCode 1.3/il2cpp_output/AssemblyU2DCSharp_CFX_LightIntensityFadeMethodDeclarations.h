﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_LightIntensityFade
struct CFX_LightIntensityFade_t329;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_LightIntensityFade::.ctor()
extern "C" void CFX_LightIntensityFade__ctor_m1779 (CFX_LightIntensityFade_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_LightIntensityFade::Start()
extern "C" void CFX_LightIntensityFade_Start_m1780 (CFX_LightIntensityFade_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_LightIntensityFade::OnEnable()
extern "C" void CFX_LightIntensityFade_OnEnable_m1781 (CFX_LightIntensityFade_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_LightIntensityFade::Update()
extern "C" void CFX_LightIntensityFade_Update_m1782 (CFX_LightIntensityFade_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
