﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct DefaultComparer_t3268;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor()
extern "C" void DefaultComparer__ctor_m26207_gshared (DefaultComparer_t3268 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m26207(__this, method) (( void (*) (DefaultComparer_t3268 *, const MethodInfo*))DefaultComparer__ctor_m26207_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26208_gshared (DefaultComparer_t3268 * __this, VirtualButtonData_t970  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m26208(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3268 *, VirtualButtonData_t970 , const MethodInfo*))DefaultComparer_GetHashCode_m26208_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26209_gshared (DefaultComparer_t3268 * __this, VirtualButtonData_t970  ___x, VirtualButtonData_t970  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m26209(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3268 *, VirtualButtonData_t970 , VirtualButtonData_t970 , const MethodInfo*))DefaultComparer_Equals_m26209_gshared)(__this, ___x, ___y, method)
