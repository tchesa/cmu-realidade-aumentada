﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
struct NullBehaviourComponentFactory_t927;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t425;
// UnityEngine.GameObject
struct GameObject_t27;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t449;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t440;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t415;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t423;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t427;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t401;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t457;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t438;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t429;

#include "codegen/il2cpp-codegen.h"

// Vuforia.MaskOutAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t425 * NullBehaviourComponentFactory_AddMaskOutBehaviour_m4805 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t449 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m4806 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t440 * NullBehaviourComponentFactory_AddTurnOffBehaviour_m4807 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t415 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m4808 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t423 * NullBehaviourComponentFactory_AddMarkerBehaviour_m4809 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t427 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m4810 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t401 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m4811 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t457 * NullBehaviourComponentFactory_AddWordBehaviour_m4812 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t438 * NullBehaviourComponentFactory_AddTextRecoBehaviour_m4813 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t429 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m4814 (NullBehaviourComponentFactory_t927 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
extern "C" void NullBehaviourComponentFactory__ctor_m4815 (NullBehaviourComponentFactory_t927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
