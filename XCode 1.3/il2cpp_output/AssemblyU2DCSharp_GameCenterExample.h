﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_BaseIOSFeaturePreview.h"

// GameCenterExample
struct  GameCenterExample_t187  : public BaseIOSFeaturePreview_t183
{
	// System.Int32 GameCenterExample::hiScore
	int32_t ___hiScore_11;
	// System.String GameCenterExample::leaderBoardId
	String_t* ___leaderBoardId_12;
	// System.String GameCenterExample::leaderBoardId2
	String_t* ___leaderBoardId2_13;
	// System.String GameCenterExample::TEST_ACHIEVEMENT_1_ID
	String_t* ___TEST_ACHIEVEMENT_1_ID_14;
	// System.String GameCenterExample::TEST_ACHIEVEMENT_2_ID
	String_t* ___TEST_ACHIEVEMENT_2_ID_15;
};
struct GameCenterExample_t187_StaticFields{
	// System.Boolean GameCenterExample::IsInitialized
	bool ___IsInitialized_16;
	// System.Int64 GameCenterExample::LB2BestScores
	int64_t ___LB2BestScores_17;
};
