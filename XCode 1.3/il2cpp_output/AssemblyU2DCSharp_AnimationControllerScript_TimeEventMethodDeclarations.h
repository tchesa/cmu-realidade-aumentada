﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript/TimeEvent
struct TimeEvent_t362;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript/TimeEvent::.ctor(System.Single,System.String)
extern "C" void TimeEvent__ctor_m1839 (TimeEvent_t362 * __this, float ___timer, String_t* ___function, const MethodInfo* method) IL2CPP_METHOD_ATTR;
