﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<ISN_Result>
struct Action_1_t99;
// System.Action`1<ISN_PlayerScoreLoadedResult>
struct Action_1_t100;
// System.Action`1<ISN_AchievementProgressResult>
struct Action_1_t101;
// System.Action
struct Action_t81;
// System.Action`1<ISN_UserInfoLoadResult>
struct Action_1_t102;
// System.Action`1<ISN_PlayerSignatureResult>
struct Action_1_t103;
// System.Collections.Generic.List`1<AchievementTemplate>
struct List_1_t104;
// UnionAssets.FLE.EventDispatcherBase
struct EventDispatcherBase_t72;
// System.Collections.Generic.Dictionary`2<System.String,GCLeaderboard>
struct Dictionary_2_t105;
// System.Collections.Generic.Dictionary`2<System.String,GameCenterPlayerTemplate>
struct Dictionary_2_t106;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;
// GameCenterPlayerTemplate
struct GameCenterPlayerTemplate_t107;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// GameCenterManager
struct  GameCenterManager_t98  : public MonoBehaviour_t18
{
};
struct GameCenterManager_t98_StaticFields{
	// System.Action`1<ISN_Result> GameCenterManager::OnAuthFinished
	Action_1_t99 * ___OnAuthFinished_12;
	// System.Action`1<ISN_Result> GameCenterManager::OnScoreSubmitted
	Action_1_t99 * ___OnScoreSubmitted_13;
	// System.Action`1<ISN_PlayerScoreLoadedResult> GameCenterManager::OnPlayerScoreLoaded
	Action_1_t100 * ___OnPlayerScoreLoaded_14;
	// System.Action`1<ISN_Result> GameCenterManager::OnScoresListLoaded
	Action_1_t99 * ___OnScoresListLoaded_15;
	// System.Action`1<ISN_Result> GameCenterManager::OnAchievementsReset
	Action_1_t99 * ___OnAchievementsReset_16;
	// System.Action`1<ISN_Result> GameCenterManager::OnAchievementsLoaded
	Action_1_t99 * ___OnAchievementsLoaded_17;
	// System.Action`1<ISN_AchievementProgressResult> GameCenterManager::OnAchievementsProgress
	Action_1_t101 * ___OnAchievementsProgress_18;
	// System.Action GameCenterManager::OnGameCenterViewDismissedAction
	Action_t81 * ___OnGameCenterViewDismissedAction_19;
	// System.Action`1<ISN_Result> GameCenterManager::OnFriendsListLoaded
	Action_1_t99 * ___OnFriendsListLoaded_20;
	// System.Action`1<ISN_UserInfoLoadResult> GameCenterManager::OnUserInfoLoaded
	Action_1_t102 * ___OnUserInfoLoaded_21;
	// System.Action`1<ISN_PlayerSignatureResult> GameCenterManager::OnPlayerSignatureRetrieveResult
	Action_1_t103 * ___OnPlayerSignatureRetrieveResult_22;
	// System.Boolean GameCenterManager::_IsInitialized
	bool ____IsInitialized_23;
	// System.Boolean GameCenterManager::_IsPlayerAuthenticated
	bool ____IsPlayerAuthenticated_24;
	// System.Boolean GameCenterManager::_IsAchievementsInfoLoaded
	bool ____IsAchievementsInfoLoaded_25;
	// System.Collections.Generic.List`1<AchievementTemplate> GameCenterManager::_achievements
	List_1_t104 * ____achievements_26;
	// UnionAssets.FLE.EventDispatcherBase GameCenterManager::_dispatcher
	EventDispatcherBase_t72 * ____dispatcher_27;
	// System.Collections.Generic.Dictionary`2<System.String,GCLeaderboard> GameCenterManager::_leaderboards
	Dictionary_2_t105 * ____leaderboards_28;
	// System.Collections.Generic.Dictionary`2<System.String,GameCenterPlayerTemplate> GameCenterManager::_players
	Dictionary_2_t106 * ____players_29;
	// System.Collections.Generic.List`1<System.String> GameCenterManager::_friendsList
	List_1_t78 * ____friendsList_30;
	// GameCenterPlayerTemplate GameCenterManager::_player
	GameCenterPlayerTemplate_t107 * ____player_31;
	// System.Action`1<ISN_Result> GameCenterManager::<>f__am$cache14
	Action_1_t99 * ___U3CU3Ef__amU24cache14_32;
	// System.Action`1<ISN_Result> GameCenterManager::<>f__am$cache15
	Action_1_t99 * ___U3CU3Ef__amU24cache15_33;
	// System.Action`1<ISN_PlayerScoreLoadedResult> GameCenterManager::<>f__am$cache16
	Action_1_t100 * ___U3CU3Ef__amU24cache16_34;
	// System.Action`1<ISN_Result> GameCenterManager::<>f__am$cache17
	Action_1_t99 * ___U3CU3Ef__amU24cache17_35;
	// System.Action`1<ISN_Result> GameCenterManager::<>f__am$cache18
	Action_1_t99 * ___U3CU3Ef__amU24cache18_36;
	// System.Action`1<ISN_Result> GameCenterManager::<>f__am$cache19
	Action_1_t99 * ___U3CU3Ef__amU24cache19_37;
	// System.Action`1<ISN_AchievementProgressResult> GameCenterManager::<>f__am$cache1A
	Action_1_t101 * ___U3CU3Ef__amU24cache1A_38;
	// System.Action GameCenterManager::<>f__am$cache1B
	Action_t81 * ___U3CU3Ef__amU24cache1B_39;
	// System.Action`1<ISN_Result> GameCenterManager::<>f__am$cache1C
	Action_1_t99 * ___U3CU3Ef__amU24cache1C_40;
	// System.Action`1<ISN_UserInfoLoadResult> GameCenterManager::<>f__am$cache1D
	Action_1_t102 * ___U3CU3Ef__amU24cache1D_41;
	// System.Action`1<ISN_PlayerSignatureResult> GameCenterManager::<>f__am$cache1E
	Action_1_t103 * ___U3CU3Ef__amU24cache1E_42;
};
