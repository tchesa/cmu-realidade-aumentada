﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_11MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m25787(__this, ___host, method) (( void (*) (Enumerator_t3230 *, Dictionary_2_t1036 *, const MethodInfo*))Enumerator__ctor_m16413_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25788(__this, method) (( Object_t * (*) (Enumerator_t3230 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16414_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m25789(__this, method) (( void (*) (Enumerator_t3230 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16415_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::Dispose()
#define Enumerator_Dispose_m25790(__this, method) (( void (*) (Enumerator_t3230 *, const MethodInfo*))Enumerator_Dispose_m16416_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::MoveNext()
#define Enumerator_MoveNext_m25791(__this, method) (( bool (*) (Enumerator_t3230 *, const MethodInfo*))Enumerator_MoveNext_m16417_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::get_Current()
#define Enumerator_get_Current_m25792(__this, method) (( int32_t (*) (Enumerator_t3230 *, const MethodInfo*))Enumerator_get_Current_m16418_gshared)(__this, method)
