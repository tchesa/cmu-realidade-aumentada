﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Terrain
struct Terrain_t1334;
// UnityEngine.TerrainData
struct TerrainData_t1331;
// UnityEngine.Material
struct Material_t45;
// UnityEngine.Terrain[]
struct TerrainU5BU5D_t1444;
// UnityEngine.GameObject
struct GameObject_t27;
// UnityEngine.Object
struct Object_t53;
struct Object_t53_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t688;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t872;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t8;
// UnityEngine.Camera
struct Camera_t510;
// UnityEngine.Transform
struct Transform_t25;
// UnityEngine.Canvas
struct Canvas_t690;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t841;
// UnityEngine.CanvasGroup
struct CanvasGroup_t579;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t689;
// UnityEngine.Texture
struct Texture_t731;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t724;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t722;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1339;
// UnityEngine.AssetBundle
struct AssetBundle_t1342;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t1341;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.String[]
struct StringU5BU5D_t260;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t37;
struct WaitForSeconds_t37_marshaled;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t1350;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t500;
// UnityEngine.Coroutine
struct Coroutine_t39;
struct Coroutine_t39_marshaled;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t1351;
// UnityEngine.RequireComponent
struct RequireComponent_t1352;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t1353;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t1354;
// UnityEngine.HideInInspector
struct HideInInspector_t1355;
// UnityEngine.ScriptableObject
struct ScriptableObject_t77;
struct ScriptableObject_t77_marshaled;
// UnityEngine.ResourceRequest
struct ResourceRequest_t1356;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1340;
struct AsyncOperation_t1340_marshaled;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t1358;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t1267;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t1433;
// UnityEngine.MeshFilter
struct MeshFilter_t597;
// UnityEngine.Mesh
struct Mesh_t601;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t21;
// System.Int32[]
struct Int32U5BU5D_t335;
// UnityEngine.Renderer
struct Renderer_t46;
// UnityEngine.Material[]
struct MaterialU5BU5D_t49;
// UnityEngine.Texture2D
struct Texture2D_t33;
// UnityEngine.Color[]
struct ColorU5BU5D_t52;
// UnityEngine.Color32[]
struct Color32U5BU5D_t945;
// System.Byte[]
struct ByteU5BU5D_t119;
// UnityEngine.RenderTexture
struct RenderTexture_t582;
// UnityEngine.GUITexture
struct GUITexture_t43;
// UnityEngine.GUIText
struct GUIText_t44;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t1374;
// System.Action`1<UnityEngine.Font>
struct Action_1_t843;
// UnityEngine.Font
struct Font_t684;
// UnityEngine.GUILayer
struct GUILayer_t1375;
// UnityEngine.GUIElement
struct GUIElement_t1370;
// UnityEngine.Gradient
struct Gradient_t1378;
struct Gradient_t1378_marshaled;
// UnityEngine.GUI/ScrollViewState
struct ScrollViewState_t1380;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t548;
// UnityEngine.GUISkin
struct GUISkin_t547;
// UnityEngine.GUIStyle
struct GUIStyle_t184;
// UnityEngine.GUIContent
struct GUIContent_t549;
// UnityEngine.TextEditor
struct TextEditor_t855;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t550;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t553;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t1382;
// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_t1383;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t1385;
// UnityEngine.RectOffset
struct RectOffset_t780;
// UnityEngine.GUIScrollGroup
struct GUIScrollGroup_t1387;
// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t1388;
// System.Exception
struct Exception_t40;
// UnityEngine.GUISettings
struct GUISettings_t1394;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t1395;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t1396;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// UnityEngine.GUIStyleState
struct GUIStyleState_t504;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_Terrain.h"
#include "UnityEngine_UnityEngine_TerrainMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TerrainData.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "UnityEngine_UnityEngine_TerrainRenderFlags.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_TreeInstance.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_TerrainChangedFlags.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TerrainColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TerrainCollider.h"
#include "mscorlib_System_String.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_66MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TerrainDataMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_66.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"
#include "UnityEngine_UnityEngine_DrivenTransformPropertiesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
#include "UnityEngine_UnityEngine_RectTransform_EdgeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"
#include "UnityEngine_UnityEngine_RectTransform_AxisMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_Canvas.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_Plane.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_RenderModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup.h"
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Byte.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23.h"
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23MethodDeclarations.h"
#include "mscorlib_System_UInt16.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation.h"
#include "UnityEngine_UnityEngine_AssetBundle.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest.h"
#include "UnityEngine_UnityEngine_AssetBundleRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ReferenceData.h"
#include "UnityEngine_UnityEngine_ReferenceDataMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "UnityEngine_UnityEngine_HideFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_SendMessageOptionsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PrimitiveType.h"
#include "UnityEngine_UnityEngine_PrimitiveTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space.h"
#include "UnityEngine_UnityEngine_SpaceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_RuntimePlatformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType.h"
#include "UnityEngine_UnityEngine_LogTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "UnityEngine_UnityEngine_CoroutineMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_RequireComponent.h"
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HideInInspector.h"
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourceRequest.h"
#include "UnityEngine_UnityEngine_ResourceRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderb.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderbMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_QualitySettings.h"
#include "UnityEngine_UnityEngine_QualitySettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpace.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter.h"
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh.h"
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_BoneWeightMethodDeclarations.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Particle.h"
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TrailRenderer.h"
#include "UnityEngine_UnityEngine_TrailRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArgumentsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Graphics.h"
#include "UnityEngine_UnityEngine_GraphicsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resolution.h"
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpaceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
#include "UnityEngine_UnityEngine_ScreenOrientationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GL.h"
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_MeshRenderer.h"
#include "UnityEngine_UnityEngine_MeshRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "UnityEngine_UnityEngine_FilterMode.h"
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "UnityEngine_UnityEngine_RenderTexture.h"
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"
#include "UnityEngine_UnityEngine_GUIElement.h"
#include "UnityEngine_UnityEngine_GUIElementMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITexture.h"
#include "UnityEngine_UnityEngine_GUITextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIText.h"
#include "UnityEngine_UnityEngine_GUITextMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_23.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Action_1_gen_23MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer.h"
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GradientColorKey.h"
#include "UnityEngine_UnityEngine_GradientColorKeyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GradientAlphaKey.h"
#include "UnityEngine_UnityEngine_GradientAlphaKeyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gradient.h"
#include "UnityEngine_UnityEngine_GradientMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScaleMode.h"
#include "UnityEngine_UnityEngine_ScaleModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewState.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_GUISkin.h"
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FocusType.h"
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UnityEngine_GUIClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache.h"
#include "UnityEngine_UnityEngine_GUILayout.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
#include "mscorlib_System_Collections_Stack.h"
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCacheMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_39MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_39.h"
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_67MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_31MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_31.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_67.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizer.h"
#include "UnityEngine_UnityEngine_RectOffset.h"
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup.h"
#include "UnityEngine_UnityEngine_GUIScrollGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_TypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ExitGUIException.h"
#include "UnityEngine_UnityEngine_ExitGUIExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FocusTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility.h"
#include "UnityEngine_UnityEngine_GUIStateObjectsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIClip.h"
#include "UnityEngine_UnityEngine_GUISettings.h"
#include "UnityEngine_UnityEngine_GUISettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegate.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegateMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_40MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_27MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_26MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_26.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_40.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_27.h"
#include "mscorlib_System_StringComparerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
#include "mscorlib_System_StringComparer.h"
#include "UnityEngine_UnityEngine_GUIStyleState.h"
#include "UnityEngine_UnityEngine_FontStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ImagePosition.h"
#include "UnityEngine_UnityEngine_ImagePositionMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Terrain::.ctor()
extern "C" void Terrain__ctor_m7224 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		__this->___m_TreeDistance_2 = (5000.0f);
		__this->___m_TreeBillboardDistance_3 = (50.0f);
		__this->___m_TreeCrossFadeLength_4 = (5.0f);
		__this->___m_TreeMaximumFullLODCount_5 = ((int32_t)50);
		__this->___m_DetailObjectDistance_6 = (80.0f);
		__this->___m_DetailObjectDensity_7 = (1.0f);
		__this->___m_HeightmapPixelError_8 = (5.0f);
		__this->___m_SplatMapDistance_9 = (1000.0f);
		__this->___m_CastShadows_11 = 1;
		__this->___m_LightmapIndex_12 = (-1);
		__this->___m_LightmapSize_13 = ((int32_t)1024);
		__this->___m_DrawTreesAndFoliage_14 = 1;
		__this->___m_CollectDetailPatches_15 = 1;
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr UnityEngine.Terrain::get_InstanceObject()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Terrain_get_InstanceObject_m7225 (Terrain_t1334 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		s_Il2CppMethodIntialized = true;
	}
	{
		Terrain_MakeSureObjectIsAlive_m7228(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = (__this->___m_TerrainInstance_17);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m6518(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0147;
		}
	}
	{
		IntPtr_t L_3 = Terrain_Construct_m7321(__this, /*hidden argument*/NULL);
		__this->___m_TerrainInstance_17 = L_3;
		IntPtr_t L_4 = (__this->___m_TerrainInstance_17);
		TerrainData_t1331 * L_5 = (__this->___m_TerrainData_1);
		Terrain_Internal_SetTerrainData_m7237(__this, L_4, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6 = (__this->___m_TerrainInstance_17);
		float L_7 = (__this->___m_TreeDistance_2);
		Terrain_Internal_SetTreeDistance_m7241(__this, L_6, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8 = (__this->___m_TerrainInstance_17);
		float L_9 = (__this->___m_TreeBillboardDistance_3);
		Terrain_Internal_SetTreeBillboardDistance_m7245(__this, L_8, L_9, /*hidden argument*/NULL);
		IntPtr_t L_10 = (__this->___m_TerrainInstance_17);
		float L_11 = (__this->___m_TreeCrossFadeLength_4);
		Terrain_Internal_SetTreeCrossFadeLength_m7249(__this, L_10, L_11, /*hidden argument*/NULL);
		IntPtr_t L_12 = (__this->___m_TerrainInstance_17);
		int32_t L_13 = (__this->___m_TreeMaximumFullLODCount_5);
		Terrain_Internal_SetTreeMaximumFullLODCount_m7253(__this, L_12, L_13, /*hidden argument*/NULL);
		IntPtr_t L_14 = (__this->___m_TerrainInstance_17);
		float L_15 = (__this->___m_DetailObjectDistance_6);
		Terrain_Internal_SetDetailObjectDistance_m7257(__this, L_14, L_15, /*hidden argument*/NULL);
		IntPtr_t L_16 = (__this->___m_TerrainInstance_17);
		float L_17 = (__this->___m_DetailObjectDensity_7);
		Terrain_Internal_SetDetailObjectDensity_m7261(__this, L_16, L_17, /*hidden argument*/NULL);
		IntPtr_t L_18 = (__this->___m_TerrainInstance_17);
		float L_19 = (__this->___m_HeightmapPixelError_8);
		Terrain_Internal_SetHeightmapPixelError_m7265(__this, L_18, L_19, /*hidden argument*/NULL);
		IntPtr_t L_20 = (__this->___m_TerrainInstance_17);
		float L_21 = (__this->___m_SplatMapDistance_9);
		Terrain_Internal_SetBasemapDistance_m7273(__this, L_20, L_21, /*hidden argument*/NULL);
		IntPtr_t L_22 = (__this->___m_TerrainInstance_17);
		int32_t L_23 = (__this->___m_HeightmapMaximumLOD_10);
		Terrain_Internal_SetHeightmapMaximumLOD_m7269(__this, L_22, L_23, /*hidden argument*/NULL);
		IntPtr_t L_24 = (__this->___m_TerrainInstance_17);
		bool L_25 = (__this->___m_CastShadows_11);
		Terrain_Internal_SetCastShadows_m7289(__this, L_24, L_25, /*hidden argument*/NULL);
		IntPtr_t L_26 = (__this->___m_TerrainInstance_17);
		int32_t L_27 = (__this->___m_LightmapIndex_12);
		Terrain_Internal_SetLightmapIndex_m7281(__this, L_26, L_27, /*hidden argument*/NULL);
		IntPtr_t L_28 = (__this->___m_TerrainInstance_17);
		int32_t L_29 = (__this->___m_LightmapSize_13);
		Terrain_Internal_SetLightmapSize_m7285(__this, L_28, L_29, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___m_TerrainInstance_17);
		bool L_31 = (__this->___m_DrawTreesAndFoliage_14);
		Terrain_Internal_SetDrawTreesAndFoliage_m7297(__this, L_30, L_31, /*hidden argument*/NULL);
		IntPtr_t L_32 = (__this->___m_TerrainInstance_17);
		bool L_33 = (__this->___m_CollectDetailPatches_15);
		Terrain_Internal_SetCollectDetailPatches_m7301(__this, L_32, L_33, /*hidden argument*/NULL);
		IntPtr_t L_34 = (__this->___m_TerrainInstance_17);
		Material_t45 * L_35 = (__this->___m_MaterialTemplate_16);
		Terrain_Internal_SetMaterialTemplate_m7293(__this, L_34, L_35, /*hidden argument*/NULL);
	}

IL_0147:
	{
		IntPtr_t L_36 = (__this->___m_TerrainInstance_17);
		return L_36;
	}
}
// System.Void UnityEngine.Terrain::set_InstanceObject(System.IntPtr)
extern "C" void Terrain_set_InstanceObject_m7226 (Terrain_t1334 * __this, IntPtr_t ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___value;
		__this->___m_TerrainInstance_17 = L_0;
		return;
	}
}
// System.Void UnityEngine.Terrain::OnDestroy()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Terrain_OnDestroy_m7227 (Terrain_t1334 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		s_Il2CppMethodIntialized = true;
	}
	{
		Terrain_OnDisable_m7324(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = (__this->___m_TerrainInstance_17);
		Terrain_Cleanup_m7229(__this, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___m_TerrainInstance_17 = L_1;
		return;
	}
}
// System.Void UnityEngine.Terrain::MakeSureObjectIsAlive()
extern "C" void Terrain_MakeSureObjectIsAlive_m7228 (Terrain_t1334 * __this, const MethodInfo* method)
{
	typedef void (*Terrain_MakeSureObjectIsAlive_m7228_ftn) (Terrain_t1334 *);
	static Terrain_MakeSureObjectIsAlive_m7228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_MakeSureObjectIsAlive_m7228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::MakeSureObjectIsAlive()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Terrain::Cleanup(System.IntPtr)
extern "C" void Terrain_Cleanup_m7229 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef void (*Terrain_Cleanup_m7229_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Cleanup_m7229_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Cleanup_m7229_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Cleanup(System.IntPtr)");
	_il2cpp_icall_func(__this, ___terrainInstance);
}
// UnityEngine.TerrainRenderFlags UnityEngine.Terrain::get_editorRenderFlags()
extern "C" int32_t Terrain_get_editorRenderFlags_m7230 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_1 = Terrain_GetEditorRenderFlags_m7232(__this, L_0, /*hidden argument*/NULL);
		return (int32_t)(L_1);
	}
}
// System.Void UnityEngine.Terrain::set_editorRenderFlags(UnityEngine.TerrainRenderFlags)
extern "C" void Terrain_set_editorRenderFlags_m7231 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___value;
		Terrain_SetEditorRenderFlags_m7233(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Terrain::GetEditorRenderFlags(System.IntPtr)
extern "C" int32_t Terrain_GetEditorRenderFlags_m7232 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef int32_t (*Terrain_GetEditorRenderFlags_m7232_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_GetEditorRenderFlags_m7232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_GetEditorRenderFlags_m7232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::GetEditorRenderFlags(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::SetEditorRenderFlags(System.IntPtr,System.Int32)
extern "C" void Terrain_SetEditorRenderFlags_m7233 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___flags, const MethodInfo* method)
{
	typedef void (*Terrain_SetEditorRenderFlags_m7233_ftn) (Terrain_t1334 *, IntPtr_t, int32_t);
	static Terrain_SetEditorRenderFlags_m7233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_SetEditorRenderFlags_m7233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::SetEditorRenderFlags(System.IntPtr,System.Int32)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___flags);
}
// UnityEngine.TerrainData UnityEngine.Terrain::get_terrainData()
extern "C" TerrainData_t1331 * Terrain_get_terrainData_m7234 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		TerrainData_t1331 * L_0 = (__this->___m_TerrainData_1);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		TerrainData_t1331 * L_2 = Terrain_Internal_GetTerrainData_m7236(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_4 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		TerrainData_t1331 * L_5 = (__this->___m_TerrainData_1);
		Terrain_Internal_SetTerrainData_m7237(__this, L_4, L_5, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TerrainData_t1331 * L_6 = (__this->___m_TerrainData_1);
		return L_6;
	}
}
// System.Void UnityEngine.Terrain::set_terrainData(UnityEngine.TerrainData)
extern "C" void Terrain_set_terrainData_m7235 (Terrain_t1334 * __this, TerrainData_t1331 * ___value, const MethodInfo* method)
{
	{
		TerrainData_t1331 * L_0 = ___value;
		__this->___m_TerrainData_1 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		TerrainData_t1331 * L_2 = ___value;
		Terrain_Internal_SetTerrainData_m7237(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TerrainData UnityEngine.Terrain::Internal_GetTerrainData(System.IntPtr)
extern "C" TerrainData_t1331 * Terrain_Internal_GetTerrainData_m7236 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef TerrainData_t1331 * (*Terrain_Internal_GetTerrainData_m7236_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetTerrainData_m7236_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetTerrainData_m7236_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetTerrainData(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetTerrainData(System.IntPtr,UnityEngine.TerrainData)
extern "C" void Terrain_Internal_SetTerrainData_m7237 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, TerrainData_t1331 * ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetTerrainData_m7237_ftn) (Terrain_t1334 *, IntPtr_t, TerrainData_t1331 *);
	static Terrain_Internal_SetTerrainData_m7237_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetTerrainData_m7237_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetTerrainData(System.IntPtr,UnityEngine.TerrainData)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::get_treeDistance()
extern "C" float Terrain_get_treeDistance_m7238 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_TreeDistance_2);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = Terrain_Internal_GetTreeDistance_m7240(__this, L_1, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___m_TreeDistance_2);
		Terrain_Internal_SetTreeDistance_m7241(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_5 = (__this->___m_TreeDistance_2);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_treeDistance(System.Single)
extern "C" void Terrain_set_treeDistance_m7239 (Terrain_t1334 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_TreeDistance_2 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = ___value;
		Terrain_Internal_SetTreeDistance_m7241(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Terrain::Internal_GetTreeDistance(System.IntPtr)
extern "C" float Terrain_Internal_GetTreeDistance_m7240 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef float (*Terrain_Internal_GetTreeDistance_m7240_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetTreeDistance_m7240_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetTreeDistance_m7240_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetTreeDistance(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetTreeDistance(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetTreeDistance_m7241 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetTreeDistance_m7241_ftn) (Terrain_t1334 *, IntPtr_t, float);
	static Terrain_Internal_SetTreeDistance_m7241_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetTreeDistance_m7241_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetTreeDistance(System.IntPtr,System.Single)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::get_treeBillboardDistance()
extern "C" float Terrain_get_treeBillboardDistance_m7242 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_TreeBillboardDistance_3);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = Terrain_Internal_GetTreeBillboardDistance_m7244(__this, L_1, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___m_TreeBillboardDistance_3);
		Terrain_Internal_SetTreeBillboardDistance_m7245(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_5 = (__this->___m_TreeBillboardDistance_3);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_treeBillboardDistance(System.Single)
extern "C" void Terrain_set_treeBillboardDistance_m7243 (Terrain_t1334 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_TreeBillboardDistance_3 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = ___value;
		Terrain_Internal_SetTreeBillboardDistance_m7245(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Terrain::Internal_GetTreeBillboardDistance(System.IntPtr)
extern "C" float Terrain_Internal_GetTreeBillboardDistance_m7244 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef float (*Terrain_Internal_GetTreeBillboardDistance_m7244_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetTreeBillboardDistance_m7244_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetTreeBillboardDistance_m7244_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetTreeBillboardDistance(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetTreeBillboardDistance(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetTreeBillboardDistance_m7245 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetTreeBillboardDistance_m7245_ftn) (Terrain_t1334 *, IntPtr_t, float);
	static Terrain_Internal_SetTreeBillboardDistance_m7245_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetTreeBillboardDistance_m7245_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetTreeBillboardDistance(System.IntPtr,System.Single)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::get_treeCrossFadeLength()
extern "C" float Terrain_get_treeCrossFadeLength_m7246 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_TreeCrossFadeLength_4);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = Terrain_Internal_GetTreeCrossFadeLength_m7248(__this, L_1, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___m_TreeCrossFadeLength_4);
		Terrain_Internal_SetTreeCrossFadeLength_m7249(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_5 = (__this->___m_TreeCrossFadeLength_4);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_treeCrossFadeLength(System.Single)
extern "C" void Terrain_set_treeCrossFadeLength_m7247 (Terrain_t1334 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_TreeCrossFadeLength_4 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = ___value;
		Terrain_Internal_SetTreeCrossFadeLength_m7249(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Terrain::Internal_GetTreeCrossFadeLength(System.IntPtr)
extern "C" float Terrain_Internal_GetTreeCrossFadeLength_m7248 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef float (*Terrain_Internal_GetTreeCrossFadeLength_m7248_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetTreeCrossFadeLength_m7248_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetTreeCrossFadeLength_m7248_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetTreeCrossFadeLength(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetTreeCrossFadeLength(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetTreeCrossFadeLength_m7249 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetTreeCrossFadeLength_m7249_ftn) (Terrain_t1334 *, IntPtr_t, float);
	static Terrain_Internal_SetTreeCrossFadeLength_m7249_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetTreeCrossFadeLength_m7249_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetTreeCrossFadeLength(System.IntPtr,System.Single)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Int32 UnityEngine.Terrain::get_treeMaximumFullLODCount()
extern "C" int32_t Terrain_get_treeMaximumFullLODCount_m7250 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TreeMaximumFullLODCount_5);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_2 = Terrain_Internal_GetTreeMaximumFullLODCount_m7252(__this, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_TreeMaximumFullLODCount_5);
		Terrain_Internal_SetTreeMaximumFullLODCount_m7253(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_5 = (__this->___m_TreeMaximumFullLODCount_5);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_treeMaximumFullLODCount(System.Int32)
extern "C" void Terrain_set_treeMaximumFullLODCount_m7251 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_TreeMaximumFullLODCount_5 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		Terrain_Internal_SetTreeMaximumFullLODCount_m7253(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Terrain::Internal_GetTreeMaximumFullLODCount(System.IntPtr)
extern "C" int32_t Terrain_Internal_GetTreeMaximumFullLODCount_m7252 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef int32_t (*Terrain_Internal_GetTreeMaximumFullLODCount_m7252_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetTreeMaximumFullLODCount_m7252_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetTreeMaximumFullLODCount_m7252_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetTreeMaximumFullLODCount(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetTreeMaximumFullLODCount(System.IntPtr,System.Int32)
extern "C" void Terrain_Internal_SetTreeMaximumFullLODCount_m7253 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetTreeMaximumFullLODCount_m7253_ftn) (Terrain_t1334 *, IntPtr_t, int32_t);
	static Terrain_Internal_SetTreeMaximumFullLODCount_m7253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetTreeMaximumFullLODCount_m7253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetTreeMaximumFullLODCount(System.IntPtr,System.Int32)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::get_detailObjectDistance()
extern "C" float Terrain_get_detailObjectDistance_m7254 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_DetailObjectDistance_6);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = Terrain_Internal_GetDetailObjectDistance_m7256(__this, L_1, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___m_DetailObjectDistance_6);
		Terrain_Internal_SetDetailObjectDistance_m7257(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_5 = (__this->___m_DetailObjectDistance_6);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_detailObjectDistance(System.Single)
extern "C" void Terrain_set_detailObjectDistance_m7255 (Terrain_t1334 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_DetailObjectDistance_6 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = ___value;
		Terrain_Internal_SetDetailObjectDistance_m7257(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Terrain::Internal_GetDetailObjectDistance(System.IntPtr)
extern "C" float Terrain_Internal_GetDetailObjectDistance_m7256 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef float (*Terrain_Internal_GetDetailObjectDistance_m7256_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetDetailObjectDistance_m7256_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetDetailObjectDistance_m7256_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetDetailObjectDistance(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetDetailObjectDistance(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetDetailObjectDistance_m7257 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetDetailObjectDistance_m7257_ftn) (Terrain_t1334 *, IntPtr_t, float);
	static Terrain_Internal_SetDetailObjectDistance_m7257_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetDetailObjectDistance_m7257_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetDetailObjectDistance(System.IntPtr,System.Single)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::get_detailObjectDensity()
extern "C" float Terrain_get_detailObjectDensity_m7258 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_DetailObjectDensity_7);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = Terrain_Internal_GetDetailObjectDensity_m7260(__this, L_1, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___m_DetailObjectDensity_7);
		Terrain_Internal_SetDetailObjectDensity_m7261(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_5 = (__this->___m_DetailObjectDensity_7);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_detailObjectDensity(System.Single)
extern "C" void Terrain_set_detailObjectDensity_m7259 (Terrain_t1334 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_DetailObjectDensity_7 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = ___value;
		Terrain_Internal_SetDetailObjectDensity_m7261(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Terrain::Internal_GetDetailObjectDensity(System.IntPtr)
extern "C" float Terrain_Internal_GetDetailObjectDensity_m7260 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef float (*Terrain_Internal_GetDetailObjectDensity_m7260_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetDetailObjectDensity_m7260_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetDetailObjectDensity_m7260_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetDetailObjectDensity(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetDetailObjectDensity(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetDetailObjectDensity_m7261 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetDetailObjectDensity_m7261_ftn) (Terrain_t1334 *, IntPtr_t, float);
	static Terrain_Internal_SetDetailObjectDensity_m7261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetDetailObjectDensity_m7261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetDetailObjectDensity(System.IntPtr,System.Single)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::get_heightmapPixelError()
extern "C" float Terrain_get_heightmapPixelError_m7262 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_HeightmapPixelError_8);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = Terrain_Internal_GetHeightmapPixelError_m7264(__this, L_1, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___m_HeightmapPixelError_8);
		Terrain_Internal_SetHeightmapPixelError_m7265(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_5 = (__this->___m_HeightmapPixelError_8);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_heightmapPixelError(System.Single)
extern "C" void Terrain_set_heightmapPixelError_m7263 (Terrain_t1334 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_HeightmapPixelError_8 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = ___value;
		Terrain_Internal_SetHeightmapPixelError_m7265(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Terrain::Internal_GetHeightmapPixelError(System.IntPtr)
extern "C" float Terrain_Internal_GetHeightmapPixelError_m7264 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef float (*Terrain_Internal_GetHeightmapPixelError_m7264_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetHeightmapPixelError_m7264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetHeightmapPixelError_m7264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetHeightmapPixelError(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetHeightmapPixelError(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetHeightmapPixelError_m7265 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetHeightmapPixelError_m7265_ftn) (Terrain_t1334 *, IntPtr_t, float);
	static Terrain_Internal_SetHeightmapPixelError_m7265_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetHeightmapPixelError_m7265_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetHeightmapPixelError(System.IntPtr,System.Single)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Int32 UnityEngine.Terrain::get_heightmapMaximumLOD()
extern "C" int32_t Terrain_get_heightmapMaximumLOD_m7266 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_HeightmapMaximumLOD_10);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_2 = Terrain_Internal_GetHeightmapMaximumLOD_m7268(__this, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_HeightmapMaximumLOD_10);
		Terrain_Internal_SetHeightmapMaximumLOD_m7269(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_5 = (__this->___m_HeightmapMaximumLOD_10);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_heightmapMaximumLOD(System.Int32)
extern "C" void Terrain_set_heightmapMaximumLOD_m7267 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_HeightmapMaximumLOD_10 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		Terrain_Internal_SetHeightmapMaximumLOD_m7269(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Terrain::Internal_GetHeightmapMaximumLOD(System.IntPtr)
extern "C" int32_t Terrain_Internal_GetHeightmapMaximumLOD_m7268 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef int32_t (*Terrain_Internal_GetHeightmapMaximumLOD_m7268_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetHeightmapMaximumLOD_m7268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetHeightmapMaximumLOD_m7268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetHeightmapMaximumLOD(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetHeightmapMaximumLOD(System.IntPtr,System.Int32)
extern "C" void Terrain_Internal_SetHeightmapMaximumLOD_m7269 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetHeightmapMaximumLOD_m7269_ftn) (Terrain_t1334 *, IntPtr_t, int32_t);
	static Terrain_Internal_SetHeightmapMaximumLOD_m7269_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetHeightmapMaximumLOD_m7269_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetHeightmapMaximumLOD(System.IntPtr,System.Int32)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::get_basemapDistance()
extern "C" float Terrain_get_basemapDistance_m7270 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_SplatMapDistance_9);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = Terrain_Internal_GetBasemapDistance_m7272(__this, L_1, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___m_SplatMapDistance_9);
		Terrain_Internal_SetBasemapDistance_m7273(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_5 = (__this->___m_SplatMapDistance_9);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_basemapDistance(System.Single)
extern "C" void Terrain_set_basemapDistance_m7271 (Terrain_t1334 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_SplatMapDistance_9 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		float L_2 = ___value;
		Terrain_Internal_SetBasemapDistance_m7273(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Terrain::Internal_GetBasemapDistance(System.IntPtr)
extern "C" float Terrain_Internal_GetBasemapDistance_m7272 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef float (*Terrain_Internal_GetBasemapDistance_m7272_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetBasemapDistance_m7272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetBasemapDistance_m7272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetBasemapDistance(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetBasemapDistance(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetBasemapDistance_m7273 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetBasemapDistance_m7273_ftn) (Terrain_t1334 *, IntPtr_t, float);
	static Terrain_Internal_SetBasemapDistance_m7273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetBasemapDistance_m7273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetBasemapDistance(System.IntPtr,System.Single)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::get_splatmapDistance()
extern "C" float Terrain_get_splatmapDistance_m7274 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		float L_0 = Terrain_get_basemapDistance_m7270(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Terrain::set_splatmapDistance(System.Single)
extern "C" void Terrain_set_splatmapDistance_m7275 (Terrain_t1334 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		Terrain_set_basemapDistance_m7271(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Terrain::get_lightmapIndex()
extern "C" int32_t Terrain_get_lightmapIndex_m7276 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_LightmapIndex_12);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_2 = Terrain_Internal_GetLightmapIndex_m7280(__this, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_LightmapIndex_12);
		Terrain_Internal_SetLightmapIndex_m7281(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_5 = (__this->___m_LightmapIndex_12);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_lightmapIndex(System.Int32)
extern "C" void Terrain_set_lightmapIndex_m7277 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_LightmapIndex_12 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		Terrain_Internal_SetLightmapIndex_m7281(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::SetLightmapIndex(System.Int32)
extern "C" void Terrain_SetLightmapIndex_m7278 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		Terrain_set_lightmapIndex_m7277(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::ShiftLightmapIndex(System.Int32)
extern "C" void Terrain_ShiftLightmapIndex_m7279 (Terrain_t1334 * __this, int32_t ___offset, const MethodInfo* method)
{
	{
		int32_t L_0 = Terrain_get_lightmapIndex_m7276(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___offset;
		Terrain_set_lightmapIndex_m7277(__this, ((int32_t)((int32_t)L_0+(int32_t)L_1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Terrain::Internal_GetLightmapIndex(System.IntPtr)
extern "C" int32_t Terrain_Internal_GetLightmapIndex_m7280 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef int32_t (*Terrain_Internal_GetLightmapIndex_m7280_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetLightmapIndex_m7280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetLightmapIndex_m7280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetLightmapIndex(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetLightmapIndex(System.IntPtr,System.Int32)
extern "C" void Terrain_Internal_SetLightmapIndex_m7281 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetLightmapIndex_m7281_ftn) (Terrain_t1334 *, IntPtr_t, int32_t);
	static Terrain_Internal_SetLightmapIndex_m7281_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetLightmapIndex_m7281_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetLightmapIndex(System.IntPtr,System.Int32)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Int32 UnityEngine.Terrain::get_lightmapSize()
extern "C" int32_t Terrain_get_lightmapSize_m7282 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_LightmapSize_13);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_2 = Terrain_Internal_GetLightmapSize_m7284(__this, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_LightmapSize_13);
		Terrain_Internal_SetLightmapSize_m7285(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_5 = (__this->___m_LightmapSize_13);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_lightmapSize(System.Int32)
extern "C" void Terrain_set_lightmapSize_m7283 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_LightmapSize_13 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		Terrain_Internal_SetLightmapSize_m7285(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Terrain::Internal_GetLightmapSize(System.IntPtr)
extern "C" int32_t Terrain_Internal_GetLightmapSize_m7284 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef int32_t (*Terrain_Internal_GetLightmapSize_m7284_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetLightmapSize_m7284_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetLightmapSize_m7284_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetLightmapSize(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetLightmapSize(System.IntPtr,System.Int32)
extern "C" void Terrain_Internal_SetLightmapSize_m7285 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetLightmapSize_m7285_ftn) (Terrain_t1334 *, IntPtr_t, int32_t);
	static Terrain_Internal_SetLightmapSize_m7285_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetLightmapSize_m7285_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetLightmapSize(System.IntPtr,System.Int32)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Boolean UnityEngine.Terrain::get_castShadows()
extern "C" bool Terrain_get_castShadows_m7286 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CastShadows_11);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_2 = Terrain_Internal_GetCastShadows_m7288(__this, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_4 = (__this->___m_CastShadows_11);
		Terrain_Internal_SetCastShadows_m7289(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		bool L_5 = (__this->___m_CastShadows_11);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_castShadows(System.Boolean)
extern "C" void Terrain_set_castShadows_m7287 (Terrain_t1334 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_CastShadows_11 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_2 = ___value;
		Terrain_Internal_SetCastShadows_m7289(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Terrain::Internal_GetCastShadows(System.IntPtr)
extern "C" bool Terrain_Internal_GetCastShadows_m7288 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef bool (*Terrain_Internal_GetCastShadows_m7288_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetCastShadows_m7288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetCastShadows_m7288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetCastShadows(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetCastShadows(System.IntPtr,System.Boolean)
extern "C" void Terrain_Internal_SetCastShadows_m7289 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, bool ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetCastShadows_m7289_ftn) (Terrain_t1334 *, IntPtr_t, bool);
	static Terrain_Internal_SetCastShadows_m7289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetCastShadows_m7289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetCastShadows(System.IntPtr,System.Boolean)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// UnityEngine.Material UnityEngine.Terrain::get_materialTemplate()
extern "C" Material_t45 * Terrain_get_materialTemplate_m7290 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		Material_t45 * L_0 = (__this->___m_MaterialTemplate_16);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Material_t45 * L_2 = Terrain_Internal_GetMaterialTemplate_m7292(__this, L_1, /*hidden argument*/NULL);
		bool L_3 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_4 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Material_t45 * L_5 = (__this->___m_MaterialTemplate_16);
		Terrain_Internal_SetMaterialTemplate_m7293(__this, L_4, L_5, /*hidden argument*/NULL);
	}

IL_002e:
	{
		Material_t45 * L_6 = (__this->___m_MaterialTemplate_16);
		return L_6;
	}
}
// System.Void UnityEngine.Terrain::set_materialTemplate(UnityEngine.Material)
extern "C" void Terrain_set_materialTemplate_m7291 (Terrain_t1334 * __this, Material_t45 * ___value, const MethodInfo* method)
{
	{
		Material_t45 * L_0 = ___value;
		__this->___m_MaterialTemplate_16 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Material_t45 * L_2 = ___value;
		Terrain_Internal_SetMaterialTemplate_m7293(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityEngine.Terrain::Internal_GetMaterialTemplate(System.IntPtr)
extern "C" Material_t45 * Terrain_Internal_GetMaterialTemplate_m7292 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef Material_t45 * (*Terrain_Internal_GetMaterialTemplate_m7292_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetMaterialTemplate_m7292_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetMaterialTemplate_m7292_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetMaterialTemplate(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetMaterialTemplate(System.IntPtr,UnityEngine.Material)
extern "C" void Terrain_Internal_SetMaterialTemplate_m7293 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, Material_t45 * ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetMaterialTemplate_m7293_ftn) (Terrain_t1334 *, IntPtr_t, Material_t45 *);
	static Terrain_Internal_SetMaterialTemplate_m7293_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetMaterialTemplate_m7293_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetMaterialTemplate(System.IntPtr,UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Boolean UnityEngine.Terrain::get_drawTreesAndFoliage()
extern "C" bool Terrain_get_drawTreesAndFoliage_m7294 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_DrawTreesAndFoliage_14);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_2 = Terrain_Internal_GetDrawTreesAndFoliage_m7296(__this, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_4 = (__this->___m_DrawTreesAndFoliage_14);
		Terrain_Internal_SetDrawTreesAndFoliage_m7297(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		bool L_5 = (__this->___m_DrawTreesAndFoliage_14);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_drawTreesAndFoliage(System.Boolean)
extern "C" void Terrain_set_drawTreesAndFoliage_m7295 (Terrain_t1334 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_DrawTreesAndFoliage_14 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_2 = ___value;
		Terrain_Internal_SetDrawTreesAndFoliage_m7297(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Terrain::Internal_GetDrawTreesAndFoliage(System.IntPtr)
extern "C" bool Terrain_Internal_GetDrawTreesAndFoliage_m7296 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef bool (*Terrain_Internal_GetDrawTreesAndFoliage_m7296_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetDrawTreesAndFoliage_m7296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetDrawTreesAndFoliage_m7296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetDrawTreesAndFoliage(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetDrawTreesAndFoliage(System.IntPtr,System.Boolean)
extern "C" void Terrain_Internal_SetDrawTreesAndFoliage_m7297 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, bool ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetDrawTreesAndFoliage_m7297_ftn) (Terrain_t1334 *, IntPtr_t, bool);
	static Terrain_Internal_SetDrawTreesAndFoliage_m7297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetDrawTreesAndFoliage_m7297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetDrawTreesAndFoliage(System.IntPtr,System.Boolean)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Boolean UnityEngine.Terrain::get_collectDetailPatches()
extern "C" bool Terrain_get_collectDetailPatches_m7298 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CollectDetailPatches_15);
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_2 = Terrain_Internal_GetCollectDetailPatches_m7300(__this, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_4 = (__this->___m_CollectDetailPatches_15);
		Terrain_Internal_SetCollectDetailPatches_m7301(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		bool L_5 = (__this->___m_CollectDetailPatches_15);
		return L_5;
	}
}
// System.Void UnityEngine.Terrain::set_collectDetailPatches(System.Boolean)
extern "C" void Terrain_set_collectDetailPatches_m7299 (Terrain_t1334 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_CollectDetailPatches_15 = L_0;
		IntPtr_t L_1 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		bool L_2 = ___value;
		Terrain_Internal_SetCollectDetailPatches_m7301(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Terrain::Internal_GetCollectDetailPatches(System.IntPtr)
extern "C" bool Terrain_Internal_GetCollectDetailPatches_m7300 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef bool (*Terrain_Internal_GetCollectDetailPatches_m7300_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetCollectDetailPatches_m7300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetCollectDetailPatches_m7300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetCollectDetailPatches(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Internal_SetCollectDetailPatches(System.IntPtr,System.Boolean)
extern "C" void Terrain_Internal_SetCollectDetailPatches_m7301 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, bool ___value, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetCollectDetailPatches_m7301_ftn) (Terrain_t1334 *, IntPtr_t, bool);
	static Terrain_Internal_SetCollectDetailPatches_m7301_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetCollectDetailPatches_m7301_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetCollectDetailPatches(System.IntPtr,System.Boolean)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___value);
}
// System.Single UnityEngine.Terrain::SampleHeight(UnityEngine.Vector3)
extern "C" float Terrain_SampleHeight_m7302 (Terrain_t1334 * __this, Vector3_t6  ___worldPosition, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = ___worldPosition;
		float L_2 = Terrain_Internal_SampleHeight_m7303(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Terrain::Internal_SampleHeight(System.IntPtr,UnityEngine.Vector3)
extern "C" float Terrain_Internal_SampleHeight_m7303 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, Vector3_t6  ___worldPosition, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___terrainInstance;
		float L_1 = Terrain_INTERNAL_CALL_Internal_SampleHeight_m7304(NULL /*static, unused*/, __this, L_0, (&___worldPosition), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Terrain::INTERNAL_CALL_Internal_SampleHeight(UnityEngine.Terrain,System.IntPtr,UnityEngine.Vector3&)
extern "C" float Terrain_INTERNAL_CALL_Internal_SampleHeight_m7304 (Object_t * __this /* static, unused */, Terrain_t1334 * ___self, IntPtr_t ___terrainInstance, Vector3_t6 * ___worldPosition, const MethodInfo* method)
{
	typedef float (*Terrain_INTERNAL_CALL_Internal_SampleHeight_m7304_ftn) (Terrain_t1334 *, IntPtr_t, Vector3_t6 *);
	static Terrain_INTERNAL_CALL_Internal_SampleHeight_m7304_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_INTERNAL_CALL_Internal_SampleHeight_m7304_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::INTERNAL_CALL_Internal_SampleHeight(UnityEngine.Terrain,System.IntPtr,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___terrainInstance, ___worldPosition);
}
// System.Void UnityEngine.Terrain::ApplyDelayedHeightmapModification()
extern "C" void Terrain_ApplyDelayedHeightmapModification_m7305 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Terrain_Internal_ApplyDelayedHeightmapModification_m7306(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::Internal_ApplyDelayedHeightmapModification(System.IntPtr)
extern "C" void Terrain_Internal_ApplyDelayedHeightmapModification_m7306 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_ApplyDelayedHeightmapModification_m7306_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_ApplyDelayedHeightmapModification_m7306_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_ApplyDelayedHeightmapModification_m7306_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_ApplyDelayedHeightmapModification(System.IntPtr)");
	_il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::AddTreeInstance(UnityEngine.TreeInstance)
extern "C" void Terrain_AddTreeInstance_m7307 (Terrain_t1334 * __this, TreeInstance_t1330  ___instance, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		TreeInstance_t1330  L_1 = ___instance;
		Terrain_Internal_AddTreeInstance_m7308(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::Internal_AddTreeInstance(System.IntPtr,UnityEngine.TreeInstance)
extern "C" void Terrain_Internal_AddTreeInstance_m7308 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, TreeInstance_t1330  ___instance, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___terrainInstance;
		Terrain_INTERNAL_CALL_Internal_AddTreeInstance_m7309(NULL /*static, unused*/, __this, L_0, (&___instance), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::INTERNAL_CALL_Internal_AddTreeInstance(UnityEngine.Terrain,System.IntPtr,UnityEngine.TreeInstance&)
extern "C" void Terrain_INTERNAL_CALL_Internal_AddTreeInstance_m7309 (Object_t * __this /* static, unused */, Terrain_t1334 * ___self, IntPtr_t ___terrainInstance, TreeInstance_t1330 * ___instance, const MethodInfo* method)
{
	typedef void (*Terrain_INTERNAL_CALL_Internal_AddTreeInstance_m7309_ftn) (Terrain_t1334 *, IntPtr_t, TreeInstance_t1330 *);
	static Terrain_INTERNAL_CALL_Internal_AddTreeInstance_m7309_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_INTERNAL_CALL_Internal_AddTreeInstance_m7309_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::INTERNAL_CALL_Internal_AddTreeInstance(UnityEngine.Terrain,System.IntPtr,UnityEngine.TreeInstance&)");
	_il2cpp_icall_func(___self, ___terrainInstance, ___instance);
}
// System.Void UnityEngine.Terrain::SetNeighbors(UnityEngine.Terrain,UnityEngine.Terrain,UnityEngine.Terrain,UnityEngine.Terrain)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Terrain_SetNeighbors_m7310 (Terrain_t1334 * __this, Terrain_t1334 * ___left, Terrain_t1334 * ___top, Terrain_t1334 * ___right, Terrain_t1334 * ___bottom, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t G_B2_0 = {0};
	Terrain_t1334 * G_B2_1 = {0};
	IntPtr_t G_B1_0 = {0};
	Terrain_t1334 * G_B1_1 = {0};
	IntPtr_t G_B3_0 = {0};
	IntPtr_t G_B3_1 = {0};
	Terrain_t1334 * G_B3_2 = {0};
	IntPtr_t G_B5_0 = {0};
	IntPtr_t G_B5_1 = {0};
	Terrain_t1334 * G_B5_2 = {0};
	IntPtr_t G_B4_0 = {0};
	IntPtr_t G_B4_1 = {0};
	Terrain_t1334 * G_B4_2 = {0};
	IntPtr_t G_B6_0 = {0};
	IntPtr_t G_B6_1 = {0};
	IntPtr_t G_B6_2 = {0};
	Terrain_t1334 * G_B6_3 = {0};
	IntPtr_t G_B8_0 = {0};
	IntPtr_t G_B8_1 = {0};
	IntPtr_t G_B8_2 = {0};
	Terrain_t1334 * G_B8_3 = {0};
	IntPtr_t G_B7_0 = {0};
	IntPtr_t G_B7_1 = {0};
	IntPtr_t G_B7_2 = {0};
	Terrain_t1334 * G_B7_3 = {0};
	IntPtr_t G_B9_0 = {0};
	IntPtr_t G_B9_1 = {0};
	IntPtr_t G_B9_2 = {0};
	IntPtr_t G_B9_3 = {0};
	Terrain_t1334 * G_B9_4 = {0};
	IntPtr_t G_B11_0 = {0};
	IntPtr_t G_B11_1 = {0};
	IntPtr_t G_B11_2 = {0};
	IntPtr_t G_B11_3 = {0};
	Terrain_t1334 * G_B11_4 = {0};
	IntPtr_t G_B10_0 = {0};
	IntPtr_t G_B10_1 = {0};
	IntPtr_t G_B10_2 = {0};
	IntPtr_t G_B10_3 = {0};
	Terrain_t1334 * G_B10_4 = {0};
	IntPtr_t G_B12_0 = {0};
	IntPtr_t G_B12_1 = {0};
	IntPtr_t G_B12_2 = {0};
	IntPtr_t G_B12_3 = {0};
	IntPtr_t G_B12_4 = {0};
	Terrain_t1334 * G_B12_5 = {0};
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Terrain_t1334 * L_1 = ___left;
		bool L_2 = Object_op_Inequality_m395(NULL /*static, unused*/, L_1, (Object_t53 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (!L_2)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		Terrain_t1334 * L_3 = ___left;
		NullCheck(L_3);
		IntPtr_t L_4 = Terrain_get_InstanceObject_m7225(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0023;
	}

IL_001e:
	{
		IntPtr_t L_5 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0023:
	{
		Terrain_t1334 * L_6 = ___top;
		bool L_7 = Object_op_Inequality_m395(NULL /*static, unused*/, L_6, (Object_t53 *)NULL, /*hidden argument*/NULL);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (!L_7)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_003a;
		}
	}
	{
		Terrain_t1334 * L_8 = ___top;
		NullCheck(L_8);
		IntPtr_t L_9 = Terrain_get_InstanceObject_m7225(L_8, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_003f;
	}

IL_003a:
	{
		IntPtr_t L_10 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		G_B6_0 = L_10;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_003f:
	{
		Terrain_t1334 * L_11 = ___right;
		bool L_12 = Object_op_Inequality_m395(NULL /*static, unused*/, L_11, (Object_t53 *)NULL, /*hidden argument*/NULL);
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		G_B7_2 = G_B6_2;
		G_B7_3 = G_B6_3;
		if (!L_12)
		{
			G_B8_0 = G_B6_0;
			G_B8_1 = G_B6_1;
			G_B8_2 = G_B6_2;
			G_B8_3 = G_B6_3;
			goto IL_0056;
		}
	}
	{
		Terrain_t1334 * L_13 = ___right;
		NullCheck(L_13);
		IntPtr_t L_14 = Terrain_get_InstanceObject_m7225(L_13, /*hidden argument*/NULL);
		G_B9_0 = L_14;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		G_B9_4 = G_B7_3;
		goto IL_005b;
	}

IL_0056:
	{
		IntPtr_t L_15 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		G_B9_0 = L_15;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
		G_B9_4 = G_B8_3;
	}

IL_005b:
	{
		Terrain_t1334 * L_16 = ___bottom;
		bool L_17 = Object_op_Inequality_m395(NULL /*static, unused*/, L_16, (Object_t53 *)NULL, /*hidden argument*/NULL);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		if (!L_17)
		{
			G_B11_0 = G_B9_0;
			G_B11_1 = G_B9_1;
			G_B11_2 = G_B9_2;
			G_B11_3 = G_B9_3;
			G_B11_4 = G_B9_4;
			goto IL_0074;
		}
	}
	{
		Terrain_t1334 * L_18 = ___bottom;
		NullCheck(L_18);
		IntPtr_t L_19 = Terrain_get_InstanceObject_m7225(L_18, /*hidden argument*/NULL);
		G_B12_0 = L_19;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		G_B12_4 = G_B10_3;
		G_B12_5 = G_B10_4;
		goto IL_0079;
	}

IL_0074:
	{
		IntPtr_t L_20 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		G_B12_0 = L_20;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
		G_B12_4 = G_B11_3;
		G_B12_5 = G_B11_4;
	}

IL_0079:
	{
		NullCheck(G_B12_5);
		Terrain_Internal_SetNeighbors_m7311(G_B12_5, G_B12_4, G_B12_3, G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::Internal_SetNeighbors(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void Terrain_Internal_SetNeighbors_m7311 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, IntPtr_t ___left, IntPtr_t ___top, IntPtr_t ___right, IntPtr_t ___bottom, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_SetNeighbors_m7311_ftn) (Terrain_t1334 *, IntPtr_t, IntPtr_t, IntPtr_t, IntPtr_t, IntPtr_t);
	static Terrain_Internal_SetNeighbors_m7311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_SetNeighbors_m7311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_SetNeighbors(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___left, ___top, ___right, ___bottom);
}
// UnityEngine.Vector3 UnityEngine.Terrain::GetPosition()
extern "C" Vector3_t6  Terrain_GetPosition_m7312 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = Terrain_Internal_GetPosition_m7313(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Terrain::Internal_GetPosition(System.IntPtr)
extern "C" Vector3_t6  Terrain_Internal_GetPosition_m7313 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef Vector3_t6  (*Terrain_Internal_GetPosition_m7313_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_GetPosition_m7313_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_GetPosition_m7313_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_GetPosition(System.IntPtr)");
	return _il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::Flush()
extern "C" void Terrain_Flush_m7314 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Terrain_Internal_Flush_m7315(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::Internal_Flush(System.IntPtr)
extern "C" void Terrain_Internal_Flush_m7315 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_Flush_m7315_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_Flush_m7315_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_Flush_m7315_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_Flush(System.IntPtr)");
	_il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::RemoveTrees(UnityEngine.Vector2,System.Single,System.Int32)
extern "C" void Terrain_RemoveTrees_m7316 (Terrain_t1334 * __this, Vector2_t29  ___position, float ___radius, int32_t ___prototypeIndex, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Vector2_t29  L_1 = ___position;
		float L_2 = ___radius;
		int32_t L_3 = ___prototypeIndex;
		Terrain_Internal_RemoveTrees_m7317(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::Internal_RemoveTrees(System.IntPtr,UnityEngine.Vector2,System.Single,System.Int32)
extern "C" void Terrain_Internal_RemoveTrees_m7317 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, Vector2_t29  ___position, float ___radius, int32_t ___prototypeIndex, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___terrainInstance;
		float L_1 = ___radius;
		int32_t L_2 = ___prototypeIndex;
		Terrain_INTERNAL_CALL_Internal_RemoveTrees_m7318(NULL /*static, unused*/, __this, L_0, (&___position), L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::INTERNAL_CALL_Internal_RemoveTrees(UnityEngine.Terrain,System.IntPtr,UnityEngine.Vector2&,System.Single,System.Int32)
extern "C" void Terrain_INTERNAL_CALL_Internal_RemoveTrees_m7318 (Object_t * __this /* static, unused */, Terrain_t1334 * ___self, IntPtr_t ___terrainInstance, Vector2_t29 * ___position, float ___radius, int32_t ___prototypeIndex, const MethodInfo* method)
{
	typedef void (*Terrain_INTERNAL_CALL_Internal_RemoveTrees_m7318_ftn) (Terrain_t1334 *, IntPtr_t, Vector2_t29 *, float, int32_t);
	static Terrain_INTERNAL_CALL_Internal_RemoveTrees_m7318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_INTERNAL_CALL_Internal_RemoveTrees_m7318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::INTERNAL_CALL_Internal_RemoveTrees(UnityEngine.Terrain,System.IntPtr,UnityEngine.Vector2&,System.Single,System.Int32)");
	_il2cpp_icall_func(___self, ___terrainInstance, ___position, ___radius, ___prototypeIndex);
}
// System.Void UnityEngine.Terrain::OnTerrainChanged(UnityEngine.TerrainChangedFlags)
extern "C" void Terrain_OnTerrainChanged_m7319 (Terrain_t1334 * __this, int32_t ___flags, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___flags;
		Terrain_Internal_OnTerrainChanged_m7320(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::Internal_OnTerrainChanged(System.IntPtr,UnityEngine.TerrainChangedFlags)
extern "C" void Terrain_Internal_OnTerrainChanged_m7320 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___flags, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_OnTerrainChanged_m7320_ftn) (Terrain_t1334 *, IntPtr_t, int32_t);
	static Terrain_Internal_OnTerrainChanged_m7320_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_OnTerrainChanged_m7320_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_OnTerrainChanged(System.IntPtr,UnityEngine.TerrainChangedFlags)");
	_il2cpp_icall_func(__this, ___terrainInstance, ___flags);
}
// System.IntPtr UnityEngine.Terrain::Construct()
extern "C" IntPtr_t Terrain_Construct_m7321 (Terrain_t1334 * __this, const MethodInfo* method)
{
	typedef IntPtr_t (*Terrain_Construct_m7321_ftn) (Terrain_t1334 *);
	static Terrain_Construct_m7321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Construct_m7321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Construct()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Terrain::OnEnable()
extern "C" void Terrain_OnEnable_m7322 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Terrain_Internal_OnEnable_m7323(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::Internal_OnEnable(System.IntPtr)
extern "C" void Terrain_Internal_OnEnable_m7323 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_OnEnable_m7323_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_OnEnable_m7323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_OnEnable_m7323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_OnEnable(System.IntPtr)");
	_il2cpp_icall_func(__this, ___terrainInstance);
}
// System.Void UnityEngine.Terrain::OnDisable()
extern "C" void Terrain_OnDisable_m7324 (Terrain_t1334 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Terrain_get_InstanceObject_m7225(__this, /*hidden argument*/NULL);
		Terrain_Internal_OnDisable_m7325(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Terrain::Internal_OnDisable(System.IntPtr)
extern "C" void Terrain_Internal_OnDisable_m7325 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method)
{
	typedef void (*Terrain_Internal_OnDisable_m7325_ftn) (Terrain_t1334 *, IntPtr_t);
	static Terrain_Internal_OnDisable_m7325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_Internal_OnDisable_m7325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::Internal_OnDisable(System.IntPtr)");
	_il2cpp_icall_func(__this, ___terrainInstance);
}
// UnityEngine.Terrain UnityEngine.Terrain::get_activeTerrain()
extern "C" Terrain_t1334 * Terrain_get_activeTerrain_m7326 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Terrain_t1334 * (*Terrain_get_activeTerrain_m7326_ftn) ();
	static Terrain_get_activeTerrain_m7326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_get_activeTerrain_m7326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::get_activeTerrain()");
	return _il2cpp_icall_func();
}
// UnityEngine.Terrain[] UnityEngine.Terrain::get_activeTerrains()
extern "C" TerrainU5BU5D_t1444* Terrain_get_activeTerrains_m7327 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef TerrainU5BU5D_t1444* (*Terrain_get_activeTerrains_m7327_ftn) ();
	static Terrain_get_activeTerrains_m7327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Terrain_get_activeTerrains_m7327_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Terrain::get_activeTerrains()");
	return _il2cpp_icall_func();
}
// UnityEngine.GameObject UnityEngine.Terrain::CreateTerrainGameObject(UnityEngine.TerrainData)
extern const Il2CppType* Terrain_t1334_0_0_0_var;
extern const Il2CppType* TerrainCollider_t1312_0_0_0_var;
extern TypeInfo* TypeU5BU5D_t1218_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t27_il2cpp_TypeInfo_var;
extern TypeInfo* Terrain_t1334_il2cpp_TypeInfo_var;
extern TypeInfo* TerrainCollider_t1312_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1163;
extern "C" GameObject_t27 * Terrain_CreateTerrainGameObject_m7328 (Object_t * __this /* static, unused */, TerrainData_t1331 * ___assignTerrain, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Terrain_t1334_0_0_0_var = il2cpp_codegen_type_from_index(946);
		TerrainCollider_t1312_0_0_0_var = il2cpp_codegen_type_from_index(947);
		TypeU5BU5D_t1218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(844);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GameObject_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Terrain_t1334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(946);
		TerrainCollider_t1312_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(947);
		_stringLiteral1163 = il2cpp_codegen_string_literal_from_index(1163);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t27 * V_0 = {0};
	Terrain_t1334 * V_1 = {0};
	TerrainCollider_t1312 * V_2 = {0};
	{
		TypeU5BU5D_t1218* L_0 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Terrain_t1334_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 0, sizeof(Type_t *))) = (Type_t *)L_1;
		TypeU5BU5D_t1218* L_2 = L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(TerrainCollider_t1312_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 1, sizeof(Type_t *))) = (Type_t *)L_3;
		GameObject_t27 * L_4 = (GameObject_t27 *)il2cpp_codegen_object_new (GameObject_t27_il2cpp_TypeInfo_var);
		GameObject__ctor_m6775(L_4, _stringLiteral1163, L_2, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t27 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_set_isStatic_m7194(L_5, 1, /*hidden argument*/NULL);
		GameObject_t27 * L_6 = V_0;
		Type_t * L_7 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Terrain_t1334_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		Component_t56 * L_8 = GameObject_GetComponent_m7192(L_6, L_7, /*hidden argument*/NULL);
		V_1 = ((Terrain_t1334 *)IsInstSealed(L_8, Terrain_t1334_il2cpp_TypeInfo_var));
		GameObject_t27 * L_9 = V_0;
		Type_t * L_10 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(TerrainCollider_t1312_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		Component_t56 * L_11 = GameObject_GetComponent_m7192(L_9, L_10, /*hidden argument*/NULL);
		V_2 = ((TerrainCollider_t1312 *)IsInstSealed(L_11, TerrainCollider_t1312_il2cpp_TypeInfo_var));
		TerrainCollider_t1312 * L_12 = V_2;
		TerrainData_t1331 * L_13 = ___assignTerrain;
		NullCheck(L_12);
		TerrainCollider_set_terrainData_m7139(L_12, L_13, /*hidden argument*/NULL);
		Terrain_t1334 * L_14 = V_1;
		TerrainData_t1331 * L_15 = ___assignTerrain;
		NullCheck(L_14);
		Terrain_set_terrainData_m7235(L_14, L_15, /*hidden argument*/NULL);
		Terrain_t1334 * L_16 = V_1;
		NullCheck(L_16);
		Terrain_OnEnable_m7322(L_16, /*hidden argument*/NULL);
		GameObject_t27 * L_17 = V_0;
		return L_17;
	}
}
// System.Void UnityEngine.Terrain::ReconnectTerrainData()
extern TypeInfo* List_1_t1468_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1469_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8171_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8172_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8173_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8174_MethodInfo_var;
extern "C" void Terrain_ReconnectTerrainData_m7329 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1468_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(948);
		Enumerator_t1469_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(949);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		List_1__ctor_m8171_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484715);
		List_1_GetEnumerator_m8172_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484716);
		Enumerator_get_Current_m8173_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484717);
		Enumerator_MoveNext_m8174_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484718);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1468 * V_0 = {0};
	Terrain_t1334 * V_1 = {0};
	Enumerator_t1469  V_2 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TerrainU5BU5D_t1444* L_0 = Terrain_get_activeTerrains_m7327(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t1468 * L_1 = (List_1_t1468 *)il2cpp_codegen_object_new (List_1_t1468_il2cpp_TypeInfo_var);
		List_1__ctor_m8171(L_1, (Object_t*)(Object_t*)L_0, /*hidden argument*/List_1__ctor_m8171_MethodInfo_var);
		V_0 = L_1;
		List_1_t1468 * L_2 = V_0;
		NullCheck(L_2);
		Enumerator_t1469  L_3 = List_1_GetEnumerator_m8172(L_2, /*hidden argument*/List_1_GetEnumerator_m8172_MethodInfo_var);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005d;
		}

IL_0017:
		{
			Terrain_t1334 * L_4 = Enumerator_get_Current_m8173((&V_2), /*hidden argument*/Enumerator_get_Current_m8173_MethodInfo_var);
			V_1 = L_4;
			Terrain_t1334 * L_5 = V_1;
			NullCheck(L_5);
			TerrainData_t1331 * L_6 = Terrain_get_terrainData_m7234(L_5, /*hidden argument*/NULL);
			bool L_7 = Object_op_Equality_m427(NULL /*static, unused*/, L_6, (Object_t53 *)NULL, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003b;
			}
		}

IL_0030:
		{
			Terrain_t1334 * L_8 = V_1;
			NullCheck(L_8);
			Terrain_OnDisable_m7324(L_8, /*hidden argument*/NULL);
			goto IL_005d;
		}

IL_003b:
		{
			Terrain_t1334 * L_9 = V_1;
			NullCheck(L_9);
			TerrainData_t1331 * L_10 = Terrain_get_terrainData_m7234(L_9, /*hidden argument*/NULL);
			Terrain_t1334 * L_11 = V_1;
			NullCheck(L_11);
			GameObject_t27 * L_12 = Component_get_gameObject_m306(L_11, /*hidden argument*/NULL);
			NullCheck(L_10);
			bool L_13 = TerrainData_HasUser_m7223(L_10, L_12, /*hidden argument*/NULL);
			if (L_13)
			{
				goto IL_005d;
			}
		}

IL_0051:
		{
			Terrain_t1334 * L_14 = V_1;
			NullCheck(L_14);
			Terrain_OnDisable_m7324(L_14, /*hidden argument*/NULL);
			Terrain_t1334 * L_15 = V_1;
			NullCheck(L_15);
			Terrain_OnEnable_m7322(L_15, /*hidden argument*/NULL);
		}

IL_005d:
		{
			bool L_16 = Enumerator_MoveNext_m8174((&V_2), /*hidden argument*/Enumerator_MoveNext_m8174_MethodInfo_var);
			if (L_16)
			{
				goto IL_0017;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x7A, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		Enumerator_t1469  L_17 = V_2;
		Enumerator_t1469  L_18 = L_17;
		Object_t * L_19 = Box(Enumerator_t1469_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_19);
		IL2CPP_END_FINALLY(110)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x7A, IL_007a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_007a:
	{
		return;
	}
}
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m4311 (DrivenRectTransformTracker_t738 * __this, Object_t53 * ___driver, RectTransform_t688 * ___rectTransform, int32_t ___drivenProperties, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m4309 (DrivenRectTransformTracker_t738 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C" void ReapplyDrivenProperties__ctor_m4418 (ReapplyDrivenProperties_t872 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C" void ReapplyDrivenProperties_Invoke_m7330 (ReapplyDrivenProperties_t872 * __this, RectTransform_t688 * ___driven, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReapplyDrivenProperties_Invoke_m7330((ReapplyDrivenProperties_t872 *)__this->___prev_9,___driven, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, RectTransform_t688 * ___driven, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, RectTransform_t688 * ___driven, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t872(Il2CppObject* delegate, RectTransform_t688 * ___driven)
{
	// Marshaling of parameter '___driven' to native representation
	RectTransform_t688 * ____driven_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.RectTransform'."));
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C" Object_t * ReapplyDrivenProperties_BeginInvoke_m7331 (ReapplyDrivenProperties_t872 * __this, RectTransform_t688 * ___driven, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C" void ReapplyDrivenProperties_EndInvoke_m7332 (ReapplyDrivenProperties_t872 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern TypeInfo* RectTransform_t688_il2cpp_TypeInfo_var;
extern TypeInfo* ReapplyDrivenProperties_t872_il2cpp_TypeInfo_var;
extern "C" void RectTransform_add_reapplyDrivenProperties_m4419 (Object_t * __this /* static, unused */, ReapplyDrivenProperties_t872 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t688_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(524);
		ReapplyDrivenProperties_t872_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t872 * L_0 = ((RectTransform_t688_StaticFields*)RectTransform_t688_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_1;
		ReapplyDrivenProperties_t872 * L_1 = ___value;
		Delegate_t506 * L_2 = Delegate_Combine_m2300(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t688_StaticFields*)RectTransform_t688_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_1 = ((ReapplyDrivenProperties_t872 *)CastclassSealed(L_2, ReapplyDrivenProperties_t872_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern TypeInfo* RectTransform_t688_il2cpp_TypeInfo_var;
extern TypeInfo* ReapplyDrivenProperties_t872_il2cpp_TypeInfo_var;
extern "C" void RectTransform_remove_reapplyDrivenProperties_m7333 (Object_t * __this /* static, unused */, ReapplyDrivenProperties_t872 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t688_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(524);
		ReapplyDrivenProperties_t872_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t872 * L_0 = ((RectTransform_t688_StaticFields*)RectTransform_t688_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_1;
		ReapplyDrivenProperties_t872 * L_1 = ___value;
		Delegate_t506 * L_2 = Delegate_Remove_m2302(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t688_StaticFields*)RectTransform_t688_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_1 = ((ReapplyDrivenProperties_t872 *)CastclassSealed(L_2, ReapplyDrivenProperties_t872_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C" Rect_t30  RectTransform_get_rect_m4139 (RectTransform_t688 * __this, const MethodInfo* method)
{
	Rect_t30  V_0 = {0};
	{
		RectTransform_INTERNAL_get_rect_m7334(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t30  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C" void RectTransform_INTERNAL_get_rect_m7334 (RectTransform_t688 * __this, Rect_t30 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m7334_ftn) (RectTransform_t688 *, Rect_t30 *);
	static RectTransform_INTERNAL_get_rect_m7334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m7334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C" Vector2_t29  RectTransform_get_anchorMin_m4193 (RectTransform_t688 * __this, const MethodInfo* method)
{
	Vector2_t29  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchorMin_m7335(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchorMin_m4285 (RectTransform_t688 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m7336(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchorMin_m7335 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m7335_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_get_anchorMin_m7335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m7335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchorMin_m7336 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m7336_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_set_anchorMin_m7336_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m7336_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C" Vector2_t29  RectTransform_get_anchorMax_m4282 (RectTransform_t688 * __this, const MethodInfo* method)
{
	Vector2_t29  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchorMax_m7337(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchorMax_m4194 (RectTransform_t688 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m7338(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchorMax_m7337 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m7337_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_get_anchorMax_m7337_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m7337_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchorMax_m7338 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m7338_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_set_anchorMax_m7338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m7338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C" Vector2_t29  RectTransform_get_anchoredPosition_m4283 (RectTransform_t688 * __this, const MethodInfo* method)
{
	Vector2_t29  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchoredPosition_m7339(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchoredPosition_m4286 (RectTransform_t688 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m7340(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchoredPosition_m7339 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m7339_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m7339_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m7339_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchoredPosition_m7340 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m7340_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m7340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m7340_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C" Vector2_t29  RectTransform_get_sizeDelta_m4284 (RectTransform_t688 * __this, const MethodInfo* method)
{
	Vector2_t29  V_0 = {0};
	{
		RectTransform_INTERNAL_get_sizeDelta_m7341(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C" void RectTransform_set_sizeDelta_m4195 (RectTransform_t688 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m7342(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_sizeDelta_m7341 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m7341_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_get_sizeDelta_m7341_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m7341_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_sizeDelta_m7342 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m7342_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_set_sizeDelta_m7342_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m7342_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C" Vector2_t29  RectTransform_get_pivot_m4192 (RectTransform_t688 * __this, const MethodInfo* method)
{
	Vector2_t29  V_0 = {0};
	{
		RectTransform_INTERNAL_get_pivot_m7343(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C" void RectTransform_set_pivot_m4287 (RectTransform_t688 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m7344(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_pivot_m7343 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m7343_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_get_pivot_m7343_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m7343_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_pivot_m7344 (RectTransform_t688 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m7344_ftn) (RectTransform_t688 *, Vector2_t29 *);
	static RectTransform_INTERNAL_set_pivot_m7344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m7344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern TypeInfo* RectTransform_t688_il2cpp_TypeInfo_var;
extern "C" void RectTransform_SendReapplyDrivenProperties_m7345 (Object_t * __this /* static, unused */, RectTransform_t688 * ___driven, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t688_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(524);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t872 * L_0 = ((RectTransform_t688_StaticFields*)RectTransform_t688_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_1;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t872 * L_1 = ((RectTransform_t688_StaticFields*)RectTransform_t688_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_1;
		RectTransform_t688 * L_2 = ___driven;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m7330(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern Il2CppCodeGenString* _stringLiteral1164;
extern "C" void RectTransform_GetLocalCorners_m7346 (RectTransform_t688 * __this, Vector3U5BU5D_t8* ___fourCornersArray, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1164 = il2cpp_codegen_string_literal_from_index(1164);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t30  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t8* L_0 = ___fourCornersArray;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t8* L_1 = ___fourCornersArray;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		Debug_LogError_m302(NULL /*static, unused*/, _stringLiteral1164, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Rect_t30  L_2 = RectTransform_get_rect_m4139(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m366((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m368((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m4208((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m4209((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t8* L_7 = ___fourCornersArray;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t6  L_10 = {0};
		Vector3__ctor_m335(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_7, 0, sizeof(Vector3_t6 )))) = L_10;
		Vector3U5BU5D_t8* L_11 = ___fourCornersArray;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t6  L_14 = {0};
		Vector3__ctor_m335(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_11, 1, sizeof(Vector3_t6 )))) = L_14;
		Vector3U5BU5D_t8* L_15 = ___fourCornersArray;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t6  L_18 = {0};
		Vector3__ctor_m335(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_15, 2, sizeof(Vector3_t6 )))) = L_18;
		Vector3U5BU5D_t8* L_19 = ___fourCornersArray;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t6  L_22 = {0};
		Vector3__ctor_m335(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_19, 3, sizeof(Vector3_t6 )))) = L_22;
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern Il2CppCodeGenString* _stringLiteral1165;
extern "C" void RectTransform_GetWorldCorners_m4333 (RectTransform_t688 * __this, Vector3U5BU5D_t8* ___fourCornersArray, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1165 = il2cpp_codegen_string_literal_from_index(1165);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t25 * V_0 = {0};
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t8* L_0 = ___fourCornersArray;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t8* L_1 = ___fourCornersArray;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		Debug_LogError_m302(NULL /*static, unused*/, _stringLiteral1165, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Vector3U5BU5D_t8* L_2 = ___fourCornersArray;
		RectTransform_GetLocalCorners_m7346(__this, L_2, /*hidden argument*/NULL);
		Transform_t25 * L_3 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0051;
	}

IL_002f:
	{
		Vector3U5BU5D_t8* L_4 = ___fourCornersArray;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t25 * L_6 = V_0;
		Vector3U5BU5D_t8* L_7 = ___fourCornersArray;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t6  L_9 = Transform_TransformPoint_m4351(L_6, (*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_7, L_8, sizeof(Vector3_t6 )))), /*hidden argument*/NULL);
		(*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_4, L_5, sizeof(Vector3_t6 )))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C" void RectTransform_SetInsetAndSizeFromParentEdge_m4417 (RectTransform_t688 * __this, int32_t ___edge, float ___inset, float ___size, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t29  V_3 = {0};
	Vector2_t29  V_4 = {0};
	Vector2_t29  V_5 = {0};
	Vector2_t29  V_6 = {0};
	Vector2_t29  V_7 = {0};
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t29 * G_B12_1 = {0};
	int32_t G_B11_0 = 0;
	Vector2_t29 * G_B11_1 = {0};
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t29 * G_B13_2 = {0};
	{
		int32_t L_0 = ___edge;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___edge;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0014;
		}
	}

IL_000e:
	{
		G_B4_0 = 1;
		goto IL_0015;
	}

IL_0014:
	{
		G_B4_0 = 0;
	}

IL_0015:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___edge;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B7_0 = 1;
	}

IL_0024:
	{
		V_1 = G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0032;
	}

IL_0031:
	{
		G_B10_0 = 0;
	}

IL_0032:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t29  L_5 = RectTransform_get_anchorMin_m4193(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m4212((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t29  L_8 = V_3;
		RectTransform_set_anchorMin_m4285(__this, L_8, /*hidden argument*/NULL);
		Vector2_t29  L_9 = RectTransform_get_anchorMax_m4282(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m4212((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t29  L_12 = V_3;
		RectTransform_set_anchorMax_m4194(__this, L_12, /*hidden argument*/NULL);
		Vector2_t29  L_13 = RectTransform_get_sizeDelta_m4284(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size;
		Vector2_set_Item_m4212((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t29  L_16 = V_4;
		RectTransform_set_sizeDelta_m4195(__this, L_16, /*hidden argument*/NULL);
		Vector2_t29  L_17 = RectTransform_get_anchoredPosition_m4283(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ac;
		}
	}
	{
		float L_20 = ___inset;
		float L_21 = ___size;
		Vector2_t29  L_22 = RectTransform_get_pivot_m4192(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m4203((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c0;
	}

IL_00ac:
	{
		float L_25 = ___inset;
		float L_26 = ___size;
		Vector2_t29  L_27 = RectTransform_get_pivot_m4192(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m4203((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c0:
	{
		Vector2_set_Item_m4212(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t29  L_30 = V_5;
		RectTransform_set_anchoredPosition_m4286(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C" void RectTransform_SetSizeWithCurrentAnchors_m4384 (RectTransform_t688 * __this, int32_t ___axis, float ___size, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t29  V_1 = {0};
	Vector2_t29  V_2 = {0};
	Vector2_t29  V_3 = {0};
	Vector2_t29  V_4 = {0};
	{
		int32_t L_0 = ___axis;
		V_0 = L_0;
		Vector2_t29  L_1 = RectTransform_get_sizeDelta_m4284(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size;
		Vector2_t29  L_4 = RectTransform_GetParentSize_m7347(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m4203((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t29  L_7 = RectTransform_get_anchorMax_m4282(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m4203((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t29  L_10 = RectTransform_get_anchorMin_m4193(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m4203((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m4212((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t29  L_13 = V_1;
		RectTransform_set_sizeDelta_m4195(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern TypeInfo* RectTransform_t688_il2cpp_TypeInfo_var;
extern "C" Vector2_t29  RectTransform_GetParentSize_m7347 (RectTransform_t688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t688_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(524);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t688 * V_0 = {0};
	Rect_t30  V_1 = {0};
	{
		Transform_t25 * L_0 = Transform_get_parent_m2605(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t688 *)IsInstSealed(L_0, RectTransform_t688_il2cpp_TypeInfo_var));
		RectTransform_t688 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m301(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t29  L_3 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_001d:
	{
		RectTransform_t688 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t30  L_5 = RectTransform_get_rect_m4139(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t29  L_6 = Rect_get_size_m4199((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern TypeInfo* Vector3U5BU5D_t8_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t848_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility__cctor_m7348 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		RectTransformUtility_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(534);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t848_StaticFields*)RectTransformUtility_t848_il2cpp_TypeInfo_var->static_fields)->___s_Corners_0 = ((Vector3U5BU5D_t8*)SZArrayNew(Vector3U5BU5D_t8_il2cpp_TypeInfo_var, 4));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern TypeInfo* RectTransformUtility_t848_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_RectangleContainsScreenPoint_m4172 (Object_t * __this /* static, unused */, RectTransform_t688 * ___rect, Vector2_t29  ___screenPoint, Camera_t510 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(534);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t688 * L_0 = ___rect;
		Camera_t510 * L_1 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7349(NULL /*static, unused*/, L_0, (&___screenPoint), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C" bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7349 (Object_t * __this /* static, unused */, RectTransform_t688 * ___rect, Vector2_t29 * ___screenPoint, Camera_t510 * ___cam, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7349_ftn) (RectTransform_t688 *, Vector2_t29 *, Camera_t510 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7349_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect, ___screenPoint, ___cam);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern TypeInfo* RectTransformUtility_t848_il2cpp_TypeInfo_var;
extern "C" Vector2_t29  RectTransformUtility_PixelAdjustPoint_m4149 (Object_t * __this /* static, unused */, Vector2_t29  ___point, Transform_t25 * ___elementTransform, Canvas_t690 * ___canvas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(534);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t29  V_0 = {0};
	{
		Vector2_t29  L_0 = ___point;
		Transform_t25 * L_1 = ___elementTransform;
		Canvas_t690 * L_2 = ___canvas;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m7350(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t848_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_PixelAdjustPoint_m7350 (Object_t * __this /* static, unused */, Vector2_t29  ___point, Transform_t25 * ___elementTransform, Canvas_t690 * ___canvas, Vector2_t29 * ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(534);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t25 * L_0 = ___elementTransform;
		Canvas_t690 * L_1 = ___canvas;
		Vector2_t29 * L_2 = ___output;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7351(NULL /*static, unused*/, (&___point), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7351 (Object_t * __this /* static, unused */, Vector2_t29 * ___point, Transform_t25 * ___elementTransform, Canvas_t690 * ___canvas, Vector2_t29 * ___output, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7351_ftn) (Vector2_t29 *, Transform_t25 *, Canvas_t690 *, Vector2_t29 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point, ___elementTransform, ___canvas, ___output);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C" Rect_t30  RectTransformUtility_PixelAdjustRect_m4150 (Object_t * __this /* static, unused */, RectTransform_t688 * ___rectTransform, Canvas_t690 * ___canvas, const MethodInfo* method)
{
	typedef Rect_t30  (*RectTransformUtility_PixelAdjustRect_m4150_ftn) (RectTransform_t688 *, Canvas_t690 *);
	static RectTransformUtility_PixelAdjustRect_m4150_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_PixelAdjustRect_m4150_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)");
	return _il2cpp_icall_func(___rectTransform, ___canvas);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern TypeInfo* RectTransformUtility_t848_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m7352 (Object_t * __this /* static, unused */, RectTransform_t688 * ___rect, Vector2_t29  ___screenPoint, Camera_t510 * ___cam, Vector3_t6 * ___worldPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(534);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t567  V_0 = {0};
	Plane_t856  V_1 = {0};
	float V_2 = 0.0f;
	{
		Vector3_t6 * L_0 = ___worldPoint;
		Vector2_t29  L_1 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6  L_2 = Vector2_op_Implicit_m4082(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t6 *)L_0) = L_2;
		Camera_t510 * L_3 = ___cam;
		Vector2_t29  L_4 = ___screenPoint;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		Ray_t567  L_5 = RectTransformUtility_ScreenPointToRay_m7353(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t688 * L_6 = ___rect;
		NullCheck(L_6);
		Quaternion_t51  L_7 = Transform_get_rotation_m2607(L_6, /*hidden argument*/NULL);
		Vector3_t6  L_8 = Vector3_get_back_m7847(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6  L_9 = Quaternion_op_Multiply_m4167(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t688 * L_10 = ___rect;
		NullCheck(L_10);
		Vector3_t6  L_11 = Transform_get_position_m334(L_10, /*hidden argument*/NULL);
		Plane__ctor_m4247((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t567  L_12 = V_0;
		bool L_13 = Plane_Raycast_m4248((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		Vector3_t6 * L_14 = ___worldPoint;
		float L_15 = V_2;
		Vector3_t6  L_16 = Ray_GetPoint_m4249((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t6 *)L_14) = L_16;
		return 1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t848_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m4206 (Object_t * __this /* static, unused */, RectTransform_t688 * ___rect, Vector2_t29  ___screenPoint, Camera_t510 * ___cam, Vector2_t29 * ___localPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(534);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6  V_0 = {0};
	{
		Vector2_t29 * L_0 = ___localPoint;
		Vector2_t29  L_1 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t29 *)L_0) = L_1;
		RectTransform_t688 * L_2 = ___rect;
		Vector2_t29  L_3 = ___screenPoint;
		Camera_t510 * L_4 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m7352(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t29 * L_6 = ___localPoint;
		RectTransform_t688 * L_7 = ___rect;
		Vector3_t6  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t6  L_9 = Transform_InverseTransformPoint_m4246(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t29  L_10 = Vector2_op_Implicit_m4043(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t29 *)L_6) = L_10;
		return 1;
	}

IL_002e:
	{
		return 0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C" Ray_t567  RectTransformUtility_ScreenPointToRay_m7353 (Object_t * __this /* static, unused */, Camera_t510 * ___cam, Vector2_t29  ___screenPos, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Camera_t510 * L_0 = ___cam;
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t510 * L_2 = ___cam;
		Vector2_t29  L_3 = ___screenPos;
		Vector3_t6  L_4 = Vector2_op_Implicit_m4082(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t567  L_5 = Camera_ScreenPointToRay_m2544(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t29  L_6 = ___screenPos;
		Vector3_t6  L_7 = Vector2_op_Implicit_m4082(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t6 * L_8 = (&V_0);
		float L_9 = (L_8->___z_3);
		L_8->___z_3 = ((float)((float)L_9-(float)(100.0f)));
		Vector3_t6  L_10 = V_0;
		Vector3_t6  L_11 = Vector3_get_forward_m2566(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t567  L_12 = {0};
		Ray__ctor_m7933(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t688_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t848_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m4314 (Object_t * __this /* static, unused */, RectTransform_t688 * ___rect, int32_t ___axis, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t688_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(524);
		RectTransformUtility_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(534);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t688 * V_1 = {0};
	Vector2_t29  V_2 = {0};
	Vector2_t29  V_3 = {0};
	Vector2_t29  V_4 = {0};
	Vector2_t29  V_5 = {0};
	float V_6 = 0.0f;
	{
		RectTransform_t688 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m427(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t688 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t25 * L_5 = Transform_GetChild_m2547(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t688 *)IsInstSealed(L_5, RectTransform_t688_il2cpp_TypeInfo_var));
		RectTransform_t688 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m395(NULL /*static, unused*/, L_6, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t688 * L_8 = V_1;
		int32_t L_9 = ___axis;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m4314(NULL /*static, unused*/, L_8, L_9, 0, 1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t688 * L_12 = ___rect;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m2548(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t688 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t29  L_15 = RectTransform_get_pivot_m4192(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis;
		int32_t L_17 = ___axis;
		float L_18 = Vector2_get_Item_m4203((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m4212((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t688 * L_19 = ___rect;
		Vector2_t29  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m4287(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t688 * L_22 = ___rect;
		NullCheck(L_22);
		Vector2_t29  L_23 = RectTransform_get_anchoredPosition_m4283(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis;
		int32_t L_25 = ___axis;
		float L_26 = Vector2_get_Item_m4203((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m4212((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t688 * L_27 = ___rect;
		Vector2_t29  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m4286(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t688 * L_29 = ___rect;
		NullCheck(L_29);
		Vector2_t29  L_30 = RectTransform_get_anchorMin_m4193(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t688 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t29  L_32 = RectTransform_get_anchorMax_m4282(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis;
		float L_34 = Vector2_get_Item_m4203((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis;
		int32_t L_36 = ___axis;
		float L_37 = Vector2_get_Item_m4203((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m4212((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis;
		float L_39 = V_6;
		Vector2_set_Item_m4212((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t688 * L_40 = ___rect;
		Vector2_t29  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m4285(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t688 * L_42 = ___rect;
		Vector2_t29  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m4194(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t688_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t848_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutAxes_m4313 (Object_t * __this /* static, unused */, RectTransform_t688 * ___rect, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t688_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(524);
		RectTransformUtility_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(534);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t688 * V_1 = {0};
	{
		RectTransform_t688 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m427(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t688 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t25 * L_5 = Transform_GetChild_m2547(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t688 *)IsInstSealed(L_5, RectTransform_t688_il2cpp_TypeInfo_var));
		RectTransform_t688 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m395(NULL /*static, unused*/, L_6, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t688 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m4313(NULL /*static, unused*/, L_8, 0, 1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t688 * L_11 = ___rect;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m2548(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t688 * L_13 = ___rect;
		RectTransform_t688 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t29  L_15 = RectTransform_get_pivot_m4192(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		Vector2_t29  L_16 = RectTransformUtility_GetTransposed_m7354(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m4287(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t688 * L_17 = ___rect;
		RectTransform_t688 * L_18 = ___rect;
		NullCheck(L_18);
		Vector2_t29  L_19 = RectTransform_get_sizeDelta_m4284(L_18, /*hidden argument*/NULL);
		Vector2_t29  L_20 = RectTransformUtility_GetTransposed_m7354(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m4195(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t688 * L_22 = ___rect;
		RectTransform_t688 * L_23 = ___rect;
		NullCheck(L_23);
		Vector2_t29  L_24 = RectTransform_get_anchoredPosition_m4283(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t848_il2cpp_TypeInfo_var);
		Vector2_t29  L_25 = RectTransformUtility_GetTransposed_m7354(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m4286(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t688 * L_26 = ___rect;
		RectTransform_t688 * L_27 = ___rect;
		NullCheck(L_27);
		Vector2_t29  L_28 = RectTransform_get_anchorMin_m4193(L_27, /*hidden argument*/NULL);
		Vector2_t29  L_29 = RectTransformUtility_GetTransposed_m7354(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m4285(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t688 * L_30 = ___rect;
		RectTransform_t688 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t29  L_32 = RectTransform_get_anchorMax_m4282(L_31, /*hidden argument*/NULL);
		Vector2_t29  L_33 = RectTransformUtility_GetTransposed_m7354(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m4194(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C" Vector2_t29  RectTransformUtility_GetTransposed_m7354 (Object_t * __this /* static, unused */, Vector2_t29  ___input, const MethodInfo* method)
{
	{
		float L_0 = ((&___input)->___y_2);
		float L_1 = ((&___input)->___x_1);
		Vector2_t29  L_2 = {0};
		Vector2__ctor_m356(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C" void WillRenderCanvases__ctor_m4111 (WillRenderCanvases_t841 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C" void WillRenderCanvases_Invoke_m7355 (WillRenderCanvases_t841 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WillRenderCanvases_Invoke_m7355((WillRenderCanvases_t841 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t841(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * WillRenderCanvases_BeginInvoke_m7356 (WillRenderCanvases_t841 * __this, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C" void WillRenderCanvases_EndInvoke_m7357 (WillRenderCanvases_t841 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t690_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t841_il2cpp_TypeInfo_var;
extern "C" void Canvas_add_willRenderCanvases_m4112 (Object_t * __this /* static, unused */, WillRenderCanvases_t841 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t690_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(525);
		WillRenderCanvases_t841_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t841 * L_0 = ((Canvas_t690_StaticFields*)Canvas_t690_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_1;
		WillRenderCanvases_t841 * L_1 = ___value;
		Delegate_t506 * L_2 = Delegate_Combine_m2300(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t690_StaticFields*)Canvas_t690_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_1 = ((WillRenderCanvases_t841 *)CastclassSealed(L_2, WillRenderCanvases_t841_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t690_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t841_il2cpp_TypeInfo_var;
extern "C" void Canvas_remove_willRenderCanvases_m7358 (Object_t * __this /* static, unused */, WillRenderCanvases_t841 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t690_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(525);
		WillRenderCanvases_t841_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t841 * L_0 = ((Canvas_t690_StaticFields*)Canvas_t690_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_1;
		WillRenderCanvases_t841 * L_1 = ___value;
		Delegate_t506 * L_2 = Delegate_Remove_m2302(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t690_StaticFields*)Canvas_t690_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_1 = ((WillRenderCanvases_t841 *)CastclassSealed(L_2, WillRenderCanvases_t841_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C" int32_t Canvas_get_renderMode_m4159 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m4159_ftn) (Canvas_t690 *);
	static Canvas_get_renderMode_m4159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m4159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C" bool Canvas_get_isRootCanvas_m4385 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m4385_ftn) (Canvas_t690 *);
	static Canvas_get_isRootCanvas_m4385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m4385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C" Camera_t510 * Canvas_get_worldCamera_m4171 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef Camera_t510 * (*Canvas_get_worldCamera_m4171_ftn) (Canvas_t690 *);
	static Canvas_get_worldCamera_m4171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m4171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C" float Canvas_get_scaleFactor_m4371 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m4371_ftn) (Canvas_t690 *);
	static Canvas_get_scaleFactor_m4371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m4371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C" void Canvas_set_scaleFactor_m4388 (Canvas_t690 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m4388_ftn) (Canvas_t690 *, float);
	static Canvas_set_scaleFactor_m4388_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m4388_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C" float Canvas_get_referencePixelsPerUnit_m4187 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m4187_ftn) (Canvas_t690 *);
	static Canvas_get_referencePixelsPerUnit_m4187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m4187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C" void Canvas_set_referencePixelsPerUnit_m4389 (Canvas_t690 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m4389_ftn) (Canvas_t690 *, float);
	static Canvas_set_referencePixelsPerUnit_m4389_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m4389_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C" bool Canvas_get_pixelPerfect_m4148 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m4148_ftn) (Canvas_t690 *);
	static Canvas_get_pixelPerfect_m4148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m4148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C" int32_t Canvas_get_renderOrder_m4161 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m4161_ftn) (Canvas_t690 *);
	static Canvas_get_renderOrder_m4161_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m4161_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C" int32_t Canvas_get_sortingOrder_m4160 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m4160_ftn) (Canvas_t690 *);
	static Canvas_get_sortingOrder_m4160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m4160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_cachedSortingLayerValue()
extern "C" int32_t Canvas_get_cachedSortingLayerValue_m4170 (Canvas_t690 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_cachedSortingLayerValue_m4170_ftn) (Canvas_t690 *);
	static Canvas_get_cachedSortingLayerValue_m4170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_cachedSortingLayerValue_m4170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_cachedSortingLayerValue()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C" Material_t45 * Canvas_GetDefaultCanvasMaterial_m4129 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t45 * (*Canvas_GetDefaultCanvasMaterial_m4129_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m4129_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m4129_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasTextMaterial()
extern "C" Material_t45 * Canvas_GetDefaultCanvasTextMaterial_m4366 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t45 * (*Canvas_GetDefaultCanvasTextMaterial_m4366_ftn) ();
	static Canvas_GetDefaultCanvasTextMaterial_m4366_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasTextMaterial_m4366_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasTextMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern TypeInfo* Canvas_t690_il2cpp_TypeInfo_var;
extern "C" void Canvas_SendWillRenderCanvases_m7359 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t690_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(525);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t841 * L_0 = ((Canvas_t690_StaticFields*)Canvas_t690_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_1;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WillRenderCanvases_t841 * L_1 = ((Canvas_t690_StaticFields*)Canvas_t690_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_1;
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m7355(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C" void Canvas_ForceUpdateCanvases_m4319 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m7359(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
extern "C" void CanvasGroup_set_alpha_m2661 (CanvasGroup_t579 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_alpha_m2661_ftn) (CanvasGroup_t579 *, float);
	static CanvasGroup_set_alpha_m2661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_alpha_m2661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_alpha(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C" bool CanvasGroup_get_interactable_m4348 (CanvasGroup_t579 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m4348_ftn) (CanvasGroup_t579 *);
	static CanvasGroup_get_interactable_m4348_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m4348_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C" bool CanvasGroup_get_blocksRaycasts_m7360 (CanvasGroup_t579 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m7360_ftn) (CanvasGroup_t579 *);
	static CanvasGroup_get_blocksRaycasts_m7360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m7360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
extern "C" void CanvasGroup_set_blocksRaycasts_m2662 (CanvasGroup_t579 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_blocksRaycasts_m2662_ftn) (CanvasGroup_t579 *, bool);
	static CanvasGroup_set_blocksRaycasts_m2662_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_blocksRaycasts_m2662_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C" bool CanvasGroup_get_ignoreParentGroups_m4147 (CanvasGroup_t579 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m4147_ftn) (CanvasGroup_t579 *);
	static CanvasGroup_get_ignoreParentGroups_m4147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m4147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C" bool CanvasGroup_IsRaycastLocationValid_m7361 (CanvasGroup_t579 * __this, Vector2_t29  ___sp, Camera_t510 * ___eventCamera, const MethodInfo* method)
{
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m7360(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.UIVertex::.cctor()
extern TypeInfo* UIVertex_t727_il2cpp_TypeInfo_var;
extern "C" void UIVertex__cctor_m7362 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t727_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(518);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t727  V_0 = {0};
	{
		Color32_t829  L_0 = {0};
		Color32__ctor_m4118(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t727_StaticFields*)UIVertex_t727_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6 = L_0;
		Vector4_t822  L_1 = {0};
		Vector4__ctor_m4143(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t727_StaticFields*)UIVertex_t727_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_1;
		Initobj (UIVertex_t727_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t6  L_2 = Vector3_get_zero_m300(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_2;
		Vector3_t6  L_3 = Vector3_get_back_m7847(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___normal_1 = L_3;
		Vector4_t822  L_4 = ((UIVertex_t727_StaticFields*)UIVertex_t727_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		(&V_0)->___tangent_5 = L_4;
		Color32_t829  L_5 = ((UIVertex_t727_StaticFields*)UIVertex_t727_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6;
		(&V_0)->___color_2 = L_5;
		Vector2_t29  L_6 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv0_3 = L_6;
		Vector2_t29  L_7 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv1_4 = L_7;
		UIVertex_t727  L_8 = V_0;
		((UIVertex_t727_StaticFields*)UIVertex_t727_il2cpp_TypeInfo_var->static_fields)->___simpleVert_8 = L_8;
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C" void CanvasRenderer_SetColor_m4153 (CanvasRenderer_t689 * __this, Color_t5  ___color, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m7363(NULL /*static, unused*/, __this, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m7363 (Object_t * __this /* static, unused */, CanvasRenderer_t689 * ___self, Color_t5 * ___color, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m7363_ftn) (CanvasRenderer_t689 *, Color_t5 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m7363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m7363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___color);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C" Color_t5  CanvasRenderer_GetColor_m4151 (CanvasRenderer_t689 * __this, const MethodInfo* method)
{
	typedef Color_t5  (*CanvasRenderer_GetColor_m4151_ftn) (CanvasRenderer_t689 *);
	static CanvasRenderer_GetColor_m4151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_GetColor_m4151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::GetColor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_isMask(System.Boolean)
extern "C" void CanvasRenderer_set_isMask_m4426 (CanvasRenderer_t689 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_isMask_m4426_ftn) (CanvasRenderer_t689 *, bool);
	static CanvasRenderer_set_isMask_m4426_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_isMask_m4426_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_isMask(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C" void CanvasRenderer_SetMaterial_m4142 (CanvasRenderer_t689 * __this, Material_t45 * ___material, Texture_t731 * ___texture, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m4142_ftn) (CanvasRenderer_t689 *, Material_t45 *, Texture_t731 *);
	static CanvasRenderer_SetMaterial_m4142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m4142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___material, ___texture);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t562_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1166;
extern "C" void CanvasRenderer_SetVertices_m4140 (CanvasRenderer_t689 * __this, List_1_t724 * ___vertices, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		UInt16_t562_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(318);
		_stringLiteral1166 = il2cpp_codegen_string_literal_from_index(1166);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t724 * L_0 = ___vertices;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0039;
		}
	}
	{
		ObjectU5BU5D_t34* L_2 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 1));
		uint16_t L_3 = ((int32_t)65535);
		Object_t * L_4 = Box(UInt16_t562_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_4;
		String_t* L_5 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1166, L_2, /*hidden argument*/NULL);
		Debug_LogWarning_m4362(NULL /*static, unused*/, L_5, __this, /*hidden argument*/NULL);
		List_1_t724 * L_6 = ___vertices;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear() */, L_6);
	}

IL_0039:
	{
		List_1_t724 * L_7 = ___vertices;
		CanvasRenderer_SetVerticesInternal_m7364(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)
extern "C" void CanvasRenderer_SetVerticesInternal_m7364 (CanvasRenderer_t689 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetVerticesInternal_m7364_ftn) (CanvasRenderer_t689 *, Object_t *);
	static CanvasRenderer_SetVerticesInternal_m7364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetVerticesInternal_m7364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(UnityEngine.UIVertex[],System.Int32)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t562_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1166;
extern "C" void CanvasRenderer_SetVertices_m4237 (CanvasRenderer_t689 * __this, UIVertexU5BU5D_t722* ___vertices, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		UInt16_t562_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(318);
		_stringLiteral1166 = il2cpp_codegen_string_literal_from_index(1166);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t34* L_1 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 1));
		uint16_t L_2 = ((int32_t)65535);
		Object_t * L_3 = Box(UInt16_t562_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		String_t* L_4 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1166, L_1, /*hidden argument*/NULL);
		Debug_LogWarning_m4362(NULL /*static, unused*/, L_4, __this, /*hidden argument*/NULL);
		___size = 0;
	}

IL_0031:
	{
		UIVertexU5BU5D_t722* L_5 = ___vertices;
		int32_t L_6 = ___size;
		CanvasRenderer_SetVerticesInternalArray_m7365(__this, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)
extern "C" void CanvasRenderer_SetVerticesInternalArray_m7365 (CanvasRenderer_t689 * __this, UIVertexU5BU5D_t722* ___vertices, int32_t ___size, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetVerticesInternalArray_m7365_ftn) (CanvasRenderer_t689 *, UIVertexU5BU5D_t722*, int32_t);
	static CanvasRenderer_SetVerticesInternalArray_m7365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetVerticesInternalArray_m7365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)");
	_il2cpp_icall_func(__this, ___vertices, ___size);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C" void CanvasRenderer_Clear_m4137 (CanvasRenderer_t689 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m4137_ftn) (CanvasRenderer_t689 *);
	static CanvasRenderer_Clear_m4137_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m4137_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C" int32_t CanvasRenderer_get_absoluteDepth_m4131 (CanvasRenderer_t689 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m4131_ftn) (CanvasRenderer_t689 *);
	static CanvasRenderer_get_absoluteDepth_m4131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m4131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m7366 (AssetBundleCreateRequest_t1339 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m7986(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t1342 * AssetBundleCreateRequest_get_assetBundle_m7367 (AssetBundleCreateRequest_t1339 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t1342 * (*AssetBundleCreateRequest_get_assetBundle_m7367_ftn) (AssetBundleCreateRequest_t1339 *);
	static AssetBundleCreateRequest_get_assetBundle_m7367_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_assetBundle_m7367_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m7368 (AssetBundleCreateRequest_t1339 * __this, const MethodInfo* method)
{
	typedef void (*AssetBundleCreateRequest_DisableCompatibilityChecks_m7368_ftn) (AssetBundleCreateRequest_t1339 *);
	static AssetBundleCreateRequest_DisableCompatibilityChecks_m7368_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_DisableCompatibilityChecks_m7368_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m7369 (AssetBundleRequest_t1341 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m7986(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t53 * AssetBundleRequest_get_asset_m7370 (AssetBundleRequest_t1341 * __this, const MethodInfo* method)
{
	{
		AssetBundle_t1342 * L_0 = (__this->___m_AssetBundle_1);
		String_t* L_1 = (__this->___m_Path_2);
		Type_t * L_2 = (__this->___m_Type_3);
		NullCheck(L_0);
		Object_t53 * L_3 = AssetBundle_Load_m7371(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.AssetBundle::Load(System.String,System.Type)
extern "C" Object_t53 * AssetBundle_Load_m7371 (AssetBundle_t1342 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	typedef Object_t53 * (*AssetBundle_Load_m7371_ftn) (AssetBundle_t1342 *, String_t*, Type_t *);
	static AssetBundle_Load_m7371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_Load_m7371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::Load(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name, ___type);
}
// System.Int32 UnityEngine.LayerMask::get_value()
extern "C" int32_t LayerMask_get_value_m7372 (LayerMask_t665 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Mask_0);
		return L_0;
	}
}
// System.Void UnityEngine.LayerMask::set_value(System.Int32)
extern "C" void LayerMask_set_value_m7373 (LayerMask_t665 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Mask_0 = L_0;
		return;
	}
}
// System.String UnityEngine.LayerMask::LayerToName(System.Int32)
extern "C" String_t* LayerMask_LayerToName_m7374 (Object_t * __this /* static, unused */, int32_t ___layer, const MethodInfo* method)
{
	typedef String_t* (*LayerMask_LayerToName_m7374_ftn) (int32_t);
	static LayerMask_LayerToName_m7374_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LayerMask_LayerToName_m7374_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LayerMask::LayerToName(System.Int32)");
	return _il2cpp_icall_func(___layer);
}
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C" int32_t LayerMask_NameToLayer_m7375 (Object_t * __this /* static, unused */, String_t* ___layerName, const MethodInfo* method)
{
	typedef int32_t (*LayerMask_NameToLayer_m7375_ftn) (String_t*);
	static LayerMask_NameToLayer_m7375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LayerMask_NameToLayer_m7375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LayerMask::NameToLayer(System.String)");
	return _il2cpp_icall_func(___layerName);
}
// System.Int32 UnityEngine.LayerMask::GetMask(System.String[])
extern "C" int32_t LayerMask_GetMask_m7376 (Object_t * __this /* static, unused */, StringU5BU5D_t260* ___layerNames, const MethodInfo* method)
{
	int32_t V_0 = 0;
	String_t* V_1 = {0};
	StringU5BU5D_t260* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		StringU5BU5D_t260* L_0 = ___layerNames;
		V_2 = L_0;
		V_3 = 0;
		goto IL_002f;
	}

IL_000b:
	{
		StringU5BU5D_t260* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(L_1, L_3, sizeof(String_t*)));
		String_t* L_4 = V_1;
		int32_t L_5 = LayerMask_NameToLayer_m7375(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		int32_t L_6 = V_4;
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = V_4;
		V_0 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)31)))&(int32_t)((int32_t)31)))))));
	}

IL_002b:
	{
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_10 = V_3;
		StringU5BU5D_t260* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C" int32_t LayerMask_op_Implicit_m4097 (Object_t * __this /* static, unused */, LayerMask_t665  ___mask, const MethodInfo* method)
{
	{
		int32_t L_0 = ((&___mask)->___m_Mask_0);
		return L_0;
	}
}
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C" LayerMask_t665  LayerMask_op_Implicit_m4095 (Object_t * __this /* static, unused */, int32_t ___intVal, const MethodInfo* method)
{
	LayerMask_t665  V_0 = {0};
	{
		int32_t L_0 = ___intVal;
		(&V_0)->___m_Mask_0 = L_0;
		LayerMask_t665  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m295 (WaitForSeconds_t37 * __this, float ___seconds, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m8107(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds;
		__this->___m_Seconds_0 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t37_marshal(const WaitForSeconds_t37& unmarshaled, WaitForSeconds_t37_marshaled& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.___m_Seconds_0;
}
extern "C" void WaitForSeconds_t37_marshal_back(const WaitForSeconds_t37_marshaled& marshaled, WaitForSeconds_t37& unmarshaled)
{
	unmarshaled.___m_Seconds_0 = marshaled.___m_Seconds_0;
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t37_marshal_cleanup(WaitForSeconds_t37_marshaled& marshaled)
{
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" void WaitForFixedUpdate__ctor_m7377 (WaitForFixedUpdate_t1350 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m8107(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" void WaitForEndOfFrame__ctor_m2246 (WaitForEndOfFrame_t500 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m8107(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m7378 (Coroutine_t39 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m8107(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m7379 (Coroutine_t39 * __this, const MethodInfo* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m7379_ftn) (Coroutine_t39 *);
	static Coroutine_ReleaseCoroutine_m7379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m7379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m7380 (Coroutine_t39 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m7379(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t39_marshal(const Coroutine_t39& unmarshaled, Coroutine_t39_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void Coroutine_t39_marshal_back(const Coroutine_t39_marshaled& marshaled, Coroutine_t39& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t39_marshal_cleanup(Coroutine_t39_marshaled& marshaled)
{
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C" void DisallowMultipleComponent__ctor_m7381 (DisallowMultipleComponent_t1351 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C" void RequireComponent__ctor_m7382 (RequireComponent_t1352 * __this, Type_t * ___requiredComponent, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent;
		__this->___m_Type0_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C" void AddComponentMenu__ctor_m7383 (AddComponentMenu_t1353 * __this, String_t* ___menuName, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		__this->___m_Ordering_1 = 0;
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C" void AddComponentMenu__ctor_m7384 (AddComponentMenu_t1353 * __this, String_t* ___menuName, int32_t ___order, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		int32_t L_1 = ___order;
		__this->___m_Ordering_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C" void ExecuteInEditMode__ctor_m7385 (ExecuteInEditMode_t1354 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.HideInInspector::.ctor()
extern "C" void HideInInspector__ctor_m7386 (HideInInspector_t1355 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" void ScriptableObject__ctor_m2161 (ScriptableObject_t77 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m7387(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m7387 (Object_t * __this /* static, unused */, ScriptableObject_t77 * ___self, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m7387_ftn) (ScriptableObject_t77 *);
	static ScriptableObject_Internal_CreateScriptableObject_m7387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m7387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C" ScriptableObject_t77 * ScriptableObject_CreateInstance_m7388 (Object_t * __this /* static, unused */, String_t* ___className, const MethodInfo* method)
{
	typedef ScriptableObject_t77 * (*ScriptableObject_CreateInstance_m7388_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m7388_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m7388_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C" ScriptableObject_t77 * ScriptableObject_CreateInstance_m7389 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		ScriptableObject_t77 * L_1 = ScriptableObject_CreateInstanceFromType_m7390(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C" ScriptableObject_t77 * ScriptableObject_CreateInstanceFromType_m7390 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ScriptableObject_t77 * (*ScriptableObject_CreateInstanceFromType_m7390_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m7390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m7390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t77_marshal(const ScriptableObject_t77& unmarshaled, ScriptableObject_t77_marshaled& marshaled)
{
}
extern "C" void ScriptableObject_t77_marshal_back(const ScriptableObject_t77_marshaled& marshaled, ScriptableObject_t77& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t77_marshal_cleanup(ScriptableObject_t77_marshaled& marshaled)
{
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m7391 (ResourceRequest_t1356 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m7986(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t53 * ResourceRequest_get_asset_m7392 (ResourceRequest_t1356 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Path_1);
		Type_t * L_1 = (__this->___m_Type_2);
		Object_t53 * L_2 = Resources_Load_m7393(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t53_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Object_t53 * Resources_Load_m2162 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t53_0_0_0_var = il2cpp_codegen_type_from_index(506);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Object_t53_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_2 = Resources_Load_m7393(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C" Object_t53 * Resources_Load_m7393 (Object_t * __this /* static, unused */, String_t* ___path, Type_t * ___systemTypeInstance, const MethodInfo* method)
{
	typedef Object_t53 * (*Resources_Load_m7393_ftn) (String_t*, Type_t *);
	static Resources_Load_m7393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m7393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path, ___systemTypeInstance);
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C" Object_t53 * Resources_GetBuiltinResource_m7394 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___path, const MethodInfo* method)
{
	typedef Object_t53 * (*Resources_GetBuiltinResource_m7394_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m7394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m7394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	return _il2cpp_icall_func(___type, ___path);
}
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern "C" AsyncOperation_t1340 * Resources_UnloadUnusedAssets_m6727 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef AsyncOperation_t1340 * (*Resources_UnloadUnusedAssets_m6727_ftn) ();
	static Resources_UnloadUnusedAssets_m6727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadUnusedAssets_m6727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadUnusedAssets()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C" void GcLeaderboard__ctor_m7395 (GcLeaderboard_t1358 * __this, Leaderboard_t1267 * ___board, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		Leaderboard_t1267 * L_0 = ___board;
		__this->___m_GenericLeaderboard_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C" void GcLeaderboard_Finalize_m7396 (GcLeaderboard_t1358 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m7405(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C" bool GcLeaderboard_Contains_m7397 (GcLeaderboard_t1358 * __this, Leaderboard_t1267 * ___board, const MethodInfo* method)
{
	{
		Leaderboard_t1267 * L_0 = (__this->___m_GenericLeaderboard_1);
		Leaderboard_t1267 * L_1 = ___board;
		return ((((Object_t*)(Leaderboard_t1267 *)L_0) == ((Object_t*)(Leaderboard_t1267 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern TypeInfo* ScoreU5BU5D_t1454_il2cpp_TypeInfo_var;
extern "C" void GcLeaderboard_SetScores_m7398 (GcLeaderboard_t1358 * __this, GcScoreDataU5BU5D_t1433* ___scoreDatas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScoreU5BU5D_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(899);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t1454* V_0 = {0};
	int32_t V_1 = 0;
	{
		Leaderboard_t1267 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t1433* L_1 = ___scoreDatas;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t1454*)SZArrayNew(ScoreU5BU5D_t1454_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t1454* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1433* L_4 = ___scoreDatas;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t1266 * L_6 = GcScoreData_ToScore_m6872(((GcScoreData_t1245 *)(GcScoreData_t1245 *)SZArrayLdElema(L_4, L_5, sizeof(GcScoreData_t1245 ))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Score_t1266 **)(Score_t1266 **)SZArrayLdElema(L_2, L_3, sizeof(Score_t1266 *))) = (Score_t1266 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1433* L_9 = ___scoreDatas;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t1267 * L_10 = (__this->___m_GenericLeaderboard_1);
		ScoreU5BU5D_t1454* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m6983(L_10, (IScoreU5BU5D_t1268*)(IScoreU5BU5D_t1268*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C" void GcLeaderboard_SetLocalScore_m7399 (GcLeaderboard_t1358 * __this, GcScoreData_t1245  ___scoreData, const MethodInfo* method)
{
	{
		Leaderboard_t1267 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t1267 * L_1 = (__this->___m_GenericLeaderboard_1);
		Score_t1266 * L_2 = GcScoreData_ToScore_m6872((&___scoreData), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m6981(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C" void GcLeaderboard_SetMaxRange_m7400 (GcLeaderboard_t1358 * __this, uint32_t ___maxRange, const MethodInfo* method)
{
	{
		Leaderboard_t1267 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1267 * L_1 = (__this->___m_GenericLeaderboard_1);
		uint32_t L_2 = ___maxRange;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m6982(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C" void GcLeaderboard_SetTitle_m7401 (GcLeaderboard_t1358 * __this, String_t* ___title, const MethodInfo* method)
{
	{
		Leaderboard_t1267 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1267 * L_1 = (__this->___m_GenericLeaderboard_1);
		String_t* L_2 = ___title;
		NullCheck(L_1);
		Leaderboard_SetTitle_m6984(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void GcLeaderboard_Internal_LoadScores_m7402 (GcLeaderboard_t1358 * __this, String_t* ___category, int32_t ___from, int32_t ___count, int32_t ___playerScope, int32_t ___timeScope, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m7402_ftn) (GcLeaderboard_t1358 *, String_t*, int32_t, int32_t, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m7402_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m7402_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category, ___from, ___count, ___playerScope, ___timeScope);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m7403 (GcLeaderboard_t1358 * __this, String_t* ___category, int32_t ___timeScope, StringU5BU5D_t260* ___userIDs, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScoresWithUsers_m7403_ftn) (GcLeaderboard_t1358 *, String_t*, int32_t, StringU5BU5D_t260*);
	static GcLeaderboard_Internal_LoadScoresWithUsers_m7403_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScoresWithUsers_m7403_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])");
	_il2cpp_icall_func(__this, ___category, ___timeScope, ___userIDs);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C" bool GcLeaderboard_Loading_m7404 (GcLeaderboard_t1358 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m7404_ftn) (GcLeaderboard_t1358 *);
	static GcLeaderboard_Loading_m7404_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m7404_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C" void GcLeaderboard_Dispose_m7405 (GcLeaderboard_t1358 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m7405_ftn) (GcLeaderboard_t1358 *);
	static GcLeaderboard_Dispose_m7405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m7405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C" int32_t QualitySettings_get_activeColorSpace_m7406 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m7406_ftn) ();
	static QualitySettings_get_activeColorSpace_m7406_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m7406_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C" Mesh_t601 * MeshFilter_get_mesh_m6434 (MeshFilter_t597 * __this, const MethodInfo* method)
{
	typedef Mesh_t601 * (*MeshFilter_get_mesh_m6434_ftn) (MeshFilter_t597 *);
	static MeshFilter_get_mesh_m6434_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_mesh_m6434_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_mesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_mesh_m6779 (MeshFilter_t597 * __this, Mesh_t601 * ___value, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_mesh_m6779_ftn) (MeshFilter_t597 *, Mesh_t601 *);
	static MeshFilter_set_mesh_m6779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_mesh_m6779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C" Mesh_t601 * MeshFilter_get_sharedMesh_m2778 (MeshFilter_t597 * __this, const MethodInfo* method)
{
	typedef Mesh_t601 * (*MeshFilter_get_sharedMesh_m2778_ftn) (MeshFilter_t597 *);
	static MeshFilter_get_sharedMesh_m2778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_sharedMesh_m2778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_sharedMesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_sharedMesh_m6506 (MeshFilter_t597 * __this, Mesh_t601 * ___value, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_sharedMesh_m6506_ftn) (MeshFilter_t597 *, Mesh_t601 *);
	static MeshFilter_set_sharedMesh_m6506_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_sharedMesh_m6506_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Mesh::.ctor()
extern "C" void Mesh__ctor_m6711 (Mesh_t601 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		Mesh_Internal_Create_m7407(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C" void Mesh_Internal_Create_m7407 (Object_t * __this /* static, unused */, Mesh_t601 * ___mono, const MethodInfo* method)
{
	typedef void (*Mesh_Internal_Create_m7407_ftn) (Mesh_t601 *);
	static Mesh_Internal_Create_m7407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Internal_Create_m7407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)");
	_il2cpp_icall_func(___mono);
}
// System.Void UnityEngine.Mesh::Clear(System.Boolean)
extern "C" void Mesh_Clear_m7408 (Mesh_t601 * __this, bool ___keepVertexLayout, const MethodInfo* method)
{
	typedef void (*Mesh_Clear_m7408_ftn) (Mesh_t601 *, bool);
	static Mesh_Clear_m7408_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Clear_m7408_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___keepVertexLayout);
}
// System.Void UnityEngine.Mesh::Clear()
extern "C" void Mesh_Clear_m6435 (Mesh_t601 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		bool L_0 = V_0;
		Mesh_Clear_m7408(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C" Vector3U5BU5D_t8* Mesh_get_vertices_m2779 (Mesh_t601 * __this, const MethodInfo* method)
{
	typedef Vector3U5BU5D_t8* (*Mesh_get_vertices_m2779_ftn) (Mesh_t601 *);
	static Mesh_get_vertices_m2779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_vertices_m2779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_vertices()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C" void Mesh_set_vertices_m6436 (Mesh_t601 * __this, Vector3U5BU5D_t8* ___value, const MethodInfo* method)
{
	typedef void (*Mesh_set_vertices_m6436_ftn) (Mesh_t601 *, Vector3U5BU5D_t8*);
	static Mesh_set_vertices_m6436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_vertices_m6436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern "C" void Mesh_set_normals_m6440 (Mesh_t601 * __this, Vector3U5BU5D_t8* ___value, const MethodInfo* method)
{
	typedef void (*Mesh_set_normals_m6440_ftn) (Mesh_t601 *, Vector3U5BU5D_t8*);
	static Mesh_set_normals_m6440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_normals_m6440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern "C" Vector2U5BU5D_t21* Mesh_get_uv_m6439 (Mesh_t601 * __this, const MethodInfo* method)
{
	typedef Vector2U5BU5D_t21* (*Mesh_get_uv_m6439_ftn) (Mesh_t601 *);
	static Mesh_get_uv_m6439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_uv_m6439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_uv()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C" void Mesh_set_uv_m6438 (Mesh_t601 * __this, Vector2U5BU5D_t21* ___value, const MethodInfo* method)
{
	typedef void (*Mesh_set_uv_m6438_ftn) (Mesh_t601 *, Vector2U5BU5D_t21*);
	static Mesh_set_uv_m6438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_uv_m6438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
extern "C" Bounds_t742  Mesh_get_bounds_m6857 (Mesh_t601 * __this, const MethodInfo* method)
{
	Bounds_t742  V_0 = {0};
	{
		Mesh_INTERNAL_get_bounds_m7409(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t742  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C" void Mesh_INTERNAL_get_bounds_m7409 (Mesh_t601 * __this, Bounds_t742 * ___value, const MethodInfo* method)
{
	typedef void (*Mesh_INTERNAL_get_bounds_m7409_ftn) (Mesh_t601 *, Bounds_t742 *);
	static Mesh_INTERNAL_get_bounds_m7409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_INTERNAL_get_bounds_m7409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Mesh::RecalculateNormals()
extern "C" void Mesh_RecalculateNormals_m6441 (Mesh_t601 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_RecalculateNormals_m6441_ftn) (Mesh_t601 *);
	static Mesh_RecalculateNormals_m6441_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateNormals_m6441_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateNormals()");
	_il2cpp_icall_func(__this);
}
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern "C" Int32U5BU5D_t335* Mesh_get_triangles_m2780 (Mesh_t601 * __this, const MethodInfo* method)
{
	typedef Int32U5BU5D_t335* (*Mesh_get_triangles_m2780_ftn) (Mesh_t601 *);
	static Mesh_get_triangles_m2780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_triangles_m2780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_triangles()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C" void Mesh_set_triangles_m6437 (Mesh_t601 * __this, Int32U5BU5D_t335* ___value, const MethodInfo* method)
{
	typedef void (*Mesh_set_triangles_m6437_ftn) (Mesh_t601 *, Int32U5BU5D_t335*);
	static Mesh_set_triangles_m6437_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_triangles_m6437_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_triangles(System.Int32[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.BoneWeight::get_weight0()
extern "C" float BoneWeight_get_weight0_m7410 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight0_0);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight0(System.Single)
extern "C" void BoneWeight_set_weight0_m7411 (BoneWeight_t1361 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight0_0 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight1()
extern "C" float BoneWeight_get_weight1_m7412 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight1_1);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight1(System.Single)
extern "C" void BoneWeight_set_weight1_m7413 (BoneWeight_t1361 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight1_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight2()
extern "C" float BoneWeight_get_weight2_m7414 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight2_2);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight2(System.Single)
extern "C" void BoneWeight_set_weight2_m7415 (BoneWeight_t1361 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight2_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight3()
extern "C" float BoneWeight_get_weight3_m7416 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight3_3);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight3(System.Single)
extern "C" void BoneWeight_set_weight3_m7417 (BoneWeight_t1361 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight3_3 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex0()
extern "C" int32_t BoneWeight_get_boneIndex0_m7418 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex0_4);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex0(System.Int32)
extern "C" void BoneWeight_set_boneIndex0_m7419 (BoneWeight_t1361 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex0_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex1()
extern "C" int32_t BoneWeight_get_boneIndex1_m7420 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex1_5);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex1(System.Int32)
extern "C" void BoneWeight_set_boneIndex1_m7421 (BoneWeight_t1361 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex1_5 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex2()
extern "C" int32_t BoneWeight_get_boneIndex2_m7422 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex2_6);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex2(System.Int32)
extern "C" void BoneWeight_set_boneIndex2_m7423 (BoneWeight_t1361 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex2_6 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex3()
extern "C" int32_t BoneWeight_get_boneIndex3_m7424 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex3_7);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex3(System.Int32)
extern "C" void BoneWeight_set_boneIndex3_m7425 (BoneWeight_t1361 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex3_7 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::GetHashCode()
extern "C" int32_t BoneWeight_GetHashCode_m7426 (BoneWeight_t1361 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		int32_t L_0 = BoneWeight_get_boneIndex0_m7418(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Int32_GetHashCode_m8175((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = BoneWeight_get_boneIndex1_m7420(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Int32_GetHashCode_m8175((&V_1), /*hidden argument*/NULL);
		int32_t L_4 = BoneWeight_get_boneIndex2_m7422(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Int32_GetHashCode_m8175((&V_2), /*hidden argument*/NULL);
		int32_t L_6 = BoneWeight_get_boneIndex3_m7424(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Int32_GetHashCode_m8175((&V_3), /*hidden argument*/NULL);
		float L_8 = BoneWeight_get_weight0_m7410(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		int32_t L_9 = Single_GetHashCode_m8176((&V_4), /*hidden argument*/NULL);
		float L_10 = BoneWeight_get_weight1_m7412(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		int32_t L_11 = Single_GetHashCode_m8176((&V_5), /*hidden argument*/NULL);
		float L_12 = BoneWeight_get_weight2_m7414(__this, /*hidden argument*/NULL);
		V_6 = L_12;
		int32_t L_13 = Single_GetHashCode_m8176((&V_6), /*hidden argument*/NULL);
		float L_14 = BoneWeight_get_weight3_m7416(__this, /*hidden argument*/NULL);
		V_7 = L_14;
		int32_t L_15 = Single_GetHashCode_m8176((&V_7), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))))^(int32_t)((int32_t)((int32_t)L_9<<(int32_t)5))))^(int32_t)((int32_t)((int32_t)L_11<<(int32_t)4))))^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)4))))^(int32_t)((int32_t)((int32_t)L_15>>(int32_t)3))));
	}
}
// System.Boolean UnityEngine.BoneWeight::Equals(System.Object)
extern TypeInfo* BoneWeight_t1361_il2cpp_TypeInfo_var;
extern TypeInfo* Vector4_t822_il2cpp_TypeInfo_var;
extern "C" bool BoneWeight_Equals_m7427 (BoneWeight_t1361 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BoneWeight_t1361_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(950);
		Vector4_t822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(951);
		s_Il2CppMethodIntialized = true;
	}
	BoneWeight_t1361  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector4_t822  V_5 = {0};
	int32_t G_B8_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, BoneWeight_t1361_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(BoneWeight_t1361 *)((BoneWeight_t1361 *)UnBox (L_1, BoneWeight_t1361_il2cpp_TypeInfo_var))));
		int32_t L_2 = BoneWeight_get_boneIndex0_m7418(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = BoneWeight_get_boneIndex0_m7418((&V_0), /*hidden argument*/NULL);
		bool L_4 = Int32_Equals_m8177((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_5 = BoneWeight_get_boneIndex1_m7420(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = BoneWeight_get_boneIndex1_m7420((&V_0), /*hidden argument*/NULL);
		bool L_7 = Int32_Equals_m8177((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_8 = BoneWeight_get_boneIndex2_m7422(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		int32_t L_9 = BoneWeight_get_boneIndex2_m7422((&V_0), /*hidden argument*/NULL);
		bool L_10 = Int32_Equals_m8177((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_11 = BoneWeight_get_boneIndex3_m7424(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		int32_t L_12 = BoneWeight_get_boneIndex3_m7424((&V_0), /*hidden argument*/NULL);
		bool L_13 = Int32_Equals_m8177((&V_4), L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00cb;
		}
	}
	{
		float L_14 = BoneWeight_get_weight0_m7410(__this, /*hidden argument*/NULL);
		float L_15 = BoneWeight_get_weight1_m7412(__this, /*hidden argument*/NULL);
		float L_16 = BoneWeight_get_weight2_m7414(__this, /*hidden argument*/NULL);
		float L_17 = BoneWeight_get_weight3_m7416(__this, /*hidden argument*/NULL);
		Vector4__ctor_m4143((&V_5), L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		float L_18 = BoneWeight_get_weight0_m7410((&V_0), /*hidden argument*/NULL);
		float L_19 = BoneWeight_get_weight1_m7412((&V_0), /*hidden argument*/NULL);
		float L_20 = BoneWeight_get_weight2_m7414((&V_0), /*hidden argument*/NULL);
		float L_21 = BoneWeight_get_weight3_m7416((&V_0), /*hidden argument*/NULL);
		Vector4_t822  L_22 = {0};
		Vector4__ctor_m4143(&L_22, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		Vector4_t822  L_23 = L_22;
		Object_t * L_24 = Box(Vector4_t822_il2cpp_TypeInfo_var, &L_23);
		bool L_25 = Vector4_Equals_m7926((&V_5), L_24, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_25));
		goto IL_00cc;
	}

IL_00cb:
	{
		G_B8_0 = 0;
	}

IL_00cc:
	{
		return G_B8_0;
	}
}
// System.Boolean UnityEngine.BoneWeight::op_Equality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
extern "C" bool BoneWeight_op_Equality_m7428 (Object_t * __this /* static, unused */, BoneWeight_t1361  ___lhs, BoneWeight_t1361  ___rhs, const MethodInfo* method)
{
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = BoneWeight_get_boneIndex0_m7418((&___lhs), /*hidden argument*/NULL);
		int32_t L_1 = BoneWeight_get_boneIndex0_m7418((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_2 = BoneWeight_get_boneIndex1_m7420((&___lhs), /*hidden argument*/NULL);
		int32_t L_3 = BoneWeight_get_boneIndex1_m7420((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_4 = BoneWeight_get_boneIndex2_m7422((&___lhs), /*hidden argument*/NULL);
		int32_t L_5 = BoneWeight_get_boneIndex2_m7422((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_6 = BoneWeight_get_boneIndex3_m7424((&___lhs), /*hidden argument*/NULL);
		int32_t L_7 = BoneWeight_get_boneIndex3_m7424((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0095;
		}
	}
	{
		float L_8 = BoneWeight_get_weight0_m7410((&___lhs), /*hidden argument*/NULL);
		float L_9 = BoneWeight_get_weight1_m7412((&___lhs), /*hidden argument*/NULL);
		float L_10 = BoneWeight_get_weight2_m7414((&___lhs), /*hidden argument*/NULL);
		float L_11 = BoneWeight_get_weight3_m7416((&___lhs), /*hidden argument*/NULL);
		Vector4_t822  L_12 = {0};
		Vector4__ctor_m4143(&L_12, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = BoneWeight_get_weight0_m7410((&___rhs), /*hidden argument*/NULL);
		float L_14 = BoneWeight_get_weight1_m7412((&___rhs), /*hidden argument*/NULL);
		float L_15 = BoneWeight_get_weight2_m7414((&___rhs), /*hidden argument*/NULL);
		float L_16 = BoneWeight_get_weight3_m7416((&___rhs), /*hidden argument*/NULL);
		Vector4_t822  L_17 = {0};
		Vector4__ctor_m4143(&L_17, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		bool L_18 = Vector4_op_Equality_m7931(NULL /*static, unused*/, L_12, L_17, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_18));
		goto IL_0096;
	}

IL_0095:
	{
		G_B6_0 = 0;
	}

IL_0096:
	{
		return G_B6_0;
	}
}
// System.Boolean UnityEngine.BoneWeight::op_Inequality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
extern "C" bool BoneWeight_op_Inequality_m7429 (Object_t * __this /* static, unused */, BoneWeight_t1361  ___lhs, BoneWeight_t1361  ___rhs, const MethodInfo* method)
{
	{
		BoneWeight_t1361  L_0 = ___lhs;
		BoneWeight_t1361  L_1 = ___rhs;
		bool L_2 = BoneWeight_op_Equality_m7428(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C" bool Renderer_get_enabled_m2554 (Renderer_t46 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_enabled_m2554_ftn) (Renderer_t46 *);
	static Renderer_get_enabled_m2554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_enabled_m2554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C" void Renderer_set_enabled_m2324 (Renderer_t46 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m2324_ftn) (Renderer_t46 *, bool);
	static Renderer_set_enabled_m2324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m2324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C" Material_t45 * Renderer_get_material_m315 (Renderer_t46 * __this, const MethodInfo* method)
{
	typedef Material_t45 * (*Renderer_get_material_m315_ftn) (Renderer_t46 *);
	static Renderer_get_material_m315_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m315_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C" void Renderer_set_material_m2326 (Renderer_t46 * __this, Material_t45 * ___value, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m2326_ftn) (Renderer_t46 *, Material_t45 *);
	static Renderer_set_material_m2326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m2326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C" void Renderer_set_sharedMaterial_m2748 (Renderer_t46 * __this, Material_t45 * ___value, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterial_m2748_ftn) (Renderer_t46 *, Material_t45 *);
	static Renderer_set_sharedMaterial_m2748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterial_m2748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C" void Renderer_set_sharedMaterials_m2749 (Renderer_t46 * __this, MaterialU5BU5D_t49* ___value, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterials_m2749_ftn) (Renderer_t46 *, MaterialU5BU5D_t49*);
	static Renderer_set_sharedMaterials_m2749_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterials_m2749_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C" MaterialU5BU5D_t49* Renderer_get_materials_m350 (Renderer_t46 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t49* (*Renderer_get_materials_m350_ftn) (Renderer_t46 *);
	static Renderer_get_materials_m350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C" int32_t Renderer_get_sortingLayerID_m4093 (Renderer_t46 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m4093_ftn) (Renderer_t46 *);
	static Renderer_get_sortingLayerID_m4093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m4093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C" int32_t Renderer_get_sortingOrder_m4094 (Renderer_t46 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m4094_ftn) (Renderer_t46 *);
	static Renderer_get_sortingOrder_m4094_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m4094_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C" Vector3_t6  Particle_get_position_m7430 (Particle_t1362 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Position_0);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
extern "C" void Particle_set_position_m7431 (Particle_t1362 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___value;
		__this->___m_Position_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C" Vector3_t6  Particle_get_velocity_m7432 (Particle_t1362 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Velocity_1);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C" void Particle_set_velocity_m7433 (Particle_t1362 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___value;
		__this->___m_Velocity_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_energy()
extern "C" float Particle_get_energy_m7434 (Particle_t1362 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Energy_5);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_energy(System.Single)
extern "C" void Particle_set_energy_m7435 (Particle_t1362 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Energy_5 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C" float Particle_get_startEnergy_m7436 (Particle_t1362 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_StartEnergy_6);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C" void Particle_set_startEnergy_m7437 (Particle_t1362 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_StartEnergy_6 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_size()
extern "C" float Particle_get_size_m7438 (Particle_t1362 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Size_2);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C" void Particle_set_size_m7439 (Particle_t1362 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Size_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_rotation()
extern "C" float Particle_get_rotation_m7440 (Particle_t1362 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Rotation_3);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C" void Particle_set_rotation_m7441 (Particle_t1362 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Rotation_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C" float Particle_get_angularVelocity_m7442 (Particle_t1362 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AngularVelocity_4);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C" void Particle_set_angularVelocity_m7443 (Particle_t1362 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_AngularVelocity_4 = L_0;
		return;
	}
}
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C" Color_t5  Particle_get_color_m7444 (Particle_t1362 * __this, const MethodInfo* method)
{
	{
		Color_t5  L_0 = (__this->___m_Color_7);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
extern "C" void Particle_set_color_m7445 (Particle_t1362 * __this, Color_t5  ___value, const MethodInfo* method)
{
	{
		Color_t5  L_0 = ___value;
		__this->___m_Color_7 = L_0;
		return;
	}
}
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.InternalDrawTextureArguments&)
extern "C" void Graphics_DrawTexture_m7446 (Object_t * __this /* static, unused */, InternalDrawTextureArguments_t1364 * ___arguments, const MethodInfo* method)
{
	typedef void (*Graphics_DrawTexture_m7446_ftn) (InternalDrawTextureArguments_t1364 *);
	static Graphics_DrawTexture_m7446_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_DrawTexture_m7446_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::DrawTexture(UnityEngine.InternalDrawTextureArguments&)");
	_il2cpp_icall_func(___arguments);
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C" int32_t Resolution_get_width_m7447 (Resolution_t1366 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Width_0);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_width(System.Int32)
extern "C" void Resolution_set_width_m7448 (Resolution_t1366 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Width_0 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C" int32_t Resolution_get_height_m7449 (Resolution_t1366 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Height_1);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_height(System.Int32)
extern "C" void Resolution_set_height_m7450 (Resolution_t1366 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Height_1 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern "C" int32_t Resolution_get_refreshRate_m7451 (Resolution_t1366 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_RefreshRate_2);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
extern "C" void Resolution_set_refreshRate_m7452 (Resolution_t1366 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_RefreshRate_2 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C" int32_t Screen_get_width_m396 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m396_ftn) ();
	static Screen_get_width_m396_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m396_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C" int32_t Screen_get_height_m397 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m397_ftn) ();
	static Screen_get_height_m397_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m397_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C" float Screen_get_dpi_m4387 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m4387_ftn) ();
	static Screen_get_dpi_m4387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m4387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Screen::get_fullScreen()
extern "C" bool Screen_get_fullScreen_m2370 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_fullScreen_m2370_ftn) ();
	static Screen_get_fullScreen_m2370_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_fullScreen_m2370_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_fullScreen()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_fullScreen(System.Boolean)
extern "C" void Screen_set_fullScreen_m2371 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*Screen_set_fullScreen_m2371_ftn) (bool);
	static Screen_set_fullScreen_m2371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_fullScreen_m2371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_fullScreen(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortrait()
extern "C" bool Screen_get_autorotateToPortrait_m6570 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToPortrait_m6570_ftn) ();
	static Screen_get_autorotateToPortrait_m6570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortrait_m6570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortrait()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)
extern "C" void Screen_set_autorotateToPortrait_m6461 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToPortrait_m6461_ftn) (bool);
	static Screen_set_autorotateToPortrait_m6461_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortrait_m6461_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortraitUpsideDown()
extern "C" bool Screen_get_autorotateToPortraitUpsideDown_m6571 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToPortraitUpsideDown_m6571_ftn) ();
	static Screen_get_autorotateToPortraitUpsideDown_m6571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortraitUpsideDown_m6571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortraitUpsideDown()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)
extern "C" void Screen_set_autorotateToPortraitUpsideDown_m6462 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToPortraitUpsideDown_m6462_ftn) (bool);
	static Screen_set_autorotateToPortraitUpsideDown_m6462_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortraitUpsideDown_m6462_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeLeft()
extern "C" bool Screen_get_autorotateToLandscapeLeft_m6568 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeLeft_m6568_ftn) ();
	static Screen_get_autorotateToLandscapeLeft_m6568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeLeft_m6568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeLeft()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)
extern "C" void Screen_set_autorotateToLandscapeLeft_m6463 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToLandscapeLeft_m6463_ftn) (bool);
	static Screen_set_autorotateToLandscapeLeft_m6463_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeLeft_m6463_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeRight()
extern "C" bool Screen_get_autorotateToLandscapeRight_m6569 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeRight_m6569_ftn) ();
	static Screen_get_autorotateToLandscapeRight_m6569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeRight_m6569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeRight()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)
extern "C" void Screen_set_autorotateToLandscapeRight_m6464 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToLandscapeRight_m6464_ftn) (bool);
	static Screen_set_autorotateToLandscapeRight_m6464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeRight_m6464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C" int32_t Screen_get_orientation_m2721 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_orientation_m2721_ftn) ();
	static Screen_get_orientation_m2721_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_orientation_m2721_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_orientation()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
extern "C" void Screen_set_orientation_m6465 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Screen_set_orientation_m6465_ftn) (int32_t);
	static Screen_set_orientation_m6465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_orientation_m6465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
extern "C" void Screen_set_sleepTimeout_m6798 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Screen_set_sleepTimeout_m6798_ftn) (int32_t);
	static Screen_set_sleepTimeout_m6798_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_sleepTimeout_m6798_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_sleepTimeout(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GL::Vertex(UnityEngine.Vector3)
extern "C" void GL_Vertex_m2786 (Object_t * __this /* static, unused */, Vector3_t6  ___v, const MethodInfo* method)
{
	{
		GL_INTERNAL_CALL_Vertex_m7453(NULL /*static, unused*/, (&___v), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::INTERNAL_CALL_Vertex(UnityEngine.Vector3&)
extern "C" void GL_INTERNAL_CALL_Vertex_m7453 (Object_t * __this /* static, unused */, Vector3_t6 * ___v, const MethodInfo* method)
{
	typedef void (*GL_INTERNAL_CALL_Vertex_m7453_ftn) (Vector3_t6 *);
	static GL_INTERNAL_CALL_Vertex_m7453_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_INTERNAL_CALL_Vertex_m7453_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::INTERNAL_CALL_Vertex(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___v);
}
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C" void GL_Begin_m2785 (Object_t * __this /* static, unused */, int32_t ___mode, const MethodInfo* method)
{
	typedef void (*GL_Begin_m2785_ftn) (int32_t);
	static GL_Begin_m2785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_Begin_m2785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::Begin(System.Int32)");
	_il2cpp_icall_func(___mode);
}
// System.Void UnityEngine.GL::End()
extern "C" void GL_End_m2787 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GL_End_m2787_ftn) ();
	static GL_End_m2787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_End_m2787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::End()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::MultMatrix(UnityEngine.Matrix4x4)
extern "C" void GL_MultMatrix_m2783 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___mat, const MethodInfo* method)
{
	{
		GL_INTERNAL_CALL_MultMatrix_m7454(NULL /*static, unused*/, (&___mat), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::INTERNAL_CALL_MultMatrix(UnityEngine.Matrix4x4&)
extern "C" void GL_INTERNAL_CALL_MultMatrix_m7454 (Object_t * __this /* static, unused */, Matrix4x4_t603 * ___mat, const MethodInfo* method)
{
	typedef void (*GL_INTERNAL_CALL_MultMatrix_m7454_ftn) (Matrix4x4_t603 *);
	static GL_INTERNAL_CALL_MultMatrix_m7454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_INTERNAL_CALL_MultMatrix_m7454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::INTERNAL_CALL_MultMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___mat);
}
// System.Void UnityEngine.GL::PushMatrix()
extern "C" void GL_PushMatrix_m2781 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GL_PushMatrix_m2781_ftn) ();
	static GL_PushMatrix_m2781_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_PushMatrix_m2781_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::PushMatrix()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::PopMatrix()
extern "C" void GL_PopMatrix_m2788 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GL_PopMatrix_m2788_ftn) ();
	static GL_PopMatrix_m2788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_PopMatrix_m2788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::PopMatrix()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::SetRevertBackfacing(System.Boolean)
extern "C" void GL_SetRevertBackfacing_m6843 (Object_t * __this /* static, unused */, bool ___revertBackFaces, const MethodInfo* method)
{
	typedef void (*GL_SetRevertBackfacing_m6843_ftn) (bool);
	static GL_SetRevertBackfacing_m6843_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_SetRevertBackfacing_m6843_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::SetRevertBackfacing(System.Boolean)");
	_il2cpp_icall_func(___revertBackFaces);
}
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color)
extern "C" void GL_Clear_m6514 (Object_t * __this /* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t5  ___backgroundColor, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		bool L_0 = ___clearDepth;
		bool L_1 = ___clearColor;
		Color_t5  L_2 = ___backgroundColor;
		float L_3 = V_0;
		GL_Clear_m7455(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern "C" void GL_Clear_m7455 (Object_t * __this /* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t5  ___backgroundColor, float ___depth, const MethodInfo* method)
{
	{
		bool L_0 = ___clearDepth;
		bool L_1 = ___clearColor;
		Color_t5  L_2 = ___backgroundColor;
		float L_3 = ___depth;
		GL_Internal_Clear_m7456(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern "C" void GL_Internal_Clear_m7456 (Object_t * __this /* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t5  ___backgroundColor, float ___depth, const MethodInfo* method)
{
	{
		bool L_0 = ___clearDepth;
		bool L_1 = ___clearColor;
		float L_2 = ___depth;
		GL_INTERNAL_CALL_Internal_Clear_m7457(NULL /*static, unused*/, L_0, L_1, (&___backgroundColor), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
extern "C" void GL_INTERNAL_CALL_Internal_Clear_m7457 (Object_t * __this /* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t5 * ___backgroundColor, float ___depth, const MethodInfo* method)
{
	typedef void (*GL_INTERNAL_CALL_Internal_Clear_m7457_ftn) (bool, bool, Color_t5 *, float);
	static GL_INTERNAL_CALL_Internal_Clear_m7457_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_INTERNAL_CALL_Internal_Clear_m7457_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)");
	_il2cpp_icall_func(___clearDepth, ___clearColor, ___backgroundColor, ___depth);
}
// System.Void UnityEngine.GL::IssuePluginEvent(System.Int32)
extern "C" void GL_IssuePluginEvent_m6597 (Object_t * __this /* static, unused */, int32_t ___eventID, const MethodInfo* method)
{
	typedef void (*GL_IssuePluginEvent_m6597_ftn) (int32_t);
	static GL_IssuePluginEvent_m6597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_IssuePluginEvent_m6597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::IssuePluginEvent(System.Int32)");
	_il2cpp_icall_func(___eventID);
}
// System.Void UnityEngine.Texture::.ctor()
extern "C" void Texture__ctor_m7458 (Texture_t731 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C" int32_t Texture_Internal_GetWidth_m7459 (Object_t * __this /* static, unused */, Texture_t731 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m7459_ftn) (Texture_t731 *);
	static Texture_Internal_GetWidth_m7459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m7459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C" int32_t Texture_Internal_GetHeight_m7460 (Object_t * __this /* static, unused */, Texture_t731 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m7460_ftn) (Texture_t731 *);
	static Texture_Internal_GetHeight_m7460_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m7460_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C" int32_t Texture_get_width_m7461 (Texture_t731 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetWidth_m7459(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture::set_width(System.Int32)
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1167;
extern "C" void Texture_set_width_m7462 (Texture_t731 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		_stringLiteral1167 = il2cpp_codegen_string_literal_from_index(1167);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t40 * L_0 = (Exception_t40 *)il2cpp_codegen_object_new (Exception_t40_il2cpp_TypeInfo_var);
		Exception__ctor_m2366(L_0, _stringLiteral1167, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C" int32_t Texture_get_height_m7463 (Texture_t731 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetHeight_m7460(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture::set_height(System.Int32)
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1167;
extern "C" void Texture_set_height_m7464 (Texture_t731 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		_stringLiteral1167 = il2cpp_codegen_string_literal_from_index(1167);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t40 * L_0 = (Exception_t40 *)il2cpp_codegen_object_new (Exception_t40_il2cpp_TypeInfo_var);
		Exception__ctor_m2366(L_0, _stringLiteral1167, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C" void Texture_set_filterMode_m6845 (Texture_t731 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Texture_set_filterMode_m6845_ftn) (Texture_t731 *, int32_t);
	static Texture_set_filterMode_m6845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_filterMode_m6845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C" void Texture_set_wrapMode_m6846 (Texture_t731 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Texture_set_wrapMode_m6846_ftn) (Texture_t731 *, int32_t);
	static Texture_set_wrapMode_m6846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_wrapMode_m6846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Texture::GetNativeTextureID()
extern "C" int32_t Texture_GetNativeTextureID_m6847 (Texture_t731 * __this, const MethodInfo* method)
{
	typedef int32_t (*Texture_GetNativeTextureID_m6847_ftn) (Texture_t731 *);
	static Texture_GetNativeTextureID_m6847_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_GetNativeTextureID_m6847_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::GetNativeTextureID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Texture2D__ctor_m2222 (Texture2D_t33 * __this, int32_t ___width, int32_t ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m7458(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		Texture2D_Internal_Create_m7465(NULL /*static, unused*/, __this, L_0, L_1, 5, 1, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Texture2D__ctor_m398 (Texture2D_t33 * __this, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m7458(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		int32_t L_2 = ___format;
		bool L_3 = ___mipmap;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		Texture2D_Internal_Create_m7465(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, 0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C" void Texture2D_Internal_Create_m7465 (Object_t * __this /* static, unused */, Texture2D_t33 * ___mono, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, bool ___linear, IntPtr_t ___nativeTex, const MethodInfo* method)
{
	typedef void (*Texture2D_Internal_Create_m7465_ftn) (Texture2D_t33 *, int32_t, int32_t, int32_t, bool, bool, IntPtr_t);
	static Texture2D_Internal_Create_m7465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m7465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono, ___width, ___height, ___format, ___mipmap, ___linear, ___nativeTex);
}
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
extern "C" int32_t Texture2D_get_format_m6528 (Texture2D_t33 * __this, const MethodInfo* method)
{
	typedef int32_t (*Texture2D_get_format_m6528_ftn) (Texture2D_t33 *);
	static Texture2D_get_format_m6528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_format_m6528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_format()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C" Texture2D_t33 * Texture2D_get_whiteTexture_m4136 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t33 * (*Texture2D_get_whiteTexture_m4136_ftn) ();
	static Texture2D_get_whiteTexture_m4136_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m4136_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C" void Texture2D_SetPixel_m2468 (Texture2D_t33 * __this, int32_t ___x, int32_t ___y, Color_t5  ___color, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x;
		int32_t L_1 = ___y;
		Texture2D_INTERNAL_CALL_SetPixel_m7466(NULL /*static, unused*/, __this, L_0, L_1, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
extern "C" void Texture2D_INTERNAL_CALL_SetPixel_m7466 (Object_t * __this /* static, unused */, Texture2D_t33 * ___self, int32_t ___x, int32_t ___y, Color_t5 * ___color, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_SetPixel_m7466_ftn) (Texture2D_t33 *, int32_t, int32_t, Color_t5 *);
	static Texture2D_INTERNAL_CALL_SetPixel_m7466_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_SetPixel_m7466_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___x, ___y, ___color);
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C" Color_t5  Texture2D_GetPixelBilinear_m4210 (Texture2D_t33 * __this, float ___u, float ___v, const MethodInfo* method)
{
	typedef Color_t5  (*Texture2D_GetPixelBilinear_m4210_ftn) (Texture2D_t33 *, float, float);
	static Texture2D_GetPixelBilinear_m4210_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixelBilinear_m4210_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)");
	return _il2cpp_icall_func(__this, ___u, ___v);
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C" void Texture2D_SetPixels_m399 (Texture2D_t33 * __this, ColorU5BU5D_t52* ___colors, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ColorU5BU5D_t52* L_0 = ___colors;
		int32_t L_1 = V_0;
		Texture2D_SetPixels_m7467(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C" void Texture2D_SetPixels_m7467 (Texture2D_t33 * __this, ColorU5BU5D_t52* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		ColorU5BU5D_t52* L_8 = ___colors;
		int32_t L_9 = ___miplevel;
		Texture2D_SetPixels_m7468(__this, 0, 0, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C" void Texture2D_SetPixels_m7468 (Texture2D_t33 * __this, int32_t ___x, int32_t ___y, int32_t ___blockWidth, int32_t ___blockHeight, ColorU5BU5D_t52* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	typedef void (*Texture2D_SetPixels_m7468_ftn) (Texture2D_t33 *, int32_t, int32_t, int32_t, int32_t, ColorU5BU5D_t52*, int32_t);
	static Texture2D_SetPixels_m7468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels_m7468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___x, ___y, ___blockWidth, ___blockHeight, ___colors, ___miplevel);
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C" void Texture2D_SetPixels32_m7469 (Texture2D_t33 * __this, Color32U5BU5D_t945* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	typedef void (*Texture2D_SetPixels32_m7469_ftn) (Texture2D_t33 *, Color32U5BU5D_t945*, int32_t);
	static Texture2D_SetPixels32_m7469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels32_m7469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___colors, ___miplevel);
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C" void Texture2D_SetPixels32_m6785 (Texture2D_t33 * __this, Color32U5BU5D_t945* ___colors, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Color32U5BU5D_t945* L_0 = ___colors;
		int32_t L_1 = V_0;
		Texture2D_SetPixels32_m7469(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[])
extern "C" bool Texture2D_LoadImage_m2223 (Texture2D_t33 * __this, ByteU5BU5D_t119* ___data, const MethodInfo* method)
{
	typedef bool (*Texture2D_LoadImage_m2223_ftn) (Texture2D_t33 *, ByteU5BU5D_t119*);
	static Texture2D_LoadImage_m2223_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_LoadImage_m2223_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::LoadImage(System.Byte[])");
	return _il2cpp_icall_func(__this, ___data);
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern "C" ColorU5BU5D_t52* Texture2D_GetPixels_m6530 (Texture2D_t33 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		ColorU5BU5D_t52* L_1 = Texture2D_GetPixels_m7470(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern "C" ColorU5BU5D_t52* Texture2D_GetPixels_m7470 (Texture2D_t33 * __this, int32_t ___miplevel, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = ___miplevel;
		ColorU5BU5D_t52* L_9 = Texture2D_GetPixels_m7471(__this, 0, 0, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" ColorU5BU5D_t52* Texture2D_GetPixels_m7471 (Texture2D_t33 * __this, int32_t ___x, int32_t ___y, int32_t ___blockWidth, int32_t ___blockHeight, int32_t ___miplevel, const MethodInfo* method)
{
	typedef ColorU5BU5D_t52* (*Texture2D_GetPixels_m7471_ftn) (Texture2D_t33 *, int32_t, int32_t, int32_t, int32_t, int32_t);
	static Texture2D_GetPixels_m7471_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels_m7471_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)");
	return _il2cpp_icall_func(__this, ___x, ___y, ___blockWidth, ___blockHeight, ___miplevel);
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
extern "C" Color32U5BU5D_t945* Texture2D_GetPixels32_m7472 (Texture2D_t33 * __this, int32_t ___miplevel, const MethodInfo* method)
{
	typedef Color32U5BU5D_t945* (*Texture2D_GetPixels32_m7472_ftn) (Texture2D_t33 *, int32_t);
	static Texture2D_GetPixels32_m7472_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels32_m7472_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels32(System.Int32)");
	return _il2cpp_icall_func(__this, ___miplevel);
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern "C" Color32U5BU5D_t945* Texture2D_GetPixels32_m6783 (Texture2D_t33 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Color32U5BU5D_t945* L_1 = Texture2D_GetPixels32_m7472(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C" void Texture2D_Apply_m7473 (Texture2D_t33 * __this, bool ___updateMipmaps, bool ___makeNoLongerReadable, const MethodInfo* method)
{
	typedef void (*Texture2D_Apply_m7473_ftn) (Texture2D_t33 *, bool, bool);
	static Texture2D_Apply_m7473_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m7473_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps, ___makeNoLongerReadable);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C" void Texture2D_Apply_m400 (Texture2D_t33 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = 0;
		V_1 = 1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m7473(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" bool Texture2D_Resize_m6529 (Texture2D_t33 * __this, int32_t ___width, int32_t ___height, int32_t ___format, bool ___hasMipMap, const MethodInfo* method)
{
	typedef bool (*Texture2D_Resize_m6529_ftn) (Texture2D_t33 *, int32_t, int32_t, int32_t, bool);
	static Texture2D_Resize_m6529_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Resize_m6529_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)");
	return _il2cpp_icall_func(__this, ___width, ___height, ___format, ___hasMipMap);
}
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern "C" void Texture2D_ReadPixels_m6782 (Texture2D_t33 * __this, Rect_t30  ___source, int32_t ___destX, int32_t ___destY, bool ___recalculateMipMaps, const MethodInfo* method)
{
	{
		int32_t L_0 = ___destX;
		int32_t L_1 = ___destY;
		bool L_2 = ___recalculateMipMaps;
		Texture2D_INTERNAL_CALL_ReadPixels_m7474(NULL /*static, unused*/, __this, (&___source), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern "C" void Texture2D_ReadPixels_m2247 (Texture2D_t33 * __this, Rect_t30  ___source, int32_t ___destX, int32_t ___destY, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		int32_t L_0 = ___destX;
		int32_t L_1 = ___destY;
		bool L_2 = V_0;
		Texture2D_INTERNAL_CALL_ReadPixels_m7474(NULL /*static, unused*/, __this, (&___source), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
extern "C" void Texture2D_INTERNAL_CALL_ReadPixels_m7474 (Object_t * __this /* static, unused */, Texture2D_t33 * ___self, Rect_t30 * ___source, int32_t ___destX, int32_t ___destY, bool ___recalculateMipMaps, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_ReadPixels_m7474_ftn) (Texture2D_t33 *, Rect_t30 *, int32_t, int32_t, bool);
	static Texture2D_INTERNAL_CALL_ReadPixels_m7474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_ReadPixels_m7474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___self, ___source, ___destX, ___destY, ___recalculateMipMaps);
}
// System.Byte[] UnityEngine.Texture2D::EncodeToPNG()
extern "C" ByteU5BU5D_t119* Texture2D_EncodeToPNG_m2251 (Texture2D_t33 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t119* (*Texture2D_EncodeToPNG_m2251_ftn) (Texture2D_t33 *);
	static Texture2D_EncodeToPNG_m2251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_EncodeToPNG_m2251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::EncodeToPNG()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" void RenderTexture__ctor_m2685 (RenderTexture_t582 * __this, int32_t ___width, int32_t ___height, int32_t ___depth, const MethodInfo* method)
{
	{
		Texture__ctor_m7458(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m7475(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		RenderTexture_set_width_m7483(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height;
		RenderTexture_set_height_m7485(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth;
		RenderTexture_set_depth_m7486(__this, L_2, /*hidden argument*/NULL);
		RenderTexture_set_format_m7487(__this, 7, /*hidden argument*/NULL);
		int32_t L_3 = QualitySettings_get_activeColorSpace_m7406(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m7481(NULL /*static, unused*/, __this, ((((int32_t)L_3) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C" void RenderTexture_Internal_CreateRenderTexture_m7475 (Object_t * __this /* static, unused */, RenderTexture_t582 * ___rt, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m7475_ftn) (RenderTexture_t582 *);
	static RenderTexture_Internal_CreateRenderTexture_m7475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m7475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern "C" RenderTexture_t582 * RenderTexture_GetTemporary_m7476 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, int32_t ___depthBuffer, int32_t ___format, int32_t ___readWrite, int32_t ___antiAliasing, const MethodInfo* method)
{
	typedef RenderTexture_t582 * (*RenderTexture_GetTemporary_m7476_ftn) (int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	static RenderTexture_GetTemporary_m7476_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetTemporary_m7476_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)");
	return _il2cpp_icall_func(___width, ___height, ___depthBuffer, ___format, ___readWrite, ___antiAliasing);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C" RenderTexture_t582 * RenderTexture_GetTemporary_m6780 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, int32_t ___depthBuffer, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	int32_t V_2 = {0};
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		int32_t L_2 = ___depthBuffer;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t582 * L_6 = RenderTexture_GetTemporary_m7476(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C" void RenderTexture_ReleaseTemporary_m6784 (Object_t * __this /* static, unused */, RenderTexture_t582 * ___temp, const MethodInfo* method)
{
	typedef void (*RenderTexture_ReleaseTemporary_m6784_ftn) (RenderTexture_t582 *);
	static RenderTexture_ReleaseTemporary_m6784_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_ReleaseTemporary_m6784_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___temp);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C" int32_t RenderTexture_Internal_GetWidth_m7477 (Object_t * __this /* static, unused */, RenderTexture_t582 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m7477_ftn) (RenderTexture_t582 *);
	static RenderTexture_Internal_GetWidth_m7477_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m7477_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono);
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C" void RenderTexture_Internal_SetWidth_m7478 (Object_t * __this /* static, unused */, RenderTexture_t582 * ___mono, int32_t ___width, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m7478_ftn) (RenderTexture_t582 *, int32_t);
	static RenderTexture_Internal_SetWidth_m7478_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m7478_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono, ___width);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C" int32_t RenderTexture_Internal_GetHeight_m7479 (Object_t * __this /* static, unused */, RenderTexture_t582 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m7479_ftn) (RenderTexture_t582 *);
	static RenderTexture_Internal_GetHeight_m7479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m7479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono);
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C" void RenderTexture_Internal_SetHeight_m7480 (Object_t * __this /* static, unused */, RenderTexture_t582 * ___mono, int32_t ___width, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m7480_ftn) (RenderTexture_t582 *, int32_t);
	static RenderTexture_Internal_SetHeight_m7480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m7480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono, ___width);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C" void RenderTexture_Internal_SetSRGBReadWrite_m7481 (Object_t * __this /* static, unused */, RenderTexture_t582 * ___mono, bool ___sRGB, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m7481_ftn) (RenderTexture_t582 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m7481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m7481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono, ___sRGB);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C" int32_t RenderTexture_get_width_m7482 (RenderTexture_t582 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m7477(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C" void RenderTexture_set_width_m7483 (RenderTexture_t582 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		RenderTexture_Internal_SetWidth_m7478(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C" int32_t RenderTexture_get_height_m7484 (RenderTexture_t582 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m7479(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C" void RenderTexture_set_height_m7485 (RenderTexture_t582 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		RenderTexture_Internal_SetHeight_m7480(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C" void RenderTexture_set_depth_m7486 (RenderTexture_t582 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_depth_m7486_ftn) (RenderTexture_t582 *, int32_t);
	static RenderTexture_set_depth_m7486_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m7486_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C" void RenderTexture_set_format_m7487 (RenderTexture_t582 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_format_m7487_ftn) (RenderTexture_t582 *, int32_t);
	static RenderTexture_set_format_m7487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m7487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C" void RenderTexture_set_active_m2689 (Object_t * __this /* static, unused */, RenderTexture_t582 * ___value, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_active_m2689_ftn) (RenderTexture_t582 *);
	static RenderTexture_set_active_m2689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_active_m2689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.Color UnityEngine.GUITexture::get_color()
extern "C" Color_t5  GUITexture_get_color_m309 (GUITexture_t43 * __this, const MethodInfo* method)
{
	Color_t5  V_0 = {0};
	{
		GUITexture_INTERNAL_get_color_m7488(__this, (&V_0), /*hidden argument*/NULL);
		Color_t5  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.GUITexture::set_color(UnityEngine.Color)
extern "C" void GUITexture_set_color_m318 (GUITexture_t43 * __this, Color_t5  ___value, const MethodInfo* method)
{
	{
		GUITexture_INTERNAL_set_color_m7489(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_get_color_m7488 (GUITexture_t43 * __this, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*GUITexture_INTERNAL_get_color_m7488_ftn) (GUITexture_t43 *, Color_t5 *);
	static GUITexture_INTERNAL_get_color_m7488_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUITexture_INTERNAL_get_color_m7488_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_set_color_m7489 (GUITexture_t43 * __this, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*GUITexture_INTERNAL_set_color_m7489_ftn) (GUITexture_t43 *, Color_t5 *);
	static GUITexture_INTERNAL_set_color_m7489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUITexture_INTERNAL_set_color_m7489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C" void GUITexture_set_texture_m402 (GUITexture_t43 * __this, Texture_t731 * ___value, const MethodInfo* method)
{
	typedef void (*GUITexture_set_texture_m402_ftn) (GUITexture_t43 *, Texture_t731 *);
	static GUITexture_set_texture_m402_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUITexture_set_texture_m402_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUITexture::set_texture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C" Material_t45 * GUIText_get_material_m312 (GUIText_t44 * __this, const MethodInfo* method)
{
	typedef Material_t45 * (*GUIText_get_material_m312_ftn) (GUIText_t44 *);
	static GUIText_get_material_m312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIText_get_material_m312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIText::get_material()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t1373_marshal(const CharacterInfo_t1373& unmarshaled, CharacterInfo_t1373_marshaled& marshaled)
{
	marshaled.___index_0 = unmarshaled.___index_0;
	marshaled.___uv_1 = unmarshaled.___uv_1;
	marshaled.___vert_2 = unmarshaled.___vert_2;
	marshaled.___width_3 = unmarshaled.___width_3;
	marshaled.___size_4 = unmarshaled.___size_4;
	marshaled.___style_5 = unmarshaled.___style_5;
	marshaled.___flipped_6 = unmarshaled.___flipped_6;
}
extern "C" void CharacterInfo_t1373_marshal_back(const CharacterInfo_t1373_marshaled& marshaled, CharacterInfo_t1373& unmarshaled)
{
	unmarshaled.___index_0 = marshaled.___index_0;
	unmarshaled.___uv_1 = marshaled.___uv_1;
	unmarshaled.___vert_2 = marshaled.___vert_2;
	unmarshaled.___width_3 = marshaled.___width_3;
	unmarshaled.___size_4 = marshaled.___size_4;
	unmarshaled.___style_5 = marshaled.___style_5;
	unmarshaled.___flipped_6 = marshaled.___flipped_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t1373_marshal_cleanup(CharacterInfo_t1373_marshaled& marshaled)
{
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FontTextureRebuildCallback__ctor_m7490 (FontTextureRebuildCallback_t1374 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke()
extern "C" void FontTextureRebuildCallback_Invoke_m7491 (FontTextureRebuildCallback_t1374 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FontTextureRebuildCallback_Invoke_m7491((FontTextureRebuildCallback_t1374 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t1374(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Font/FontTextureRebuildCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * FontTextureRebuildCallback_BeginInvoke_m7492 (FontTextureRebuildCallback_t1374 * __this, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::EndInvoke(System.IAsyncResult)
extern "C" void FontTextureRebuildCallback_EndInvoke_m7493 (FontTextureRebuildCallback_t1374 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t684_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t843_il2cpp_TypeInfo_var;
extern "C" void Font_add_textureRebuilt_m4124 (Object_t * __this /* static, unused */, Action_1_t843 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t684_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		Action_1_t843_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(514);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t843 * L_0 = ((Font_t684_StaticFields*)Font_t684_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_1;
		Action_1_t843 * L_1 = ___value;
		Delegate_t506 * L_2 = Delegate_Combine_m2300(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t684_StaticFields*)Font_t684_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_1 = ((Action_1_t843 *)CastclassSealed(L_2, Action_1_t843_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t684_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t843_il2cpp_TypeInfo_var;
extern "C" void Font_remove_textureRebuilt_m7494 (Object_t * __this /* static, unused */, Action_1_t843 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t684_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		Action_1_t843_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(514);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t843 * L_0 = ((Font_t684_StaticFields*)Font_t684_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_1;
		Action_1_t843 * L_1 = ___value;
		Delegate_t506 * L_2 = Delegate_Remove_m2302(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t684_StaticFields*)Font_t684_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_1 = ((Action_1_t843 *)CastclassSealed(L_2, Action_1_t843_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.Material UnityEngine.Font::get_material()
extern "C" Material_t45 * Font_get_material_m4367 (Font_t684 * __this, const MethodInfo* method)
{
	typedef Material_t45 * (*Font_get_material_m4367_ftn) (Font_t684 *);
	static Font_get_material_m4367_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_material_m4367_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Font::HasCharacter(System.Char)
extern "C" bool Font_HasCharacter_m4258 (Font_t684 * __this, uint16_t ___c, const MethodInfo* method)
{
	typedef bool (*Font_HasCharacter_m4258_ftn) (Font_t684 *, uint16_t);
	static Font_HasCharacter_m4258_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_HasCharacter_m4258_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::HasCharacter(System.Char)");
	return _il2cpp_icall_func(__this, ___c);
}
// System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
extern TypeInfo* Font_t684_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8178_MethodInfo_var;
extern "C" void Font_InvokeTextureRebuilt_Internal_m7495 (Object_t * __this /* static, unused */, Font_t684 * ___font, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t684_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		Action_1_Invoke_m8178_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484719);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t843 * V_0 = {0};
	{
		Action_1_t843 * L_0 = ((Font_t684_StaticFields*)Font_t684_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_1;
		V_0 = L_0;
		Action_1_t843 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t843 * L_2 = V_0;
		Font_t684 * L_3 = ___font;
		NullCheck(L_2);
		Action_1_Invoke_m8178(L_2, L_3, /*hidden argument*/Action_1_Invoke_m8178_MethodInfo_var);
	}

IL_0013:
	{
		Font_t684 * L_4 = ___font;
		NullCheck(L_4);
		FontTextureRebuildCallback_t1374 * L_5 = (L_4->___m_FontTextureRebuildCallback_2);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Font_t684 * L_6 = ___font;
		NullCheck(L_6);
		FontTextureRebuildCallback_t1374 * L_7 = (L_6->___m_FontTextureRebuildCallback_2);
		NullCheck(L_7);
		FontTextureRebuildCallback_Invoke_m7491(L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C" bool Font_get_dynamic_m4370 (Font_t684 * __this, const MethodInfo* method)
{
	typedef bool (*Font_get_dynamic_m4370_ftn) (Font_t684 *);
	static Font_get_dynamic_m4370_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_dynamic_m4370_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_dynamic()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_fontSize()
extern "C" int32_t Font_get_fontSize_m4372 (Font_t684 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_fontSize_m4372_ftn) (Font_t684 *);
	static Font_get_fontSize_m4372_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontSize_m4372_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontSize()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C" GUIElement_t1370 * GUILayer_HitTest_m7496 (GUILayer_t1375 * __this, Vector3_t6  ___screenPosition, const MethodInfo* method)
{
	{
		GUIElement_t1370 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m7497(NULL /*static, unused*/, __this, (&___screenPosition), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C" GUIElement_t1370 * GUILayer_INTERNAL_CALL_HitTest_m7497 (Object_t * __this /* static, unused */, GUILayer_t1375 * ___self, Vector3_t6 * ___screenPosition, const MethodInfo* method)
{
	typedef GUIElement_t1370 * (*GUILayer_INTERNAL_CALL_HitTest_m7497_ftn) (GUILayer_t1375 *, Vector3_t6 *);
	static GUILayer_INTERNAL_CALL_HitTest_m7497_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m7497_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___screenPosition);
}
// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
extern "C" void GradientColorKey__ctor_m7498 (GradientColorKey_t1376 * __this, Color_t5  ___col, float ___time, const MethodInfo* method)
{
	{
		Color_t5  L_0 = ___col;
		__this->___color_0 = L_0;
		float L_1 = ___time;
		__this->___time_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.GradientAlphaKey::.ctor(System.Single,System.Single)
extern "C" void GradientAlphaKey__ctor_m7499 (GradientAlphaKey_t1377 * __this, float ___alpha, float ___time, const MethodInfo* method)
{
	{
		float L_0 = ___alpha;
		__this->___alpha_0 = L_0;
		float L_1 = ___time;
		__this->___time_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m7500 (Gradient_t1378 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		Gradient_Init_m7501(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m7501 (Gradient_t1378 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Init_m7501_ftn) (Gradient_t1378 *);
	static Gradient_Init_m7501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m7501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m7502 (Gradient_t1378 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Cleanup_m7502_ftn) (Gradient_t1378 *);
	static Gradient_Cleanup_m7502_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m7502_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m7503 (Gradient_t1378 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m7502(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t1378_marshal(const Gradient_t1378& unmarshaled, Gradient_t1378_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void Gradient_t1378_marshal_back(const Gradient_t1378_marshaled& marshaled, Gradient_t1378& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t1378_marshal_cleanup(Gradient_t1378_marshaled& marshaled)
{
}
// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
extern "C" void ScrollViewState__ctor_m7504 (ScrollViewState_t1380 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
extern "C" void WindowFunction__ctor_m2473 (WindowFunction_t548 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUI/WindowFunction::Invoke(System.Int32)
extern "C" void WindowFunction_Invoke_m7505 (WindowFunction_t548 * __this, int32_t ___id, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WindowFunction_Invoke_m7505((WindowFunction_t548 *)__this->___prev_9,___id, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t548(Il2CppObject* delegate, int32_t ___id)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___id' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___id);

	// Marshaling cleanup of parameter '___id' native representation

}
// System.IAsyncResult UnityEngine.GUI/WindowFunction::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern "C" Object_t * WindowFunction_BeginInvoke_m7506 (WindowFunction_t548 * __this, int32_t ___id, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t59_il2cpp_TypeInfo_var, &___id);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUI/WindowFunction::EndInvoke(System.IAsyncResult)
extern "C" void WindowFunction_EndInvoke_m7507 (WindowFunction_t548 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.GUI::.cctor()
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t1309_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t220_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1168;
extern Il2CppCodeGenString* _stringLiteral1169;
extern Il2CppCodeGenString* _stringLiteral1170;
extern Il2CppCodeGenString* _stringLiteral1171;
extern Il2CppCodeGenString* _stringLiteral1172;
extern Il2CppCodeGenString* _stringLiteral1173;
extern Il2CppCodeGenString* _stringLiteral1174;
extern "C" void GUI__cctor_m7508 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		GenericStack_t1309_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(952);
		DateTime_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		_stringLiteral1168 = il2cpp_codegen_string_literal_from_index(1168);
		_stringLiteral1169 = il2cpp_codegen_string_literal_from_index(1169);
		_stringLiteral1170 = il2cpp_codegen_string_literal_from_index(1170);
		_stringLiteral1171 = il2cpp_codegen_string_literal_from_index(1171);
		_stringLiteral1172 = il2cpp_codegen_string_literal_from_index(1172);
		_stringLiteral1173 = il2cpp_codegen_string_literal_from_index(1173);
		_stringLiteral1174 = il2cpp_codegen_string_literal_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___scrollStepSize_0 = (10.0f);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___hotTextField_2 = (-1);
		NullCheck(_stringLiteral1168);
		int32_t L_0 = String_GetHashCode_m2155(_stringLiteral1168, /*hidden argument*/NULL);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___boxHash_5 = L_0;
		NullCheck(_stringLiteral1169);
		int32_t L_1 = String_GetHashCode_m2155(_stringLiteral1169, /*hidden argument*/NULL);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___repeatButtonHash_6 = L_1;
		NullCheck(_stringLiteral1170);
		int32_t L_2 = String_GetHashCode_m2155(_stringLiteral1170, /*hidden argument*/NULL);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___toggleHash_7 = L_2;
		NullCheck(_stringLiteral1171);
		int32_t L_3 = String_GetHashCode_m2155(_stringLiteral1171, /*hidden argument*/NULL);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___buttonGridHash_8 = L_3;
		NullCheck(_stringLiteral1172);
		int32_t L_4 = String_GetHashCode_m2155(_stringLiteral1172, /*hidden argument*/NULL);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___sliderHash_9 = L_4;
		NullCheck(_stringLiteral1173);
		int32_t L_5 = String_GetHashCode_m2155(_stringLiteral1173, /*hidden argument*/NULL);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___beginGroupHash_10 = L_5;
		NullCheck(_stringLiteral1174);
		int32_t L_6 = String_GetHashCode_m2155(_stringLiteral1174, /*hidden argument*/NULL);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___scrollviewHash_11 = L_6;
		GenericStack_t1309 * L_7 = (GenericStack_t1309 *)il2cpp_codegen_object_new (GenericStack_t1309_il2cpp_TypeInfo_var);
		GenericStack__ctor_m7126(L_7, /*hidden argument*/NULL);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t220_il2cpp_TypeInfo_var);
		DateTime_t220  L_8 = DateTime_get_Now_m2490(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_nextScrollStepTime_m7509(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_set_nextScrollStepTime_m7509 (Object_t * __this /* static, unused */, DateTime_t220  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t220  L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___U3CnextScrollStepTimeU3Ek__BackingField_13 = L_0;
		return;
	}
}
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_set_skin_m7510 (Object_t * __this /* static, unused */, GUISkin_t547 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUISkin_t547 * L_0 = ___value;
		bool L_1 = Object_op_Implicit_m301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_2 = GUIUtility_GetDefaultSkin_m7612(NULL /*static, unused*/, /*hidden argument*/NULL);
		___value = L_2;
	}

IL_0017:
	{
		GUISkin_t547 * L_3 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___s_Skin_3 = L_3;
		GUISkin_t547 * L_4 = ___value;
		NullCheck(L_4);
		GUISkin_MakeCurrent_m7678(L_4, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" GUISkin_t547 * GUI_get_skin_m2469 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_0 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___s_Skin_3;
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.GUI::get_color()
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" Color_t5  GUI_get_color_m2308 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	Color_t5  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_INTERNAL_get_color_m7511(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t5  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_set_color_m2310 (Object_t * __this /* static, unused */, Color_t5  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_INTERNAL_set_color_m7512(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void GUI_INTERNAL_get_color_m7511 (Object_t * __this /* static, unused */, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_get_color_m7511_ftn) (Color_t5 *);
	static GUI_INTERNAL_get_color_m7511_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_get_color_m7511_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUI_INTERNAL_set_color_m7512 (Object_t * __this /* static, unused */, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_set_color_m7512_ftn) (Color_t5 *);
	static GUI_INTERNAL_set_color_m7512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_set_color_m7512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::set_backgroundColor(UnityEngine.Color)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_set_backgroundColor_m7513 (Object_t * __this /* static, unused */, Color_t5  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_INTERNAL_set_backgroundColor_m7514(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_set_backgroundColor(UnityEngine.Color&)
extern "C" void GUI_INTERNAL_set_backgroundColor_m7514 (Object_t * __this /* static, unused */, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_set_backgroundColor_m7514_ftn) (Color_t5 *);
	static GUI_INTERNAL_set_backgroundColor_m7514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_set_backgroundColor_m7514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_set_backgroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
extern "C" void GUI_set_changed_m7515 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUI_set_changed_m7515_ftn) (bool);
	static GUI_set_changed_m7515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_set_changed_m7515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::set_changed(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::set_enabled(System.Boolean)
extern "C" void GUI_set_enabled_m2299 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUI_set_enabled_m2299_ftn) (bool);
	static GUI_set_enabled_m2299_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_set_enabled_m2299_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::set_enabled(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m2304 (Object_t * __this /* static, unused */, Rect_t30  ___position, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_2 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_3 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___s_Skin_3;
		NullCheck(L_3);
		GUIStyle_t184 * L_4 = GUISkin_get_label_m7635(L_3, /*hidden argument*/NULL);
		GUI_Label_m7516(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m2297 (Object_t * __this /* static, unused */, Rect_t30  ___position, String_t* ___text, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_2 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUIStyle_t184 * L_3 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_Label_m7516(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m7516 (Object_t * __this /* static, unused */, Rect_t30  ___position, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = ___position;
		GUIContent_t549 * L_1 = ___content;
		GUIStyle_t184 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_DoLabel_m7517(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_DoLabel_m7517 (Object_t * __this /* static, unused */, Rect_t30  ___position, GUIContent_t549 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t549 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_INTERNAL_CALL_DoLabel_m7518(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_INTERNAL_CALL_DoLabel_m7518 (Object_t * __this /* static, unused */, Rect_t30 * ___position, GUIContent_t549 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_CALL_DoLabel_m7518_ftn) (Rect_t30 *, GUIContent_t549 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoLabel_m7518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoLabel_m7518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	_il2cpp_icall_func(___position, ___content, ___style);
}
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_DrawTexture_m2305 (Object_t * __this /* static, unused */, Rect_t30  ___position, Texture_t731 * ___image, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	int32_t V_2 = {0};
	{
		V_0 = (0.0f);
		V_1 = 1;
		V_2 = 0;
		Rect_t30  L_0 = ___position;
		Texture_t731 * L_1 = ___image;
		int32_t L_2 = V_2;
		bool L_3 = V_1;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m7519(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.ScaleMode,System.Boolean,System.Single)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* InternalDrawTextureArguments_t1364_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1175;
extern "C" void GUI_DrawTexture_m7519 (Object_t * __this /* static, unused */, Rect_t30  ___position, Texture_t731 * ___image, int32_t ___scaleMode, bool ___alphaBlend, float ___imageAspect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		InternalDrawTextureArguments_t1364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(953);
		_stringLiteral1175 = il2cpp_codegen_string_literal_from_index(1175);
		s_Il2CppMethodIntialized = true;
	}
	Material_t45 * V_0 = {0};
	float V_1 = 0.0f;
	InternalDrawTextureArguments_t1364  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	int32_t V_7 = {0};
	Material_t45 * G_B8_0 = {0};
	{
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)7))))
		{
			goto IL_0277;
		}
	}
	{
		Texture_t731 * L_2 = ___image;
		bool L_3 = Object_op_Equality_m427(NULL /*static, unused*/, L_2, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		Debug_LogWarning_m430(NULL /*static, unused*/, _stringLiteral1175, /*hidden argument*/NULL);
		return;
	}

IL_0027:
	{
		float L_4 = ___imageAspect;
		if ((!(((float)L_4) == ((float)(0.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		Texture_t731 * L_5 = ___image;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_5);
		Texture_t731 * L_7 = ___image;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_7);
		___imageAspect = ((float)((float)(((float)((float)L_6)))/(float)(((float)((float)L_8)))));
	}

IL_0044:
	{
		bool L_9 = ___alphaBlend;
		if (!L_9)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		Material_t45 * L_10 = GUI_get_blendMaterial_m7520(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_10;
		goto IL_0059;
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		Material_t45 * L_11 = GUI_get_blitMaterial_m7521(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_11;
	}

IL_0059:
	{
		V_0 = G_B8_0;
		float L_12 = Rect_get_width_m370((&___position), /*hidden argument*/NULL);
		float L_13 = Rect_get_height_m372((&___position), /*hidden argument*/NULL);
		V_1 = ((float)((float)L_12/(float)L_13));
		Initobj (InternalDrawTextureArguments_t1364_il2cpp_TypeInfo_var, (&V_2));
		Texture_t731 * L_14 = ___image;
		(&V_2)->___texture_1 = L_14;
		(&V_2)->___leftBorder_3 = 0;
		(&V_2)->___rightBorder_4 = 0;
		(&V_2)->___topBorder_5 = 0;
		(&V_2)->___bottomBorder_6 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		Color_t5  L_15 = GUI_get_color_m2308(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color32_t829  L_16 = Color32_op_Implicit_m4144(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		(&V_2)->___color_7 = L_16;
		Material_t45 * L_17 = V_0;
		(&V_2)->___mat_8 = L_17;
		int32_t L_18 = ___scaleMode;
		V_7 = L_18;
		int32_t L_19 = V_7;
		if (L_19 == 0)
		{
			goto IL_00ce;
		}
		if (L_19 == 1)
		{
			goto IL_0102;
		}
		if (L_19 == 2)
		{
			goto IL_0187;
		}
	}
	{
		goto IL_0277;
	}

IL_00ce:
	{
		Rect_t30  L_20 = ___position;
		(&V_2)->___screenRect_0 = L_20;
		Rect_t30  L_21 = {0};
		Rect__ctor_m387(&L_21, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_21;
		Graphics_DrawTexture_m7446(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
		goto IL_0277;
	}

IL_0102:
	{
		float L_22 = V_1;
		float L_23 = ___imageAspect;
		if ((!(((float)L_22) > ((float)L_23))))
		{
			goto IL_0147;
		}
	}
	{
		float L_24 = ___imageAspect;
		float L_25 = V_1;
		V_3 = ((float)((float)L_24/(float)L_25));
		Rect_t30  L_26 = ___position;
		(&V_2)->___screenRect_0 = L_26;
		float L_27 = V_3;
		float L_28 = V_3;
		Rect_t30  L_29 = {0};
		Rect__ctor_m387(&L_29, (0.0f), ((float)((float)((float)((float)(1.0f)-(float)L_27))*(float)(0.5f))), (1.0f), L_28, /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_29;
		Graphics_DrawTexture_m7446(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
		goto IL_0182;
	}

IL_0147:
	{
		float L_30 = V_1;
		float L_31 = ___imageAspect;
		V_4 = ((float)((float)L_30/(float)L_31));
		Rect_t30  L_32 = ___position;
		(&V_2)->___screenRect_0 = L_32;
		float L_33 = V_4;
		float L_34 = V_4;
		Rect_t30  L_35 = {0};
		Rect__ctor_m387(&L_35, ((float)((float)(0.5f)-(float)((float)((float)L_33*(float)(0.5f))))), (0.0f), L_34, (1.0f), /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_35;
		Graphics_DrawTexture_m7446(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
	}

IL_0182:
	{
		goto IL_0277;
	}

IL_0187:
	{
		float L_36 = V_1;
		float L_37 = ___imageAspect;
		if ((!(((float)L_36) > ((float)L_37))))
		{
			goto IL_0203;
		}
	}
	{
		float L_38 = ___imageAspect;
		float L_39 = V_1;
		V_5 = ((float)((float)L_38/(float)L_39));
		float L_40 = Rect_get_xMin_m4217((&___position), /*hidden argument*/NULL);
		float L_41 = Rect_get_width_m370((&___position), /*hidden argument*/NULL);
		float L_42 = V_5;
		float L_43 = Rect_get_yMin_m4216((&___position), /*hidden argument*/NULL);
		float L_44 = V_5;
		float L_45 = Rect_get_width_m370((&___position), /*hidden argument*/NULL);
		float L_46 = Rect_get_height_m372((&___position), /*hidden argument*/NULL);
		Rect_t30  L_47 = {0};
		Rect__ctor_m387(&L_47, ((float)((float)L_40+(float)((float)((float)((float)((float)L_41*(float)((float)((float)(1.0f)-(float)L_42))))*(float)(0.5f))))), L_43, ((float)((float)L_44*(float)L_45)), L_46, /*hidden argument*/NULL);
		(&V_2)->___screenRect_0 = L_47;
		Rect_t30  L_48 = {0};
		Rect__ctor_m387(&L_48, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_48;
		Graphics_DrawTexture_m7446(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
		goto IL_0272;
	}

IL_0203:
	{
		float L_49 = V_1;
		float L_50 = ___imageAspect;
		V_6 = ((float)((float)L_49/(float)L_50));
		float L_51 = Rect_get_xMin_m4217((&___position), /*hidden argument*/NULL);
		float L_52 = Rect_get_yMin_m4216((&___position), /*hidden argument*/NULL);
		float L_53 = Rect_get_height_m372((&___position), /*hidden argument*/NULL);
		float L_54 = V_6;
		float L_55 = Rect_get_width_m370((&___position), /*hidden argument*/NULL);
		float L_56 = V_6;
		float L_57 = Rect_get_height_m372((&___position), /*hidden argument*/NULL);
		Rect_t30  L_58 = {0};
		Rect__ctor_m387(&L_58, L_51, ((float)((float)L_52+(float)((float)((float)((float)((float)L_53*(float)((float)((float)(1.0f)-(float)L_54))))*(float)(0.5f))))), L_55, ((float)((float)L_56*(float)L_57)), /*hidden argument*/NULL);
		(&V_2)->___screenRect_0 = L_58;
		Rect_t30  L_59 = {0};
		Rect__ctor_m387(&L_59, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_59;
		Graphics_DrawTexture_m7446(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
	}

IL_0272:
	{
		goto IL_0277;
	}

IL_0277:
	{
		return;
	}
}
// UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
extern "C" Material_t45 * GUI_get_blendMaterial_m7520 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t45 * (*GUI_get_blendMaterial_m7520_ftn) ();
	static GUI_get_blendMaterial_m7520_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_get_blendMaterial_m7520_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::get_blendMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
extern "C" Material_t45 * GUI_get_blitMaterial_m7521 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t45 * (*GUI_get_blitMaterial_m7521_ftn) ();
	static GUI_get_blitMaterial_m7521_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_get_blitMaterial_m7521_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::get_blitMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_Box_m7522 (Object_t * __this /* static, unused */, Rect_t30  ___position, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___boxHash_5;
		int32_t L_1 = GUIUtility_GetControlID_m7604(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t725 * L_2 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m7773(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_002a;
		}
	}
	{
		GUIStyle_t184 * L_4 = ___style;
		Rect_t30  L_5 = ___position;
		GUIContent_t549 * L_6 = ___content;
		int32_t L_7 = V_0;
		NullCheck(L_4);
		GUIStyle_Draw_m7743(L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m2298 (Object_t * __this /* static, unused */, Rect_t30  ___position, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_2 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_3 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___s_Skin_3;
		NullCheck(L_3);
		GUIStyle_t184 * L_4 = GUISkin_get_button_m2482(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		IntPtr_t L_5 = (L_4->___m_Ptr_0);
		bool L_6 = GUI_DoButton_m7523(NULL /*static, unused*/, L_0, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.GUIContent)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m2484 (Object_t * __this /* static, unused */, Rect_t30  ___position, GUIContent_t549 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = ___position;
		GUIContent_t549 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_2 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___s_Skin_3;
		NullCheck(L_2);
		GUIStyle_t184 * L_3 = GUISkin_get_button_m2482(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		bool L_5 = GUI_DoButton_m7523(NULL /*static, unused*/, L_0, L_1, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m2485 (Object_t * __this /* static, unused */, Rect_t30  ___position, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = ___position;
		GUIContent_t549 * L_1 = ___content;
		GUIStyle_t184 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		bool L_4 = GUI_DoButton_m7523(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoButton_m7523 (Object_t * __this /* static, unused */, Rect_t30  ___position, GUIContent_t549 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t549 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		bool L_2 = GUI_INTERNAL_CALL_DoButton_m7524(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoButton_m7524 (Object_t * __this /* static, unused */, Rect_t30 * ___position, GUIContent_t549 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef bool (*GUI_INTERNAL_CALL_DoButton_m7524_ftn) (Rect_t30 *, GUIContent_t549 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoButton_m7524_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoButton_m7524_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	return _il2cpp_icall_func(___position, ___content, ___style);
}
// System.String UnityEngine.GUI::PasswordFieldGetStrToShow(System.String,System.Char)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* GUI_PasswordFieldGetStrToShow_m7525 (Object_t * __this /* static, unused */, String_t* ___password, uint16_t ___maskChar, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B4_0 = {0};
	{
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_001f;
		}
	}
	{
		Event_t725 * L_2 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m7773(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0035;
		}
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_5 = ___password;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m410(L_5, /*hidden argument*/NULL);
		uint16_t L_7 = ___maskChar;
		NullCheck(L_4);
		String_t* L_8 = String_PadRight_m8179(L_4, L_6, L_7, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		goto IL_0036;
	}

IL_0035:
	{
		String_t* L_9 = ___password;
		G_B4_0 = L_9;
	}

IL_0036:
	{
		return G_B4_0;
	}
}
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char)
extern const Il2CppType* TextEditor_t855_0_0_0_var;
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextEditor_t855_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUI_DoTextField_m7526 (Object_t * __this /* static, unused */, Rect_t30  ___position, int32_t ___id, GUIContent_t549 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t184 * ___style, String_t* ___secureText, uint16_t ___maskChar, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t855_0_0_0_var = il2cpp_codegen_type_from_index(562);
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		TextEditor_t855_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(562);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	TextEditor_t855 * V_0 = {0};
	{
		int32_t L_0 = ___maxLength;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		GUIContent_t549 * L_1 = ___content;
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m4240(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m410(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___maxLength;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}
	{
		GUIContent_t549 * L_5 = ___content;
		GUIContent_t549 * L_6 = ___content;
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m4240(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___maxLength;
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m411(L_7, 0, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m7683(L_5, L_9, /*hidden argument*/NULL);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(TextEditor_t855_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_11 = ___id;
		Object_t * L_12 = GUIUtility_GetStateObject_m7605(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = ((TextEditor_t855 *)CastclassClass(L_12, TextEditor_t855_il2cpp_TypeInfo_var));
		TextEditor_t855 * L_13 = V_0;
		NullCheck(L_13);
		GUIContent_t549 * L_14 = (L_13->___content_4);
		GUIContent_t549 * L_15 = ___content;
		NullCheck(L_15);
		String_t* L_16 = GUIContent_get_text_m4240(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		GUIContent_set_text_m7683(L_14, L_16, /*hidden argument*/NULL);
		TextEditor_t855 * L_17 = V_0;
		NullCheck(L_17);
		TextEditor_SaveBackup_m7028(L_17, /*hidden argument*/NULL);
		TextEditor_t855 * L_18 = V_0;
		Rect_t30  L_19 = ___position;
		NullCheck(L_18);
		L_18->___position_6 = L_19;
		TextEditor_t855 * L_20 = V_0;
		GUIStyle_t184 * L_21 = ___style;
		NullCheck(L_20);
		L_20->___style_5 = L_21;
		TextEditor_t855 * L_22 = V_0;
		bool L_23 = ___multiline;
		NullCheck(L_22);
		L_22->___multiline_7 = L_23;
		TextEditor_t855 * L_24 = V_0;
		int32_t L_25 = ___id;
		NullCheck(L_24);
		L_24->___controlID_3 = L_25;
		TextEditor_t855 * L_26 = V_0;
		NullCheck(L_26);
		TextEditor_ClampPos_m7029(L_26, /*hidden argument*/NULL);
		int32_t L_27 = GUIUtility_get_keyboardControl_m7608(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_28 = ___id;
		if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
		{
			goto IL_00a4;
		}
	}
	{
		Event_t725 * L_29 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		int32_t L_30 = Event_get_type_m7773(L_29, /*hidden argument*/NULL);
		if ((((int32_t)L_30) == ((int32_t)8)))
		{
			goto IL_00a4;
		}
	}
	{
		TextEditor_t855 * L_31 = V_0;
		NullCheck(L_31);
		TextEditor_UpdateScrollOffsetIfNeeded_m7026(L_31, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		Rect_t30  L_32 = ___position;
		int32_t L_33 = ___id;
		GUIContent_t549 * L_34 = ___content;
		bool L_35 = ___multiline;
		int32_t L_36 = ___maxLength;
		GUIStyle_t184 * L_37 = ___style;
		String_t* L_38 = ___secureText;
		uint16_t L_39 = ___maskChar;
		TextEditor_t855 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_HandleTextFieldEventForTouchscreen_m7527(NULL /*static, unused*/, L_32, L_33, L_34, L_35, L_36, L_37, L_38, L_39, L_40, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::HandleTextFieldEventForTouchscreen(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char,UnityEngine.TextEditor)
extern const Il2CppType* TextEditor_t855_0_0_0_var;
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextEditor_t855_il2cpp_TypeInfo_var;
extern "C" void GUI_HandleTextFieldEventForTouchscreen_m7527 (Object_t * __this /* static, unused */, Rect_t30  ___position, int32_t ___id, GUIContent_t549 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t184 * ___style, String_t* ___secureText, uint16_t ___maskChar, TextEditor_t855 * ___editor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t855_0_0_0_var = il2cpp_codegen_type_from_index(562);
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		TextEditor_t855_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(562);
		s_Il2CppMethodIntialized = true;
	}
	Event_t725 * V_0 = {0};
	TextEditor_t855 * V_1 = {0};
	String_t* V_2 = {0};
	int32_t V_3 = {0};
	TextEditor_t855 * G_B11_0 = {0};
	TextEditor_t855 * G_B10_0 = {0};
	String_t* G_B12_0 = {0};
	TextEditor_t855 * G_B12_1 = {0};
	{
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t725 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = Event_get_type_m7773(L_1, /*hidden argument*/NULL);
		V_3 = L_2;
		int32_t L_3 = V_3;
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_4 = V_3;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_00bb;
		}
	}
	{
		goto IL_0159;
	}

IL_001f:
	{
		Event_t725 * L_5 = V_0;
		NullCheck(L_5);
		Vector2_t29  L_6 = Event_get_mousePosition_m7776(L_5, /*hidden argument*/NULL);
		bool L_7 = Rect_Contains_m7871((&___position), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00b6;
		}
	}
	{
		int32_t L_8 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m7606(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		int32_t L_9 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___hotTextField_2;
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		int32_t L_10 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___hotTextField_2;
		int32_t L_11 = ___id;
		if ((((int32_t)L_10) == ((int32_t)L_11)))
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(TextEditor_t855_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		int32_t L_13 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___hotTextField_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		Object_t * L_14 = GUIUtility_GetStateObject_m7605(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_1 = ((TextEditor_t855 *)CastclassClass(L_14, TextEditor_t855_il2cpp_TypeInfo_var));
		TextEditor_t855 * L_15 = V_1;
		NullCheck(L_15);
		L_15->___keyboardOnScreen_0 = (TouchScreenKeyboard_t726 *)NULL;
	}

IL_006e:
	{
		int32_t L_16 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___hotTextField_2 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		int32_t L_17 = GUIUtility_get_keyboardControl_m7608(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = ___id;
		if ((((int32_t)L_17) == ((int32_t)L_18)))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_19 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_set_keyboardControl_m7609(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_0085:
	{
		TextEditor_t855 * L_20 = ___editor;
		String_t* L_21 = ___secureText;
		G_B10_0 = L_20;
		if (!L_21)
		{
			G_B11_0 = L_20;
			goto IL_0095;
		}
	}
	{
		String_t* L_22 = ___secureText;
		G_B12_0 = L_22;
		G_B12_1 = G_B10_0;
		goto IL_009b;
	}

IL_0095:
	{
		GUIContent_t549 * L_23 = ___content;
		NullCheck(L_23);
		String_t* L_24 = GUIContent_get_text_m4240(L_23, /*hidden argument*/NULL);
		G_B12_0 = L_24;
		G_B12_1 = G_B11_0;
	}

IL_009b:
	{
		bool L_25 = ___multiline;
		String_t* L_26 = ___secureText;
		TouchScreenKeyboard_t726 * L_27 = TouchScreenKeyboard_Open_m4298(NULL /*static, unused*/, G_B12_0, 0, 1, L_25, ((((int32_t)((((Object_t*)(String_t*)L_26) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		NullCheck(G_B12_1);
		G_B12_1->___keyboardOnScreen_0 = L_27;
		Event_t725 * L_28 = V_0;
		NullCheck(L_28);
		Event_Use_m7816(L_28, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		goto IL_0159;
	}

IL_00bb:
	{
		TextEditor_t855 * L_29 = ___editor;
		NullCheck(L_29);
		TouchScreenKeyboard_t726 * L_30 = (L_29->___keyboardOnScreen_0);
		if (!L_30)
		{
			goto IL_0126;
		}
	}
	{
		GUIContent_t549 * L_31 = ___content;
		TextEditor_t855 * L_32 = ___editor;
		NullCheck(L_32);
		TouchScreenKeyboard_t726 * L_33 = (L_32->___keyboardOnScreen_0);
		NullCheck(L_33);
		String_t* L_34 = TouchScreenKeyboard_get_text_m4222(L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		GUIContent_set_text_m7683(L_31, L_34, /*hidden argument*/NULL);
		int32_t L_35 = ___maxLength;
		if ((((int32_t)L_35) < ((int32_t)0)))
		{
			goto IL_0107;
		}
	}
	{
		GUIContent_t549 * L_36 = ___content;
		NullCheck(L_36);
		String_t* L_37 = GUIContent_get_text_m4240(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m410(L_37, /*hidden argument*/NULL);
		int32_t L_39 = ___maxLength;
		if ((((int32_t)L_38) <= ((int32_t)L_39)))
		{
			goto IL_0107;
		}
	}
	{
		GUIContent_t549 * L_40 = ___content;
		GUIContent_t549 * L_41 = ___content;
		NullCheck(L_41);
		String_t* L_42 = GUIContent_get_text_m4240(L_41, /*hidden argument*/NULL);
		int32_t L_43 = ___maxLength;
		NullCheck(L_42);
		String_t* L_44 = String_Substring_m411(L_42, 0, L_43, /*hidden argument*/NULL);
		NullCheck(L_40);
		GUIContent_set_text_m7683(L_40, L_44, /*hidden argument*/NULL);
	}

IL_0107:
	{
		TextEditor_t855 * L_45 = ___editor;
		NullCheck(L_45);
		TouchScreenKeyboard_t726 * L_46 = (L_45->___keyboardOnScreen_0);
		NullCheck(L_46);
		bool L_47 = TouchScreenKeyboard_get_done_m4245(L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0126;
		}
	}
	{
		TextEditor_t855 * L_48 = ___editor;
		NullCheck(L_48);
		L_48->___keyboardOnScreen_0 = (TouchScreenKeyboard_t726 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_set_changed_m7515(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0126:
	{
		GUIContent_t549 * L_49 = ___content;
		NullCheck(L_49);
		String_t* L_50 = GUIContent_get_text_m4240(L_49, /*hidden argument*/NULL);
		V_2 = L_50;
		String_t* L_51 = ___secureText;
		if (!L_51)
		{
			goto IL_0142;
		}
	}
	{
		GUIContent_t549 * L_52 = ___content;
		String_t* L_53 = V_2;
		uint16_t L_54 = ___maskChar;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		String_t* L_55 = GUI_PasswordFieldGetStrToShow_m7525(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		GUIContent_set_text_m7683(L_52, L_55, /*hidden argument*/NULL);
	}

IL_0142:
	{
		GUIStyle_t184 * L_56 = ___style;
		Rect_t30  L_57 = ___position;
		GUIContent_t549 * L_58 = ___content;
		int32_t L_59 = ___id;
		NullCheck(L_56);
		GUIStyle_Draw_m7744(L_56, L_57, L_58, L_59, 0, /*hidden argument*/NULL);
		GUIContent_t549 * L_60 = ___content;
		String_t* L_61 = V_2;
		NullCheck(L_60);
		GUIContent_set_text_m7683(L_60, L_61, /*hidden argument*/NULL);
		goto IL_0159;
	}

IL_0159:
	{
		return;
	}
}
// System.Void UnityEngine.GUI::BeginGroup(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUI_BeginGroup_m7528 (Object_t * __this /* static, unused */, Rect_t30  ___position, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t505_StaticFields*)GUI_t505_il2cpp_TypeInfo_var->static_fields)->___beginGroupHash_10;
		int32_t L_1 = GUIUtility_GetControlID_m7604(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIContent_t549 * L_2 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_3 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___none_3;
		if ((!(((Object_t*)(GUIContent_t549 *)L_2) == ((Object_t*)(GUIContent_t549 *)L_3))))
		{
			goto IL_0027;
		}
	}
	{
		GUIStyle_t184 * L_4 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_5 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t184 *)L_4) == ((Object_t*)(GUIStyle_t184 *)L_5)))
		{
			goto IL_006d;
		}
	}

IL_0027:
	{
		Event_t725 * L_6 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Event_get_type_m7773(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) == ((int32_t)7)))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_004c;
	}

IL_003e:
	{
		GUIStyle_t184 * L_9 = ___style;
		Rect_t30  L_10 = ___position;
		GUIContent_t549 * L_11 = ___content;
		int32_t L_12 = V_0;
		NullCheck(L_9);
		GUIStyle_Draw_m7743(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		goto IL_006d;
	}

IL_004c:
	{
		Event_t725 * L_13 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector2_t29  L_14 = Event_get_mousePosition_m7776(L_13, /*hidden argument*/NULL);
		bool L_15 = Rect_Contains_m7871((&___position), L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_set_mouseUsed_m7620(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0068:
	{
		goto IL_006d;
	}

IL_006d:
	{
		Rect_t30  L_16 = ___position;
		Vector2_t29  L_17 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t29  L_18 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIClip_Push_m7621(NULL /*static, unused*/, L_16, L_17, L_18, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::EndGroup()
extern "C" void GUI_EndGroup_m7529 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		GUIClip_Pop_m7623(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUI_Window_m2701 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t30  ___clientRect, WindowFunction_t548 * ___func, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		Rect_t30  L_1 = ___clientRect;
		WindowFunction_t548 * L_2 = ___func;
		String_t* L_3 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_4 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_5 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_t184 * L_6 = GUISkin_get_window_m2470(L_5, /*hidden argument*/NULL);
		GUISkin_t547 * L_7 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_8 = GUI_DoWindow_m7533(NULL /*static, unused*/, L_0, L_1, L_2, L_4, L_6, L_7, 1, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Rect UnityEngine.GUI::ModalWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String,UnityEngine.GUIStyle)
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUI_ModalWindow_m2474 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t30  ___clientRect, WindowFunction_t548 * ___func, String_t* ___text, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		Rect_t30  L_1 = ___clientRect;
		WindowFunction_t548 * L_2 = ___func;
		String_t* L_3 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_4 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		GUIStyle_t184 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_6 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_7 = GUI_DoModalWindow_m7530(NULL /*static, unused*/, L_0, L_1, L_2, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Rect UnityEngine.GUI::DoModalWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUI_DoModalWindow_m7530 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t30  ___clientRect, WindowFunction_t548 * ___func, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, GUISkin_t547 * ___skin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		WindowFunction_t548 * L_1 = ___func;
		GUIContent_t549 * L_2 = ___content;
		GUIStyle_t184 * L_3 = ___style;
		GUISkin_t547 * L_4 = ___skin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		Rect_t30  L_5 = GUI_INTERNAL_CALL_DoModalWindow_m7531(NULL /*static, unused*/, L_0, (&___clientRect), L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoModalWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin)
extern "C" Rect_t30  GUI_INTERNAL_CALL_DoModalWindow_m7531 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t30 * ___clientRect, WindowFunction_t548 * ___func, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, GUISkin_t547 * ___skin, const MethodInfo* method)
{
	typedef Rect_t30  (*GUI_INTERNAL_CALL_DoModalWindow_m7531_ftn) (int32_t, Rect_t30 *, WindowFunction_t548 *, GUIContent_t549 *, GUIStyle_t184 *, GUISkin_t547 *);
	static GUI_INTERNAL_CALL_DoModalWindow_m7531_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoModalWindow_m7531_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoModalWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin)");
	return _il2cpp_icall_func(___id, ___clientRect, ___func, ___content, ___style, ___skin);
}
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var;
extern "C" void GUI_CallWindowDelegate_m7532 (Object_t * __this /* static, unused */, WindowFunction_t548 * ___func, int32_t ___id, GUISkin_t547 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(287);
		s_Il2CppMethodIntialized = true;
	}
	GUISkin_t547 * V_0 = {0};
	GUILayoutOptionU5BU5D_t550* V_1 = {0};
	{
		int32_t L_0 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m7548(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_1 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t725 * L_2 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m7773(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_4 = ___forceRect;
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		GUILayoutOptionU5BU5D_t550* L_5 = ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 2));
		float L_6 = ___width;
		GUILayoutOption_t553 * L_7 = GUILayout_Width_m2550(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t553 **)(GUILayoutOption_t553 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t553 *))) = (GUILayoutOption_t553 *)L_7;
		GUILayoutOptionU5BU5D_t550* L_8 = L_5;
		float L_9 = ___height;
		GUILayoutOption_t553 * L_10 = GUILayout_Height_m7543(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		*((GUILayoutOption_t553 **)(GUILayoutOption_t553 **)SZArrayLdElema(L_8, 1, sizeof(GUILayoutOption_t553 *))) = (GUILayoutOption_t553 *)L_10;
		V_1 = L_8;
		int32_t L_11 = ___id;
		GUIStyle_t184 * L_12 = ___style;
		GUILayoutOptionU5BU5D_t550* L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m7550(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_004d:
	{
		int32_t L_14 = ___id;
		GUIStyle_t184 * L_15 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m7550(NULL /*static, unused*/, L_14, L_15, (GUILayoutOptionU5BU5D_t550*)(GUILayoutOptionU5BU5D_t550*)NULL, /*hidden argument*/NULL);
	}

IL_0056:
	{
		GUISkin_t547 * L_16 = ____skin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_set_skin_m7510(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		WindowFunction_t548 * L_17 = ___func;
		int32_t L_18 = ___id;
		NullCheck(L_17);
		WindowFunction_Invoke_m7505(L_17, L_18, /*hidden argument*/NULL);
		Event_t725 * L_19 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = Event_get_type_m7773(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)8))))
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutUtility_Layout_m7552(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0078:
	{
		GUISkin_t547 * L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_set_skin_m7510(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.GUI::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUI_DoWindow_m7533 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t30  ___clientRect, WindowFunction_t548 * ___func, GUIContent_t549 * ___title, GUIStyle_t184 * ___style, GUISkin_t547 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		WindowFunction_t548 * L_1 = ___func;
		GUIContent_t549 * L_2 = ___title;
		GUIStyle_t184 * L_3 = ___style;
		GUISkin_t547 * L_4 = ___skin;
		bool L_5 = ___forceRectOnLayout;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		Rect_t30  L_6 = GUI_INTERNAL_CALL_DoWindow_m7534(NULL /*static, unused*/, L_0, (&___clientRect), L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern "C" Rect_t30  GUI_INTERNAL_CALL_DoWindow_m7534 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t30 * ___clientRect, WindowFunction_t548 * ___func, GUIContent_t549 * ___title, GUIStyle_t184 * ___style, GUISkin_t547 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method)
{
	typedef Rect_t30  (*GUI_INTERNAL_CALL_DoWindow_m7534_ftn) (int32_t, Rect_t30 *, WindowFunction_t548 *, GUIContent_t549 *, GUIStyle_t184 *, GUISkin_t547 *, bool);
	static GUI_INTERNAL_CALL_DoWindow_m7534_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoWindow_m7534_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)");
	return _il2cpp_icall_func(___id, ___clientRect, ___func, ___title, ___style, ___skin, ___forceRectOnLayout);
}
// System.Void UnityEngine.GUILayout::Label(UnityEngine.Texture,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Label_m7535 (Object_t * __this /* static, unused */, Texture_t731 * ___image, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t731 * L_0 = ___image;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_1 = GUIContent_Temp_m7685(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_2 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t184 * L_3 = GUISkin_get_label_m7635(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_4 = ___options;
		GUILayout_DoLabel_m7536(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Label_m2477 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_1 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_2 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t184 * L_3 = GUISkin_get_label_m7635(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_4 = ___options;
		GUILayout_DoLabel_m7536(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::DoLabel(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUILayout_DoLabel_m7536 (Object_t * __this /* static, unused */, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t549 * L_0 = ___content;
		GUIStyle_t184 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t550* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_3 = GUILayoutUtility_GetRect_m7563(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t549 * L_4 = ___content;
		GUIStyle_t184 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_Label_m7516(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_Button_m2495 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_1 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_2 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t184 * L_3 = GUISkin_get_button_m2482(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_4 = ___options;
		bool L_5 = GUILayout_DoButton_m7537(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.GUILayout::DoButton(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_DoButton_m7537 (Object_t * __this /* static, unused */, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t549 * L_0 = ___content;
		GUIStyle_t184 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t550* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_3 = GUILayoutUtility_GetRect_m7563(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t549 * L_4 = ___content;
		GUIStyle_t184 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		bool L_6 = GUI_Button_m2485(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String UnityEngine.GUILayout::TextField(System.String,System.Int32,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" String_t* GUILayout_TextField_m2551 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___maxLength, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		int32_t L_1 = ___maxLength;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_2 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t184 * L_3 = GUISkin_get_textField_m7637(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_4 = ___options;
		String_t* L_5 = GUILayout_DoTextField_m7538(NULL /*static, unused*/, L_0, L_1, 0, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.String UnityEngine.GUILayout::TextField(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" String_t* GUILayout_TextField_m2494 (Object_t * __this /* static, unused */, String_t* ___text, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	{
		String_t* L_0 = ___text;
		GUIStyle_t184 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t550* L_2 = ___options;
		String_t* L_3 = GUILayout_DoTextField_m7538(NULL /*static, unused*/, L_0, (-1), 0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.GUILayout::DoTextField(System.String,System.Int32,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" String_t* GUILayout_DoTextField_m7538 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___maxLength, bool ___multiline, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GUIContent_t549 * V_1 = {0};
	Rect_t30  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_GetControlID_m7603(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_2 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = GUIUtility_get_keyboardControl_m7608(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_5 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_6 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0036;
	}

IL_0025:
	{
		String_t* L_7 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		String_t* L_8 = Input_get_compositionString_m4235(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m409(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_10 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_0036:
	{
		GUIContent_t549 * L_11 = V_1;
		GUIStyle_t184 * L_12 = ___style;
		GUILayoutOptionU5BU5D_t550* L_13 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_14 = GUILayoutUtility_GetRect_m7563(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		int32_t L_15 = GUIUtility_get_keyboardControl_m7608(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0052;
		}
	}
	{
		String_t* L_17 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_18 = GUIContent_Temp_m7684(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
	}

IL_0052:
	{
		Rect_t30  L_19 = V_2;
		int32_t L_20 = V_0;
		GUIContent_t549 * L_21 = V_1;
		bool L_22 = ___multiline;
		int32_t L_23 = ___maxLength;
		GUIStyle_t184 * L_24 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_DoTextField_m7526(NULL /*static, unused*/, L_19, L_20, L_21, L_22, L_23, L_24, (String_t*)NULL, 0, /*hidden argument*/NULL);
		GUIContent_t549 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = GUIContent_get_text_m4240(L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.Void UnityEngine.GUILayout::Space(System.Single)
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Space_m2475 (Object_t * __this /* static, unused */, float ___pixels, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(287);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_0 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t1383 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		bool L_2 = (L_1->___isVertical_11);
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		float L_3 = ___pixels;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_4 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_5 = ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 1));
		float L_6 = ___pixels;
		GUILayoutOption_t553 * L_7 = GUILayout_Height_m7543(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t553 **)(GUILayoutOption_t553 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t553 *))) = (GUILayoutOption_t553 *)L_7;
		GUILayoutUtility_GetRect_m7565(NULL /*static, unused*/, (0.0f), L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_003e:
	{
		float L_8 = ___pixels;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_9 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_10 = ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 1));
		float L_11 = ___pixels;
		GUILayoutOption_t553 * L_12 = GUILayout_Width_m2550(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_12);
		*((GUILayoutOption_t553 **)(GUILayoutOption_t553 **)SZArrayLdElema(L_10, 0, sizeof(GUILayoutOption_t553 *))) = (GUILayoutOption_t553 *)L_12;
		GUILayoutUtility_GetRect_m7565(NULL /*static, unused*/, L_8, (0.0f), L_9, L_10, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::FlexibleSpace()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var;
extern "C" void GUILayout_FlexibleSpace_m2480 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(287);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t553 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_0 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t1383 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		bool L_2 = (L_1->___isVertical_11);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		GUILayoutOption_t553 * L_3 = GUILayout_ExpandHeight_m7545(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002c;
	}

IL_0025:
	{
		GUILayoutOption_t553 * L_4 = GUILayout_ExpandWidth_m7544(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002c:
	{
		GUILayoutOption_t553 * L_5 = V_0;
		int32_t L_6 = ((int32_t)10000);
		Object_t * L_7 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_5);
		L_5->___value_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_8 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_9 = ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 1));
		GUILayoutOption_t553 * L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_10);
		*((GUILayoutOption_t553 **)(GUILayoutOption_t553 **)SZArrayLdElema(L_9, 0, sizeof(GUILayoutOption_t553 *))) = (GUILayoutOption_t553 *)L_10;
		GUILayoutUtility_GetRect_m7565(NULL /*static, unused*/, (0.0f), (0.0f), L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m2479 (Object_t * __this /* static, unused */, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_0 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___none_3;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_1 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_2 = ___options;
		GUILayout_BeginHorizontal_m7540(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m7539 (Object_t * __this /* static, unused */, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_0 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___none_3;
		GUIStyle_t184 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t550* L_2 = ___options;
		GUILayout_BeginHorizontal_m7540(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern const Il2CppType* GUILayoutGroup_t1383_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m7540 (Object_t * __this /* static, unused */, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t1383_0_0_0_var = il2cpp_codegen_type_from_index(954);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t1383 * V_0 = {0};
	{
		GUIStyle_t184 * L_0 = ___style;
		GUILayoutOptionU5BU5D_t550* L_1 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t1383_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutGroup_t1383 * L_3 = GUILayoutUtility_BeginLayoutGroup_m7560(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GUILayoutGroup_t1383 * L_4 = V_0;
		NullCheck(L_4);
		L_4->___isVertical_11 = 0;
		GUIStyle_t184 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_6 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t184 *)L_5) == ((Object_t*)(GUIStyle_t184 *)L_6))))
		{
			goto IL_002f;
		}
	}
	{
		GUIContent_t549 * L_7 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_8 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___none_3;
		if ((((Object_t*)(GUIContent_t549 *)L_7) == ((Object_t*)(GUIContent_t549 *)L_8)))
		{
			goto IL_003c;
		}
	}

IL_002f:
	{
		GUILayoutGroup_t1383 * L_9 = V_0;
		NullCheck(L_9);
		Rect_t30  L_10 = (((GUILayoutEntry_t1385 *)L_9)->___rect_4);
		GUIContent_t549 * L_11 = ___content;
		GUIStyle_t184 * L_12 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_Box_m7522(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndHorizontal()
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1176;
extern "C" void GUILayout_EndHorizontal_m2486 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		_stringLiteral1176 = il2cpp_codegen_string_literal_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m7551(NULL /*static, unused*/, _stringLiteral1176, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m7561(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginVertical_m2476 (Object_t * __this /* static, unused */, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_0 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___none_3;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_1 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_2 = ___options;
		GUILayout_BeginVertical_m7541(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern const Il2CppType* GUILayoutGroup_t1383_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginVertical_m7541 (Object_t * __this /* static, unused */, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t1383_0_0_0_var = il2cpp_codegen_type_from_index(954);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t1383 * V_0 = {0};
	{
		GUIStyle_t184 * L_0 = ___style;
		GUILayoutOptionU5BU5D_t550* L_1 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t1383_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutGroup_t1383 * L_3 = GUILayoutUtility_BeginLayoutGroup_m7560(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GUILayoutGroup_t1383 * L_4 = V_0;
		NullCheck(L_4);
		L_4->___isVertical_11 = 1;
		GUIStyle_t184 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_6 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t184 *)L_5) == ((Object_t*)(GUIStyle_t184 *)L_6)))
		{
			goto IL_0031;
		}
	}
	{
		GUILayoutGroup_t1383 * L_7 = V_0;
		NullCheck(L_7);
		Rect_t30  L_8 = (((GUILayoutEntry_t1385 *)L_7)->___rect_4);
		GUIContent_t549 * L_9 = ___content;
		GUIStyle_t184 * L_10 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_Box_m7522(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndVertical()
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1177;
extern "C" void GUILayout_EndVertical_m2478 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		_stringLiteral1177 = il2cpp_codegen_string_literal_from_index(1177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m7551(NULL /*static, unused*/, _stringLiteral1177, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m7561(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect)
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginArea_m2549 (Object_t * __this /* static, unused */, Rect_t30  ___screenRect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = ___screenRect;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_1 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___none_3;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_2 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginArea_m7542(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern const Il2CppType* GUILayoutGroup_t1383_0_0_0_var;
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginArea_m7542 (Object_t * __this /* static, unused */, Rect_t30  ___screenRect, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t1383_0_0_0_var = il2cpp_codegen_type_from_index(954);
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t1383 * V_0 = {0};
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t184 * L_0 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t1383_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutGroup_t1383 * L_2 = GUILayoutUtility_BeginLayoutArea_m7562(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Event_t725 * L_3 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = Event_get_type_m7773(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)8))))
		{
			goto IL_0088;
		}
	}
	{
		GUILayoutGroup_t1383 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___resetCoords_12 = 1;
		GUILayoutGroup_t1383 * L_6 = V_0;
		GUILayoutGroup_t1383 * L_7 = V_0;
		float L_8 = Rect_get_width_m370((&___screenRect), /*hidden argument*/NULL);
		float L_9 = L_8;
		V_1 = L_9;
		NullCheck(L_7);
		((GUILayoutEntry_t1385 *)L_7)->___maxWidth_1 = L_9;
		float L_10 = V_1;
		NullCheck(L_6);
		((GUILayoutEntry_t1385 *)L_6)->___minWidth_0 = L_10;
		GUILayoutGroup_t1383 * L_11 = V_0;
		GUILayoutGroup_t1383 * L_12 = V_0;
		float L_13 = Rect_get_height_m372((&___screenRect), /*hidden argument*/NULL);
		float L_14 = L_13;
		V_1 = L_14;
		NullCheck(L_12);
		((GUILayoutEntry_t1385 *)L_12)->___maxHeight_3 = L_14;
		float L_15 = V_1;
		NullCheck(L_11);
		((GUILayoutEntry_t1385 *)L_11)->___minHeight_2 = L_15;
		GUILayoutGroup_t1383 * L_16 = V_0;
		float L_17 = Rect_get_xMin_m4217((&___screenRect), /*hidden argument*/NULL);
		float L_18 = Rect_get_yMin_m4216((&___screenRect), /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_19 = V_0;
		NullCheck(L_19);
		Rect_t30 * L_20 = &(((GUILayoutEntry_t1385 *)L_19)->___rect_4);
		float L_21 = Rect_get_xMax_m4208(L_20, /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_22 = V_0;
		NullCheck(L_22);
		Rect_t30 * L_23 = &(((GUILayoutEntry_t1385 *)L_22)->___rect_4);
		float L_24 = Rect_get_yMax_m4209(L_23, /*hidden argument*/NULL);
		Rect_t30  L_25 = Rect_MinMaxRect_m7869(NULL /*static, unused*/, L_17, L_18, L_21, L_24, /*hidden argument*/NULL);
		NullCheck(L_16);
		((GUILayoutEntry_t1385 *)L_16)->___rect_4 = L_25;
	}

IL_0088:
	{
		GUILayoutGroup_t1383 * L_26 = V_0;
		NullCheck(L_26);
		Rect_t30  L_27 = (((GUILayoutEntry_t1385 *)L_26)->___rect_4);
		GUIContent_t549 * L_28 = ___content;
		GUIStyle_t184 * L_29 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_BeginGroup_m7528(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndArea()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern "C" void GUILayout_EndArea_m2556 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_2 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GenericStack_t1309 * L_3 = (L_2->___layoutGroups_1);
		NullCheck(L_3);
		VirtFuncInvoker0< Object_t * >::Invoke(18 /* System.Object System.Collections.Stack::Pop() */, L_3);
		LayoutCache_t1382 * L_4 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_5 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GenericStack_t1309 * L_6 = (L_5->___layoutGroups_1);
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(17 /* System.Object System.Collections.Stack::Peek() */, L_6);
		NullCheck(L_4);
		L_4->___topLevel_0 = ((GUILayoutGroup_t1383 *)CastclassClass(L_7, GUILayoutGroup_t1383_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_EndGroup_m7529(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t553_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t553 * GUILayout_Width_m2550 (Object_t * __this /* static, unused */, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutOption_t553_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(288);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___width;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t36_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t553 * L_3 = (GUILayoutOption_t553 *)il2cpp_codegen_object_new (GUILayoutOption_t553_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m7601(L_3, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MinWidth(System.Single)
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t553_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t553 * GUILayout_MinWidth_m2493 (Object_t * __this /* static, unused */, float ___minWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutOption_t553_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(288);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___minWidth;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t36_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t553 * L_3 = (GUILayoutOption_t553 *)il2cpp_codegen_object_new (GUILayoutOption_t553_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m7601(L_3, 2, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Height(System.Single)
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t553_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t553 * GUILayout_Height_m7543 (Object_t * __this /* static, unused */, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutOption_t553_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(288);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___height;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t36_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t553 * L_3 = (GUILayoutOption_t553 *)il2cpp_codegen_object_new (GUILayoutOption_t553_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m7601(L_3, 1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandWidth(System.Boolean)
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t553_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t553 * GUILayout_ExpandWidth_m7544 (Object_t * __this /* static, unused */, bool ___expand, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		GUILayoutOption_t553_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(288);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		bool L_0 = ___expand;
		G_B1_0 = 6;
		if (!L_0)
		{
			G_B2_0 = 6;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		int32_t L_1 = G_B3_0;
		Object_t * L_2 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t553 * L_3 = (GUILayoutOption_t553 *)il2cpp_codegen_object_new (GUILayoutOption_t553_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m7601(L_3, G_B3_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandHeight(System.Boolean)
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t553_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t553 * GUILayout_ExpandHeight_m7545 (Object_t * __this /* static, unused */, bool ___expand, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		GUILayoutOption_t553_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(288);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		bool L_0 = ___expand;
		G_B1_0 = 7;
		if (!L_0)
		{
			G_B2_0 = 7;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		int32_t L_1 = G_B3_0;
		Object_t * L_2 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t553 * L_3 = (GUILayoutOption_t553 *)il2cpp_codegen_object_new (GUILayoutOption_t553_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m7601(L_3, G_B3_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.GUILayoutUtility/LayoutCache::.ctor()
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t1309_il2cpp_TypeInfo_var;
extern "C" void LayoutCache__ctor_m7546 (LayoutCache_t1382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		GenericStack_t1309_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(952);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutGroup_t1383 * L_0 = (GUILayoutGroup_t1383 *)il2cpp_codegen_object_new (GUILayoutGroup_t1383_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m7581(L_0, /*hidden argument*/NULL);
		__this->___topLevel_0 = L_0;
		GenericStack_t1309 * L_1 = (GenericStack_t1309 *)il2cpp_codegen_object_new (GenericStack_t1309_il2cpp_TypeInfo_var);
		GenericStack__ctor_m7126(L_1, /*hidden argument*/NULL);
		__this->___layoutGroups_1 = L_1;
		GUILayoutGroup_t1383 * L_2 = (GUILayoutGroup_t1383 *)il2cpp_codegen_object_new (GUILayoutGroup_t1383_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m7581(L_2, /*hidden argument*/NULL);
		__this->___windows_2 = L_2;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		GenericStack_t1309 * L_3 = (__this->___layoutGroups_1);
		GUILayoutGroup_t1383 * L_4 = (__this->___topLevel_0);
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(19 /* System.Void System.Collections.Stack::Push(System.Object) */, L_3, L_4);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::.cctor()
extern TypeInfo* Dictionary_2_t1384_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t1382_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m8180_MethodInfo_var;
extern "C" void GUILayoutUtility__cctor_m7547 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1384_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		LayoutCache_t1382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(955);
		Dictionary_2__ctor_m8180_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484720);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1384 * L_0 = (Dictionary_2_t1384 *)il2cpp_codegen_object_new (Dictionary_2_t1384_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m8180(L_0, /*hidden argument*/Dictionary_2__ctor_m8180_MethodInfo_var);
		((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___storedLayouts_0 = L_0;
		Dictionary_2_t1384 * L_1 = (Dictionary_2_t1384 *)il2cpp_codegen_object_new (Dictionary_2_t1384_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m8180(L_1, /*hidden argument*/Dictionary_2__ctor_m8180_MethodInfo_var);
		((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___storedWindows_1 = L_1;
		LayoutCache_t1382 * L_2 = (LayoutCache_t1382 *)il2cpp_codegen_object_new (LayoutCache_t1382_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m7546(L_2, /*hidden argument*/NULL);
		((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2 = L_2;
		Rect_t30  L_3 = {0};
		Rect__ctor_m387(&L_3, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3 = L_3;
		return;
	}
}
// UnityEngine.GUILayoutUtility/LayoutCache UnityEngine.GUILayoutUtility::SelectIDList(System.Int32,System.Boolean)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t1382_il2cpp_TypeInfo_var;
extern "C" LayoutCache_t1382 * GUILayoutUtility_SelectIDList_m7548 (Object_t * __this /* static, unused */, int32_t ___instanceID, bool ___isWindow, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		LayoutCache_t1382_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(955);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1384 * V_0 = {0};
	LayoutCache_t1382 * V_1 = {0};
	Dictionary_2_t1384 * G_B3_0 = {0};
	{
		bool L_0 = ___isWindow;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Dictionary_2_t1384 * L_1 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___storedWindows_1;
		G_B3_0 = L_1;
		goto IL_0015;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Dictionary_2_t1384 * L_2 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___storedLayouts_0;
		G_B3_0 = L_2;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		Dictionary_2_t1384 * L_3 = V_0;
		int32_t L_4 = ___instanceID;
		NullCheck(L_3);
		bool L_5 = (bool)VirtFuncInvoker2< bool, int32_t, LayoutCache_t1382 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::TryGetValue(!0,!1&) */, L_3, L_4, (&V_1));
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		LayoutCache_t1382 * L_6 = (LayoutCache_t1382 *)il2cpp_codegen_object_new (LayoutCache_t1382_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m7546(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		Dictionary_2_t1384 * L_7 = V_0;
		int32_t L_8 = ___instanceID;
		LayoutCache_t1382 * L_9 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, LayoutCache_t1382 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Item(!0,!1) */, L_7, L_8, L_9);
		goto IL_0037;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_10 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_11 = V_1;
		NullCheck(L_11);
		GUILayoutGroup_t1383 * L_12 = (L_11->___topLevel_0);
		NullCheck(L_10);
		L_10->___topLevel_0 = L_12;
		LayoutCache_t1382 * L_13 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_14 = V_1;
		NullCheck(L_14);
		GenericStack_t1309 * L_15 = (L_14->___layoutGroups_1);
		NullCheck(L_13);
		L_13->___layoutGroups_1 = L_15;
		LayoutCache_t1382 * L_16 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_17 = V_1;
		NullCheck(L_17);
		GUILayoutGroup_t1383 * L_18 = (L_17->___windows_2);
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		LayoutCache_t1382 * L_19 = V_1;
		return L_19;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Begin(System.Int32)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Begin_m7549 (Object_t * __this /* static, unused */, int32_t ___instanceID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t1382 * V_0 = {0};
	GUILayoutGroup_t1383 * V_1 = {0};
	{
		int32_t L_0 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_1 = GUILayoutUtility_SelectIDList_m7548(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t725 * L_2 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m7773(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_4 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_5 = V_0;
		GUILayoutGroup_t1383 * L_6 = (GUILayoutGroup_t1383 *)il2cpp_codegen_object_new (GUILayoutGroup_t1383_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m7581(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t1383 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t1382 * L_9 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GenericStack_t1309 * L_10 = (L_9->___layoutGroups_1);
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Stack::Clear() */, L_10);
		LayoutCache_t1382 * L_11 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GenericStack_t1309 * L_12 = (L_11->___layoutGroups_1);
		LayoutCache_t1382 * L_13 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_13);
		GUILayoutGroup_t1383 * L_14 = (L_13->___topLevel_0);
		NullCheck(L_12);
		VirtActionInvoker1< Object_t * >::Invoke(19 /* System.Void System.Collections.Stack::Push(System.Object) */, L_12, L_14);
		LayoutCache_t1382 * L_15 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_16 = V_0;
		GUILayoutGroup_t1383 * L_17 = (GUILayoutGroup_t1383 *)il2cpp_codegen_object_new (GUILayoutGroup_t1383_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m7581(L_17, /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_18 = L_17;
		V_1 = L_18;
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		GUILayoutGroup_t1383 * L_19 = V_1;
		NullCheck(L_15);
		L_15->___windows_2 = L_19;
		goto IL_00a5;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_20 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_21 = V_0;
		NullCheck(L_21);
		GUILayoutGroup_t1383 * L_22 = (L_21->___topLevel_0);
		NullCheck(L_20);
		L_20->___topLevel_0 = L_22;
		LayoutCache_t1382 * L_23 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_24 = V_0;
		NullCheck(L_24);
		GenericStack_t1309 * L_25 = (L_24->___layoutGroups_1);
		NullCheck(L_23);
		L_23->___layoutGroups_1 = L_25;
		LayoutCache_t1382 * L_26 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_27 = V_0;
		NullCheck(L_27);
		GUILayoutGroup_t1383 * L_28 = (L_27->___windows_2);
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
	}

IL_00a5:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::BeginWindow(System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_BeginWindow_m7550 (Object_t * __this /* static, unused */, int32_t ___windowID, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t1382 * V_0 = {0};
	GUILayoutGroup_t1383 * V_1 = {0};
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_1 = GUILayoutUtility_SelectIDList_m7548(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t725 * L_2 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m7773(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_00ab;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_4 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_5 = V_0;
		GUILayoutGroup_t1383 * L_6 = (GUILayoutGroup_t1383 *)il2cpp_codegen_object_new (GUILayoutGroup_t1383_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m7581(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t1383 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t1382 * L_9 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GUILayoutGroup_t1383 * L_10 = (L_9->___topLevel_0);
		GUIStyle_t184 * L_11 = ___style;
		NullCheck(L_10);
		GUILayoutEntry_set_style_m7572(L_10, L_11, /*hidden argument*/NULL);
		LayoutCache_t1382 * L_12 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t1383 * L_13 = (L_12->___topLevel_0);
		int32_t L_14 = ___windowID;
		NullCheck(L_13);
		L_13->___windowID_16 = L_14;
		GUILayoutOptionU5BU5D_t550* L_15 = ___options;
		if (!L_15)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_16 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_16);
		GUILayoutGroup_t1383 * L_17 = (L_16->___topLevel_0);
		GUILayoutOptionU5BU5D_t550* L_18 = ___options;
		NullCheck(L_17);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t550* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_17, L_18);
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_19 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_19);
		GenericStack_t1309 * L_20 = (L_19->___layoutGroups_1);
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Stack::Clear() */, L_20);
		LayoutCache_t1382 * L_21 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_21);
		GenericStack_t1309 * L_22 = (L_21->___layoutGroups_1);
		LayoutCache_t1382 * L_23 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t1383 * L_24 = (L_23->___topLevel_0);
		NullCheck(L_22);
		VirtActionInvoker1< Object_t * >::Invoke(19 /* System.Void System.Collections.Stack::Push(System.Object) */, L_22, L_24);
		LayoutCache_t1382 * L_25 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_26 = V_0;
		GUILayoutGroup_t1383 * L_27 = (GUILayoutGroup_t1383 *)il2cpp_codegen_object_new (GUILayoutGroup_t1383_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m7581(L_27, /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_28 = L_27;
		V_1 = L_28;
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
		GUILayoutGroup_t1383 * L_29 = V_1;
		NullCheck(L_25);
		L_25->___windows_2 = L_29;
		goto IL_00db;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_30 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_31 = V_0;
		NullCheck(L_31);
		GUILayoutGroup_t1383 * L_32 = (L_31->___topLevel_0);
		NullCheck(L_30);
		L_30->___topLevel_0 = L_32;
		LayoutCache_t1382 * L_33 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_34 = V_0;
		NullCheck(L_34);
		GenericStack_t1309 * L_35 = (L_34->___layoutGroups_1);
		NullCheck(L_33);
		L_33->___layoutGroups_1 = L_35;
		LayoutCache_t1382 * L_36 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_37 = V_0;
		NullCheck(L_37);
		GUILayoutGroup_t1383 * L_38 = (L_37->___windows_2);
		NullCheck(L_36);
		L_36->___windows_2 = L_38;
	}

IL_00db:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::EndGroup(System.String)
extern "C" void GUILayoutUtility_EndGroup_m7551 (Object_t * __this /* static, unused */, String_t* ___groupName, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Layout()
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Layout_m7552 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_0 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t1383 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		int32_t L_2 = (L_1->___windowID_16);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_3 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_3);
		GUILayoutGroup_t1383 * L_4 = (L_3->___topLevel_0);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_4);
		LayoutCache_t1382 * L_5 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GUILayoutGroup_t1383 * L_6 = (L_5->___topLevel_0);
		int32_t L_7 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t1382 * L_8 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_8);
		GUILayoutGroup_t1383 * L_9 = (L_8->___topLevel_0);
		NullCheck(L_9);
		float L_10 = (((GUILayoutEntry_t1385 *)L_9)->___maxWidth_1);
		float L_11 = Mathf_Min_m376(NULL /*static, unused*/, (((float)((float)L_7))), L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_6, (0.0f), L_11);
		LayoutCache_t1382 * L_12 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t1383 * L_13 = (L_12->___topLevel_0);
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_13);
		LayoutCache_t1382 * L_14 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_14);
		GUILayoutGroup_t1383 * L_15 = (L_14->___topLevel_0);
		int32_t L_16 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t1382 * L_17 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_17);
		GUILayoutGroup_t1383 * L_18 = (L_17->___topLevel_0);
		NullCheck(L_18);
		float L_19 = (((GUILayoutEntry_t1385 *)L_18)->___maxHeight_3);
		float L_20 = Mathf_Min_m376(NULL /*static, unused*/, (((float)((float)L_16))), L_19, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_15, (0.0f), L_20);
		LayoutCache_t1382 * L_21 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_21);
		GUILayoutGroup_t1383 * L_22 = (L_21->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m7554(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_00a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_23 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t1383 * L_24 = (L_23->___topLevel_0);
		GUILayoutUtility_LayoutSingleGroup_m7555(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		LayoutCache_t1382 * L_25 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GUILayoutGroup_t1383 * L_26 = (L_25->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m7554(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFromEditorWindow()
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m7553 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_0 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t1383 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_1);
		LayoutCache_t1382 * L_2 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GUILayoutGroup_t1383 * L_3 = (L_2->___topLevel_0);
		int32_t L_4 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_3, (0.0f), (((float)((float)L_4))));
		LayoutCache_t1382 * L_5 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GUILayoutGroup_t1383 * L_6 = (L_5->___topLevel_0);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_6);
		LayoutCache_t1382 * L_7 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_7);
		GUILayoutGroup_t1383 * L_8 = (L_7->___topLevel_0);
		int32_t L_9 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_8, (0.0f), (((float)((float)L_9))));
		LayoutCache_t1382 * L_10 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_10);
		GUILayoutGroup_t1383 * L_11 = (L_10->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m7554(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFreeGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1470_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8181_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8182_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8183_MethodInfo_var;
extern "C" void GUILayoutUtility_LayoutFreeGroup_m7554 (Object_t * __this /* static, unused */, GUILayoutGroup_t1383 * ___toplevel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		Enumerator_t1470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		List_1_GetEnumerator_m8181_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484721);
		Enumerator_get_Current_m8182_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484722);
		Enumerator_MoveNext_m8183_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484723);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t1383 * V_0 = {0};
	Enumerator_t1470  V_1 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GUILayoutGroup_t1383 * L_0 = ___toplevel;
		NullCheck(L_0);
		List_1_t1386 * L_1 = (L_0->___entries_10);
		NullCheck(L_1);
		Enumerator_t1470  L_2 = List_1_GetEnumerator_m8181(L_1, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_1 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			GUILayoutEntry_t1385 * L_3 = Enumerator_get_Current_m8182((&V_1), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_0 = ((GUILayoutGroup_t1383 *)CastclassClass(L_3, GUILayoutGroup_t1383_il2cpp_TypeInfo_var));
			GUILayoutGroup_t1383 * L_4 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutSingleGroup_m7555(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m8183((&V_1), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_6 = V_1;
		Enumerator_t1470  L_7 = L_6;
		Object_t * L_8 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_8);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0041:
	{
		GUILayoutGroup_t1383 * L_9 = ___toplevel;
		NullCheck(L_9);
		GUILayoutGroup_ResetCursor_m7585(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutSingleGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutSingleGroup_m7555 (Object_t * __this /* static, unused */, GUILayoutGroup_t1383 * ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Rect_t30  V_4 = {0};
	{
		GUILayoutGroup_t1383 * L_0 = ___i;
		NullCheck(L_0);
		bool L_1 = (L_0->___isWindow_15);
		if (L_1)
		{
			goto IL_0074;
		}
	}
	{
		GUILayoutGroup_t1383 * L_2 = ___i;
		NullCheck(L_2);
		float L_3 = (((GUILayoutEntry_t1385 *)L_2)->___minWidth_0);
		V_0 = L_3;
		GUILayoutGroup_t1383 * L_4 = ___i;
		NullCheck(L_4);
		float L_5 = (((GUILayoutEntry_t1385 *)L_4)->___maxWidth_1);
		V_1 = L_5;
		GUILayoutGroup_t1383 * L_6 = ___i;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_6);
		GUILayoutGroup_t1383 * L_7 = ___i;
		GUILayoutGroup_t1383 * L_8 = ___i;
		NullCheck(L_8);
		Rect_t30 * L_9 = &(((GUILayoutEntry_t1385 *)L_8)->___rect_4);
		float L_10 = Rect_get_x_m366(L_9, /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_11 = ___i;
		NullCheck(L_11);
		float L_12 = (((GUILayoutEntry_t1385 *)L_11)->___maxWidth_1);
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = Mathf_Clamp_m375(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_7, L_10, L_15);
		GUILayoutGroup_t1383 * L_16 = ___i;
		NullCheck(L_16);
		float L_17 = (((GUILayoutEntry_t1385 *)L_16)->___minHeight_2);
		V_2 = L_17;
		GUILayoutGroup_t1383 * L_18 = ___i;
		NullCheck(L_18);
		float L_19 = (((GUILayoutEntry_t1385 *)L_18)->___maxHeight_3);
		V_3 = L_19;
		GUILayoutGroup_t1383 * L_20 = ___i;
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_20);
		GUILayoutGroup_t1383 * L_21 = ___i;
		GUILayoutGroup_t1383 * L_22 = ___i;
		NullCheck(L_22);
		Rect_t30 * L_23 = &(((GUILayoutEntry_t1385 *)L_22)->___rect_4);
		float L_24 = Rect_get_y_m368(L_23, /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_25 = ___i;
		NullCheck(L_25);
		float L_26 = (((GUILayoutEntry_t1385 *)L_25)->___maxHeight_3);
		float L_27 = V_2;
		float L_28 = V_3;
		float L_29 = Mathf_Clamp_m375(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_21, L_24, L_29);
		goto IL_00e8;
	}

IL_0074:
	{
		GUILayoutGroup_t1383 * L_30 = ___i;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_30);
		GUILayoutGroup_t1383 * L_31 = ___i;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___windowID_16);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_33 = GUILayoutUtility_Internal_GetWindowRect_m7556(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		GUILayoutGroup_t1383 * L_34 = ___i;
		float L_35 = Rect_get_x_m366((&V_4), /*hidden argument*/NULL);
		float L_36 = Rect_get_width_m370((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_37 = ___i;
		NullCheck(L_37);
		float L_38 = (((GUILayoutEntry_t1385 *)L_37)->___minWidth_0);
		GUILayoutGroup_t1383 * L_39 = ___i;
		NullCheck(L_39);
		float L_40 = (((GUILayoutEntry_t1385 *)L_39)->___maxWidth_1);
		float L_41 = Mathf_Clamp_m375(NULL /*static, unused*/, L_36, L_38, L_40, /*hidden argument*/NULL);
		NullCheck(L_34);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_34, L_35, L_41);
		GUILayoutGroup_t1383 * L_42 = ___i;
		NullCheck(L_42);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_42);
		GUILayoutGroup_t1383 * L_43 = ___i;
		float L_44 = Rect_get_y_m368((&V_4), /*hidden argument*/NULL);
		float L_45 = Rect_get_height_m372((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t1383 * L_46 = ___i;
		NullCheck(L_46);
		float L_47 = (((GUILayoutEntry_t1385 *)L_46)->___minHeight_2);
		GUILayoutGroup_t1383 * L_48 = ___i;
		NullCheck(L_48);
		float L_49 = (((GUILayoutEntry_t1385 *)L_48)->___maxHeight_3);
		float L_50 = Mathf_Clamp_m375(NULL /*static, unused*/, L_45, L_47, L_49, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_43, L_44, L_50);
		GUILayoutGroup_t1383 * L_51 = ___i;
		NullCheck(L_51);
		int32_t L_52 = (L_51->___windowID_16);
		GUILayoutGroup_t1383 * L_53 = ___i;
		NullCheck(L_53);
		Rect_t30  L_54 = (((GUILayoutEntry_t1385 *)L_53)->___rect_4);
		GUILayoutUtility_Internal_MoveWindow_m7557(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		return;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)
extern "C" Rect_t30  GUILayoutUtility_Internal_GetWindowRect_m7556 (Object_t * __this /* static, unused */, int32_t ___windowID, const MethodInfo* method)
{
	typedef Rect_t30  (*GUILayoutUtility_Internal_GetWindowRect_m7556_ftn) (int32_t);
	static GUILayoutUtility_Internal_GetWindowRect_m7556_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_Internal_GetWindowRect_m7556_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)");
	return _il2cpp_icall_func(___windowID);
}
// System.Void UnityEngine.GUILayoutUtility::Internal_MoveWindow(System.Int32,UnityEngine.Rect)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Internal_MoveWindow_m7557 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t30  ___r, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m7558(NULL /*static, unused*/, L_0, (&___r), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m7558 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t30 * ___r, const MethodInfo* method)
{
	typedef void (*GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m7558_ftn) (int32_t, Rect_t30 *);
	static GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m7558_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m7558_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)");
	_il2cpp_icall_func(___windowID, ___r);
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::CreateGUILayoutGroupInstanceOfType(System.Type)
extern const Il2CppType* GUILayoutGroup_t1383_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1178;
extern "C" GUILayoutGroup_t1383 * GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m7559 (Object_t * __this /* static, unused */, Type_t * ___LayoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t1383_0_0_0_var = il2cpp_codegen_type_from_index(954);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		_stringLiteral1178 = il2cpp_codegen_string_literal_from_index(1178);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t1383_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = ___LayoutType;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t513 * L_3 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_3, _stringLiteral1178, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		Type_t * L_4 = ___LayoutType;
		Object_t * L_5 = Activator_CreateInstance_m8130(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return ((GUILayoutGroup_t1383 *)CastclassClass(L_5, GUILayoutGroup_t1383_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::BeginLayoutGroup(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[],System.Type)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t1402_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1179;
extern "C" GUILayoutGroup_t1383 * GUILayoutUtility_BeginLayoutGroup_m7560 (Object_t * __this /* static, unused */, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, Type_t * ___LayoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		EventType_t1402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(959);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		_stringLiteral1179 = il2cpp_codegen_string_literal_from_index(1179);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t1383 * V_0 = {0};
	int32_t V_1 = {0};
	{
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_004f;
	}

IL_001f:
	{
		Type_t * L_4 = ___LayoutType;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutGroup_t1383 * L_5 = GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m7559(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GUILayoutGroup_t1383 * L_6 = V_0;
		GUIStyle_t184 * L_7 = ___style;
		NullCheck(L_6);
		GUILayoutEntry_set_style_m7572(L_6, L_7, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_8 = ___options;
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		GUILayoutGroup_t1383 * L_9 = V_0;
		GUILayoutOptionU5BU5D_t550* L_10 = ___options;
		NullCheck(L_9);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t550* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_9, L_10);
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_11 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GUILayoutGroup_t1383 * L_12 = (L_11->___topLevel_0);
		GUILayoutGroup_t1383 * L_13 = V_0;
		NullCheck(L_12);
		GUILayoutGroup_Add_m7587(L_12, L_13, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_14 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_14);
		GUILayoutGroup_t1383 * L_15 = (L_14->___topLevel_0);
		NullCheck(L_15);
		GUILayoutEntry_t1385 * L_16 = GUILayoutGroup_GetNext_m7586(L_15, /*hidden argument*/NULL);
		V_0 = ((GUILayoutGroup_t1383 *)IsInstClass(L_16, GUILayoutGroup_t1383_il2cpp_TypeInfo_var));
		GUILayoutGroup_t1383 * L_17 = V_0;
		if (L_17)
		{
			goto IL_0089;
		}
	}
	{
		Event_t725 * L_18 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = Event_get_type_m7773(L_18, /*hidden argument*/NULL);
		int32_t L_20 = L_19;
		Object_t * L_21 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m2203(NULL /*static, unused*/, _stringLiteral1179, L_21, /*hidden argument*/NULL);
		ArgumentException_t513 * L_23 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_23, L_22, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_23);
	}

IL_0089:
	{
		GUILayoutGroup_t1383 * L_24 = V_0;
		NullCheck(L_24);
		GUILayoutGroup_ResetCursor_m7585(L_24, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_25 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GenericStack_t1309 * L_26 = (L_25->___layoutGroups_1);
		GUILayoutGroup_t1383 * L_27 = V_0;
		NullCheck(L_26);
		VirtActionInvoker1< Object_t * >::Invoke(19 /* System.Void System.Collections.Stack::Push(System.Object) */, L_26, L_27);
		LayoutCache_t1382 * L_28 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		GUILayoutGroup_t1383 * L_29 = V_0;
		NullCheck(L_28);
		L_28->___topLevel_0 = L_29;
		GUILayoutGroup_t1383 * L_30 = V_0;
		return L_30;
	}
}
// System.Void UnityEngine.GUILayoutUtility::EndLayoutGroup()
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_EndLayoutGroup_m7561 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_2 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GenericStack_t1309 * L_3 = (L_2->___layoutGroups_1);
		NullCheck(L_3);
		VirtFuncInvoker0< Object_t * >::Invoke(18 /* System.Object System.Collections.Stack::Pop() */, L_3);
		LayoutCache_t1382 * L_4 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t1382 * L_5 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GenericStack_t1309 * L_6 = (L_5->___layoutGroups_1);
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(17 /* System.Object System.Collections.Stack::Peek() */, L_6);
		NullCheck(L_4);
		L_4->___topLevel_0 = ((GUILayoutGroup_t1383 *)CastclassClass(L_7, GUILayoutGroup_t1383_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::BeginLayoutArea(UnityEngine.GUIStyle,System.Type)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t1383_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t1402_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1179;
extern "C" GUILayoutGroup_t1383 * GUILayoutUtility_BeginLayoutArea_m7562 (Object_t * __this /* static, unused */, GUIStyle_t184 * ___style, Type_t * ___LayoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUILayoutGroup_t1383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		EventType_t1402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(959);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		_stringLiteral1179 = il2cpp_codegen_string_literal_from_index(1179);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t1383 * V_0 = {0};
	int32_t V_1 = {0};
	{
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0042;
	}

IL_001f:
	{
		Type_t * L_4 = ___LayoutType;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutGroup_t1383 * L_5 = GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m7559(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GUILayoutGroup_t1383 * L_6 = V_0;
		GUIStyle_t184 * L_7 = ___style;
		NullCheck(L_6);
		GUILayoutEntry_set_style_m7572(L_6, L_7, /*hidden argument*/NULL);
		LayoutCache_t1382 * L_8 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_8);
		GUILayoutGroup_t1383 * L_9 = (L_8->___windows_2);
		GUILayoutGroup_t1383 * L_10 = V_0;
		NullCheck(L_9);
		GUILayoutGroup_Add_m7587(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_11 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GUILayoutGroup_t1383 * L_12 = (L_11->___windows_2);
		NullCheck(L_12);
		GUILayoutEntry_t1385 * L_13 = GUILayoutGroup_GetNext_m7586(L_12, /*hidden argument*/NULL);
		V_0 = ((GUILayoutGroup_t1383 *)IsInstClass(L_13, GUILayoutGroup_t1383_il2cpp_TypeInfo_var));
		GUILayoutGroup_t1383 * L_14 = V_0;
		if (L_14)
		{
			goto IL_007c;
		}
	}
	{
		Event_t725 * L_15 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = Event_get_type_m7773(L_15, /*hidden argument*/NULL);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2203(NULL /*static, unused*/, _stringLiteral1179, L_18, /*hidden argument*/NULL);
		ArgumentException_t513 * L_20 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_20, L_19, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_007c:
	{
		GUILayoutGroup_t1383 * L_21 = V_0;
		NullCheck(L_21);
		GUILayoutGroup_ResetCursor_m7585(L_21, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_22 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_22);
		GenericStack_t1309 * L_23 = (L_22->___layoutGroups_1);
		GUILayoutGroup_t1383 * L_24 = V_0;
		NullCheck(L_23);
		VirtActionInvoker1< Object_t * >::Invoke(19 /* System.Void System.Collections.Stack::Push(System.Object) */, L_23, L_24);
		LayoutCache_t1382 * L_25 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		GUILayoutGroup_t1383 * L_26 = V_0;
		NullCheck(L_25);
		L_25->___topLevel_0 = L_26;
		GUILayoutGroup_t1383 * L_27 = V_0;
		return L_27;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUILayoutUtility_GetRect_m2483 (Object_t * __this /* static, unused */, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t549 * L_0 = ___content;
		GUIStyle_t184 * L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_2 = GUILayoutUtility_DoGetRect_m7564(NULL /*static, unused*/, L_0, L_1, (GUILayoutOptionU5BU5D_t550*)(GUILayoutOptionU5BU5D_t550*)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUILayoutUtility_GetRect_m7563 (Object_t * __this /* static, unused */, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t549 * L_0 = ___content;
		GUIStyle_t184 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t550* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_3 = GUILayoutUtility_DoGetRect_m7564(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::DoGetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUIWordWrapSizer_t1388_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t1385_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUILayoutUtility_DoGetRect_m7564 (Object_t * __this /* static, unused */, GUIContent_t549 * ___content, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUIWordWrapSizer_t1388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(960);
		GUILayoutEntry_t1385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(957);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t29  V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m7618(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0091;
	}

IL_0024:
	{
		GUIStyle_t184 * L_4 = ___style;
		NullCheck(L_4);
		bool L_5 = GUIStyle_get_isHeightDependantOnWidth_m7756(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_6 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t1383 * L_7 = (L_6->___topLevel_0);
		GUIStyle_t184 * L_8 = ___style;
		GUIContent_t549 * L_9 = ___content;
		GUILayoutOptionU5BU5D_t550* L_10 = ___options;
		GUIWordWrapSizer_t1388 * L_11 = (GUIWordWrapSizer_t1388 *)il2cpp_codegen_object_new (GUIWordWrapSizer_t1388_il2cpp_TypeInfo_var);
		GUIWordWrapSizer__ctor_m7598(L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUILayoutGroup_Add_m7587(L_7, L_11, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_004b:
	{
		GUIStyle_t184 * L_12 = ___style;
		GUIContent_t549 * L_13 = ___content;
		NullCheck(L_12);
		Vector2_t29  L_14 = GUIStyle_CalcSize_m7752(L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_15 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t1383 * L_16 = (L_15->___topLevel_0);
		float L_17 = ((&V_0)->___x_1);
		float L_18 = ((&V_0)->___x_1);
		float L_19 = ((&V_0)->___y_2);
		float L_20 = ((&V_0)->___y_2);
		GUIStyle_t184 * L_21 = ___style;
		GUILayoutOptionU5BU5D_t550* L_22 = ___options;
		GUILayoutEntry_t1385 * L_23 = (GUILayoutEntry_t1385 *)il2cpp_codegen_object_new (GUILayoutEntry_t1385_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m7569(L_23, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUILayoutGroup_Add_m7587(L_16, L_23, /*hidden argument*/NULL);
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_24 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_24;
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_25 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_25;
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_26 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_26);
		GUILayoutGroup_t1383 * L_27 = (L_26->___topLevel_0);
		NullCheck(L_27);
		GUILayoutEntry_t1385 * L_28 = GUILayoutGroup_GetNext_m7586(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Rect_t30  L_29 = (L_28->___rect_4);
		return L_29;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUILayoutUtility_GetRect_m7565 (Object_t * __this /* static, unused */, float ___width, float ___height, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___width;
		float L_1 = ___width;
		float L_2 = ___height;
		float L_3 = ___height;
		GUIStyle_t184 * L_4 = ___style;
		GUILayoutOptionU5BU5D_t550* L_5 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_6 = GUILayoutUtility_DoGetRect_m7566(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::DoGetRect(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t1385_il2cpp_TypeInfo_var;
extern "C" Rect_t30  GUILayoutUtility_DoGetRect_m7566 (Object_t * __this /* static, unused */, float ___minWidth, float ___maxWidth, float ___minHeight, float ___maxHeight, GUIStyle_t184 * ___style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUILayoutEntry_t1385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(957);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0047;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_4 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_4);
		GUILayoutGroup_t1383 * L_5 = (L_4->___topLevel_0);
		float L_6 = ___minWidth;
		float L_7 = ___maxWidth;
		float L_8 = ___minHeight;
		float L_9 = ___maxHeight;
		GUIStyle_t184 * L_10 = ___style;
		GUILayoutOptionU5BU5D_t550* L_11 = ___options;
		GUILayoutEntry_t1385 * L_12 = (GUILayoutEntry_t1385 *)il2cpp_codegen_object_new (GUILayoutEntry_t1385_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m7569(L_12, L_6, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUILayoutGroup_Add_m7587(L_5, L_12, /*hidden argument*/NULL);
		Rect_t30  L_13 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_13;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		Rect_t30  L_14 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_14;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		LayoutCache_t1382 * L_15 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t1383 * L_16 = (L_15->___topLevel_0);
		NullCheck(L_16);
		GUILayoutEntry_t1385 * L_17 = GUILayoutGroup_GetNext_m7586(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Rect_t30  L_18 = (L_17->___rect_4);
		return L_18;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutUtility::get_spaceStyle()
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t184 * GUILayoutUtility_get_spaceStyle_m7567 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_0 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t184 * L_1 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_2 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		NullCheck(L_2);
		GUIStyle_set_stretchWidth_m7737(L_2, 0, /*hidden argument*/NULL);
		GUIStyle_t184 * L_3 = ((GUILayoutUtility_t551_StaticFields*)GUILayoutUtility_t551_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		return L_3;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m7568 (GUILayoutEntry_t1385 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t184 * ____style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = {0};
		Rect__ctor_m387(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_1 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t184 * L_6 = ____style;
		if (L_6)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_7 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		____style = L_7;
	}

IL_005b:
	{
		GUIStyle_t184 * L_8 = ____style;
		GUILayoutEntry_set_style_m7572(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m7569 (GUILayoutEntry_t1385 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t184 * ____style, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = {0};
		Rect__ctor_m387(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_1 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t184 * L_6 = ____style;
		GUILayoutEntry_set_style_m7572(__this, L_6, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_7 = ___options;
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t550* >::Invoke(10 /* System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[]) */, __this, L_7);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.cctor()
extern TypeInfo* GUILayoutEntry_t1385_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__cctor_m7570 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t1385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(957);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t30  L_0 = {0};
		Rect__ctor_m387(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutEntry_t1385_StaticFields*)GUILayoutEntry_t1385_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_8 = L_0;
		((GUILayoutEntry_t1385_StaticFields*)GUILayoutEntry_t1385_il2cpp_TypeInfo_var->static_fields)->___indent_9 = 0;
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutEntry::get_style()
extern "C" GUIStyle_t184 * GUILayoutEntry_get_style_m7571 (GUILayoutEntry_t1385 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_Style_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutEntry::set_style(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_set_style_m7572 (GUILayoutEntry_t1385 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_Style_7 = L_0;
		GUIStyle_t184 * L_1 = ___value;
		VirtActionInvoker1< GUIStyle_t184 * >::Invoke(9 /* System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle) */, __this, L_1);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin()
extern "C" RectOffset_t780 * GUILayoutEntry_get_margin_m7573 (GUILayoutEntry_t1385 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectOffset_t780 * L_1 = GUIStyle_get_margin_m7727(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcWidth()
extern "C" void GUILayoutEntry_CalcWidth_m7574 (GUILayoutEntry_t1385 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcHeight()
extern "C" void GUILayoutEntry_CalcHeight_m7575 (GUILayoutEntry_t1385 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetHorizontal_m7576 (GUILayoutEntry_t1385 * __this, float ___x, float ___width, const MethodInfo* method)
{
	{
		Rect_t30 * L_0 = &(__this->___rect_4);
		float L_1 = ___x;
		Rect_set_x_m367(L_0, L_1, /*hidden argument*/NULL);
		Rect_t30 * L_2 = &(__this->___rect_4);
		float L_3 = ___width;
		Rect_set_width_m371(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetVertical_m7577 (GUILayoutEntry_t1385 * __this, float ___y, float ___height, const MethodInfo* method)
{
	{
		Rect_t30 * L_0 = &(__this->___rect_4);
		float L_1 = ___y;
		Rect_set_y_m369(L_0, L_1, /*hidden argument*/NULL);
		Rect_t30 * L_2 = &(__this->___rect_4);
		float L_3 = ___height;
		Rect_set_height_m373(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_ApplyStyleSettings_m7578 (GUILayoutEntry_t1385 * __this, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	GUILayoutEntry_t1385 * G_B3_0 = {0};
	GUILayoutEntry_t1385 * G_B1_0 = {0};
	GUILayoutEntry_t1385 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	GUILayoutEntry_t1385 * G_B4_1 = {0};
	GUILayoutEntry_t1385 * G_B7_0 = {0};
	GUILayoutEntry_t1385 * G_B5_0 = {0};
	GUILayoutEntry_t1385 * G_B6_0 = {0};
	int32_t G_B8_0 = 0;
	GUILayoutEntry_t1385 * G_B8_1 = {0};
	{
		GUIStyle_t184 * L_0 = ___style;
		NullCheck(L_0);
		float L_1 = GUIStyle_get_fixedWidth_m7734(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			G_B3_0 = __this;
			goto IL_0022;
		}
	}
	{
		GUIStyle_t184 * L_2 = ___style;
		NullCheck(L_2);
		bool L_3 = GUIStyle_get_stretchWidth_m7736(L_2, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if (!L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0022;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0023:
	{
		NullCheck(G_B4_1);
		G_B4_1->___stretchWidth_5 = G_B4_0;
		GUIStyle_t184 * L_4 = ___style;
		NullCheck(L_4);
		float L_5 = GUIStyle_get_fixedHeight_m7735(L_4, /*hidden argument*/NULL);
		G_B5_0 = __this;
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			G_B7_0 = __this;
			goto IL_004a;
		}
	}
	{
		GUIStyle_t184 * L_6 = ___style;
		NullCheck(L_6);
		bool L_7 = GUIStyle_get_stretchHeight_m7738(L_6, /*hidden argument*/NULL);
		G_B6_0 = G_B5_0;
		if (!L_7)
		{
			G_B7_0 = G_B5_0;
			goto IL_004a;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
	}

IL_004b:
	{
		NullCheck(G_B8_1);
		G_B8_1->___stretchHeight_6 = G_B8_0;
		GUIStyle_t184 * L_8 = ___style;
		__this->___m_Style_7 = L_8;
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry_ApplyOptions_m7579 (GUILayoutEntry_t1385 * __this, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t553 * V_0 = {0};
	GUILayoutOptionU5BU5D_t550* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	float V_4 = 0.0f;
	{
		GUILayoutOptionU5BU5D_t550* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t550* L_1 = ___options;
		V_1 = L_1;
		V_2 = 0;
		goto IL_01a0;
	}

IL_0010:
	{
		GUILayoutOptionU5BU5D_t550* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(GUILayoutOption_t553 **)(GUILayoutOption_t553 **)SZArrayLdElema(L_2, L_4, sizeof(GUILayoutOption_t553 *)));
		GUILayoutOption_t553 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___type_0);
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0046;
		}
		if (L_7 == 1)
		{
			goto IL_006e;
		}
		if (L_7 == 2)
		{
			goto IL_0096;
		}
		if (L_7 == 3)
		{
			goto IL_00c9;
		}
		if (L_7 == 4)
		{
			goto IL_0103;
		}
		if (L_7 == 5)
		{
			goto IL_0136;
		}
		if (L_7 == 6)
		{
			goto IL_0170;
		}
		if (L_7 == 7)
		{
			goto IL_0186;
		}
	}
	{
		goto IL_019c;
	}

IL_0046:
	{
		GUILayoutOption_t553 * L_8 = V_0;
		NullCheck(L_8);
		Object_t * L_9 = (L_8->___value_1);
		float L_10 = ((*(float*)((float*)UnBox (L_9, Single_t36_il2cpp_TypeInfo_var))));
		V_4 = L_10;
		__this->___maxWidth_1 = L_10;
		float L_11 = V_4;
		__this->___minWidth_0 = L_11;
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_006e:
	{
		GUILayoutOption_t553 * L_12 = V_0;
		NullCheck(L_12);
		Object_t * L_13 = (L_12->___value_1);
		float L_14 = ((*(float*)((float*)UnBox (L_13, Single_t36_il2cpp_TypeInfo_var))));
		V_4 = L_14;
		__this->___maxHeight_3 = L_14;
		float L_15 = V_4;
		__this->___minHeight_2 = L_15;
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0096:
	{
		GUILayoutOption_t553 * L_16 = V_0;
		NullCheck(L_16);
		Object_t * L_17 = (L_16->___value_1);
		__this->___minWidth_0 = ((*(float*)((float*)UnBox (L_17, Single_t36_il2cpp_TypeInfo_var))));
		float L_18 = (__this->___maxWidth_1);
		float L_19 = (__this->___minWidth_0);
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_00c4;
		}
	}
	{
		float L_20 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_20;
	}

IL_00c4:
	{
		goto IL_019c;
	}

IL_00c9:
	{
		GUILayoutOption_t553 * L_21 = V_0;
		NullCheck(L_21);
		Object_t * L_22 = (L_21->___value_1);
		__this->___maxWidth_1 = ((*(float*)((float*)UnBox (L_22, Single_t36_il2cpp_TypeInfo_var))));
		float L_23 = (__this->___minWidth_0);
		float L_24 = (__this->___maxWidth_1);
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_00f7;
		}
	}
	{
		float L_25 = (__this->___maxWidth_1);
		__this->___minWidth_0 = L_25;
	}

IL_00f7:
	{
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_0103:
	{
		GUILayoutOption_t553 * L_26 = V_0;
		NullCheck(L_26);
		Object_t * L_27 = (L_26->___value_1);
		__this->___minHeight_2 = ((*(float*)((float*)UnBox (L_27, Single_t36_il2cpp_TypeInfo_var))));
		float L_28 = (__this->___maxHeight_3);
		float L_29 = (__this->___minHeight_2);
		if ((!(((float)L_28) < ((float)L_29))))
		{
			goto IL_0131;
		}
	}
	{
		float L_30 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_30;
	}

IL_0131:
	{
		goto IL_019c;
	}

IL_0136:
	{
		GUILayoutOption_t553 * L_31 = V_0;
		NullCheck(L_31);
		Object_t * L_32 = (L_31->___value_1);
		__this->___maxHeight_3 = ((*(float*)((float*)UnBox (L_32, Single_t36_il2cpp_TypeInfo_var))));
		float L_33 = (__this->___minHeight_2);
		float L_34 = (__this->___maxHeight_3);
		if ((!(((float)L_33) > ((float)L_34))))
		{
			goto IL_0164;
		}
	}
	{
		float L_35 = (__this->___maxHeight_3);
		__this->___minHeight_2 = L_35;
	}

IL_0164:
	{
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0170:
	{
		GUILayoutOption_t553 * L_36 = V_0;
		NullCheck(L_36);
		Object_t * L_37 = (L_36->___value_1);
		__this->___stretchWidth_5 = ((*(int32_t*)((int32_t*)UnBox (L_37, Int32_t59_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_0186:
	{
		GUILayoutOption_t553 * L_38 = V_0;
		NullCheck(L_38);
		Object_t * L_39 = (L_38->___value_1);
		__this->___stretchHeight_6 = ((*(int32_t*)((int32_t*)UnBox (L_39, Int32_t59_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_019c:
	{
		int32_t L_40 = V_2;
		V_2 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_01a0:
	{
		int32_t L_41 = V_2;
		GUILayoutOptionU5BU5D_t550* L_42 = V_1;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		float L_43 = (__this->___maxWidth_1);
		if ((((float)L_43) == ((float)(0.0f))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_44 = (__this->___maxWidth_1);
		float L_45 = (__this->___minWidth_0);
		if ((!(((float)L_44) < ((float)L_45))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_46 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_46;
	}

IL_01d6:
	{
		float L_47 = (__this->___maxHeight_3);
		if ((((float)L_47) == ((float)(0.0f))))
		{
			goto IL_0203;
		}
	}
	{
		float L_48 = (__this->___maxHeight_3);
		float L_49 = (__this->___minHeight_2);
		if ((!(((float)L_48) < ((float)L_49))))
		{
			goto IL_0203;
		}
	}
	{
		float L_50 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_50;
	}

IL_0203:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutEntry::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t1385_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral937;
extern Il2CppCodeGenString* _stringLiteral1180;
extern Il2CppCodeGenString* _stringLiteral1181;
extern Il2CppCodeGenString* _stringLiteral1182;
extern Il2CppCodeGenString* _stringLiteral146;
extern Il2CppCodeGenString* _stringLiteral1183;
extern Il2CppCodeGenString* _stringLiteral1184;
extern "C" String_t* GUILayoutEntry_ToString_m7580 (GUILayoutEntry_t1385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		GUILayoutEntry_t1385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(957);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral937 = il2cpp_codegen_string_literal_from_index(937);
		_stringLiteral1180 = il2cpp_codegen_string_literal_from_index(1180);
		_stringLiteral1181 = il2cpp_codegen_string_literal_from_index(1181);
		_stringLiteral1182 = il2cpp_codegen_string_literal_from_index(1182);
		_stringLiteral146 = il2cpp_codegen_string_literal_from_index(146);
		_stringLiteral1183 = il2cpp_codegen_string_literal_from_index(1183);
		_stringLiteral1184 = il2cpp_codegen_string_literal_from_index(1184);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t34* G_B5_1 = {0};
	ObjectU5BU5D_t34* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B5_4 = 0;
	ObjectU5BU5D_t34* G_B5_5 = {0};
	ObjectU5BU5D_t34* G_B5_6 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t34* G_B4_1 = {0};
	ObjectU5BU5D_t34* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	int32_t G_B4_4 = 0;
	ObjectU5BU5D_t34* G_B4_5 = {0};
	ObjectU5BU5D_t34* G_B4_6 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t34* G_B6_2 = {0};
	ObjectU5BU5D_t34* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	int32_t G_B6_5 = 0;
	ObjectU5BU5D_t34* G_B6_6 = {0};
	ObjectU5BU5D_t34* G_B6_7 = {0};
	int32_t G_B8_0 = 0;
	ObjectU5BU5D_t34* G_B8_1 = {0};
	ObjectU5BU5D_t34* G_B8_2 = {0};
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t34* G_B7_1 = {0};
	ObjectU5BU5D_t34* G_B7_2 = {0};
	String_t* G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	ObjectU5BU5D_t34* G_B9_2 = {0};
	ObjectU5BU5D_t34* G_B9_3 = {0};
	int32_t G_B11_0 = 0;
	ObjectU5BU5D_t34* G_B11_1 = {0};
	ObjectU5BU5D_t34* G_B11_2 = {0};
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t34* G_B10_1 = {0};
	ObjectU5BU5D_t34* G_B10_2 = {0};
	String_t* G_B12_0 = {0};
	int32_t G_B12_1 = 0;
	ObjectU5BU5D_t34* G_B12_2 = {0};
	ObjectU5BU5D_t34* G_B12_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		goto IL_001d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m409(NULL /*static, unused*/, L_1, _stringLiteral937, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t1385_il2cpp_TypeInfo_var);
		int32_t L_5 = ((GUILayoutEntry_t1385_StaticFields*)GUILayoutEntry_t1385_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_000d;
		}
	}
	{
		ObjectU5BU5D_t34* L_6 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, ((int32_t)12)));
		String_t* L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 0, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_6;
		ObjectU5BU5D_t34* L_9 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 6));
		GUIStyle_t184 * L_10 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		G_B4_0 = 0;
		G_B4_1 = L_9;
		G_B4_2 = L_9;
		G_B4_3 = _stringLiteral1180;
		G_B4_4 = 1;
		G_B4_5 = L_8;
		G_B4_6 = L_8;
		if (!L_10)
		{
			G_B5_0 = 0;
			G_B5_1 = L_9;
			G_B5_2 = L_9;
			G_B5_3 = _stringLiteral1180;
			G_B5_4 = 1;
			G_B5_5 = L_8;
			G_B5_6 = L_8;
			goto IL_005d;
		}
	}
	{
		GUIStyle_t184 * L_11 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = GUIStyle_get_name_m7716(L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		G_B6_5 = G_B4_4;
		G_B6_6 = G_B4_5;
		G_B6_7 = G_B4_6;
		goto IL_0062;
	}

IL_005d:
	{
		G_B6_0 = _stringLiteral1181;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
		G_B6_5 = G_B5_4;
		G_B6_6 = G_B5_5;
		G_B6_7 = G_B5_6;
	}

IL_0062:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t34* L_13 = G_B6_3;
		Type_t * L_14 = Object_GetType_m303(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 1, sizeof(Object_t *))) = (Object_t *)L_14;
		ObjectU5BU5D_t34* L_15 = L_13;
		Rect_t30 * L_16 = &(__this->___rect_4);
		float L_17 = Rect_get_x_m366(L_16, /*hidden argument*/NULL);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t36_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t34* L_20 = L_15;
		Rect_t30 * L_21 = &(__this->___rect_4);
		float L_22 = Rect_get_xMax_m4208(L_21, /*hidden argument*/NULL);
		float L_23 = L_22;
		Object_t * L_24 = Box(Single_t36_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 3, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t34* L_25 = L_20;
		Rect_t30 * L_26 = &(__this->___rect_4);
		float L_27 = Rect_get_y_m368(L_26, /*hidden argument*/NULL);
		float L_28 = L_27;
		Object_t * L_29 = Box(Single_t36_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t34* L_30 = L_25;
		Rect_t30 * L_31 = &(__this->___rect_4);
		float L_32 = Rect_get_yMax_m4209(L_31, /*hidden argument*/NULL);
		float L_33 = L_32;
		Object_t * L_34 = Box(Single_t36_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 5);
		ArrayElementTypeCheck (L_30, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 5, sizeof(Object_t *))) = (Object_t *)L_34;
		String_t* L_35 = UnityString_Format_m7985(NULL /*static, unused*/, G_B6_4, L_30, /*hidden argument*/NULL);
		NullCheck(G_B6_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_6, G_B6_5);
		ArrayElementTypeCheck (G_B6_6, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_6, G_B6_5, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t34* L_36 = G_B6_7;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 2);
		ArrayElementTypeCheck (L_36, _stringLiteral1182);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral1182;
		ObjectU5BU5D_t34* L_37 = L_36;
		float L_38 = (__this->___minWidth_0);
		float L_39 = L_38;
		Object_t * L_40 = Box(Single_t36_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 3);
		ArrayElementTypeCheck (L_37, L_40);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_37, 3, sizeof(Object_t *))) = (Object_t *)L_40;
		ObjectU5BU5D_t34* L_41 = L_37;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 4);
		ArrayElementTypeCheck (L_41, _stringLiteral146);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral146;
		ObjectU5BU5D_t34* L_42 = L_41;
		float L_43 = (__this->___maxWidth_1);
		float L_44 = L_43;
		Object_t * L_45 = Box(Single_t36_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 5);
		ArrayElementTypeCheck (L_42, L_45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 5, sizeof(Object_t *))) = (Object_t *)L_45;
		ObjectU5BU5D_t34* L_46 = L_42;
		int32_t L_47 = (__this->___stretchWidth_5);
		G_B7_0 = 6;
		G_B7_1 = L_46;
		G_B7_2 = L_46;
		if (!L_47)
		{
			G_B8_0 = 6;
			G_B8_1 = L_46;
			G_B8_2 = L_46;
			goto IL_0101;
		}
	}
	{
		G_B9_0 = _stringLiteral1183;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_0106;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B9_0 = L_48;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_0106:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B9_2, G_B9_1, sizeof(Object_t *))) = (Object_t *)G_B9_0;
		ObjectU5BU5D_t34* L_49 = G_B9_3;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 7);
		ArrayElementTypeCheck (L_49, _stringLiteral1184);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_49, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral1184;
		ObjectU5BU5D_t34* L_50 = L_49;
		float L_51 = (__this->___minHeight_2);
		float L_52 = L_51;
		Object_t * L_53 = Box(Single_t36_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 8);
		ArrayElementTypeCheck (L_50, L_53);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_50, 8, sizeof(Object_t *))) = (Object_t *)L_53;
		ObjectU5BU5D_t34* L_54 = L_50;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)9));
		ArrayElementTypeCheck (L_54, _stringLiteral146);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_54, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)_stringLiteral146;
		ObjectU5BU5D_t34* L_55 = L_54;
		float L_56 = (__this->___maxHeight_3);
		float L_57 = L_56;
		Object_t * L_58 = Box(Single_t36_il2cpp_TypeInfo_var, &L_57);
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)10));
		ArrayElementTypeCheck (L_55, L_58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_58;
		ObjectU5BU5D_t34* L_59 = L_55;
		int32_t L_60 = (__this->___stretchHeight_6);
		G_B10_0 = ((int32_t)11);
		G_B10_1 = L_59;
		G_B10_2 = L_59;
		if (!L_60)
		{
			G_B11_0 = ((int32_t)11);
			G_B11_1 = L_59;
			G_B11_2 = L_59;
			goto IL_014d;
		}
	}
	{
		G_B12_0 = _stringLiteral1183;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_0152;
	}

IL_014d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B12_0 = L_61;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_0152:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		ArrayElementTypeCheck (G_B12_2, G_B12_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B12_2, G_B12_1, sizeof(Object_t *))) = (Object_t *)G_B12_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = String_Concat_m2254(NULL /*static, unused*/, G_B12_3, /*hidden argument*/NULL);
		return L_62;
	}
}
// System.Void UnityEngine.GUILayoutGroup::.ctor()
extern TypeInfo* List_1_t1386_il2cpp_TypeInfo_var;
extern TypeInfo* RectOffset_t780_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t1385_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8184_MethodInfo_var;
extern "C" void GUILayoutGroup__ctor_m7581 (GUILayoutGroup_t1383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1386_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		RectOffset_t780_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		GUILayoutEntry_t1385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(957);
		List_1__ctor_m8184_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484724);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1386 * L_0 = (List_1_t1386 *)il2cpp_codegen_object_new (List_1_t1386_il2cpp_TypeInfo_var);
		List_1__ctor_m8184(L_0, /*hidden argument*/List_1__ctor_m8184_MethodInfo_var);
		__this->___entries_10 = L_0;
		__this->___isVertical_11 = 1;
		__this->___sameSize_14 = 1;
		__this->___windowID_16 = (-1);
		__this->___stretchableCountX_18 = ((int32_t)100);
		__this->___stretchableCountY_19 = ((int32_t)100);
		__this->___childMinWidth_22 = (100.0f);
		__this->___childMaxWidth_23 = (100.0f);
		__this->___childMinHeight_24 = (100.0f);
		__this->___childMaxHeight_25 = (100.0f);
		RectOffset_t780 * L_1 = (RectOffset_t780 *)il2cpp_codegen_object_new (RectOffset_t780_il2cpp_TypeInfo_var);
		RectOffset__ctor_m4412(L_1, /*hidden argument*/NULL);
		__this->___m_Margin_26 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_2 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t1385_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m7568(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin()
extern "C" RectOffset_t780 * GUILayoutGroup_get_margin_m7582 (GUILayoutGroup_t1383 * __this, const MethodInfo* method)
{
	{
		RectOffset_t780 * L_0 = (__this->___m_Margin_26);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern "C" void GUILayoutGroup_ApplyOptions_m7583 (GUILayoutGroup_t1383 * __this, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t553 * V_0 = {0};
	GUILayoutOptionU5BU5D_t550* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	{
		GUILayoutOptionU5BU5D_t550* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t550* L_1 = ___options;
		GUILayoutEntry_ApplyOptions_m7579(__this, L_1, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t550* L_2 = ___options;
		V_1 = L_2;
		V_2 = 0;
		goto IL_0098;
	}

IL_0017:
	{
		GUILayoutOptionU5BU5D_t550* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_0 = (*(GUILayoutOption_t553 **)(GUILayoutOption_t553 **)SZArrayLdElema(L_3, L_5, sizeof(GUILayoutOption_t553 *)));
		GUILayoutOption_t553 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___type_0);
		V_3 = L_7;
		int32_t L_8 = V_3;
		if (L_8 == 0)
		{
			goto IL_0065;
		}
		if (L_8 == 1)
		{
			goto IL_0071;
		}
		if (L_8 == 2)
		{
			goto IL_0065;
		}
		if (L_8 == 3)
		{
			goto IL_0065;
		}
		if (L_8 == 4)
		{
			goto IL_0071;
		}
		if (L_8 == 5)
		{
			goto IL_0071;
		}
		if (L_8 == 6)
		{
			goto IL_0094;
		}
		if (L_8 == 7)
		{
			goto IL_0094;
		}
		if (L_8 == 8)
		{
			goto IL_0094;
		}
		if (L_8 == 9)
		{
			goto IL_0094;
		}
		if (L_8 == 10)
		{
			goto IL_0094;
		}
		if (L_8 == 11)
		{
			goto IL_0094;
		}
		if (L_8 == 12)
		{
			goto IL_0094;
		}
		if (L_8 == 13)
		{
			goto IL_007d;
		}
	}
	{
		goto IL_0094;
	}

IL_0065:
	{
		__this->___userSpecifiedHeight_21 = 1;
		goto IL_0094;
	}

IL_0071:
	{
		__this->___userSpecifiedWidth_20 = 1;
		goto IL_0094;
	}

IL_007d:
	{
		GUILayoutOption_t553 * L_9 = V_0;
		NullCheck(L_9);
		Object_t * L_10 = (L_9->___value_1);
		__this->___spacing_13 = (((float)((float)((*(int32_t*)((int32_t*)UnBox (L_10, Int32_t59_il2cpp_TypeInfo_var)))))));
		goto IL_0094;
	}

IL_0094:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_12 = V_2;
		GUILayoutOptionU5BU5D_t550* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutGroup_ApplyStyleSettings_m7584 (GUILayoutGroup_t1383 * __this, GUIStyle_t184 * ___style, const MethodInfo* method)
{
	RectOffset_t780 * V_0 = {0};
	{
		GUIStyle_t184 * L_0 = ___style;
		GUILayoutEntry_ApplyStyleSettings_m7578(__this, L_0, /*hidden argument*/NULL);
		GUIStyle_t184 * L_1 = ___style;
		NullCheck(L_1);
		RectOffset_t780 * L_2 = GUIStyle_get_margin_m7727(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		RectOffset_t780 * L_3 = (__this->___m_Margin_26);
		RectOffset_t780 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_left_m4410(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_set_left_m7701(L_3, L_5, /*hidden argument*/NULL);
		RectOffset_t780 * L_6 = (__this->___m_Margin_26);
		RectOffset_t780 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = RectOffset_get_right_m7702(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectOffset_set_right_m7703(L_6, L_8, /*hidden argument*/NULL);
		RectOffset_t780 * L_9 = (__this->___m_Margin_26);
		RectOffset_t780 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m4411(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		RectOffset_set_top_m7704(L_9, L_11, /*hidden argument*/NULL);
		RectOffset_t780 * L_12 = (__this->___m_Margin_26);
		RectOffset_t780 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_bottom_m7705(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		RectOffset_set_bottom_m7706(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ResetCursor()
extern "C" void GUILayoutGroup_ResetCursor_m7585 (GUILayoutGroup_t1383 * __this, const MethodInfo* method)
{
	{
		__this->___cursor_17 = 0;
		return;
	}
}
// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::GetNext()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t1402_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185;
extern Il2CppCodeGenString* _stringLiteral1186;
extern Il2CppCodeGenString* _stringLiteral1187;
extern Il2CppCodeGenString* _stringLiteral1188;
extern "C" GUILayoutEntry_t1385 * GUILayoutGroup_GetNext_m7586 (GUILayoutGroup_t1383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		EventType_t1402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(959);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		_stringLiteral1185 = il2cpp_codegen_string_literal_from_index(1185);
		_stringLiteral1186 = il2cpp_codegen_string_literal_from_index(1186);
		_stringLiteral1187 = il2cpp_codegen_string_literal_from_index(1187);
		_stringLiteral1188 = il2cpp_codegen_string_literal_from_index(1188);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutEntry_t1385 * V_0 = {0};
	{
		int32_t L_0 = (__this->___cursor_17);
		List_1_t1386 * L_1 = (__this->___entries_10);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_1);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_0038;
		}
	}
	{
		List_1_t1386 * L_3 = (__this->___entries_10);
		int32_t L_4 = (__this->___cursor_17);
		NullCheck(L_3);
		GUILayoutEntry_t1385 * L_5 = (GUILayoutEntry_t1385 *)VirtFuncInvoker1< GUILayoutEntry_t1385 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_3, L_4);
		V_0 = L_5;
		int32_t L_6 = (__this->___cursor_17);
		__this->___cursor_17 = ((int32_t)((int32_t)L_6+(int32_t)1));
		GUILayoutEntry_t1385 * L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		ObjectU5BU5D_t34* L_8 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 7));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, _stringLiteral1185);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral1185;
		ObjectU5BU5D_t34* L_9 = L_8;
		int32_t L_10 = (__this->___cursor_17);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 1, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t34* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, _stringLiteral1186);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral1186;
		ObjectU5BU5D_t34* L_14 = L_13;
		List_1_t1386 * L_15 = (__this->___entries_10);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_15);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 3, sizeof(Object_t *))) = (Object_t *)L_18;
		ObjectU5BU5D_t34* L_19 = L_14;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, _stringLiteral1187);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral1187;
		ObjectU5BU5D_t34* L_20 = L_19;
		Event_t725 * L_21 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Event_get_rawType_m4259(L_21, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t34* L_25 = L_20;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 6);
		ArrayElementTypeCheck (L_25, _stringLiteral1188);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral1188;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m2254(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		ArgumentException_t513 * L_27 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_27, L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_27);
	}
}
// System.Void UnityEngine.GUILayoutGroup::Add(UnityEngine.GUILayoutEntry)
extern "C" void GUILayoutGroup_Add_m7587 (GUILayoutGroup_t1383 * __this, GUILayoutEntry_t1385 * ___e, const MethodInfo* method)
{
	{
		List_1_t1386 * L_0 = (__this->___entries_10);
		GUILayoutEntry_t1385 * L_1 = ___e;
		NullCheck(L_0);
		VirtActionInvoker1< GUILayoutEntry_t1385 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcWidth()
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1470_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8181_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8182_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8183_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcWidth_m7588 (GUILayoutGroup_t1383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		Enumerator_t1470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		List_1_GetEnumerator_m8181_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484721);
		Enumerator_get_Current_m8182_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484722);
		Enumerator_MoveNext_m8183_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484723);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	GUILayoutEntry_t1385 * V_3 = {0};
	Enumerator_t1470  V_4 = {0};
	RectOffset_t780 * V_5 = {0};
	int32_t V_6 = 0;
	GUILayoutEntry_t1385 * V_7 = {0};
	Enumerator_t1470  V_8 = {0};
	RectOffset_t780 * V_9 = {0};
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	int32_t G_B39_0 = 0;
	int32_t G_B39_1 = 0;
	GUILayoutGroup_t1383 * G_B39_2 = {0};
	int32_t G_B38_0 = 0;
	int32_t G_B38_1 = 0;
	GUILayoutGroup_t1383 * G_B38_2 = {0};
	int32_t G_B40_0 = 0;
	int32_t G_B40_1 = 0;
	int32_t G_B40_2 = 0;
	GUILayoutGroup_t1383 * G_B40_3 = {0};
	{
		List_1_t1386 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t184 * L_2 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t780 * L_3 = GUIStyle_get_padding_m7728(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_horizontal_m4405(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)((float)L_4)));
		V_13 = L_5;
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = L_5;
		float L_6 = V_13;
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = L_6;
		return;
	}

IL_0033:
	{
		__this->___childMinWidth_22 = (0.0f);
		__this->___childMaxWidth_23 = (0.0f);
		V_0 = 0;
		V_1 = 0;
		__this->___stretchableCountX_18 = 0;
		V_2 = 1;
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_016a;
		}
	}
	{
		List_1_t1386 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1470  L_9 = List_1_GetEnumerator_m8181(L_8, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_4 = L_9;
	}

IL_006e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0125;
		}

IL_0073:
		{
			GUILayoutEntry_t1385 * L_10 = Enumerator_get_Current_m8182((&V_4), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_3 = L_10;
			GUILayoutEntry_t1385 * L_11 = V_3;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_11);
			GUILayoutEntry_t1385 * L_12 = V_3;
			NullCheck(L_12);
			RectOffset_t780 * L_13 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_12);
			V_5 = L_13;
			GUILayoutEntry_t1385 * L_14 = V_3;
			NullCheck(L_14);
			GUIStyle_t184 * L_15 = GUILayoutEntry_get_style_m7571(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUIStyle_t184 * L_16 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t184 *)L_15) == ((Object_t*)(GUIStyle_t184 *)L_16)))
			{
				goto IL_0112;
			}
		}

IL_0099:
		{
			bool L_17 = V_2;
			if (L_17)
			{
				goto IL_00c0;
			}
		}

IL_009f:
		{
			RectOffset_t780 * L_18 = V_5;
			NullCheck(L_18);
			int32_t L_19 = RectOffset_get_left_m4410(L_18, /*hidden argument*/NULL);
			int32_t L_20 = V_0;
			int32_t L_21 = Mathf_Min_m288(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
			V_0 = L_21;
			RectOffset_t780 * L_22 = V_5;
			NullCheck(L_22);
			int32_t L_23 = RectOffset_get_right_m7702(L_22, /*hidden argument*/NULL);
			int32_t L_24 = V_1;
			int32_t L_25 = Mathf_Min_m288(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
			V_1 = L_25;
			goto IL_00d2;
		}

IL_00c0:
		{
			RectOffset_t780 * L_26 = V_5;
			NullCheck(L_26);
			int32_t L_27 = RectOffset_get_left_m4410(L_26, /*hidden argument*/NULL);
			V_0 = L_27;
			RectOffset_t780 * L_28 = V_5;
			NullCheck(L_28);
			int32_t L_29 = RectOffset_get_right_m7702(L_28, /*hidden argument*/NULL);
			V_1 = L_29;
			V_2 = 0;
		}

IL_00d2:
		{
			GUILayoutEntry_t1385 * L_30 = V_3;
			NullCheck(L_30);
			float L_31 = (L_30->___minWidth_0);
			RectOffset_t780 * L_32 = V_5;
			NullCheck(L_32);
			int32_t L_33 = RectOffset_get_horizontal_m4405(L_32, /*hidden argument*/NULL);
			float L_34 = (__this->___childMinWidth_22);
			float L_35 = Mathf_Max_m4353(NULL /*static, unused*/, ((float)((float)L_31+(float)(((float)((float)L_33))))), L_34, /*hidden argument*/NULL);
			__this->___childMinWidth_22 = L_35;
			GUILayoutEntry_t1385 * L_36 = V_3;
			NullCheck(L_36);
			float L_37 = (L_36->___maxWidth_1);
			RectOffset_t780 * L_38 = V_5;
			NullCheck(L_38);
			int32_t L_39 = RectOffset_get_horizontal_m4405(L_38, /*hidden argument*/NULL);
			float L_40 = (__this->___childMaxWidth_23);
			float L_41 = Mathf_Max_m4353(NULL /*static, unused*/, ((float)((float)L_37+(float)(((float)((float)L_39))))), L_40, /*hidden argument*/NULL);
			__this->___childMaxWidth_23 = L_41;
		}

IL_0112:
		{
			int32_t L_42 = (__this->___stretchableCountX_18);
			GUILayoutEntry_t1385 * L_43 = V_3;
			NullCheck(L_43);
			int32_t L_44 = (L_43->___stretchWidth_5);
			__this->___stretchableCountX_18 = ((int32_t)((int32_t)L_42+(int32_t)L_44));
		}

IL_0125:
		{
			bool L_45 = Enumerator_MoveNext_m8183((&V_4), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_45)
			{
				goto IL_0073;
			}
		}

IL_0131:
		{
			IL2CPP_LEAVE(0x143, FINALLY_0136);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0136;
	}

FINALLY_0136:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_46 = V_4;
		Enumerator_t1470  L_47 = L_46;
		Object_t * L_48 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_48);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_48);
		IL2CPP_END_FINALLY(310)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(310)
	{
		IL2CPP_JUMP_TBL(0x143, IL_0143)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0143:
	{
		float L_49 = (__this->___childMinWidth_22);
		int32_t L_50 = V_0;
		int32_t L_51 = V_1;
		__this->___childMinWidth_22 = ((float)((float)L_49-(float)(((float)((float)((int32_t)((int32_t)L_50+(int32_t)L_51)))))));
		float L_52 = (__this->___childMaxWidth_23);
		int32_t L_53 = V_0;
		int32_t L_54 = V_1;
		__this->___childMaxWidth_23 = ((float)((float)L_52-(float)(((float)((float)((int32_t)((int32_t)L_53+(int32_t)L_54)))))));
		goto IL_02ea;
	}

IL_016a:
	{
		V_6 = 0;
		List_1_t1386 * L_55 = (__this->___entries_10);
		NullCheck(L_55);
		Enumerator_t1470  L_56 = List_1_GetEnumerator_m8181(L_55, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_8 = L_56;
	}

IL_017a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0273;
		}

IL_017f:
		{
			GUILayoutEntry_t1385 * L_57 = Enumerator_get_Current_m8182((&V_8), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_7 = L_57;
			GUILayoutEntry_t1385 * L_58 = V_7;
			NullCheck(L_58);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_58);
			GUILayoutEntry_t1385 * L_59 = V_7;
			NullCheck(L_59);
			RectOffset_t780 * L_60 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_59);
			V_9 = L_60;
			GUILayoutEntry_t1385 * L_61 = V_7;
			NullCheck(L_61);
			GUIStyle_t184 * L_62 = GUILayoutEntry_get_style_m7571(L_61, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUIStyle_t184 * L_63 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t184 *)L_62) == ((Object_t*)(GUIStyle_t184 *)L_63)))
			{
				goto IL_0237;
			}
		}

IL_01a9:
		{
			bool L_64 = V_2;
			if (L_64)
			{
				goto IL_01d2;
			}
		}

IL_01af:
		{
			int32_t L_65 = V_6;
			RectOffset_t780 * L_66 = V_9;
			NullCheck(L_66);
			int32_t L_67 = RectOffset_get_left_m4410(L_66, /*hidden argument*/NULL);
			if ((((int32_t)L_65) <= ((int32_t)L_67)))
			{
				goto IL_01c4;
			}
		}

IL_01bd:
		{
			int32_t L_68 = V_6;
			G_B22_0 = L_68;
			goto IL_01cb;
		}

IL_01c4:
		{
			RectOffset_t780 * L_69 = V_9;
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_left_m4410(L_69, /*hidden argument*/NULL);
			G_B22_0 = L_70;
		}

IL_01cb:
		{
			V_10 = G_B22_0;
			goto IL_01d7;
		}

IL_01d2:
		{
			V_10 = 0;
			V_2 = 0;
		}

IL_01d7:
		{
			float L_71 = (__this->___childMinWidth_22);
			GUILayoutEntry_t1385 * L_72 = V_7;
			NullCheck(L_72);
			float L_73 = (L_72->___minWidth_0);
			float L_74 = (__this->___spacing_13);
			int32_t L_75 = V_10;
			__this->___childMinWidth_22 = ((float)((float)L_71+(float)((float)((float)((float)((float)L_73+(float)L_74))+(float)(((float)((float)L_75)))))));
			float L_76 = (__this->___childMaxWidth_23);
			GUILayoutEntry_t1385 * L_77 = V_7;
			NullCheck(L_77);
			float L_78 = (L_77->___maxWidth_1);
			float L_79 = (__this->___spacing_13);
			int32_t L_80 = V_10;
			__this->___childMaxWidth_23 = ((float)((float)L_76+(float)((float)((float)((float)((float)L_78+(float)L_79))+(float)(((float)((float)L_80)))))));
			RectOffset_t780 * L_81 = V_9;
			NullCheck(L_81);
			int32_t L_82 = RectOffset_get_right_m7702(L_81, /*hidden argument*/NULL);
			V_6 = L_82;
			int32_t L_83 = (__this->___stretchableCountX_18);
			GUILayoutEntry_t1385 * L_84 = V_7;
			NullCheck(L_84);
			int32_t L_85 = (L_84->___stretchWidth_5);
			__this->___stretchableCountX_18 = ((int32_t)((int32_t)L_83+(int32_t)L_85));
			goto IL_0273;
		}

IL_0237:
		{
			float L_86 = (__this->___childMinWidth_22);
			GUILayoutEntry_t1385 * L_87 = V_7;
			NullCheck(L_87);
			float L_88 = (L_87->___minWidth_0);
			__this->___childMinWidth_22 = ((float)((float)L_86+(float)L_88));
			float L_89 = (__this->___childMaxWidth_23);
			GUILayoutEntry_t1385 * L_90 = V_7;
			NullCheck(L_90);
			float L_91 = (L_90->___maxWidth_1);
			__this->___childMaxWidth_23 = ((float)((float)L_89+(float)L_91));
			int32_t L_92 = (__this->___stretchableCountX_18);
			GUILayoutEntry_t1385 * L_93 = V_7;
			NullCheck(L_93);
			int32_t L_94 = (L_93->___stretchWidth_5);
			__this->___stretchableCountX_18 = ((int32_t)((int32_t)L_92+(int32_t)L_94));
		}

IL_0273:
		{
			bool L_95 = Enumerator_MoveNext_m8183((&V_8), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_95)
			{
				goto IL_017f;
			}
		}

IL_027f:
		{
			IL2CPP_LEAVE(0x291, FINALLY_0284);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0284;
	}

FINALLY_0284:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_96 = V_8;
		Enumerator_t1470  L_97 = L_96;
		Object_t * L_98 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_98);
		IL2CPP_END_FINALLY(644)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(644)
	{
		IL2CPP_JUMP_TBL(0x291, IL_0291)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0291:
	{
		float L_99 = (__this->___childMinWidth_22);
		float L_100 = (__this->___spacing_13);
		__this->___childMinWidth_22 = ((float)((float)L_99-(float)L_100));
		float L_101 = (__this->___childMaxWidth_23);
		float L_102 = (__this->___spacing_13);
		__this->___childMaxWidth_23 = ((float)((float)L_101-(float)L_102));
		List_1_t1386 * L_103 = (__this->___entries_10);
		NullCheck(L_103);
		int32_t L_104 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_103);
		if (!L_104)
		{
			goto IL_02e6;
		}
	}
	{
		List_1_t1386 * L_105 = (__this->___entries_10);
		NullCheck(L_105);
		GUILayoutEntry_t1385 * L_106 = (GUILayoutEntry_t1385 *)VirtFuncInvoker1< GUILayoutEntry_t1385 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_105, 0);
		NullCheck(L_106);
		RectOffset_t780 * L_107 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_106);
		NullCheck(L_107);
		int32_t L_108 = RectOffset_get_left_m4410(L_107, /*hidden argument*/NULL);
		V_0 = L_108;
		int32_t L_109 = V_6;
		V_1 = L_109;
		goto IL_02ea;
	}

IL_02e6:
	{
		int32_t L_110 = 0;
		V_1 = L_110;
		V_0 = L_110;
	}

IL_02ea:
	{
		V_11 = (0.0f);
		V_12 = (0.0f);
		GUIStyle_t184 * L_111 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_112 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t184 *)L_111) == ((Object_t*)(GUIStyle_t184 *)L_112))))
		{
			goto IL_0313;
		}
	}
	{
		bool L_113 = (__this->___userSpecifiedWidth_20);
		if (!L_113)
		{
			goto IL_034a;
		}
	}

IL_0313:
	{
		GUIStyle_t184 * L_114 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_114);
		RectOffset_t780 * L_115 = GUIStyle_get_padding_m7728(L_114, /*hidden argument*/NULL);
		NullCheck(L_115);
		int32_t L_116 = RectOffset_get_left_m4410(L_115, /*hidden argument*/NULL);
		int32_t L_117 = V_0;
		int32_t L_118 = Mathf_Max_m4262(NULL /*static, unused*/, L_116, L_117, /*hidden argument*/NULL);
		V_11 = (((float)((float)L_118)));
		GUIStyle_t184 * L_119 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_119);
		RectOffset_t780 * L_120 = GUIStyle_get_padding_m7728(L_119, /*hidden argument*/NULL);
		NullCheck(L_120);
		int32_t L_121 = RectOffset_get_right_m7702(L_120, /*hidden argument*/NULL);
		int32_t L_122 = V_1;
		int32_t L_123 = Mathf_Max_m4262(NULL /*static, unused*/, L_121, L_122, /*hidden argument*/NULL);
		V_12 = (((float)((float)L_123)));
		goto IL_036c;
	}

IL_034a:
	{
		RectOffset_t780 * L_124 = (__this->___m_Margin_26);
		int32_t L_125 = V_0;
		NullCheck(L_124);
		RectOffset_set_left_m7701(L_124, L_125, /*hidden argument*/NULL);
		RectOffset_t780 * L_126 = (__this->___m_Margin_26);
		int32_t L_127 = V_1;
		NullCheck(L_126);
		RectOffset_set_right_m7703(L_126, L_127, /*hidden argument*/NULL);
		float L_128 = (0.0f);
		V_12 = L_128;
		V_11 = L_128;
	}

IL_036c:
	{
		float L_129 = (((GUILayoutEntry_t1385 *)__this)->___minWidth_0);
		float L_130 = (__this->___childMinWidth_22);
		float L_131 = V_11;
		float L_132 = V_12;
		float L_133 = Mathf_Max_m4353(NULL /*static, unused*/, L_129, ((float)((float)((float)((float)L_130+(float)L_131))+(float)L_132)), /*hidden argument*/NULL);
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = L_133;
		float L_134 = (((GUILayoutEntry_t1385 *)__this)->___maxWidth_1);
		if ((!(((float)L_134) == ((float)(0.0f)))))
		{
			goto IL_03db;
		}
	}
	{
		int32_t L_135 = (((GUILayoutEntry_t1385 *)__this)->___stretchWidth_5);
		int32_t L_136 = (__this->___stretchableCountX_18);
		GUIStyle_t184 * L_137 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_137);
		bool L_138 = GUIStyle_get_stretchWidth_m7736(L_137, /*hidden argument*/NULL);
		G_B38_0 = L_136;
		G_B38_1 = L_135;
		G_B38_2 = __this;
		if (!L_138)
		{
			G_B39_0 = L_136;
			G_B39_1 = L_135;
			G_B39_2 = __this;
			goto IL_03bc;
		}
	}
	{
		G_B40_0 = 1;
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		G_B40_3 = G_B38_2;
		goto IL_03bd;
	}

IL_03bc:
	{
		G_B40_0 = 0;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
		G_B40_3 = G_B39_2;
	}

IL_03bd:
	{
		NullCheck(G_B40_3);
		((GUILayoutEntry_t1385 *)G_B40_3)->___stretchWidth_5 = ((int32_t)((int32_t)G_B40_2+(int32_t)((int32_t)((int32_t)G_B40_1+(int32_t)G_B40_0))));
		float L_139 = (__this->___childMaxWidth_23);
		float L_140 = V_11;
		float L_141 = V_12;
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = ((float)((float)((float)((float)L_139+(float)L_140))+(float)L_141));
		goto IL_03e2;
	}

IL_03db:
	{
		((GUILayoutEntry_t1385 *)__this)->___stretchWidth_5 = 0;
	}

IL_03e2:
	{
		float L_142 = (((GUILayoutEntry_t1385 *)__this)->___maxWidth_1);
		float L_143 = (((GUILayoutEntry_t1385 *)__this)->___minWidth_0);
		float L_144 = Mathf_Max_m4353(NULL /*static, unused*/, L_142, L_143, /*hidden argument*/NULL);
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = L_144;
		GUIStyle_t184 * L_145 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_145);
		float L_146 = GUIStyle_get_fixedWidth_m7734(L_145, /*hidden argument*/NULL);
		if ((((float)L_146) == ((float)(0.0f))))
		{
			goto IL_0431;
		}
	}
	{
		GUIStyle_t184 * L_147 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_147);
		float L_148 = GUIStyle_get_fixedWidth_m7734(L_147, /*hidden argument*/NULL);
		float L_149 = L_148;
		V_13 = L_149;
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = L_149;
		float L_150 = V_13;
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = L_150;
		((GUILayoutEntry_t1385 *)__this)->___stretchWidth_5 = 0;
	}

IL_0431:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1470_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8181_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8182_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8183_MethodInfo_var;
extern "C" void GUILayoutGroup_SetHorizontal_m7589 (GUILayoutGroup_t1383 * __this, float ___x, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		Enumerator_t1470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		List_1_GetEnumerator_m8181_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484721);
		Enumerator_get_Current_m8182_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484722);
		Enumerator_MoveNext_m8183_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484723);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t780 * V_0 = {0};
	GUILayoutEntry_t1385 * V_1 = {0};
	Enumerator_t1470  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	GUILayoutEntry_t1385 * V_8 = {0};
	Enumerator_t1470  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	int32_t V_15 = 0;
	bool V_16 = false;
	GUILayoutEntry_t1385 * V_17 = {0};
	Enumerator_t1470  V_18 = {0};
	float V_19 = 0.0f;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B39_0 = 0;
	{
		float L_0 = ___x;
		float L_1 = ___width;
		GUILayoutEntry_SetHorizontal_m7576(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___resetCoords_12);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		___x = (0.0f);
	}

IL_001a:
	{
		GUIStyle_t184 * L_3 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_t780 * L_4 = GUIStyle_get_padding_m7728(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = (__this->___isVertical_11);
		if (!L_5)
		{
			goto IL_01bb;
		}
	}
	{
		GUIStyle_t184 * L_6 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_7 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t184 *)L_6) == ((Object_t*)(GUIStyle_t184 *)L_7)))
		{
			goto IL_00eb;
		}
	}
	{
		List_1_t1386 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1470  L_9 = List_1_GetEnumerator_m8181(L_8, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_2 = L_9;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c9;
		}

IL_0052:
		{
			GUILayoutEntry_t1385 * L_10 = Enumerator_get_Current_m8182((&V_2), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_1 = L_10;
			GUILayoutEntry_t1385 * L_11 = V_1;
			NullCheck(L_11);
			RectOffset_t780 * L_12 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_11);
			NullCheck(L_12);
			int32_t L_13 = RectOffset_get_left_m4410(L_12, /*hidden argument*/NULL);
			RectOffset_t780 * L_14 = V_0;
			NullCheck(L_14);
			int32_t L_15 = RectOffset_get_left_m4410(L_14, /*hidden argument*/NULL);
			int32_t L_16 = Mathf_Max_m4262(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
			V_3 = (((float)((float)L_16)));
			float L_17 = ___x;
			float L_18 = V_3;
			V_4 = ((float)((float)L_17+(float)L_18));
			float L_19 = ___width;
			GUILayoutEntry_t1385 * L_20 = V_1;
			NullCheck(L_20);
			RectOffset_t780 * L_21 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_20);
			NullCheck(L_21);
			int32_t L_22 = RectOffset_get_right_m7702(L_21, /*hidden argument*/NULL);
			RectOffset_t780 * L_23 = V_0;
			NullCheck(L_23);
			int32_t L_24 = RectOffset_get_right_m7702(L_23, /*hidden argument*/NULL);
			int32_t L_25 = Mathf_Max_m4262(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
			float L_26 = V_3;
			V_5 = ((float)((float)((float)((float)L_19-(float)(((float)((float)L_25)))))-(float)L_26));
			GUILayoutEntry_t1385 * L_27 = V_1;
			NullCheck(L_27);
			int32_t L_28 = (L_27->___stretchWidth_5);
			if (!L_28)
			{
				goto IL_00ae;
			}
		}

IL_009f:
		{
			GUILayoutEntry_t1385 * L_29 = V_1;
			float L_30 = V_4;
			float L_31 = V_5;
			NullCheck(L_29);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_29, L_30, L_31);
			goto IL_00c9;
		}

IL_00ae:
		{
			GUILayoutEntry_t1385 * L_32 = V_1;
			float L_33 = V_4;
			float L_34 = V_5;
			GUILayoutEntry_t1385 * L_35 = V_1;
			NullCheck(L_35);
			float L_36 = (L_35->___minWidth_0);
			GUILayoutEntry_t1385 * L_37 = V_1;
			NullCheck(L_37);
			float L_38 = (L_37->___maxWidth_1);
			float L_39 = Mathf_Clamp_m375(NULL /*static, unused*/, L_34, L_36, L_38, /*hidden argument*/NULL);
			NullCheck(L_32);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_32, L_33, L_39);
		}

IL_00c9:
		{
			bool L_40 = Enumerator_MoveNext_m8183((&V_2), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_40)
			{
				goto IL_0052;
			}
		}

IL_00d5:
		{
			IL2CPP_LEAVE(0xE6, FINALLY_00da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_00da;
	}

FINALLY_00da:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_41 = V_2;
		Enumerator_t1470  L_42 = L_41;
		Object_t * L_43 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_43);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_43);
		IL2CPP_END_FINALLY(218)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(218)
	{
		IL2CPP_JUMP_TBL(0xE6, IL_00e6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_00e6:
	{
		goto IL_01b6;
	}

IL_00eb:
	{
		float L_44 = ___x;
		RectOffset_t780 * L_45 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_45);
		int32_t L_46 = RectOffset_get_left_m4410(L_45, /*hidden argument*/NULL);
		V_6 = ((float)((float)L_44-(float)(((float)((float)L_46)))));
		float L_47 = ___width;
		RectOffset_t780 * L_48 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_horizontal_m4405(L_48, /*hidden argument*/NULL);
		V_7 = ((float)((float)L_47+(float)(((float)((float)L_49)))));
		List_1_t1386 * L_50 = (__this->___entries_10);
		NullCheck(L_50);
		Enumerator_t1470  L_51 = List_1_GetEnumerator_m8181(L_50, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_9 = L_51;
	}

IL_0118:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0198;
		}

IL_011d:
		{
			GUILayoutEntry_t1385 * L_52 = Enumerator_get_Current_m8182((&V_9), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_8 = L_52;
			GUILayoutEntry_t1385 * L_53 = V_8;
			NullCheck(L_53);
			int32_t L_54 = (L_53->___stretchWidth_5);
			if (!L_54)
			{
				goto IL_015e;
			}
		}

IL_0132:
		{
			GUILayoutEntry_t1385 * L_55 = V_8;
			float L_56 = V_6;
			GUILayoutEntry_t1385 * L_57 = V_8;
			NullCheck(L_57);
			RectOffset_t780 * L_58 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_57);
			NullCheck(L_58);
			int32_t L_59 = RectOffset_get_left_m4410(L_58, /*hidden argument*/NULL);
			float L_60 = V_7;
			GUILayoutEntry_t1385 * L_61 = V_8;
			NullCheck(L_61);
			RectOffset_t780 * L_62 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_61);
			NullCheck(L_62);
			int32_t L_63 = RectOffset_get_horizontal_m4405(L_62, /*hidden argument*/NULL);
			NullCheck(L_55);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_55, ((float)((float)L_56+(float)(((float)((float)L_59))))), ((float)((float)L_60-(float)(((float)((float)L_63))))));
			goto IL_0198;
		}

IL_015e:
		{
			GUILayoutEntry_t1385 * L_64 = V_8;
			float L_65 = V_6;
			GUILayoutEntry_t1385 * L_66 = V_8;
			NullCheck(L_66);
			RectOffset_t780 * L_67 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_66);
			NullCheck(L_67);
			int32_t L_68 = RectOffset_get_left_m4410(L_67, /*hidden argument*/NULL);
			float L_69 = V_7;
			GUILayoutEntry_t1385 * L_70 = V_8;
			NullCheck(L_70);
			RectOffset_t780 * L_71 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_70);
			NullCheck(L_71);
			int32_t L_72 = RectOffset_get_horizontal_m4405(L_71, /*hidden argument*/NULL);
			GUILayoutEntry_t1385 * L_73 = V_8;
			NullCheck(L_73);
			float L_74 = (L_73->___minWidth_0);
			GUILayoutEntry_t1385 * L_75 = V_8;
			NullCheck(L_75);
			float L_76 = (L_75->___maxWidth_1);
			float L_77 = Mathf_Clamp_m375(NULL /*static, unused*/, ((float)((float)L_69-(float)(((float)((float)L_72))))), L_74, L_76, /*hidden argument*/NULL);
			NullCheck(L_64);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_64, ((float)((float)L_65+(float)(((float)((float)L_68))))), L_77);
		}

IL_0198:
		{
			bool L_78 = Enumerator_MoveNext_m8183((&V_9), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_78)
			{
				goto IL_011d;
			}
		}

IL_01a4:
		{
			IL2CPP_LEAVE(0x1B6, FINALLY_01a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_01a9;
	}

FINALLY_01a9:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_79 = V_9;
		Enumerator_t1470  L_80 = L_79;
		Object_t * L_81 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_80);
		NullCheck(L_81);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_81);
		IL2CPP_END_FINALLY(425)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(425)
	{
		IL2CPP_JUMP_TBL(0x1B6, IL_01b6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_01b6:
	{
		goto IL_03b0;
	}

IL_01bb:
	{
		GUIStyle_t184 * L_82 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_83 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t184 *)L_82) == ((Object_t*)(GUIStyle_t184 *)L_83)))
		{
			goto IL_0248;
		}
	}
	{
		RectOffset_t780 * L_84 = V_0;
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_left_m4410(L_84, /*hidden argument*/NULL);
		V_10 = (((float)((float)L_85)));
		RectOffset_t780 * L_86 = V_0;
		NullCheck(L_86);
		int32_t L_87 = RectOffset_get_right_m7702(L_86, /*hidden argument*/NULL);
		V_11 = (((float)((float)L_87)));
		List_1_t1386 * L_88 = (__this->___entries_10);
		NullCheck(L_88);
		int32_t L_89 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_88);
		if (!L_89)
		{
			goto IL_0239;
		}
	}
	{
		float L_90 = V_10;
		List_1_t1386 * L_91 = (__this->___entries_10);
		NullCheck(L_91);
		GUILayoutEntry_t1385 * L_92 = (GUILayoutEntry_t1385 *)VirtFuncInvoker1< GUILayoutEntry_t1385 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_91, 0);
		NullCheck(L_92);
		RectOffset_t780 * L_93 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_92);
		NullCheck(L_93);
		int32_t L_94 = RectOffset_get_left_m4410(L_93, /*hidden argument*/NULL);
		float L_95 = Mathf_Max_m4353(NULL /*static, unused*/, L_90, (((float)((float)L_94))), /*hidden argument*/NULL);
		V_10 = L_95;
		float L_96 = V_11;
		List_1_t1386 * L_97 = (__this->___entries_10);
		List_1_t1386 * L_98 = (__this->___entries_10);
		NullCheck(L_98);
		int32_t L_99 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_98);
		NullCheck(L_97);
		GUILayoutEntry_t1385 * L_100 = (GUILayoutEntry_t1385 *)VirtFuncInvoker1< GUILayoutEntry_t1385 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_97, ((int32_t)((int32_t)L_99-(int32_t)1)));
		NullCheck(L_100);
		RectOffset_t780 * L_101 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_100);
		NullCheck(L_101);
		int32_t L_102 = RectOffset_get_right_m7702(L_101, /*hidden argument*/NULL);
		float L_103 = Mathf_Max_m4353(NULL /*static, unused*/, L_96, (((float)((float)L_102))), /*hidden argument*/NULL);
		V_11 = L_103;
	}

IL_0239:
	{
		float L_104 = ___x;
		float L_105 = V_10;
		___x = ((float)((float)L_104+(float)L_105));
		float L_106 = ___width;
		float L_107 = V_11;
		float L_108 = V_10;
		___width = ((float)((float)L_106-(float)((float)((float)L_107+(float)L_108))));
	}

IL_0248:
	{
		float L_109 = ___width;
		float L_110 = (__this->___spacing_13);
		List_1_t1386 * L_111 = (__this->___entries_10);
		NullCheck(L_111);
		int32_t L_112 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_111);
		V_12 = ((float)((float)L_109-(float)((float)((float)L_110*(float)(((float)((float)((int32_t)((int32_t)L_112-(int32_t)1)))))))));
		V_13 = (0.0f);
		float L_113 = (__this->___childMinWidth_22);
		float L_114 = (__this->___childMaxWidth_23);
		if ((((float)L_113) == ((float)L_114)))
		{
			goto IL_02a1;
		}
	}
	{
		float L_115 = V_12;
		float L_116 = (__this->___childMinWidth_22);
		float L_117 = (__this->___childMaxWidth_23);
		float L_118 = (__this->___childMinWidth_22);
		float L_119 = Mathf_Clamp_m375(NULL /*static, unused*/, ((float)((float)((float)((float)L_115-(float)L_116))/(float)((float)((float)L_117-(float)L_118)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_13 = L_119;
	}

IL_02a1:
	{
		V_14 = (0.0f);
		float L_120 = V_12;
		float L_121 = (__this->___childMaxWidth_23);
		if ((!(((float)L_120) > ((float)L_121))))
		{
			goto IL_02d4;
		}
	}
	{
		int32_t L_122 = (__this->___stretchableCountX_18);
		if ((((int32_t)L_122) <= ((int32_t)0)))
		{
			goto IL_02d4;
		}
	}
	{
		float L_123 = V_12;
		float L_124 = (__this->___childMaxWidth_23);
		int32_t L_125 = (__this->___stretchableCountX_18);
		V_14 = ((float)((float)((float)((float)L_123-(float)L_124))/(float)(((float)((float)L_125)))));
	}

IL_02d4:
	{
		V_15 = 0;
		V_16 = 1;
		List_1_t1386 * L_126 = (__this->___entries_10);
		NullCheck(L_126);
		Enumerator_t1470  L_127 = List_1_GetEnumerator_m8181(L_126, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_18 = L_127;
	}

IL_02e7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0392;
		}

IL_02ec:
		{
			GUILayoutEntry_t1385 * L_128 = Enumerator_get_Current_m8182((&V_18), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_17 = L_128;
			GUILayoutEntry_t1385 * L_129 = V_17;
			NullCheck(L_129);
			float L_130 = (L_129->___minWidth_0);
			GUILayoutEntry_t1385 * L_131 = V_17;
			NullCheck(L_131);
			float L_132 = (L_131->___maxWidth_1);
			float L_133 = V_13;
			float L_134 = Mathf_Lerp_m434(NULL /*static, unused*/, L_130, L_132, L_133, /*hidden argument*/NULL);
			V_19 = L_134;
			float L_135 = V_19;
			float L_136 = V_14;
			GUILayoutEntry_t1385 * L_137 = V_17;
			NullCheck(L_137);
			int32_t L_138 = (L_137->___stretchWidth_5);
			V_19 = ((float)((float)L_135+(float)((float)((float)L_136*(float)(((float)((float)L_138)))))));
			GUILayoutEntry_t1385 * L_139 = V_17;
			NullCheck(L_139);
			GUIStyle_t184 * L_140 = GUILayoutEntry_get_style_m7571(L_139, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUIStyle_t184 * L_141 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t184 *)L_140) == ((Object_t*)(GUIStyle_t184 *)L_141)))
			{
				goto IL_0371;
			}
		}

IL_032d:
		{
			GUILayoutEntry_t1385 * L_142 = V_17;
			NullCheck(L_142);
			RectOffset_t780 * L_143 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_142);
			NullCheck(L_143);
			int32_t L_144 = RectOffset_get_left_m4410(L_143, /*hidden argument*/NULL);
			V_20 = L_144;
			bool L_145 = V_16;
			if (!L_145)
			{
				goto IL_0348;
			}
		}

IL_0342:
		{
			V_20 = 0;
			V_16 = 0;
		}

IL_0348:
		{
			int32_t L_146 = V_15;
			int32_t L_147 = V_20;
			if ((((int32_t)L_146) <= ((int32_t)L_147)))
			{
				goto IL_0358;
			}
		}

IL_0351:
		{
			int32_t L_148 = V_15;
			G_B39_0 = L_148;
			goto IL_035a;
		}

IL_0358:
		{
			int32_t L_149 = V_20;
			G_B39_0 = L_149;
		}

IL_035a:
		{
			V_21 = G_B39_0;
			float L_150 = ___x;
			int32_t L_151 = V_21;
			___x = ((float)((float)L_150+(float)(((float)((float)L_151)))));
			GUILayoutEntry_t1385 * L_152 = V_17;
			NullCheck(L_152);
			RectOffset_t780 * L_153 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_152);
			NullCheck(L_153);
			int32_t L_154 = RectOffset_get_right_m7702(L_153, /*hidden argument*/NULL);
			V_15 = L_154;
		}

IL_0371:
		{
			GUILayoutEntry_t1385 * L_155 = V_17;
			float L_156 = ___x;
			float L_157 = bankers_roundf(L_156);
			float L_158 = V_19;
			float L_159 = bankers_roundf(L_158);
			NullCheck(L_155);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_155, L_157, L_159);
			float L_160 = ___x;
			float L_161 = V_19;
			float L_162 = (__this->___spacing_13);
			___x = ((float)((float)L_160+(float)((float)((float)L_161+(float)L_162))));
		}

IL_0392:
		{
			bool L_163 = Enumerator_MoveNext_m8183((&V_18), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_163)
			{
				goto IL_02ec;
			}
		}

IL_039e:
		{
			IL2CPP_LEAVE(0x3B0, FINALLY_03a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_03a3;
	}

FINALLY_03a3:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_164 = V_18;
		Enumerator_t1470  L_165 = L_164;
		Object_t * L_166 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_165);
		NullCheck(L_166);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_166);
		IL2CPP_END_FINALLY(931)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(931)
	{
		IL2CPP_JUMP_TBL(0x3B0, IL_03b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_03b0:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcHeight()
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1470_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8181_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8182_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8183_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcHeight_m7590 (GUILayoutGroup_t1383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		Enumerator_t1470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		List_1_GetEnumerator_m8181_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484721);
		Enumerator_get_Current_m8182_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484722);
		Enumerator_MoveNext_m8183_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484723);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	GUILayoutEntry_t1385 * V_4 = {0};
	Enumerator_t1470  V_5 = {0};
	RectOffset_t780 * V_6 = {0};
	int32_t V_7 = 0;
	bool V_8 = false;
	GUILayoutEntry_t1385 * V_9 = {0};
	Enumerator_t1470  V_10 = {0};
	RectOffset_t780 * V_11 = {0};
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B36_0 = 0;
	int32_t G_B36_1 = 0;
	GUILayoutGroup_t1383 * G_B36_2 = {0};
	int32_t G_B35_0 = 0;
	int32_t G_B35_1 = 0;
	GUILayoutGroup_t1383 * G_B35_2 = {0};
	int32_t G_B37_0 = 0;
	int32_t G_B37_1 = 0;
	int32_t G_B37_2 = 0;
	GUILayoutGroup_t1383 * G_B37_3 = {0};
	{
		List_1_t1386 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t184 * L_2 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t780 * L_3 = GUIStyle_get_padding_m7728(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_vertical_m4406(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)((float)L_4)));
		V_14 = L_5;
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_5;
		float L_6 = V_14;
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_6;
		return;
	}

IL_0033:
	{
		float L_7 = (0.0f);
		V_14 = L_7;
		__this->___childMaxHeight_25 = L_7;
		float L_8 = V_14;
		__this->___childMinHeight_24 = L_8;
		V_0 = 0;
		V_1 = 0;
		__this->___stretchableCountY_19 = 0;
		bool L_9 = (__this->___isVertical_11);
		if (!L_9)
		{
			goto IL_01d4;
		}
	}
	{
		V_2 = 0;
		V_3 = 1;
		List_1_t1386 * L_10 = (__this->___entries_10);
		NullCheck(L_10);
		Enumerator_t1470  L_11 = List_1_GetEnumerator_m8181(L_10, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_5 = L_11;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0159;
		}

IL_0075:
		{
			GUILayoutEntry_t1385 * L_12 = Enumerator_get_Current_m8182((&V_5), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_4 = L_12;
			GUILayoutEntry_t1385 * L_13 = V_4;
			NullCheck(L_13);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_13);
			GUILayoutEntry_t1385 * L_14 = V_4;
			NullCheck(L_14);
			RectOffset_t780 * L_15 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_14);
			V_6 = L_15;
			GUILayoutEntry_t1385 * L_16 = V_4;
			NullCheck(L_16);
			GUIStyle_t184 * L_17 = GUILayoutEntry_get_style_m7571(L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUIStyle_t184 * L_18 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t184 *)L_17) == ((Object_t*)(GUIStyle_t184 *)L_18)))
			{
				goto IL_011d;
			}
		}

IL_009f:
		{
			bool L_19 = V_3;
			if (L_19)
			{
				goto IL_00b9;
			}
		}

IL_00a5:
		{
			int32_t L_20 = V_2;
			RectOffset_t780 * L_21 = V_6;
			NullCheck(L_21);
			int32_t L_22 = RectOffset_get_top_m4411(L_21, /*hidden argument*/NULL);
			int32_t L_23 = Mathf_Max_m4262(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
			V_7 = L_23;
			goto IL_00be;
		}

IL_00b9:
		{
			V_7 = 0;
			V_3 = 0;
		}

IL_00be:
		{
			float L_24 = (__this->___childMinHeight_24);
			GUILayoutEntry_t1385 * L_25 = V_4;
			NullCheck(L_25);
			float L_26 = (L_25->___minHeight_2);
			float L_27 = (__this->___spacing_13);
			int32_t L_28 = V_7;
			__this->___childMinHeight_24 = ((float)((float)L_24+(float)((float)((float)((float)((float)L_26+(float)L_27))+(float)(((float)((float)L_28)))))));
			float L_29 = (__this->___childMaxHeight_25);
			GUILayoutEntry_t1385 * L_30 = V_4;
			NullCheck(L_30);
			float L_31 = (L_30->___maxHeight_3);
			float L_32 = (__this->___spacing_13);
			int32_t L_33 = V_7;
			__this->___childMaxHeight_25 = ((float)((float)L_29+(float)((float)((float)((float)((float)L_31+(float)L_32))+(float)(((float)((float)L_33)))))));
			RectOffset_t780 * L_34 = V_6;
			NullCheck(L_34);
			int32_t L_35 = RectOffset_get_bottom_m7705(L_34, /*hidden argument*/NULL);
			V_2 = L_35;
			int32_t L_36 = (__this->___stretchableCountY_19);
			GUILayoutEntry_t1385 * L_37 = V_4;
			NullCheck(L_37);
			int32_t L_38 = (L_37->___stretchHeight_6);
			__this->___stretchableCountY_19 = ((int32_t)((int32_t)L_36+(int32_t)L_38));
			goto IL_0159;
		}

IL_011d:
		{
			float L_39 = (__this->___childMinHeight_24);
			GUILayoutEntry_t1385 * L_40 = V_4;
			NullCheck(L_40);
			float L_41 = (L_40->___minHeight_2);
			__this->___childMinHeight_24 = ((float)((float)L_39+(float)L_41));
			float L_42 = (__this->___childMaxHeight_25);
			GUILayoutEntry_t1385 * L_43 = V_4;
			NullCheck(L_43);
			float L_44 = (L_43->___maxHeight_3);
			__this->___childMaxHeight_25 = ((float)((float)L_42+(float)L_44));
			int32_t L_45 = (__this->___stretchableCountY_19);
			GUILayoutEntry_t1385 * L_46 = V_4;
			NullCheck(L_46);
			int32_t L_47 = (L_46->___stretchHeight_6);
			__this->___stretchableCountY_19 = ((int32_t)((int32_t)L_45+(int32_t)L_47));
		}

IL_0159:
		{
			bool L_48 = Enumerator_MoveNext_m8183((&V_5), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_48)
			{
				goto IL_0075;
			}
		}

IL_0165:
		{
			IL2CPP_LEAVE(0x177, FINALLY_016a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_016a;
	}

FINALLY_016a:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_49 = V_5;
		Enumerator_t1470  L_50 = L_49;
		Object_t * L_51 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_51);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_51);
		IL2CPP_END_FINALLY(362)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(362)
	{
		IL2CPP_JUMP_TBL(0x177, IL_0177)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0177:
	{
		float L_52 = (__this->___childMinHeight_24);
		float L_53 = (__this->___spacing_13);
		__this->___childMinHeight_24 = ((float)((float)L_52-(float)L_53));
		float L_54 = (__this->___childMaxHeight_25);
		float L_55 = (__this->___spacing_13);
		__this->___childMaxHeight_25 = ((float)((float)L_54-(float)L_55));
		List_1_t1386 * L_56 = (__this->___entries_10);
		NullCheck(L_56);
		int32_t L_57 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_56);
		if (!L_57)
		{
			goto IL_01cb;
		}
	}
	{
		List_1_t1386 * L_58 = (__this->___entries_10);
		NullCheck(L_58);
		GUILayoutEntry_t1385 * L_59 = (GUILayoutEntry_t1385 *)VirtFuncInvoker1< GUILayoutEntry_t1385 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_58, 0);
		NullCheck(L_59);
		RectOffset_t780 * L_60 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_59);
		NullCheck(L_60);
		int32_t L_61 = RectOffset_get_top_m4411(L_60, /*hidden argument*/NULL);
		V_0 = L_61;
		int32_t L_62 = V_2;
		V_1 = L_62;
		goto IL_01cf;
	}

IL_01cb:
	{
		int32_t L_63 = 0;
		V_0 = L_63;
		V_1 = L_63;
	}

IL_01cf:
	{
		goto IL_02b0;
	}

IL_01d4:
	{
		V_8 = 1;
		List_1_t1386 * L_64 = (__this->___entries_10);
		NullCheck(L_64);
		Enumerator_t1470  L_65 = List_1_GetEnumerator_m8181(L_64, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_10 = L_65;
	}

IL_01e4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0292;
		}

IL_01e9:
		{
			GUILayoutEntry_t1385 * L_66 = Enumerator_get_Current_m8182((&V_10), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_9 = L_66;
			GUILayoutEntry_t1385 * L_67 = V_9;
			NullCheck(L_67);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_67);
			GUILayoutEntry_t1385 * L_68 = V_9;
			NullCheck(L_68);
			RectOffset_t780 * L_69 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_68);
			V_11 = L_69;
			GUILayoutEntry_t1385 * L_70 = V_9;
			NullCheck(L_70);
			GUIStyle_t184 * L_71 = GUILayoutEntry_get_style_m7571(L_70, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUIStyle_t184 * L_72 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t184 *)L_71) == ((Object_t*)(GUIStyle_t184 *)L_72)))
			{
				goto IL_027e;
			}
		}

IL_0213:
		{
			bool L_73 = V_8;
			if (L_73)
			{
				goto IL_023b;
			}
		}

IL_021a:
		{
			RectOffset_t780 * L_74 = V_11;
			NullCheck(L_74);
			int32_t L_75 = RectOffset_get_top_m4411(L_74, /*hidden argument*/NULL);
			int32_t L_76 = V_0;
			int32_t L_77 = Mathf_Min_m288(NULL /*static, unused*/, L_75, L_76, /*hidden argument*/NULL);
			V_0 = L_77;
			RectOffset_t780 * L_78 = V_11;
			NullCheck(L_78);
			int32_t L_79 = RectOffset_get_bottom_m7705(L_78, /*hidden argument*/NULL);
			int32_t L_80 = V_1;
			int32_t L_81 = Mathf_Min_m288(NULL /*static, unused*/, L_79, L_80, /*hidden argument*/NULL);
			V_1 = L_81;
			goto IL_024e;
		}

IL_023b:
		{
			RectOffset_t780 * L_82 = V_11;
			NullCheck(L_82);
			int32_t L_83 = RectOffset_get_top_m4411(L_82, /*hidden argument*/NULL);
			V_0 = L_83;
			RectOffset_t780 * L_84 = V_11;
			NullCheck(L_84);
			int32_t L_85 = RectOffset_get_bottom_m7705(L_84, /*hidden argument*/NULL);
			V_1 = L_85;
			V_8 = 0;
		}

IL_024e:
		{
			GUILayoutEntry_t1385 * L_86 = V_9;
			NullCheck(L_86);
			float L_87 = (L_86->___minHeight_2);
			float L_88 = (__this->___childMinHeight_24);
			float L_89 = Mathf_Max_m4353(NULL /*static, unused*/, L_87, L_88, /*hidden argument*/NULL);
			__this->___childMinHeight_24 = L_89;
			GUILayoutEntry_t1385 * L_90 = V_9;
			NullCheck(L_90);
			float L_91 = (L_90->___maxHeight_3);
			float L_92 = (__this->___childMaxHeight_25);
			float L_93 = Mathf_Max_m4353(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
			__this->___childMaxHeight_25 = L_93;
		}

IL_027e:
		{
			int32_t L_94 = (__this->___stretchableCountY_19);
			GUILayoutEntry_t1385 * L_95 = V_9;
			NullCheck(L_95);
			int32_t L_96 = (L_95->___stretchHeight_6);
			__this->___stretchableCountY_19 = ((int32_t)((int32_t)L_94+(int32_t)L_96));
		}

IL_0292:
		{
			bool L_97 = Enumerator_MoveNext_m8183((&V_10), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_97)
			{
				goto IL_01e9;
			}
		}

IL_029e:
		{
			IL2CPP_LEAVE(0x2B0, FINALLY_02a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_02a3;
	}

FINALLY_02a3:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_98 = V_10;
		Enumerator_t1470  L_99 = L_98;
		Object_t * L_100 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_99);
		NullCheck(L_100);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_100);
		IL2CPP_END_FINALLY(675)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(675)
	{
		IL2CPP_JUMP_TBL(0x2B0, IL_02b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_02b0:
	{
		V_12 = (0.0f);
		V_13 = (0.0f);
		GUIStyle_t184 * L_101 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_102 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t184 *)L_101) == ((Object_t*)(GUIStyle_t184 *)L_102))))
		{
			goto IL_02d9;
		}
	}
	{
		bool L_103 = (__this->___userSpecifiedHeight_21);
		if (!L_103)
		{
			goto IL_0310;
		}
	}

IL_02d9:
	{
		GUIStyle_t184 * L_104 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_104);
		RectOffset_t780 * L_105 = GUIStyle_get_padding_m7728(L_104, /*hidden argument*/NULL);
		NullCheck(L_105);
		int32_t L_106 = RectOffset_get_top_m4411(L_105, /*hidden argument*/NULL);
		int32_t L_107 = V_0;
		int32_t L_108 = Mathf_Max_m4262(NULL /*static, unused*/, L_106, L_107, /*hidden argument*/NULL);
		V_12 = (((float)((float)L_108)));
		GUIStyle_t184 * L_109 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_109);
		RectOffset_t780 * L_110 = GUIStyle_get_padding_m7728(L_109, /*hidden argument*/NULL);
		NullCheck(L_110);
		int32_t L_111 = RectOffset_get_bottom_m7705(L_110, /*hidden argument*/NULL);
		int32_t L_112 = V_1;
		int32_t L_113 = Mathf_Max_m4262(NULL /*static, unused*/, L_111, L_112, /*hidden argument*/NULL);
		V_13 = (((float)((float)L_113)));
		goto IL_0332;
	}

IL_0310:
	{
		RectOffset_t780 * L_114 = (__this->___m_Margin_26);
		int32_t L_115 = V_0;
		NullCheck(L_114);
		RectOffset_set_top_m7704(L_114, L_115, /*hidden argument*/NULL);
		RectOffset_t780 * L_116 = (__this->___m_Margin_26);
		int32_t L_117 = V_1;
		NullCheck(L_116);
		RectOffset_set_bottom_m7706(L_116, L_117, /*hidden argument*/NULL);
		float L_118 = (0.0f);
		V_13 = L_118;
		V_12 = L_118;
	}

IL_0332:
	{
		float L_119 = (((GUILayoutEntry_t1385 *)__this)->___minHeight_2);
		float L_120 = (__this->___childMinHeight_24);
		float L_121 = V_12;
		float L_122 = V_13;
		float L_123 = Mathf_Max_m4353(NULL /*static, unused*/, L_119, ((float)((float)((float)((float)L_120+(float)L_121))+(float)L_122)), /*hidden argument*/NULL);
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_123;
		float L_124 = (((GUILayoutEntry_t1385 *)__this)->___maxHeight_3);
		if ((!(((float)L_124) == ((float)(0.0f)))))
		{
			goto IL_03a1;
		}
	}
	{
		int32_t L_125 = (((GUILayoutEntry_t1385 *)__this)->___stretchHeight_6);
		int32_t L_126 = (__this->___stretchableCountY_19);
		GUIStyle_t184 * L_127 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_127);
		bool L_128 = GUIStyle_get_stretchHeight_m7738(L_127, /*hidden argument*/NULL);
		G_B35_0 = L_126;
		G_B35_1 = L_125;
		G_B35_2 = __this;
		if (!L_128)
		{
			G_B36_0 = L_126;
			G_B36_1 = L_125;
			G_B36_2 = __this;
			goto IL_0382;
		}
	}
	{
		G_B37_0 = 1;
		G_B37_1 = G_B35_0;
		G_B37_2 = G_B35_1;
		G_B37_3 = G_B35_2;
		goto IL_0383;
	}

IL_0382:
	{
		G_B37_0 = 0;
		G_B37_1 = G_B36_0;
		G_B37_2 = G_B36_1;
		G_B37_3 = G_B36_2;
	}

IL_0383:
	{
		NullCheck(G_B37_3);
		((GUILayoutEntry_t1385 *)G_B37_3)->___stretchHeight_6 = ((int32_t)((int32_t)G_B37_2+(int32_t)((int32_t)((int32_t)G_B37_1+(int32_t)G_B37_0))));
		float L_129 = (__this->___childMaxHeight_25);
		float L_130 = V_12;
		float L_131 = V_13;
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = ((float)((float)((float)((float)L_129+(float)L_130))+(float)L_131));
		goto IL_03a8;
	}

IL_03a1:
	{
		((GUILayoutEntry_t1385 *)__this)->___stretchHeight_6 = 0;
	}

IL_03a8:
	{
		float L_132 = (((GUILayoutEntry_t1385 *)__this)->___maxHeight_3);
		float L_133 = (((GUILayoutEntry_t1385 *)__this)->___minHeight_2);
		float L_134 = Mathf_Max_m4353(NULL /*static, unused*/, L_132, L_133, /*hidden argument*/NULL);
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_134;
		GUIStyle_t184 * L_135 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_135);
		float L_136 = GUIStyle_get_fixedHeight_m7735(L_135, /*hidden argument*/NULL);
		if ((((float)L_136) == ((float)(0.0f))))
		{
			goto IL_03f7;
		}
	}
	{
		GUIStyle_t184 * L_137 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_137);
		float L_138 = GUIStyle_get_fixedHeight_m7735(L_137, /*hidden argument*/NULL);
		float L_139 = L_138;
		V_14 = L_139;
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_139;
		float L_140 = V_14;
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_140;
		((GUILayoutEntry_t1385 *)__this)->___stretchHeight_6 = 0;
	}

IL_03f7:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1470_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8181_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8182_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8183_MethodInfo_var;
extern "C" void GUILayoutGroup_SetVertical_m7591 (GUILayoutGroup_t1383 * __this, float ___y, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		Enumerator_t1470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		List_1_GetEnumerator_m8181_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484721);
		Enumerator_get_Current_m8182_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484722);
		Enumerator_MoveNext_m8183_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484723);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t780 * V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	bool V_7 = false;
	GUILayoutEntry_t1385 * V_8 = {0};
	Enumerator_t1470  V_9 = {0};
	float V_10 = 0.0f;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	GUILayoutEntry_t1385 * V_13 = {0};
	Enumerator_t1470  V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	GUILayoutEntry_t1385 * V_20 = {0};
	Enumerator_t1470  V_21 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	{
		float L_0 = ___y;
		float L_1 = ___height;
		GUILayoutEntry_SetVertical_m7577(__this, L_0, L_1, /*hidden argument*/NULL);
		List_1_t1386 * L_2 = (__this->___entries_10);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GUIStyle_t184 * L_4 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		RectOffset_t780 * L_5 = GUIStyle_get_padding_m7728(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		bool L_6 = (__this->___resetCoords_12);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		___y = (0.0f);
	}

IL_0037:
	{
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_022f;
		}
	}
	{
		GUIStyle_t184 * L_8 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_9 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t184 *)L_8) == ((Object_t*)(GUIStyle_t184 *)L_9)))
		{
			goto IL_00c6;
		}
	}
	{
		RectOffset_t780 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m4411(L_10, /*hidden argument*/NULL);
		V_1 = (((float)((float)L_11)));
		RectOffset_t780 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_bottom_m7705(L_12, /*hidden argument*/NULL);
		V_2 = (((float)((float)L_13)));
		List_1_t1386 * L_14 = (__this->___entries_10);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_14);
		if (!L_15)
		{
			goto IL_00ba;
		}
	}
	{
		float L_16 = V_1;
		List_1_t1386 * L_17 = (__this->___entries_10);
		NullCheck(L_17);
		GUILayoutEntry_t1385 * L_18 = (GUILayoutEntry_t1385 *)VirtFuncInvoker1< GUILayoutEntry_t1385 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_17, 0);
		NullCheck(L_18);
		RectOffset_t780 * L_19 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_18);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m4411(L_19, /*hidden argument*/NULL);
		float L_21 = Mathf_Max_m4353(NULL /*static, unused*/, L_16, (((float)((float)L_20))), /*hidden argument*/NULL);
		V_1 = L_21;
		float L_22 = V_2;
		List_1_t1386 * L_23 = (__this->___entries_10);
		List_1_t1386 * L_24 = (__this->___entries_10);
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_24);
		NullCheck(L_23);
		GUILayoutEntry_t1385 * L_26 = (GUILayoutEntry_t1385 *)VirtFuncInvoker1< GUILayoutEntry_t1385 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_23, ((int32_t)((int32_t)L_25-(int32_t)1)));
		NullCheck(L_26);
		RectOffset_t780 * L_27 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_26);
		NullCheck(L_27);
		int32_t L_28 = RectOffset_get_bottom_m7705(L_27, /*hidden argument*/NULL);
		float L_29 = Mathf_Max_m4353(NULL /*static, unused*/, L_22, (((float)((float)L_28))), /*hidden argument*/NULL);
		V_2 = L_29;
	}

IL_00ba:
	{
		float L_30 = ___y;
		float L_31 = V_1;
		___y = ((float)((float)L_30+(float)L_31));
		float L_32 = ___height;
		float L_33 = V_2;
		float L_34 = V_1;
		___height = ((float)((float)L_32-(float)((float)((float)L_33+(float)L_34))));
	}

IL_00c6:
	{
		float L_35 = ___height;
		float L_36 = (__this->___spacing_13);
		List_1_t1386 * L_37 = (__this->___entries_10);
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_37);
		V_3 = ((float)((float)L_35-(float)((float)((float)L_36*(float)(((float)((float)((int32_t)((int32_t)L_38-(int32_t)1)))))))));
		V_4 = (0.0f);
		float L_39 = (__this->___childMinHeight_24);
		float L_40 = (__this->___childMaxHeight_25);
		if ((((float)L_39) == ((float)L_40)))
		{
			goto IL_011d;
		}
	}
	{
		float L_41 = V_3;
		float L_42 = (__this->___childMinHeight_24);
		float L_43 = (__this->___childMaxHeight_25);
		float L_44 = (__this->___childMinHeight_24);
		float L_45 = Mathf_Clamp_m375(NULL /*static, unused*/, ((float)((float)((float)((float)L_41-(float)L_42))/(float)((float)((float)L_43-(float)L_44)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_45;
	}

IL_011d:
	{
		V_5 = (0.0f);
		float L_46 = V_3;
		float L_47 = (__this->___childMaxHeight_25);
		if ((!(((float)L_46) > ((float)L_47))))
		{
			goto IL_014e;
		}
	}
	{
		int32_t L_48 = (__this->___stretchableCountY_19);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_014e;
		}
	}
	{
		float L_49 = V_3;
		float L_50 = (__this->___childMaxHeight_25);
		int32_t L_51 = (__this->___stretchableCountY_19);
		V_5 = ((float)((float)((float)((float)L_49-(float)L_50))/(float)(((float)((float)L_51)))));
	}

IL_014e:
	{
		V_6 = 0;
		V_7 = 1;
		List_1_t1386 * L_52 = (__this->___entries_10);
		NullCheck(L_52);
		Enumerator_t1470  L_53 = List_1_GetEnumerator_m8181(L_52, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_9 = L_53;
	}

IL_0161:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020c;
		}

IL_0166:
		{
			GUILayoutEntry_t1385 * L_54 = Enumerator_get_Current_m8182((&V_9), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_8 = L_54;
			GUILayoutEntry_t1385 * L_55 = V_8;
			NullCheck(L_55);
			float L_56 = (L_55->___minHeight_2);
			GUILayoutEntry_t1385 * L_57 = V_8;
			NullCheck(L_57);
			float L_58 = (L_57->___maxHeight_3);
			float L_59 = V_4;
			float L_60 = Mathf_Lerp_m434(NULL /*static, unused*/, L_56, L_58, L_59, /*hidden argument*/NULL);
			V_10 = L_60;
			float L_61 = V_10;
			float L_62 = V_5;
			GUILayoutEntry_t1385 * L_63 = V_8;
			NullCheck(L_63);
			int32_t L_64 = (L_63->___stretchHeight_6);
			V_10 = ((float)((float)L_61+(float)((float)((float)L_62*(float)(((float)((float)L_64)))))));
			GUILayoutEntry_t1385 * L_65 = V_8;
			NullCheck(L_65);
			GUIStyle_t184 * L_66 = GUILayoutEntry_get_style_m7571(L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUIStyle_t184 * L_67 = GUILayoutUtility_get_spaceStyle_m7567(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t184 *)L_66) == ((Object_t*)(GUIStyle_t184 *)L_67)))
			{
				goto IL_01eb;
			}
		}

IL_01a7:
		{
			GUILayoutEntry_t1385 * L_68 = V_8;
			NullCheck(L_68);
			RectOffset_t780 * L_69 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_68);
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_top_m4411(L_69, /*hidden argument*/NULL);
			V_11 = L_70;
			bool L_71 = V_7;
			if (!L_71)
			{
				goto IL_01c2;
			}
		}

IL_01bc:
		{
			V_11 = 0;
			V_7 = 0;
		}

IL_01c2:
		{
			int32_t L_72 = V_6;
			int32_t L_73 = V_11;
			if ((((int32_t)L_72) <= ((int32_t)L_73)))
			{
				goto IL_01d2;
			}
		}

IL_01cb:
		{
			int32_t L_74 = V_6;
			G_B22_0 = L_74;
			goto IL_01d4;
		}

IL_01d2:
		{
			int32_t L_75 = V_11;
			G_B22_0 = L_75;
		}

IL_01d4:
		{
			V_12 = G_B22_0;
			float L_76 = ___y;
			int32_t L_77 = V_12;
			___y = ((float)((float)L_76+(float)(((float)((float)L_77)))));
			GUILayoutEntry_t1385 * L_78 = V_8;
			NullCheck(L_78);
			RectOffset_t780 * L_79 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_78);
			NullCheck(L_79);
			int32_t L_80 = RectOffset_get_bottom_m7705(L_79, /*hidden argument*/NULL);
			V_6 = L_80;
		}

IL_01eb:
		{
			GUILayoutEntry_t1385 * L_81 = V_8;
			float L_82 = ___y;
			float L_83 = bankers_roundf(L_82);
			float L_84 = V_10;
			float L_85 = bankers_roundf(L_84);
			NullCheck(L_81);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_81, L_83, L_85);
			float L_86 = ___y;
			float L_87 = V_10;
			float L_88 = (__this->___spacing_13);
			___y = ((float)((float)L_86+(float)((float)((float)L_87+(float)L_88))));
		}

IL_020c:
		{
			bool L_89 = Enumerator_MoveNext_m8183((&V_9), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_89)
			{
				goto IL_0166;
			}
		}

IL_0218:
		{
			IL2CPP_LEAVE(0x22A, FINALLY_021d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_021d;
	}

FINALLY_021d:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_90 = V_9;
		Enumerator_t1470  L_91 = L_90;
		Object_t * L_92 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_91);
		NullCheck(L_92);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_92);
		IL2CPP_END_FINALLY(541)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(541)
	{
		IL2CPP_JUMP_TBL(0x22A, IL_022a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_022a:
	{
		goto IL_03c1;
	}

IL_022f:
	{
		GUIStyle_t184 * L_93 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_94 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t184 *)L_93) == ((Object_t*)(GUIStyle_t184 *)L_94)))
		{
			goto IL_02f6;
		}
	}
	{
		List_1_t1386 * L_95 = (__this->___entries_10);
		NullCheck(L_95);
		Enumerator_t1470  L_96 = List_1_GetEnumerator_m8181(L_95, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_14 = L_96;
	}

IL_024c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02d3;
		}

IL_0251:
		{
			GUILayoutEntry_t1385 * L_97 = Enumerator_get_Current_m8182((&V_14), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_13 = L_97;
			GUILayoutEntry_t1385 * L_98 = V_13;
			NullCheck(L_98);
			RectOffset_t780 * L_99 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_98);
			NullCheck(L_99);
			int32_t L_100 = RectOffset_get_top_m4411(L_99, /*hidden argument*/NULL);
			RectOffset_t780 * L_101 = V_0;
			NullCheck(L_101);
			int32_t L_102 = RectOffset_get_top_m4411(L_101, /*hidden argument*/NULL);
			int32_t L_103 = Mathf_Max_m4262(NULL /*static, unused*/, L_100, L_102, /*hidden argument*/NULL);
			V_15 = (((float)((float)L_103)));
			float L_104 = ___y;
			float L_105 = V_15;
			V_16 = ((float)((float)L_104+(float)L_105));
			float L_106 = ___height;
			GUILayoutEntry_t1385 * L_107 = V_13;
			NullCheck(L_107);
			RectOffset_t780 * L_108 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_107);
			NullCheck(L_108);
			int32_t L_109 = RectOffset_get_bottom_m7705(L_108, /*hidden argument*/NULL);
			RectOffset_t780 * L_110 = V_0;
			NullCheck(L_110);
			int32_t L_111 = RectOffset_get_bottom_m7705(L_110, /*hidden argument*/NULL);
			int32_t L_112 = Mathf_Max_m4262(NULL /*static, unused*/, L_109, L_111, /*hidden argument*/NULL);
			float L_113 = V_15;
			V_17 = ((float)((float)((float)((float)L_106-(float)(((float)((float)L_112)))))-(float)L_113));
			GUILayoutEntry_t1385 * L_114 = V_13;
			NullCheck(L_114);
			int32_t L_115 = (L_114->___stretchHeight_6);
			if (!L_115)
			{
				goto IL_02b5;
			}
		}

IL_02a5:
		{
			GUILayoutEntry_t1385 * L_116 = V_13;
			float L_117 = V_16;
			float L_118 = V_17;
			NullCheck(L_116);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_116, L_117, L_118);
			goto IL_02d3;
		}

IL_02b5:
		{
			GUILayoutEntry_t1385 * L_119 = V_13;
			float L_120 = V_16;
			float L_121 = V_17;
			GUILayoutEntry_t1385 * L_122 = V_13;
			NullCheck(L_122);
			float L_123 = (L_122->___minHeight_2);
			GUILayoutEntry_t1385 * L_124 = V_13;
			NullCheck(L_124);
			float L_125 = (L_124->___maxHeight_3);
			float L_126 = Mathf_Clamp_m375(NULL /*static, unused*/, L_121, L_123, L_125, /*hidden argument*/NULL);
			NullCheck(L_119);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_119, L_120, L_126);
		}

IL_02d3:
		{
			bool L_127 = Enumerator_MoveNext_m8183((&V_14), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_127)
			{
				goto IL_0251;
			}
		}

IL_02df:
		{
			IL2CPP_LEAVE(0x2F1, FINALLY_02e4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_02e4;
	}

FINALLY_02e4:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_128 = V_14;
		Enumerator_t1470  L_129 = L_128;
		Object_t * L_130 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_129);
		NullCheck(L_130);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_130);
		IL2CPP_END_FINALLY(740)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(740)
	{
		IL2CPP_JUMP_TBL(0x2F1, IL_02f1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_02f1:
	{
		goto IL_03c1;
	}

IL_02f6:
	{
		float L_131 = ___y;
		RectOffset_t780 * L_132 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_132);
		int32_t L_133 = RectOffset_get_top_m4411(L_132, /*hidden argument*/NULL);
		V_18 = ((float)((float)L_131-(float)(((float)((float)L_133)))));
		float L_134 = ___height;
		RectOffset_t780 * L_135 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_135);
		int32_t L_136 = RectOffset_get_vertical_m4406(L_135, /*hidden argument*/NULL);
		V_19 = ((float)((float)L_134+(float)(((float)((float)L_136)))));
		List_1_t1386 * L_137 = (__this->___entries_10);
		NullCheck(L_137);
		Enumerator_t1470  L_138 = List_1_GetEnumerator_m8181(L_137, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_21 = L_138;
	}

IL_0323:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03a3;
		}

IL_0328:
		{
			GUILayoutEntry_t1385 * L_139 = Enumerator_get_Current_m8182((&V_21), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_20 = L_139;
			GUILayoutEntry_t1385 * L_140 = V_20;
			NullCheck(L_140);
			int32_t L_141 = (L_140->___stretchHeight_6);
			if (!L_141)
			{
				goto IL_0369;
			}
		}

IL_033d:
		{
			GUILayoutEntry_t1385 * L_142 = V_20;
			float L_143 = V_18;
			GUILayoutEntry_t1385 * L_144 = V_20;
			NullCheck(L_144);
			RectOffset_t780 * L_145 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_144);
			NullCheck(L_145);
			int32_t L_146 = RectOffset_get_top_m4411(L_145, /*hidden argument*/NULL);
			float L_147 = V_19;
			GUILayoutEntry_t1385 * L_148 = V_20;
			NullCheck(L_148);
			RectOffset_t780 * L_149 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_148);
			NullCheck(L_149);
			int32_t L_150 = RectOffset_get_vertical_m4406(L_149, /*hidden argument*/NULL);
			NullCheck(L_142);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_142, ((float)((float)L_143+(float)(((float)((float)L_146))))), ((float)((float)L_147-(float)(((float)((float)L_150))))));
			goto IL_03a3;
		}

IL_0369:
		{
			GUILayoutEntry_t1385 * L_151 = V_20;
			float L_152 = V_18;
			GUILayoutEntry_t1385 * L_153 = V_20;
			NullCheck(L_153);
			RectOffset_t780 * L_154 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_153);
			NullCheck(L_154);
			int32_t L_155 = RectOffset_get_top_m4411(L_154, /*hidden argument*/NULL);
			float L_156 = V_19;
			GUILayoutEntry_t1385 * L_157 = V_20;
			NullCheck(L_157);
			RectOffset_t780 * L_158 = (RectOffset_t780 *)VirtFuncInvoker0< RectOffset_t780 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_157);
			NullCheck(L_158);
			int32_t L_159 = RectOffset_get_vertical_m4406(L_158, /*hidden argument*/NULL);
			GUILayoutEntry_t1385 * L_160 = V_20;
			NullCheck(L_160);
			float L_161 = (L_160->___minHeight_2);
			GUILayoutEntry_t1385 * L_162 = V_20;
			NullCheck(L_162);
			float L_163 = (L_162->___maxHeight_3);
			float L_164 = Mathf_Clamp_m375(NULL /*static, unused*/, ((float)((float)L_156-(float)(((float)((float)L_159))))), L_161, L_163, /*hidden argument*/NULL);
			NullCheck(L_151);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_151, ((float)((float)L_152+(float)(((float)((float)L_155))))), L_164);
		}

IL_03a3:
		{
			bool L_165 = Enumerator_MoveNext_m8183((&V_21), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_165)
			{
				goto IL_0328;
			}
		}

IL_03af:
		{
			IL2CPP_LEAVE(0x3C1, FINALLY_03b4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_03b4;
	}

FINALLY_03b4:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_166 = V_21;
		Enumerator_t1470  L_167 = L_166;
		Object_t * L_168 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_167);
		NullCheck(L_168);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_168);
		IL2CPP_END_FINALLY(948)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(948)
	{
		IL2CPP_JUMP_TBL(0x3C1, IL_03c1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_03c1:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutGroup::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t1385_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1470_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8181_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8182_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8183_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral937;
extern Il2CppCodeGenString* _stringLiteral1189;
extern Il2CppCodeGenString* _stringLiteral1190;
extern Il2CppCodeGenString* _stringLiteral1111;
extern Il2CppCodeGenString* _stringLiteral1191;
extern "C" String_t* GUILayoutGroup_ToString_m7592 (GUILayoutGroup_t1383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		GUILayoutEntry_t1385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(957);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enumerator_t1470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		List_1_GetEnumerator_m8181_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484721);
		Enumerator_get_Current_m8182_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484722);
		Enumerator_MoveNext_m8183_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484723);
		_stringLiteral937 = il2cpp_codegen_string_literal_from_index(937);
		_stringLiteral1189 = il2cpp_codegen_string_literal_from_index(1189);
		_stringLiteral1190 = il2cpp_codegen_string_literal_from_index(1190);
		_stringLiteral1111 = il2cpp_codegen_string_literal_from_index(1111);
		_stringLiteral1191 = il2cpp_codegen_string_literal_from_index(1191);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	GUILayoutEntry_t1385 * V_3 = {0};
	Enumerator_t1470  V_4 = {0};
	String_t* V_5 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0023;
	}

IL_0013:
	{
		String_t* L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m409(NULL /*static, unused*/, L_2, _stringLiteral937, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t1385_il2cpp_TypeInfo_var);
		int32_t L_6 = ((GUILayoutEntry_t1385_StaticFields*)GUILayoutEntry_t1385_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_7 = V_0;
		V_5 = L_7;
		ObjectU5BU5D_t34* L_8 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 5));
		String_t* L_9 = V_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t34* L_10 = L_8;
		String_t* L_11 = GUILayoutEntry_ToString_m7580(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 1, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral1189);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral1189;
		ObjectU5BU5D_t34* L_13 = L_12;
		float L_14 = (__this->___childMinHeight_24);
		float L_15 = L_14;
		Object_t * L_16 = Box(Single_t36_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 3, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t34* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, _stringLiteral1190);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral1190;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2254(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t1385_il2cpp_TypeInfo_var);
		int32_t L_19 = ((GUILayoutEntry_t1385_StaticFields*)GUILayoutEntry_t1385_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t1385_StaticFields*)GUILayoutEntry_t1385_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_19+(int32_t)4));
		List_1_t1386 * L_20 = (__this->___entries_10);
		NullCheck(L_20);
		Enumerator_t1470  L_21 = List_1_GetEnumerator_m8181(L_20, /*hidden argument*/List_1_GetEnumerator_m8181_MethodInfo_var);
		V_4 = L_21;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a1;
		}

IL_0087:
		{
			GUILayoutEntry_t1385 * L_22 = Enumerator_get_Current_m8182((&V_4), /*hidden argument*/Enumerator_get_Current_m8182_MethodInfo_var);
			V_3 = L_22;
			String_t* L_23 = V_0;
			GUILayoutEntry_t1385 * L_24 = V_3;
			NullCheck(L_24);
			String_t* L_25 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.GUILayoutEntry::ToString() */, L_24);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m2152(NULL /*static, unused*/, L_23, L_25, _stringLiteral1111, /*hidden argument*/NULL);
			V_0 = L_26;
		}

IL_00a1:
		{
			bool L_27 = Enumerator_MoveNext_m8183((&V_4), /*hidden argument*/Enumerator_MoveNext_m8183_MethodInfo_var);
			if (L_27)
			{
				goto IL_0087;
			}
		}

IL_00ad:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_00b2;
	}

FINALLY_00b2:
	{ // begin finally (depth: 1)
		Enumerator_t1470  L_28 = V_4;
		Enumerator_t1470  L_29 = L_28;
		Object_t * L_30 = Box(Enumerator_t1470_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_30);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_30);
		IL2CPP_END_FINALLY(178)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(178)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_00bf:
	{
		String_t* L_31 = V_0;
		String_t* L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m2152(NULL /*static, unused*/, L_31, L_32, _stringLiteral1191, /*hidden argument*/NULL);
		V_0 = L_33;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t1385_il2cpp_TypeInfo_var);
		int32_t L_34 = ((GUILayoutEntry_t1385_StaticFields*)GUILayoutEntry_t1385_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t1385_StaticFields*)GUILayoutEntry_t1385_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_34-(int32_t)4));
		String_t* L_35 = V_0;
		return L_35;
	}
}
// System.Void UnityEngine.GUIScrollGroup::.ctor()
extern "C" void GUIScrollGroup__ctor_m7593 (GUIScrollGroup_t1387 * __this, const MethodInfo* method)
{
	{
		__this->___allowHorizontalScroll_33 = 1;
		__this->___allowVerticalScroll_34 = 1;
		GUILayoutGroup__ctor_m7581(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcWidth()
extern "C" void GUIScrollGroup_CalcWidth_m7594 (GUIScrollGroup_t1387 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t1385 *)__this)->___minWidth_0);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t1385 *)__this)->___maxWidth_1);
		V_1 = L_1;
		bool L_2 = (__this->___allowHorizontalScroll_33);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = (0.0f);
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcWidth_m7588(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t1385 *)__this)->___minWidth_0);
		__this->___calcMinWidth_27 = L_3;
		float L_4 = (((GUILayoutEntry_t1385 *)__this)->___maxWidth_1);
		__this->___calcMaxWidth_28 = L_4;
		bool L_5 = (__this->___allowHorizontalScroll_33);
		if (!L_5)
		{
			goto IL_009e;
		}
	}
	{
		float L_6 = (((GUILayoutEntry_t1385 *)__this)->___minWidth_0);
		if ((!(((float)L_6) > ((float)(32.0f)))))
		{
			goto IL_0073;
		}
	}
	{
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = (32.0f);
	}

IL_0073:
	{
		float L_7 = V_0;
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_0085;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = L_8;
	}

IL_0085:
	{
		float L_9 = V_1;
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_009e;
		}
	}
	{
		float L_10 = V_1;
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = L_10;
		((GUILayoutEntry_t1385 *)__this)->___stretchWidth_5 = 0;
	}

IL_009e:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetHorizontal(System.Single,System.Single)
extern "C" void GUIScrollGroup_SetHorizontal_m7595 (GUIScrollGroup_t1387 * __this, float ___x, float ___width, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (__this->___needsVerticalScrollbar_36);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		float L_1 = ___width;
		GUIStyle_t184 * L_2 = (__this->___verticalScrollbar_38);
		NullCheck(L_2);
		float L_3 = GUIStyle_get_fixedWidth_m7734(L_2, /*hidden argument*/NULL);
		GUIStyle_t184 * L_4 = (__this->___verticalScrollbar_38);
		NullCheck(L_4);
		RectOffset_t780 * L_5 = GUIStyle_get_margin_m7727(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = RectOffset_get_left_m4410(L_5, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)((float)((float)L_1-(float)L_3))-(float)(((float)((float)L_6)))));
		goto IL_0030;
	}

IL_002f:
	{
		float L_7 = ___width;
		G_B3_0 = L_7;
	}

IL_0030:
	{
		V_0 = G_B3_0;
		bool L_8 = (__this->___allowHorizontalScroll_33);
		if (!L_8)
		{
			goto IL_0091;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinWidth_27);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0091;
		}
	}
	{
		__this->___needsHorizontalScrollbar_35 = 1;
		float L_11 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = L_11;
		float L_12 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = L_12;
		float L_13 = ___x;
		float L_14 = (__this->___calcMinWidth_27);
		GUILayoutGroup_SetHorizontal_m7589(__this, L_13, L_14, /*hidden argument*/NULL);
		Rect_t30 * L_15 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_16 = ___width;
		Rect_set_width_m371(L_15, L_16, /*hidden argument*/NULL);
		float L_17 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_17;
		goto IL_00d6;
	}

IL_0091:
	{
		__this->___needsHorizontalScrollbar_35 = 0;
		bool L_18 = (__this->___allowHorizontalScroll_33);
		if (!L_18)
		{
			goto IL_00bb;
		}
	}
	{
		float L_19 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = L_19;
		float L_20 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = L_20;
	}

IL_00bb:
	{
		float L_21 = ___x;
		float L_22 = V_0;
		GUILayoutGroup_SetHorizontal_m7589(__this, L_21, L_22, /*hidden argument*/NULL);
		Rect_t30 * L_23 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_24 = ___width;
		Rect_set_width_m371(L_23, L_24, /*hidden argument*/NULL);
		float L_25 = V_0;
		__this->___clientWidth_31 = L_25;
	}

IL_00d6:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcHeight()
extern "C" void GUIScrollGroup_CalcHeight_m7596 (GUIScrollGroup_t1387 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t1385 *)__this)->___minHeight_2);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t1385 *)__this)->___maxHeight_3);
		V_1 = L_1;
		bool L_2 = (__this->___allowVerticalScroll_34);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = (0.0f);
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcHeight_m7590(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t1385 *)__this)->___minHeight_2);
		__this->___calcMinHeight_29 = L_3;
		float L_4 = (((GUILayoutEntry_t1385 *)__this)->___maxHeight_3);
		__this->___calcMaxHeight_30 = L_4;
		bool L_5 = (__this->___needsHorizontalScrollbar_35);
		if (!L_5)
		{
			goto IL_0092;
		}
	}
	{
		GUIStyle_t184 * L_6 = (__this->___horizontalScrollbar_37);
		NullCheck(L_6);
		float L_7 = GUIStyle_get_fixedHeight_m7735(L_6, /*hidden argument*/NULL);
		GUIStyle_t184 * L_8 = (__this->___horizontalScrollbar_37);
		NullCheck(L_8);
		RectOffset_t780 * L_9 = GUIStyle_get_margin_m7727(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_top_m4411(L_9, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_7+(float)(((float)((float)L_10)))));
		float L_11 = (((GUILayoutEntry_t1385 *)__this)->___minHeight_2);
		float L_12 = V_2;
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = ((float)((float)L_11+(float)L_12));
		float L_13 = (((GUILayoutEntry_t1385 *)__this)->___maxHeight_3);
		float L_14 = V_2;
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = ((float)((float)L_13+(float)L_14));
	}

IL_0092:
	{
		bool L_15 = (__this->___allowVerticalScroll_34);
		if (!L_15)
		{
			goto IL_00e3;
		}
	}
	{
		float L_16 = (((GUILayoutEntry_t1385 *)__this)->___minHeight_2);
		if ((!(((float)L_16) > ((float)(32.0f)))))
		{
			goto IL_00b8;
		}
	}
	{
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = (32.0f);
	}

IL_00b8:
	{
		float L_17 = V_0;
		if ((((float)L_17) == ((float)(0.0f))))
		{
			goto IL_00ca;
		}
	}
	{
		float L_18 = V_0;
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_18;
	}

IL_00ca:
	{
		float L_19 = V_1;
		if ((((float)L_19) == ((float)(0.0f))))
		{
			goto IL_00e3;
		}
	}
	{
		float L_20 = V_1;
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_20;
		((GUILayoutEntry_t1385 *)__this)->___stretchHeight_6 = 0;
	}

IL_00e3:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetVertical(System.Single,System.Single)
extern "C" void GUIScrollGroup_SetVertical_m7597 (GUIScrollGroup_t1387 * __this, float ___y, float ___height, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = ___height;
		V_0 = L_0;
		bool L_1 = (__this->___needsHorizontalScrollbar_35);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		float L_2 = V_0;
		GUIStyle_t184 * L_3 = (__this->___horizontalScrollbar_37);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedHeight_m7735(L_3, /*hidden argument*/NULL);
		GUIStyle_t184 * L_5 = (__this->___horizontalScrollbar_37);
		NullCheck(L_5);
		RectOffset_t780 * L_6 = GUIStyle_get_margin_m7727(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_top_m4411(L_6, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_2-(float)((float)((float)L_4+(float)(((float)((float)L_7)))))));
	}

IL_002d:
	{
		bool L_8 = (__this->___allowVerticalScroll_34);
		if (!L_8)
		{
			goto IL_0139;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinHeight_29);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0139;
		}
	}
	{
		bool L_11 = (__this->___needsHorizontalScrollbar_35);
		if (L_11)
		{
			goto IL_00db;
		}
	}
	{
		bool L_12 = (__this->___needsVerticalScrollbar_36);
		if (L_12)
		{
			goto IL_00db;
		}
	}
	{
		Rect_t30 * L_13 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_14 = Rect_get_width_m370(L_13, /*hidden argument*/NULL);
		GUIStyle_t184 * L_15 = (__this->___verticalScrollbar_38);
		NullCheck(L_15);
		float L_16 = GUIStyle_get_fixedWidth_m7734(L_15, /*hidden argument*/NULL);
		GUIStyle_t184 * L_17 = (__this->___verticalScrollbar_38);
		NullCheck(L_17);
		RectOffset_t780 * L_18 = GUIStyle_get_margin_m7727(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = RectOffset_get_left_m4410(L_18, /*hidden argument*/NULL);
		__this->___clientWidth_31 = ((float)((float)((float)((float)L_14-(float)L_16))-(float)(((float)((float)L_19)))));
		float L_20 = (__this->___clientWidth_31);
		float L_21 = (__this->___calcMinWidth_27);
		if ((!(((float)L_20) < ((float)L_21))))
		{
			goto IL_00a6;
		}
	}
	{
		float L_22 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_22;
	}

IL_00a6:
	{
		Rect_t30 * L_23 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_24 = Rect_get_width_m370(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		Rect_t30 * L_25 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_26 = Rect_get_x_m366(L_25, /*hidden argument*/NULL);
		float L_27 = (__this->___clientWidth_31);
		GUIScrollGroup_SetHorizontal_m7595(__this, L_26, L_27, /*hidden argument*/NULL);
		GUIScrollGroup_CalcHeight_m7596(__this, /*hidden argument*/NULL);
		Rect_t30 * L_28 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_29 = V_1;
		Rect_set_width_m371(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00db:
	{
		float L_30 = (((GUILayoutEntry_t1385 *)__this)->___minHeight_2);
		V_2 = L_30;
		float L_31 = (((GUILayoutEntry_t1385 *)__this)->___maxHeight_3);
		V_3 = L_31;
		float L_32 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_32;
		float L_33 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_33;
		float L_34 = ___y;
		float L_35 = (__this->___calcMinHeight_29);
		GUILayoutGroup_SetVertical_m7591(__this, L_34, L_35, /*hidden argument*/NULL);
		float L_36 = V_2;
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_36;
		float L_37 = V_3;
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_37;
		Rect_t30 * L_38 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_39 = ___height;
		Rect_set_height_m373(L_38, L_39, /*hidden argument*/NULL);
		float L_40 = (__this->___calcMinHeight_29);
		__this->___clientHeight_32 = L_40;
		goto IL_0177;
	}

IL_0139:
	{
		bool L_41 = (__this->___allowVerticalScroll_34);
		if (!L_41)
		{
			goto IL_015c;
		}
	}
	{
		float L_42 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_42;
		float L_43 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_43;
	}

IL_015c:
	{
		float L_44 = ___y;
		float L_45 = V_0;
		GUILayoutGroup_SetVertical_m7591(__this, L_44, L_45, /*hidden argument*/NULL);
		Rect_t30 * L_46 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_47 = ___height;
		Rect_set_height_m373(L_46, L_47, /*hidden argument*/NULL);
		float L_48 = V_0;
		__this->___clientHeight_32 = L_48;
	}

IL_0177:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutEntry_t1385_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern "C" void GUIWordWrapSizer__ctor_m7598 (GUIWordWrapSizer_t1388 * __this, GUIStyle_t184 * ____style, GUIContent_t549 * ____content, GUILayoutOptionU5BU5D_t550* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t1385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(957);
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t184 * L_0 = ____style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t1385_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m7568(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_0, /*hidden argument*/NULL);
		GUIContent_t549 * L_1 = ____content;
		GUIContent_t549 * L_2 = (GUIContent_t549 *)il2cpp_codegen_object_new (GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent__ctor_m7681(L_2, L_1, /*hidden argument*/NULL);
		__this->___content_10 = L_2;
		GUILayoutOptionU5BU5D_t550* L_3 = ___options;
		GUILayoutEntry_ApplyOptions_m7579(__this, L_3, /*hidden argument*/NULL);
		float L_4 = (((GUILayoutEntry_t1385 *)__this)->___minHeight_2);
		__this->___forcedMinHeight_11 = L_4;
		float L_5 = (((GUILayoutEntry_t1385 *)__this)->___maxHeight_3);
		__this->___forcedMaxHeight_12 = L_5;
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m7599 (GUIWordWrapSizer_t1388 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t1385 *)__this)->___minWidth_0);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (((GUILayoutEntry_t1385 *)__this)->___maxWidth_1);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}

IL_0020:
	{
		GUIStyle_t184 * L_2 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		GUIContent_t549 * L_3 = (__this->___content_10);
		NullCheck(L_2);
		GUIStyle_CalcMinMaxWidth_m7757(L_2, L_3, (&V_0), (&V_1), /*hidden argument*/NULL);
		float L_4 = (((GUILayoutEntry_t1385 *)__this)->___minWidth_0);
		if ((!(((float)L_4) == ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		float L_5 = V_0;
		((GUILayoutEntry_t1385 *)__this)->___minWidth_0 = L_5;
	}

IL_004c:
	{
		float L_6 = (((GUILayoutEntry_t1385 *)__this)->___maxWidth_1);
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}
	{
		float L_7 = V_1;
		((GUILayoutEntry_t1385 *)__this)->___maxWidth_1 = L_7;
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m7600 (GUIWordWrapSizer_t1388 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (__this->___forcedMinHeight_11);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (__this->___forcedMaxHeight_12);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}

IL_0020:
	{
		GUIStyle_t184 * L_2 = GUILayoutEntry_get_style_m7571(__this, /*hidden argument*/NULL);
		GUIContent_t549 * L_3 = (__this->___content_10);
		Rect_t30 * L_4 = &(((GUILayoutEntry_t1385 *)__this)->___rect_4);
		float L_5 = Rect_get_width_m370(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_6 = GUIStyle_CalcHeight_m7754(L_2, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (__this->___forcedMinHeight_11);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_8;
		goto IL_0065;
	}

IL_0059:
	{
		float L_9 = (__this->___forcedMinHeight_11);
		((GUILayoutEntry_t1385 *)__this)->___minHeight_2 = L_9;
	}

IL_0065:
	{
		float L_10 = (__this->___forcedMaxHeight_12);
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = V_0;
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_11;
		goto IL_008d;
	}

IL_0081:
	{
		float L_12 = (__this->___forcedMaxHeight_12);
		((GUILayoutEntry_t1385 *)__this)->___maxHeight_3 = L_12;
	}

IL_008d:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutOption::.ctor(UnityEngine.GUILayoutOption/Type,System.Object)
extern "C" void GUILayoutOption__ctor_m7601 (GUILayoutOption_t553 * __this, int32_t ___type, Object_t * ___value, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type;
		__this->___type_0 = L_0;
		Object_t * L_1 = ___value;
		__this->___value_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.GUIUtility::.cctor()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern "C" void GUIUtility__cctor_m7602 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t29  L_0 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		((GUIUtility_t1392_StaticFields*)GUIUtility_t1392_il2cpp_TypeInfo_var->static_fields)->___s_EditorScreenPointOffset_2 = L_0;
		((GUIUtility_t1392_StaticFields*)GUIUtility_t1392_il2cpp_TypeInfo_var->static_fields)->___s_HasKeyboardFocus_3 = 0;
		return;
	}
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(UnityEngine.FocusType)
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_GetControlID_m7603 (Object_t * __this /* static, unused */, int32_t ___focus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___focus;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		int32_t L_1 = GUIUtility_GetControlID_m7604(NULL /*static, unused*/, 0, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)
extern "C" int32_t GUIUtility_GetControlID_m7604 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focus, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_GetControlID_m7604_ftn) (int32_t, int32_t);
	static GUIUtility_GetControlID_m7604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_GetControlID_m7604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)");
	return _il2cpp_icall_func(___hint, ___focus);
}
// System.Object UnityEngine.GUIUtility::GetStateObject(System.Type,System.Int32)
extern TypeInfo* GUIStateObjects_t1260_il2cpp_TypeInfo_var;
extern "C" Object_t * GUIUtility_GetStateObject_m7605 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___controlID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStateObjects_t1260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(908);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___t;
		int32_t L_1 = ___controlID;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t1260_il2cpp_TypeInfo_var);
		Object_t * L_2 = GUIStateObjects_GetStateObject_m6935(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.GUIUtility::set_hotControl(System.Int32)
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_set_hotControl_m7606 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_Internal_SetHotControl_m7607(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)
extern "C" void GUIUtility_Internal_SetHotControl_m7607 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_Internal_SetHotControl_m7607_ftn) (int32_t);
	static GUIUtility_Internal_SetHotControl_m7607_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_SetHotControl_m7607_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.Int32 UnityEngine.GUIUtility::get_keyboardControl()
extern "C" int32_t GUIUtility_get_keyboardControl_m7608 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_get_keyboardControl_m7608_ftn) ();
	static GUIUtility_get_keyboardControl_m7608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_keyboardControl_m7608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_keyboardControl()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_keyboardControl(System.Int32)
extern "C" void GUIUtility_set_keyboardControl_m7609 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_keyboardControl_m7609_ftn) (int32_t);
	static GUIUtility_set_keyboardControl_m7609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_keyboardControl_m7609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_keyboardControl(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
extern "C" String_t* GUIUtility_get_systemCopyBuffer_m7610 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GUIUtility_get_systemCopyBuffer_m7610_ftn) ();
	static GUIUtility_get_systemCopyBuffer_m7610_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_systemCopyBuffer_m7610_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_systemCopyBuffer()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
extern "C" void GUIUtility_set_systemCopyBuffer_m7611 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_systemCopyBuffer_m7611_ftn) (String_t*);
	static GUIUtility_set_systemCopyBuffer_m7611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_systemCopyBuffer_m7611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::GetDefaultSkin()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern "C" GUISkin_t547 * GUIUtility_GetDefaultSkin_m7612 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUIUtility_t1392_StaticFields*)GUIUtility_t1392_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0;
		GUISkin_t547 * L_1 = GUIUtility_Internal_GetDefaultSkin_m7613(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
extern "C" GUISkin_t547 * GUIUtility_Internal_GetDefaultSkin_m7613 (Object_t * __this /* static, unused */, int32_t ___skinMode, const MethodInfo* method)
{
	typedef GUISkin_t547 * (*GUIUtility_Internal_GetDefaultSkin_m7613_ftn) (int32_t);
	static GUIUtility_Internal_GetDefaultSkin_m7613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetDefaultSkin_m7613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)");
	return _il2cpp_icall_func(___skinMode);
}
// System.Void UnityEngine.GUIUtility::BeginGUI(System.Int32,System.Int32,System.Int32)
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_BeginGUI_m7614 (Object_t * __this /* static, unused */, int32_t ___skinMode, int32_t ___instanceID, int32_t ___useGUILayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___skinMode;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		((GUIUtility_t1392_StaticFields*)GUIUtility_t1392_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0 = L_0;
		int32_t L_1 = ___instanceID;
		((GUIUtility_t1392_StaticFields*)GUIUtility_t1392_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_set_skin_m7510(NULL /*static, unused*/, (GUISkin_t547 *)NULL, /*hidden argument*/NULL);
		int32_t L_2 = ___useGUILayout;
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m7548(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = ___instanceID;
		GUILayoutUtility_Begin_m7549(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_set_changed_m7515(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
extern "C" void GUIUtility_Internal_ExitGUI_m7615 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUIUtility_Internal_ExitGUI_m7615_ftn) ();
	static GUIUtility_Internal_ExitGUI_m7615_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_ExitGUI_m7615_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_ExitGUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::EndGUI(System.Int32)
extern TypeInfo* GUILayoutUtility_t551_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_EndGUI_m7616 (Object_t * __this /* static, unused */, int32_t ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Event_t725 * L_0 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_0);
			int32_t L_1 = Event_get_type_m7773(L_0, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_1) == ((uint32_t)8))))
			{
				goto IL_0042;
			}
		}

IL_0010:
		{
			int32_t L_2 = ___layoutType;
			V_0 = L_2;
			int32_t L_3 = V_0;
			if (L_3 == 0)
			{
				goto IL_0029;
			}
			if (L_3 == 1)
			{
				goto IL_002e;
			}
			if (L_3 == 2)
			{
				goto IL_0038;
			}
		}

IL_0024:
		{
			goto IL_0042;
		}

IL_0029:
		{
			goto IL_0042;
		}

IL_002e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUILayoutUtility_Layout_m7552(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutFromEditorWindow_m7553(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0042:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
			int32_t L_4 = ((GUIUtility_t1392_StaticFields*)GUIUtility_t1392_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t551_il2cpp_TypeInfo_var);
			GUILayoutUtility_SelectIDList_m7548(NULL /*static, unused*/, L_4, 0, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
			GUIContent_ClearStaticCache_m7686(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x5E, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m7615(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUIUtility::EndGUIFromException(System.Exception)
extern TypeInfo* ExitGUIException_t1390_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern "C" bool GUIUtility_EndGUIFromException_m7617 (Object_t * __this /* static, unused */, Exception_t40 * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExitGUIException_t1390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t40 * L_0 = ___exception;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Exception_t40 * L_1 = ___exception;
		if (((ExitGUIException_t1390 *)IsInstSealed(L_1, ExitGUIException_t1390_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		Exception_t40 * L_2 = ___exception;
		NullCheck(L_2);
		Exception_t40 * L_3 = (Exception_t40 *)VirtFuncInvoker0< Exception_t40 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_2);
		if (((ExitGUIException_t1390 *)IsInstSealed(L_3, ExitGUIException_t1390_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		return 0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m7615(NULL /*static, unused*/, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityEngine.GUIUtility::CheckOnGUI()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1192;
extern "C" void GUIUtility_CheckOnGUI_m7618 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		_stringLiteral1192 = il2cpp_codegen_string_literal_from_index(1192);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_Internal_GetGUIDepth_m7619(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t513 * L_1 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_1, _stringLiteral1192, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
extern "C" int32_t GUIUtility_Internal_GetGUIDepth_m7619 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_Internal_GetGUIDepth_m7619_ftn) ();
	static GUIUtility_Internal_GetGUIDepth_m7619_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetGUIDepth_m7619_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetGUIDepth()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)
extern "C" void GUIUtility_set_mouseUsed_m7620 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_mouseUsed_m7620_ftn) (bool);
	static GUIUtility_set_mouseUsed_m7620_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_mouseUsed_m7620_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUIClip::Push(UnityEngine.Rect,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" void GUIClip_Push_m7621 (Object_t * __this /* static, unused */, Rect_t30  ___screenRect, Vector2_t29  ___scrollOffset, Vector2_t29  ___renderOffset, bool ___resetOffset, const MethodInfo* method)
{
	{
		bool L_0 = ___resetOffset;
		GUIClip_INTERNAL_CALL_Push_m7622(NULL /*static, unused*/, (&___screenRect), (&___scrollOffset), (&___renderOffset), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
extern "C" void GUIClip_INTERNAL_CALL_Push_m7622 (Object_t * __this /* static, unused */, Rect_t30 * ___screenRect, Vector2_t29 * ___scrollOffset, Vector2_t29 * ___renderOffset, bool ___resetOffset, const MethodInfo* method)
{
	typedef void (*GUIClip_INTERNAL_CALL_Push_m7622_ftn) (Rect_t30 *, Vector2_t29 *, Vector2_t29 *, bool);
	static GUIClip_INTERNAL_CALL_Push_m7622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIClip_INTERNAL_CALL_Push_m7622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)");
	_il2cpp_icall_func(___screenRect, ___scrollOffset, ___renderOffset, ___resetOffset);
}
// System.Void UnityEngine.GUIClip::Pop()
extern "C" void GUIClip_Pop_m7623 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUIClip_Pop_m7623_ftn) ();
	static GUIClip_Pop_m7623_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIClip_Pop_m7623_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIClip::Pop()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GUISettings::.ctor()
extern "C" void GUISettings__ctor_m7624 (GUISettings_t1394 * __this, const MethodInfo* method)
{
	{
		__this->___m_DoubleClickSelectsWord_0 = 1;
		__this->___m_TripleClickSelectsLine_1 = 1;
		Color_t5  L_0 = Color_get_white_m283(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_CursorColor_2 = L_0;
		__this->___m_CursorFlashSpeed_3 = (-1.0f);
		Color_t5  L_1 = {0};
		Color__ctor_m6489(&L_1, (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		__this->___m_SelectionColor_4 = L_1;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void SkinChangedDelegate__ctor_m7625 (SkinChangedDelegate_t1395 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::Invoke()
extern "C" void SkinChangedDelegate_Invoke_m7626 (SkinChangedDelegate_t1395 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SkinChangedDelegate_Invoke_m7626((SkinChangedDelegate_t1395 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t1395(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.GUISkin/SkinChangedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * SkinChangedDelegate_BeginInvoke_m7627 (SkinChangedDelegate_t1395 * __this, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void SkinChangedDelegate_EndInvoke_m7628 (SkinChangedDelegate_t1395 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.GUISkin::.ctor()
extern TypeInfo* GUISettings_t1394_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyleU5BU5D_t1396_il2cpp_TypeInfo_var;
extern "C" void GUISkin__ctor_m7629 (GUISkin_t547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISettings_t1394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(963);
		GUIStyleU5BU5D_t1396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(964);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISettings_t1394 * L_0 = (GUISettings_t1394 *)il2cpp_codegen_object_new (GUISettings_t1394_il2cpp_TypeInfo_var);
		GUISettings__ctor_m7624(L_0, /*hidden argument*/NULL);
		__this->___m_Settings_23 = L_0;
		ScriptableObject__ctor_m2161(__this, /*hidden argument*/NULL);
		__this->___m_CustomStyles_22 = ((GUIStyleU5BU5D_t1396*)SZArrayNew(GUIStyleU5BU5D_t1396_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Void UnityEngine.GUISkin::OnEnable()
extern TypeInfo* Enumerator_t1471_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m8185_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m8186_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8187_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8188_MethodInfo_var;
extern "C" void GUISkin_OnEnable_m7630 (GUISkin_t547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1471_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(965);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		Dictionary_2_get_Values_m8185_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484725);
		ValueCollection_GetEnumerator_m8186_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484726);
		Enumerator_get_Current_m8187_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484727);
		Enumerator_MoveNext_m8188_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484728);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t184 * V_0 = {0};
	Enumerator_t1471  V_1 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_0 = (__this->___styles_25);
		NullCheck(L_0);
		ValueCollection_t1472 * L_1 = Dictionary_2_get_Values_m8185(L_0, /*hidden argument*/Dictionary_2_get_Values_m8185_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t1471  L_2 = ValueCollection_GetEnumerator_m8186(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m8186_MethodInfo_var);
		V_1 = L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002a;
		}

IL_001c:
		{
			GUIStyle_t184 * L_3 = Enumerator_get_Current_m8187((&V_1), /*hidden argument*/Enumerator_get_Current_m8187_MethodInfo_var);
			V_0 = L_3;
			GUIStyle_t184 * L_4 = V_0;
			NullCheck(L_4);
			GUIStyle_CreateObjectReferences_m7712(L_4, /*hidden argument*/NULL);
		}

IL_002a:
		{
			bool L_5 = Enumerator_MoveNext_m8188((&V_1), /*hidden argument*/Enumerator_MoveNext_m8188_MethodInfo_var);
			if (L_5)
			{
				goto IL_001c;
			}
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x47, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		Enumerator_t1471  L_6 = V_1;
		Enumerator_t1471  L_7 = L_6;
		Object_t * L_8 = Box(Enumerator_t1471_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_8);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0047:
	{
		return;
	}
}
// UnityEngine.Font UnityEngine.GUISkin::get_font()
extern "C" Font_t684 * GUISkin_get_font_m7631 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		Font_t684 * L_0 = (__this->___m_Font_1);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_font(UnityEngine.Font)
extern TypeInfo* GUISkin_t547_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUISkin_set_font_m7632 (GUISkin_t547 * __this, Font_t684 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t547_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t684 * L_0 = ___value;
		__this->___m_Font_1 = L_0;
		GUISkin_t547 * L_1 = ((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___current_27;
		bool L_2 = Object_op_Equality_m427(NULL /*static, unused*/, L_1, __this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Font_t684 * L_3 = (__this->___m_Font_1);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m7747(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_box()
extern "C" GUIStyle_t184 * GUISkin_get_box_m7633 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_box_2);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_box(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_box_m7634 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_box_2 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
extern "C" GUIStyle_t184 * GUISkin_get_label_m7635 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_label_5);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_label(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_label_m7636 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_label_5 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textField()
extern "C" GUIStyle_t184 * GUISkin_get_textField_m7637 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_textField_6);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textField(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textField_m7638 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_textField_6 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textArea()
extern "C" GUIStyle_t184 * GUISkin_get_textArea_m2492 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_textArea_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textArea(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textArea_m7639 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_textArea_7 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_button()
extern "C" GUIStyle_t184 * GUISkin_get_button_m2482 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_button_3);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_button(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_button_m7640 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_button_3 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_toggle()
extern "C" GUIStyle_t184 * GUISkin_get_toggle_m7641 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_toggle_4);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_toggle(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_toggle_m7642 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_toggle_4 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_window()
extern "C" GUIStyle_t184 * GUISkin_get_window_m2470 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_window_8);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_window(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_window_m7643 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_window_8 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSlider()
extern "C" GUIStyle_t184 * GUISkin_get_horizontalSlider_m7644 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_horizontalSlider_9);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSlider_m7645 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_horizontalSlider_9 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSliderThumb()
extern "C" GUIStyle_t184 * GUISkin_get_horizontalSliderThumb_m7646 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_horizontalSliderThumb_10);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSliderThumb_m7647 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_horizontalSliderThumb_10 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSlider()
extern "C" GUIStyle_t184 * GUISkin_get_verticalSlider_m7648 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_verticalSlider_11);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSlider_m7649 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_verticalSlider_11 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSliderThumb()
extern "C" GUIStyle_t184 * GUISkin_get_verticalSliderThumb_m7650 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_verticalSliderThumb_12);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSliderThumb_m7651 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_verticalSliderThumb_12 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbar()
extern "C" GUIStyle_t184 * GUISkin_get_horizontalScrollbar_m7652 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_horizontalScrollbar_13);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbar_m7653 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_horizontalScrollbar_13 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarThumb()
extern "C" GUIStyle_t184 * GUISkin_get_horizontalScrollbarThumb_m7654 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_horizontalScrollbarThumb_14);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarThumb_m7655 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_horizontalScrollbarThumb_14 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarLeftButton()
extern "C" GUIStyle_t184 * GUISkin_get_horizontalScrollbarLeftButton_m7656 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_horizontalScrollbarLeftButton_15);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarLeftButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m7657 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_horizontalScrollbarLeftButton_15 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarRightButton()
extern "C" GUIStyle_t184 * GUISkin_get_horizontalScrollbarRightButton_m7658 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_horizontalScrollbarRightButton_16);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarRightButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m7659 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_horizontalScrollbarRightButton_16 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbar()
extern "C" GUIStyle_t184 * GUISkin_get_verticalScrollbar_m7660 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_verticalScrollbar_17);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbar_m7661 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_verticalScrollbar_17 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarThumb()
extern "C" GUIStyle_t184 * GUISkin_get_verticalScrollbarThumb_m7662 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_verticalScrollbarThumb_18);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarThumb_m7663 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_verticalScrollbarThumb_18 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarUpButton()
extern "C" GUIStyle_t184 * GUISkin_get_verticalScrollbarUpButton_m7664 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_verticalScrollbarUpButton_19);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarUpButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarUpButton_m7665 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_verticalScrollbarUpButton_19 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarDownButton()
extern "C" GUIStyle_t184 * GUISkin_get_verticalScrollbarDownButton_m7666 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_verticalScrollbarDownButton_20);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarDownButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarDownButton_m7667 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_verticalScrollbarDownButton_20 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_scrollView()
extern "C" GUIStyle_t184 * GUISkin_get_scrollView_m7668 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = (__this->___m_ScrollView_21);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_scrollView(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_scrollView_m7669 (GUISkin_t547 * __this, GUIStyle_t184 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t184 * L_0 = ___value;
		__this->___m_ScrollView_21 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle[] UnityEngine.GUISkin::get_customStyles()
extern "C" GUIStyleU5BU5D_t1396* GUISkin_get_customStyles_m7670 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t1396* L_0 = (__this->___m_CustomStyles_22);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_customStyles(UnityEngine.GUIStyle[])
extern "C" void GUISkin_set_customStyles_m7671 (GUISkin_t547 * __this, GUIStyleU5BU5D_t1396* ___value, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t1396* L_0 = ___value;
		__this->___m_CustomStyles_22 = L_0;
		GUISkin_Apply_m7674(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISettings UnityEngine.GUISkin::get_settings()
extern "C" GUISettings_t1394 * GUISkin_get_settings_m7672 (GUISkin_t547 * __this, const MethodInfo* method)
{
	{
		GUISettings_t1394 * L_0 = (__this->___m_Settings_23);
		return L_0;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_error()
extern TypeInfo* GUISkin_t547_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t184 * GUISkin_get_error_m7673 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t547_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t184 * L_0 = ((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___ms_Error_24;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t184 * L_1 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_1, /*hidden argument*/NULL);
		((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___ms_Error_24 = L_1;
	}

IL_0014:
	{
		GUIStyle_t184 * L_2 = ((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___ms_Error_24;
		return L_2;
	}
}
// System.Void UnityEngine.GUISkin::Apply()
extern Il2CppCodeGenString* _stringLiteral1193;
extern "C" void GUISkin_Apply_m7674 (GUISkin_t547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1193 = il2cpp_codegen_string_literal_from_index(1193);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleU5BU5D_t1396* L_0 = (__this->___m_CustomStyles_22);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral1193, /*hidden argument*/NULL);
	}

IL_0015:
	{
		GUISkin_BuildStyleCache_m7675(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUISkin::BuildStyleCache()
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern TypeInfo* StringComparer_t1473_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1397_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m8190_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1147;
extern Il2CppCodeGenString* _stringLiteral1194;
extern Il2CppCodeGenString* _stringLiteral1195;
extern Il2CppCodeGenString* _stringLiteral1196;
extern Il2CppCodeGenString* _stringLiteral1197;
extern Il2CppCodeGenString* _stringLiteral1198;
extern Il2CppCodeGenString* _stringLiteral1199;
extern Il2CppCodeGenString* _stringLiteral1200;
extern Il2CppCodeGenString* _stringLiteral1201;
extern Il2CppCodeGenString* _stringLiteral1202;
extern Il2CppCodeGenString* _stringLiteral1203;
extern Il2CppCodeGenString* _stringLiteral1204;
extern Il2CppCodeGenString* _stringLiteral1205;
extern Il2CppCodeGenString* _stringLiteral1206;
extern Il2CppCodeGenString* _stringLiteral1207;
extern Il2CppCodeGenString* _stringLiteral1208;
extern Il2CppCodeGenString* _stringLiteral1209;
extern Il2CppCodeGenString* _stringLiteral1210;
extern Il2CppCodeGenString* _stringLiteral1211;
extern Il2CppCodeGenString* _stringLiteral1212;
extern "C" void GUISkin_BuildStyleCache_m7675 (GUISkin_t547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		StringComparer_t1473_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(966);
		Dictionary_2_t1397_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(967);
		Dictionary_2__ctor_m8190_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484729);
		_stringLiteral1147 = il2cpp_codegen_string_literal_from_index(1147);
		_stringLiteral1194 = il2cpp_codegen_string_literal_from_index(1194);
		_stringLiteral1195 = il2cpp_codegen_string_literal_from_index(1195);
		_stringLiteral1196 = il2cpp_codegen_string_literal_from_index(1196);
		_stringLiteral1197 = il2cpp_codegen_string_literal_from_index(1197);
		_stringLiteral1198 = il2cpp_codegen_string_literal_from_index(1198);
		_stringLiteral1199 = il2cpp_codegen_string_literal_from_index(1199);
		_stringLiteral1200 = il2cpp_codegen_string_literal_from_index(1200);
		_stringLiteral1201 = il2cpp_codegen_string_literal_from_index(1201);
		_stringLiteral1202 = il2cpp_codegen_string_literal_from_index(1202);
		_stringLiteral1203 = il2cpp_codegen_string_literal_from_index(1203);
		_stringLiteral1204 = il2cpp_codegen_string_literal_from_index(1204);
		_stringLiteral1205 = il2cpp_codegen_string_literal_from_index(1205);
		_stringLiteral1206 = il2cpp_codegen_string_literal_from_index(1206);
		_stringLiteral1207 = il2cpp_codegen_string_literal_from_index(1207);
		_stringLiteral1208 = il2cpp_codegen_string_literal_from_index(1208);
		_stringLiteral1209 = il2cpp_codegen_string_literal_from_index(1209);
		_stringLiteral1210 = il2cpp_codegen_string_literal_from_index(1210);
		_stringLiteral1211 = il2cpp_codegen_string_literal_from_index(1211);
		_stringLiteral1212 = il2cpp_codegen_string_literal_from_index(1212);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIStyle_t184 * L_0 = (__this->___m_box_2);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t184 * L_1 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_1, /*hidden argument*/NULL);
		__this->___m_box_2 = L_1;
	}

IL_0016:
	{
		GUIStyle_t184 * L_2 = (__this->___m_button_3);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		GUIStyle_t184 * L_3 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_3, /*hidden argument*/NULL);
		__this->___m_button_3 = L_3;
	}

IL_002c:
	{
		GUIStyle_t184 * L_4 = (__this->___m_toggle_4);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t184 * L_5 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_5, /*hidden argument*/NULL);
		__this->___m_toggle_4 = L_5;
	}

IL_0042:
	{
		GUIStyle_t184 * L_6 = (__this->___m_label_5);
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t184 * L_7 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_7, /*hidden argument*/NULL);
		__this->___m_label_5 = L_7;
	}

IL_0058:
	{
		GUIStyle_t184 * L_8 = (__this->___m_window_8);
		if (L_8)
		{
			goto IL_006e;
		}
	}
	{
		GUIStyle_t184 * L_9 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_9, /*hidden argument*/NULL);
		__this->___m_window_8 = L_9;
	}

IL_006e:
	{
		GUIStyle_t184 * L_10 = (__this->___m_textField_6);
		if (L_10)
		{
			goto IL_0084;
		}
	}
	{
		GUIStyle_t184 * L_11 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_11, /*hidden argument*/NULL);
		__this->___m_textField_6 = L_11;
	}

IL_0084:
	{
		GUIStyle_t184 * L_12 = (__this->___m_textArea_7);
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		GUIStyle_t184 * L_13 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_13, /*hidden argument*/NULL);
		__this->___m_textArea_7 = L_13;
	}

IL_009a:
	{
		GUIStyle_t184 * L_14 = (__this->___m_horizontalSlider_9);
		if (L_14)
		{
			goto IL_00b0;
		}
	}
	{
		GUIStyle_t184 * L_15 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_15, /*hidden argument*/NULL);
		__this->___m_horizontalSlider_9 = L_15;
	}

IL_00b0:
	{
		GUIStyle_t184 * L_16 = (__this->___m_horizontalSliderThumb_10);
		if (L_16)
		{
			goto IL_00c6;
		}
	}
	{
		GUIStyle_t184 * L_17 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_17, /*hidden argument*/NULL);
		__this->___m_horizontalSliderThumb_10 = L_17;
	}

IL_00c6:
	{
		GUIStyle_t184 * L_18 = (__this->___m_verticalSlider_11);
		if (L_18)
		{
			goto IL_00dc;
		}
	}
	{
		GUIStyle_t184 * L_19 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_19, /*hidden argument*/NULL);
		__this->___m_verticalSlider_11 = L_19;
	}

IL_00dc:
	{
		GUIStyle_t184 * L_20 = (__this->___m_verticalSliderThumb_12);
		if (L_20)
		{
			goto IL_00f2;
		}
	}
	{
		GUIStyle_t184 * L_21 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_21, /*hidden argument*/NULL);
		__this->___m_verticalSliderThumb_12 = L_21;
	}

IL_00f2:
	{
		GUIStyle_t184 * L_22 = (__this->___m_horizontalScrollbar_13);
		if (L_22)
		{
			goto IL_0108;
		}
	}
	{
		GUIStyle_t184 * L_23 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_23, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbar_13 = L_23;
	}

IL_0108:
	{
		GUIStyle_t184 * L_24 = (__this->___m_horizontalScrollbarThumb_14);
		if (L_24)
		{
			goto IL_011e;
		}
	}
	{
		GUIStyle_t184 * L_25 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_25, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarThumb_14 = L_25;
	}

IL_011e:
	{
		GUIStyle_t184 * L_26 = (__this->___m_horizontalScrollbarLeftButton_15);
		if (L_26)
		{
			goto IL_0134;
		}
	}
	{
		GUIStyle_t184 * L_27 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_27, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarLeftButton_15 = L_27;
	}

IL_0134:
	{
		GUIStyle_t184 * L_28 = (__this->___m_horizontalScrollbarRightButton_16);
		if (L_28)
		{
			goto IL_014a;
		}
	}
	{
		GUIStyle_t184 * L_29 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_29, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarRightButton_16 = L_29;
	}

IL_014a:
	{
		GUIStyle_t184 * L_30 = (__this->___m_verticalScrollbar_17);
		if (L_30)
		{
			goto IL_0160;
		}
	}
	{
		GUIStyle_t184 * L_31 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_31, /*hidden argument*/NULL);
		__this->___m_verticalScrollbar_17 = L_31;
	}

IL_0160:
	{
		GUIStyle_t184 * L_32 = (__this->___m_verticalScrollbarThumb_18);
		if (L_32)
		{
			goto IL_0176;
		}
	}
	{
		GUIStyle_t184 * L_33 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_33, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarThumb_18 = L_33;
	}

IL_0176:
	{
		GUIStyle_t184 * L_34 = (__this->___m_verticalScrollbarUpButton_19);
		if (L_34)
		{
			goto IL_018c;
		}
	}
	{
		GUIStyle_t184 * L_35 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_35, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarUpButton_19 = L_35;
	}

IL_018c:
	{
		GUIStyle_t184 * L_36 = (__this->___m_verticalScrollbarDownButton_20);
		if (L_36)
		{
			goto IL_01a2;
		}
	}
	{
		GUIStyle_t184 * L_37 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_37, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarDownButton_20 = L_37;
	}

IL_01a2:
	{
		GUIStyle_t184 * L_38 = (__this->___m_ScrollView_21);
		if (L_38)
		{
			goto IL_01b8;
		}
	}
	{
		GUIStyle_t184 * L_39 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_39, /*hidden argument*/NULL);
		__this->___m_ScrollView_21 = L_39;
	}

IL_01b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1473_il2cpp_TypeInfo_var);
		StringComparer_t1473 * L_40 = StringComparer_get_OrdinalIgnoreCase_m8189(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_41 = (Dictionary_2_t1397 *)il2cpp_codegen_object_new (Dictionary_2_t1397_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m8190(L_41, L_40, /*hidden argument*/Dictionary_2__ctor_m8190_MethodInfo_var);
		__this->___styles_25 = L_41;
		Dictionary_2_t1397 * L_42 = (__this->___styles_25);
		GUIStyle_t184 * L_43 = (__this->___m_box_2);
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_42, _stringLiteral1147, L_43);
		GUIStyle_t184 * L_44 = (__this->___m_box_2);
		NullCheck(L_44);
		GUIStyle_set_name_m7717(L_44, _stringLiteral1147, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_45 = (__this->___styles_25);
		GUIStyle_t184 * L_46 = (__this->___m_button_3);
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_45, _stringLiteral1194, L_46);
		GUIStyle_t184 * L_47 = (__this->___m_button_3);
		NullCheck(L_47);
		GUIStyle_set_name_m7717(L_47, _stringLiteral1194, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_48 = (__this->___styles_25);
		GUIStyle_t184 * L_49 = (__this->___m_toggle_4);
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_48, _stringLiteral1195, L_49);
		GUIStyle_t184 * L_50 = (__this->___m_toggle_4);
		NullCheck(L_50);
		GUIStyle_set_name_m7717(L_50, _stringLiteral1195, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_51 = (__this->___styles_25);
		GUIStyle_t184 * L_52 = (__this->___m_label_5);
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_51, _stringLiteral1196, L_52);
		GUIStyle_t184 * L_53 = (__this->___m_label_5);
		NullCheck(L_53);
		GUIStyle_set_name_m7717(L_53, _stringLiteral1196, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_54 = (__this->___styles_25);
		GUIStyle_t184 * L_55 = (__this->___m_window_8);
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_54, _stringLiteral1197, L_55);
		GUIStyle_t184 * L_56 = (__this->___m_window_8);
		NullCheck(L_56);
		GUIStyle_set_name_m7717(L_56, _stringLiteral1197, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_57 = (__this->___styles_25);
		GUIStyle_t184 * L_58 = (__this->___m_textField_6);
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_57, _stringLiteral1198, L_58);
		GUIStyle_t184 * L_59 = (__this->___m_textField_6);
		NullCheck(L_59);
		GUIStyle_set_name_m7717(L_59, _stringLiteral1198, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_60 = (__this->___styles_25);
		GUIStyle_t184 * L_61 = (__this->___m_textArea_7);
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_60, _stringLiteral1199, L_61);
		GUIStyle_t184 * L_62 = (__this->___m_textArea_7);
		NullCheck(L_62);
		GUIStyle_set_name_m7717(L_62, _stringLiteral1199, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_63 = (__this->___styles_25);
		GUIStyle_t184 * L_64 = (__this->___m_horizontalSlider_9);
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_63, _stringLiteral1200, L_64);
		GUIStyle_t184 * L_65 = (__this->___m_horizontalSlider_9);
		NullCheck(L_65);
		GUIStyle_set_name_m7717(L_65, _stringLiteral1200, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_66 = (__this->___styles_25);
		GUIStyle_t184 * L_67 = (__this->___m_horizontalSliderThumb_10);
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_66, _stringLiteral1201, L_67);
		GUIStyle_t184 * L_68 = (__this->___m_horizontalSliderThumb_10);
		NullCheck(L_68);
		GUIStyle_set_name_m7717(L_68, _stringLiteral1201, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_69 = (__this->___styles_25);
		GUIStyle_t184 * L_70 = (__this->___m_verticalSlider_11);
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_69, _stringLiteral1202, L_70);
		GUIStyle_t184 * L_71 = (__this->___m_verticalSlider_11);
		NullCheck(L_71);
		GUIStyle_set_name_m7717(L_71, _stringLiteral1202, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_72 = (__this->___styles_25);
		GUIStyle_t184 * L_73 = (__this->___m_verticalSliderThumb_12);
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_72, _stringLiteral1203, L_73);
		GUIStyle_t184 * L_74 = (__this->___m_verticalSliderThumb_12);
		NullCheck(L_74);
		GUIStyle_set_name_m7717(L_74, _stringLiteral1203, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_75 = (__this->___styles_25);
		GUIStyle_t184 * L_76 = (__this->___m_horizontalScrollbar_13);
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_75, _stringLiteral1204, L_76);
		GUIStyle_t184 * L_77 = (__this->___m_horizontalScrollbar_13);
		NullCheck(L_77);
		GUIStyle_set_name_m7717(L_77, _stringLiteral1204, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_78 = (__this->___styles_25);
		GUIStyle_t184 * L_79 = (__this->___m_horizontalScrollbarThumb_14);
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_78, _stringLiteral1205, L_79);
		GUIStyle_t184 * L_80 = (__this->___m_horizontalScrollbarThumb_14);
		NullCheck(L_80);
		GUIStyle_set_name_m7717(L_80, _stringLiteral1205, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_81 = (__this->___styles_25);
		GUIStyle_t184 * L_82 = (__this->___m_horizontalScrollbarLeftButton_15);
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_81, _stringLiteral1206, L_82);
		GUIStyle_t184 * L_83 = (__this->___m_horizontalScrollbarLeftButton_15);
		NullCheck(L_83);
		GUIStyle_set_name_m7717(L_83, _stringLiteral1206, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_84 = (__this->___styles_25);
		GUIStyle_t184 * L_85 = (__this->___m_horizontalScrollbarRightButton_16);
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_84, _stringLiteral1207, L_85);
		GUIStyle_t184 * L_86 = (__this->___m_horizontalScrollbarRightButton_16);
		NullCheck(L_86);
		GUIStyle_set_name_m7717(L_86, _stringLiteral1207, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_87 = (__this->___styles_25);
		GUIStyle_t184 * L_88 = (__this->___m_verticalScrollbar_17);
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_87, _stringLiteral1208, L_88);
		GUIStyle_t184 * L_89 = (__this->___m_verticalScrollbar_17);
		NullCheck(L_89);
		GUIStyle_set_name_m7717(L_89, _stringLiteral1208, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_90 = (__this->___styles_25);
		GUIStyle_t184 * L_91 = (__this->___m_verticalScrollbarThumb_18);
		NullCheck(L_90);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_90, _stringLiteral1209, L_91);
		GUIStyle_t184 * L_92 = (__this->___m_verticalScrollbarThumb_18);
		NullCheck(L_92);
		GUIStyle_set_name_m7717(L_92, _stringLiteral1209, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_93 = (__this->___styles_25);
		GUIStyle_t184 * L_94 = (__this->___m_verticalScrollbarUpButton_19);
		NullCheck(L_93);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_93, _stringLiteral1210, L_94);
		GUIStyle_t184 * L_95 = (__this->___m_verticalScrollbarUpButton_19);
		NullCheck(L_95);
		GUIStyle_set_name_m7717(L_95, _stringLiteral1210, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_96 = (__this->___styles_25);
		GUIStyle_t184 * L_97 = (__this->___m_verticalScrollbarDownButton_20);
		NullCheck(L_96);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_96, _stringLiteral1211, L_97);
		GUIStyle_t184 * L_98 = (__this->___m_verticalScrollbarDownButton_20);
		NullCheck(L_98);
		GUIStyle_set_name_m7717(L_98, _stringLiteral1211, /*hidden argument*/NULL);
		Dictionary_2_t1397 * L_99 = (__this->___styles_25);
		GUIStyle_t184 * L_100 = (__this->___m_ScrollView_21);
		NullCheck(L_99);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_99, _stringLiteral1212, L_100);
		GUIStyle_t184 * L_101 = (__this->___m_ScrollView_21);
		NullCheck(L_101);
		GUIStyle_set_name_m7717(L_101, _stringLiteral1212, /*hidden argument*/NULL);
		GUIStyleU5BU5D_t1396* L_102 = (__this->___m_CustomStyles_22);
		if (!L_102)
		{
			goto IL_0516;
		}
	}
	{
		V_0 = 0;
		goto IL_0508;
	}

IL_04d2:
	{
		GUIStyleU5BU5D_t1396* L_103 = (__this->___m_CustomStyles_22);
		int32_t L_104 = V_0;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		int32_t L_105 = L_104;
		if ((*(GUIStyle_t184 **)(GUIStyle_t184 **)SZArrayLdElema(L_103, L_105, sizeof(GUIStyle_t184 *))))
		{
			goto IL_04e4;
		}
	}
	{
		goto IL_0504;
	}

IL_04e4:
	{
		Dictionary_2_t1397 * L_106 = (__this->___styles_25);
		GUIStyleU5BU5D_t1396* L_107 = (__this->___m_CustomStyles_22);
		int32_t L_108 = V_0;
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, L_108);
		int32_t L_109 = L_108;
		NullCheck((*(GUIStyle_t184 **)(GUIStyle_t184 **)SZArrayLdElema(L_107, L_109, sizeof(GUIStyle_t184 *))));
		String_t* L_110 = GUIStyle_get_name_m7716((*(GUIStyle_t184 **)(GUIStyle_t184 **)SZArrayLdElema(L_107, L_109, sizeof(GUIStyle_t184 *))), /*hidden argument*/NULL);
		GUIStyleU5BU5D_t1396* L_111 = (__this->___m_CustomStyles_22);
		int32_t L_112 = V_0;
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, L_112);
		int32_t L_113 = L_112;
		NullCheck(L_106);
		VirtActionInvoker2< String_t*, GUIStyle_t184 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_106, L_110, (*(GUIStyle_t184 **)(GUIStyle_t184 **)SZArrayLdElema(L_111, L_113, sizeof(GUIStyle_t184 *))));
	}

IL_0504:
	{
		int32_t L_114 = V_0;
		V_0 = ((int32_t)((int32_t)L_114+(int32_t)1));
	}

IL_0508:
	{
		int32_t L_115 = V_0;
		GUIStyleU5BU5D_t1396* L_116 = (__this->___m_CustomStyles_22);
		NullCheck(L_116);
		if ((((int32_t)L_115) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_116)->max_length)))))))
		{
			goto IL_04d2;
		}
	}

IL_0516:
	{
		GUIStyle_t184 * L_117 = GUISkin_get_error_m7673(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_117);
		GUIStyle_set_stretchHeight_m7739(L_117, 1, /*hidden argument*/NULL);
		GUIStyle_t184 * L_118 = GUISkin_get_error_m7673(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_118);
		GUIStyleState_t504 * L_119 = GUIStyle_get_normal_m2291(L_118, /*hidden argument*/NULL);
		Color_t5  L_120 = Color_get_red_m2327(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_119);
		GUIStyleState_set_textColor_m2292(L_119, L_120, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::GetStyle(System.String)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t1402_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1213;
extern Il2CppCodeGenString* _stringLiteral1214;
extern Il2CppCodeGenString* _stringLiteral1215;
extern "C" GUIStyle_t184 * GUISkin_GetStyle_m7676 (GUISkin_t547 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		EventType_t1402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(959);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1213 = il2cpp_codegen_string_literal_from_index(1213);
		_stringLiteral1214 = il2cpp_codegen_string_literal_from_index(1214);
		_stringLiteral1215 = il2cpp_codegen_string_literal_from_index(1215);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t184 * V_0 = {0};
	{
		String_t* L_0 = ___styleName;
		GUIStyle_t184 * L_1 = GUISkin_FindStyle_m7677(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIStyle_t184 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		GUIStyle_t184 * L_3 = V_0;
		return L_3;
	}

IL_0010:
	{
		ObjectU5BU5D_t34* L_4 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 6));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral1213);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral1213;
		ObjectU5BU5D_t34* L_5 = L_4;
		String_t* L_6 = ___styleName;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t34* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1214);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral1214;
		ObjectU5BU5D_t34* L_8 = L_7;
		String_t* L_9 = Object_get_name_m2286(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t34* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral1215);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral1215;
		ObjectU5BU5D_t34* L_11 = L_10;
		Event_t725 * L_12 = Event_get_current_m7812(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = Event_get_type_m7773(L_12, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5, sizeof(Object_t *))) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2254(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Debug_LogWarning_m430(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GUIStyle_t184 * L_17 = GUISkin_get_error_m7673(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_17;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::FindStyle(System.String)
extern Il2CppCodeGenString* _stringLiteral1216;
extern "C" GUIStyle_t184 * GUISkin_FindStyle_m7677 (GUISkin_t547 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1216 = il2cpp_codegen_string_literal_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t184 * V_0 = {0};
	{
		bool L_0 = Object_op_Equality_m427(NULL /*static, unused*/, __this, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Debug_LogError_m302(NULL /*static, unused*/, _stringLiteral1216, /*hidden argument*/NULL);
		return (GUIStyle_t184 *)NULL;
	}

IL_0018:
	{
		Dictionary_2_t1397 * L_1 = (__this->___styles_25);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		GUISkin_BuildStyleCache_m7675(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Dictionary_2_t1397 * L_2 = (__this->___styles_25);
		String_t* L_3 = ___styleName;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, GUIStyle_t184 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::TryGetValue(!0,!1&) */, L_2, L_3, (&V_0));
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		GUIStyle_t184 * L_5 = V_0;
		return L_5;
	}

IL_003e:
	{
		return (GUIStyle_t184 *)NULL;
	}
}
// System.Void UnityEngine.GUISkin::MakeCurrent()
extern TypeInfo* GUISkin_t547_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUISkin_MakeCurrent_m7678 (GUISkin_t547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t547_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___current_27 = __this;
		Font_t684 * L_0 = GUISkin_get_font_m7631(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m7747(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SkinChangedDelegate_t1395 * L_1 = ((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_26;
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		SkinChangedDelegate_t1395 * L_2 = ((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_26;
		NullCheck(L_2);
		SkinChangedDelegate_Invoke_m7626(L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.GUISkin::GetEnumerator()
extern TypeInfo* Enumerator_t1471_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m8185_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m8186_MethodInfo_var;
extern "C" Object_t * GUISkin_GetEnumerator_m7679 (GUISkin_t547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1471_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(965);
		Dictionary_2_get_Values_m8185_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484725);
		ValueCollection_GetEnumerator_m8186_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484726);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1397 * L_0 = (__this->___styles_25);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GUISkin_BuildStyleCache_m7675(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Dictionary_2_t1397 * L_1 = (__this->___styles_25);
		NullCheck(L_1);
		ValueCollection_t1472 * L_2 = Dictionary_2_get_Values_m8185(L_1, /*hidden argument*/Dictionary_2_get_Values_m8185_MethodInfo_var);
		NullCheck(L_2);
		Enumerator_t1471  L_3 = ValueCollection_GetEnumerator_m8186(L_2, /*hidden argument*/ValueCollection_GetEnumerator_m8186_MethodInfo_var);
		Enumerator_t1471  L_4 = L_3;
		Object_t * L_5 = Box(Enumerator_t1471_il2cpp_TypeInfo_var, &L_4);
		return (Object_t *)L_5;
	}
}
// System.Void UnityEngine.GUIContent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m7680 (GUIContent_t549 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m2481 (GUIContent_t549 * __this, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___text;
		__this->___m_Text_0 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.GUIContent)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m7681 (GUIContent_t549 * __this, GUIContent_t549 * ___src, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		GUIContent_t549 * L_2 = ___src;
		NullCheck(L_2);
		String_t* L_3 = (L_2->___m_Text_0);
		__this->___m_Text_0 = L_3;
		GUIContent_t549 * L_4 = ___src;
		NullCheck(L_4);
		Texture_t731 * L_5 = (L_4->___m_Image_1);
		__this->___m_Image_1 = L_5;
		GUIContent_t549 * L_6 = ___src;
		NullCheck(L_6);
		String_t* L_7 = (L_6->___m_Tooltip_2);
		__this->___m_Tooltip_2 = L_7;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern "C" void GUIContent__cctor_m7682 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		GUIContent_t549 * L_1 = (GUIContent_t549 *)il2cpp_codegen_object_new (GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent__ctor_m2481(L_1, L_0, /*hidden argument*/NULL);
		((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___none_3 = L_1;
		GUIContent_t549 * L_2 = (GUIContent_t549 *)il2cpp_codegen_object_new (GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent__ctor_m7680(L_2, /*hidden argument*/NULL);
		((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_Text_4 = L_2;
		GUIContent_t549 * L_3 = (GUIContent_t549 *)il2cpp_codegen_object_new (GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent__ctor_m7680(L_3, /*hidden argument*/NULL);
		((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_Image_5 = L_3;
		GUIContent_t549 * L_4 = (GUIContent_t549 *)il2cpp_codegen_object_new (GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent__ctor_m7680(L_4, /*hidden argument*/NULL);
		((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_6 = L_4;
		return;
	}
}
// System.String UnityEngine.GUIContent::get_text()
extern "C" String_t* GUIContent_get_text_m4240 (GUIContent_t549 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Text_0);
		return L_0;
	}
}
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C" void GUIContent_set_text_m7683 (GUIContent_t549 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_Text_0 = L_0;
		return;
	}
}
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern "C" GUIContent_t549 * GUIContent_Temp_m7684 (Object_t * __this /* static, unused */, String_t* ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_0 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_Text_4;
		String_t* L_1 = ___t;
		NullCheck(L_0);
		L_0->___m_Text_0 = L_1;
		GUIContent_t549 * L_2 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_Text_4;
		return L_2;
	}
}
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(UnityEngine.Texture)
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern "C" GUIContent_t549 * GUIContent_Temp_m7685 (Object_t * __this /* static, unused */, Texture_t731 * ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_0 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_Image_5;
		Texture_t731 * L_1 = ___i;
		NullCheck(L_0);
		L_0->___m_Image_1 = L_1;
		GUIContent_t549 * L_2 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_Image_5;
		return L_2;
	}
}
// System.Void UnityEngine.GUIContent::ClearStaticCache()
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern "C" void GUIContent_ClearStaticCache_m7686 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent_t549 * L_0 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_Text_4;
		NullCheck(L_0);
		L_0->___m_Text_0 = (String_t*)NULL;
		GUIContent_t549 * L_1 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_Image_5;
		NullCheck(L_1);
		L_1->___m_Image_1 = (Texture_t731 *)NULL;
		GUIContent_t549 * L_2 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_6;
		NullCheck(L_2);
		L_2->___m_Text_0 = (String_t*)NULL;
		GUIContent_t549 * L_3 = ((GUIContent_t549_StaticFields*)GUIContent_t549_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_6;
		NullCheck(L_3);
		L_3->___m_Image_1 = (Texture_t731 *)NULL;
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::.ctor()
extern "C" void GUIStyleState__ctor_m7687 (GUIStyleState_t504 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		GUIStyleState_Init_m7691(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C" void GUIStyleState__ctor_m7688 (GUIStyleState_t504 * __this, GUIStyle_t184 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		GUIStyle_t184 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		GUIStyleState_RefreshAssetReference_m7689(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::RefreshAssetReference()
extern "C" void GUIStyleState_RefreshAssetReference_m7689 (GUIStyleState_t504 * __this, const MethodInfo* method)
{
	{
		Texture2D_t33 * L_0 = GUIStyleState_GetBackgroundInternal_m7694(__this, /*hidden argument*/NULL);
		__this->___m_BackgroundInternal_2 = L_0;
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Finalize()
extern "C" void GUIStyleState_Finalize_m7690 (GUIStyleState_t504 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t184 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			GUIStyleState_Cleanup_m7692(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Init()
extern "C" void GUIStyleState_Init_m7691 (GUIStyleState_t504 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Init_m7691_ftn) (GUIStyleState_t504 *);
	static GUIStyleState_Init_m7691_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Init_m7691_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::Cleanup()
extern "C" void GUIStyleState_Cleanup_m7692 (GUIStyleState_t504 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Cleanup_m7692_ftn) (GUIStyleState_t504 *);
	static GUIStyleState_Cleanup_m7692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Cleanup_m7692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::SetBackgroundInternal(UnityEngine.Texture2D)
extern "C" void GUIStyleState_SetBackgroundInternal_m7693 (GUIStyleState_t504 * __this, Texture2D_t33 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyleState_SetBackgroundInternal_m7693_ftn) (GUIStyleState_t504 *, Texture2D_t33 *);
	static GUIStyleState_SetBackgroundInternal_m7693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_SetBackgroundInternal_m7693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::SetBackgroundInternal(UnityEngine.Texture2D)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
extern "C" Texture2D_t33 * GUIStyleState_GetBackgroundInternal_m7694 (GUIStyleState_t504 * __this, const MethodInfo* method)
{
	typedef Texture2D_t33 * (*GUIStyleState_GetBackgroundInternal_m7694_ftn) (GUIStyleState_t504 *);
	static GUIStyleState_GetBackgroundInternal_m7694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_GetBackgroundInternal_m7694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::GetBackgroundInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::set_background(UnityEngine.Texture2D)
extern "C" void GUIStyleState_set_background_m2472 (GUIStyleState_t504 * __this, Texture2D_t33 * ___value, const MethodInfo* method)
{
	{
		Texture2D_t33 * L_0 = ___value;
		GUIStyleState_SetBackgroundInternal_m7693(__this, L_0, /*hidden argument*/NULL);
		Texture2D_t33 * L_1 = ___value;
		__this->___m_BackgroundInternal_2 = L_1;
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
extern "C" void GUIStyleState_set_textColor_m2292 (GUIStyleState_t504 * __this, Color_t5  ___value, const MethodInfo* method)
{
	{
		GUIStyleState_INTERNAL_set_textColor_m7695(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
extern "C" void GUIStyleState_INTERNAL_set_textColor_m7695 (GUIStyleState_t504 * __this, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyleState_INTERNAL_set_textColor_m7695_ftn) (GUIStyleState_t504 *, Color_t5 *);
	static GUIStyleState_INTERNAL_set_textColor_m7695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_INTERNAL_set_textColor_m7695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C" void RectOffset__ctor_m4412 (RectOffset_t780 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		RectOffset_Init_m7699(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C" void RectOffset__ctor_m7696 (RectOffset_t780 * __this, GUIStyle_t184 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		GUIStyle_t184 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void RectOffset__ctor_m7697 (RectOffset_t780 * __this, int32_t ___left, int32_t ___right, int32_t ___top, int32_t ___bottom, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		RectOffset_Init_m7699(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___left;
		RectOffset_set_left_m7701(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___right;
		RectOffset_set_right_m7703(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___top;
		RectOffset_set_top_m7704(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___bottom;
		RectOffset_set_bottom_m7706(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C" void RectOffset_Finalize_m7698 (RectOffset_t780 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t184 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m7700(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C" void RectOffset_Init_m7699 (RectOffset_t780 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m7699_ftn) (RectOffset_t780 *);
	static RectOffset_Init_m7699_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m7699_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C" void RectOffset_Cleanup_m7700 (RectOffset_t780 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m7700_ftn) (RectOffset_t780 *);
	static RectOffset_Cleanup_m7700_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m7700_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C" int32_t RectOffset_get_left_m4410 (RectOffset_t780 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m4410_ftn) (RectOffset_t780 *);
	static RectOffset_get_left_m4410_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m4410_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C" void RectOffset_set_left_m7701 (RectOffset_t780 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m7701_ftn) (RectOffset_t780 *, int32_t);
	static RectOffset_set_left_m7701_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m7701_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C" int32_t RectOffset_get_right_m7702 (RectOffset_t780 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m7702_ftn) (RectOffset_t780 *);
	static RectOffset_get_right_m7702_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m7702_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C" void RectOffset_set_right_m7703 (RectOffset_t780 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m7703_ftn) (RectOffset_t780 *, int32_t);
	static RectOffset_set_right_m7703_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m7703_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C" int32_t RectOffset_get_top_m4411 (RectOffset_t780 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m4411_ftn) (RectOffset_t780 *);
	static RectOffset_get_top_m4411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m4411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C" void RectOffset_set_top_m7704 (RectOffset_t780 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m7704_ftn) (RectOffset_t780 *, int32_t);
	static RectOffset_set_top_m7704_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m7704_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C" int32_t RectOffset_get_bottom_m7705 (RectOffset_t780 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m7705_ftn) (RectOffset_t780 *);
	static RectOffset_get_bottom_m7705_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m7705_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C" void RectOffset_set_bottom_m7706 (RectOffset_t780 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m7706_ftn) (RectOffset_t780 *, int32_t);
	static RectOffset_set_bottom_m7706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m7706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C" int32_t RectOffset_get_horizontal_m4405 (RectOffset_t780 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m4405_ftn) (RectOffset_t780 *);
	static RectOffset_get_horizontal_m4405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m4405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C" int32_t RectOffset_get_vertical_m4406 (RectOffset_t780 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m4406_ftn) (RectOffset_t780 *);
	static RectOffset_get_vertical_m4406_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m4406_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C" Rect_t30  RectOffset_Remove_m7707 (RectOffset_t780 * __this, Rect_t30  ___rect, const MethodInfo* method)
{
	{
		Rect_t30  L_0 = RectOffset_INTERNAL_CALL_Remove_m7708(NULL /*static, unused*/, __this, (&___rect), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Rect UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)
extern "C" Rect_t30  RectOffset_INTERNAL_CALL_Remove_m7708 (Object_t * __this /* static, unused */, RectOffset_t780 * ___self, Rect_t30 * ___rect, const MethodInfo* method)
{
	typedef Rect_t30  (*RectOffset_INTERNAL_CALL_Remove_m7708_ftn) (RectOffset_t780 *, Rect_t30 *);
	static RectOffset_INTERNAL_CALL_Remove_m7708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m7708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)");
	return _il2cpp_icall_func(___self, ___rect);
}
// System.String UnityEngine.RectOffset::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1217;
extern "C" String_t* RectOffset_ToString_m7709 (RectOffset_t780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		_stringLiteral1217 = il2cpp_codegen_string_literal_from_index(1217);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 4));
		int32_t L_1 = RectOffset_get_left_m4410(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m7702(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m4411(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m7705(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1217, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void UnityEngine.GUIStyle::.ctor()
extern "C" void GUIStyle__ctor_m2290 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		GUIStyle_Init_m7713(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::.ctor(UnityEngine.GUIStyle)
extern "C" void GUIStyle__ctor_m2471 (GUIStyle_t184 * __this, GUIStyle_t184 * ___other, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		GUIStyle_t184 * L_0 = ___other;
		GUIStyle_InitCopy_m7714(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::.cctor()
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUIStyle__cctor_m7710 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUIStyle_t184_StaticFields*)GUIStyle_t184_il2cpp_TypeInfo_var->static_fields)->___showKeyboardFocus_14 = 1;
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Finalize()
extern "C" void GUIStyle_Finalize_m7711 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GUIStyle_Cleanup_m7715(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyle::CreateObjectReferences()
extern "C" void GUIStyle_CreateObjectReferences_m7712 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	{
		Font_t684 * L_0 = GUIStyle_GetFontInternal_m7741(__this, /*hidden argument*/NULL);
		__this->___m_FontInternal_13 = L_0;
		GUIStyleState_t504 * L_1 = GUIStyle_get_normal_m2291(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyleState_RefreshAssetReference_m7689(L_1, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = GUIStyle_get_hover_m7718(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyleState_RefreshAssetReference_m7689(L_2, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_3 = GUIStyle_get_active_m7719(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyleState_RefreshAssetReference_m7689(L_3, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_4 = GUIStyle_get_focused_m7723(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyleState_RefreshAssetReference_m7689(L_4, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_5 = GUIStyle_get_onNormal_m7720(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyleState_RefreshAssetReference_m7689(L_5, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_6 = GUIStyle_get_onHover_m7721(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyleState_RefreshAssetReference_m7689(L_6, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_7 = GUIStyle_get_onActive_m7722(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUIStyleState_RefreshAssetReference_m7689(L_7, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_8 = GUIStyle_get_onFocused_m7724(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GUIStyleState_RefreshAssetReference_m7689(L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Init()
extern "C" void GUIStyle_Init_m7713 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Init_m7713_ftn) (GUIStyle_t184 *);
	static GUIStyle_Init_m7713_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Init_m7713_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::InitCopy(UnityEngine.GUIStyle)
extern "C" void GUIStyle_InitCopy_m7714 (GUIStyle_t184 * __this, GUIStyle_t184 * ___other, const MethodInfo* method)
{
	typedef void (*GUIStyle_InitCopy_m7714_ftn) (GUIStyle_t184 *, GUIStyle_t184 *);
	static GUIStyle_InitCopy_m7714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_InitCopy_m7714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::InitCopy(UnityEngine.GUIStyle)");
	_il2cpp_icall_func(__this, ___other);
}
// System.Void UnityEngine.GUIStyle::Cleanup()
extern "C" void GUIStyle_Cleanup_m7715 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Cleanup_m7715_ftn) (GUIStyle_t184 *);
	static GUIStyle_Cleanup_m7715_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Cleanup_m7715_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.String UnityEngine.GUIStyle::get_name()
extern "C" String_t* GUIStyle_get_name_m7716 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef String_t* (*GUIStyle_get_name_m7716_ftn) (GUIStyle_t184 *);
	static GUIStyle_get_name_m7716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_name_m7716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_name(System.String)
extern "C" void GUIStyle_set_name_m7717 (GUIStyle_t184 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_name_m7717_ftn) (GUIStyle_t184 *, String_t*);
	static GUIStyle_set_name_m7717_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_name_m7717_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
extern TypeInfo* GUIStyleState_t504_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t504 * GUIStyle_get_normal_m2291 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t504 * L_0 = (__this->___m_Normal_1);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m7725(__this, 0, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = (GUIStyleState_t504 *)il2cpp_codegen_object_new (GUIStyleState_t504_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m7688(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Normal_1 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t504 * L_3 = (__this->___m_Normal_1);
		return L_3;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_hover()
extern TypeInfo* GUIStyleState_t504_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t504 * GUIStyle_get_hover_m7718 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t504 * L_0 = (__this->___m_Hover_2);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m7725(__this, 1, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = (GUIStyleState_t504 *)il2cpp_codegen_object_new (GUIStyleState_t504_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m7688(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Hover_2 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t504 * L_3 = (__this->___m_Hover_2);
		return L_3;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_active()
extern TypeInfo* GUIStyleState_t504_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t504 * GUIStyle_get_active_m7719 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t504 * L_0 = (__this->___m_Active_3);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m7725(__this, 2, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = (GUIStyleState_t504 *)il2cpp_codegen_object_new (GUIStyleState_t504_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m7688(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Active_3 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t504 * L_3 = (__this->___m_Active_3);
		return L_3;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_onNormal()
extern TypeInfo* GUIStyleState_t504_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t504 * GUIStyle_get_onNormal_m7720 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t504 * L_0 = (__this->___m_OnNormal_5);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m7725(__this, 4, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = (GUIStyleState_t504 *)il2cpp_codegen_object_new (GUIStyleState_t504_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m7688(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_OnNormal_5 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t504 * L_3 = (__this->___m_OnNormal_5);
		return L_3;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_onHover()
extern TypeInfo* GUIStyleState_t504_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t504 * GUIStyle_get_onHover_m7721 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t504 * L_0 = (__this->___m_OnHover_6);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m7725(__this, 5, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = (GUIStyleState_t504 *)il2cpp_codegen_object_new (GUIStyleState_t504_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m7688(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_OnHover_6 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t504 * L_3 = (__this->___m_OnHover_6);
		return L_3;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_onActive()
extern TypeInfo* GUIStyleState_t504_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t504 * GUIStyle_get_onActive_m7722 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t504 * L_0 = (__this->___m_OnActive_7);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m7725(__this, 6, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = (GUIStyleState_t504 *)il2cpp_codegen_object_new (GUIStyleState_t504_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m7688(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_OnActive_7 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t504 * L_3 = (__this->___m_OnActive_7);
		return L_3;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_focused()
extern TypeInfo* GUIStyleState_t504_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t504 * GUIStyle_get_focused_m7723 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t504 * L_0 = (__this->___m_Focused_4);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m7725(__this, 3, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = (GUIStyleState_t504 *)il2cpp_codegen_object_new (GUIStyleState_t504_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m7688(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Focused_4 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t504 * L_3 = (__this->___m_Focused_4);
		return L_3;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_onFocused()
extern TypeInfo* GUIStyleState_t504_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t504 * GUIStyle_get_onFocused_m7724 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t504 * L_0 = (__this->___m_OnFocused_8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m7725(__this, 7, /*hidden argument*/NULL);
		GUIStyleState_t504 * L_2 = (GUIStyleState_t504 *)il2cpp_codegen_object_new (GUIStyleState_t504_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m7688(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_OnFocused_8 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t504 * L_3 = (__this->___m_OnFocused_8);
		return L_3;
	}
}
// System.IntPtr UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetStyleStatePtr_m7725 (GUIStyle_t184 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetStyleStatePtr_m7725_ftn) (GUIStyle_t184 *, int32_t);
	static GUIStyle_GetStyleStatePtr_m7725_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetStyleStatePtr_m7725_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_border()
extern TypeInfo* RectOffset_t780_il2cpp_TypeInfo_var;
extern "C" RectOffset_t780 * GUIStyle_get_border_m7726 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t780_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t780 * L_0 = (__this->___m_Border_9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m7729(__this, 0, /*hidden argument*/NULL);
		RectOffset_t780 * L_2 = (RectOffset_t780 *)il2cpp_codegen_object_new (RectOffset_t780_il2cpp_TypeInfo_var);
		RectOffset__ctor_m7696(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Border_9 = L_2;
	}

IL_001e:
	{
		RectOffset_t780 * L_3 = (__this->___m_Border_9);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_margin()
extern TypeInfo* RectOffset_t780_il2cpp_TypeInfo_var;
extern "C" RectOffset_t780 * GUIStyle_get_margin_m7727 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t780_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t780 * L_0 = (__this->___m_Margin_11);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m7729(__this, 1, /*hidden argument*/NULL);
		RectOffset_t780 * L_2 = (RectOffset_t780 *)il2cpp_codegen_object_new (RectOffset_t780_il2cpp_TypeInfo_var);
		RectOffset__ctor_m7696(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Margin_11 = L_2;
	}

IL_001e:
	{
		RectOffset_t780 * L_3 = (__this->___m_Margin_11);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_padding()
extern TypeInfo* RectOffset_t780_il2cpp_TypeInfo_var;
extern "C" RectOffset_t780 * GUIStyle_get_padding_m7728 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t780_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t780 * L_0 = (__this->___m_Padding_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m7729(__this, 2, /*hidden argument*/NULL);
		RectOffset_t780 * L_2 = (RectOffset_t780 *)il2cpp_codegen_object_new (RectOffset_t780_il2cpp_TypeInfo_var);
		RectOffset__ctor_m7696(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Padding_10 = L_2;
	}

IL_001e:
	{
		RectOffset_t780 * L_3 = (__this->___m_Padding_10);
		return L_3;
	}
}
// System.IntPtr UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetRectOffsetPtr_m7729 (GUIStyle_t184 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetRectOffsetPtr_m7729_ftn) (GUIStyle_t184 *, int32_t);
	static GUIStyle_GetRectOffsetPtr_m7729_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetRectOffsetPtr_m7729_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
extern "C" int32_t GUIStyle_get_imagePosition_m7730 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef int32_t (*GUIStyle_get_imagePosition_m7730_ftn) (GUIStyle_t184 *);
	static GUIStyle_get_imagePosition_m7730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_imagePosition_m7730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_imagePosition()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C" void GUIStyle_set_alignment_m2295 (GUIStyle_t184 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_alignment_m2295_ftn) (GUIStyle_t184 *, int32_t);
	static GUIStyle_set_alignment_m2295_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_alignment_m2295_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_wordWrap()
extern "C" bool GUIStyle_get_wordWrap_m7731 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_wordWrap_m7731_ftn) (GUIStyle_t184 *);
	static GUIStyle_get_wordWrap_m7731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_wordWrap_m7731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_wordWrap()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
extern "C" void GUIStyle_set_wordWrap_m2296 (GUIStyle_t184 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_wordWrap_m2296_ftn) (GUIStyle_t184 *, bool);
	static GUIStyle_set_wordWrap_m2296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_wordWrap_m2296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_wordWrap(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::set_contentOffset(UnityEngine.Vector2)
extern "C" void GUIStyle_set_contentOffset_m7732 (GUIStyle_t184 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		GUIStyle_INTERNAL_set_contentOffset_m7733(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_set_contentOffset_m7733 (GUIStyle_t184 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_set_contentOffset_m7733_ftn) (GUIStyle_t184 *, Vector2_t29 *);
	static GUIStyle_INTERNAL_set_contentOffset_m7733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_set_contentOffset_m7733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::get_fixedWidth()
extern "C" float GUIStyle_get_fixedWidth_m7734 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedWidth_m7734_ftn) (GUIStyle_t184 *);
	static GUIStyle_get_fixedWidth_m7734_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedWidth_m7734_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.GUIStyle::get_fixedHeight()
extern "C" float GUIStyle_get_fixedHeight_m7735 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedHeight_m7735_ftn) (GUIStyle_t184 *);
	static GUIStyle_get_fixedHeight_m7735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedHeight_m7735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
extern "C" bool GUIStyle_get_stretchWidth_m7736 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchWidth_m7736_ftn) (GUIStyle_t184 *);
	static GUIStyle_get_stretchWidth_m7736_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchWidth_m7736_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
extern "C" void GUIStyle_set_stretchWidth_m7737 (GUIStyle_t184 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchWidth_m7737_ftn) (GUIStyle_t184 *, bool);
	static GUIStyle_set_stretchWidth_m7737_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchWidth_m7737_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
extern "C" bool GUIStyle_get_stretchHeight_m7738 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchHeight_m7738_ftn) (GUIStyle_t184 *);
	static GUIStyle_get_stretchHeight_m7738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchHeight_m7738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
extern "C" void GUIStyle_set_stretchHeight_m7739 (GUIStyle_t184 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchHeight_m7739_ftn) (GUIStyle_t184 *, bool);
	static GUIStyle_set_stretchHeight_m7739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchHeight_m7739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)
extern "C" float GUIStyle_Internal_GetLineHeight_m7740 (Object_t * __this /* static, unused */, IntPtr_t ___target, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_GetLineHeight_m7740_ftn) (IntPtr_t);
	static GUIStyle_Internal_GetLineHeight_m7740_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_GetLineHeight_m7740_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)");
	return _il2cpp_icall_func(___target);
}
// UnityEngine.Font UnityEngine.GUIStyle::GetFontInternal()
extern "C" Font_t684 * GUIStyle_GetFontInternal_m7741 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	typedef Font_t684 * (*GUIStyle_GetFontInternal_m7741_ftn) (GUIStyle_t184 *);
	static GUIStyle_GetFontInternal_m7741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetFontInternal_m7741_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetFontInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C" void GUIStyle_set_fontSize_m2293 (GUIStyle_t184 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_fontSize_m2293_ftn) (GUIStyle_t184 *, int32_t);
	static GUIStyle_set_fontSize_m2293_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_fontSize_m2293_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_fontSize(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::set_fontStyle(UnityEngine.FontStyle)
extern "C" void GUIStyle_set_fontStyle_m2294 (GUIStyle_t184 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_fontStyle_m2294_ftn) (GUIStyle_t184 *, int32_t);
	static GUIStyle_set_fontStyle_m2294_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_fontStyle_m2294_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_fontStyle(UnityEngine.FontStyle)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::get_lineHeight()
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_get_lineHeight_m7742 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		float L_1 = GUIStyle_Internal_GetLineHeight_m7740(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = bankers_roundf(L_1);
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
extern "C" void GUIStyle_Draw_m7743 (GUIStyle_t184 * __this, Rect_t30  ___position, GUIContent_t549 * ___content, int32_t ___controlID, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Rect_t30  L_0 = ___position;
		GUIContent_t549 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		bool L_3 = V_0;
		GUIStyle_Draw_m7744(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1218;
extern "C" void GUIStyle_Draw_m7744 (GUIStyle_t184 * __this, Rect_t30  ___position, GUIContent_t549 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		_stringLiteral1218 = il2cpp_codegen_string_literal_from_index(1218);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t549 * L_0 = ___content;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_1 = (__this->___m_Ptr_0);
		Rect_t30  L_2 = ___position;
		GUIContent_t549 * L_3 = ___content;
		int32_t L_4 = ___controlID;
		bool L_5 = ___on;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw2_m7745(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001b:
	{
		Debug_LogError_m302(NULL /*static, unused*/, _stringLiteral1218, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_Draw2(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_Draw2_m7745 (Object_t * __this /* static, unused */, IntPtr_t ___style, Rect_t30  ___position, GUIContent_t549 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___style;
		GUIContent_t549 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		bool L_3 = ___on;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_Draw2_m7746(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_Draw2_m7746 (Object_t * __this /* static, unused */, IntPtr_t ___style, Rect_t30 * ___position, GUIContent_t549 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_Draw2_m7746_ftn) (IntPtr_t, Rect_t30 *, GUIContent_t549 *, int32_t, bool);
	static GUIStyle_INTERNAL_CALL_Internal_Draw2_m7746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_Draw2_m7746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___style, ___position, ___content, ___controlID, ___on);
}
// System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
extern "C" void GUIStyle_SetDefaultFont_m7747 (Object_t * __this /* static, unused */, Font_t684 * ___font, const MethodInfo* method)
{
	typedef void (*GUIStyle_SetDefaultFont_m7747_ftn) (Font_t684 *);
	static GUIStyle_SetDefaultFont_m7747_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_SetDefaultFont_m7747_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)");
	_il2cpp_icall_func(___font);
}
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t184 * GUIStyle_get_none_m7748 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_0 = ((GUIStyle_t184_StaticFields*)GUIStyle_t184_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t184 * L_1 = (GUIStyle_t184 *)il2cpp_codegen_object_new (GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2290(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		((GUIStyle_t184_StaticFields*)GUIStyle_t184_il2cpp_TypeInfo_var->static_fields)->___s_None_15 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_2 = ((GUIStyle_t184_StaticFields*)GUIStyle_t184_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::GetCursorPixelPosition(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" Vector2_t29  GUIStyle_GetCursorPixelPosition_m7749 (GUIStyle_t184 * __this, Rect_t30  ___position, GUIContent_t549 * ___content, int32_t ___cursorStringIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t29  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t30  L_1 = ___position;
		GUIContent_t549 * L_2 = ___content;
		int32_t L_3 = ___cursorStringIndex;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_Internal_GetCursorPixelPosition_m7750(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_GetCursorPixelPosition_m7750 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t30  ___position, GUIContent_t549 * ___content, int32_t ___cursorStringIndex, Vector2_t29 * ___ret, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___target;
		GUIContent_t549 * L_1 = ___content;
		int32_t L_2 = ___cursorStringIndex;
		Vector2_t29 * L_3 = ___ret;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m7751(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m7751 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t30 * ___position, GUIContent_t549 * ___content, int32_t ___cursorStringIndex, Vector2_t29 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m7751_ftn) (IntPtr_t, Rect_t30 *, GUIContent_t549 *, int32_t, Vector2_t29 *);
	static GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m7751_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m7751_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___position, ___content, ___cursorStringIndex, ___ret);
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" Vector2_t29  GUIStyle_CalcSize_m7752 (GUIStyle_t184 * __this, GUIContent_t549 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t29  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t549 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcSize_m7753(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)
extern "C" void GUIStyle_Internal_CalcSize_m7753 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t549 * ___content, Vector2_t29 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcSize_m7753_ftn) (IntPtr_t, GUIContent_t549 *, Vector2_t29 *);
	static GUIStyle_Internal_CalcSize_m7753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcSize_m7753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___content, ___ret);
}
// System.Single UnityEngine.GUIStyle::CalcHeight(UnityEngine.GUIContent,System.Single)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_CalcHeight_m7754 (GUIStyle_t184 * __this, GUIContent_t549 * ___content, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t549 * L_1 = ___content;
		float L_2 = ___width;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		float L_3 = GUIStyle_Internal_CalcHeight_m7755(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
extern "C" float GUIStyle_Internal_CalcHeight_m7755 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t549 * ___content, float ___width, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_CalcHeight_m7755_ftn) (IntPtr_t, GUIContent_t549 *, float);
	static GUIStyle_Internal_CalcHeight_m7755_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcHeight_m7755_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)");
	return _il2cpp_icall_func(___target, ___content, ___width);
}
// System.Boolean UnityEngine.GUIStyle::get_isHeightDependantOnWidth()
extern "C" bool GUIStyle_get_isHeightDependantOnWidth_m7756 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		float L_0 = GUIStyle_get_fixedHeight_m7735(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		bool L_1 = GUIStyle_get_wordWrap_m7731(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = GUIStyle_get_imagePosition_m7730(__this, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B4_0 = 0;
	}

IL_002a:
	{
		G_B6_0 = G_B4_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B6_0 = 0;
	}

IL_002d:
	{
		return G_B6_0;
	}
}
// System.Void UnityEngine.GUIStyle::CalcMinMaxWidth(UnityEngine.GUIContent,System.Single&,System.Single&)
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_CalcMinMaxWidth_m7757 (GUIStyle_t184 * __this, GUIContent_t549 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t549 * L_1 = ___content;
		float* L_2 = ___minWidth;
		float* L_3 = ___maxWidth;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcMinMaxWidth_m7758(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
extern "C" void GUIStyle_Internal_CalcMinMaxWidth_m7758 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t549 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcMinMaxWidth_m7758_ftn) (IntPtr_t, GUIContent_t549 *, float*, float*);
	static GUIStyle_Internal_CalcMinMaxWidth_m7758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcMinMaxWidth_m7758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)");
	_il2cpp_icall_func(___target, ___content, ___minWidth, ___maxWidth);
}
// System.String UnityEngine.GUIStyle::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1219;
extern "C" String_t* GUIStyle_ToString_m7759 (GUIStyle_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1219 = il2cpp_codegen_string_literal_from_index(1219);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = GUIStyle_get_name_m7716(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		String_t* L_2 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1219, L_0, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUIStyle::op_Implicit(System.String)
extern TypeInfo* GUISkin_t547_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1220;
extern "C" GUIStyle_t184 * GUIStyle_op_Implicit_m7760 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t547_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		_stringLiteral1220 = il2cpp_codegen_string_literal_from_index(1220);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t547 * L_0 = ((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___current_27;
		bool L_1 = Object_op_Equality_m427(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogError_m302(NULL /*static, unused*/, _stringLiteral1220, /*hidden argument*/NULL);
		GUIStyle_t184 * L_2 = GUISkin_get_error_m7673(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0020:
	{
		GUISkin_t547 * L_3 = ((GUISkin_t547_StaticFields*)GUISkin_t547_il2cpp_TypeInfo_var->static_fields)->___current_27;
		String_t* L_4 = ___str;
		NullCheck(L_3);
		GUIStyle_t184 * L_5 = GUISkin_GetStyle_m7676(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
