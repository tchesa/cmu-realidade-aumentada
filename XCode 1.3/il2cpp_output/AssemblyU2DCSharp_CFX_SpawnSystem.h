﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CFX_SpawnSystem
struct CFX_SpawnSystem_t334;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t311;
// System.Int32[]
struct Int32U5BU5D_t335;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t336;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t337;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// CFX_SpawnSystem
struct  CFX_SpawnSystem_t334  : public MonoBehaviour_t18
{
	// UnityEngine.GameObject[] CFX_SpawnSystem::objectsToPreload
	GameObjectU5BU5D_t311* ___objectsToPreload_2;
	// System.Int32[] CFX_SpawnSystem::objectsToPreloadTimes
	Int32U5BU5D_t335* ___objectsToPreloadTimes_3;
	// System.Boolean CFX_SpawnSystem::hideObjectsInHierarchy
	bool ___hideObjectsInHierarchy_4;
	// System.Boolean CFX_SpawnSystem::allObjectsLoaded
	bool ___allObjectsLoaded_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>> CFX_SpawnSystem::instantiatedObjects
	Dictionary_2_t336 * ___instantiatedObjects_6;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CFX_SpawnSystem::poolCursors
	Dictionary_2_t337 * ___poolCursors_7;
};
struct CFX_SpawnSystem_t334_StaticFields{
	// CFX_SpawnSystem CFX_SpawnSystem::instance
	CFX_SpawnSystem_t334 * ___instance_1;
};
