﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_10MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m25364(__this, ___object, ___method, method) (( void (*) (Transform_1_t3212 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m16461_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m25365(__this, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Transform_1_t3212 *, int32_t, SurfaceAbstractBehaviour_t436 *, const MethodInfo*))Transform_1_Invoke_m16462_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m25366(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3212 *, int32_t, SurfaceAbstractBehaviour_t436 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m16463_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m25367(__this, ___result, method) (( DictionaryEntry_t58  (*) (Transform_1_t3212 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m16464_gshared)(__this, ___result, method)
