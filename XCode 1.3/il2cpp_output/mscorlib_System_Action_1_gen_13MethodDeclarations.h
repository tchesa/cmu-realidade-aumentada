﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_29MethodDeclarations.h"

// System.Void System.Action`1<ISN_CheckUrlResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2260(__this, ___object, ___method, method) (( void (*) (Action_1_t155 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m17054_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<ISN_CheckUrlResult>::Invoke(T)
#define Action_1_Invoke_m2261(__this, ___obj, method) (( void (*) (Action_1_t155 *, ISN_CheckUrlResult_t156 *, const MethodInfo*))Action_1_Invoke_m17055_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<ISN_CheckUrlResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m17645(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t155 *, ISN_CheckUrlResult_t156 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m17057_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<ISN_CheckUrlResult>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m17646(__this, ___result, method) (( void (*) (Action_1_t155 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m17059_gshared)(__this, ___result, method)
