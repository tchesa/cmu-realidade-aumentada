﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_IOSDialogResult.h"

// IOSDialogResult
struct  IOSDialogResult_t90 
{
	// System.Int32 IOSDialogResult::value__
	int32_t ___value___1;
};
