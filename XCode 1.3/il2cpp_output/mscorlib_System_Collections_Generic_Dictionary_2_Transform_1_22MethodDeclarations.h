﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>
struct Transform_1_t2792;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m18656_gshared (Transform_1_t2792 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m18656(__this, ___object, ___method, method) (( void (*) (Transform_1_t2792 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m18656_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::Invoke(TKey,TValue)
extern "C" Object_t * Transform_1_Invoke_m18657_gshared (Transform_1_t2792 * __this, Object_t * ___key, float ___value, const MethodInfo* method);
#define Transform_1_Invoke_m18657(__this, ___key, ___value, method) (( Object_t * (*) (Transform_1_t2792 *, Object_t *, float, const MethodInfo*))Transform_1_Invoke_m18657_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m18658_gshared (Transform_1_t2792 * __this, Object_t * ___key, float ___value, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m18658(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2792 *, Object_t *, float, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m18658_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m18659_gshared (Transform_1_t2792 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m18659(__this, ___result, method) (( Object_t * (*) (Transform_1_t2792 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m18659_gshared)(__this, ___result, method)
