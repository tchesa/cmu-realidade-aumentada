﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_35MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m23359(__this, ___host, method) (( void (*) (Enumerator_t1140 *, Dictionary_2_t949 *, const MethodInfo*))Enumerator__ctor_m16451_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23360(__this, method) (( Object_t * (*) (Enumerator_t1140 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16452_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m23361(__this, method) (( void (*) (Enumerator_t1140 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16453_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::Dispose()
#define Enumerator_Dispose_m6544(__this, method) (( void (*) (Enumerator_t1140 *, const MethodInfo*))Enumerator_Dispose_m16454_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::MoveNext()
#define Enumerator_MoveNext_m6543(__this, method) (( bool (*) (Enumerator_t1140 *, const MethodInfo*))Enumerator_MoveNext_m16455_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::get_Current()
#define Enumerator_get_Current_m6542(__this, method) (( VirtualButton_t1061 * (*) (Enumerator_t1140 *, const MethodInfo*))Enumerator_get_Current_m16456_gshared)(__this, method)
