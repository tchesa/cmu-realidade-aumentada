﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m15954(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2624 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m15838_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m15955(__this, method) (( String_t* (*) (KeyValuePair_2_t2624 *, const MethodInfo*))KeyValuePair_2_get_Key_m15839_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15956(__this, ___value, method) (( void (*) (KeyValuePair_2_t2624 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15840_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m15957(__this, method) (( int32_t (*) (KeyValuePair_2_t2624 *, const MethodInfo*))KeyValuePair_2_get_Value_m15841_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15958(__this, ___value, method) (( void (*) (KeyValuePair_2_t2624 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m15842_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m15959(__this, method) (( String_t* (*) (KeyValuePair_2_t2624 *, const MethodInfo*))KeyValuePair_2_ToString_m15843_gshared)(__this, method)
