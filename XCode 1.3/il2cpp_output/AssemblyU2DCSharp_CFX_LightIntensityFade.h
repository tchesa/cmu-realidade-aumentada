﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// CFX_LightIntensityFade
struct  CFX_LightIntensityFade_t329  : public MonoBehaviour_t18
{
	// System.Single CFX_LightIntensityFade::duration
	float ___duration_1;
	// System.Single CFX_LightIntensityFade::delay
	float ___delay_2;
	// System.Single CFX_LightIntensityFade::finalIntensity
	float ___finalIntensity_3;
	// System.Single CFX_LightIntensityFade::baseIntensity
	float ___baseIntensity_4;
	// System.Boolean CFX_LightIntensityFade::autodestruct
	bool ___autodestruct_5;
	// System.Single CFX_LightIntensityFade::p_lifetime
	float ___p_lifetime_6;
	// System.Single CFX_LightIntensityFade::p_delay
	float ___p_delay_7;
};
