﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Facebook.Unity.FacebookSettings
struct FacebookSettings_t246;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;
// System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>
struct List_1_t247;

#include "UnityEngine_UnityEngine_ScriptableObject.h"

// Facebook.Unity.FacebookSettings
struct  FacebookSettings_t246  : public ScriptableObject_t77
{
	// System.Int32 Facebook.Unity.FacebookSettings::selectedAppIndex
	int32_t ___selectedAppIndex_5;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.FacebookSettings::appIds
	List_1_t78 * ___appIds_6;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.FacebookSettings::appLabels
	List_1_t78 * ___appLabels_7;
	// System.Boolean Facebook.Unity.FacebookSettings::cookie
	bool ___cookie_8;
	// System.Boolean Facebook.Unity.FacebookSettings::logging
	bool ___logging_9;
	// System.Boolean Facebook.Unity.FacebookSettings::status
	bool ___status_10;
	// System.Boolean Facebook.Unity.FacebookSettings::xfbml
	bool ___xfbml_11;
	// System.Boolean Facebook.Unity.FacebookSettings::frictionlessRequests
	bool ___frictionlessRequests_12;
	// System.String Facebook.Unity.FacebookSettings::iosURLSuffix
	String_t* ___iosURLSuffix_13;
	// System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes> Facebook.Unity.FacebookSettings::appLinkSchemes
	List_1_t247 * ___appLinkSchemes_14;
};
struct FacebookSettings_t246_StaticFields{
	// Facebook.Unity.FacebookSettings Facebook.Unity.FacebookSettings::instance
	FacebookSettings_t246 * ___instance_4;
};
