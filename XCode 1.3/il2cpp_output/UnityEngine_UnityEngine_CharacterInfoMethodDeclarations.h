﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void CharacterInfo_t1373_marshal(const CharacterInfo_t1373& unmarshaled, CharacterInfo_t1373_marshaled& marshaled);
extern "C" void CharacterInfo_t1373_marshal_back(const CharacterInfo_t1373_marshaled& marshaled, CharacterInfo_t1373& unmarshaled);
extern "C" void CharacterInfo_t1373_marshal_cleanup(CharacterInfo_t1373_marshaled& marshaled);
