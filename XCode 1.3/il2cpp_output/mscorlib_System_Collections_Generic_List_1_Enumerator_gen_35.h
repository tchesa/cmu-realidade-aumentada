﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<iAdBanner>
struct List_1_t462;
// iAdBanner
struct iAdBanner_t172;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<iAdBanner>
struct  Enumerator_t2742 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<iAdBanner>::l
	List_1_t462 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<iAdBanner>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<iAdBanner>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<iAdBanner>::current
	iAdBanner_t172 * ___current_3;
};
