﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.DateTime>
struct Action_1_t152;

#include "AssemblyU2DCSharp_ISN_Singleton_1_gen_1.h"

// IOSDateTimePicker
struct  IOSDateTimePicker_t150  : public ISN_Singleton_1_t151
{
	// System.Action`1<System.DateTime> IOSDateTimePicker::OnDateChanged
	Action_1_t152 * ___OnDateChanged_5;
	// System.Action`1<System.DateTime> IOSDateTimePicker::OnPickerClosed
	Action_1_t152 * ___OnPickerClosed_6;
};
struct IOSDateTimePicker_t150_StaticFields{
	// System.Action`1<System.DateTime> IOSDateTimePicker::<>f__am$cache2
	Action_1_t152 * ___U3CU3Ef__amU24cache2_7;
	// System.Action`1<System.DateTime> IOSDateTimePicker::<>f__am$cache3
	Action_1_t152 * ___U3CU3Ef__amU24cache3_8;
};
