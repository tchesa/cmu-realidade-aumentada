﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t337;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2650;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3658;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t3659;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t2813;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t2817;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m2587_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2587(__this, method) (( void (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2__ctor_m2587_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m18862_gshared (Dictionary_2_t337 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m18862(__this, ___comparer, method) (( void (*) (Dictionary_2_t337 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m18862_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m18863_gshared (Dictionary_2_t337 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m18863(__this, ___capacity, method) (( void (*) (Dictionary_2_t337 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m18863_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m18864_gshared (Dictionary_2_t337 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m18864(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t337 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m18864_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m18865_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m18865(__this, method) (( Object_t * (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m18865_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m18866_gshared (Dictionary_2_t337 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m18866(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t337 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m18866_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m18867_gshared (Dictionary_2_t337 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m18867(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t337 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m18867_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m18868_gshared (Dictionary_2_t337 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m18868(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t337 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m18868_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m18869_gshared (Dictionary_2_t337 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m18869(__this, ___key, method) (( bool (*) (Dictionary_2_t337 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m18869_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m18870_gshared (Dictionary_2_t337 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m18870(__this, ___key, method) (( void (*) (Dictionary_2_t337 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m18870_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18871_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18871(__this, method) (( bool (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18871_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18872_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18872(__this, method) (( Object_t * (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18872_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18873_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18873(__this, method) (( bool (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18873_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18874_gshared (Dictionary_2_t337 * __this, KeyValuePair_2_t2811  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18874(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t337 *, KeyValuePair_2_t2811 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18874_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18875_gshared (Dictionary_2_t337 * __this, KeyValuePair_2_t2811  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18875(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t337 *, KeyValuePair_2_t2811 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18875_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18876_gshared (Dictionary_2_t337 * __this, KeyValuePair_2U5BU5D_t3658* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18876(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t337 *, KeyValuePair_2U5BU5D_t3658*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18876_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18877_gshared (Dictionary_2_t337 * __this, KeyValuePair_2_t2811  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18877(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t337 *, KeyValuePair_2_t2811 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18877_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m18878_gshared (Dictionary_2_t337 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m18878(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t337 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m18878_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18879_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18879(__this, method) (( Object_t * (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18879_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18880_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18880(__this, method) (( Object_t* (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18880_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18881_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18881(__this, method) (( Object_t * (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18881_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m18882_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m18882(__this, method) (( int32_t (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_get_Count_m18882_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m18883_gshared (Dictionary_2_t337 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m18883(__this, ___key, method) (( int32_t (*) (Dictionary_2_t337 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m18883_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m18884_gshared (Dictionary_2_t337 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m18884(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t337 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m18884_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m18885_gshared (Dictionary_2_t337 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m18885(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t337 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m18885_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m18886_gshared (Dictionary_2_t337 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m18886(__this, ___size, method) (( void (*) (Dictionary_2_t337 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m18886_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m18887_gshared (Dictionary_2_t337 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m18887(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t337 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m18887_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2811  Dictionary_2_make_pair_m18888_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m18888(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2811  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m18888_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m18889_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m18889(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m18889_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m18890_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m18890(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m18890_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m18891_gshared (Dictionary_2_t337 * __this, KeyValuePair_2U5BU5D_t3658* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m18891(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t337 *, KeyValuePair_2U5BU5D_t3658*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m18891_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m18892_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m18892(__this, method) (( void (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_Resize_m18892_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m18893_gshared (Dictionary_2_t337 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m18893(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t337 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m18893_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m18894_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m18894(__this, method) (( void (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_Clear_m18894_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m18895_gshared (Dictionary_2_t337 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m18895(__this, ___key, method) (( bool (*) (Dictionary_2_t337 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m18895_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m18896_gshared (Dictionary_2_t337 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m18896(__this, ___value, method) (( bool (*) (Dictionary_2_t337 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m18896_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m18897_gshared (Dictionary_2_t337 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m18897(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t337 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m18897_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m18898_gshared (Dictionary_2_t337 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m18898(__this, ___sender, method) (( void (*) (Dictionary_2_t337 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m18898_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m18899_gshared (Dictionary_2_t337 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m18899(__this, ___key, method) (( bool (*) (Dictionary_2_t337 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m18899_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m18900_gshared (Dictionary_2_t337 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m18900(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t337 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m18900_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Keys()
extern "C" KeyCollection_t2813 * Dictionary_2_get_Keys_m18901_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m18901(__this, method) (( KeyCollection_t2813 * (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_get_Keys_m18901_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t2817 * Dictionary_2_get_Values_m18902_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m18902(__this, method) (( ValueCollection_t2817 * (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_get_Values_m18902_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m18903_gshared (Dictionary_2_t337 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m18903(__this, ___key, method) (( int32_t (*) (Dictionary_2_t337 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m18903_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m18904_gshared (Dictionary_2_t337 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m18904(__this, ___value, method) (( int32_t (*) (Dictionary_2_t337 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m18904_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m18905_gshared (Dictionary_2_t337 * __this, KeyValuePair_2_t2811  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m18905(__this, ___pair, method) (( bool (*) (Dictionary_2_t337 *, KeyValuePair_2_t2811 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m18905_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2815  Dictionary_2_GetEnumerator_m18906_gshared (Dictionary_2_t337 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m18906(__this, method) (( Enumerator_t2815  (*) (Dictionary_2_t337 *, const MethodInfo*))Dictionary_2_GetEnumerator_m18906_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m18907_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m18907(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m18907_gshared)(__this /* static, unused */, ___key, ___value, method)
