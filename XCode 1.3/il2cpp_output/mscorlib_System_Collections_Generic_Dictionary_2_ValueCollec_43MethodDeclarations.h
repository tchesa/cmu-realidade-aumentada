﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m17921(__this, ___dictionary, method) (( void (*) (ValueCollection_t2750 *, Dictionary_2_t177 *, const MethodInfo*))ValueCollection__ctor_m16163_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17922(__this, ___item, method) (( void (*) (ValueCollection_t2750 *, iAdBanner_t172 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17923(__this, method) (( void (*) (ValueCollection_t2750 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16165_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17924(__this, ___item, method) (( bool (*) (ValueCollection_t2750 *, iAdBanner_t172 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16166_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17925(__this, ___item, method) (( bool (*) (ValueCollection_t2750 *, iAdBanner_t172 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16167_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17926(__this, method) (( Object_t* (*) (ValueCollection_t2750 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16168_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m17927(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2750 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16169_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17928(__this, method) (( Object_t * (*) (ValueCollection_t2750 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16170_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17929(__this, method) (( bool (*) (ValueCollection_t2750 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16171_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17930(__this, method) (( bool (*) (ValueCollection_t2750 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16172_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m17931(__this, method) (( Object_t * (*) (ValueCollection_t2750 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16173_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m17932(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2750 *, iAdBannerU5BU5D_t2738*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m16174_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::GetEnumerator()
#define ValueCollection_GetEnumerator_m17933(__this, method) (( Enumerator_t3642  (*) (ValueCollection_t2750 *, const MethodInfo*))ValueCollection_GetEnumerator_m16175_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iAdBanner>::get_Count()
#define ValueCollection_get_Count_m17934(__this, method) (( int32_t (*) (ValueCollection_t2750 *, const MethodInfo*))ValueCollection_get_Count_m16176_gshared)(__this, method)
