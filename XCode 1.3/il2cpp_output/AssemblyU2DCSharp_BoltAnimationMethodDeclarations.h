﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BoltAnimation
struct BoltAnimation_t378;

#include "codegen/il2cpp-codegen.h"

// System.Void BoltAnimation::.ctor()
extern "C" void BoltAnimation__ctor_m1939 (BoltAnimation_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoltAnimation::Update()
extern "C" void BoltAnimation_Update_m1940 (BoltAnimation_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
