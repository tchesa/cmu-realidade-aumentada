﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// AnimationControllerScript/TimeEvent
struct  TimeEvent_t362  : public Object_t
{
	// System.String AnimationControllerScript/TimeEvent::name
	String_t* ___name_0;
	// System.Single AnimationControllerScript/TimeEvent::timer
	float ___timer_1;
	// System.String AnimationControllerScript/TimeEvent::function
	String_t* ___function_2;
};
