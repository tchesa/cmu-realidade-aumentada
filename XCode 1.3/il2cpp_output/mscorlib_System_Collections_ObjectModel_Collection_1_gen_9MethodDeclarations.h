﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct Collection_1_t3556;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2538;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3767;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t2099;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void Collection_1__ctor_m29809_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1__ctor_m29809(__this, method) (( void (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1__ctor_m29809_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29810_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29810(__this, method) (( bool (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29810_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m29811_gshared (Collection_1_t3556 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m29811(__this, ___array, ___index, method) (( void (*) (Collection_1_t3556 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m29811_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m29812_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m29812(__this, method) (( Object_t * (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m29812_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m29813_gshared (Collection_1_t3556 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m29813(__this, ___value, method) (( int32_t (*) (Collection_1_t3556 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m29813_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m29814_gshared (Collection_1_t3556 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m29814(__this, ___value, method) (( bool (*) (Collection_1_t3556 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m29814_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m29815_gshared (Collection_1_t3556 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m29815(__this, ___value, method) (( int32_t (*) (Collection_1_t3556 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m29815_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m29816_gshared (Collection_1_t3556 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m29816(__this, ___index, ___value, method) (( void (*) (Collection_1_t3556 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m29816_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m29817_gshared (Collection_1_t3556 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m29817(__this, ___value, method) (( void (*) (Collection_1_t3556 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m29817_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m29818_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m29818(__this, method) (( bool (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m29818_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m29819_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m29819(__this, method) (( Object_t * (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m29819_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m29820_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m29820(__this, method) (( bool (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m29820_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m29821_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m29821(__this, method) (( bool (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m29821_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m29822_gshared (Collection_1_t3556 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m29822(__this, ___index, method) (( Object_t * (*) (Collection_1_t3556 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m29822_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m29823_gshared (Collection_1_t3556 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m29823(__this, ___index, ___value, method) (( void (*) (Collection_1_t3556 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m29823_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void Collection_1_Add_m29824_gshared (Collection_1_t3556 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Collection_1_Add_m29824(__this, ___item, method) (( void (*) (Collection_1_t3556 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Collection_1_Add_m29824_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void Collection_1_Clear_m29825_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_Clear_m29825(__this, method) (( void (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_Clear_m29825_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m29826_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m29826(__this, method) (( void (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_ClearItems_m29826_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m29827_gshared (Collection_1_t3556 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Collection_1_Contains_m29827(__this, ___item, method) (( bool (*) (Collection_1_t3556 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Collection_1_Contains_m29827_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m29828_gshared (Collection_1_t3556 * __this, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m29828(__this, ___array, ___index, method) (( void (*) (Collection_1_t3556 *, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, const MethodInfo*))Collection_1_CopyTo_m29828_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m29829_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m29829(__this, method) (( Object_t* (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_GetEnumerator_m29829_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m29830_gshared (Collection_1_t3556 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m29830(__this, ___item, method) (( int32_t (*) (Collection_1_t3556 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Collection_1_IndexOf_m29830_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m29831_gshared (Collection_1_t3556 * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Collection_1_Insert_m29831(__this, ___index, ___item, method) (( void (*) (Collection_1_t3556 *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Collection_1_Insert_m29831_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m29832_gshared (Collection_1_t3556 * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m29832(__this, ___index, ___item, method) (( void (*) (Collection_1_t3556 *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Collection_1_InsertItem_m29832_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m29833_gshared (Collection_1_t3556 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Collection_1_Remove_m29833(__this, ___item, method) (( bool (*) (Collection_1_t3556 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Collection_1_Remove_m29833_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m29834_gshared (Collection_1_t3556 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m29834(__this, ___index, method) (( void (*) (Collection_1_t3556 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m29834_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m29835_gshared (Collection_1_t3556 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m29835(__this, ___index, method) (( void (*) (Collection_1_t3556 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m29835_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m29836_gshared (Collection_1_t3556 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m29836(__this, method) (( int32_t (*) (Collection_1_t3556 *, const MethodInfo*))Collection_1_get_Count_m29836_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t2100  Collection_1_get_Item_m29837_gshared (Collection_1_t3556 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m29837(__this, ___index, method) (( CustomAttributeNamedArgument_t2100  (*) (Collection_1_t3556 *, int32_t, const MethodInfo*))Collection_1_get_Item_m29837_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m29838_gshared (Collection_1_t3556 * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m29838(__this, ___index, ___value, method) (( void (*) (Collection_1_t3556 *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Collection_1_set_Item_m29838_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m29839_gshared (Collection_1_t3556 * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m29839(__this, ___index, ___item, method) (( void (*) (Collection_1_t3556 *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Collection_1_SetItem_m29839_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m29840_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m29840(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m29840_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeNamedArgument_t2100  Collection_1_ConvertItem_m29841_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m29841(__this /* static, unused */, ___item, method) (( CustomAttributeNamedArgument_t2100  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m29841_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m29842_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m29842(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m29842_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m29843_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m29843(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m29843_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m29844_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m29844(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m29844_gshared)(__this /* static, unused */, ___list, method)
