﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TerrainCollider
struct TerrainCollider_t1312;
// UnityEngine.TerrainData
struct TerrainData_t1331;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.TerrainCollider::set_terrainData(UnityEngine.TerrainData)
extern "C" void TerrainCollider_set_terrainData_m7139 (TerrainCollider_t1312 * __this, TerrainData_t1331 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
