﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t25;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Bezier
struct  Bezier_t376  : public MonoBehaviour_t18
{
	// System.Int32 Bezier::iterations
	int32_t ___iterations_1;
	// UnityEngine.Transform Bezier::p1
	Transform_t25 * ___p1_2;
	// UnityEngine.Transform Bezier::p2
	Transform_t25 * ___p2_3;
	// UnityEngine.Transform Bezier::p3
	Transform_t25 * ___p3_4;
	// UnityEngine.Transform Bezier::p4
	Transform_t25 * ___p4_5;
};
