﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_3.h"

// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C" void Nullable_1__ctor_m18512_gshared (Nullable_1_t555 * __this, bool ___value, const MethodInfo* method);
#define Nullable_1__ctor_m18512(__this, ___value, method) (( void (*) (Nullable_1_t555 *, bool, const MethodInfo*))Nullable_1__ctor_m18512_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m2512_gshared (Nullable_1_t555 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2512(__this, method) (( bool (*) (Nullable_1_t555 *, const MethodInfo*))Nullable_1_get_HasValue_m2512_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::get_Value()
extern "C" bool Nullable_1_get_Value_m2513_gshared (Nullable_1_t555 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2513(__this, method) (( bool (*) (Nullable_1_t555 *, const MethodInfo*))Nullable_1_get_Value_m2513_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m18513_gshared (Nullable_1_t555 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18513(__this, ___other, method) (( bool (*) (Nullable_1_t555 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m18513_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m18514_gshared (Nullable_1_t555 * __this, Nullable_1_t555  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18514(__this, ___other, method) (( bool (*) (Nullable_1_t555 *, Nullable_1_t555 , const MethodInfo*))Nullable_1_Equals_m18514_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m18515_gshared (Nullable_1_t555 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m18515(__this, method) (( int32_t (*) (Nullable_1_t555 *, const MethodInfo*))Nullable_1_GetHashCode_m18515_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::GetValueOrDefault()
extern "C" bool Nullable_1_GetValueOrDefault_m18516_gshared (Nullable_1_t555 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m18516(__this, method) (( bool (*) (Nullable_1_t555 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m18516_gshared)(__this, method)
// System.String System.Nullable`1<System.Boolean>::ToString()
extern "C" String_t* Nullable_1_ToString_m18517_gshared (Nullable_1_t555 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m18517(__this, method) (( String_t* (*) (Nullable_1_t555 *, const MethodInfo*))Nullable_1_ToString_m18517_gshared)(__this, method)
