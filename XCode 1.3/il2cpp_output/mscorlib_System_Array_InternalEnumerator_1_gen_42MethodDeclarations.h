﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m22410(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3039 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15544_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22411(__this, method) (( void (*) (InternalEnumerator_1_t3039 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15545_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22412(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3039 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15546_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m22413(__this, method) (( void (*) (InternalEnumerator_1_t3039 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15547_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22414(__this, method) (( bool (*) (InternalEnumerator_1_t3039 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15548_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m22415(__this, method) (( HideExcessAreaAbstractBehaviour_t413 * (*) (InternalEnumerator_1_t3039 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15549_gshared)(__this, method)
