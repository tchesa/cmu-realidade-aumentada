﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MultiplayerManagerExample
struct MultiplayerManagerExample_t191;
// UnionAssets.FLE.CEvent
struct CEvent_t74;

#include "codegen/il2cpp-codegen.h"

// System.Void MultiplayerManagerExample::.ctor()
extern "C" void MultiplayerManagerExample__ctor_m1088 (MultiplayerManagerExample_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::Awake()
extern "C" void MultiplayerManagerExample_Awake_m1089 (MultiplayerManagerExample_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::OnGUI()
extern "C" void MultiplayerManagerExample_OnGUI_m1090 (MultiplayerManagerExample_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::OnGCPlayerConnected(UnionAssets.FLE.CEvent)
extern "C" void MultiplayerManagerExample_OnGCPlayerConnected_m1091 (MultiplayerManagerExample_t191 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::OnGCPlayerDisconnected(UnionAssets.FLE.CEvent)
extern "C" void MultiplayerManagerExample_OnGCPlayerDisconnected_m1092 (MultiplayerManagerExample_t191 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::OnGCMatchStart(UnionAssets.FLE.CEvent)
extern "C" void MultiplayerManagerExample_OnGCMatchStart_m1093 (MultiplayerManagerExample_t191 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::OnGCDataReceived(UnionAssets.FLE.CEvent)
extern "C" void MultiplayerManagerExample_OnGCDataReceived_m1094 (MultiplayerManagerExample_t191 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
