﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m22221(__this, ___host, method) (( void (*) (Enumerator_t1124 *, Dictionary_2_t885 *, const MethodInfo*))Enumerator__ctor_m16177_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22222(__this, method) (( Object_t * (*) (Enumerator_t1124 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16178_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m22223(__this, method) (( void (*) (Enumerator_t1124 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m6456(__this, method) (( void (*) (Enumerator_t1124 *, const MethodInfo*))Enumerator_Dispose_m16180_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m6455(__this, method) (( bool (*) (Enumerator_t1124 *, const MethodInfo*))Enumerator_MoveNext_m16181_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m6454(__this, method) (( VideoBackgroundAbstractBehaviour_t445 * (*) (Enumerator_t1124 *, const MethodInfo*))Enumerator_get_Current_m16182_gshared)(__this, method)
