﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m25745(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3226 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15555_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25746(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3226 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15556_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25747(__this, method) (( void (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15557_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25748(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3226 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15558_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25749(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3226 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15559_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25750(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3226 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15560_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25751(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3226 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15561_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25752(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3226 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15562_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25753(__this, method) (( bool (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15563_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25754(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3226 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15564_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25755(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15565_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m25756(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3226 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15566_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m25757(__this, method) (( void (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15567_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m25758(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3226 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15568_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25759(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3226 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15569_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m25760(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3226 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15570_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m25761(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3226 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15571_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25762(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3226 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15572_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25763(__this, method) (( bool (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15573_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25764(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15574_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25765(__this, method) (( bool (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15575_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25766(__this, method) (( bool (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15576_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m25767(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3226 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15577_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m25768(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3226 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15578_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::Contains(T)
#define ReadOnlyCollection_1_Contains_m25769(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3226 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15579_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m25770(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3226 *, SurfaceU5BU5D_t3209*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15580_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m25771(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m25772(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3226 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15582_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::get_Count()
#define ReadOnlyCollection_1_get_Count_m25773(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3226 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15583_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m25774(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3226 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15584_gshared)(__this, ___index, method)
