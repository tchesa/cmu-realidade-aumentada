﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AppRequestResult
struct AppRequestResult_t277;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t221;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.AppRequestResult::.ctor(System.String)
extern "C" void AppRequestResult__ctor_m1587 (AppRequestResult_t277 * __this, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppRequestResult::get_RequestID()
extern "C" String_t* AppRequestResult_get_RequestID_m1588 (AppRequestResult_t277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppRequestResult::set_RequestID(System.String)
extern "C" void AppRequestResult_set_RequestID_m1589 (AppRequestResult_t277 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AppRequestResult::get_To()
extern "C" Object_t* AppRequestResult_get_To_m1590 (AppRequestResult_t277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppRequestResult::set_To(System.Collections.Generic.IEnumerable`1<System.String>)
extern "C" void AppRequestResult_set_To_m1591 (AppRequestResult_t277 * __this, Object_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
