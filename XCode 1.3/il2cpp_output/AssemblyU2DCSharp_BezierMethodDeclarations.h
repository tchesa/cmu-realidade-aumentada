﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Bezier
struct Bezier_t376;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Bezier::.ctor()
extern "C" void Bezier__ctor_m1931 (Bezier_t376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Bezier::GetBezierPosition(System.Single)
extern "C" Vector3_t6  Bezier_GetBezierPosition_m1932 (Bezier_t376 * __this, float ___progress, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bezier::OnDrawGizmos()
extern "C" void Bezier_OnDrawGizmos_m1933 (Bezier_t376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
