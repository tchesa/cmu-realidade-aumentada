﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupCreateResult>
struct FacebookDelegate_1_t471;
// Facebook.Unity.FacebookBase
struct FacebookBase_t227;
// Facebook.Unity.MethodArguments
struct MethodArguments_t248;

#include "mscorlib_System_Object.h"

// Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupCreateResult>
struct  MethodCall_1_t2768  : public Object_t
{
	// System.String Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupCreateResult>::<MethodName>k__BackingField
	String_t* ___U3CMethodNameU3Ek__BackingField_0;
	// Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupCreateResult>::<Callback>k__BackingField
	FacebookDelegate_1_t471 * ___U3CCallbackU3Ek__BackingField_1;
	// Facebook.Unity.FacebookBase Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupCreateResult>::<FacebookImpl>k__BackingField
	FacebookBase_t227 * ___U3CFacebookImplU3Ek__BackingField_2;
	// Facebook.Unity.MethodArguments Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupCreateResult>::<Parameters>k__BackingField
	MethodArguments_t248 * ___U3CParametersU3Ek__BackingField_3;
};
