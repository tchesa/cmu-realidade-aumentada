﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t457;
// Vuforia.Word
struct Word_t1026;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t25;
// UnityEngine.GameObject
struct GameObject_t27;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"

// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void WordAbstractBehaviour_InternalUnregisterTrackable_m6409 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern "C" Object_t * WordAbstractBehaviour_get_Word_m6410 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern "C" String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m6411 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m6412 (WordAbstractBehaviour_t457 * __this, String_t* ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern "C" int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m6413 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m6414 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m6415 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m6416 (WordAbstractBehaviour_t457 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m6417 (WordAbstractBehaviour_t457 * __this, Object_t * ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C" void WordAbstractBehaviour__ctor_m2794 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m6418 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m6419 (WordAbstractBehaviour_t457 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t25 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m6420 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t27 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m6421 (WordAbstractBehaviour_t457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
