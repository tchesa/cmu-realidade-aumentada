﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.EditorFacebook
struct EditorFacebook_t266;
// System.String
struct String_t;
// Facebook.Unity.IFacebookCallbackHandler
struct IFacebookCallbackHandler_t483;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t242;
// Facebook.Unity.InitDelegate
struct InitDelegate_t241;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t221;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>
struct FacebookDelegate_1_t465;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t476;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>
struct FacebookDelegate_1_t468;
// System.Uri
struct Uri_t292;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>
struct FacebookDelegate_1_t469;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupCreateResult>
struct FacebookDelegate_1_t471;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupJoinResult>
struct FacebookDelegate_1_t472;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>
struct FacebookDelegate_1_t473;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t475;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppInviteResult>
struct FacebookDelegate_1_t479;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>
struct FacebookDelegate_1_t470;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode.h"
#include "mscorlib_System_Nullable_1_gen_0.h"
#include "mscorlib_System_Nullable_1_gen_1.h"
#include "mscorlib_System_Nullable_1_gen_2.h"

// System.Void Facebook.Unity.Editor.EditorFacebook::.ctor()
extern "C" void EditorFacebook__ctor_m1514 (EditorFacebook_t266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.Editor.EditorFacebook::get_LimitEventUsage()
extern "C" bool EditorFacebook_get_LimitEventUsage_m1515 (EditorFacebook_t266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::set_LimitEventUsage(System.Boolean)
extern "C" void EditorFacebook_set_LimitEventUsage_m1516 (EditorFacebook_t266 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.ShareDialogMode Facebook.Unity.Editor.EditorFacebook::get_ShareDialogMode()
extern "C" int32_t EditorFacebook_get_ShareDialogMode_m1517 (EditorFacebook_t266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern "C" void EditorFacebook_set_ShareDialogMode_m1518 (EditorFacebook_t266 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Editor.EditorFacebook::get_SDKName()
extern "C" String_t* EditorFacebook_get_SDKName_m1519 (EditorFacebook_t266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Editor.EditorFacebook::get_SDKVersion()
extern "C" String_t* EditorFacebook_get_SDKVersion_m1520 (EditorFacebook_t266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.IFacebookCallbackHandler Facebook.Unity.Editor.EditorFacebook::get_EditorGameObject()
extern "C" Object_t * EditorFacebook_get_EditorGameObject_m1521 (EditorFacebook_t266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::Init(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.Boolean,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern "C" void EditorFacebook_Init_m1522 (EditorFacebook_t266 * __this, String_t* ___appId, bool ___cookie, bool ___logging, bool ___status, bool ___xfbml, String_t* ___channelUrl, String_t* ___authResponse, bool ___frictionlessRequests, HideUnityDelegate_t242 * ___hideUnityDelegate, InitDelegate_t241 * ___onInitComplete, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern "C" void EditorFacebook_LogInWithReadPermissions_m1523 (EditorFacebook_t266 * __this, Object_t* ___permissions, FacebookDelegate_1_t465 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern "C" void EditorFacebook_LogInWithPublishPermissions_m1524 (EditorFacebook_t266 * __this, Object_t* ___permissions, FacebookDelegate_1_t465 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern "C" void EditorFacebook_AppRequest_m1525 (EditorFacebook_t266 * __this, String_t* ___message, Nullable_1_t466  ___actionType, String_t* ___objectId, Object_t* ___to, Object_t* ___filters, Object_t* ___excludeIds, Nullable_1_t467  ___maxRecipients, String_t* ___data, String_t* ___title, FacebookDelegate_1_t468 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern "C" void EditorFacebook_ShareLink_m1526 (EditorFacebook_t266 * __this, Uri_t292 * ___contentURL, String_t* ___contentTitle, String_t* ___contentDescription, Uri_t292 * ___photoURL, FacebookDelegate_1_t469 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern "C" void EditorFacebook_FeedShare_m1527 (EditorFacebook_t266 * __this, String_t* ___toId, Uri_t292 * ___link, String_t* ___linkName, String_t* ___linkCaption, String_t* ___linkDescription, Uri_t292 * ___picture, String_t* ___mediaSource, FacebookDelegate_1_t469 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::GameGroupCreate(System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupCreateResult>)
extern "C" void EditorFacebook_GameGroupCreate_m1528 (EditorFacebook_t266 * __this, String_t* ___name, String_t* ___description, String_t* ___privacy, FacebookDelegate_1_t471 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::GameGroupJoin(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupJoinResult>)
extern "C" void EditorFacebook_GameGroupJoin_m1529 (EditorFacebook_t266 * __this, String_t* ___id, FacebookDelegate_1_t472 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::ActivateApp(System.String)
extern "C" void EditorFacebook_ActivateApp_m1530 (EditorFacebook_t266 * __this, String_t* ___appId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern "C" void EditorFacebook_GetAppLink_m1531 (EditorFacebook_t266 * __this, FacebookDelegate_1_t473 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" void EditorFacebook_AppEventsLogEvent_m1532 (EditorFacebook_t266 * __this, String_t* ___logEvent, Nullable_1_t474  ___valueToSum, Dictionary_2_t475 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" void EditorFacebook_AppEventsLogPurchase_m1533 (EditorFacebook_t266 * __this, float ___logPurchase, String_t* ___currency, Dictionary_2_t475 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::AppInvite(System.Uri,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppInviteResult>)
extern "C" void EditorFacebook_AppInvite_m1534 (EditorFacebook_t266 * __this, Uri_t292 * ___appLinkUrl, Uri_t292 * ___previewImageUrl, FacebookDelegate_1_t479 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern "C" void EditorFacebook_FetchDeferredAppLink_m1535 (EditorFacebook_t266 * __this, FacebookDelegate_1_t473 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern "C" void EditorFacebook_Pay_m1536 (EditorFacebook_t266 * __this, String_t* ___product, String_t* ___action, int32_t ___quantity, Nullable_1_t467  ___quantityMin, Nullable_1_t467  ___quantityMax, String_t* ___requestId, String_t* ___pricepointId, String_t* ___testCurrency, FacebookDelegate_1_t470 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnAppRequestsComplete(System.String)
extern "C" void EditorFacebook_OnAppRequestsComplete_m1537 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnGetAppLinkComplete(System.String)
extern "C" void EditorFacebook_OnGetAppLinkComplete_m1538 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnGroupCreateComplete(System.String)
extern "C" void EditorFacebook_OnGroupCreateComplete_m1539 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnGroupJoinComplete(System.String)
extern "C" void EditorFacebook_OnGroupJoinComplete_m1540 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnLoginComplete(System.String)
extern "C" void EditorFacebook_OnLoginComplete_m1541 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnShareLinkComplete(System.String)
extern "C" void EditorFacebook_OnShareLinkComplete_m1542 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnAppInviteComplete(System.String)
extern "C" void EditorFacebook_OnAppInviteComplete_m1543 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnFetchDeferredAppLinkComplete(System.String)
extern "C" void EditorFacebook_OnFetchDeferredAppLinkComplete_m1544 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnPayComplete(System.String)
extern "C" void EditorFacebook_OnPayComplete_m1545 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnFacebookAuthResponseChange(System.String)
extern "C" void EditorFacebook_OnFacebookAuthResponseChange_m1546 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebook::OnUrlResponse(System.String)
extern "C" void EditorFacebook_OnUrlResponse_m1547 (EditorFacebook_t266 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
