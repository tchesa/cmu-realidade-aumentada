﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "UnityEngine_UnityEngine_iPhoneGeneration.h"

// UnityEngine.iPhoneGeneration
struct  iPhoneGeneration_t501 
{
	// System.Int32 UnityEngine.iPhoneGeneration::value__
	int32_t ___value___1;
};
