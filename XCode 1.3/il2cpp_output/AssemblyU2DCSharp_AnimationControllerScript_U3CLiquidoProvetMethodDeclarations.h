﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript/<LiquidoProveta>c__Iterator13
struct U3CLiquidoProvetaU3Ec__Iterator13_t368;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript/<LiquidoProveta>c__Iterator13::.ctor()
extern "C" void U3CLiquidoProvetaU3Ec__Iterator13__ctor_m1864 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<LiquidoProveta>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLiquidoProvetaU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1865 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<LiquidoProveta>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLiquidoProvetaU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m1866 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationControllerScript/<LiquidoProveta>c__Iterator13::MoveNext()
extern "C" bool U3CLiquidoProvetaU3Ec__Iterator13_MoveNext_m1867 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<LiquidoProveta>c__Iterator13::Dispose()
extern "C" void U3CLiquidoProvetaU3Ec__Iterator13_Dispose_m1868 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<LiquidoProveta>c__Iterator13::Reset()
extern "C" void U3CLiquidoProvetaU3Ec__Iterator13_Reset_m1869 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
