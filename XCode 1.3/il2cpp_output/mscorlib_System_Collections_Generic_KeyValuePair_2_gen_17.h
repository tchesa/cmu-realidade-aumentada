﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSStoreProductView
struct IOSStoreProductView_t134;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,IOSStoreProductView>
struct  KeyValuePair_2_t2734 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,IOSStoreProductView>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,IOSStoreProductView>::value
	IOSStoreProductView_t134 * ___value_1;
};
