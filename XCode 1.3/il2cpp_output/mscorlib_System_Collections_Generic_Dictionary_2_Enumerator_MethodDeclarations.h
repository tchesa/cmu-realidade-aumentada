﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m17762(__this, ___dictionary, method) (( void (*) (Enumerator_t503 *, Dictionary_2_t174 *, const MethodInfo*))Enumerator__ctor_m16419_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17763(__this, method) (( Object_t * (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16420_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m17764(__this, method) (( void (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16421_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17765(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16422_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17766(__this, method) (( Object_t * (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16423_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17767(__this, method) (( Object_t * (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16424_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::MoveNext()
#define Enumerator_MoveNext_m2283(__this, method) (( bool (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_MoveNext_m16425_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::get_Current()
#define Enumerator_get_Current_m2281(__this, method) (( KeyValuePair_2_t502  (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_get_Current_m16426_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17768(__this, method) (( int32_t (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_get_CurrentKey_m16427_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17769(__this, method) (( iAdBanner_t172 * (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_get_CurrentValue_m16428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::Reset()
#define Enumerator_Reset_m17770(__this, method) (( void (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_Reset_m16429_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::VerifyState()
#define Enumerator_VerifyState_m17771(__this, method) (( void (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_VerifyState_m16430_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17772(__this, method) (( void (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_VerifyCurrent_m16431_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,iAdBanner>::Dispose()
#define Enumerator_Dispose_m17773(__this, method) (( void (*) (Enumerator_t503 *, const MethodInfo*))Enumerator_Dispose_m16432_gshared)(__this, method)
