﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSProductTemplate
struct IOSProductTemplate_t135;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSProductTemplate::.ctor()
extern "C" void IOSProductTemplate__ctor_m780 (IOSProductTemplate_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
