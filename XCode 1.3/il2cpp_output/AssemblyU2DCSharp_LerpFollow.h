﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t25;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// LerpFollow
struct  LerpFollow_t382  : public MonoBehaviour_t18
{
	// UnityEngine.Transform LerpFollow::target
	Transform_t25 * ___target_1;
	// System.Single LerpFollow::movementSpeed
	float ___movementSpeed_2;
	// System.Single LerpFollow::rotationSpeed
	float ___rotationSpeed_3;
};
