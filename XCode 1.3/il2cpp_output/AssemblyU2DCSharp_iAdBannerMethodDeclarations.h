﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iAdBanner
struct iAdBanner_t172;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"

// System.Void iAdBanner::.ctor(UnityEngine.TextAnchor,System.Int32)
extern "C" void iAdBanner__ctor_m941 (iAdBanner_t172 * __this, int32_t ___anchor, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" void iAdBanner__ctor_m942 (iAdBanner_t172 * __this, int32_t ___x, int32_t ___y, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::_IADCreateBannerAd(System.Int32,System.Int32)
extern "C" void iAdBanner__IADCreateBannerAd_m943 (Object_t * __this /* static, unused */, int32_t ___gravity, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::_IADCreateBannerAdPos(System.Int32,System.Int32,System.Int32)
extern "C" void iAdBanner__IADCreateBannerAdPos_m944 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::_IADShowAd(System.Int32)
extern "C" void iAdBanner__IADShowAd_m945 (Object_t * __this /* static, unused */, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::_IADHideAd(System.Int32)
extern "C" void iAdBanner__IADHideAd_m946 (Object_t * __this /* static, unused */, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::Hide()
extern "C" void iAdBanner_Hide_m947 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::Show()
extern "C" void iAdBanner_Show_m948 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iAdBanner::get_id()
extern "C" int32_t iAdBanner_get_id_m949 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iAdBanner::get_IsLoaded()
extern "C" bool iAdBanner_get_IsLoaded_m950 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iAdBanner::get_IsOnScreen()
extern "C" bool iAdBanner_get_IsOnScreen_m951 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iAdBanner::get_ShowOnLoad()
extern "C" bool iAdBanner_get_ShowOnLoad_m952 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::set_ShowOnLoad(System.Boolean)
extern "C" void iAdBanner_set_ShowOnLoad_m953 (iAdBanner_t172 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextAnchor iAdBanner::get_anchor()
extern "C" int32_t iAdBanner_get_anchor_m954 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iAdBanner::get_gravity()
extern "C" int32_t iAdBanner_get_gravity_m955 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::didFailToReceiveAdWithError()
extern "C" void iAdBanner_didFailToReceiveAdWithError_m956 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::bannerViewDidLoadAd()
extern "C" void iAdBanner_bannerViewDidLoadAd_m957 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::bannerViewWillLoadAd()
extern "C" void iAdBanner_bannerViewWillLoadAd_m958 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::bannerViewActionDidFinish()
extern "C" void iAdBanner_bannerViewActionDidFinish_m959 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::bannerViewActionShouldBegin()
extern "C" void iAdBanner_bannerViewActionShouldBegin_m960 (iAdBanner_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::<AdLoadedAction>m__26()
extern "C" void iAdBanner_U3CAdLoadedActionU3Em__26_m961 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::<FailToReceiveAdAction>m__27()
extern "C" void iAdBanner_U3CFailToReceiveAdActionU3Em__27_m962 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::<AdWiewLoadedAction>m__28()
extern "C" void iAdBanner_U3CAdWiewLoadedActionU3Em__28_m963 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::<AdViewActionBeginAction>m__29()
extern "C" void iAdBanner_U3CAdViewActionBeginActionU3Em__29_m964 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBanner::<AdViewFinishedAction>m__2A()
extern "C" void iAdBanner_U3CAdViewFinishedActionU3Em__2A_m965 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
