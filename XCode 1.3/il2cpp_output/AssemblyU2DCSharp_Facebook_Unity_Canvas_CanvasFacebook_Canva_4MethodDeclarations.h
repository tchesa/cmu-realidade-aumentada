﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<System.Object>
struct CanvasUIMethodCall_1_t2764;
// Facebook.Unity.Canvas.CanvasFacebook
struct CanvasFacebook_t226;
// System.String
struct String_t;
// Facebook.Unity.MethodArguments
struct MethodArguments_t248;
// Facebook.Unity.FacebookDelegate`1<System.Object>
struct FacebookDelegate_1_t573;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<System.Object>::.ctor(Facebook.Unity.Canvas.CanvasFacebook,System.String,System.String)
extern "C" void CanvasUIMethodCall_1__ctor_m18261_gshared (CanvasUIMethodCall_1_t2764 * __this, CanvasFacebook_t226 * ___canvasImpl, String_t* ___methodName, String_t* ___callbackMethod, const MethodInfo* method);
#define CanvasUIMethodCall_1__ctor_m18261(__this, ___canvasImpl, ___methodName, ___callbackMethod, method) (( void (*) (CanvasUIMethodCall_1_t2764 *, CanvasFacebook_t226 *, String_t*, String_t*, const MethodInfo*))CanvasUIMethodCall_1__ctor_m18261_gshared)(__this, ___canvasImpl, ___methodName, ___callbackMethod, method)
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<System.Object>::Call(Facebook.Unity.MethodArguments)
extern "C" void CanvasUIMethodCall_1_Call_m18263_gshared (CanvasUIMethodCall_1_t2764 * __this, MethodArguments_t248 * ___args, const MethodInfo* method);
#define CanvasUIMethodCall_1_Call_m18263(__this, ___args, method) (( void (*) (CanvasUIMethodCall_1_t2764 *, MethodArguments_t248 *, const MethodInfo*))CanvasUIMethodCall_1_Call_m18263_gshared)(__this, ___args, method)
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<System.Object>::UI(System.String,Facebook.Unity.MethodArguments,Facebook.Unity.FacebookDelegate`1<T>)
extern "C" void CanvasUIMethodCall_1_UI_m18265_gshared (CanvasUIMethodCall_1_t2764 * __this, String_t* ___method, MethodArguments_t248 * ___args, FacebookDelegate_1_t573 * ___callback, const MethodInfo* method);
#define CanvasUIMethodCall_1_UI_m18265(__this, ___method, ___args, ___callback, method) (( void (*) (CanvasUIMethodCall_1_t2764 *, String_t*, MethodArguments_t248 *, FacebookDelegate_1_t573 *, const MethodInfo*))CanvasUIMethodCall_1_UI_m18265_gshared)(__this, ___method, ___args, ___callback, method)
