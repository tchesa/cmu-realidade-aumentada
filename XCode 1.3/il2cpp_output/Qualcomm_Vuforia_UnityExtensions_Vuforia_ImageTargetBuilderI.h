﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.TrackableSource
struct TrackableSource_t947;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"

// Vuforia.ImageTargetBuilderImpl
struct  ImageTargetBuilderImpl_t946  : public ImageTargetBuilder_t935
{
	// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::mTrackableSource
	TrackableSource_t947 * ___mTrackableSource_0;
};
