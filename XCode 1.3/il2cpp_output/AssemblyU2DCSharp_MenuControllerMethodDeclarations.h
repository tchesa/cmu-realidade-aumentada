﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuController
struct MenuController_t338;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuController::.ctor()
extern "C" void MenuController__ctor_m1801 (MenuController_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::Start()
extern "C" void MenuController_Start_m1802 (MenuController_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::Play_OnClick()
extern "C" void MenuController_Play_OnClick_m1803 (MenuController_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::Info_OnClick()
extern "C" void MenuController_Info_OnClick_m1804 (MenuController_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::CMU_OnClick()
extern "C" void MenuController_CMU_OnClick_m1805 (MenuController_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::Info_Back_OnClick()
extern "C" void MenuController_Info_Back_OnClick_m1806 (MenuController_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
