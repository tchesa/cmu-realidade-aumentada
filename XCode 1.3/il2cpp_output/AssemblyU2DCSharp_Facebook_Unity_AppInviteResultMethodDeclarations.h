﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AppInviteResult
struct AppInviteResult_t274;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.AppInviteResult::.ctor(System.String)
extern "C" void AppInviteResult__ctor_m1577 (AppInviteResult_t274 * __this, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
