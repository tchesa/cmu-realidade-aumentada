﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_92.h"
#include "System_System_Text_RegularExpressions_Mark.h"

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29361_gshared (InternalEnumerator_1_t3501 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29361(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3501 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29361_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29362_gshared (InternalEnumerator_1_t3501 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29362(__this, method) (( void (*) (InternalEnumerator_1_t3501 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29362_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29363_gshared (InternalEnumerator_1_t3501 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29363(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3501 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29363_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29364_gshared (InternalEnumerator_1_t3501 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29364(__this, method) (( void (*) (InternalEnumerator_1_t3501 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29364_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29365_gshared (InternalEnumerator_1_t3501 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29365(__this, method) (( bool (*) (InternalEnumerator_1_t3501 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29365_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern "C" Mark_t1605  InternalEnumerator_1_get_Current_m29366_gshared (InternalEnumerator_1_t3501 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29366(__this, method) (( Mark_t1605  (*) (InternalEnumerator_1_t3501 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29366_gshared)(__this, method)
