﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.IO.StringReader
struct StringReader_t303;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t28;

#include "mscorlib_System_Object.h"

// Facebook.MiniJSON.Json/Parser
struct  Parser_t302  : public Object_t
{
	// System.IO.StringReader Facebook.MiniJSON.Json/Parser::json
	StringReader_t303 * ___json_2;
};
struct Parser_t302_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Facebook.MiniJSON.Json/Parser::<>f__switch$map1
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map1_3;
};
