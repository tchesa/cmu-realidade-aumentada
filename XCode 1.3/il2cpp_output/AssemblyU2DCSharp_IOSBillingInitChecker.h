﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSBillingInitChecker/BillingInitListener
struct BillingInitListener_t125;

#include "mscorlib_System_Object.h"

// IOSBillingInitChecker
struct  IOSBillingInitChecker_t126  : public Object_t
{
	// IOSBillingInitChecker/BillingInitListener IOSBillingInitChecker::_listener
	BillingInitListener_t125 * ____listener_0;
};
