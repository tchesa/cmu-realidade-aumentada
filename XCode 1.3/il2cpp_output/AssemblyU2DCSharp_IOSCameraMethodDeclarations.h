﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSCamera
struct IOSCamera_t145;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;
// UnityEngine.Texture2D
struct Texture2D_t33;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// IOSImagePickResult
struct IOSImagePickResult_t149;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSCamera::.ctor()
extern "C" void IOSCamera__ctor_m820 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_SaveToCameraRoll(System.String)
extern "C" void IOSCamera__ISN_SaveToCameraRoll_m821 (Object_t * __this /* static, unused */, String_t* ___encodedMedia, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_GetImageFromCamera()
extern "C" void IOSCamera__ISN_GetImageFromCamera_m822 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_GetVideoPathFromAlbum()
extern "C" void IOSCamera__ISN_GetVideoPathFromAlbum_m823 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_GetImageFromAlbum()
extern "C" void IOSCamera__ISN_GetImageFromAlbum_m824 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_InitCameraAPI(System.Single,System.Int32,System.Int32)
extern "C" void IOSCamera__ISN_InitCameraAPI_m825 (Object_t * __this /* static, unused */, float ___compressionRate, int32_t ___maxSize, int32_t ___encodingType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::Awake()
extern "C" void IOSCamera_Awake_m826 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::SaveTextureToCameraRoll(System.Byte[])
extern "C" void IOSCamera_SaveTextureToCameraRoll_m827 (IOSCamera_t145 * __this, ByteU5BU5D_t119* ___texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::SaveTextureToCameraRoll(UnityEngine.Texture2D)
extern "C" void IOSCamera_SaveTextureToCameraRoll_m828 (IOSCamera_t145 * __this, Texture2D_t33 * ___texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::SaveScreenshotToCameraRoll()
extern "C" void IOSCamera_SaveScreenshotToCameraRoll_m829 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::GetVideoPathFromAlbum()
extern "C" void IOSCamera_GetVideoPathFromAlbum_m830 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::GetImageFromCamera()
extern "C" void IOSCamera_GetImageFromCamera_m831 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::GetImageFromAlbum()
extern "C" void IOSCamera_GetImageFromAlbum_m832 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::OnImagePickedEvent(System.String)
extern "C" void IOSCamera_OnImagePickedEvent_m833 (IOSCamera_t145 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::OnImageSaveFailed()
extern "C" void IOSCamera_OnImageSaveFailed_m834 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::OnImageSaveSuccess()
extern "C" void IOSCamera_OnImageSaveSuccess_m835 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSCamera::SaveScreenshot()
extern "C" Object_t * IOSCamera_SaveScreenshot_m836 (IOSCamera_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::<OnImagePicked>m__1B(IOSImagePickResult)
extern "C" void IOSCamera_U3COnImagePickedU3Em__1B_m837 (Object_t * __this /* static, unused */, IOSImagePickResult_t149 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::<OnImageSaved>m__1C(ISN_Result)
extern "C" void IOSCamera_U3COnImageSavedU3Em__1C_m838 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
