﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t26;
// UnityEngine.GameObject
struct GameObject_t27;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t19;
// iTween/EasingFunction
struct EasingFunction_t9;
// iTween/ApplyTween
struct ApplyTween_t13;
// UnityEngine.AudioSource
struct AudioSource_t20;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t8;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t21;
// UnityEngine.Color[,]
struct ColorU5BU2CU5D_t22;
// System.Single[]
struct SingleU5BU5D_t23;
// UnityEngine.Rect[]
struct RectU5BU5D_t24;
// iTween/CRSpline
struct CRSpline_t7;
// UnityEngine.Transform
struct Transform_t25;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t28;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EaseType.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_LoopType.h"
#include "UnityEngine_UnityEngine_Space.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_NamedValueColor.h"

// iTween
struct  iTween_t15  : public MonoBehaviour_t18
{
	// System.String iTween::id
	String_t* ___id_3;
	// System.String iTween::type
	String_t* ___type_4;
	// System.String iTween::method
	String_t* ___method_5;
	// iTween/EaseType iTween::easeType
	int32_t ___easeType_6;
	// System.Single iTween::time
	float ___time_7;
	// System.Single iTween::delay
	float ___delay_8;
	// iTween/LoopType iTween::loopType
	int32_t ___loopType_9;
	// System.Boolean iTween::isRunning
	bool ___isRunning_10;
	// System.Boolean iTween::isPaused
	bool ___isPaused_11;
	// System.String iTween::_name
	String_t* ____name_12;
	// System.Single iTween::runningTime
	float ___runningTime_13;
	// System.Single iTween::percentage
	float ___percentage_14;
	// System.Single iTween::delayStarted
	float ___delayStarted_15;
	// System.Boolean iTween::kinematic
	bool ___kinematic_16;
	// System.Boolean iTween::isLocal
	bool ___isLocal_17;
	// System.Boolean iTween::loop
	bool ___loop_18;
	// System.Boolean iTween::reverse
	bool ___reverse_19;
	// System.Boolean iTween::wasPaused
	bool ___wasPaused_20;
	// System.Boolean iTween::physics
	bool ___physics_21;
	// System.Collections.Hashtable iTween::tweenArguments
	Hashtable_t19 * ___tweenArguments_22;
	// UnityEngine.Space iTween::space
	int32_t ___space_23;
	// iTween/EasingFunction iTween::ease
	EasingFunction_t9 * ___ease_24;
	// iTween/ApplyTween iTween::apply
	ApplyTween_t13 * ___apply_25;
	// UnityEngine.AudioSource iTween::audioSource
	AudioSource_t20 * ___audioSource_26;
	// UnityEngine.Vector3[] iTween::vector3s
	Vector3U5BU5D_t8* ___vector3s_27;
	// UnityEngine.Vector2[] iTween::vector2s
	Vector2U5BU5D_t21* ___vector2s_28;
	// UnityEngine.Color[,] iTween::colors
	ColorU5BU2CU5D_t22* ___colors_29;
	// System.Single[] iTween::floats
	SingleU5BU5D_t23* ___floats_30;
	// UnityEngine.Rect[] iTween::rects
	RectU5BU5D_t24* ___rects_31;
	// iTween/CRSpline iTween::path
	CRSpline_t7 * ___path_32;
	// UnityEngine.Vector3 iTween::preUpdate
	Vector3_t6  ___preUpdate_33;
	// UnityEngine.Vector3 iTween::postUpdate
	Vector3_t6  ___postUpdate_34;
	// iTween/NamedValueColor iTween::namedcolorvalue
	int32_t ___namedcolorvalue_35;
	// System.Single iTween::lastRealTime
	float ___lastRealTime_36;
	// System.Boolean iTween::useRealTime
	bool ___useRealTime_37;
	// UnityEngine.Transform iTween::thisTransform
	Transform_t25 * ___thisTransform_38;
};
struct iTween_t15_StaticFields{
	// System.Collections.Generic.List`1<System.Collections.Hashtable> iTween::tweens
	List_1_t26 * ___tweens_1;
	// UnityEngine.GameObject iTween::cameraFade
	GameObject_t27 * ___cameraFade_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map0
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map0_39;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map1
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map1_40;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map2
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map2_41;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map3
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map3_42;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map4
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map4_43;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map5
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map5_44;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map6
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map6_45;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map7
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map7_46;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map8
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map8_47;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map9
	Dictionary_2_t28 * ___U3CU3Ef__switchU24map9_48;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$mapA
	Dictionary_2_t28 * ___U3CU3Ef__switchU24mapA_49;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$mapB
	Dictionary_2_t28 * ___U3CU3Ef__switchU24mapB_50;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$mapC
	Dictionary_2_t28 * ___U3CU3Ef__switchU24mapC_51;
};
