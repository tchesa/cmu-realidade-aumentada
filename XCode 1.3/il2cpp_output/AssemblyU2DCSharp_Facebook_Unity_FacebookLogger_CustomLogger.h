﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.IFacebookLogger
struct IFacebookLogger_t297;

#include "mscorlib_System_Object.h"

// Facebook.Unity.FacebookLogger/CustomLogger
struct  CustomLogger_t296  : public Object_t
{
	// Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger/CustomLogger::logger
	Object_t * ___logger_0;
};
