﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Terrain
struct Terrain_t1334;
// UnityEngine.TerrainData
struct TerrainData_t1331;
// UnityEngine.Material
struct Material_t45;
// UnityEngine.Terrain[]
struct TerrainU5BU5D_t1444;
// UnityEngine.GameObject
struct GameObject_t27;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_TerrainRenderFlags.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_TreeInstance.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_TerrainChangedFlags.h"

// System.Void UnityEngine.Terrain::.ctor()
extern "C" void Terrain__ctor_m7224 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.Terrain::get_InstanceObject()
extern "C" IntPtr_t Terrain_get_InstanceObject_m7225 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_InstanceObject(System.IntPtr)
extern "C" void Terrain_set_InstanceObject_m7226 (Terrain_t1334 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::OnDestroy()
extern "C" void Terrain_OnDestroy_m7227 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::MakeSureObjectIsAlive()
extern "C" void Terrain_MakeSureObjectIsAlive_m7228 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Cleanup(System.IntPtr)
extern "C" void Terrain_Cleanup_m7229 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TerrainRenderFlags UnityEngine.Terrain::get_editorRenderFlags()
extern "C" int32_t Terrain_get_editorRenderFlags_m7230 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_editorRenderFlags(UnityEngine.TerrainRenderFlags)
extern "C" void Terrain_set_editorRenderFlags_m7231 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::GetEditorRenderFlags(System.IntPtr)
extern "C" int32_t Terrain_GetEditorRenderFlags_m7232 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::SetEditorRenderFlags(System.IntPtr,System.Int32)
extern "C" void Terrain_SetEditorRenderFlags_m7233 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TerrainData UnityEngine.Terrain::get_terrainData()
extern "C" TerrainData_t1331 * Terrain_get_terrainData_m7234 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_terrainData(UnityEngine.TerrainData)
extern "C" void Terrain_set_terrainData_m7235 (Terrain_t1334 * __this, TerrainData_t1331 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TerrainData UnityEngine.Terrain::Internal_GetTerrainData(System.IntPtr)
extern "C" TerrainData_t1331 * Terrain_Internal_GetTerrainData_m7236 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetTerrainData(System.IntPtr,UnityEngine.TerrainData)
extern "C" void Terrain_Internal_SetTerrainData_m7237 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, TerrainData_t1331 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::get_treeDistance()
extern "C" float Terrain_get_treeDistance_m7238 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_treeDistance(System.Single)
extern "C" void Terrain_set_treeDistance_m7239 (Terrain_t1334 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::Internal_GetTreeDistance(System.IntPtr)
extern "C" float Terrain_Internal_GetTreeDistance_m7240 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetTreeDistance(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetTreeDistance_m7241 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::get_treeBillboardDistance()
extern "C" float Terrain_get_treeBillboardDistance_m7242 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_treeBillboardDistance(System.Single)
extern "C" void Terrain_set_treeBillboardDistance_m7243 (Terrain_t1334 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::Internal_GetTreeBillboardDistance(System.IntPtr)
extern "C" float Terrain_Internal_GetTreeBillboardDistance_m7244 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetTreeBillboardDistance(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetTreeBillboardDistance_m7245 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::get_treeCrossFadeLength()
extern "C" float Terrain_get_treeCrossFadeLength_m7246 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_treeCrossFadeLength(System.Single)
extern "C" void Terrain_set_treeCrossFadeLength_m7247 (Terrain_t1334 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::Internal_GetTreeCrossFadeLength(System.IntPtr)
extern "C" float Terrain_Internal_GetTreeCrossFadeLength_m7248 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetTreeCrossFadeLength(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetTreeCrossFadeLength_m7249 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::get_treeMaximumFullLODCount()
extern "C" int32_t Terrain_get_treeMaximumFullLODCount_m7250 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_treeMaximumFullLODCount(System.Int32)
extern "C" void Terrain_set_treeMaximumFullLODCount_m7251 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::Internal_GetTreeMaximumFullLODCount(System.IntPtr)
extern "C" int32_t Terrain_Internal_GetTreeMaximumFullLODCount_m7252 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetTreeMaximumFullLODCount(System.IntPtr,System.Int32)
extern "C" void Terrain_Internal_SetTreeMaximumFullLODCount_m7253 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::get_detailObjectDistance()
extern "C" float Terrain_get_detailObjectDistance_m7254 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_detailObjectDistance(System.Single)
extern "C" void Terrain_set_detailObjectDistance_m7255 (Terrain_t1334 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::Internal_GetDetailObjectDistance(System.IntPtr)
extern "C" float Terrain_Internal_GetDetailObjectDistance_m7256 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetDetailObjectDistance(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetDetailObjectDistance_m7257 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::get_detailObjectDensity()
extern "C" float Terrain_get_detailObjectDensity_m7258 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_detailObjectDensity(System.Single)
extern "C" void Terrain_set_detailObjectDensity_m7259 (Terrain_t1334 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::Internal_GetDetailObjectDensity(System.IntPtr)
extern "C" float Terrain_Internal_GetDetailObjectDensity_m7260 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetDetailObjectDensity(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetDetailObjectDensity_m7261 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::get_heightmapPixelError()
extern "C" float Terrain_get_heightmapPixelError_m7262 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_heightmapPixelError(System.Single)
extern "C" void Terrain_set_heightmapPixelError_m7263 (Terrain_t1334 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::Internal_GetHeightmapPixelError(System.IntPtr)
extern "C" float Terrain_Internal_GetHeightmapPixelError_m7264 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetHeightmapPixelError(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetHeightmapPixelError_m7265 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::get_heightmapMaximumLOD()
extern "C" int32_t Terrain_get_heightmapMaximumLOD_m7266 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_heightmapMaximumLOD(System.Int32)
extern "C" void Terrain_set_heightmapMaximumLOD_m7267 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::Internal_GetHeightmapMaximumLOD(System.IntPtr)
extern "C" int32_t Terrain_Internal_GetHeightmapMaximumLOD_m7268 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetHeightmapMaximumLOD(System.IntPtr,System.Int32)
extern "C" void Terrain_Internal_SetHeightmapMaximumLOD_m7269 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::get_basemapDistance()
extern "C" float Terrain_get_basemapDistance_m7270 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_basemapDistance(System.Single)
extern "C" void Terrain_set_basemapDistance_m7271 (Terrain_t1334 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::Internal_GetBasemapDistance(System.IntPtr)
extern "C" float Terrain_Internal_GetBasemapDistance_m7272 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetBasemapDistance(System.IntPtr,System.Single)
extern "C" void Terrain_Internal_SetBasemapDistance_m7273 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::get_splatmapDistance()
extern "C" float Terrain_get_splatmapDistance_m7274 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_splatmapDistance(System.Single)
extern "C" void Terrain_set_splatmapDistance_m7275 (Terrain_t1334 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::get_lightmapIndex()
extern "C" int32_t Terrain_get_lightmapIndex_m7276 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_lightmapIndex(System.Int32)
extern "C" void Terrain_set_lightmapIndex_m7277 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::SetLightmapIndex(System.Int32)
extern "C" void Terrain_SetLightmapIndex_m7278 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::ShiftLightmapIndex(System.Int32)
extern "C" void Terrain_ShiftLightmapIndex_m7279 (Terrain_t1334 * __this, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::Internal_GetLightmapIndex(System.IntPtr)
extern "C" int32_t Terrain_Internal_GetLightmapIndex_m7280 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetLightmapIndex(System.IntPtr,System.Int32)
extern "C" void Terrain_Internal_SetLightmapIndex_m7281 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::get_lightmapSize()
extern "C" int32_t Terrain_get_lightmapSize_m7282 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_lightmapSize(System.Int32)
extern "C" void Terrain_set_lightmapSize_m7283 (Terrain_t1334 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Terrain::Internal_GetLightmapSize(System.IntPtr)
extern "C" int32_t Terrain_Internal_GetLightmapSize_m7284 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetLightmapSize(System.IntPtr,System.Int32)
extern "C" void Terrain_Internal_SetLightmapSize_m7285 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Terrain::get_castShadows()
extern "C" bool Terrain_get_castShadows_m7286 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_castShadows(System.Boolean)
extern "C" void Terrain_set_castShadows_m7287 (Terrain_t1334 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Terrain::Internal_GetCastShadows(System.IntPtr)
extern "C" bool Terrain_Internal_GetCastShadows_m7288 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetCastShadows(System.IntPtr,System.Boolean)
extern "C" void Terrain_Internal_SetCastShadows_m7289 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Terrain::get_materialTemplate()
extern "C" Material_t45 * Terrain_get_materialTemplate_m7290 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_materialTemplate(UnityEngine.Material)
extern "C" void Terrain_set_materialTemplate_m7291 (Terrain_t1334 * __this, Material_t45 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Terrain::Internal_GetMaterialTemplate(System.IntPtr)
extern "C" Material_t45 * Terrain_Internal_GetMaterialTemplate_m7292 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetMaterialTemplate(System.IntPtr,UnityEngine.Material)
extern "C" void Terrain_Internal_SetMaterialTemplate_m7293 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, Material_t45 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Terrain::get_drawTreesAndFoliage()
extern "C" bool Terrain_get_drawTreesAndFoliage_m7294 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_drawTreesAndFoliage(System.Boolean)
extern "C" void Terrain_set_drawTreesAndFoliage_m7295 (Terrain_t1334 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Terrain::Internal_GetDrawTreesAndFoliage(System.IntPtr)
extern "C" bool Terrain_Internal_GetDrawTreesAndFoliage_m7296 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetDrawTreesAndFoliage(System.IntPtr,System.Boolean)
extern "C" void Terrain_Internal_SetDrawTreesAndFoliage_m7297 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Terrain::get_collectDetailPatches()
extern "C" bool Terrain_get_collectDetailPatches_m7298 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_collectDetailPatches(System.Boolean)
extern "C" void Terrain_set_collectDetailPatches_m7299 (Terrain_t1334 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Terrain::Internal_GetCollectDetailPatches(System.IntPtr)
extern "C" bool Terrain_Internal_GetCollectDetailPatches_m7300 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetCollectDetailPatches(System.IntPtr,System.Boolean)
extern "C" void Terrain_Internal_SetCollectDetailPatches_m7301 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::SampleHeight(UnityEngine.Vector3)
extern "C" float Terrain_SampleHeight_m7302 (Terrain_t1334 * __this, Vector3_t6  ___worldPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::Internal_SampleHeight(System.IntPtr,UnityEngine.Vector3)
extern "C" float Terrain_Internal_SampleHeight_m7303 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, Vector3_t6  ___worldPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Terrain::INTERNAL_CALL_Internal_SampleHeight(UnityEngine.Terrain,System.IntPtr,UnityEngine.Vector3&)
extern "C" float Terrain_INTERNAL_CALL_Internal_SampleHeight_m7304 (Object_t * __this /* static, unused */, Terrain_t1334 * ___self, IntPtr_t ___terrainInstance, Vector3_t6 * ___worldPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::ApplyDelayedHeightmapModification()
extern "C" void Terrain_ApplyDelayedHeightmapModification_m7305 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_ApplyDelayedHeightmapModification(System.IntPtr)
extern "C" void Terrain_Internal_ApplyDelayedHeightmapModification_m7306 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::AddTreeInstance(UnityEngine.TreeInstance)
extern "C" void Terrain_AddTreeInstance_m7307 (Terrain_t1334 * __this, TreeInstance_t1330  ___instance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_AddTreeInstance(System.IntPtr,UnityEngine.TreeInstance)
extern "C" void Terrain_Internal_AddTreeInstance_m7308 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, TreeInstance_t1330  ___instance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::INTERNAL_CALL_Internal_AddTreeInstance(UnityEngine.Terrain,System.IntPtr,UnityEngine.TreeInstance&)
extern "C" void Terrain_INTERNAL_CALL_Internal_AddTreeInstance_m7309 (Object_t * __this /* static, unused */, Terrain_t1334 * ___self, IntPtr_t ___terrainInstance, TreeInstance_t1330 * ___instance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::SetNeighbors(UnityEngine.Terrain,UnityEngine.Terrain,UnityEngine.Terrain,UnityEngine.Terrain)
extern "C" void Terrain_SetNeighbors_m7310 (Terrain_t1334 * __this, Terrain_t1334 * ___left, Terrain_t1334 * ___top, Terrain_t1334 * ___right, Terrain_t1334 * ___bottom, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_SetNeighbors(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void Terrain_Internal_SetNeighbors_m7311 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, IntPtr_t ___left, IntPtr_t ___top, IntPtr_t ___right, IntPtr_t ___bottom, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Terrain::GetPosition()
extern "C" Vector3_t6  Terrain_GetPosition_m7312 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Terrain::Internal_GetPosition(System.IntPtr)
extern "C" Vector3_t6  Terrain_Internal_GetPosition_m7313 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Flush()
extern "C" void Terrain_Flush_m7314 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_Flush(System.IntPtr)
extern "C" void Terrain_Internal_Flush_m7315 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::RemoveTrees(UnityEngine.Vector2,System.Single,System.Int32)
extern "C" void Terrain_RemoveTrees_m7316 (Terrain_t1334 * __this, Vector2_t29  ___position, float ___radius, int32_t ___prototypeIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_RemoveTrees(System.IntPtr,UnityEngine.Vector2,System.Single,System.Int32)
extern "C" void Terrain_Internal_RemoveTrees_m7317 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, Vector2_t29  ___position, float ___radius, int32_t ___prototypeIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::INTERNAL_CALL_Internal_RemoveTrees(UnityEngine.Terrain,System.IntPtr,UnityEngine.Vector2&,System.Single,System.Int32)
extern "C" void Terrain_INTERNAL_CALL_Internal_RemoveTrees_m7318 (Object_t * __this /* static, unused */, Terrain_t1334 * ___self, IntPtr_t ___terrainInstance, Vector2_t29 * ___position, float ___radius, int32_t ___prototypeIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::OnTerrainChanged(UnityEngine.TerrainChangedFlags)
extern "C" void Terrain_OnTerrainChanged_m7319 (Terrain_t1334 * __this, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_OnTerrainChanged(System.IntPtr,UnityEngine.TerrainChangedFlags)
extern "C" void Terrain_Internal_OnTerrainChanged_m7320 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.Terrain::Construct()
extern "C" IntPtr_t Terrain_Construct_m7321 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::OnEnable()
extern "C" void Terrain_OnEnable_m7322 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_OnEnable(System.IntPtr)
extern "C" void Terrain_Internal_OnEnable_m7323 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::OnDisable()
extern "C" void Terrain_OnDisable_m7324 (Terrain_t1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::Internal_OnDisable(System.IntPtr)
extern "C" void Terrain_Internal_OnDisable_m7325 (Terrain_t1334 * __this, IntPtr_t ___terrainInstance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Terrain UnityEngine.Terrain::get_activeTerrain()
extern "C" Terrain_t1334 * Terrain_get_activeTerrain_m7326 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Terrain[] UnityEngine.Terrain::get_activeTerrains()
extern "C" TerrainU5BU5D_t1444* Terrain_get_activeTerrains_m7327 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Terrain::CreateTerrainGameObject(UnityEngine.TerrainData)
extern "C" GameObject_t27 * Terrain_CreateTerrainGameObject_m7328 (Object_t * __this /* static, unused */, TerrainData_t1331 * ___assignTerrain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::ReconnectTerrainData()
extern "C" void Terrain_ReconnectTerrainData_m7329 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
