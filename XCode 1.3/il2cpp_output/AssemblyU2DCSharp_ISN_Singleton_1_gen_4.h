﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSVideoManager
struct IOSVideoManager_t161;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// ISN_Singleton`1<IOSVideoManager>
struct  ISN_Singleton_1_t162  : public EventDispatcher_t69
{
};
struct ISN_Singleton_1_t162_StaticFields{
	// T ISN_Singleton`1<IOSVideoManager>::_instance
	IOSVideoManager_t161 * ____instance_3;
	// System.Boolean ISN_Singleton`1<IOSVideoManager>::applicationIsQuitting
	bool ___applicationIsQuitting_4;
};
