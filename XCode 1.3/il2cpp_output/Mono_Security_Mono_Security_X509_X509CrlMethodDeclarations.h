﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Crl
struct X509Crl_t1658;
// System.Byte[]
struct ByteU5BU5D_t119;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1681;
// System.String
struct String_t;
// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t1660;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1548;
// System.Security.Cryptography.DSA
struct DSA_t1654;
// System.Security.Cryptography.RSA
struct RSA_t1655;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1536;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.X509.X509Crl::.ctor(System.Byte[])
extern "C" void X509Crl__ctor_m9560 (X509Crl_t1658 * __this, ByteU5BU5D_t119* ___crl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl::Parse(System.Byte[])
extern "C" void X509Crl_Parse_m9561 (X509Crl_t1658 * __this, ByteU5BU5D_t119* ___crl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::get_Extensions()
extern "C" X509ExtensionCollection_t1681 * X509Crl_get_Extensions_m9237 (X509Crl_t1658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_Hash()
extern "C" ByteU5BU5D_t119* X509Crl_get_Hash_m9562 (X509Crl_t1658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::get_IssuerName()
extern "C" String_t* X509Crl_get_IssuerName_m9245 (X509Crl_t1658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl::get_NextUpdate()
extern "C" DateTime_t220  X509Crl_get_NextUpdate_m9243 (X509Crl_t1658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::Compare(System.Byte[],System.Byte[])
extern "C" bool X509Crl_Compare_m9563 (X509Crl_t1658 * __this, ByteU5BU5D_t119* ___array1, ByteU5BU5D_t119* ___array2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(Mono.Security.X509.X509Certificate)
extern "C" X509CrlEntry_t1660 * X509Crl_GetCrlEntry_m9241 (X509Crl_t1658 * __this, X509Certificate_t1548 * ___x509, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(System.Byte[])
extern "C" X509CrlEntry_t1660 * X509Crl_GetCrlEntry_m9564 (X509Crl_t1658 * __this, ByteU5BU5D_t119* ___serialNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::GetHashName()
extern "C" String_t* X509Crl_GetHashName_m9565 (X509Crl_t1658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.DSA)
extern "C" bool X509Crl_VerifySignature_m9566 (X509Crl_t1658 * __this, DSA_t1654 * ___dsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.RSA)
extern "C" bool X509Crl_VerifySignature_m9567 (X509Crl_t1658 * __this, RSA_t1655 * ___rsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" bool X509Crl_VerifySignature_m9240 (X509Crl_t1658 * __this, AsymmetricAlgorithm_t1536 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
