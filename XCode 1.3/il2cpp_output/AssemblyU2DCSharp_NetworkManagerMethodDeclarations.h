﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkManager
struct NetworkManager_t196;
// BasePackage
struct BasePackage_t199;

#include "codegen/il2cpp-codegen.h"

// System.Void NetworkManager::.ctor()
extern "C" void NetworkManager__ctor_m1108 (NetworkManager_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkManager::send(BasePackage)
extern "C" void NetworkManager_send_m1109 (Object_t * __this /* static, unused */, BasePackage_t199 * ___pack, const MethodInfo* method) IL2CPP_METHOD_ATTR;
