﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>
struct List_1_t461;
// UnionAssets.FLE.DataEventHandlerFunction
struct DataEventHandlerFunction_t459;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.DataEventHandlerFunction>
struct  Enumerator_t2679 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.DataEventHandlerFunction>::l
	List_1_t461 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.DataEventHandlerFunction>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.DataEventHandlerFunction>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.DataEventHandlerFunction>::current
	DataEventHandlerFunction_t459 * ___current_3;
};
