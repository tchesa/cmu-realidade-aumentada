﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AsyncRequestString
struct AsyncRequestString_t290;
// System.Uri
struct Uri_t292;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t288;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t294;
// UnityEngine.WWWForm
struct WWWForm_t293;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t295;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_HttpMethod.h"

// System.Void Facebook.Unity.AsyncRequestString::.ctor()
extern "C" void AsyncRequestString__ctor_m1640 (AsyncRequestString_t290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AsyncRequestString::Post(System.Uri,System.Collections.Generic.Dictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern "C" void AsyncRequestString_Post_m1641 (Object_t * __this /* static, unused */, Uri_t292 * ___url, Dictionary_2_t288 * ___formData, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AsyncRequestString::Get(System.Uri,System.Collections.Generic.Dictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern "C" void AsyncRequestString_Get_m1642 (Object_t * __this /* static, unused */, Uri_t292 * ___url, Dictionary_2_t288 * ___formData, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AsyncRequestString::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern "C" void AsyncRequestString_Request_m1643 (Object_t * __this /* static, unused */, Uri_t292 * ___url, int32_t ___method, WWWForm_t293 * ___query, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AsyncRequestString::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern "C" void AsyncRequestString_Request_m1644 (Object_t * __this /* static, unused */, Uri_t292 * ___url, int32_t ___method, Object_t* ___formData, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Facebook.Unity.AsyncRequestString::Start()
extern "C" Object_t * AsyncRequestString_Start_m1645 (AsyncRequestString_t290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetUrl(System.Uri)
extern "C" AsyncRequestString_t290 * AsyncRequestString_SetUrl_m1646 (AsyncRequestString_t290 * __this, Uri_t292 * ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetMethod(Facebook.Unity.HttpMethod)
extern "C" AsyncRequestString_t290 * AsyncRequestString_SetMethod_m1647 (AsyncRequestString_t290 * __this, int32_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetFormData(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C" AsyncRequestString_t290 * AsyncRequestString_SetFormData_m1648 (AsyncRequestString_t290 * __this, Object_t* ___formData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetQuery(UnityEngine.WWWForm)
extern "C" AsyncRequestString_t290 * AsyncRequestString_SetQuery_m1649 (AsyncRequestString_t290 * __this, WWWForm_t293 * ___query, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetCallback(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern "C" AsyncRequestString_t290 * AsyncRequestString_SetCallback_m1650 (AsyncRequestString_t290 * __this, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
