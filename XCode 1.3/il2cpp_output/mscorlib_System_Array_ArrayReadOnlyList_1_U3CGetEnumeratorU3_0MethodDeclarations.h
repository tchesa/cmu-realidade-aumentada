﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t3555;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m29773_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3555 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m29773(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3555 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m29773_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" CustomAttributeTypedArgument_t2101  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29774_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3555 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29774(__this, method) (( CustomAttributeTypedArgument_t2101  (*) (U3CGetEnumeratorU3Ec__Iterator0_t3555 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29774_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29775_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3555 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29775(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3555 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29775_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29776_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3555 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29776(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t3555 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29776_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29777_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3555 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29777(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3555 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29777_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m29778_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3555 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m29778(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3555 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m29778_gshared)(__this, method)
