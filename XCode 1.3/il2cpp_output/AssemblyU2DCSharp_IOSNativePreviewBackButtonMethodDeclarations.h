﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativePreviewBackButton
struct IOSNativePreviewBackButton_t213;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNativePreviewBackButton::.ctor()
extern "C" void IOSNativePreviewBackButton__ctor_m1191 (IOSNativePreviewBackButton_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSNativePreviewBackButton IOSNativePreviewBackButton::Create()
extern "C" IOSNativePreviewBackButton_t213 * IOSNativePreviewBackButton_Create_m1192 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePreviewBackButton::Awake()
extern "C" void IOSNativePreviewBackButton_Awake_m1193 (IOSNativePreviewBackButton_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePreviewBackButton::OnGUI()
extern "C" void IOSNativePreviewBackButton_OnGUI_m1194 (IOSNativePreviewBackButton_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
