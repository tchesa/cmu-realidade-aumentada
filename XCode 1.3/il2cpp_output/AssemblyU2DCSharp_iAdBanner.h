﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t81;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcherBase.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"

// iAdBanner
struct  iAdBanner_t172  : public EventDispatcherBase_t72
{
	// System.Boolean iAdBanner::_IsLoaded
	bool ____IsLoaded_2;
	// System.Boolean iAdBanner::_IsOnScreen
	bool ____IsOnScreen_3;
	// System.Boolean iAdBanner::firstLoad
	bool ___firstLoad_4;
	// System.Boolean iAdBanner::_ShowOnLoad
	bool ____ShowOnLoad_5;
	// System.Int32 iAdBanner::_id
	int32_t ____id_6;
	// UnityEngine.TextAnchor iAdBanner::_anchor
	int32_t ____anchor_7;
	// System.Action iAdBanner::AdLoadedAction
	Action_t81 * ___AdLoadedAction_8;
	// System.Action iAdBanner::FailToReceiveAdAction
	Action_t81 * ___FailToReceiveAdAction_9;
	// System.Action iAdBanner::AdWiewLoadedAction
	Action_t81 * ___AdWiewLoadedAction_10;
	// System.Action iAdBanner::AdViewActionBeginAction
	Action_t81 * ___AdViewActionBeginAction_11;
	// System.Action iAdBanner::AdViewFinishedAction
	Action_t81 * ___AdViewFinishedAction_12;
};
struct iAdBanner_t172_StaticFields{
	// System.Action iAdBanner::<>f__am$cacheB
	Action_t81 * ___U3CU3Ef__amU24cacheB_13;
	// System.Action iAdBanner::<>f__am$cacheC
	Action_t81 * ___U3CU3Ef__amU24cacheC_14;
	// System.Action iAdBanner::<>f__am$cacheD
	Action_t81 * ___U3CU3Ef__amU24cacheD_15;
	// System.Action iAdBanner::<>f__am$cacheE
	Action_t81 * ___U3CU3Ef__amU24cacheE_16;
	// System.Action iAdBanner::<>f__am$cacheF
	Action_t81 * ___U3CU3Ef__amU24cacheF_17;
};
