﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t33;

#include "AssemblyU2DCSharp_BaseIOSFeaturePreview.h"

// NativeIOSActionsExample
struct  NativeIOSActionsExample_t208  : public BaseIOSFeaturePreview_t183
{
	// UnityEngine.Texture2D NativeIOSActionsExample::hello_texture
	Texture2D_t33 * ___hello_texture_11;
	// UnityEngine.Texture2D NativeIOSActionsExample::drawTexture
	Texture2D_t33 * ___drawTexture_12;
};
