﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>
struct List_1_t247;
// Facebook.Unity.FacebookSettings/UrlSchemes
struct UrlSchemes_t245;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<Facebook.Unity.FacebookSettings/UrlSchemes>
struct  Enumerator_t2774 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Facebook.Unity.FacebookSettings/UrlSchemes>::l
	List_1_t247 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Facebook.Unity.FacebookSettings/UrlSchemes>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Facebook.Unity.FacebookSettings/UrlSchemes>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Facebook.Unity.FacebookSettings/UrlSchemes>::current
	UrlSchemes_t245 * ___current_3;
};
