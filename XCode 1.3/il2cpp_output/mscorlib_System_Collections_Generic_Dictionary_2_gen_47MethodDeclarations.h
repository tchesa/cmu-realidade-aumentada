﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3290;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2606;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t3735;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct IEnumerator_1_t3736;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct KeyCollection_t3295;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ValueCollection_t3299;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_42.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__42.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor()
extern "C" void Dictionary_2__ctor_m26546_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m26546(__this, method) (( void (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2__ctor_m26546_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m26548_gshared (Dictionary_2_t3290 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m26548(__this, ___comparer, method) (( void (*) (Dictionary_2_t3290 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m26548_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m26550_gshared (Dictionary_2_t3290 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m26550(__this, ___capacity, method) (( void (*) (Dictionary_2_t3290 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m26550_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m26552_gshared (Dictionary_2_t3290 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m26552(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3290 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m26552_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m26554_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m26554(__this, method) (( Object_t * (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m26554_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m26556_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m26556(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m26556_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m26558_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m26558(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3290 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m26558_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m26560_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m26560(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3290 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m26560_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m26562_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m26562(__this, ___key, method) (( bool (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m26562_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m26564_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m26564(__this, ___key, method) (( void (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m26564_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26566_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26566(__this, method) (( bool (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26566_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26568_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26568(__this, method) (( Object_t * (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26570_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26570(__this, method) (( bool (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26570_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26572_gshared (Dictionary_2_t3290 * __this, KeyValuePair_2_t3292  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26572(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3290 *, KeyValuePair_2_t3292 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26572_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26574_gshared (Dictionary_2_t3290 * __this, KeyValuePair_2_t3292  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26574(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3290 *, KeyValuePair_2_t3292 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26574_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26576_gshared (Dictionary_2_t3290 * __this, KeyValuePair_2U5BU5D_t3735* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26576(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3290 *, KeyValuePair_2U5BU5D_t3735*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26576_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26578_gshared (Dictionary_2_t3290 * __this, KeyValuePair_2_t3292  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26578(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3290 *, KeyValuePair_2_t3292 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26578_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m26580_gshared (Dictionary_2_t3290 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m26580(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3290 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m26580_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26582_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26582(__this, method) (( Object_t * (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26582_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26584_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26584(__this, method) (( Object_t* (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26584_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26586_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26586(__this, method) (( Object_t * (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26586_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m26588_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m26588(__this, method) (( int32_t (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_get_Count_m26588_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Item(TKey)
extern "C" ProfileData_t1064  Dictionary_2_get_Item_m26590_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m26590(__this, ___key, method) (( ProfileData_t1064  (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m26590_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m26592_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, ProfileData_t1064  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m26592(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3290 *, Object_t *, ProfileData_t1064 , const MethodInfo*))Dictionary_2_set_Item_m26592_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m26594_gshared (Dictionary_2_t3290 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m26594(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3290 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m26594_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m26596_gshared (Dictionary_2_t3290 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m26596(__this, ___size, method) (( void (*) (Dictionary_2_t3290 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m26596_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m26598_gshared (Dictionary_2_t3290 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m26598(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3290 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m26598_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3292  Dictionary_2_make_pair_m26600_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t1064  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m26600(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3292  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t1064 , const MethodInfo*))Dictionary_2_make_pair_m26600_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m26602_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t1064  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m26602(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t1064 , const MethodInfo*))Dictionary_2_pick_key_m26602_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_value(TKey,TValue)
extern "C" ProfileData_t1064  Dictionary_2_pick_value_m26604_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t1064  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m26604(__this /* static, unused */, ___key, ___value, method) (( ProfileData_t1064  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t1064 , const MethodInfo*))Dictionary_2_pick_value_m26604_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m26606_gshared (Dictionary_2_t3290 * __this, KeyValuePair_2U5BU5D_t3735* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m26606(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3290 *, KeyValuePair_2U5BU5D_t3735*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m26606_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Resize()
extern "C" void Dictionary_2_Resize_m26608_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m26608(__this, method) (( void (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_Resize_m26608_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m26610_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, ProfileData_t1064  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m26610(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3290 *, Object_t *, ProfileData_t1064 , const MethodInfo*))Dictionary_2_Add_m26610_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Clear()
extern "C" void Dictionary_2_Clear_m26612_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m26612(__this, method) (( void (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_Clear_m26612_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m26614_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m26614(__this, ___key, method) (( bool (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m26614_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m26616_gshared (Dictionary_2_t3290 * __this, ProfileData_t1064  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m26616(__this, ___value, method) (( bool (*) (Dictionary_2_t3290 *, ProfileData_t1064 , const MethodInfo*))Dictionary_2_ContainsValue_m26616_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m26618_gshared (Dictionary_2_t3290 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m26618(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3290 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m26618_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m26620_gshared (Dictionary_2_t3290 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m26620(__this, ___sender, method) (( void (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m26620_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m26622_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m26622(__this, ___key, method) (( bool (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m26622_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m26624_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, ProfileData_t1064 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m26624(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3290 *, Object_t *, ProfileData_t1064 *, const MethodInfo*))Dictionary_2_TryGetValue_m26624_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Keys()
extern "C" KeyCollection_t3295 * Dictionary_2_get_Keys_m26626_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m26626(__this, method) (( KeyCollection_t3295 * (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_get_Keys_m26626_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Values()
extern "C" ValueCollection_t3299 * Dictionary_2_get_Values_m26628_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m26628(__this, method) (( ValueCollection_t3299 * (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_get_Values_m26628_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m26630_gshared (Dictionary_2_t3290 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m26630(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m26630_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTValue(System.Object)
extern "C" ProfileData_t1064  Dictionary_2_ToTValue_m26632_gshared (Dictionary_2_t3290 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m26632(__this, ___value, method) (( ProfileData_t1064  (*) (Dictionary_2_t3290 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m26632_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m26634_gshared (Dictionary_2_t3290 * __this, KeyValuePair_2_t3292  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m26634(__this, ___pair, method) (( bool (*) (Dictionary_2_t3290 *, KeyValuePair_2_t3292 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m26634_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
extern "C" Enumerator_t3297  Dictionary_2_GetEnumerator_m26636_gshared (Dictionary_2_t3290 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m26636(__this, method) (( Enumerator_t3297  (*) (Dictionary_2_t3290 *, const MethodInfo*))Dictionary_2_GetEnumerator_m26636_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m26638_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t1064  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m26638(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t1064 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m26638_gshared)(__this /* static, unused */, ___key, ___value, method)
