﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19
struct U3CITakeScreenshotU3Ec__Iterator19_t393;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::.ctor()
extern "C" void U3CITakeScreenshotU3Ec__Iterator19__ctor_m2007 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CITakeScreenshotU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2008 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CITakeScreenshotU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m2009 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::MoveNext()
extern "C" bool U3CITakeScreenshotU3Ec__Iterator19_MoveNext_m2010 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::Dispose()
extern "C" void U3CITakeScreenshotU3Ec__Iterator19_Dispose_m2011 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::Reset()
extern "C" void U3CITakeScreenshotU3Ec__Iterator19_Reset_m2012 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
