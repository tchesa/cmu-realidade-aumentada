﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t917;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t919;

#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::get_SmartTerrainTrackable()
extern "C" Object_t * SmartTerrainTrackableBehaviour_get_SmartTerrainTrackable_m4730 (SmartTerrainTrackableBehaviour_t917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::get_AutomaticUpdatesDisabled()
extern "C" bool SmartTerrainTrackableBehaviour_get_AutomaticUpdatesDisabled_m4731 (SmartTerrainTrackableBehaviour_t917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders()
extern "C" void SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m4732 (SmartTerrainTrackableBehaviour_t917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::SetAutomaticUpdatesDisabled(System.Boolean)
extern "C" void SmartTerrainTrackableBehaviour_SetAutomaticUpdatesDisabled_m4733 (SmartTerrainTrackableBehaviour_t917 * __this, bool ___disabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::Start()
extern "C" void SmartTerrainTrackableBehaviour_Start_m4734 (SmartTerrainTrackableBehaviour_t917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::.ctor()
extern "C" void SmartTerrainTrackableBehaviour__ctor_m4735 (SmartTerrainTrackableBehaviour_t917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
