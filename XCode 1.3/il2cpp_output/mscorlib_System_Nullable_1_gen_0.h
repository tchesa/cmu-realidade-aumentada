﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "AssemblyU2DCSharp_Facebook_Unity_OGActionType.h"

// System.Nullable`1<Facebook.Unity.OGActionType>
struct  Nullable_1_t466 
{
	// T System.Nullable`1<Facebook.Unity.OGActionType>::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::has_value
	bool ___has_value_1;
};
