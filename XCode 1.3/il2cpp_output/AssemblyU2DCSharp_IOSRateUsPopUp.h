﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<IOSDialogResult>
struct Action_1_t165;

#include "AssemblyU2DCSharp_BaseIOSPopup.h"

// IOSRateUsPopUp
struct  IOSRateUsPopUp_t167  : public BaseIOSPopup_t164
{
	// System.String IOSRateUsPopUp::rate
	String_t* ___rate_5;
	// System.String IOSRateUsPopUp::remind
	String_t* ___remind_6;
	// System.String IOSRateUsPopUp::declined
	String_t* ___declined_7;
	// System.Action`1<IOSDialogResult> IOSRateUsPopUp::OnComplete
	Action_1_t165 * ___OnComplete_8;
};
struct IOSRateUsPopUp_t167_StaticFields{
	// System.Action`1<IOSDialogResult> IOSRateUsPopUp::<>f__am$cache4
	Action_1_t165 * ___U3CU3Ef__amU24cache4_9;
};
