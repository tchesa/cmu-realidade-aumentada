﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t33;
// System.Object
struct Object_t;
// ArUiController
struct ArUiController_t374;

#include "mscorlib_System_Object.h"

// ArUiController/<ITakeScreenshot>c__Iterator17
struct  U3CITakeScreenshotU3Ec__Iterator17_t375  : public Object_t
{
	// UnityEngine.Texture2D ArUiController/<ITakeScreenshot>c__Iterator17::<tex>__0
	Texture2D_t33 * ___U3CtexU3E__0_0;
	// System.Int32 ArUiController/<ITakeScreenshot>c__Iterator17::$PC
	int32_t ___U24PC_1;
	// System.Object ArUiController/<ITakeScreenshot>c__Iterator17::$current
	Object_t * ___U24current_2;
	// ArUiController ArUiController/<ITakeScreenshot>c__Iterator17::<>f__this
	ArUiController_t374 * ___U3CU3Ef__this_3;
};
