﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX3_Demo_Translate
struct CFX3_Demo_Translate_t317;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX3_Demo_Translate::.ctor()
extern "C" void CFX3_Demo_Translate__ctor_m1739 (CFX3_Demo_Translate_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo_Translate::Start()
extern "C" void CFX3_Demo_Translate_Start_m1740 (CFX3_Demo_Translate_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo_Translate::Update()
extern "C" void CFX3_Demo_Translate_Update_m1741 (CFX3_Demo_Translate_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
