﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<ISN_Result>
struct Action_1_t99;
// System.Action
struct Action_t81;
// System.Action`1<iCloudData>
struct Action_1_t182;

#include "AssemblyU2DCSharp_ISN_Singleton_1_gen_5.h"

// iCloudManager
struct  iCloudManager_t180  : public ISN_Singleton_1_t181
{
	// System.Action`1<ISN_Result> iCloudManager::OnCloundInitAction
	Action_1_t99 * ___OnCloundInitAction_9;
	// System.Action iCloudManager::OnCloundDataChangedAction
	Action_t81 * ___OnCloundDataChangedAction_10;
	// System.Action`1<iCloudData> iCloudManager::OnCloudDataReceivedAction
	Action_1_t182 * ___OnCloudDataReceivedAction_11;
};
struct iCloudManager_t180_StaticFields{
	// System.Action`1<ISN_Result> iCloudManager::<>f__am$cache3
	Action_1_t99 * ___U3CU3Ef__amU24cache3_12;
	// System.Action iCloudManager::<>f__am$cache4
	Action_t81 * ___U3CU3Ef__amU24cache4_13;
	// System.Action`1<iCloudData> iCloudManager::<>f__am$cache5
	Action_1_t182 * ___U3CU3Ef__amU24cache5_14;
};
