﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.LoginResult
struct LoginResult_t282;
// System.String
struct String_t;
// Facebook.Unity.AccessToken
struct AccessToken_t219;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t225;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t485;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void Facebook.Unity.LoginResult::.ctor(System.String)
extern "C" void LoginResult__ctor_m1602 (LoginResult_t282 * __this, String_t* ___response, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.LoginResult::.cctor()
extern "C" void LoginResult__cctor_m1603 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AccessToken Facebook.Unity.LoginResult::get_AccessToken()
extern "C" AccessToken_t219 * LoginResult_get_AccessToken_m1604 (LoginResult_t282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.LoginResult::set_AccessToken(Facebook.Unity.AccessToken)
extern "C" void LoginResult_set_AccessToken_m1605 (LoginResult_t282 * __this, AccessToken_t219 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AccessToken Facebook.Unity.LoginResult::ParseAccessTokenFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C" AccessToken_t219 * LoginResult_ParseAccessTokenFromResult_m1606 (Object_t * __this /* static, unused */, Object_t* ___resultDictionary, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Facebook.Unity.LoginResult::ParseExpirationDateFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C" DateTime_t220  LoginResult_ParseExpirationDateFromResult_m1607 (Object_t * __this /* static, unused */, Object_t* ___resultDictionary, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> Facebook.Unity.LoginResult::ParsePermissionFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C" Object_t* LoginResult_ParsePermissionFromResult_m1608 (Object_t * __this /* static, unused */, Object_t* ___resultDictionary, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Facebook.Unity.LoginResult::FromTimestamp(System.Int32)
extern "C" DateTime_t220  LoginResult_FromTimestamp_m1609 (Object_t * __this /* static, unused */, int32_t ___timestamp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AccessToken Facebook.Unity.LoginResult::ParseAccessTokeFromResponse()
extern "C" AccessToken_t219 * LoginResult_ParseAccessTokeFromResponse_m1610 (LoginResult_t282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.LoginResult::<ParsePermissionFromResult>m__34(System.Object)
extern "C" String_t* LoginResult_U3CParsePermissionFromResultU3Em__34_m1611 (Object_t * __this /* static, unused */, Object_t * ___permission, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.LoginResult::<ParseAccessTokeFromResponse>m__35(System.Object)
extern "C" String_t* LoginResult_U3CParseAccessTokeFromResponseU3Em__35_m1612 (Object_t * __this /* static, unused */, Object_t * ___permission, const MethodInfo* method) IL2CPP_METHOD_ATTR;
