﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// iCloudData
struct  iCloudData_t179  : public Object_t
{
	// System.String iCloudData::_key
	String_t* ____key_0;
	// System.String iCloudData::_val
	String_t* ____val_1;
	// System.Boolean iCloudData::_IsEmpty
	bool ____IsEmpty_2;
};
