﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m22498(__this, ___l, method) (( void (*) (Enumerator_t1134 *, List_1_t903 *, const MethodInfo*))Enumerator__ctor_m15550_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m22499(__this, method) (( void (*) (Enumerator_t1134 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15551_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22500(__this, method) (( Object_t * (*) (Enumerator_t1134 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15552_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m6500(__this, method) (( void (*) (Enumerator_t1134 *, const MethodInfo*))Enumerator_Dispose_m15553_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m22501(__this, method) (( void (*) (Enumerator_t1134 *, const MethodInfo*))Enumerator_VerifyState_m15554_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m6499(__this, method) (( bool (*) (Enumerator_t1134 *, const MethodInfo*))Enumerator_MoveNext_m2489_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m6498(__this, method) (( HideExcessAreaAbstractBehaviour_t413 * (*) (Enumerator_t1134 *, const MethodInfo*))Enumerator_get_Current_m2488_gshared)(__this, method)
