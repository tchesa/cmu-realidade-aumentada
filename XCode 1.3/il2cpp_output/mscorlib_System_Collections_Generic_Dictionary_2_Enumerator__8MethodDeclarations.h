﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m15988(__this, ___dictionary, method) (( void (*) (Enumerator_t2627 *, Dictionary_2_t28 *, const MethodInfo*))Enumerator__ctor_m15876_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15989(__this, method) (( Object_t * (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15877_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m15990(__this, method) (( void (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15878_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15991(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15879_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15992(__this, method) (( Object_t * (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15880_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15993(__this, method) (( Object_t * (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15881_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m15994(__this, method) (( bool (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_MoveNext_m15882_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m15995(__this, method) (( KeyValuePair_2_t2624  (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_get_Current_m15883_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15996(__this, method) (( String_t* (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_get_CurrentKey_m15884_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15997(__this, method) (( int32_t (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_get_CurrentValue_m15885_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Reset()
#define Enumerator_Reset_m15998(__this, method) (( void (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_Reset_m15886_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m15999(__this, method) (( void (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_VerifyState_m15887_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16000(__this, method) (( void (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_VerifyCurrent_m15888_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m16001(__this, method) (( void (*) (Enumerator_t2627 *, const MethodInfo*))Enumerator_Dispose_m15889_gshared)(__this, method)
