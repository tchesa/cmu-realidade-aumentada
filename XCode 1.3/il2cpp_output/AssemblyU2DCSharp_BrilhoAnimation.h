﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t45;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// BrilhoAnimation
struct  BrilhoAnimation_t344  : public MonoBehaviour_t18
{
	// UnityEngine.Material BrilhoAnimation::mat
	Material_t45 * ___mat_1;
	// System.Single BrilhoAnimation::fade
	float ___fade_2;
	// System.Single BrilhoAnimation::delay
	float ___delay_3;
	// System.Boolean BrilhoAnimation::activated
	bool ___activated_4;
	// System.Single BrilhoAnimation::maxAlpha
	float ___maxAlpha_5;
	// System.Boolean BrilhoAnimation::_activated
	bool ____activated_6;
	// System.Single BrilhoAnimation::startedTime
	float ___startedTime_7;
};
