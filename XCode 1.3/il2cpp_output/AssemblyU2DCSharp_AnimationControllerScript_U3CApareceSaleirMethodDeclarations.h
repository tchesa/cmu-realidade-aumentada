﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript/<ApareceSaleiro>c__Iterator12
struct U3CApareceSaleiroU3Ec__Iterator12_t367;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript/<ApareceSaleiro>c__Iterator12::.ctor()
extern "C" void U3CApareceSaleiroU3Ec__Iterator12__ctor_m1858 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<ApareceSaleiro>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CApareceSaleiroU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1859 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<ApareceSaleiro>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CApareceSaleiroU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m1860 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationControllerScript/<ApareceSaleiro>c__Iterator12::MoveNext()
extern "C" bool U3CApareceSaleiroU3Ec__Iterator12_MoveNext_m1861 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<ApareceSaleiro>c__Iterator12::Dispose()
extern "C" void U3CApareceSaleiroU3Ec__Iterator12_Dispose_m1862 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<ApareceSaleiro>c__Iterator12::Reset()
extern "C" void U3CApareceSaleiroU3Ec__Iterator12_Reset_m1863 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
