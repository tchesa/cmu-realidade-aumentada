﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX3_Demo
struct CFX3_Demo_t314;
// UnityEngine.GameObject
struct GameObject_t27;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX3_Demo::.ctor()
extern "C" void CFX3_Demo__ctor_m1729 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo::Awake()
extern "C" void CFX3_Demo_Awake_m1730 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo::Update()
extern "C" void CFX3_Demo_Update_m1731 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo::OnGUI()
extern "C" void CFX3_Demo_OnGUI_m1732 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX3_Demo::spawnParticle()
extern "C" GameObject_t27 * CFX3_Demo_spawnParticle_m1733 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX3_Demo::CheckForDeletedParticles()
extern "C" Object_t * CFX3_Demo_CheckForDeletedParticles_m1734 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX3_Demo::RandomSpawnsCoroutine()
extern "C" Object_t * CFX3_Demo_RandomSpawnsCoroutine_m1735 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo::prevParticle()
extern "C" void CFX3_Demo_prevParticle_m1736 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo::nextParticle()
extern "C" void CFX3_Demo_nextParticle_m1737 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_Demo::destroyParticles()
extern "C" void CFX3_Demo_destroyParticles_m1738 (CFX3_Demo_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
