﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartUIController
struct StartUIController_t390;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void StartUIController::.ctor()
extern "C" void StartUIController__ctor_m1989 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController::Start()
extern "C" void StartUIController_Start_m1990 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController::Update()
extern "C" void StartUIController_Update_m1991 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController::Seta_OnClick()
extern "C" void StartUIController_Seta_OnClick_m1992 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController::InfoGroup_OnBeginDrag()
extern "C" void StartUIController_InfoGroup_OnBeginDrag_m1993 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController::InfoGroup_OnEndGrag()
extern "C" void StartUIController_InfoGroup_OnEndGrag_m1994 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController::Iniciar_OnClick()
extern "C" void StartUIController_Iniciar_OnClick_m1995 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController::Vimeo_OnClick()
extern "C" void StartUIController_Vimeo_OnClick_m1996 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StartUIController::StartTransiction()
extern "C" Object_t * StartUIController_StartTransiction_m1997 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartUIController::test()
extern "C" void StartUIController_test_m1998 (StartUIController_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
