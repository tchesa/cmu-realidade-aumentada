﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisconnectButton
struct DisconnectButton_t193;

#include "codegen/il2cpp-codegen.h"

// System.Void DisconnectButton::.ctor()
extern "C" void DisconnectButton__ctor_m1098 (DisconnectButton_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisconnectButton::Start()
extern "C" void DisconnectButton_Start_m1099 (DisconnectButton_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisconnectButton::OnGUI()
extern "C" void DisconnectButton_OnGUI_m1100 (DisconnectButton_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
