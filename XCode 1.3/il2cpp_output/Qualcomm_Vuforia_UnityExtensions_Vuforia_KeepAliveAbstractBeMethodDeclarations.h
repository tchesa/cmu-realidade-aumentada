﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.KeepAliveAbstractBehaviour
struct KeepAliveAbstractBehaviour_t421;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t1107;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepARCameraAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepARCameraAlive_m5964 (KeepAliveAbstractBehaviour_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepARCameraAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepARCameraAlive_m5965 (KeepAliveAbstractBehaviour_t421 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTrackableBehavioursAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m5966 (KeepAliveAbstractBehaviour_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTrackableBehavioursAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepTrackableBehavioursAlive_m5967 (KeepAliveAbstractBehaviour_t421 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTextRecoBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m5968 (KeepAliveAbstractBehaviour_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTextRecoBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepTextRecoBehaviourAlive_m5969 (KeepAliveAbstractBehaviour_t421 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepUDTBuildingBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m5970 (KeepAliveAbstractBehaviour_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepUDTBuildingBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepUDTBuildingBehaviourAlive_m5971 (KeepAliveAbstractBehaviour_t421 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepSmartTerrainAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepSmartTerrainAlive_m5972 (KeepAliveAbstractBehaviour_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepSmartTerrainAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepSmartTerrainAlive_m5973 (KeepAliveAbstractBehaviour_t421 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepCloudRecoBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m5974 (KeepAliveAbstractBehaviour_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepCloudRecoBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepCloudRecoBehaviourAlive_m5975 (KeepAliveAbstractBehaviour_t421 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.KeepAliveAbstractBehaviour Vuforia.KeepAliveAbstractBehaviour::get_Instance()
extern "C" KeepAliveAbstractBehaviour_t421 * KeepAliveAbstractBehaviour_get_Instance_m5976 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::RegisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C" void KeepAliveAbstractBehaviour_RegisterEventHandler_m5977 (KeepAliveAbstractBehaviour_t421 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::UnregisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C" bool KeepAliveAbstractBehaviour_UnregisterEventHandler_m5978 (KeepAliveAbstractBehaviour_t421 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::OnLevelWasLoaded()
extern "C" void KeepAliveAbstractBehaviour_OnLevelWasLoaded_m5979 (KeepAliveAbstractBehaviour_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::.ctor()
extern "C" void KeepAliveAbstractBehaviour__ctor_m2744 (KeepAliveAbstractBehaviour_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
