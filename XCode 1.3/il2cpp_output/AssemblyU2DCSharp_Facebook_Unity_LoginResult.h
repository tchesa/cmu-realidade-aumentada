﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Facebook.Unity.AccessToken
struct AccessToken_t219;
// System.Func`2<System.Object,System.String>
struct Func_2_t283;

#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase.h"

// Facebook.Unity.LoginResult
struct  LoginResult_t282  : public ResultBase_t275
{
	// Facebook.Unity.AccessToken Facebook.Unity.LoginResult::<AccessToken>k__BackingField
	AccessToken_t219 * ___U3CAccessTokenU3Ek__BackingField_9;
};
struct LoginResult_t282_StaticFields{
	// System.String Facebook.Unity.LoginResult::UserIdKey
	String_t* ___UserIdKey_5;
	// System.String Facebook.Unity.LoginResult::ExpirationTimestampKey
	String_t* ___ExpirationTimestampKey_6;
	// System.String Facebook.Unity.LoginResult::PermissionsKey
	String_t* ___PermissionsKey_7;
	// System.String Facebook.Unity.LoginResult::AccessTokenKey
	String_t* ___AccessTokenKey_8;
	// System.Func`2<System.Object,System.String> Facebook.Unity.LoginResult::<>f__am$cache5
	Func_2_t283 * ___U3CU3Ef__amU24cache5_10;
	// System.Func`2<System.Object,System.String> Facebook.Unity.LoginResult::<>f__am$cache6
	Func_2_t283 * ___U3CU3Ef__amU24cache6_11;
};
