﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t1237;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1218;
// UnityEngine.SetupCoroutine
struct SetupCoroutine_t1239;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.WritableAttribute
struct WritableAttribute_t1240;
// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t1241;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t1262;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t1252;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t1265;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t1264;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t1266;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t1246;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t1434;
// System.Action`1<System.Boolean>
struct Action_1_t129;
// UnityEngine.Texture2D
struct Texture2D_t33;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t1432;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t1433;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t1247;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t1248;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t1249;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t1435;
// System.String[]
struct StringU5BU5D_t260;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t1250;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t1436;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t1253;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t1263;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t1267;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t1270;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t1268;
// UnityEngine.GUILayer
struct GUILayer_t1375;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t1277;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t1278;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t1279;
// UnityEngine.RangeAttribute
struct RangeAttribute_t1280;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t1281;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t1282;
// UnityEngine.SliderState
struct SliderState_t1283;
// UnityEngine.StackTraceUtility
struct StackTraceUtility_t1284;
// System.Diagnostics.StackTrace
struct StackTrace_t1437;
// UnityEngine.UnityException
struct UnityException_t853;
// System.Exception
struct Exception_t40;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// UnityEngine.TextEditor
struct TextEditor_t855;
// UnityEngine.TextGenerator
struct TextGenerator_t723;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1288;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1289;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t724;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t865;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t860;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t858;
// UnityEngine.Font
struct Font_t684;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t722;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1440;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1441;
// UnityEngine.TrackedReference
struct TrackedReference_t1290;
struct TrackedReference_t1290_marshaled;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t1292;
// UnityEngine.Object
struct Object_t53;
struct Object_t53_marshaled;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1293;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Delegate
struct Delegate_t506;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t1294;
// System.Object[]
struct ObjectU5BU5D_t34;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1296;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1301;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1297;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1299;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t673;
// UnityEngine.UserAuthorizationDialog
struct UserAuthorizationDialog_t1302;
// UnityEngine.WWW
struct WWW_t289;
// UnityEngine.WWWForm
struct WWWForm_t293;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t288;
// System.Collections.Hashtable
struct Hashtable_t19;
// System.Text.Encoding
struct Encoding_t1442;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t1303;
// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t1304;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t1306;
// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t1308;
// UnityEngineInternal.GenericStack
struct GenericStack_t1309;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t840;
// UnityEngine.Rigidbody
struct Rigidbody_t50;
// UnityEngine.Collider
struct Collider_t316;
// UnityEngine.MeshCollider
struct MeshCollider_t918;
// UnityEngine.Mesh
struct Mesh_t601;
// UnityEngine.TerrainCollider
struct TerrainCollider_t1312;
// UnityEngine.TerrainData
struct TerrainData_t1331;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t835;
// UnityEngine.Collider2D
struct Collider2D_t839;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1314;
// UnityEngine.Transform
struct Transform_t25;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1316;
// System.Single[]
struct SingleU5BU5D_t23;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1317;
// UnityEngine.AudioClip
struct AudioClip_t31;
// UnityEngine.AudioSource
struct AudioSource_t20;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1012;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t575;
// UnityEngine.AnimationEvent
struct AnimationEvent_t1318;
struct AnimationEvent_t1318_marshaled;
// UnityEngine.AnimationState
struct AnimationState_t1323;
// UnityEngine.AnimationCurve
struct AnimationCurve_t1322;
struct AnimationCurve_t1322_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1443;
// UnityEngine.GameObject
struct GameObject_t27;
// UnityEngine.Component
struct Component_t56;
// UnityEngine.Component[]
struct ComponentU5BU5D_t54;
// UnityEngine.Light
struct Light_t47;
// UnityEngine.Renderer
struct Renderer_t46;
// UnityEngine.GUIText
struct GUIText_t44;
// UnityEngine.GUITexture
struct GUITexture_t43;
// UnityEngine.AnimationClip
struct AnimationClip_t1319;
// UnityEngine.Animator
struct Animator_t360;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t864;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_U3CModuleU3E.h"
#include "UnityEngine_U3CModuleU3EMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "System_System_Collections_Generic_Stack_1_genMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_56MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_56.h"
#include "UnityEngine_UnityEngine_RequireComponent.h"
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_Binder.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "UnityEngine_UnityEngine_WritableAttribute.h"
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Double.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCente.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCenteMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_60MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_60.h"
#include "mscorlib_System_Action_1_gen_8.h"
#include "mscorlib_System_Action_1_gen_25MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_25.h"
#include "mscorlib_System_Action_1_gen_8MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
#include "mscorlib_System_Action_1_gen_26MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_26.h"
#include "mscorlib_System_Action_1_gen_27MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_27.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderbMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderb.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_28MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_28.h"
#include "mscorlib_System_Action_1_gen_28MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_28.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FilterMode.h"
#include "UnityEngine_UnityEngine_FilterModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
#include "UnityEngine_UnityEngine_TextureWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
#include "UnityEngine_UnityEngine_RenderTextureFormatMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWriteMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStateObjects.h"
#include "UnityEngine_UnityEngine_GUIStateObjectsMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_38MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_38.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_GUILayer.h"
#include "UnityEngine_UnityEngine_GUIElement.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_RenderTexture.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Rigidbody.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
#include "UnityEngine_UnityEngine_Collider2D.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RangeAttribute.h"
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderState.h"
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackFrame.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Reflection_ParameterInfo.h"
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityException.h"
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor.h"
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset.h"
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_ValueTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UnityEngine_TextGenerator.h"
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_61MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_62MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_61.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_62.h"
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_TrackedReference.h"
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_Delegate.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_0.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_1.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_2.h"
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_63MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_63.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_29MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_29.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_64MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_64.h"
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen_3.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "UnityEngine_UnityEngine_GUIStyleState.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UserAuthorization.h"
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
#include "UnityEngine_UnityEngine_WWW.h"
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm.h"
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9.h"
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
#include "mscorlib_System_IO_StringReaderMethodDeclarations.h"
#include "mscorlib_System_IO_StringReader.h"
#include "mscorlib_System_Text_Encoding.h"
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
#include "mscorlib_System_StringComparison.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_iPhone.h"
#include "UnityEngine_UnityEngine_iPhoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_iPhoneGeneration.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
#include "mscorlib_System_Enum.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
#include "mscorlib_System_Collections_Stack.h"
#include "UnityEngine_UnityEngine_Physics.h"
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider.h"
#include "UnityEngine_UnityEngine_BoxColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshCollider.h"
#include "UnityEngine_UnityEngine_MeshColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh.h"
#include "UnityEngine_UnityEngine_TerrainCollider.h"
#include "UnityEngine_UnityEngine_TerrainColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TerrainData.h"
#include "UnityEngine_UnityEngine_Physics2D.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_65MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_65.h"
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings.h"
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip.h"
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioListener.h"
#include "UnityEngine_UnityEngine_AudioListenerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource.h"
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
#include "mscorlib_System_UInt64.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture.h"
#include "UnityEngine_UnityEngine_WebCamTextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEvent.h"
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState.h"
#include "UnityEngine_UnityEngine_AnimationClip.h"
#include "UnityEngine_UnityEngine_AnimationClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve.h"
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PrimitiveType.h"
#include "UnityEngine_UnityEngine_Light.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "UnityEngine_UnityEngine_GUIText.h"
#include "UnityEngine_UnityEngine_GUITexture.h"
#include "UnityEngine_UnityEngine_AnimationInfo.h"
#include "UnityEngine_UnityEngine_AnimationInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"
#include "UnityEngine_UnityEngine_SkeletonBone.h"
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanLimit.h"
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanBone.h"
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorControllerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TreeInstance.h"
#include "UnityEngine_UnityEngine_TreeInstanceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TerrainDataMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TerrainChangedFlags.h"
#include "UnityEngine_UnityEngine_TerrainChangedFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TerrainRenderFlags.h"
#include "UnityEngine_UnityEngine_TerrainRenderFlagsMethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m437_gshared (Component_t56 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m437(__this, method) (( Object_t * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t1375_m8131(__this, method) (( GUILayer_t1375 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C" void WrapperlessIcall__ctor_m6859 (WrapperlessIcall_t1237 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const Il2CppType* MonoBehaviour_t18_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t1351_0_0_0_var;
extern TypeInfo* Stack_1_t1448_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m8117_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m8118_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m8119_MethodInfo_var;
extern "C" Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6860 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t18_0_0_0_var = il2cpp_codegen_type_from_index(878);
		DisallowMultipleComponent_t1351_0_0_0_var = il2cpp_codegen_type_from_index(879);
		Stack_1_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(880);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Stack_1__ctor_m8117_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484684);
		Stack_1_Push_m8118_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484685);
		Stack_1_Pop_m8119_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484686);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t1448 * V_0 = {0};
	Type_t * V_1 = {0};
	ObjectU5BU5D_t34* V_2 = {0};
	{
		Stack_1_t1448 * L_0 = (Stack_1_t1448 *)il2cpp_codegen_object_new (Stack_1_t1448_il2cpp_TypeInfo_var);
		Stack_1__ctor_m8117(L_0, /*hidden argument*/Stack_1__ctor_m8117_MethodInfo_var);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		Stack_1_t1448 * L_1 = V_0;
		Type_t * L_2 = ___type;
		NullCheck(L_1);
		Stack_1_Push_m8118(L_1, L_2, /*hidden argument*/Stack_1_Push_m8118_MethodInfo_var);
		Type_t * L_3 = ___type;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type = L_4;
	}

IL_001a:
	{
		Type_t * L_5 = ___type;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_6 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t18_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_6) == ((Object_t*)(Type_t *)L_7))))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (Type_t *)NULL;
		goto IL_005a;
	}

IL_0037:
	{
		Stack_1_t1448 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m8119(L_8, /*hidden argument*/Stack_1_Pop_m8119_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t1351_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t34* L_12 = (ObjectU5BU5D_t34*)VirtFuncInvoker2< ObjectU5BU5D_t34*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, 0);
		V_2 = L_12;
		ObjectU5BU5D_t34* L_13 = V_2;
		NullCheck(L_13);
		if (!(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))
		{
			goto IL_005a;
		}
	}
	{
		Type_t * L_14 = V_1;
		return L_14;
	}

IL_005a:
	{
		Stack_1_t1448 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count() */, L_15);
		if ((((int32_t)L_16) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const Il2CppType* RequireComponent_t1352_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t18_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1352_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1218_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1078_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m6823_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m8120_MethodInfo_var;
extern "C" TypeU5BU5D_t1218* AttributeHelperEngine_GetRequiredComponents_m6861 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RequireComponent_t1352_0_0_0_var = il2cpp_codegen_type_from_index(881);
		MonoBehaviour_t18_0_0_0_var = il2cpp_codegen_type_from_index(878);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		RequireComponent_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(881);
		TypeU5BU5D_t1218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(844);
		List_1_t1078_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(865);
		List_1__ctor_m6823_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484651);
		List_1_ToArray_m8120_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484687);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1078 * V_0 = {0};
	ObjectU5BU5D_t34* V_1 = {0};
	int32_t V_2 = 0;
	RequireComponent_t1352 * V_3 = {0};
	TypeU5BU5D_t1218* V_4 = {0};
	{
		V_0 = (List_1_t1078 *)NULL;
		goto IL_00d9;
	}

IL_0007:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t1352_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t34* L_2 = (ObjectU5BU5D_t34*)VirtFuncInvoker2< ObjectU5BU5D_t34*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_1 = L_2;
		V_2 = 0;
		goto IL_00c8;
	}

IL_0020:
	{
		ObjectU5BU5D_t34* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_3 = ((RequireComponent_t1352 *)CastclassSealed((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), RequireComponent_t1352_il2cpp_TypeInfo_var));
		List_1_t1078 * L_6 = V_0;
		if (L_6)
		{
			goto IL_0073;
		}
	}
	{
		ObjectU5BU5D_t34* L_7 = V_1;
		NullCheck(L_7);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0073;
		}
	}
	{
		Type_t * L_8 = ___klass;
		NullCheck(L_8);
		Type_t * L_9 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t18_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_9) == ((Object_t*)(Type_t *)L_10))))
		{
			goto IL_0073;
		}
	}
	{
		TypeU5BU5D_t1218* L_11 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 3));
		RequireComponent_t1352 * L_12 = V_3;
		NullCheck(L_12);
		Type_t * L_13 = (L_12->___m_Type0_0);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_13);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_11, 0, sizeof(Type_t *))) = (Type_t *)L_13;
		TypeU5BU5D_t1218* L_14 = L_11;
		RequireComponent_t1352 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = (L_15->___m_Type1_1);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		ArrayElementTypeCheck (L_14, L_16);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_14, 1, sizeof(Type_t *))) = (Type_t *)L_16;
		TypeU5BU5D_t1218* L_17 = L_14;
		RequireComponent_t1352 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = (L_18->___m_Type2_2);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		ArrayElementTypeCheck (L_17, L_19);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_17, 2, sizeof(Type_t *))) = (Type_t *)L_19;
		V_4 = L_17;
		TypeU5BU5D_t1218* L_20 = V_4;
		return L_20;
	}

IL_0073:
	{
		List_1_t1078 * L_21 = V_0;
		if (L_21)
		{
			goto IL_007f;
		}
	}
	{
		List_1_t1078 * L_22 = (List_1_t1078 *)il2cpp_codegen_object_new (List_1_t1078_il2cpp_TypeInfo_var);
		List_1__ctor_m6823(L_22, /*hidden argument*/List_1__ctor_m6823_MethodInfo_var);
		V_0 = L_22;
	}

IL_007f:
	{
		RequireComponent_t1352 * L_23 = V_3;
		NullCheck(L_23);
		Type_t * L_24 = (L_23->___m_Type0_0);
		if (!L_24)
		{
			goto IL_0096;
		}
	}
	{
		List_1_t1078 * L_25 = V_0;
		RequireComponent_t1352 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = (L_26->___m_Type0_0);
		NullCheck(L_25);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_25, L_27);
	}

IL_0096:
	{
		RequireComponent_t1352 * L_28 = V_3;
		NullCheck(L_28);
		Type_t * L_29 = (L_28->___m_Type1_1);
		if (!L_29)
		{
			goto IL_00ad;
		}
	}
	{
		List_1_t1078 * L_30 = V_0;
		RequireComponent_t1352 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = (L_31->___m_Type1_1);
		NullCheck(L_30);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_30, L_32);
	}

IL_00ad:
	{
		RequireComponent_t1352 * L_33 = V_3;
		NullCheck(L_33);
		Type_t * L_34 = (L_33->___m_Type2_2);
		if (!L_34)
		{
			goto IL_00c4;
		}
	}
	{
		List_1_t1078 * L_35 = V_0;
		RequireComponent_t1352 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = (L_36->___m_Type2_2);
		NullCheck(L_35);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_35, L_37);
	}

IL_00c4:
	{
		int32_t L_38 = V_2;
		V_2 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00c8:
	{
		int32_t L_39 = V_2;
		ObjectU5BU5D_t34* L_40 = V_1;
		NullCheck(L_40);
		if ((((int32_t)L_39) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_40)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		Type_t * L_41 = ___klass;
		NullCheck(L_41);
		Type_t * L_42 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_41);
		___klass = L_42;
	}

IL_00d9:
	{
		Type_t * L_43 = ___klass;
		if (!L_43)
		{
			goto IL_00ef;
		}
	}
	{
		Type_t * L_44 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t18_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_44) == ((Object_t*)(Type_t *)L_45))))
		{
			goto IL_0007;
		}
	}

IL_00ef:
	{
		List_1_t1078 * L_46 = V_0;
		if (L_46)
		{
			goto IL_00f7;
		}
	}
	{
		return (TypeU5BU5D_t1218*)NULL;
	}

IL_00f7:
	{
		List_1_t1078 * L_47 = V_0;
		NullCheck(L_47);
		TypeU5BU5D_t1218* L_48 = List_1_ToArray_m8120(L_47, /*hidden argument*/List_1_ToArray_m8120_MethodInfo_var);
		return L_48;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t1354_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t18_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool AttributeHelperEngine_CheckIsEditorScript_m6862 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t1354_0_0_0_var = il2cpp_codegen_type_from_index(882);
		MonoBehaviour_t18_0_0_0_var = il2cpp_codegen_type_from_index(878);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		goto IL_0029;
	}

IL_0005:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t1354_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t34* L_2 = (ObjectU5BU5D_t34*)VirtFuncInvoker2< ObjectU5BU5D_t34*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_0 = L_2;
		ObjectU5BU5D_t34* L_3 = V_0;
		NullCheck(L_3);
		if (!(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))))
		{
			goto IL_0021;
		}
	}
	{
		return 1;
	}

IL_0021:
	{
		Type_t * L_4 = ___klass;
		NullCheck(L_4);
		Type_t * L_5 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_4);
		___klass = L_5;
	}

IL_0029:
	{
		Type_t * L_6 = ___klass;
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_7 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t18_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0005;
		}
	}

IL_003f:
	{
		return 0;
	}
}
// System.Void UnityEngine.SetupCoroutine::.ctor()
extern "C" void SetupCoroutine__ctor_m6863 (SetupCoroutine_t1239 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern "C" Object_t * SetupCoroutine_InvokeMember_m6864 (Object_t * __this /* static, unused */, Object_t * ___behaviour, String_t* ___name, Object_t * ___variable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		V_0 = (ObjectU5BU5D_t34*)NULL;
		Object_t * L_0 = ___variable;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 1));
		ObjectU5BU5D_t34* L_1 = V_0;
		Object_t * L_2 = ___variable;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_2;
	}

IL_0013:
	{
		Object_t * L_3 = ___behaviour;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m303(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name;
		Object_t * L_6 = ___behaviour;
		ObjectU5BU5D_t34* L_7 = V_0;
		NullCheck(L_4);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker8< Object_t *, String_t*, int32_t, Binder_t1449 *, Object_t *, ObjectU5BU5D_t34*, ParameterModifierU5BU5D_t1450*, CultureInfo_t521 *, StringU5BU5D_t260* >::Invoke(71 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1449 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t1450*)(ParameterModifierU5BU5D_t1450*)NULL, (CultureInfo_t521 *)NULL, (StringU5BU5D_t260*)(StringU5BU5D_t260*)NULL);
		return L_8;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeStatic(System.Type,System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern "C" Object_t * SetupCoroutine_InvokeStatic_m6865 (Object_t * __this /* static, unused */, Type_t * ___klass, String_t* ___name, Object_t * ___variable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		V_0 = (ObjectU5BU5D_t34*)NULL;
		Object_t * L_0 = ___variable;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 1));
		ObjectU5BU5D_t34* L_1 = V_0;
		Object_t * L_2 = ___variable;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_2;
	}

IL_0013:
	{
		Type_t * L_3 = ___klass;
		String_t* L_4 = ___name;
		ObjectU5BU5D_t34* L_5 = V_0;
		NullCheck(L_3);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker8< Object_t *, String_t*, int32_t, Binder_t1449 *, Object_t *, ObjectU5BU5D_t34*, ParameterModifierU5BU5D_t1450*, CultureInfo_t521 *, StringU5BU5D_t260* >::Invoke(71 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_3, L_4, ((int32_t)312), (Binder_t1449 *)NULL, NULL, L_5, (ParameterModifierU5BU5D_t1450*)(ParameterModifierU5BU5D_t1450*)NULL, (CultureInfo_t521 *)NULL, (StringU5BU5D_t260*)(StringU5BU5D_t260*)NULL);
		return L_6;
	}
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C" void WritableAttribute__ctor_m6866 (WritableAttribute_t1240 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C" void AssemblyIsEditorAssembly__ctor_m6867 (AssemblyIsEditorAssembly_t1241 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern TypeInfo* UserProfile_t1262_il2cpp_TypeInfo_var;
extern "C" UserProfile_t1262 * GcUserProfileData_ToUserProfile_m6868 (GcUserProfileData_t1242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfile_t1262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(883);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	{
		String_t* L_0 = (__this->___userName_0);
		String_t* L_1 = (__this->___userID_1);
		int32_t L_2 = (__this->___isFriend_2);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t33 * L_3 = (__this->___image_3);
		UserProfile_t1262 * L_4 = (UserProfile_t1262 *)il2cpp_codegen_object_new (UserProfile_t1262_il2cpp_TypeInfo_var);
		UserProfile__ctor_m6942(L_4, G_B3_2, G_B3_1, G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppCodeGenString* _stringLiteral1073;
extern "C" void GcUserProfileData_AddToArray_m6869 (GcUserProfileData_t1242 * __this, UserProfileU5BU5D_t1252** ___array, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1073 = il2cpp_codegen_string_literal_from_index(1073);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t1252** L_0 = ___array;
		NullCheck((*((UserProfileU5BU5D_t1252**)L_0)));
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t1252**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t1252** L_3 = ___array;
		int32_t L_4 = ___number;
		UserProfile_t1262 * L_5 = GcUserProfileData_ToUserProfile_m6868(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t1252**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t1252**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t1252**)L_3)), L_5);
		*((UserProfile_t1262 **)(UserProfile_t1262 **)SZArrayLdElema((*((UserProfileU5BU5D_t1252**)L_3)), L_4, sizeof(UserProfile_t1262 *))) = (UserProfile_t1262 *)L_5;
		goto IL_002a;
	}

IL_0020:
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral1073, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern TypeInfo* AchievementDescription_t1265_il2cpp_TypeInfo_var;
extern "C" AchievementDescription_t1265 * GcAchievementDescriptionData_ToAchievementDescription_m6870 (GcAchievementDescriptionData_t1243 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescription_t1265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	Texture2D_t33 * G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	String_t* G_B2_4 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	Texture2D_t33 * G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B1_4 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	Texture2D_t33 * G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	String_t* G_B3_5 = {0};
	{
		String_t* L_0 = (__this->___m_Identifier_0);
		String_t* L_1 = (__this->___m_Title_1);
		Texture2D_t33 * L_2 = (__this->___m_Image_2);
		String_t* L_3 = (__this->___m_AchievedDescription_3);
		String_t* L_4 = (__this->___m_UnachievedDescription_4);
		int32_t L_5 = (__this->___m_Hidden_5);
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = (__this->___m_Points_6);
		AchievementDescription_t1265 * L_7 = (AchievementDescription_t1265 *)il2cpp_codegen_object_new (AchievementDescription_t1265_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m6962(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern TypeInfo* Achievement_t1264_il2cpp_TypeInfo_var;
extern "C" Achievement_t1264 * GcAchievementData_ToAchievement_m6871 (GcAchievementData_t1244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t1264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(885);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t220  V_0 = {0};
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = {0};
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = {0};
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = {0};
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = {0};
	{
		String_t* L_0 = (__this->___m_Identifier_0);
		double L_1 = (__this->___m_PercentCompleted_1);
		int32_t L_2 = (__this->___m_Completed_2);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = (__this->___m_Hidden_3);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m2510((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_LastReportedDate_4);
		DateTime_t220  L_5 = DateTime_AddSeconds_m2505((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t1264 * L_6 = (Achievement_t1264 *)il2cpp_codegen_object_new (Achievement_t1264_il2cpp_TypeInfo_var);
		Achievement__ctor_m6951(L_6, G_B6_3, G_B6_2, G_B6_1, G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1244_marshal(const GcAchievementData_t1244& unmarshaled, GcAchievementData_t1244_marshaled& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Identifier_0);
	marshaled.___m_PercentCompleted_1 = unmarshaled.___m_PercentCompleted_1;
	marshaled.___m_Completed_2 = unmarshaled.___m_Completed_2;
	marshaled.___m_Hidden_3 = unmarshaled.___m_Hidden_3;
	marshaled.___m_LastReportedDate_4 = unmarshaled.___m_LastReportedDate_4;
}
extern "C" void GcAchievementData_t1244_marshal_back(const GcAchievementData_t1244_marshaled& marshaled, GcAchievementData_t1244& unmarshaled)
{
	unmarshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0);
	unmarshaled.___m_PercentCompleted_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.___m_Completed_2 = marshaled.___m_Completed_2;
	unmarshaled.___m_Hidden_3 = marshaled.___m_Hidden_3;
	unmarshaled.___m_LastReportedDate_4 = marshaled.___m_LastReportedDate_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1244_marshal_cleanup(GcAchievementData_t1244_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern TypeInfo* Score_t1266_il2cpp_TypeInfo_var;
extern "C" Score_t1266 * GcScoreData_ToScore_m6872 (GcScoreData_t1245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Score_t1266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(886);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t220  V_0 = {0};
	{
		String_t* L_0 = (__this->___m_Category_0);
		int32_t L_1 = (__this->___m_ValueHigh_2);
		int32_t L_2 = (__this->___m_ValueLow_1);
		String_t* L_3 = (__this->___m_PlayerID_5);
		DateTime__ctor_m2510((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_Date_3);
		DateTime_t220  L_5 = DateTime_AddSeconds_m2505((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		String_t* L_6 = (__this->___m_FormattedValue_4);
		int32_t L_7 = (__this->___m_Rank_6);
		Score_t1266 * L_8 = (Score_t1266 *)il2cpp_codegen_object_new (Score_t1266_il2cpp_TypeInfo_var);
		Score__ctor_m6973(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)((int64_t)L_2))))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t1245_marshal(const GcScoreData_t1245& unmarshaled, GcScoreData_t1245_marshaled& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Category_0);
	marshaled.___m_ValueLow_1 = unmarshaled.___m_ValueLow_1;
	marshaled.___m_ValueHigh_2 = unmarshaled.___m_ValueHigh_2;
	marshaled.___m_Date_3 = unmarshaled.___m_Date_3;
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.___m_FormattedValue_4);
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.___m_PlayerID_5);
	marshaled.___m_Rank_6 = unmarshaled.___m_Rank_6;
}
extern "C" void GcScoreData_t1245_marshal_back(const GcScoreData_t1245_marshaled& marshaled, GcScoreData_t1245& unmarshaled)
{
	unmarshaled.___m_Category_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0);
	unmarshaled.___m_ValueLow_1 = marshaled.___m_ValueLow_1;
	unmarshaled.___m_ValueHigh_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.___m_Date_3 = marshaled.___m_Date_3;
	unmarshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4);
	unmarshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5);
	unmarshaled.___m_Rank_6 = marshaled.___m_Rank_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t1245_marshal_cleanup(GcScoreData_t1245_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C" void GameCenterPlatform__ctor_m6873 (GameCenterPlatform_t1246 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern TypeInfo* AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern TypeInfo* UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1254_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8121_MethodInfo_var;
extern "C" void GameCenterPlatform__cctor_m6874 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(888);
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(890);
		List_1_t1254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		List_1__ctor_m8121_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484688);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9 = ((AchievementDescriptionU5BU5D_t1251*)SZArrayNew(AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var, 0));
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_friends_10 = ((UserProfileU5BU5D_t1252*)SZArrayNew(UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var, 0));
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_users_11 = ((UserProfileU5BU5D_t1252*)SZArrayNew(UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var, 0));
		List_1_t1254 * L_0 = (List_1_t1254 *)il2cpp_codegen_object_new (List_1_t1254_il2cpp_TypeInfo_var);
		List_1__ctor_m8121(L_0, /*hidden argument*/List_1__ctor_m8121_MethodInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m6875 (GameCenterPlatform_t1246 * __this, Object_t * ___user, Action_1_t129 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t129 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1 = L_0;
		GameCenterPlatform_Internal_LoadFriends_m6919(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m6876 (GameCenterPlatform_t1246 * __this, Object_t * ___user, Action_1_t129 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t129 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0 = L_0;
		GameCenterPlatform_Internal_Authenticate_m6913(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern TypeInfo* AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m6877 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(888);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t1251* L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t1251* L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_1);
		int32_t L_2 = ___size;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9 = ((AchievementDescriptionU5BU5D_t1251*)SZArrayNew(AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var, L_3));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetAchievementDescription_m6878 (Object_t * __this /* static, unused */, GcAchievementDescriptionData_t1243  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t1251* L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		int32_t L_1 = ___number;
		AchievementDescription_t1265 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m6870((&___data), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((AchievementDescription_t1265 **)(AchievementDescription_t1265 **)SZArrayLdElema(L_0, L_1, sizeof(AchievementDescription_t1265 *))) = (AchievementDescription_t1265 *)L_2;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1074;
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m6879 (Object_t * __this /* static, unused */, Texture2D_t33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		_stringLiteral1074 = il2cpp_codegen_string_literal_from_index(1074);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t1251* L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_0);
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral1074, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t1251* L_3 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		int32_t L_4 = ___number;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Texture2D_t33 * L_6 = ___texture;
		NullCheck((*(AchievementDescription_t1265 **)(AchievementDescription_t1265 **)SZArrayLdElema(L_3, L_5, sizeof(AchievementDescription_t1265 *))));
		AchievementDescription_SetImage_m6964((*(AchievementDescription_t1265 **)(AchievementDescription_t1265 **)SZArrayLdElema(L_3, L_5, sizeof(AchievementDescription_t1265 *))), L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8122_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1075;
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m6880 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m8122_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484689);
		_stringLiteral1075 = il2cpp_codegen_string_literal_from_index(1075);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t1247 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2;
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t1251* L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t1251* L_2 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral1075, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t1247 * L_3 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2;
		AchievementDescriptionU5BU5D_t1251* L_4 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_3);
		Action_1_Invoke_m8122(L_3, (IAchievementDescriptionU5BU5D_t1451*)(IAchievementDescriptionU5BU5D_t1451*)L_4, /*hidden argument*/Action_1_Invoke_m8122_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m6881 (Object_t * __this /* static, unused */, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t129 * G_B3_0 = {0};
	Action_1_t129 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	Action_1_t129 * G_B4_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m6891(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t129 * L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0;
		int32_t L_2 = ___result;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m2235(G_B4_1, G_B4_0, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearFriends_m6882 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size;
		GameCenterPlatform_SafeClearArray_m6909(NULL /*static, unused*/, (&((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetFriends_m6883 (Object_t * __this /* static, unused */, GcUserProfileData_t1242  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number;
		GcUserProfileData_AddToArray_m6869((&___data), (&((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetFriendImage_m6884 (Object_t * __this /* static, unused */, Texture2D_t33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Texture2D_t33 * L_0 = ___texture;
		int32_t L_1 = ___number;
		GameCenterPlatform_SafeSetUserImage_m6908(NULL /*static, unused*/, (&((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m6885 (Object_t * __this /* static, unused */, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t129 * G_B5_0 = {0};
	Action_1_t129 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Action_1_t129 * G_B6_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t1252* L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_friends_10;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		LocalUser_t1253 * L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		UserProfileU5BU5D_t1252* L_2 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_friends_10;
		NullCheck(L_1);
		LocalUser_SetFriends_m6937(L_1, (IUserProfileU5BU5D_t1263*)(IUserProfileU5BU5D_t1263*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_3 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1;
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_4 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1;
		int32_t L_5 = ___result;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m2235(G_B6_1, G_B6_0, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern TypeInfo* AchievementU5BU5D_t1452_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8123_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1076;
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m6886 (Object_t * __this /* static, unused */, GcAchievementDataU5BU5D_t1432* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		AchievementU5BU5D_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		Action_1_Invoke_m8123_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484690);
		_stringLiteral1076 = il2cpp_codegen_string_literal_from_index(1076);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t1452* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t1248 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3;
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t1432* L_1 = ___result;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral1076, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t1432* L_2 = ___result;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t1452*)SZArrayNew(AchievementU5BU5D_t1452_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t1452* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t1432* L_5 = ___result;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t1264 * L_7 = GcAchievementData_ToAchievement_m6871(((GcAchievementData_t1244 *)(GcAchievementData_t1244 *)SZArrayLdElema(L_5, L_6, sizeof(GcAchievementData_t1244 ))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		*((Achievement_t1264 **)(Achievement_t1264 **)SZArrayLdElema(L_3, L_4, sizeof(Achievement_t1264 *))) = (Achievement_t1264 *)L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t1432* L_10 = ___result;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t1248 * L_11 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3;
		AchievementU5BU5D_t1452* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m8123(L_11, (IAchievementU5BU5D_t1453*)(IAchievementU5BU5D_t1453*)L_12, /*hidden argument*/Action_1_Invoke_m8123_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m6887 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m2235(L_1, L_2, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m6888 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m2235(L_1, L_2, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern TypeInfo* ScoreU5BU5D_t1454_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8124_MethodInfo_var;
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m6889 (Object_t * __this /* static, unused */, GcScoreDataU5BU5D_t1433* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		ScoreU5BU5D_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(899);
		Action_1_Invoke_m8124_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484691);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t1454* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t1249 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6;
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t1433* L_1 = ___result;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t1454*)SZArrayNew(ScoreU5BU5D_t1454_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t1454* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1433* L_4 = ___result;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t1266 * L_6 = GcScoreData_ToScore_m6872(((GcScoreData_t1245 *)(GcScoreData_t1245 *)SZArrayLdElema(L_4, L_5, sizeof(GcScoreData_t1245 ))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Score_t1266 **)(Score_t1266 **)SZArrayLdElema(L_2, L_3, sizeof(Score_t1266 *))) = (Score_t1266 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1433* L_9 = ___result;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t1249 * L_10 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6;
		ScoreU5BU5D_t1454* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m8124(L_10, (IScoreU5BU5D_t1268*)(IScoreU5BU5D_t1268*)L_11, /*hidden argument*/Action_1_Invoke_m8124_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern TypeInfo* LocalUser_t1253_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral134;
extern "C" Object_t * GameCenterPlatform_get_localUser_m6890 (GameCenterPlatform_t1246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		LocalUser_t1253_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(900);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral134 = il2cpp_codegen_string_literal_from_index(134);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		LocalUser_t1253 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t1253 * L_1 = (LocalUser_t1253 *)il2cpp_codegen_object_new (LocalUser_t1253_il2cpp_TypeInfo_var);
		LocalUser__ctor_m6936(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m6914(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		LocalUser_t1253 * L_3 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m385(NULL /*static, unused*/, L_4, _stringLiteral134, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m6891(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		LocalUser_t1253 * L_6 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_PopulateLocalUser_m6891 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		LocalUser_t1253 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m6914(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m6938(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t1253 * L_2 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m6915(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m6944(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t1253 * L_4 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m6916(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m6945(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t1253 * L_6 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		bool L_7 = GameCenterPlatform_Internal_Underage_m6917(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m6939(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t1253 * L_8 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		Texture2D_t33 * L_9 = GameCenterPlatform_Internal_UserImage_m6918(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m6946(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern TypeInfo* AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8122_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m6892 (GameCenterPlatform_t1246 * __this, Action_1_t1247 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(888);
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m8122_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484689);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6900(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1247 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m8122(L_1, (IAchievementDescriptionU5BU5D_t1451*)(IAchievementDescriptionU5BU5D_t1451*)((AchievementDescriptionU5BU5D_t1251*)SZArrayNew(AchievementDescriptionU5BU5D_t1251_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m8122_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1247 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2 = L_2;
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m6920(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_ReportProgress_m6893 (GameCenterPlatform_t1246 * __this, String_t* ___id, double ___progress, Action_1_t129 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6900(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t129 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m2235(L_1, 0, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t129 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4 = L_2;
		String_t* L_3 = ___id;
		double L_4 = ___progress;
		GameCenterPlatform_Internal_ReportProgress_m6922(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern TypeInfo* AchievementU5BU5D_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8123_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadAchievements_m6894 (GameCenterPlatform_t1246 * __this, Action_1_t1248 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementU5BU5D_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(896);
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m8123_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484690);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6900(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1248 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m8123(L_1, (IAchievementU5BU5D_t1453*)(IAchievementU5BU5D_t1453*)((AchievementU5BU5D_t1452*)SZArrayNew(AchievementU5BU5D_t1452_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m8123_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1248 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3 = L_2;
		GameCenterPlatform_Internal_LoadAchievements_m6921(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_ReportScore_m6895 (GameCenterPlatform_t1246 * __this, int64_t ___score, String_t* ___board, Action_1_t129 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6900(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t129 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m2235(L_1, 0, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t129 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5 = L_2;
		int64_t L_3 = ___score;
		String_t* L_4 = ___board;
		GameCenterPlatform_Internal_ReportScore_m6923(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern TypeInfo* ScoreU5BU5D_t1454_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8124_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadScores_m6896 (GameCenterPlatform_t1246 * __this, String_t* ___category, Action_1_t1249 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScoreU5BU5D_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(899);
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m8124_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484691);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6900(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1249 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m8124(L_1, (IScoreU5BU5D_t1268*)(IScoreU5BU5D_t1268*)((ScoreU5BU5D_t1454*)SZArrayNew(ScoreU5BU5D_t1454_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m8124_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1249 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6 = L_2;
		String_t* L_3 = ___category;
		GameCenterPlatform_Internal_LoadScores_m6924(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern TypeInfo* Leaderboard_t1267_il2cpp_TypeInfo_var;
extern TypeInfo* GcLeaderboard_t1358_il2cpp_TypeInfo_var;
extern TypeInfo* ILeaderboard_t1435_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadScores_m6897 (GameCenterPlatform_t1246 * __this, Object_t * ___board, Action_1_t129 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Leaderboard_t1267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(901);
		GcLeaderboard_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(887);
		ILeaderboard_t1435_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(902);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1267 * V_0 = {0};
	GcLeaderboard_t1358 * V_1 = {0};
	Range_t1269  V_2 = {0};
	Range_t1269  V_3 = {0};
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6900(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t129 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m2235(L_1, 0, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t129 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7 = L_2;
		Object_t * L_3 = ___board;
		V_0 = ((Leaderboard_t1267 *)CastclassClass(L_3, Leaderboard_t1267_il2cpp_TypeInfo_var));
		Leaderboard_t1267 * L_4 = V_0;
		GcLeaderboard_t1358 * L_5 = (GcLeaderboard_t1358 *)il2cpp_codegen_object_new (GcLeaderboard_t1358_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m7395(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t1254 * L_6 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14;
		GcLeaderboard_t1358 * L_7 = V_1;
		NullCheck(L_6);
		VirtActionInvoker1< GcLeaderboard_t1358 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Add(!0) */, L_6, L_7);
		Leaderboard_t1267 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t260* L_9 = Leaderboard_GetUserFilter_m6985(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GcLeaderboard_t1358 * L_10 = V_1;
		Object_t * L_11 = ___board;
		NullCheck(L_11);
		String_t* L_12 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t1435_il2cpp_TypeInfo_var, L_11);
		Object_t * L_13 = ___board;
		NullCheck(L_13);
		int32_t L_14 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t1435_il2cpp_TypeInfo_var, L_13);
		Leaderboard_t1267 * L_15 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t260* L_16 = Leaderboard_GetUserFilter_m6985(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		GcLeaderboard_Internal_LoadScoresWithUsers_m7403(L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_005d:
	{
		GcLeaderboard_t1358 * L_17 = V_1;
		Object_t * L_18 = ___board;
		NullCheck(L_18);
		String_t* L_19 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t1435_il2cpp_TypeInfo_var, L_18);
		Object_t * L_20 = ___board;
		NullCheck(L_20);
		Range_t1269  L_21 = (Range_t1269 )InterfaceFuncInvoker0< Range_t1269  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t1435_il2cpp_TypeInfo_var, L_20);
		V_2 = L_21;
		int32_t L_22 = ((&V_2)->___from_0);
		Object_t * L_23 = ___board;
		NullCheck(L_23);
		Range_t1269  L_24 = (Range_t1269 )InterfaceFuncInvoker0< Range_t1269  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t1435_il2cpp_TypeInfo_var, L_23);
		V_3 = L_24;
		int32_t L_25 = ((&V_3)->___count_1);
		Object_t * L_26 = ___board;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t1435_il2cpp_TypeInfo_var, L_26);
		Object_t * L_28 = ___board;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t1435_il2cpp_TypeInfo_var, L_28);
		NullCheck(L_17);
		GcLeaderboard_Internal_LoadScores_m7402(L_17, L_19, L_22, L_25, L_27, L_29, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m6898 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m2235(L_1, L_2, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern TypeInfo* Leaderboard_t1267_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1455_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8125_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8126_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8127_MethodInfo_var;
extern "C" bool GameCenterPlatform_GetLoading_m6899 (GameCenterPlatform_t1246 * __this, Object_t * ___board, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Leaderboard_t1267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(901);
		Enumerator_t1455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(903);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		List_1_GetEnumerator_m8125_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484692);
		Enumerator_get_Current_m8126_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484693);
		Enumerator_MoveNext_m8127_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484694);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t1358 * V_0 = {0};
	Enumerator_t1455  V_1 = {0};
	bool V_2 = false;
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6900(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		List_1_t1254 * L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14;
		NullCheck(L_1);
		Enumerator_t1455  L_2 = List_1_GetEnumerator_m8125(L_1, /*hidden argument*/List_1_GetEnumerator_m8125_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t1358 * L_3 = Enumerator_get_Current_m8126((&V_1), /*hidden argument*/Enumerator_get_Current_m8126_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t1358 * L_4 = V_0;
			Object_t * L_5 = ___board;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m7397(L_4, ((Leaderboard_t1267 *)CastclassClass(L_5, Leaderboard_t1267_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t1358 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m7404(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m8127((&V_1), /*hidden argument*/Enumerator_MoveNext_m8127_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t1455  L_10 = V_1;
		Enumerator_t1455  L_11 = L_10;
		Object_t * L_12 = Box(Enumerator_t1455_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_005f:
	{
		return 0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern TypeInfo* ILocalUser_t1434_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1077;
extern "C" bool GameCenterPlatform_VerifyAuthentication_m6900 (GameCenterPlatform_t1246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILocalUser_t1434_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(904);
		_stringLiteral1077 = il2cpp_codegen_string_literal_from_index(1077);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = GameCenterPlatform_get_localUser_m6890(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t1434_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral1077, /*hidden argument*/NULL);
		return 0;
	}

IL_001c:
	{
		return 1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowAchievementsUI_m6901 (GameCenterPlatform_t1246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m6925(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m6902 (GameCenterPlatform_t1246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m6926(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearUsers_m6903 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size;
		GameCenterPlatform_SafeClearArray_m6909(NULL /*static, unused*/, (&((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetUser_m6904 (Object_t * __this /* static, unused */, GcUserProfileData_t1242  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number;
		GcUserProfileData_AddToArray_m6869((&___data), (&((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetUserImage_m6905 (Object_t * __this /* static, unused */, Texture2D_t33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Texture2D_t33 * L_0 = ___texture;
		int32_t L_1 = ___number;
		GameCenterPlatform_SafeSetUserImage_m6908(NULL /*static, unused*/, (&((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8128_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m6906 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m8128_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484695);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t1250 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t1250 * L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8;
		UserProfileU5BU5D_t1252* L_2 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_users_11;
		NullCheck(L_1);
		Action_1_Invoke_m8128(L_1, (IUserProfileU5BU5D_t1263*)(IUserProfileU5BU5D_t1263*)L_2, /*hidden argument*/Action_1_Invoke_m8128_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern TypeInfo* UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m8128_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadUsers_m6907 (GameCenterPlatform_t1246 * __this, StringU5BU5D_t260* ___userIds, Action_1_t1250 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(890);
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m8128_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484695);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6900(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1250 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m8128(L_1, (IUserProfileU5BU5D_t1263*)(IUserProfileU5BU5D_t1263*)((UserProfileU5BU5D_t1252*)SZArrayNew(UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m8128_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1250 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8 = L_2;
		StringU5BU5D_t260* L_3 = ___userIds;
		GameCenterPlatform_Internal_LoadUsers_m6927(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern TypeInfo* Texture2D_t33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1078;
extern Il2CppCodeGenString* _stringLiteral1079;
extern "C" void GameCenterPlatform_SafeSetUserImage_m6908 (Object_t * __this /* static, unused */, UserProfileU5BU5D_t1252** ___array, Texture2D_t33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		_stringLiteral1078 = il2cpp_codegen_string_literal_from_index(1078);
		_stringLiteral1079 = il2cpp_codegen_string_literal_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t1252** L_0 = ___array;
		NullCheck((*((UserProfileU5BU5D_t1252**)L_0)));
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t1252**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral1078, /*hidden argument*/NULL);
		Texture2D_t33 * L_3 = (Texture2D_t33 *)il2cpp_codegen_object_new (Texture2D_t33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2222(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t1252** L_4 = ___array;
		NullCheck((*((UserProfileU5BU5D_t1252**)L_4)));
		int32_t L_5 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t1252**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t1252** L_7 = ___array;
		int32_t L_8 = ___number;
		NullCheck((*((UserProfileU5BU5D_t1252**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t1252**)L_7)), L_8);
		int32_t L_9 = L_8;
		Texture2D_t33 * L_10 = ___texture;
		NullCheck((*(UserProfile_t1262 **)(UserProfile_t1262 **)SZArrayLdElema((*((UserProfileU5BU5D_t1252**)L_7)), L_9, sizeof(UserProfile_t1262 *))));
		UserProfile_SetImage_m6946((*(UserProfile_t1262 **)(UserProfile_t1262 **)SZArrayLdElema((*((UserProfileU5BU5D_t1252**)L_7)), L_9, sizeof(UserProfile_t1262 *))), L_10, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral1079, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern TypeInfo* UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SafeClearArray_m6909 (Object_t * __this /* static, unused */, UserProfileU5BU5D_t1252** ___array, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(890);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t1252** L_0 = ___array;
		if (!(*((UserProfileU5BU5D_t1252**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t1252** L_1 = ___array;
		NullCheck((*((UserProfileU5BU5D_t1252**)L_1)));
		int32_t L_2 = ___size;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t1252**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t1252** L_3 = ___array;
		int32_t L_4 = ___size;
		*((Object_t **)(L_3)) = (Object_t *)((UserProfileU5BU5D_t1252*)SZArrayNew(UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var, L_4));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern TypeInfo* Leaderboard_t1267_il2cpp_TypeInfo_var;
extern "C" Object_t * GameCenterPlatform_CreateLeaderboard_m6910 (GameCenterPlatform_t1246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Leaderboard_t1267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(901);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1267 * V_0 = {0};
	{
		Leaderboard_t1267 * L_0 = (Leaderboard_t1267 *)il2cpp_codegen_object_new (Leaderboard_t1267_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m6979(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t1267 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern TypeInfo* Achievement_t1264_il2cpp_TypeInfo_var;
extern "C" Object_t * GameCenterPlatform_CreateAchievement_m6911 (GameCenterPlatform_t1246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t1264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(885);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t1264 * V_0 = {0};
	{
		Achievement_t1264 * L_0 = (Achievement_t1264 *)il2cpp_codegen_object_new (Achievement_t1264_il2cpp_TypeInfo_var);
		Achievement__ctor_m6953(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t1264 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2235_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m6912 (Object_t * __this /* static, unused */, bool ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		Action_1_Invoke_m2235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_0 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		Action_1_t129 * L_1 = ((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12;
		bool L_2 = ___result;
		NullCheck(L_1);
		Action_1_Invoke_m2235(L_1, L_2, /*hidden argument*/Action_1_Invoke_m2235_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C" void GameCenterPlatform_Internal_Authenticate_m6913 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m6913_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m6913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m6913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C" bool GameCenterPlatform_Internal_Authenticated_m6914 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m6914_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m6914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m6914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C" String_t* GameCenterPlatform_Internal_UserName_m6915 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m6915_ftn) ();
	static GameCenterPlatform_Internal_UserName_m6915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m6915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C" String_t* GameCenterPlatform_Internal_UserID_m6916 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m6916_ftn) ();
	static GameCenterPlatform_Internal_UserID_m6916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m6916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C" bool GameCenterPlatform_Internal_Underage_m6917 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m6917_ftn) ();
	static GameCenterPlatform_Internal_Underage_m6917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m6917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C" Texture2D_t33 * GameCenterPlatform_Internal_UserImage_m6918 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t33 * (*GameCenterPlatform_Internal_UserImage_m6918_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m6918_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m6918_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C" void GameCenterPlatform_Internal_LoadFriends_m6919 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m6919_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m6919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m6919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m6920 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m6920_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m6920_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m6920_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m6921 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m6921_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m6921_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m6921_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C" void GameCenterPlatform_Internal_ReportProgress_m6922 (Object_t * __this /* static, unused */, String_t* ___id, double ___progress, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m6922_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m6922_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m6922_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id, ___progress);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C" void GameCenterPlatform_Internal_ReportScore_m6923 (Object_t * __this /* static, unused */, int64_t ___score, String_t* ___category, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m6923_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m6923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m6923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score, ___category);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C" void GameCenterPlatform_Internal_LoadScores_m6924 (Object_t * __this /* static, unused */, String_t* ___category, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m6924_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m6924_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m6924_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m6925 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m6925_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m6925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m6925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m6926 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m6926_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m6926_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m6926_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C" void GameCenterPlatform_Internal_LoadUsers_m6927 (Object_t * __this /* static, unused */, StringU5BU5D_t260* ___userIds, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m6927_ftn) (StringU5BU5D_t260*);
	static GameCenterPlatform_Internal_LoadUsers_m6927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m6927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m6928 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m6928_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m6928_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m6928_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6929 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6929_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6929_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6929_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ResetAllAchievements_m6930 (Object_t * __this /* static, unused */, Action_1_t129 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t129 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t1246_StaticFields*)GameCenterPlatform_t1246_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12 = L_0;
		GameCenterPlatform_Internal_ResetAllAchievements_m6928(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m6931 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6929(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern TypeInfo* GameCenterPlatform_t1246_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m6932 (Object_t * __this /* static, unused */, String_t* ___leaderboardID, int32_t ___timeScope, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t1246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(889);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID;
		int32_t L_1 = ___timeScope;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t1246_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6933(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6933 (Object_t * __this /* static, unused */, String_t* ___leaderboardID, int32_t ___timeScope, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6933_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6933_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6933_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID, ___timeScope);
}
// System.Void UnityEngine.GUIStateObjects::.cctor()
extern TypeInfo* Dictionary_2_t1261_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStateObjects_t1260_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m8129_MethodInfo_var;
extern "C" void GUIStateObjects__cctor_m6934 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(907);
		GUIStateObjects_t1260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(908);
		Dictionary_2__ctor_m8129_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484696);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1261 * L_0 = (Dictionary_2_t1261 *)il2cpp_codegen_object_new (Dictionary_2_t1261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m8129(L_0, /*hidden argument*/Dictionary_2__ctor_m8129_MethodInfo_var);
		((GUIStateObjects_t1260_StaticFields*)GUIStateObjects_t1260_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.GUIStateObjects::GetStateObject(System.Type,System.Int32)
extern TypeInfo* GUIStateObjects_t1260_il2cpp_TypeInfo_var;
extern "C" Object_t * GUIStateObjects_GetStateObject_m6935 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___controlID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStateObjects_t1260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(908);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t1260_il2cpp_TypeInfo_var);
		Dictionary_2_t1261 * L_0 = ((GUIStateObjects_t1260_StaticFields*)GUIStateObjects_t1260_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0;
		int32_t L_1 = ___controlID;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, int32_t, Object_t ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_3 = V_0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m303(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = ___t;
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_0031;
		}
	}

IL_001e:
	{
		Type_t * L_6 = ___t;
		Object_t * L_7 = Activator_CreateInstance_m8130(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t1260_il2cpp_TypeInfo_var);
		Dictionary_2_t1261 * L_8 = ((GUIStateObjects_t1260_StaticFields*)GUIStateObjects_t1260_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0;
		int32_t L_9 = ___controlID;
		Object_t * L_10 = V_0;
		NullCheck(L_8);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(!0,!1) */, L_8, L_9, L_10);
	}

IL_0031:
	{
		Object_t * L_11 = V_0;
		return L_11;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern TypeInfo* UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var;
extern "C" void LocalUser__ctor_m6936 (LocalUser_t1253 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(890);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfile__ctor_m6941(__this, /*hidden argument*/NULL);
		__this->___m_Friends_5 = (IUserProfileU5BU5D_t1263*)((UserProfileU5BU5D_t1252*)SZArrayNew(UserProfileU5BU5D_t1252_il2cpp_TypeInfo_var, 0));
		__this->___m_Authenticated_6 = 0;
		__this->___m_Underage_7 = 0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern "C" void LocalUser_SetFriends_m6937 (LocalUser_t1253 * __this, IUserProfileU5BU5D_t1263* ___friends, const MethodInfo* method)
{
	{
		IUserProfileU5BU5D_t1263* L_0 = ___friends;
		__this->___m_Friends_5 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern "C" void LocalUser_SetAuthenticated_m6938 (LocalUser_t1253 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_Authenticated_6 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern "C" void LocalUser_SetUnderage_m6939 (LocalUser_t1253 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_Underage_7 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern "C" bool LocalUser_get_authenticated_m6940 (LocalUser_t1253 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Authenticated_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern TypeInfo* Texture2D_t33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1080;
extern Il2CppCodeGenString* _stringLiteral134;
extern "C" void UserProfile__ctor_m6941 (UserProfile_t1262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		_stringLiteral1080 = il2cpp_codegen_string_literal_from_index(1080);
		_stringLiteral134 = il2cpp_codegen_string_literal_from_index(134);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		__this->___m_UserName_0 = _stringLiteral1080;
		__this->___m_ID_1 = _stringLiteral134;
		__this->___m_IsFriend_2 = 0;
		__this->___m_State_3 = 3;
		Texture2D_t33 * L_0 = (Texture2D_t33 *)il2cpp_codegen_object_new (Texture2D_t33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2222(L_0, ((int32_t)32), ((int32_t)32), /*hidden argument*/NULL);
		__this->___m_Image_4 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern "C" void UserProfile__ctor_m6942 (UserProfile_t1262 * __this, String_t* ___name, String_t* ___id, bool ___friend, int32_t ___state, Texture2D_t33 * ___image, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___m_UserName_0 = L_0;
		String_t* L_1 = ___id;
		__this->___m_ID_1 = L_1;
		bool L_2 = ___friend;
		__this->___m_IsFriend_2 = L_2;
		int32_t L_3 = ___state;
		__this->___m_State_3 = L_3;
		Texture2D_t33 * L_4 = ___image;
		__this->___m_Image_4 = L_4;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t41_il2cpp_TypeInfo_var;
extern TypeInfo* UserState_t1274_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1081;
extern "C" String_t* UserProfile_ToString_m6943 (UserProfile_t1262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Boolean_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		UserState_t1274_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(909);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1081 = il2cpp_codegen_string_literal_from_index(1081);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 7));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t34* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_3 = L_2;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName() */, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t34* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_6 = L_5;
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend() */, __this);
		bool L_8 = L_7;
		Object_t * L_9 = Box(Boolean_t41_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t34* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_11 = L_10;
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state() */, __this);
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(UserState_t1274_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 6, sizeof(Object_t *))) = (Object_t *)L_14;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2254(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern "C" void UserProfile_SetUserName_m6944 (UserProfile_t1262 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		__this->___m_UserName_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern "C" void UserProfile_SetUserID_m6945 (UserProfile_t1262 * __this, String_t* ___id, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id;
		__this->___m_ID_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern "C" void UserProfile_SetImage_m6946 (UserProfile_t1262 * __this, Texture2D_t33 * ___image, const MethodInfo* method)
{
	{
		Texture2D_t33 * L_0 = ___image;
		__this->___m_Image_4 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern "C" String_t* UserProfile_get_userName_m6947 (UserProfile_t1262 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_UserName_0);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern "C" String_t* UserProfile_get_id_m6948 (UserProfile_t1262 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_ID_1);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern "C" bool UserProfile_get_isFriend_m6949 (UserProfile_t1262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_IsFriend_2);
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern "C" int32_t UserProfile_get_state_m6950 (UserProfile_t1262 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_State_3);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern "C" void Achievement__ctor_m6951 (Achievement_t1264 * __this, String_t* ___id, double ___percentCompleted, bool ___completed, bool ___hidden, DateTime_t220  ___lastReportedDate, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String) */, __this, L_0);
		double L_1 = ___percentCompleted;
		VirtActionInvoker1< double >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double) */, __this, L_1);
		bool L_2 = ___completed;
		__this->___m_Completed_0 = L_2;
		bool L_3 = ___hidden;
		__this->___m_Hidden_1 = L_3;
		DateTime_t220  L_4 = ___lastReportedDate;
		__this->___m_LastReportedDate_2 = L_4;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern TypeInfo* DateTime_t220_il2cpp_TypeInfo_var;
extern "C" void Achievement__ctor_m6952 (Achievement_t1264 * __this, String_t* ___id, double ___percent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String) */, __this, L_0);
		double L_1 = ___percent;
		VirtActionInvoker1< double >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double) */, __this, L_1);
		__this->___m_Hidden_1 = 0;
		__this->___m_Completed_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t220_il2cpp_TypeInfo_var);
		DateTime_t220  L_2 = ((DateTime_t220_StaticFields*)DateTime_t220_il2cpp_TypeInfo_var->static_fields)->___MinValue_13;
		__this->___m_LastReportedDate_2 = L_2;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern Il2CppCodeGenString* _stringLiteral1082;
extern "C" void Achievement__ctor_m6953 (Achievement_t1264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1082 = il2cpp_codegen_string_literal_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	{
		Achievement__ctor_m6952(__this, _stringLiteral1082, (0.0), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t60_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t41_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t220_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1081;
extern "C" String_t* Achievement_ToString_m6954 (Achievement_t1264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Double_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Boolean_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		DateTime_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1081 = il2cpp_codegen_string_literal_from_index(1081);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, ((int32_t)9)));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t34* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_3 = L_2;
		double L_4 = (double)VirtFuncInvoker0< double >::Invoke(6 /* System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted() */, __this);
		double L_5 = L_4;
		Object_t * L_6 = Box(Double_t60_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t34* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_8 = L_7;
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed() */, __this);
		bool L_10 = L_9;
		Object_t * L_11 = Box(Boolean_t41_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 4, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_13 = L_12;
		bool L_14 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden() */, __this);
		bool L_15 = L_14;
		Object_t * L_16 = Box(Boolean_t41_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t34* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_18 = L_17;
		DateTime_t220  L_19 = (DateTime_t220 )VirtFuncInvoker0< DateTime_t220  >::Invoke(10 /* System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate() */, __this);
		DateTime_t220  L_20 = L_19;
		Object_t * L_21 = Box(DateTime_t220_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 8, sizeof(Object_t *))) = (Object_t *)L_21;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m2254(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern "C" String_t* Achievement_get_id_m6955 (Achievement_t1264 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern "C" void Achievement_set_id_m6956 (Achievement_t1264 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern "C" double Achievement_get_percentCompleted_m6957 (Achievement_t1264 * __this, const MethodInfo* method)
{
	{
		double L_0 = (__this->___U3CpercentCompletedU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern "C" void Achievement_set_percentCompleted_m6958 (Achievement_t1264 * __this, double ___value, const MethodInfo* method)
{
	{
		double L_0 = ___value;
		__this->___U3CpercentCompletedU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern "C" bool Achievement_get_completed_m6959 (Achievement_t1264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Completed_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern "C" bool Achievement_get_hidden_m6960 (Achievement_t1264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Hidden_1);
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern "C" DateTime_t220  Achievement_get_lastReportedDate_m6961 (Achievement_t1264 * __this, const MethodInfo* method)
{
	{
		DateTime_t220  L_0 = (__this->___m_LastReportedDate_2);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern "C" void AchievementDescription__ctor_m6962 (AchievementDescription_t1265 * __this, String_t* ___id, String_t* ___title, Texture2D_t33 * ___image, String_t* ___achievedDescription, String_t* ___unachievedDescription, bool ___hidden, int32_t ___points, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String) */, __this, L_0);
		String_t* L_1 = ___title;
		__this->___m_Title_0 = L_1;
		Texture2D_t33 * L_2 = ___image;
		__this->___m_Image_1 = L_2;
		String_t* L_3 = ___achievedDescription;
		__this->___m_AchievedDescription_2 = L_3;
		String_t* L_4 = ___unachievedDescription;
		__this->___m_UnachievedDescription_3 = L_4;
		bool L_5 = ___hidden;
		__this->___m_Hidden_4 = L_5;
		int32_t L_6 = ___points;
		__this->___m_Points_5 = L_6;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t41_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1081;
extern "C" String_t* AchievementDescription_ToString_m6963 (AchievementDescription_t1265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		Boolean_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1081 = il2cpp_codegen_string_literal_from_index(1081);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, ((int32_t)11)));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t34* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_3 = L_2;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title() */, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t34* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_6 = L_5;
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription() */, __this);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_9 = L_8;
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription() */, __this);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 6, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t34* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_12 = L_11;
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points() */, __this);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 8, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t34* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, _stringLiteral1081);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)_stringLiteral1081;
		ObjectU5BU5D_t34* L_17 = L_16;
		bool L_18 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden() */, __this);
		bool L_19 = L_18;
		Object_t * L_20 = Box(Boolean_t41_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m2254(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern "C" void AchievementDescription_SetImage_m6964 (AchievementDescription_t1265 * __this, Texture2D_t33 * ___image, const MethodInfo* method)
{
	{
		Texture2D_t33 * L_0 = ___image;
		__this->___m_Image_1 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern "C" String_t* AchievementDescription_get_id_m6965 (AchievementDescription_t1265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern "C" void AchievementDescription_set_id_m6966 (AchievementDescription_t1265 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern "C" String_t* AchievementDescription_get_title_m6967 (AchievementDescription_t1265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Title_0);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern "C" String_t* AchievementDescription_get_achievedDescription_m6968 (AchievementDescription_t1265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_AchievedDescription_2);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern "C" String_t* AchievementDescription_get_unachievedDescription_m6969 (AchievementDescription_t1265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_UnachievedDescription_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern "C" bool AchievementDescription_get_hidden_m6970 (AchievementDescription_t1265 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Hidden_4);
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern "C" int32_t AchievementDescription_get_points_m6971 (AchievementDescription_t1265 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Points_5);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern TypeInfo* DateTime_t220_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral134;
extern "C" void Score__ctor_m6972 (Score_t1266 * __this, String_t* ___leaderboardID, int64_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral134 = il2cpp_codegen_string_literal_from_index(134);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID;
		int64_t L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t220_il2cpp_TypeInfo_var);
		DateTime_t220  L_2 = DateTime_get_Now_m2490(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Score__ctor_m6973(__this, L_0, L_1, _stringLiteral134, L_2, L_3, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern "C" void Score__ctor_m6973 (Score_t1266 * __this, String_t* ___leaderboardID, int64_t ___value, String_t* ___userID, DateTime_t220  ___date, String_t* ___formattedValue, int32_t ___rank, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___leaderboardID;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String) */, __this, L_0);
		int64_t L_1 = ___value;
		VirtActionInvoker1< int64_t >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64) */, __this, L_1);
		String_t* L_2 = ___userID;
		__this->___m_UserID_2 = L_2;
		DateTime_t220  L_3 = ___date;
		__this->___m_Date_0 = L_3;
		String_t* L_4 = ___formattedValue;
		__this->___m_FormattedValue_1 = L_4;
		int32_t L_5 = ___rank;
		__this->___m_Rank_3 = L_5;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t507_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t220_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1083;
extern Il2CppCodeGenString* _stringLiteral1084;
extern Il2CppCodeGenString* _stringLiteral1085;
extern Il2CppCodeGenString* _stringLiteral1086;
extern Il2CppCodeGenString* _stringLiteral1087;
extern "C" String_t* Score_ToString_m6974 (Score_t1266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		Int64_t507_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		DateTime_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1083 = il2cpp_codegen_string_literal_from_index(1083);
		_stringLiteral1084 = il2cpp_codegen_string_literal_from_index(1084);
		_stringLiteral1085 = il2cpp_codegen_string_literal_from_index(1085);
		_stringLiteral1086 = il2cpp_codegen_string_literal_from_index(1086);
		_stringLiteral1087 = il2cpp_codegen_string_literal_from_index(1087);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, ((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1083);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral1083;
		ObjectU5BU5D_t34* L_1 = L_0;
		int32_t L_2 = (__this->___m_Rank_3);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t34* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1084);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral1084;
		ObjectU5BU5D_t34* L_6 = L_5;
		int64_t L_7 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(6 /* System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value() */, __this);
		int64_t L_8 = L_7;
		Object_t * L_9 = Box(Int64_t507_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t34* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral1085);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral1085;
		ObjectU5BU5D_t34* L_11 = L_10;
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID() */, __this);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t34* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, _stringLiteral1086);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral1086;
		ObjectU5BU5D_t34* L_14 = L_13;
		String_t* L_15 = (__this->___m_UserID_2);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 7, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t34* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, _stringLiteral1087);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral1087;
		ObjectU5BU5D_t34* L_17 = L_16;
		DateTime_t220  L_18 = (__this->___m_Date_0);
		DateTime_t220  L_19 = L_18;
		Object_t * L_20 = Box(DateTime_t220_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)9));
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m2254(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern "C" String_t* Score_get_leaderboardID_m6975 (Score_t1266 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CleaderboardIDU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern "C" void Score_set_leaderboardID_m6976 (Score_t1266 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CleaderboardIDU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern "C" int64_t Score_get_value_m6977 (Score_t1266 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->___U3CvalueU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern "C" void Score_set_value_m6978 (Score_t1266 * __this, int64_t ___value, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value;
		__this->___U3CvalueU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern TypeInfo* Score_t1266_il2cpp_TypeInfo_var;
extern TypeInfo* ScoreU5BU5D_t1454_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t260_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1088;
extern "C" void Leaderboard__ctor_m6979 (Leaderboard_t1267 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Score_t1266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(886);
		ScoreU5BU5D_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(899);
		StringU5BU5D_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		_stringLiteral1088 = il2cpp_codegen_string_literal_from_index(1088);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(8 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String) */, __this, _stringLiteral1088);
		Range_t1269  L_0 = {0};
		Range__ctor_m7000(&L_0, 1, ((int32_t)10), /*hidden argument*/NULL);
		VirtActionInvoker1< Range_t1269  >::Invoke(10 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range) */, __this, L_0);
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope) */, __this, 0);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope) */, __this, 2);
		__this->___m_Loading_0 = 0;
		Score_t1266 * L_1 = (Score_t1266 *)il2cpp_codegen_object_new (Score_t1266_il2cpp_TypeInfo_var);
		Score__ctor_m6972(L_1, _stringLiteral1088, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		__this->___m_LocalUserScore_1 = L_1;
		__this->___m_MaxRange_2 = 0;
		__this->___m_Scores_3 = (IScoreU5BU5D_t1268*)((ScoreU5BU5D_t1454*)SZArrayNew(ScoreU5BU5D_t1454_il2cpp_TypeInfo_var, 0));
		__this->___m_Title_4 = _stringLiteral1088;
		__this->___m_UserIDs_5 = ((StringU5BU5D_t260*)SZArrayNew(StringU5BU5D_t260_il2cpp_TypeInfo_var, 0));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t41_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t558_il2cpp_TypeInfo_var;
extern TypeInfo* UserScope_t1275_il2cpp_TypeInfo_var;
extern TypeInfo* TimeScope_t1276_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1089;
extern Il2CppCodeGenString* _stringLiteral1090;
extern Il2CppCodeGenString* _stringLiteral1091;
extern Il2CppCodeGenString* _stringLiteral1092;
extern Il2CppCodeGenString* _stringLiteral104;
extern Il2CppCodeGenString* _stringLiteral1093;
extern Il2CppCodeGenString* _stringLiteral1094;
extern Il2CppCodeGenString* _stringLiteral1095;
extern Il2CppCodeGenString* _stringLiteral1096;
extern Il2CppCodeGenString* _stringLiteral1097;
extern "C" String_t* Leaderboard_ToString_m6980 (Leaderboard_t1267 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Boolean_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		UInt32_t558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(315);
		UserScope_t1275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(910);
		TimeScope_t1276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1089 = il2cpp_codegen_string_literal_from_index(1089);
		_stringLiteral1090 = il2cpp_codegen_string_literal_from_index(1090);
		_stringLiteral1091 = il2cpp_codegen_string_literal_from_index(1091);
		_stringLiteral1092 = il2cpp_codegen_string_literal_from_index(1092);
		_stringLiteral104 = il2cpp_codegen_string_literal_from_index(104);
		_stringLiteral1093 = il2cpp_codegen_string_literal_from_index(1093);
		_stringLiteral1094 = il2cpp_codegen_string_literal_from_index(1094);
		_stringLiteral1095 = il2cpp_codegen_string_literal_from_index(1095);
		_stringLiteral1096 = il2cpp_codegen_string_literal_from_index(1096);
		_stringLiteral1097 = il2cpp_codegen_string_literal_from_index(1097);
		s_Il2CppMethodIntialized = true;
	}
	Range_t1269  V_0 = {0};
	Range_t1269  V_1 = {0};
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, ((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1089);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral1089;
		ObjectU5BU5D_t34* L_1 = L_0;
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id() */, __this);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t34* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral1090);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral1090;
		ObjectU5BU5D_t34* L_4 = L_3;
		String_t* L_5 = (__this->___m_Title_4);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t34* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1091);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral1091;
		ObjectU5BU5D_t34* L_7 = L_6;
		bool L_8 = (__this->___m_Loading_0);
		bool L_9 = L_8;
		Object_t * L_10 = Box(Boolean_t41_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 5, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t34* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral1092);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral1092;
		ObjectU5BU5D_t34* L_12 = L_11;
		Range_t1269  L_13 = (Range_t1269 )VirtFuncInvoker0< Range_t1269  >::Invoke(6 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range() */, __this);
		V_0 = L_13;
		int32_t L_14 = ((&V_0)->___from_0);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 7, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t34* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral104);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral104;
		ObjectU5BU5D_t34* L_18 = L_17;
		Range_t1269  L_19 = (Range_t1269 )VirtFuncInvoker0< Range_t1269  >::Invoke(6 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range() */, __this);
		V_1 = L_19;
		int32_t L_20 = ((&V_1)->___count_1);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_22;
		ObjectU5BU5D_t34* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, _stringLiteral1093);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)_stringLiteral1093;
		ObjectU5BU5D_t34* L_24 = L_23;
		uint32_t L_25 = (__this->___m_MaxRange_2);
		uint32_t L_26 = L_25;
		Object_t * L_27 = Box(UInt32_t558_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t34* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, _stringLiteral1094);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)_stringLiteral1094;
		ObjectU5BU5D_t34* L_29 = L_28;
		IScoreU5BU5D_t1268* L_30 = (__this->___m_Scores_3);
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)((int32_t)(((Array_t *)L_30)->max_length))));
		Object_t * L_32 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_29, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_32;
		ObjectU5BU5D_t34* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, _stringLiteral1095);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)_stringLiteral1095;
		ObjectU5BU5D_t34* L_34 = L_33;
		int32_t L_35 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope() */, __this);
		int32_t L_36 = L_35;
		Object_t * L_37 = Box(UserScope_t1275_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_34, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_37;
		ObjectU5BU5D_t34* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, _stringLiteral1096);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_38, ((int32_t)16), sizeof(Object_t *))) = (Object_t *)_stringLiteral1096;
		ObjectU5BU5D_t34* L_39 = L_38;
		int32_t L_40 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope() */, __this);
		int32_t L_41 = L_40;
		Object_t * L_42 = Box(TimeScope_t1276_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_39, ((int32_t)17), sizeof(Object_t *))) = (Object_t *)L_42;
		ObjectU5BU5D_t34* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, _stringLiteral1097);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_43, ((int32_t)18), sizeof(Object_t *))) = (Object_t *)_stringLiteral1097;
		ObjectU5BU5D_t34* L_44 = L_43;
		StringU5BU5D_t260* L_45 = (__this->___m_UserIDs_5);
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)((int32_t)(((Array_t *)L_45)->max_length))));
		Object_t * L_47 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)19), sizeof(Object_t *))) = (Object_t *)L_47;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m2254(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern "C" void Leaderboard_SetLocalUserScore_m6981 (Leaderboard_t1267 * __this, Object_t * ___score, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___score;
		__this->___m_LocalUserScore_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern "C" void Leaderboard_SetMaxRange_m6982 (Leaderboard_t1267 * __this, uint32_t ___maxRange, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___maxRange;
		__this->___m_MaxRange_2 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern "C" void Leaderboard_SetScores_m6983 (Leaderboard_t1267 * __this, IScoreU5BU5D_t1268* ___scores, const MethodInfo* method)
{
	{
		IScoreU5BU5D_t1268* L_0 = ___scores;
		__this->___m_Scores_3 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern "C" void Leaderboard_SetTitle_m6984 (Leaderboard_t1267 * __this, String_t* ___title, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title;
		__this->___m_Title_4 = L_0;
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern "C" StringU5BU5D_t260* Leaderboard_GetUserFilter_m6985 (Leaderboard_t1267 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t260* L_0 = (__this->___m_UserIDs_5);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern "C" String_t* Leaderboard_get_id_m6986 (Leaderboard_t1267 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern "C" void Leaderboard_set_id_m6987 (Leaderboard_t1267 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_6 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern "C" int32_t Leaderboard_get_userScope_m6988 (Leaderboard_t1267 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CuserScopeU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C" void Leaderboard_set_userScope_m6989 (Leaderboard_t1267 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CuserScopeU3Ek__BackingField_7 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern "C" Range_t1269  Leaderboard_get_range_m6990 (Leaderboard_t1267 * __this, const MethodInfo* method)
{
	{
		Range_t1269  L_0 = (__this->___U3CrangeU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C" void Leaderboard_set_range_m6991 (Leaderboard_t1267 * __this, Range_t1269  ___value, const MethodInfo* method)
{
	{
		Range_t1269  L_0 = ___value;
		__this->___U3CrangeU3Ek__BackingField_8 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern "C" int32_t Leaderboard_get_timeScope_m6992 (Leaderboard_t1267 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CtimeScopeU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C" void Leaderboard_set_timeScope_m6993 (Leaderboard_t1267 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CtimeScopeU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C" void HitInfo_SendMessage_m6994 (HitInfo_t1271 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		GameObject_t27 * L_0 = (__this->___target_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		GameObject_SendMessage_m431(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C" bool HitInfo_Compare_m6995 (Object_t * __this /* static, unused */, HitInfo_t1271  ___lhs, HitInfo_t1271  ___rhs, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t27 * L_0 = ((&___lhs)->___target_0);
		GameObject_t27 * L_1 = ((&___rhs)->___target_0);
		bool L_2 = Object_op_Equality_m427(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Camera_t510 * L_3 = ((&___lhs)->___camera_1);
		Camera_t510 * L_4 = ((&___rhs)->___camera_1);
		bool L_5 = Object_op_Equality_m427(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C" bool HitInfo_op_Implicit_m6996 (Object_t * __this /* static, unused */, HitInfo_t1271  ___exists, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t27 * L_0 = ((&___exists)->___target_0);
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t510 * L_2 = ((&___exists)->___camera_1);
		bool L_3 = Object_op_Inequality_m395(NULL /*static, unused*/, L_2, (Object_t53 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern TypeInfo* HitInfoU5BU5D_t1273_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t1271_il2cpp_TypeInfo_var;
extern TypeInfo* SendMouseEvents_t1272_il2cpp_TypeInfo_var;
extern TypeInfo* RaycastHit2DU5BU5D_t835_il2cpp_TypeInfo_var;
extern TypeInfo* RaycastHit2D_t838_il2cpp_TypeInfo_var;
extern "C" void SendMouseEvents__cctor_m6997 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HitInfoU5BU5D_t1273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(912);
		HitInfo_t1271_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(913);
		SendMouseEvents_t1272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(914);
		RaycastHit2DU5BU5D_t835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(915);
		RaycastHit2D_t838_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(916);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t1271  V_0 = {0};
	HitInfo_t1271  V_1 = {0};
	HitInfo_t1271  V_2 = {0};
	HitInfo_t1271  V_3 = {0};
	HitInfo_t1271  V_4 = {0};
	HitInfo_t1271  V_5 = {0};
	HitInfo_t1271  V_6 = {0};
	HitInfo_t1271  V_7 = {0};
	HitInfo_t1271  V_8 = {0};
	RaycastHit2D_t838  V_9 = {0};
	{
		HitInfoU5BU5D_t1273* L_0 = ((HitInfoU5BU5D_t1273*)SZArrayNew(HitInfoU5BU5D_t1273_il2cpp_TypeInfo_var, 3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t1271  L_1 = V_0;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_0, 0, sizeof(HitInfo_t1271 )))) = L_1;
		HitInfoU5BU5D_t1273* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t1271  L_3 = V_1;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_2, 1, sizeof(HitInfo_t1271 )))) = L_3;
		HitInfoU5BU5D_t1273* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t1271  L_5 = V_2;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_4, 2, sizeof(HitInfo_t1271 )))) = L_5;
		((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_3 = L_4;
		HitInfoU5BU5D_t1273* L_6 = ((HitInfoU5BU5D_t1273*)SZArrayNew(HitInfoU5BU5D_t1273_il2cpp_TypeInfo_var, 3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t1271  L_7 = V_3;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_6, 0, sizeof(HitInfo_t1271 )))) = L_7;
		HitInfoU5BU5D_t1273* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t1271  L_9 = V_4;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_8, 1, sizeof(HitInfo_t1271 )))) = L_9;
		HitInfoU5BU5D_t1273* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t1271  L_11 = V_5;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_10, 2, sizeof(HitInfo_t1271 )))) = L_11;
		((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4 = L_10;
		HitInfoU5BU5D_t1273* L_12 = ((HitInfoU5BU5D_t1273*)SZArrayNew(HitInfoU5BU5D_t1273_il2cpp_TypeInfo_var, 3));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t1271  L_13 = V_6;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_12, 0, sizeof(HitInfo_t1271 )))) = L_13;
		HitInfoU5BU5D_t1273* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t1271  L_15 = V_7;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_14, 1, sizeof(HitInfo_t1271 )))) = L_15;
		HitInfoU5BU5D_t1273* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t1271  L_17 = V_8;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_16, 2, sizeof(HitInfo_t1271 )))) = L_17;
		((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5 = L_16;
		RaycastHit2DU5BU5D_t835* L_18 = ((RaycastHit2DU5BU5D_t835*)SZArrayNew(RaycastHit2DU5BU5D_t835_il2cpp_TypeInfo_var, 1));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		Initobj (RaycastHit2D_t838_il2cpp_TypeInfo_var, (&V_9));
		RaycastHit2D_t838  L_19 = V_9;
		(*(RaycastHit2D_t838 *)((RaycastHit2D_t838 *)(RaycastHit2D_t838 *)SZArrayLdElema(L_18, 0, sizeof(RaycastHit2D_t838 )))) = L_19;
		((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseRayHits2D_6 = L_18;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32,System.Int32)
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern TypeInfo* SendMouseEvents_t1272_il2cpp_TypeInfo_var;
extern TypeInfo* CameraU5BU5D_t600_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t1271_il2cpp_TypeInfo_var;
extern TypeInfo* Physics2D_t837_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t1375_m8131_MethodInfo_var;
extern "C" void SendMouseEvents_DoSendMouseEvents_m6998 (Object_t * __this /* static, unused */, int32_t ___mouseUsed, int32_t ___skipRTCameras, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		SendMouseEvents_t1272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(914);
		CameraU5BU5D_t600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(918);
		HitInfo_t1271_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(913);
		Physics2D_t837_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		Component_GetComponent_TisGUILayer_t1375_m8131_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484697);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Camera_t510 * V_5 = {0};
	Rect_t30  V_6 = {0};
	GUILayer_t1375 * V_7 = {0};
	GUIElement_t1370 * V_8 = {0};
	Ray_t567  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	RaycastHit_t566  V_12 = {0};
	int32_t V_13 = 0;
	HitInfo_t1271  V_14 = {0};
	Vector3_t6  V_15 = {0};
	float G_B23_0 = 0.0f;
	HitInfo_t1271 * G_B26_0 = {0};
	HitInfo_t1271 * G_B25_0 = {0};
	GameObject_t27 * G_B27_0 = {0};
	HitInfo_t1271 * G_B27_1 = {0};
	HitInfo_t1271 * G_B34_0 = {0};
	HitInfo_t1271 * G_B33_0 = {0};
	GameObject_t27 * G_B35_0 = {0};
	HitInfo_t1271 * G_B35_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		Vector3_t6  L_0 = Input_get_mousePosition_m2316(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		int32_t L_1 = Camera_get_allCamerasCount_m8013(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		CameraU5BU5D_t600* L_2 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		CameraU5BU5D_t600* L_3 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		NullCheck(L_3);
		int32_t L_4 = V_2;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_0030;
		}
	}

IL_0025:
	{
		int32_t L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7 = ((CameraU5BU5D_t600*)SZArrayNew(CameraU5BU5D_t600_il2cpp_TypeInfo_var, L_5));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		CameraU5BU5D_t600* L_6 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		int32_t L_7 = Camera_GetAllCameras_m8014(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_3 = 0;
		goto IL_0060;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_8 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_14));
		HitInfo_t1271  L_10 = V_14;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_8, L_9, sizeof(HitInfo_t1271 )))) = L_10;
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0060:
	{
		int32_t L_12 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_13 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_14 = ___mouseUsed;
		if (L_14)
		{
			goto IL_033f;
		}
	}
	{
		V_4 = 0;
		goto IL_0337;
	}

IL_007b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		CameraU5BU5D_t600* L_15 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		int32_t L_16 = V_4;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		V_5 = (*(Camera_t510 **)(Camera_t510 **)SZArrayLdElema(L_15, L_17, sizeof(Camera_t510 *)));
		Camera_t510 * L_18 = V_5;
		bool L_19 = Object_op_Equality_m427(NULL /*static, unused*/, L_18, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_20 = ___skipRTCameras;
		if (!L_20)
		{
			goto IL_00af;
		}
	}
	{
		Camera_t510 * L_21 = V_5;
		NullCheck(L_21);
		RenderTexture_t582 * L_22 = Camera_get_targetTexture_m8005(L_21, /*hidden argument*/NULL);
		bool L_23 = Object_op_Inequality_m395(NULL /*static, unused*/, L_22, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00af;
		}
	}

IL_00aa:
	{
		goto IL_0331;
	}

IL_00af:
	{
		Camera_t510 * L_24 = V_5;
		NullCheck(L_24);
		Rect_t30  L_25 = Camera_get_pixelRect_m6444(L_24, /*hidden argument*/NULL);
		V_6 = L_25;
		Vector3_t6  L_26 = V_0;
		bool L_27 = Rect_Contains_m7872((&V_6), L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_0331;
	}

IL_00ca:
	{
		Camera_t510 * L_28 = V_5;
		NullCheck(L_28);
		GUILayer_t1375 * L_29 = Component_GetComponent_TisGUILayer_t1375_m8131(L_28, /*hidden argument*/Component_GetComponent_TisGUILayer_t1375_m8131_MethodInfo_var);
		V_7 = L_29;
		GUILayer_t1375 * L_30 = V_7;
		bool L_31 = Object_op_Implicit_m301(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0145;
		}
	}
	{
		GUILayer_t1375 * L_32 = V_7;
		Vector3_t6  L_33 = V_0;
		NullCheck(L_32);
		GUIElement_t1370 * L_34 = GUILayer_HitTest_m7496(L_32, L_33, /*hidden argument*/NULL);
		V_8 = L_34;
		GUIElement_t1370 * L_35 = V_8;
		bool L_36 = Object_op_Implicit_m301(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_37 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		GUIElement_t1370 * L_38 = V_8;
		NullCheck(L_38);
		GameObject_t27 * L_39 = Component_get_gameObject_m306(L_38, /*hidden argument*/NULL);
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_37, 0, sizeof(HitInfo_t1271 )))->___target_0 = L_39;
		HitInfoU5BU5D_t1273* L_40 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 0);
		Camera_t510 * L_41 = V_5;
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_40, 0, sizeof(HitInfo_t1271 )))->___camera_1 = L_41;
		goto IL_0145;
	}

IL_0123:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_42 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_42, 0, sizeof(HitInfo_t1271 )))->___target_0 = (GameObject_t27 *)NULL;
		HitInfoU5BU5D_t1273* L_43 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_43, 0, sizeof(HitInfo_t1271 )))->___camera_1 = (Camera_t510 *)NULL;
	}

IL_0145:
	{
		Camera_t510 * L_44 = V_5;
		NullCheck(L_44);
		int32_t L_45 = Camera_get_eventMask_m8001(L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_0156;
		}
	}
	{
		goto IL_0331;
	}

IL_0156:
	{
		Camera_t510 * L_46 = V_5;
		Vector3_t6  L_47 = V_0;
		NullCheck(L_46);
		Ray_t567  L_48 = Camera_ScreenPointToRay_m2544(L_46, L_47, /*hidden argument*/NULL);
		V_9 = L_48;
		Vector3_t6  L_49 = Ray_get_direction_m4086((&V_9), /*hidden argument*/NULL);
		V_15 = L_49;
		float L_50 = ((&V_15)->___z_3);
		V_10 = L_50;
		float L_51 = V_10;
		bool L_52 = Mathf_Approximately_m4062(NULL /*static, unused*/, (0.0f), L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_018d;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_01a4;
	}

IL_018d:
	{
		Camera_t510 * L_53 = V_5;
		NullCheck(L_53);
		float L_54 = Camera_get_farClipPlane_m4083(L_53, /*hidden argument*/NULL);
		Camera_t510 * L_55 = V_5;
		NullCheck(L_55);
		float L_56 = Camera_get_nearClipPlane_m4084(L_55, /*hidden argument*/NULL);
		float L_57 = V_10;
		float L_58 = fabsf(((float)((float)((float)((float)L_54-(float)L_56))/(float)L_57)));
		G_B23_0 = L_58;
	}

IL_01a4:
	{
		V_11 = G_B23_0;
		Ray_t567  L_59 = V_9;
		float L_60 = V_11;
		Camera_t510 * L_61 = V_5;
		NullCheck(L_61);
		int32_t L_62 = Camera_get_cullingMask_m4096(L_61, /*hidden argument*/NULL);
		Camera_t510 * L_63 = V_5;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_eventMask_m8001(L_63, /*hidden argument*/NULL);
		bool L_65 = Physics_Raycast_m4164(NULL /*static, unused*/, L_59, (&V_12), ((float)((float)L_60+(float)(1.0f))), ((int32_t)((int32_t)((int32_t)((int32_t)L_62&(int32_t)L_64))&(int32_t)((int32_t)-5))), /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_0223;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_66 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 1);
		Camera_t510 * L_67 = V_5;
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_66, 1, sizeof(HitInfo_t1271 )))->___camera_1 = L_67;
		HitInfoU5BU5D_t1273* L_68 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, 1);
		Rigidbody_t50 * L_69 = RaycastHit_get_rigidbody_m7138((&V_12), /*hidden argument*/NULL);
		bool L_70 = Object_op_Implicit_m301(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		G_B25_0 = ((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_68, 1, sizeof(HitInfo_t1271 )));
		if (!L_70)
		{
			G_B26_0 = ((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_68, 1, sizeof(HitInfo_t1271 )));
			goto IL_020d;
		}
	}
	{
		Rigidbody_t50 * L_71 = RaycastHit_get_rigidbody_m7138((&V_12), /*hidden argument*/NULL);
		NullCheck(L_71);
		GameObject_t27 * L_72 = Component_get_gameObject_m306(L_71, /*hidden argument*/NULL);
		G_B27_0 = L_72;
		G_B27_1 = G_B25_0;
		goto IL_0219;
	}

IL_020d:
	{
		Collider_t316 * L_73 = RaycastHit_get_collider_m4101((&V_12), /*hidden argument*/NULL);
		NullCheck(L_73);
		GameObject_t27 * L_74 = Component_get_gameObject_m306(L_73, /*hidden argument*/NULL);
		G_B27_0 = L_74;
		G_B27_1 = G_B26_0;
	}

IL_0219:
	{
		G_B27_1->___target_0 = G_B27_0;
		goto IL_025f;
	}

IL_0223:
	{
		Camera_t510 * L_75 = V_5;
		NullCheck(L_75);
		int32_t L_76 = Camera_get_clearFlags_m8008(L_75, /*hidden argument*/NULL);
		if ((((int32_t)L_76) == ((int32_t)1)))
		{
			goto IL_023d;
		}
	}
	{
		Camera_t510 * L_77 = V_5;
		NullCheck(L_77);
		int32_t L_78 = Camera_get_clearFlags_m8008(L_77, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_78) == ((uint32_t)2))))
		{
			goto IL_025f;
		}
	}

IL_023d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_79 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 1);
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_79, 1, sizeof(HitInfo_t1271 )))->___target_0 = (GameObject_t27 *)NULL;
		HitInfoU5BU5D_t1273* L_80 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 1);
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_80, 1, sizeof(HitInfo_t1271 )))->___camera_1 = (Camera_t510 *)NULL;
	}

IL_025f:
	{
		Ray_t567  L_81 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t835* L_82 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseRayHits2D_6;
		float L_83 = V_11;
		Camera_t510 * L_84 = V_5;
		NullCheck(L_84);
		int32_t L_85 = Camera_get_cullingMask_m4096(L_84, /*hidden argument*/NULL);
		Camera_t510 * L_86 = V_5;
		NullCheck(L_86);
		int32_t L_87 = Camera_get_eventMask_m8001(L_86, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t837_il2cpp_TypeInfo_var);
		int32_t L_88 = Physics2D_GetRayIntersectionNonAlloc_m7145(NULL /*static, unused*/, L_81, L_82, L_83, ((int32_t)((int32_t)((int32_t)((int32_t)L_85&(int32_t)L_87))&(int32_t)((int32_t)-5))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_88) == ((uint32_t)1))))
		{
			goto IL_02f5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_89 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 2);
		Camera_t510 * L_90 = V_5;
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_89, 2, sizeof(HitInfo_t1271 )))->___camera_1 = L_90;
		HitInfoU5BU5D_t1273* L_91 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 2);
		RaycastHit2DU5BU5D_t835* L_92 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseRayHits2D_6;
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, 0);
		Rigidbody2D_t1314 * L_93 = RaycastHit2D_get_rigidbody_m7147(((RaycastHit2D_t838 *)(RaycastHit2D_t838 *)SZArrayLdElema(L_92, 0, sizeof(RaycastHit2D_t838 ))), /*hidden argument*/NULL);
		bool L_94 = Object_op_Implicit_m301(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		G_B33_0 = ((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_91, 2, sizeof(HitInfo_t1271 )));
		if (!L_94)
		{
			G_B34_0 = ((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_91, 2, sizeof(HitInfo_t1271 )));
			goto IL_02d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t835* L_95 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseRayHits2D_6;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 0);
		Rigidbody2D_t1314 * L_96 = RaycastHit2D_get_rigidbody_m7147(((RaycastHit2D_t838 *)(RaycastHit2D_t838 *)SZArrayLdElema(L_95, 0, sizeof(RaycastHit2D_t838 ))), /*hidden argument*/NULL);
		NullCheck(L_96);
		GameObject_t27 * L_97 = Component_get_gameObject_m306(L_96, /*hidden argument*/NULL);
		G_B35_0 = L_97;
		G_B35_1 = G_B33_0;
		goto IL_02eb;
	}

IL_02d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t835* L_98 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseRayHits2D_6;
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 0);
		Collider2D_t839 * L_99 = RaycastHit2D_get_collider_m4088(((RaycastHit2D_t838 *)(RaycastHit2D_t838 *)SZArrayLdElema(L_98, 0, sizeof(RaycastHit2D_t838 ))), /*hidden argument*/NULL);
		NullCheck(L_99);
		GameObject_t27 * L_100 = Component_get_gameObject_m306(L_99, /*hidden argument*/NULL);
		G_B35_0 = L_100;
		G_B35_1 = G_B34_0;
	}

IL_02eb:
	{
		G_B35_1->___target_0 = G_B35_0;
		goto IL_0331;
	}

IL_02f5:
	{
		Camera_t510 * L_101 = V_5;
		NullCheck(L_101);
		int32_t L_102 = Camera_get_clearFlags_m8008(L_101, /*hidden argument*/NULL);
		if ((((int32_t)L_102) == ((int32_t)1)))
		{
			goto IL_030f;
		}
	}
	{
		Camera_t510 * L_103 = V_5;
		NullCheck(L_103);
		int32_t L_104 = Camera_get_clearFlags_m8008(L_103, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_104) == ((uint32_t)2))))
		{
			goto IL_0331;
		}
	}

IL_030f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_105 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, 2);
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_105, 2, sizeof(HitInfo_t1271 )))->___target_0 = (GameObject_t27 *)NULL;
		HitInfoU5BU5D_t1273* L_106 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_106);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_106, 2);
		((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_106, 2, sizeof(HitInfo_t1271 )))->___camera_1 = (Camera_t510 *)NULL;
	}

IL_0331:
	{
		int32_t L_107 = V_4;
		V_4 = ((int32_t)((int32_t)L_107+(int32_t)1));
	}

IL_0337:
	{
		int32_t L_108 = V_4;
		int32_t L_109 = V_1;
		if ((((int32_t)L_108) < ((int32_t)L_109)))
		{
			goto IL_007b;
		}
	}

IL_033f:
	{
		V_13 = 0;
		goto IL_0365;
	}

IL_0347:
	{
		int32_t L_110 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_111 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		int32_t L_112 = V_13;
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, L_112);
		SendMouseEvents_SendEvents_m6999(NULL /*static, unused*/, L_110, (*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_111, L_112, sizeof(HitInfo_t1271 )))), /*hidden argument*/NULL);
		int32_t L_113 = V_13;
		V_13 = ((int32_t)((int32_t)L_113+(int32_t)1));
	}

IL_0365:
	{
		int32_t L_114 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_115 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_5;
		NullCheck(L_115);
		if ((((int32_t)L_114) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_115)->max_length)))))))
		{
			goto IL_0347;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern TypeInfo* SendMouseEvents_t1272_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t1271_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1098;
extern Il2CppCodeGenString* _stringLiteral1099;
extern Il2CppCodeGenString* _stringLiteral1100;
extern Il2CppCodeGenString* _stringLiteral1101;
extern Il2CppCodeGenString* _stringLiteral1102;
extern Il2CppCodeGenString* _stringLiteral1103;
extern Il2CppCodeGenString* _stringLiteral1104;
extern "C" void SendMouseEvents_SendEvents_m6999 (Object_t * __this /* static, unused */, int32_t ___i, HitInfo_t1271  ___hit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		SendMouseEvents_t1272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(914);
		HitInfo_t1271_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(913);
		_stringLiteral1098 = il2cpp_codegen_string_literal_from_index(1098);
		_stringLiteral1099 = il2cpp_codegen_string_literal_from_index(1099);
		_stringLiteral1100 = il2cpp_codegen_string_literal_from_index(1100);
		_stringLiteral1101 = il2cpp_codegen_string_literal_from_index(1101);
		_stringLiteral1102 = il2cpp_codegen_string_literal_from_index(1102);
		_stringLiteral1103 = il2cpp_codegen_string_literal_from_index(1103);
		_stringLiteral1104 = il2cpp_codegen_string_literal_from_index(1104);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t1271  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2314(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m2612(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		HitInfo_t1271  L_3 = ___hit;
		bool L_4 = HitInfo_op_Implicit_m6996(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_5 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_6 = ___i;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		HitInfo_t1271  L_7 = ___hit;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_5, L_6, sizeof(HitInfo_t1271 )))) = L_7;
		HitInfoU5BU5D_t1273* L_8 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_9 = ___i;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		HitInfo_SendMessage_m6994(((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_8, L_9, sizeof(HitInfo_t1271 ))), _stringLiteral1098, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_11 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_12 = ___i;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		bool L_13 = HitInfo_op_Implicit_m6996(NULL /*static, unused*/, (*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_11, L_12, sizeof(HitInfo_t1271 )))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c8;
		}
	}
	{
		HitInfo_t1271  L_14 = ___hit;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_15 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_16 = ___i;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		bool L_17 = HitInfo_Compare_m6995(NULL /*static, unused*/, L_14, (*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_15, L_16, sizeof(HitInfo_t1271 )))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_18 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_19 = ___i;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		HitInfo_SendMessage_m6994(((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_18, L_19, sizeof(HitInfo_t1271 ))), _stringLiteral1099, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_20 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_21 = ___i;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		HitInfo_SendMessage_m6994(((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_20, L_21, sizeof(HitInfo_t1271 ))), _stringLiteral1100, /*hidden argument*/NULL);
		HitInfoU5BU5D_t1273* L_22 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_23 = ___i;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		Initobj (HitInfo_t1271_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t1271  L_24 = V_2;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_22, L_23, sizeof(HitInfo_t1271 )))) = L_24;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_25 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_26 = ___i;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		bool L_27 = HitInfo_op_Implicit_m6996(NULL /*static, unused*/, (*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_25, L_26, sizeof(HitInfo_t1271 )))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_28 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_4;
		int32_t L_29 = ___i;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		HitInfo_SendMessage_m6994(((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_28, L_29, sizeof(HitInfo_t1271 ))), _stringLiteral1101, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		HitInfo_t1271  L_30 = ___hit;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_31 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_3;
		int32_t L_32 = ___i;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		bool L_33 = HitInfo_Compare_m6995(NULL /*static, unused*/, L_30, (*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_31, L_32, sizeof(HitInfo_t1271 )))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0133;
		}
	}
	{
		HitInfo_t1271  L_34 = ___hit;
		bool L_35 = HitInfo_op_Implicit_m6996(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m6994((&___hit), _stringLiteral1102, /*hidden argument*/NULL);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_36 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_3;
		int32_t L_37 = ___i;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		bool L_38 = HitInfo_op_Implicit_m6996(NULL /*static, unused*/, (*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_36, L_37, sizeof(HitInfo_t1271 )))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_39 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_3;
		int32_t L_40 = ___i;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		HitInfo_SendMessage_m6994(((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_39, L_40, sizeof(HitInfo_t1271 ))), _stringLiteral1103, /*hidden argument*/NULL);
	}

IL_0162:
	{
		HitInfo_t1271  L_41 = ___hit;
		bool L_42 = HitInfo_op_Implicit_m6996(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m6994((&___hit), _stringLiteral1104, /*hidden argument*/NULL);
		HitInfo_SendMessage_m6994((&___hit), _stringLiteral1102, /*hidden argument*/NULL);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1272_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1273* L_43 = ((SendMouseEvents_t1272_StaticFields*)SendMouseEvents_t1272_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_3;
		int32_t L_44 = ___i;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		HitInfo_t1271  L_45 = ___hit;
		(*(HitInfo_t1271 *)((HitInfo_t1271 *)(HitInfo_t1271 *)SZArrayLdElema(L_43, L_44, sizeof(HitInfo_t1271 )))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern "C" void Range__ctor_m7000 (Range_t1269 * __this, int32_t ___fromValue, int32_t ___valueCount, const MethodInfo* method)
{
	{
		int32_t L_0 = ___fromValue;
		__this->___from_0 = L_0;
		int32_t L_1 = ___valueCount;
		__this->___count_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C" void PropertyAttribute__ctor_m7001 (PropertyAttribute_t1277 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C" void TooltipAttribute__ctor_m7002 (TooltipAttribute_t1278 * __this, String_t* ___tooltip, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m7001(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip;
		__this->___tooltip_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C" void SpaceAttribute__ctor_m7003 (SpaceAttribute_t1279 * __this, float ___height, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m7001(__this, /*hidden argument*/NULL);
		float L_0 = ___height;
		__this->___height_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C" void RangeAttribute__ctor_m7004 (RangeAttribute_t1280 * __this, float ___min, float ___max, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m7001(__this, /*hidden argument*/NULL);
		float L_0 = ___min;
		__this->___min_0 = L_0;
		float L_1 = ___max;
		__this->___max_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C" void TextAreaAttribute__ctor_m7005 (TextAreaAttribute_t1281 * __this, int32_t ___minLines, int32_t ___maxLines, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m7001(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___minLines;
		__this->___minLines_0 = L_0;
		int32_t L_1 = ___maxLines;
		__this->___maxLines_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C" void SelectionBaseAttribute__ctor_m7006 (SelectionBaseAttribute_t1282 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C" void SliderState__ctor_m7007 (SliderState_t1283 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::.ctor()
extern "C" void StackTraceUtility__ctor_m7008 (StackTraceUtility_t1284 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t1284_il2cpp_TypeInfo_var;
extern "C" void StackTraceUtility__cctor_m7009 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		StackTraceUtility_t1284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((StackTraceUtility_t1284_StaticFields*)StackTraceUtility_t1284_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern TypeInfo* StackTraceUtility_t1284_il2cpp_TypeInfo_var;
extern "C" void StackTraceUtility_SetProjectFolder_m7010 (Object_t * __this /* static, unused */, String_t* ___folder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackTraceUtility_t1284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___folder;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		((StackTraceUtility_t1284_StaticFields*)StackTraceUtility_t1284_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0 = L_0;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern TypeInfo* StackTrace_t1437_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t1284_il2cpp_TypeInfo_var;
extern "C" String_t* StackTraceUtility_ExtractStackTrace_m7011 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackTrace_t1437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(920);
		StackTraceUtility_t1284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		s_Il2CppMethodIntialized = true;
	}
	StackTrace_t1437 * V_0 = {0};
	String_t* V_1 = {0};
	{
		StackTrace_t1437 * L_0 = (StackTrace_t1437 *)il2cpp_codegen_object_new (StackTrace_t1437_il2cpp_TypeInfo_var);
		StackTrace__ctor_m8132(L_0, 1, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t1437 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m7016(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_ToString_m8133(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1105;
extern Il2CppCodeGenString* _stringLiteral1106;
extern Il2CppCodeGenString* _stringLiteral1107;
extern Il2CppCodeGenString* _stringLiteral1108;
extern Il2CppCodeGenString* _stringLiteral1109;
extern Il2CppCodeGenString* _stringLiteral1110;
extern "C" bool StackTraceUtility_IsSystemStacktraceType_m7012 (Object_t * __this /* static, unused */, Object_t * ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1105 = il2cpp_codegen_string_literal_from_index(1105);
		_stringLiteral1106 = il2cpp_codegen_string_literal_from_index(1106);
		_stringLiteral1107 = il2cpp_codegen_string_literal_from_index(1107);
		_stringLiteral1108 = il2cpp_codegen_string_literal_from_index(1108);
		_stringLiteral1109 = il2cpp_codegen_string_literal_from_index(1109);
		_stringLiteral1110 = il2cpp_codegen_string_literal_from_index(1110);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___name;
		V_0 = ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m2414(L_1, _stringLiteral1105, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m2414(L_3, _stringLiteral1106, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m2414(L_5, _stringLiteral1107, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m2414(L_7, _stringLiteral1108, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m2414(L_9, _stringLiteral1109, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m2414(L_11, _stringLiteral1110, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t1284_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1111;
extern "C" String_t* StackTraceUtility_ExtractStringFromException_m7013 (Object_t * __this /* static, unused */, Object_t * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		StackTraceUtility_t1284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		_stringLiteral1111 = il2cpp_codegen_string_literal_from_index(1111);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		Object_t * L_2 = ___exception;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		StackTraceUtility_ExtractStringFromExceptionInternal_m7014(NULL /*static, unused*/, L_2, (&V_0), (&V_1), /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		String_t* L_4 = V_1;
		String_t* L_5 = String_Concat_m2152(NULL /*static, unused*/, L_3, _stringLiteral1111, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t305_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTrace_t1437_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t1284_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1112;
extern Il2CppCodeGenString* _stringLiteral1113;
extern Il2CppCodeGenString* _stringLiteral1111;
extern Il2CppCodeGenString* _stringLiteral1114;
extern Il2CppCodeGenString* _stringLiteral1115;
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m7014 (Object_t * __this /* static, unused */, Object_t * ___exceptiono, String_t** ___message, String_t** ___stackTrace, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		StringBuilder_t305_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(309);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		StackTrace_t1437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(920);
		StackTraceUtility_t1284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		_stringLiteral1112 = il2cpp_codegen_string_literal_from_index(1112);
		_stringLiteral1113 = il2cpp_codegen_string_literal_from_index(1113);
		_stringLiteral1111 = il2cpp_codegen_string_literal_from_index(1111);
		_stringLiteral1114 = il2cpp_codegen_string_literal_from_index(1114);
		_stringLiteral1115 = il2cpp_codegen_string_literal_from_index(1115);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t40 * V_0 = {0};
	StringBuilder_t305 * V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	StackTrace_t1437 * V_5 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___exceptiono;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t513 * L_1 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_1, _stringLiteral1112, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___exceptiono;
		V_0 = ((Exception_t40 *)IsInstClass(L_2, Exception_t40_il2cpp_TypeInfo_var));
		Exception_t40 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentException_t513 * L_4 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_4, _stringLiteral1113, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0029:
	{
		Exception_t40 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		Exception_t40 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m410(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004b:
	{
		StringBuilder_t305 * L_10 = (StringBuilder_t305 *)il2cpp_codegen_object_new (StringBuilder_t305_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m6520(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		*((Object_t **)(L_11)) = (Object_t *)L_12;
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_2 = L_13;
		goto IL_00ff;
	}

IL_0063:
	{
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m410(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		Exception_t40 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_16);
		V_2 = L_17;
		goto IL_008c;
	}

IL_007a:
	{
		Exception_t40 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_18);
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m2152(NULL /*static, unused*/, L_19, _stringLiteral1111, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
	}

IL_008c:
	{
		Exception_t40 * L_22 = V_0;
		NullCheck(L_22);
		Type_t * L_23 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(10 /* System.Type System.Exception::GetType() */, L_22);
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		V_3 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_4 = L_25;
		Exception_t40 * L_26 = V_0;
		NullCheck(L_26);
		String_t* L_27 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_26);
		if (!L_27)
		{
			goto IL_00b2;
		}
	}
	{
		Exception_t40 * L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_28);
		V_4 = L_29;
	}

IL_00b2:
	{
		String_t* L_30 = V_4;
		NullCheck(L_30);
		String_t* L_31 = String_Trim_m8134(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m410(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d8;
		}
	}
	{
		String_t* L_33 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m409(NULL /*static, unused*/, L_33, _stringLiteral1114, /*hidden argument*/NULL);
		V_3 = L_34;
		String_t* L_35 = V_3;
		String_t* L_36 = V_4;
		String_t* L_37 = String_Concat_m409(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
	}

IL_00d8:
	{
		String_t** L_38 = ___message;
		String_t* L_39 = V_3;
		*((Object_t **)(L_38)) = (Object_t *)L_39;
		Exception_t40 * L_40 = V_0;
		NullCheck(L_40);
		Exception_t40 * L_41 = (Exception_t40 *)VirtFuncInvoker0< Exception_t40 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_40);
		if (!L_41)
		{
			goto IL_00f8;
		}
	}
	{
		String_t* L_42 = V_3;
		String_t* L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m2303(NULL /*static, unused*/, _stringLiteral1115, L_42, _stringLiteral1111, L_43, /*hidden argument*/NULL);
		V_2 = L_44;
	}

IL_00f8:
	{
		Exception_t40 * L_45 = V_0;
		NullCheck(L_45);
		Exception_t40 * L_46 = (Exception_t40 *)VirtFuncInvoker0< Exception_t40 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_45);
		V_0 = L_46;
	}

IL_00ff:
	{
		Exception_t40 * L_47 = V_0;
		if (L_47)
		{
			goto IL_0063;
		}
	}
	{
		StringBuilder_t305 * L_48 = V_1;
		String_t* L_49 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m409(NULL /*static, unused*/, L_49, _stringLiteral1111, /*hidden argument*/NULL);
		NullCheck(L_48);
		StringBuilder_Append_m2533(L_48, L_50, /*hidden argument*/NULL);
		StackTrace_t1437 * L_51 = (StackTrace_t1437 *)il2cpp_codegen_object_new (StackTrace_t1437_il2cpp_TypeInfo_var);
		StackTrace__ctor_m8132(L_51, 1, 1, /*hidden argument*/NULL);
		V_5 = L_51;
		StringBuilder_t305 * L_52 = V_1;
		StackTrace_t1437 * L_53 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		String_t* L_54 = StackTraceUtility_ExtractFormattedStackTrace_m7016(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		StringBuilder_Append_m2533(L_52, L_54, /*hidden argument*/NULL);
		String_t** L_55 = ___stackTrace;
		StringBuilder_t305 * L_56 = V_1;
		NullCheck(L_56);
		String_t* L_57 = StringBuilder_ToString_m2528(L_56, /*hidden argument*/NULL);
		*((Object_t **)(L_55)) = (Object_t *)L_57;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t493_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t305_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t1284_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1116;
extern Il2CppCodeGenString* _stringLiteral1117;
extern Il2CppCodeGenString* _stringLiteral1118;
extern Il2CppCodeGenString* _stringLiteral1119;
extern Il2CppCodeGenString* _stringLiteral1120;
extern Il2CppCodeGenString* _stringLiteral1121;
extern Il2CppCodeGenString* _stringLiteral1122;
extern Il2CppCodeGenString* _stringLiteral1123;
extern Il2CppCodeGenString* _stringLiteral1124;
extern Il2CppCodeGenString* _stringLiteral1125;
extern Il2CppCodeGenString* _stringLiteral1126;
extern Il2CppCodeGenString* _stringLiteral1127;
extern Il2CppCodeGenString* _stringLiteral1128;
extern Il2CppCodeGenString* _stringLiteral747;
extern Il2CppCodeGenString* _stringLiteral1111;
extern "C" String_t* StackTraceUtility_PostprocessStacktrace_m7015 (Object_t * __this /* static, unused */, String_t* ___oldString, bool ___stripEngineInternalInformation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		CharU5BU5D_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(66);
		StringBuilder_t305_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(309);
		StackTraceUtility_t1284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		_stringLiteral1116 = il2cpp_codegen_string_literal_from_index(1116);
		_stringLiteral1117 = il2cpp_codegen_string_literal_from_index(1117);
		_stringLiteral1118 = il2cpp_codegen_string_literal_from_index(1118);
		_stringLiteral1119 = il2cpp_codegen_string_literal_from_index(1119);
		_stringLiteral1120 = il2cpp_codegen_string_literal_from_index(1120);
		_stringLiteral1121 = il2cpp_codegen_string_literal_from_index(1121);
		_stringLiteral1122 = il2cpp_codegen_string_literal_from_index(1122);
		_stringLiteral1123 = il2cpp_codegen_string_literal_from_index(1123);
		_stringLiteral1124 = il2cpp_codegen_string_literal_from_index(1124);
		_stringLiteral1125 = il2cpp_codegen_string_literal_from_index(1125);
		_stringLiteral1126 = il2cpp_codegen_string_literal_from_index(1126);
		_stringLiteral1127 = il2cpp_codegen_string_literal_from_index(1127);
		_stringLiteral1128 = il2cpp_codegen_string_literal_from_index(1128);
		_stringLiteral747 = il2cpp_codegen_string_literal_from_index(747);
		_stringLiteral1111 = il2cpp_codegen_string_literal_from_index(1111);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t260* V_0 = {0};
	StringBuilder_t305 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___oldString;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_000c:
	{
		String_t* L_2 = ___oldString;
		CharU5BU5D_t493* L_3 = ((CharU5BU5D_t493*)SZArrayNew(CharU5BU5D_t493_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)10);
		NullCheck(L_2);
		StringU5BU5D_t260* L_4 = String_Split_m2171(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___oldString;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m410(L_5, /*hidden argument*/NULL);
		StringBuilder_t305 * L_7 = (StringBuilder_t305 *)il2cpp_codegen_object_new (StringBuilder_t305_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m6520(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		StringU5BU5D_t260* L_8 = V_0;
		int32_t L_9 = V_2;
		StringU5BU5D_t260* L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((*(String_t**)(String_t**)SZArrayLdElema(L_10, L_12, sizeof(String_t*))));
		String_t* L_13 = String_Trim_m8134((*(String_t**)(String_t**)SZArrayLdElema(L_10, L_12, sizeof(String_t*))), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_13);
		*((String_t**)(String_t**)SZArrayLdElema(L_8, L_9, sizeof(String_t*))) = (String_t*)L_13;
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_15 = V_2;
		StringU5BU5D_t260* L_16 = V_0;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		StringU5BU5D_t260* L_17 = V_0;
		int32_t L_18 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		V_4 = (*(String_t**)(String_t**)SZArrayLdElema(L_17, L_19, sizeof(String_t*)));
		String_t* L_20 = V_4;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m410(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0070;
		}
	}
	{
		String_t* L_22 = V_4;
		NullCheck(L_22);
		uint16_t L_23 = String_get_Chars_m2170(L_22, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		String_t* L_24 = V_4;
		NullCheck(L_24);
		bool L_25 = String_StartsWith_m2414(L_24, _stringLiteral1116, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		bool L_26 = ___stripEngineInternalInformation;
		if (!L_26)
		{
			goto IL_00a7;
		}
	}
	{
		String_t* L_27 = V_4;
		NullCheck(L_27);
		bool L_28 = String_StartsWith_m2414(L_27, _stringLiteral1117, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		bool L_29 = ___stripEngineInternalInformation;
		if (!L_29)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_30 = V_3;
		StringU5BU5D_t260* L_31 = V_0;
		NullCheck(L_31);
		if ((((int32_t)L_30) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_31)->max_length))))-(int32_t)1)))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_32 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		bool L_33 = StackTraceUtility_IsSystemStacktraceType_m7012(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00fa;
		}
	}
	{
		StringU5BU5D_t260* L_34 = V_0;
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)((int32_t)L_35+(int32_t)1)));
		int32_t L_36 = ((int32_t)((int32_t)L_35+(int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		bool L_37 = StackTraceUtility_IsSystemStacktraceType_m7012(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_34, L_36, sizeof(String_t*))), /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		String_t* L_38 = V_4;
		NullCheck(L_38);
		int32_t L_39 = String_IndexOf_m2278(L_38, _stringLiteral1118, /*hidden argument*/NULL);
		V_5 = L_39;
		int32_t L_40 = V_5;
		if ((((int32_t)L_40) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_41 = V_4;
		int32_t L_42 = V_5;
		NullCheck(L_41);
		String_t* L_43 = String_Substring_m411(L_41, 0, L_42, /*hidden argument*/NULL);
		V_4 = L_43;
	}

IL_00fa:
	{
		String_t* L_44 = V_4;
		NullCheck(L_44);
		int32_t L_45 = String_IndexOf_m2278(L_44, _stringLiteral1119, /*hidden argument*/NULL);
		if ((((int32_t)L_45) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		String_t* L_46 = V_4;
		NullCheck(L_46);
		int32_t L_47 = String_IndexOf_m2278(L_46, _stringLiteral1120, /*hidden argument*/NULL);
		if ((((int32_t)L_47) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		String_t* L_48 = V_4;
		NullCheck(L_48);
		int32_t L_49 = String_IndexOf_m2278(L_48, _stringLiteral1121, /*hidden argument*/NULL);
		if ((((int32_t)L_49) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		bool L_50 = ___stripEngineInternalInformation;
		if (!L_50)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_51 = V_4;
		NullCheck(L_51);
		bool L_52 = String_StartsWith_m2414(L_51, _stringLiteral1122, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_53 = V_4;
		NullCheck(L_53);
		bool L_54 = String_EndsWith_m6512(L_53, _stringLiteral1123, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		String_t* L_55 = V_4;
		NullCheck(L_55);
		bool L_56 = String_StartsWith_m2414(L_55, _stringLiteral1124, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0188;
		}
	}
	{
		String_t* L_57 = V_4;
		NullCheck(L_57);
		String_t* L_58 = String_Remove_m4264(L_57, 0, 3, /*hidden argument*/NULL);
		V_4 = L_58;
	}

IL_0188:
	{
		String_t* L_59 = V_4;
		NullCheck(L_59);
		int32_t L_60 = String_IndexOf_m2278(L_59, _stringLiteral1125, /*hidden argument*/NULL);
		V_6 = L_60;
		V_7 = (-1);
		int32_t L_61 = V_6;
		if ((((int32_t)L_61) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		String_t* L_62 = V_4;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = String_IndexOf_m8135(L_62, _stringLiteral1123, L_63, /*hidden argument*/NULL);
		V_7 = L_64;
	}

IL_01b1:
	{
		int32_t L_65 = V_6;
		if ((((int32_t)L_65) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		int32_t L_66 = V_7;
		int32_t L_67 = V_6;
		if ((((int32_t)L_66) <= ((int32_t)L_67)))
		{
			goto IL_01d4;
		}
	}
	{
		String_t* L_68 = V_4;
		int32_t L_69 = V_6;
		int32_t L_70 = V_7;
		int32_t L_71 = V_6;
		NullCheck(L_68);
		String_t* L_72 = String_Remove_m4264(L_68, L_69, ((int32_t)((int32_t)((int32_t)((int32_t)L_70-(int32_t)L_71))+(int32_t)1)), /*hidden argument*/NULL);
		V_4 = L_72;
	}

IL_01d4:
	{
		String_t* L_73 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_73);
		String_t* L_75 = String_Replace_m2245(L_73, _stringLiteral1126, L_74, /*hidden argument*/NULL);
		V_4 = L_75;
		String_t* L_76 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		String_t* L_77 = ((StackTraceUtility_t1284_StaticFields*)StackTraceUtility_t1284_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		String_t* L_78 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_76);
		String_t* L_79 = String_Replace_m2245(L_76, L_77, L_78, /*hidden argument*/NULL);
		V_4 = L_79;
		String_t* L_80 = V_4;
		NullCheck(L_80);
		String_t* L_81 = String_Replace_m8136(L_80, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_4 = L_81;
		String_t* L_82 = V_4;
		NullCheck(L_82);
		int32_t L_83 = String_LastIndexOf_m8137(L_82, _stringLiteral1127, /*hidden argument*/NULL);
		V_8 = L_83;
		int32_t L_84 = V_8;
		if ((((int32_t)L_84) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		String_t* L_85 = V_4;
		int32_t L_86 = V_8;
		NullCheck(L_85);
		String_t* L_87 = String_Remove_m4264(L_85, L_86, 5, /*hidden argument*/NULL);
		V_4 = L_87;
		String_t* L_88 = V_4;
		int32_t L_89 = V_8;
		NullCheck(L_88);
		String_t* L_90 = String_Insert_m4266(L_88, L_89, _stringLiteral1128, /*hidden argument*/NULL);
		V_4 = L_90;
		String_t* L_91 = V_4;
		String_t* L_92 = V_4;
		NullCheck(L_92);
		int32_t L_93 = String_get_Length_m410(L_92, /*hidden argument*/NULL);
		NullCheck(L_91);
		String_t* L_94 = String_Insert_m4266(L_91, L_93, _stringLiteral747, /*hidden argument*/NULL);
		V_4 = L_94;
	}

IL_024e:
	{
		StringBuilder_t305 * L_95 = V_1;
		String_t* L_96 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = String_Concat_m409(NULL /*static, unused*/, L_96, _stringLiteral1111, /*hidden argument*/NULL);
		NullCheck(L_95);
		StringBuilder_Append_m2533(L_95, L_97, /*hidden argument*/NULL);
	}

IL_0261:
	{
		int32_t L_98 = V_3;
		V_3 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_0265:
	{
		int32_t L_99 = V_3;
		StringU5BU5D_t260* L_100 = V_0;
		NullCheck(L_100);
		if ((((int32_t)L_99) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_100)->max_length)))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		StringBuilder_t305 * L_101 = V_1;
		NullCheck(L_101);
		String_t* L_102 = StringBuilder_ToString_m2528(L_101, /*hidden argument*/NULL);
		return L_102;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern TypeInfo* StringBuilder_t305_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t1284_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral877;
extern Il2CppCodeGenString* _stringLiteral1129;
extern Il2CppCodeGenString* _stringLiteral1130;
extern Il2CppCodeGenString* _stringLiteral1131;
extern Il2CppCodeGenString* _stringLiteral747;
extern Il2CppCodeGenString* _stringLiteral1132;
extern Il2CppCodeGenString* _stringLiteral1133;
extern Il2CppCodeGenString* _stringLiteral1128;
extern Il2CppCodeGenString* _stringLiteral1111;
extern "C" String_t* StackTraceUtility_ExtractFormattedStackTrace_m7016 (Object_t * __this /* static, unused */, StackTrace_t1437 * ___stackTrace, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t305_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(309);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		StackTraceUtility_t1284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		_stringLiteral877 = il2cpp_codegen_string_literal_from_index(877);
		_stringLiteral1129 = il2cpp_codegen_string_literal_from_index(1129);
		_stringLiteral1130 = il2cpp_codegen_string_literal_from_index(1130);
		_stringLiteral1131 = il2cpp_codegen_string_literal_from_index(1131);
		_stringLiteral747 = il2cpp_codegen_string_literal_from_index(747);
		_stringLiteral1132 = il2cpp_codegen_string_literal_from_index(1132);
		_stringLiteral1133 = il2cpp_codegen_string_literal_from_index(1133);
		_stringLiteral1128 = il2cpp_codegen_string_literal_from_index(1128);
		_stringLiteral1111 = il2cpp_codegen_string_literal_from_index(1111);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t305 * V_0 = {0};
	int32_t V_1 = 0;
	StackFrame_t1456 * V_2 = {0};
	MethodBase_t1457 * V_3 = {0};
	Type_t * V_4 = {0};
	String_t* V_5 = {0};
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t1458* V_7 = {0};
	bool V_8 = false;
	String_t* V_9 = {0};
	int32_t V_10 = 0;
	{
		StringBuilder_t305 * L_0 = (StringBuilder_t305 *)il2cpp_codegen_object_new (StringBuilder_t305_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m6520(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_01c9;
	}

IL_0012:
	{
		StackTrace_t1437 * L_1 = ___stackTrace;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t1456 * L_3 = (StackFrame_t1456 *)VirtFuncInvoker1< StackFrame_t1456 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t1456 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t1457 * L_5 = (MethodBase_t1457 *)VirtFuncInvoker0< MethodBase_t1457 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t1457 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_01c5;
	}

IL_002c:
	{
		MethodBase_t1457 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_01c5;
	}

IL_0040:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m410(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0071;
		}
	}
	{
		StringBuilder_t305 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m2533(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t305 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m2533(L_17, _stringLiteral877, /*hidden argument*/NULL);
	}

IL_0071:
	{
		StringBuilder_t305 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m2533(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t305 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m2533(L_21, _stringLiteral1129, /*hidden argument*/NULL);
		StringBuilder_t305 * L_22 = V_0;
		MethodBase_t1457 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m2533(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t305 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m2533(L_25, _stringLiteral1130, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t1457 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t1458* L_27 = (ParameterInfoU5BU5D_t1458*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1458* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = 1;
		goto IL_00ee;
	}

IL_00b7:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t305 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m2533(L_29, _stringLiteral1131, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = 0;
	}

IL_00d2:
	{
		StringBuilder_t305 * L_30 = V_0;
		ParameterInfoU5BU5D_t1458* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		NullCheck((*(ParameterInfo_t1459 **)(ParameterInfo_t1459 **)SZArrayLdElema(L_31, L_33, sizeof(ParameterInfo_t1459 *))));
		Type_t * L_34 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, (*(ParameterInfo_t1459 **)(ParameterInfo_t1459 **)SZArrayLdElema(L_31, L_33, sizeof(ParameterInfo_t1459 *))));
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_34);
		NullCheck(L_30);
		StringBuilder_Append_m2533(L_30, L_35, /*hidden argument*/NULL);
		int32_t L_36 = V_6;
		V_6 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_37 = V_6;
		ParameterInfoU5BU5D_t1458* L_38 = V_7;
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_38)->max_length)))))))
		{
			goto IL_00b7;
		}
	}
	{
		StringBuilder_t305 * L_39 = V_0;
		NullCheck(L_39);
		StringBuilder_Append_m2533(L_39, _stringLiteral747, /*hidden argument*/NULL);
		StackFrame_t1456 * L_40 = V_2;
		NullCheck(L_40);
		String_t* L_41 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_40);
		V_9 = L_41;
		String_t* L_42 = V_9;
		if (!L_42)
		{
			goto IL_01b9;
		}
	}
	{
		Type_t * L_43 = V_4;
		NullCheck(L_43);
		String_t* L_44 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_43);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_45 = String_op_Equality_m385(NULL /*static, unused*/, L_44, _stringLiteral1132, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0140;
		}
	}
	{
		Type_t * L_46 = V_4;
		NullCheck(L_46);
		String_t* L_47 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_48 = String_op_Equality_m385(NULL /*static, unused*/, L_47, _stringLiteral1133, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01b9;
		}
	}

IL_0140:
	{
		StringBuilder_t305 * L_49 = V_0;
		NullCheck(L_49);
		StringBuilder_Append_m2533(L_49, _stringLiteral1128, /*hidden argument*/NULL);
		String_t* L_50 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		String_t* L_51 = ((StackTraceUtility_t1284_StaticFields*)StackTraceUtility_t1284_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_50);
		bool L_52 = String_StartsWith_m2414(L_50, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0182;
		}
	}
	{
		String_t* L_53 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1284_il2cpp_TypeInfo_var);
		String_t* L_54 = ((StackTraceUtility_t1284_StaticFields*)StackTraceUtility_t1284_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_54);
		int32_t L_55 = String_get_Length_m410(L_54, /*hidden argument*/NULL);
		String_t* L_56 = V_9;
		NullCheck(L_56);
		int32_t L_57 = String_get_Length_m410(L_56, /*hidden argument*/NULL);
		String_t* L_58 = ((StackTraceUtility_t1284_StaticFields*)StackTraceUtility_t1284_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_58);
		int32_t L_59 = String_get_Length_m410(L_58, /*hidden argument*/NULL);
		NullCheck(L_53);
		String_t* L_60 = String_Substring_m411(L_53, L_55, ((int32_t)((int32_t)L_57-(int32_t)L_59)), /*hidden argument*/NULL);
		V_9 = L_60;
	}

IL_0182:
	{
		StringBuilder_t305 * L_61 = V_0;
		String_t* L_62 = V_9;
		NullCheck(L_61);
		StringBuilder_Append_m2533(L_61, L_62, /*hidden argument*/NULL);
		StringBuilder_t305 * L_63 = V_0;
		NullCheck(L_63);
		StringBuilder_Append_m2533(L_63, _stringLiteral1129, /*hidden argument*/NULL);
		StringBuilder_t305 * L_64 = V_0;
		StackFrame_t1456 * L_65 = V_2;
		NullCheck(L_65);
		int32_t L_66 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_65);
		V_10 = L_66;
		String_t* L_67 = Int32_ToString_m2156((&V_10), /*hidden argument*/NULL);
		NullCheck(L_64);
		StringBuilder_Append_m2533(L_64, L_67, /*hidden argument*/NULL);
		StringBuilder_t305 * L_68 = V_0;
		NullCheck(L_68);
		StringBuilder_Append_m2533(L_68, _stringLiteral747, /*hidden argument*/NULL);
	}

IL_01b9:
	{
		StringBuilder_t305 * L_69 = V_0;
		NullCheck(L_69);
		StringBuilder_Append_m2533(L_69, _stringLiteral1111, /*hidden argument*/NULL);
	}

IL_01c5:
	{
		int32_t L_70 = V_1;
		V_1 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_01c9:
	{
		int32_t L_71 = V_1;
		StackTrace_t1437 * L_72 = ___stackTrace;
		NullCheck(L_72);
		int32_t L_73 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_72);
		if ((((int32_t)L_71) < ((int32_t)L_73)))
		{
			goto IL_0012;
		}
	}
	{
		StringBuilder_t305 * L_74 = V_0;
		NullCheck(L_74);
		String_t* L_75 = StringBuilder_ToString_m2528(L_74, /*hidden argument*/NULL);
		return L_75;
	}
}
// System.Void UnityEngine.UnityException::.ctor()
extern Il2CppCodeGenString* _stringLiteral1134;
extern "C" void UnityException__ctor_m7017 (UnityException_t853 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1134 = il2cpp_codegen_string_literal_from_index(1134);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m2366(__this, _stringLiteral1134, /*hidden argument*/NULL);
		Exception_set_HResult_m8138(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C" void UnityException__ctor_m7018 (UnityException_t853 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m2366(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m8138(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern "C" void UnityException__ctor_m7019 (UnityException_t853 * __this, String_t* ___message, Exception_t40 * ___innerException, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t40 * L_1 = ___innerException;
		Exception__ctor_m8139(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m8138(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnityException__ctor_m7020 (UnityException_t853 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1438 * L_0 = ___info;
		StreamingContext_t1439  L_1 = ___context;
		Exception__ctor_m8140(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::.ctor()
extern TypeInfo* GUIContent_t549_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern "C" void TextEditor__ctor_m4238 (TextEditor_t855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t549 * L_0 = (GUIContent_t549 *)il2cpp_codegen_object_new (GUIContent_t549_il2cpp_TypeInfo_var);
		GUIContent__ctor_m7680(L_0, /*hidden argument*/NULL);
		__this->___content_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_1 = GUIStyle_get_none_m7748(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___style_5 = L_1;
		Vector2_t29  L_2 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scrollOffset_11 = L_2;
		__this->___m_iAltCursorPos_19 = (-1);
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern "C" void TextEditor_ClearCursorPos_m7021 (TextEditor_t855 * __this, const MethodInfo* method)
{
	{
		__this->___hasHorizontalCursorPos_8 = 0;
		__this->___m_iAltCursorPos_19 = (-1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnFocus()
extern "C" void TextEditor_OnFocus_m4241 (TextEditor_t855 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___multiline_7);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_1 = 0;
		V_0 = L_1;
		__this->___selectPos_2 = L_1;
		int32_t L_2 = V_0;
		__this->___pos_1 = L_2;
		goto IL_0026;
	}

IL_0020:
	{
		TextEditor_SelectAll_m7023(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		__this->___m_HasFocus_10 = 1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnLostFocus()
extern "C" void TextEditor_OnLostFocus_m7022 (TextEditor_t855 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasFocus_10 = 0;
		Vector2_t29  L_0 = Vector2_get_zero_m4028(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scrollOffset_11 = L_0;
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectAll()
extern "C" void TextEditor_SelectAll_m7023 (TextEditor_t855 * __this, const MethodInfo* method)
{
	{
		__this->___pos_1 = 0;
		GUIContent_t549 * L_0 = (__this->___content_4);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m4240(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m410(L_1, /*hidden argument*/NULL);
		__this->___selectPos_2 = L_2;
		TextEditor_ClearCursorPos_m7021(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_DeleteSelection_m7024 (TextEditor_t855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIContent_t549 * L_0 = (__this->___content_4);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m4240(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m410(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = (__this->___pos_1);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_5 = V_0;
		__this->___pos_1 = L_5;
	}

IL_0024:
	{
		int32_t L_6 = (__this->___selectPos_2);
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) <= ((int32_t)L_7)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_8 = V_0;
		__this->___selectPos_2 = L_8;
	}

IL_0037:
	{
		int32_t L_9 = (__this->___pos_1);
		int32_t L_10 = (__this->___selectPos_2);
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_004a;
		}
	}
	{
		return 0;
	}

IL_004a:
	{
		int32_t L_11 = (__this->___pos_1);
		int32_t L_12 = (__this->___selectPos_2);
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_00c0;
		}
	}
	{
		GUIContent_t549 * L_13 = (__this->___content_4);
		GUIContent_t549 * L_14 = (__this->___content_4);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m4240(L_14, /*hidden argument*/NULL);
		int32_t L_16 = (__this->___pos_1);
		NullCheck(L_15);
		String_t* L_17 = String_Substring_m411(L_15, 0, L_16, /*hidden argument*/NULL);
		GUIContent_t549 * L_18 = (__this->___content_4);
		NullCheck(L_18);
		String_t* L_19 = GUIContent_get_text_m4240(L_18, /*hidden argument*/NULL);
		int32_t L_20 = (__this->___selectPos_2);
		GUIContent_t549 * L_21 = (__this->___content_4);
		NullCheck(L_21);
		String_t* L_22 = GUIContent_get_text_m4240(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m410(L_22, /*hidden argument*/NULL);
		int32_t L_24 = (__this->___selectPos_2);
		NullCheck(L_19);
		String_t* L_25 = String_Substring_m411(L_19, L_20, ((int32_t)((int32_t)L_23-(int32_t)L_24)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m409(NULL /*static, unused*/, L_17, L_25, /*hidden argument*/NULL);
		NullCheck(L_13);
		GUIContent_set_text_m7683(L_13, L_26, /*hidden argument*/NULL);
		int32_t L_27 = (__this->___pos_1);
		__this->___selectPos_2 = L_27;
		goto IL_0120;
	}

IL_00c0:
	{
		GUIContent_t549 * L_28 = (__this->___content_4);
		GUIContent_t549 * L_29 = (__this->___content_4);
		NullCheck(L_29);
		String_t* L_30 = GUIContent_get_text_m4240(L_29, /*hidden argument*/NULL);
		int32_t L_31 = (__this->___selectPos_2);
		NullCheck(L_30);
		String_t* L_32 = String_Substring_m411(L_30, 0, L_31, /*hidden argument*/NULL);
		GUIContent_t549 * L_33 = (__this->___content_4);
		NullCheck(L_33);
		String_t* L_34 = GUIContent_get_text_m4240(L_33, /*hidden argument*/NULL);
		int32_t L_35 = (__this->___pos_1);
		GUIContent_t549 * L_36 = (__this->___content_4);
		NullCheck(L_36);
		String_t* L_37 = GUIContent_get_text_m4240(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m410(L_37, /*hidden argument*/NULL);
		int32_t L_39 = (__this->___pos_1);
		NullCheck(L_34);
		String_t* L_40 = String_Substring_m411(L_34, L_35, ((int32_t)((int32_t)L_38-(int32_t)L_39)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m409(NULL /*static, unused*/, L_32, L_40, /*hidden argument*/NULL);
		NullCheck(L_28);
		GUIContent_set_text_m7683(L_28, L_41, /*hidden argument*/NULL);
		int32_t L_42 = (__this->___selectPos_2);
		__this->___pos_1 = L_42;
	}

IL_0120:
	{
		TextEditor_ClearCursorPos_m7021(__this, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern "C" void TextEditor_ReplaceSelection_m7025 (TextEditor_t855 * __this, String_t* ___replace, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_DeleteSelection_m7024(__this, /*hidden argument*/NULL);
		GUIContent_t549 * L_0 = (__this->___content_4);
		GUIContent_t549 * L_1 = (__this->___content_4);
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m4240(L_1, /*hidden argument*/NULL);
		int32_t L_3 = (__this->___pos_1);
		String_t* L_4 = ___replace;
		NullCheck(L_2);
		String_t* L_5 = String_Insert_m4266(L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIContent_set_text_m7683(L_0, L_5, /*hidden argument*/NULL);
		int32_t L_6 = (__this->___pos_1);
		String_t* L_7 = ___replace;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m410(L_7, /*hidden argument*/NULL);
		int32_t L_9 = ((int32_t)((int32_t)L_6+(int32_t)L_8));
		V_0 = L_9;
		__this->___pos_1 = L_9;
		int32_t L_10 = V_0;
		__this->___selectPos_2 = L_10;
		TextEditor_ClearCursorPos_m7021(__this, /*hidden argument*/NULL);
		TextEditor_UpdateScrollOffset_m7027(__this, /*hidden argument*/NULL);
		__this->___m_TextHeightPotentiallyChanged_12 = 1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffsetIfNeeded()
extern "C" void TextEditor_UpdateScrollOffsetIfNeeded_m7026 (TextEditor_t855 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_TextHeightPotentiallyChanged_12);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		TextEditor_UpdateScrollOffset_m7027(__this, /*hidden argument*/NULL);
		__this->___m_TextHeightPotentiallyChanged_12 = 0;
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern "C" void TextEditor_UpdateScrollOffset_m7027 (TextEditor_t855 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Rect_t30  V_1 = {0};
	Vector2_t29  V_2 = {0};
	Vector2_t29  V_3 = {0};
	Vector2_t29 * G_B17_0 = {0};
	Vector2_t29 * G_B16_0 = {0};
	float G_B18_0 = 0.0f;
	Vector2_t29 * G_B18_1 = {0};
	{
		int32_t L_0 = (__this->___pos_1);
		V_0 = L_0;
		GUIStyle_t184 * L_1 = (__this->___style_5);
		Rect_t30 * L_2 = &(__this->___position_6);
		float L_3 = Rect_get_width_m370(L_2, /*hidden argument*/NULL);
		Rect_t30 * L_4 = &(__this->___position_6);
		float L_5 = Rect_get_height_m372(L_4, /*hidden argument*/NULL);
		Rect_t30  L_6 = {0};
		Rect__ctor_m387(&L_6, (0.0f), (0.0f), L_3, L_5, /*hidden argument*/NULL);
		GUIContent_t549 * L_7 = (__this->___content_4);
		int32_t L_8 = V_0;
		NullCheck(L_1);
		Vector2_t29  L_9 = GUIStyle_GetCursorPixelPosition_m7749(L_1, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->___graphicalCursorPos_13 = L_9;
		GUIStyle_t184 * L_10 = (__this->___style_5);
		NullCheck(L_10);
		RectOffset_t780 * L_11 = GUIStyle_get_padding_m7728(L_10, /*hidden argument*/NULL);
		Rect_t30  L_12 = (__this->___position_6);
		NullCheck(L_11);
		Rect_t30  L_13 = RectOffset_Remove_m7707(L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		GUIStyle_t184 * L_14 = (__this->___style_5);
		GUIContent_t549 * L_15 = (__this->___content_4);
		NullCheck(L_14);
		Vector2_t29  L_16 = GUIStyle_CalcSize_m7752(L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = ((&V_3)->___x_1);
		GUIStyle_t184 * L_18 = (__this->___style_5);
		GUIContent_t549 * L_19 = (__this->___content_4);
		Rect_t30 * L_20 = &(__this->___position_6);
		float L_21 = Rect_get_width_m370(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_22 = GUIStyle_CalcHeight_m7754(L_18, L_19, L_21, /*hidden argument*/NULL);
		Vector2__ctor_m356((&V_2), L_17, L_22, /*hidden argument*/NULL);
		float L_23 = ((&V_2)->___x_1);
		Rect_t30 * L_24 = &(__this->___position_6);
		float L_25 = Rect_get_width_m370(L_24, /*hidden argument*/NULL);
		if ((!(((float)L_23) < ((float)L_25))))
		{
			goto IL_00c3;
		}
	}
	{
		Vector2_t29 * L_26 = &(__this->___scrollOffset_11);
		L_26->___x_1 = (0.0f);
		goto IL_015f;
	}

IL_00c3:
	{
		Vector2_t29 * L_27 = &(__this->___graphicalCursorPos_13);
		float L_28 = (L_27->___x_1);
		Vector2_t29 * L_29 = &(__this->___scrollOffset_11);
		float L_30 = (L_29->___x_1);
		float L_31 = Rect_get_width_m370((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_28+(float)(1.0f)))) > ((float)((float)((float)L_30+(float)L_31))))))
		{
			goto IL_010a;
		}
	}
	{
		Vector2_t29 * L_32 = &(__this->___scrollOffset_11);
		Vector2_t29 * L_33 = &(__this->___graphicalCursorPos_13);
		float L_34 = (L_33->___x_1);
		float L_35 = Rect_get_width_m370((&V_1), /*hidden argument*/NULL);
		L_32->___x_1 = ((float)((float)L_34-(float)L_35));
	}

IL_010a:
	{
		Vector2_t29 * L_36 = &(__this->___graphicalCursorPos_13);
		float L_37 = (L_36->___x_1);
		Vector2_t29 * L_38 = &(__this->___scrollOffset_11);
		float L_39 = (L_38->___x_1);
		GUIStyle_t184 * L_40 = (__this->___style_5);
		NullCheck(L_40);
		RectOffset_t780 * L_41 = GUIStyle_get_padding_m7728(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		int32_t L_42 = RectOffset_get_left_m4410(L_41, /*hidden argument*/NULL);
		if ((!(((float)L_37) < ((float)((float)((float)L_39+(float)(((float)((float)L_42)))))))))
		{
			goto IL_015f;
		}
	}
	{
		Vector2_t29 * L_43 = &(__this->___scrollOffset_11);
		Vector2_t29 * L_44 = &(__this->___graphicalCursorPos_13);
		float L_45 = (L_44->___x_1);
		GUIStyle_t184 * L_46 = (__this->___style_5);
		NullCheck(L_46);
		RectOffset_t780 * L_47 = GUIStyle_get_padding_m7728(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = RectOffset_get_left_m4410(L_47, /*hidden argument*/NULL);
		L_43->___x_1 = ((float)((float)L_45-(float)(((float)((float)L_48)))));
	}

IL_015f:
	{
		float L_49 = ((&V_2)->___y_2);
		float L_50 = Rect_get_height_m372((&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_49) < ((float)L_50))))
		{
			goto IL_0187;
		}
	}
	{
		Vector2_t29 * L_51 = &(__this->___scrollOffset_11);
		L_51->___y_2 = (0.0f);
		goto IL_0259;
	}

IL_0187:
	{
		Vector2_t29 * L_52 = &(__this->___graphicalCursorPos_13);
		float L_53 = (L_52->___y_2);
		GUIStyle_t184 * L_54 = (__this->___style_5);
		NullCheck(L_54);
		float L_55 = GUIStyle_get_lineHeight_m7742(L_54, /*hidden argument*/NULL);
		Vector2_t29 * L_56 = &(__this->___scrollOffset_11);
		float L_57 = (L_56->___y_2);
		float L_58 = Rect_get_height_m372((&V_1), /*hidden argument*/NULL);
		GUIStyle_t184 * L_59 = (__this->___style_5);
		NullCheck(L_59);
		RectOffset_t780 * L_60 = GUIStyle_get_padding_m7728(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		int32_t L_61 = RectOffset_get_top_m4411(L_60, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_53+(float)L_55))) > ((float)((float)((float)((float)((float)L_57+(float)L_58))+(float)(((float)((float)L_61)))))))))
		{
			goto IL_0204;
		}
	}
	{
		Vector2_t29 * L_62 = &(__this->___scrollOffset_11);
		Vector2_t29 * L_63 = &(__this->___graphicalCursorPos_13);
		float L_64 = (L_63->___y_2);
		float L_65 = Rect_get_height_m372((&V_1), /*hidden argument*/NULL);
		GUIStyle_t184 * L_66 = (__this->___style_5);
		NullCheck(L_66);
		RectOffset_t780 * L_67 = GUIStyle_get_padding_m7728(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		int32_t L_68 = RectOffset_get_top_m4411(L_67, /*hidden argument*/NULL);
		GUIStyle_t184 * L_69 = (__this->___style_5);
		NullCheck(L_69);
		float L_70 = GUIStyle_get_lineHeight_m7742(L_69, /*hidden argument*/NULL);
		L_62->___y_2 = ((float)((float)((float)((float)((float)((float)L_64-(float)L_65))-(float)(((float)((float)L_68)))))+(float)L_70));
	}

IL_0204:
	{
		Vector2_t29 * L_71 = &(__this->___graphicalCursorPos_13);
		float L_72 = (L_71->___y_2);
		Vector2_t29 * L_73 = &(__this->___scrollOffset_11);
		float L_74 = (L_73->___y_2);
		GUIStyle_t184 * L_75 = (__this->___style_5);
		NullCheck(L_75);
		RectOffset_t780 * L_76 = GUIStyle_get_padding_m7728(L_75, /*hidden argument*/NULL);
		NullCheck(L_76);
		int32_t L_77 = RectOffset_get_top_m4411(L_76, /*hidden argument*/NULL);
		if ((!(((float)L_72) < ((float)((float)((float)L_74+(float)(((float)((float)L_77)))))))))
		{
			goto IL_0259;
		}
	}
	{
		Vector2_t29 * L_78 = &(__this->___scrollOffset_11);
		Vector2_t29 * L_79 = &(__this->___graphicalCursorPos_13);
		float L_80 = (L_79->___y_2);
		GUIStyle_t184 * L_81 = (__this->___style_5);
		NullCheck(L_81);
		RectOffset_t780 * L_82 = GUIStyle_get_padding_m7728(L_81, /*hidden argument*/NULL);
		NullCheck(L_82);
		int32_t L_83 = RectOffset_get_top_m4411(L_82, /*hidden argument*/NULL);
		L_78->___y_2 = ((float)((float)L_80-(float)(((float)((float)L_83)))));
	}

IL_0259:
	{
		Vector2_t29 * L_84 = &(__this->___scrollOffset_11);
		float L_85 = (L_84->___y_2);
		if ((!(((float)L_85) > ((float)(0.0f)))))
		{
			goto IL_02cb;
		}
	}
	{
		float L_86 = ((&V_2)->___y_2);
		Vector2_t29 * L_87 = &(__this->___scrollOffset_11);
		float L_88 = (L_87->___y_2);
		float L_89 = Rect_get_height_m372((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_86-(float)L_88))) < ((float)L_89))))
		{
			goto IL_02cb;
		}
	}
	{
		Vector2_t29 * L_90 = &(__this->___scrollOffset_11);
		float L_91 = ((&V_2)->___y_2);
		float L_92 = Rect_get_height_m372((&V_1), /*hidden argument*/NULL);
		GUIStyle_t184 * L_93 = (__this->___style_5);
		NullCheck(L_93);
		RectOffset_t780 * L_94 = GUIStyle_get_padding_m7728(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		int32_t L_95 = RectOffset_get_top_m4411(L_94, /*hidden argument*/NULL);
		GUIStyle_t184 * L_96 = (__this->___style_5);
		NullCheck(L_96);
		RectOffset_t780 * L_97 = GUIStyle_get_padding_m7728(L_96, /*hidden argument*/NULL);
		NullCheck(L_97);
		int32_t L_98 = RectOffset_get_bottom_m7705(L_97, /*hidden argument*/NULL);
		L_90->___y_2 = ((float)((float)((float)((float)((float)((float)L_91-(float)L_92))-(float)(((float)((float)L_95)))))-(float)(((float)((float)L_98)))));
	}

IL_02cb:
	{
		Vector2_t29 * L_99 = &(__this->___scrollOffset_11);
		Vector2_t29 * L_100 = &(__this->___scrollOffset_11);
		float L_101 = (L_100->___y_2);
		G_B16_0 = L_99;
		if ((!(((float)L_101) < ((float)(0.0f)))))
		{
			G_B17_0 = L_99;
			goto IL_02f0;
		}
	}
	{
		G_B18_0 = (0.0f);
		G_B18_1 = G_B16_0;
		goto IL_02fb;
	}

IL_02f0:
	{
		Vector2_t29 * L_102 = &(__this->___scrollOffset_11);
		float L_103 = (L_102->___y_2);
		G_B18_0 = L_103;
		G_B18_1 = G_B17_0;
	}

IL_02fb:
	{
		G_B18_1->___y_2 = G_B18_0;
		return;
	}
}
// System.Void UnityEngine.TextEditor::SaveBackup()
extern "C" void TextEditor_SaveBackup_m7028 (TextEditor_t855 * __this, const MethodInfo* method)
{
	{
		GUIContent_t549 * L_0 = (__this->___content_4);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m4240(L_0, /*hidden argument*/NULL);
		__this->___oldText_20 = L_1;
		int32_t L_2 = (__this->___pos_1);
		__this->___oldPos_21 = L_2;
		int32_t L_3 = (__this->___selectPos_2);
		__this->___oldSelectPos_22 = L_3;
		return;
	}
}
// System.Void UnityEngine.TextEditor::Copy()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern "C" void TextEditor_Copy_m4242 (TextEditor_t855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		int32_t L_0 = (__this->___selectPos_2);
		int32_t L_1 = (__this->___pos_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = (__this->___isPasswordField_9);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		int32_t L_3 = (__this->___pos_1);
		int32_t L_4 = (__this->___selectPos_2);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0058;
		}
	}
	{
		GUIContent_t549 * L_5 = (__this->___content_4);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m4240(L_5, /*hidden argument*/NULL);
		int32_t L_7 = (__this->___pos_1);
		int32_t L_8 = (__this->___selectPos_2);
		int32_t L_9 = (__this->___pos_1);
		NullCheck(L_6);
		String_t* L_10 = String_Substring_m411(L_6, L_7, ((int32_t)((int32_t)L_8-(int32_t)L_9)), /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_007c;
	}

IL_0058:
	{
		GUIContent_t549 * L_11 = (__this->___content_4);
		NullCheck(L_11);
		String_t* L_12 = GUIContent_get_text_m4240(L_11, /*hidden argument*/NULL);
		int32_t L_13 = (__this->___selectPos_2);
		int32_t L_14 = (__this->___pos_1);
		int32_t L_15 = (__this->___selectPos_2);
		NullCheck(L_12);
		String_t* L_16 = String_Substring_m411(L_12, L_13, ((int32_t)((int32_t)L_14-(int32_t)L_15)), /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_007c:
	{
		String_t* L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		GUIUtility_set_systemCopyBuffer_m7611(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::Paste()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_Paste_m4239 (TextEditor_t855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m7610(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_3 = String_op_Inequality_m433(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_4 = V_0;
		TextEditor_ReplaceSelection_m7025(__this, L_4, /*hidden argument*/NULL);
		return 1;
	}

IL_001f:
	{
		return 0;
	}
}
// System.Void UnityEngine.TextEditor::ClampPos()
extern TypeInfo* GUIUtility_t1392_il2cpp_TypeInfo_var;
extern "C" void TextEditor_ClampPos_m7029 (TextEditor_t855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t1392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(921);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasFocus_10);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = (__this->___controlID_3);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIUtility_get_keyboardControl_m7608(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		TextEditor_OnLostFocus_m7022(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		bool L_3 = (__this->___m_HasFocus_10);
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_4 = (__this->___controlID_3);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1392_il2cpp_TypeInfo_var);
		int32_t L_5 = GUIUtility_get_keyboardControl_m7608(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0042;
		}
	}
	{
		TextEditor_OnFocus_m4241(__this, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_6 = (__this->___pos_1);
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		__this->___pos_1 = 0;
		goto IL_008b;
	}

IL_005a:
	{
		int32_t L_7 = (__this->___pos_1);
		GUIContent_t549 * L_8 = (__this->___content_4);
		NullCheck(L_8);
		String_t* L_9 = GUIContent_get_text_m4240(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m410(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)L_10)))
		{
			goto IL_008b;
		}
	}
	{
		GUIContent_t549 * L_11 = (__this->___content_4);
		NullCheck(L_11);
		String_t* L_12 = GUIContent_get_text_m4240(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m410(L_12, /*hidden argument*/NULL);
		__this->___pos_1 = L_13;
	}

IL_008b:
	{
		int32_t L_14 = (__this->___selectPos_2);
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_00a3;
		}
	}
	{
		__this->___selectPos_2 = 0;
		goto IL_00d4;
	}

IL_00a3:
	{
		int32_t L_15 = (__this->___selectPos_2);
		GUIContent_t549 * L_16 = (__this->___content_4);
		NullCheck(L_16);
		String_t* L_17 = GUIContent_get_text_m4240(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = String_get_Length_m410(L_17, /*hidden argument*/NULL);
		if ((((int32_t)L_15) <= ((int32_t)L_18)))
		{
			goto IL_00d4;
		}
	}
	{
		GUIContent_t549 * L_19 = (__this->___content_4);
		NullCheck(L_19);
		String_t* L_20 = GUIContent_get_text_m4240(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m410(L_20, /*hidden argument*/NULL);
		__this->___selectPos_2 = L_21;
	}

IL_00d4:
	{
		int32_t L_22 = (__this->___m_iAltCursorPos_19);
		GUIContent_t549 * L_23 = (__this->___content_4);
		NullCheck(L_23);
		String_t* L_24 = GUIContent_get_text_m4240(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = String_get_Length_m410(L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_22) <= ((int32_t)L_25)))
		{
			goto IL_0105;
		}
	}
	{
		GUIContent_t549 * L_26 = (__this->___content_4);
		NullCheck(L_26);
		String_t* L_27 = GUIContent_get_text_m4240(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m410(L_27, /*hidden argument*/NULL);
		__this->___m_iAltCursorPos_19 = L_28;
	}

IL_0105:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern TypeInfo* Color32_t829_il2cpp_TypeInfo_var;
extern "C" bool TextGenerationSettings_CompareColors_m7030 (TextGenerationSettings_t824 * __this, Color_t5  ___left, Color_t5  ___right, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color32_t829_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(699);
		s_Il2CppMethodIntialized = true;
	}
	Color32_t829  V_0 = {0};
	Color32_t829  V_1 = {0};
	{
		Color_t5  L_0 = ___left;
		Color32_t829  L_1 = Color32_op_Implicit_m4144(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Color_t5  L_2 = ___right;
		Color32_t829  L_3 = Color32_op_Implicit_m4144(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Color32_t829  L_4 = V_0;
		Color32_t829  L_5 = L_4;
		Object_t * L_6 = Box(Color32_t829_il2cpp_TypeInfo_var, &L_5);
		Color32_t829  L_7 = V_1;
		Color32_t829  L_8 = L_7;
		Object_t * L_9 = Box(Color32_t829_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, L_6, L_9);
		return L_10;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" bool TextGenerationSettings_CompareVector2_m7031 (TextGenerationSettings_t824 * __this, Vector2_t29  ___left, Vector2_t29  ___right, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		float L_0 = ((&___left)->___x_1);
		float L_1 = ((&___right)->___x_1);
		bool L_2 = Mathf_Approximately_m4062(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = ((&___left)->___y_2);
		float L_4 = ((&___right)->___y_2);
		bool L_5 = Mathf_Approximately_m4062(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern "C" bool TextGenerationSettings_Equals_m7032 (TextGenerationSettings_t824 * __this, TextGenerationSettings_t824  ___other, const MethodInfo* method)
{
	int32_t G_B20_0 = 0;
	{
		Color_t5  L_0 = (__this->___color_1);
		Color_t5  L_1 = ((&___other)->___color_1);
		bool L_2 = TextGenerationSettings_CompareColors_m7030(__this, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_3 = (__this->___fontSize_2);
		int32_t L_4 = ((&___other)->___fontSize_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0174;
		}
	}
	{
		float L_5 = (__this->___scaleFactor_5);
		float L_6 = ((&___other)->___scaleFactor_5);
		bool L_7 = Mathf_Approximately_m4062(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_8 = (__this->___resizeTextMinSize_9);
		int32_t L_9 = ((&___other)->___resizeTextMinSize_9);
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_10 = (__this->___resizeTextMaxSize_10);
		int32_t L_11 = ((&___other)->___resizeTextMaxSize_10);
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0174;
		}
	}
	{
		float L_12 = (__this->___lineSpacing_3);
		float L_13 = ((&___other)->___lineSpacing_3);
		bool L_14 = Mathf_Approximately_m4062(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_15 = (__this->___fontStyle_6);
		int32_t L_16 = ((&___other)->___fontStyle_6);
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_17 = (__this->___richText_4);
		bool L_18 = ((&___other)->___richText_4);
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_19 = (__this->___textAnchor_7);
		int32_t L_20 = ((&___other)->___textAnchor_7);
		if ((!(((uint32_t)L_19) == ((uint32_t)L_20))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_21 = (__this->___resizeTextForBestFit_8);
		bool L_22 = ((&___other)->___resizeTextForBestFit_8);
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_23 = (__this->___resizeTextMinSize_9);
		int32_t L_24 = ((&___other)->___resizeTextMinSize_9);
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_25 = (__this->___resizeTextMaxSize_10);
		int32_t L_26 = ((&___other)->___resizeTextMaxSize_10);
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_27 = (__this->___resizeTextForBestFit_8);
		bool L_28 = ((&___other)->___resizeTextForBestFit_8);
		if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_29 = (__this->___updateBounds_11);
		bool L_30 = ((&___other)->___updateBounds_11);
		if ((!(((uint32_t)L_29) == ((uint32_t)L_30))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_31 = (__this->___horizontalOverflow_13);
		int32_t L_32 = ((&___other)->___horizontalOverflow_13);
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_33 = (__this->___verticalOverflow_12);
		int32_t L_34 = ((&___other)->___verticalOverflow_12);
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_0174;
		}
	}
	{
		Vector2_t29  L_35 = (__this->___generationExtents_14);
		Vector2_t29  L_36 = ((&___other)->___generationExtents_14);
		bool L_37 = TextGenerationSettings_CompareVector2_m7031(__this, L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0174;
		}
	}
	{
		Vector2_t29  L_38 = (__this->___pivot_15);
		Vector2_t29  L_39 = ((&___other)->___pivot_15);
		bool L_40 = TextGenerationSettings_CompareVector2_m7031(__this, L_38, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0174;
		}
	}
	{
		Font_t684 * L_41 = (__this->___font_0);
		Font_t684 * L_42 = ((&___other)->___font_0);
		bool L_43 = Object_op_Equality_m427(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		G_B20_0 = ((int32_t)(L_43));
		goto IL_0175;
	}

IL_0174:
	{
		G_B20_0 = 0;
	}

IL_0175:
	{
		return G_B20_0;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor()
extern "C" void TextGenerator__ctor_m4220 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	{
		TextGenerator__ctor_m4365(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern TypeInfo* List_1_t724_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1288_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1289_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8141_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m8142_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m8143_MethodInfo_var;
extern "C" void TextGenerator__ctor_m4365 (TextGenerator_t723 * __this, int32_t ___initialCapacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(517);
		List_1_t1288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(922);
		List_1_t1289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(923);
		List_1__ctor_m8141_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484698);
		List_1__ctor_m8142_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484699);
		List_1__ctor_m8143_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484700);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity;
		List_1_t724 * L_1 = (List_1_t724 *)il2cpp_codegen_object_new (List_1_t724_il2cpp_TypeInfo_var);
		List_1__ctor_m8141(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m8141_MethodInfo_var);
		__this->___m_Verts_5 = L_1;
		int32_t L_2 = ___initialCapacity;
		List_1_t1288 * L_3 = (List_1_t1288 *)il2cpp_codegen_object_new (List_1_t1288_il2cpp_TypeInfo_var);
		List_1__ctor_m8142(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m8142_MethodInfo_var);
		__this->___m_Characters_6 = L_3;
		List_1_t1289 * L_4 = (List_1_t1289 *)il2cpp_codegen_object_new (List_1_t1289_il2cpp_TypeInfo_var);
		List_1__ctor_m8143(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m8143_MethodInfo_var);
		__this->___m_Lines_7 = L_4;
		TextGenerator_Init_m7040(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C" void TextGenerator_System_IDisposable_Dispose_m7033 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m7041(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern "C" void TextGenerator_Finalize_m7034 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern Il2CppCodeGenString* _stringLiteral1135;
extern Il2CppCodeGenString* _stringLiteral1136;
extern "C" TextGenerationSettings_t824  TextGenerator_ValidatedSettings_m7035 (TextGenerator_t723 * __this, TextGenerationSettings_t824  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1135 = il2cpp_codegen_string_literal_from_index(1135);
		_stringLiteral1136 = il2cpp_codegen_string_literal_from_index(1136);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t684 * L_0 = ((&___settings)->___font_0);
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t684 * L_2 = ((&___settings)->___font_0);
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m4370(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t824  L_4 = ___settings;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = ((&___settings)->___fontSize_2);
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = ((&___settings)->___fontStyle_6);
		if (!L_6)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		Debug_LogWarning_m430(NULL /*static, unused*/, _stringLiteral1135, /*hidden argument*/NULL);
		(&___settings)->___fontSize_2 = 0;
		(&___settings)->___fontStyle_6 = 0;
	}

IL_0057:
	{
		bool L_7 = ((&___settings)->___resizeTextForBestFit_8);
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		Debug_LogWarning_m430(NULL /*static, unused*/, _stringLiteral1136, /*hidden argument*/NULL);
		(&___settings)->___resizeTextForBestFit_8 = 0;
	}

IL_0075:
	{
		TextGenerationSettings_t824  L_8 = ___settings;
		return L_8;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C" void TextGenerator_Invalidate_m4369 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasGenerated_3 = 0;
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C" void TextGenerator_GetCharacters_m7036 (TextGenerator_t723 * __this, List_1_t1288 * ___characters, const MethodInfo* method)
{
	{
		List_1_t1288 * L_0 = ___characters;
		TextGenerator_GetCharactersInternal_m7050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C" void TextGenerator_GetLines_m7037 (TextGenerator_t723 * __this, List_1_t1289 * ___lines, const MethodInfo* method)
{
	{
		List_1_t1289 * L_0 = ___lines;
		TextGenerator_GetLinesInternal_m7052(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void TextGenerator_GetVertices_m7038 (TextGenerator_t723 * __this, List_1_t724 * ___vertices, const MethodInfo* method)
{
	{
		List_1_t724 * L_0 = ___vertices;
		TextGenerator_GetVerticesInternal_m7047(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredWidth_m4374 (TextGenerator_t723 * __this, String_t* ___str, TextGenerationSettings_t824  ___settings, const MethodInfo* method)
{
	Rect_t30  V_0 = {0};
	{
		(&___settings)->___horizontalOverflow_13 = 1;
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t824  L_1 = ___settings;
		TextGenerator_Populate_m4269(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t30  L_2 = TextGenerator_get_rectExtents_m4270(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m370((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredHeight_m4375 (TextGenerator_t723 * __this, String_t* ___str, TextGenerationSettings_t824  ___settings, const MethodInfo* method)
{
	Rect_t30  V_0 = {0};
	{
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t824  L_1 = ___settings;
		TextGenerator_Populate_m4269(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t30  L_2 = TextGenerator_get_rectExtents_m4270(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m372((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextGenerator_Populate_m4269 (TextGenerator_t723 * __this, String_t* ___str, TextGenerationSettings_t824  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasGenerated_3);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str;
		String_t* L_2 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m385(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t824  L_4 = (__this->___m_LastSettings_2);
		bool L_5 = TextGenerationSettings_Equals_m7032((&___settings), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = (__this->___m_LastValid_4);
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str;
		TextGenerationSettings_t824  L_8 = ___settings;
		bool L_9 = TextGenerator_PopulateAlways_m7039(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C" bool TextGenerator_PopulateAlways_m7039 (TextGenerator_t723 * __this, String_t* ___str, TextGenerationSettings_t824  ___settings, const MethodInfo* method)
{
	TextGenerationSettings_t824  V_0 = {0};
	{
		String_t* L_0 = ___str;
		__this->___m_LastString_1 = L_0;
		__this->___m_HasGenerated_3 = 1;
		__this->___m_CachedVerts_8 = 0;
		__this->___m_CachedCharacters_9 = 0;
		__this->___m_CachedLines_10 = 0;
		TextGenerationSettings_t824  L_1 = ___settings;
		__this->___m_LastSettings_2 = L_1;
		TextGenerationSettings_t824  L_2 = ___settings;
		TextGenerationSettings_t824  L_3 = TextGenerator_ValidatedSettings_m7035(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str;
		Font_t684 * L_5 = ((&V_0)->___font_0);
		Color_t5  L_6 = ((&V_0)->___color_1);
		int32_t L_7 = ((&V_0)->___fontSize_2);
		float L_8 = ((&V_0)->___scaleFactor_5);
		float L_9 = ((&V_0)->___lineSpacing_3);
		int32_t L_10 = ((&V_0)->___fontStyle_6);
		bool L_11 = ((&V_0)->___richText_4);
		bool L_12 = ((&V_0)->___resizeTextForBestFit_8);
		int32_t L_13 = ((&V_0)->___resizeTextMinSize_9);
		int32_t L_14 = ((&V_0)->___resizeTextMaxSize_10);
		int32_t L_15 = ((&V_0)->___verticalOverflow_12);
		int32_t L_16 = ((&V_0)->___horizontalOverflow_13);
		bool L_17 = ((&V_0)->___updateBounds_11);
		int32_t L_18 = ((&V_0)->___textAnchor_7);
		Vector2_t29  L_19 = ((&V_0)->___generationExtents_14);
		Vector2_t29  L_20 = ((&V_0)->___pivot_15);
		bool L_21 = ((&V_0)->___generateOutOfBounds_16);
		bool L_22 = TextGenerator_Populate_Internal_m7042(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		__this->___m_LastValid_4 = L_22;
		bool L_23 = (__this->___m_LastValid_4);
		return L_23;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C" Object_t* TextGenerator_get_verts_m4373 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedVerts_8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t724 * L_1 = (__this->___m_Verts_5);
		TextGenerator_GetVertices_m7038(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedVerts_8 = 1;
	}

IL_001e:
	{
		List_1_t724 * L_2 = (__this->___m_Verts_5);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C" Object_t* TextGenerator_get_characters_m4253 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedCharacters_9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1288 * L_1 = (__this->___m_Characters_6);
		TextGenerator_GetCharacters_m7036(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedCharacters_9 = 1;
	}

IL_001e:
	{
		List_1_t1288 * L_2 = (__this->___m_Characters_6);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C" Object_t* TextGenerator_get_lines_m4250 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedLines_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1289 * L_1 = (__this->___m_Lines_7);
		TextGenerator_GetLines_m7037(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedLines_10 = 1;
	}

IL_001e:
	{
		List_1_t1289 * L_2 = (__this->___m_Lines_7);
		return L_2;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C" void TextGenerator_Init_m7040 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m7040_ftn) (TextGenerator_t723 *);
	static TextGenerator_Init_m7040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m7040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C" void TextGenerator_Dispose_cpp_m7041 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m7041_ftn) (TextGenerator_t723 *);
	static TextGenerator_Dispose_cpp_m7041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m7041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_m7042 (TextGenerator_t723 * __this, String_t* ___str, Font_t684 * ___font, Color_t5  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, Vector2_t29  ___extents, Vector2_t29  ___pivot, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t684 * L_1 = ___font;
		Color_t5  L_2 = ___color;
		int32_t L_3 = ___fontSize;
		float L_4 = ___scaleFactor;
		float L_5 = ___lineSpacing;
		int32_t L_6 = ___style;
		bool L_7 = ___richText;
		bool L_8 = ___resizeTextForBestFit;
		int32_t L_9 = ___resizeTextMinSize;
		int32_t L_10 = ___resizeTextMaxSize;
		int32_t L_11 = ___verticalOverFlow;
		int32_t L_12 = ___horizontalOverflow;
		bool L_13 = ___updateBounds;
		int32_t L_14 = ___anchor;
		float L_15 = ((&___extents)->___x_1);
		float L_16 = ((&___extents)->___y_2);
		float L_17 = ((&___pivot)->___x_1);
		float L_18 = ((&___pivot)->___y_2);
		bool L_19 = ___generateOutOfBounds;
		bool L_20 = TextGenerator_Populate_Internal_cpp_m7043(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_cpp_m7043 (TextGenerator_t723 * __this, String_t* ___str, Font_t684 * ___font, Color_t5  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t684 * L_1 = ___font;
		int32_t L_2 = ___fontSize;
		float L_3 = ___scaleFactor;
		float L_4 = ___lineSpacing;
		int32_t L_5 = ___style;
		bool L_6 = ___richText;
		bool L_7 = ___resizeTextForBestFit;
		int32_t L_8 = ___resizeTextMinSize;
		int32_t L_9 = ___resizeTextMaxSize;
		int32_t L_10 = ___verticalOverFlow;
		int32_t L_11 = ___horizontalOverflow;
		bool L_12 = ___updateBounds;
		int32_t L_13 = ___anchor;
		float L_14 = ___extentsX;
		float L_15 = ___extentsY;
		float L_16 = ___pivotX;
		float L_17 = ___pivotY;
		bool L_18 = ___generateOutOfBounds;
		bool L_19 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7044(NULL /*static, unused*/, __this, L_0, L_1, (&___color), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7044 (Object_t * __this /* static, unused */, TextGenerator_t723 * ___self, String_t* ___str, Font_t684 * ___font, Color_t5 * ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7044_ftn) (TextGenerator_t723 *, String_t*, Font_t684 *, Color_t5 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7044_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7044_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)");
	return _il2cpp_icall_func(___self, ___str, ___font, ___color, ___fontSize, ___scaleFactor, ___lineSpacing, ___style, ___richText, ___resizeTextForBestFit, ___resizeTextMinSize, ___resizeTextMaxSize, ___verticalOverFlow, ___horizontalOverflow, ___updateBounds, ___anchor, ___extentsX, ___extentsY, ___pivotX, ___pivotY, ___generateOutOfBounds);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C" Rect_t30  TextGenerator_get_rectExtents_m4270 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	Rect_t30  V_0 = {0};
	{
		TextGenerator_INTERNAL_get_rectExtents_m7045(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t30  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m7045 (TextGenerator_t723 * __this, Rect_t30 * ___value, const MethodInfo* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m7045_ftn) (TextGenerator_t723 *, Rect_t30 *);
	static TextGenerator_INTERNAL_get_rectExtents_m7045_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m7045_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C" int32_t TextGenerator_get_vertexCount_m7046 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m7046_ftn) (TextGenerator_t723 *);
	static TextGenerator_get_vertexCount_m7046_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m7046_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C" void TextGenerator_GetVerticesInternal_m7047 (TextGenerator_t723 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m7047_ftn) (TextGenerator_t723 *, Object_t *);
	static TextGenerator_GetVerticesInternal_m7047_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m7047_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
extern "C" UIVertexU5BU5D_t722* TextGenerator_GetVerticesArray_m7048 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef UIVertexU5BU5D_t722* (*TextGenerator_GetVerticesArray_m7048_ftn) (TextGenerator_t723 *);
	static TextGenerator_GetVerticesArray_m7048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesArray_m7048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C" int32_t TextGenerator_get_characterCount_m7049 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m7049_ftn) (TextGenerator_t723 *);
	static TextGenerator_get_characterCount_m7049_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m7049_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" int32_t TextGenerator_get_characterCountVisible_m4252 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2149(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = (__this->___m_LastString_1);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m410(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m7046(__this, /*hidden argument*/NULL);
		int32_t L_5 = Mathf_Max_m4262(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m288(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C" void TextGenerator_GetCharactersInternal_m7050 (TextGenerator_t723 * __this, Object_t * ___characters, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m7050_ftn) (TextGenerator_t723 *, Object_t *);
	static TextGenerator_GetCharactersInternal_m7050_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m7050_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters);
}
// UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
extern "C" UICharInfoU5BU5D_t1440* TextGenerator_GetCharactersArray_m7051 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef UICharInfoU5BU5D_t1440* (*TextGenerator_GetCharactersArray_m7051_ftn) (TextGenerator_t723 *);
	static TextGenerator_GetCharactersArray_m7051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersArray_m7051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C" int32_t TextGenerator_get_lineCount_m4251 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m4251_ftn) (TextGenerator_t723 *);
	static TextGenerator_get_lineCount_m4251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m4251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C" void TextGenerator_GetLinesInternal_m7052 (TextGenerator_t723 * __this, Object_t * ___lines, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m7052_ftn) (TextGenerator_t723 *, Object_t *);
	static TextGenerator_GetLinesInternal_m7052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m7052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines);
}
// UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
extern "C" UILineInfoU5BU5D_t1441* TextGenerator_GetLinesArray_m7053 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef UILineInfoU5BU5D_t1441* (*TextGenerator_GetLinesArray_m7053_ftn) (TextGenerator_t723 *);
	static TextGenerator_GetLinesArray_m7053_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesArray_m7053_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
extern "C" int32_t TextGenerator_get_fontSizeUsedForBestFit_m4289 (TextGenerator_t723 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_fontSizeUsedForBestFit_m4289_ftn) (TextGenerator_t723 *);
	static TextGenerator_get_fontSizeUsedForBestFit_m4289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_fontSizeUsedForBestFit_m4289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern TypeInfo* TrackedReference_t1290_il2cpp_TypeInfo_var;
extern "C" bool TrackedReference_Equals_m7054 (TrackedReference_t1290 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackedReference_t1290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(924);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = TrackedReference_op_Equality_m7056(NULL /*static, unused*/, ((TrackedReference_t1290 *)IsInstClass(L_0, TrackedReference_t1290_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C" int32_t TrackedReference_GetHashCode_m7055 (TrackedReference_t1290 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		int32_t L_1 = IntPtr_op_Explicit_m8144(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool TrackedReference_op_Equality_m7056 (Object_t * __this /* static, unused */, TrackedReference_t1290 * ___x, TrackedReference_t1290 * ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		TrackedReference_t1290 * L_0 = ___x;
		V_0 = L_0;
		TrackedReference_t1290 * L_1 = ___y;
		V_1 = L_1;
		Object_t * L_2 = V_1;
		if (L_2)
		{
			goto IL_0012;
		}
	}
	{
		Object_t * L_3 = V_0;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		return 1;
	}

IL_0012:
	{
		Object_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		TrackedReference_t1290 * L_5 = ___x;
		NullCheck(L_5);
		IntPtr_t L_6 = (L_5->___m_Ptr_0);
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_8 = IntPtr_op_Equality_m6518(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0029:
	{
		Object_t * L_9 = V_0;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		TrackedReference_t1290 * L_10 = ___y;
		NullCheck(L_10);
		IntPtr_t L_11 = (L_10->___m_Ptr_0);
		IntPtr_t L_12 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_13 = IntPtr_op_Equality_m6518(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0040:
	{
		TrackedReference_t1290 * L_14 = ___x;
		NullCheck(L_14);
		IntPtr_t L_15 = (L_14->___m_Ptr_0);
		TrackedReference_t1290 * L_16 = ___y;
		NullCheck(L_16);
		IntPtr_t L_17 = (L_16->___m_Ptr_0);
		bool L_18 = IntPtr_op_Equality_m6518(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1290_marshal(const TrackedReference_t1290& unmarshaled, TrackedReference_t1290_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void TrackedReference_t1290_marshal_back(const TrackedReference_t1290_marshaled& marshaled, TrackedReference_t1290& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1290_marshal_cleanup(TrackedReference_t1290_marshaled& marshaled)
{
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C" void ArgumentCache__ctor_m7057 (ArgumentCache_t1292 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C" Object_t53 * ArgumentCache_get_unityObjectArgument_m7058 (ArgumentCache_t1292 * __this, const MethodInfo* method)
{
	{
		Object_t53 * L_0 = (__this->___m_ObjectArgument_0);
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C" String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7059 (ArgumentCache_t1292 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_ObjectArgumentAssemblyTypeName_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C" int32_t ArgumentCache_get_intArgument_m7060 (ArgumentCache_t1292 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_IntArgument_2);
		return L_0;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C" float ArgumentCache_get_floatArgument_m7061 (ArgumentCache_t1292 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FloatArgument_3);
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C" String_t* ArgumentCache_get_stringArgument_m7062 (ArgumentCache_t1292 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringArgument_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C" bool ArgumentCache_get_boolArgument_m7063 (ArgumentCache_t1292 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_BoolArgument_5);
		return L_0;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C" void BaseInvokableCall__ctor_m7064 (BaseInvokableCall_t1293 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69;
extern Il2CppCodeGenString* _stringLiteral1137;
extern "C" void BaseInvokableCall__ctor_m7065 (BaseInvokableCall_t1293 * __this, Object_t * ___target, MethodInfo_t * ___function, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral69 = il2cpp_codegen_string_literal_from_index(69);
		_stringLiteral1137 = il2cpp_codegen_string_literal_from_index(1137);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___target;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, _stringLiteral69, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		MethodInfo_t * L_2 = ___function;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, _stringLiteral1137, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C" bool BaseInvokableCall_AllowInvoke_m7066 (Object_t * __this /* static, unused */, Delegate_t506 * ___delegate, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Delegate_t506 * L_0 = ___delegate;
		NullCheck(L_0);
		MethodInfo_t * L_1 = Delegate_get_Method_m8145(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Reflection.MethodBase::get_IsStatic() */, L_1);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		Delegate_t506 * L_3 = ___delegate;
		NullCheck(L_3);
		Object_t * L_4 = Delegate_get_Target_m2159(L_3, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((Object_t*)(Object_t *)L_4) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const Il2CppType* UnityAction_t691_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_t691_il2cpp_TypeInfo_var;
extern "C" void InvokableCall__ctor_m7067 (InvokableCall_t1294 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAction_t691_0_0_0_var = il2cpp_codegen_type_from_index(536);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		UnityAction_t691_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(536);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___target;
		MethodInfo_t * L_1 = ___theFunction;
		BaseInvokableCall__ctor_m7065(__this, L_0, L_1, /*hidden argument*/NULL);
		UnityAction_t691 * L_2 = (__this->___Delegate_0);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(UnityAction_t691_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_4 = ___target;
		MethodInfo_t * L_5 = ___theFunction;
		Delegate_t506 * L_6 = Delegate_CreateDelegate_m2731(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		Delegate_t506 * L_7 = Delegate_Combine_m2300(NULL /*static, unused*/, L_2, ((UnityAction_t691 *)IsInstSealed(L_6, UnityAction_t691_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->___Delegate_0 = ((UnityAction_t691 *)CastclassSealed(L_7, UnityAction_t691_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C" void InvokableCall_Invoke_m7068 (InvokableCall_t1294 * __this, ObjectU5BU5D_t34* ___args, const MethodInfo* method)
{
	{
		UnityAction_t691 * L_0 = (__this->___Delegate_0);
		bool L_1 = BaseInvokableCall_AllowInvoke_m7066(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UnityAction_t691 * L_2 = (__this->___Delegate_0);
		NullCheck(L_2);
		UnityAction_Invoke_m4130(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_Find_m7069 (InvokableCall_t1294 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_t691 * L_0 = (__this->___Delegate_0);
		NullCheck(L_0);
		Object_t * L_1 = Delegate_get_Target_m2159(L_0, /*hidden argument*/NULL);
		Object_t * L_2 = ___targetObj;
		if ((!(((Object_t*)(Object_t *)L_1) == ((Object_t*)(Object_t *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_t691 * L_3 = (__this->___Delegate_0);
		NullCheck(L_3);
		MethodInfo_t * L_4 = Delegate_get_Method_m8145(L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method;
		G_B3_0 = ((((Object_t*)(MethodInfo_t *)L_4) == ((Object_t*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern TypeInfo* ArgumentCache_t1292_il2cpp_TypeInfo_var;
extern "C" void PersistentCall__ctor_m7070 (PersistentCall_t1296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentCache_t1292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(925);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArgumentCache_t1292 * L_0 = (ArgumentCache_t1292 *)il2cpp_codegen_object_new (ArgumentCache_t1292_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m7057(L_0, /*hidden argument*/NULL);
		__this->___m_Arguments_3 = L_0;
		__this->___m_CallState_4 = 2;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C" Object_t53 * PersistentCall_get_target_m7071 (PersistentCall_t1296 * __this, const MethodInfo* method)
{
	{
		Object_t53 * L_0 = (__this->___m_Target_0);
		return L_0;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C" String_t* PersistentCall_get_methodName_m7072 (PersistentCall_t1296 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_MethodName_1);
		return L_0;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C" int32_t PersistentCall_get_mode_m7073 (PersistentCall_t1296 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Mode_2);
		return L_0;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C" ArgumentCache_t1292 * PersistentCall_get_arguments_m7074 (PersistentCall_t1296 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_t1292 * L_0 = (__this->___m_Arguments_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool PersistentCall_IsValid_m7075 (PersistentCall_t1296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Object_t53 * L_0 = PersistentCall_get_target_m7071(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m7072(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2149(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern TypeInfo* CachedInvokableCall_1_t1460_il2cpp_TypeInfo_var;
extern TypeInfo* CachedInvokableCall_1_t1461_il2cpp_TypeInfo_var;
extern TypeInfo* CachedInvokableCall_1_t1462_il2cpp_TypeInfo_var;
extern TypeInfo* CachedInvokableCall_1_t1463_il2cpp_TypeInfo_var;
extern TypeInfo* InvokableCall_t1294_il2cpp_TypeInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m8146_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m8147_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m8148_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m8149_MethodInfo_var;
extern "C" BaseInvokableCall_t1293 * PersistentCall_GetRuntimeCall_m7076 (PersistentCall_t1296 * __this, UnityEventBase_t1301 * ___theEvent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CachedInvokableCall_1_t1460_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(926);
		CachedInvokableCall_1_t1461_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(927);
		CachedInvokableCall_1_t1462_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(928);
		CachedInvokableCall_1_t1463_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(929);
		InvokableCall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(930);
		CachedInvokableCall_1__ctor_m8146_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484701);
		CachedInvokableCall_1__ctor_m8147_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484702);
		CachedInvokableCall_1__ctor_m8148_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484703);
		CachedInvokableCall_1__ctor_m8149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484704);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfo_t * V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = (__this->___m_CallState_4);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		UnityEventBase_t1301 * L_1 = ___theEvent;
		if (L_1)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (BaseInvokableCall_t1293 *)NULL;
	}

IL_0013:
	{
		UnityEventBase_t1301 * L_2 = ___theEvent;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m7089(L_2, __this, /*hidden argument*/NULL);
		V_0 = L_3;
		MethodInfo_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		return (BaseInvokableCall_t1293 *)NULL;
	}

IL_0023:
	{
		int32_t L_5 = (__this->___m_Mode_2);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6 == 0)
		{
			goto IL_0051;
		}
		if (L_6 == 1)
		{
			goto IL_00d2;
		}
		if (L_6 == 2)
		{
			goto IL_005f;
		}
		if (L_6 == 3)
		{
			goto IL_008a;
		}
		if (L_6 == 4)
		{
			goto IL_0072;
		}
		if (L_6 == 5)
		{
			goto IL_00a2;
		}
		if (L_6 == 6)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00df;
	}

IL_0051:
	{
		UnityEventBase_t1301 * L_7 = ___theEvent;
		Object_t53 * L_8 = PersistentCall_get_target_m7071(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_0;
		NullCheck(L_7);
		BaseInvokableCall_t1293 * L_10 = (BaseInvokableCall_t1293 *)VirtFuncInvoker2< BaseInvokableCall_t1293 *, Object_t *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		return L_10;
	}

IL_005f:
	{
		Object_t53 * L_11 = PersistentCall_get_target_m7071(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_0;
		ArgumentCache_t1292 * L_13 = (__this->___m_Arguments_3);
		BaseInvokableCall_t1293 * L_14 = PersistentCall_GetObjectCall_m7077(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0072:
	{
		Object_t53 * L_15 = PersistentCall_get_target_m7071(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_0;
		ArgumentCache_t1292 * L_17 = (__this->___m_Arguments_3);
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m7061(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t1460 * L_19 = (CachedInvokableCall_1_t1460 *)il2cpp_codegen_object_new (CachedInvokableCall_1_t1460_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m8146(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m8146_MethodInfo_var);
		return L_19;
	}

IL_008a:
	{
		Object_t53 * L_20 = PersistentCall_get_target_m7071(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_0;
		ArgumentCache_t1292 * L_22 = (__this->___m_Arguments_3);
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m7060(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t1461 * L_24 = (CachedInvokableCall_1_t1461 *)il2cpp_codegen_object_new (CachedInvokableCall_1_t1461_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m8147(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m8147_MethodInfo_var);
		return L_24;
	}

IL_00a2:
	{
		Object_t53 * L_25 = PersistentCall_get_target_m7071(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_0;
		ArgumentCache_t1292 * L_27 = (__this->___m_Arguments_3);
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m7062(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t1462 * L_29 = (CachedInvokableCall_1_t1462 *)il2cpp_codegen_object_new (CachedInvokableCall_1_t1462_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m8148(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m8148_MethodInfo_var);
		return L_29;
	}

IL_00ba:
	{
		Object_t53 * L_30 = PersistentCall_get_target_m7071(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_0;
		ArgumentCache_t1292 * L_32 = (__this->___m_Arguments_3);
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m7063(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t1463 * L_34 = (CachedInvokableCall_1_t1463 *)il2cpp_codegen_object_new (CachedInvokableCall_1_t1463_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m8149(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m8149_MethodInfo_var);
		return L_34;
	}

IL_00d2:
	{
		Object_t53 * L_35 = PersistentCall_get_target_m7071(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_0;
		InvokableCall_t1294 * L_37 = (InvokableCall_t1294 *)il2cpp_codegen_object_new (InvokableCall_t1294_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m7067(L_37, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}

IL_00df:
	{
		return (BaseInvokableCall_t1293 *)NULL;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern const Il2CppType* Object_t53_0_0_0_var;
extern const Il2CppType* CachedInvokableCall_1_t1465_0_0_0_var;
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1218_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* BaseInvokableCall_t1293_il2cpp_TypeInfo_var;
extern "C" BaseInvokableCall_t1293 * PersistentCall_GetObjectCall_m7077 (Object_t * __this /* static, unused */, Object_t53 * ___target, MethodInfo_t * ___method, ArgumentCache_t1292 * ___arguments, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t53_0_0_0_var = il2cpp_codegen_type_from_index(506);
		CachedInvokableCall_1_t1465_0_0_0_var = il2cpp_codegen_type_from_index(931);
		MethodInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(404);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		TypeU5BU5D_t1218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(844);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		BaseInvokableCall_t1293_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(932);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	ConstructorInfo_t1464 * V_3 = {0};
	Object_t53 * V_4 = {0};
	Type_t * G_B3_0 = {0};
	Type_t * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Object_t53_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t1292 * L_1 = ___arguments;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7059(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2149(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentCache_t1292 * L_4 = ___arguments;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7059(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m8150, L_5, 0, "UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Object_t53_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0038:
	{
		V_0 = G_B3_0;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t1465_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t1218* L_11 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_12);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_11, 0, sizeof(Type_t *))) = (Type_t *)L_12;
		NullCheck(L_10);
		Type_t * L_13 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1218* >::Invoke(77 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t1218* L_15 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 3));
		Type_t * L_16 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Object_t53_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_16);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_15, 0, sizeof(Type_t *))) = (Type_t *)L_16;
		TypeU5BU5D_t1218* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		ArrayElementTypeCheck (L_17, L_18);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_17, 1, sizeof(Type_t *))) = (Type_t *)L_18;
		TypeU5BU5D_t1218* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, L_20);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_19, 2, sizeof(Type_t *))) = (Type_t *)L_20;
		NullCheck(L_14);
		ConstructorInfo_t1464 * L_21 = (ConstructorInfo_t1464 *)VirtFuncInvoker1< ConstructorInfo_t1464 *, TypeU5BU5D_t1218* >::Invoke(67 /* System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[]) */, L_14, L_19);
		V_3 = L_21;
		ArgumentCache_t1292 * L_22 = ___arguments;
		NullCheck(L_22);
		Object_t53 * L_23 = ArgumentCache_get_unityObjectArgument_m7058(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t53 * L_24 = V_4;
		bool L_25 = Object_op_Inequality_m395(NULL /*static, unused*/, L_24, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00aa;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t53 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m303(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00aa;
		}
	}
	{
		V_4 = (Object_t53 *)NULL;
	}

IL_00aa:
	{
		ConstructorInfo_t1464 * L_30 = V_3;
		ObjectU5BU5D_t34* L_31 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 3));
		Object_t53 * L_32 = ___target;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 0);
		ArrayElementTypeCheck (L_31, L_32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 0, sizeof(Object_t *))) = (Object_t *)L_32;
		ObjectU5BU5D_t34* L_33 = L_31;
		MethodInfo_t * L_34 = ___method;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 1);
		ArrayElementTypeCheck (L_33, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, 1, sizeof(Object_t *))) = (Object_t *)L_34;
		ObjectU5BU5D_t34* L_35 = L_33;
		Object_t53 * L_36 = V_4;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 2);
		ArrayElementTypeCheck (L_35, L_36);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_35, 2, sizeof(Object_t *))) = (Object_t *)L_36;
		NullCheck(L_30);
		Object_t * L_37 = ConstructorInfo_Invoke_m8151(L_30, L_35, /*hidden argument*/NULL);
		return ((BaseInvokableCall_t1293 *)IsInstClass(L_37, BaseInvokableCall_t1293_il2cpp_TypeInfo_var));
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern TypeInfo* List_1_t1298_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8152_MethodInfo_var;
extern "C" void PersistentCallGroup__ctor_m7078 (PersistentCallGroup_t1297 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(934);
		List_1__ctor_m8152_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484705);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		List_1_t1298 * L_0 = (List_1_t1298 *)il2cpp_codegen_object_new (List_1_t1298_il2cpp_TypeInfo_var);
		List_1__ctor_m8152(L_0, /*hidden argument*/List_1__ctor_m8152_MethodInfo_var);
		__this->___m_Calls_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern TypeInfo* Enumerator_t1466_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m8153_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m8154_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m8155_MethodInfo_var;
extern "C" void PersistentCallGroup_Initialize_m7079 (PersistentCallGroup_t1297 * __this, InvokableCallList_t1299 * ___invokableList, UnityEventBase_t1301 * ___unityEventBase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1466_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(935);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		List_1_GetEnumerator_m8153_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484706);
		Enumerator_get_Current_m8154_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484707);
		Enumerator_MoveNext_m8155_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484708);
		s_Il2CppMethodIntialized = true;
	}
	PersistentCall_t1296 * V_0 = {0};
	Enumerator_t1466  V_1 = {0};
	BaseInvokableCall_t1293 * V_2 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1298 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		Enumerator_t1466  L_1 = List_1_GetEnumerator_m8153(L_0, /*hidden argument*/List_1_GetEnumerator_m8153_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			PersistentCall_t1296 * L_2 = Enumerator_get_Current_m8154((&V_1), /*hidden argument*/Enumerator_get_Current_m8154_MethodInfo_var);
			V_0 = L_2;
			PersistentCall_t1296 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m7075(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0029;
			}
		}

IL_0024:
		{
			goto IL_003e;
		}

IL_0029:
		{
			PersistentCall_t1296 * L_5 = V_0;
			UnityEventBase_t1301 * L_6 = ___unityEventBase;
			NullCheck(L_5);
			BaseInvokableCall_t1293 * L_7 = PersistentCall_GetRuntimeCall_m7076(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t1293 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_003e;
			}
		}

IL_0037:
		{
			InvokableCallList_t1299 * L_9 = ___invokableList;
			BaseInvokableCall_t1293 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m7081(L_9, L_10, /*hidden argument*/NULL);
		}

IL_003e:
		{
			bool L_11 = Enumerator_MoveNext_m8155((&V_1), /*hidden argument*/Enumerator_MoveNext_m8155_MethodInfo_var);
			if (L_11)
			{
				goto IL_0011;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Enumerator_t1466  L_12 = V_1;
		Enumerator_t1466  L_13 = L_12;
		Object_t * L_14 = Box(Enumerator_t1466_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_14);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_005b:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern TypeInfo* List_1_t1300_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8156_MethodInfo_var;
extern "C" void InvokableCallList__ctor_m7080 (InvokableCallList_t1299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(936);
		List_1__ctor_m8156_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484709);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1300 * L_0 = (List_1_t1300 *)il2cpp_codegen_object_new (List_1_t1300_il2cpp_TypeInfo_var);
		List_1__ctor_m8156(L_0, /*hidden argument*/List_1__ctor_m8156_MethodInfo_var);
		__this->___m_PersistentCalls_0 = L_0;
		List_1_t1300 * L_1 = (List_1_t1300 *)il2cpp_codegen_object_new (List_1_t1300_il2cpp_TypeInfo_var);
		List_1__ctor_m8156(L_1, /*hidden argument*/List_1__ctor_m8156_MethodInfo_var);
		__this->___m_RuntimeCalls_1 = L_1;
		List_1_t1300 * L_2 = (List_1_t1300 *)il2cpp_codegen_object_new (List_1_t1300_il2cpp_TypeInfo_var);
		List_1__ctor_m8156(L_2, /*hidden argument*/List_1__ctor_m8156_MethodInfo_var);
		__this->___m_ExecutingCalls_2 = L_2;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C" void InvokableCallList_AddPersistentInvokableCall_m7081 (InvokableCallList_t1299 * __this, BaseInvokableCall_t1293 * ___call, const MethodInfo* method)
{
	{
		List_1_t1300 * L_0 = (__this->___m_PersistentCalls_0);
		BaseInvokableCall_t1293 * L_1 = ___call;
		NullCheck(L_0);
		VirtActionInvoker1< BaseInvokableCall_t1293 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C" void InvokableCallList_AddListener_m7082 (InvokableCallList_t1299 * __this, BaseInvokableCall_t1293 * ___call, const MethodInfo* method)
{
	{
		List_1_t1300 * L_0 = (__this->___m_RuntimeCalls_1);
		BaseInvokableCall_t1293 * L_1 = ___call;
		NullCheck(L_0);
		VirtActionInvoker1< BaseInvokableCall_t1293 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern TypeInfo* List_1_t1300_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1467_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8156_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m8157_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAll_m8158_MethodInfo_var;
extern "C" void InvokableCallList_RemoveListener_m7083 (InvokableCallList_t1299 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(936);
		Predicate_1_t1467_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(937);
		List_1__ctor_m8156_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484709);
		Predicate_1__ctor_m8157_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484710);
		List_1_RemoveAll_m8158_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484711);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1300 * V_0 = {0};
	int32_t V_1 = 0;
	{
		List_1_t1300 * L_0 = (List_1_t1300 *)il2cpp_codegen_object_new (List_1_t1300_il2cpp_TypeInfo_var);
		List_1__ctor_m8156(L_0, /*hidden argument*/List_1__ctor_m8156_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003b;
	}

IL_000d:
	{
		List_1_t1300 * L_1 = (__this->___m_RuntimeCalls_1);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t1293 * L_3 = (BaseInvokableCall_t1293 *)VirtFuncInvoker1< BaseInvokableCall_t1293 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32) */, L_1, L_2);
		Object_t * L_4 = ___targetObj;
		MethodInfo_t * L_5 = ___method;
		NullCheck(L_3);
		bool L_6 = (bool)VirtFuncInvoker2< bool, Object_t *, MethodInfo_t * >::Invoke(5 /* System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo) */, L_3, L_4, L_5);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t1300 * L_7 = V_0;
		List_1_t1300 * L_8 = (__this->___m_RuntimeCalls_1);
		int32_t L_9 = V_1;
		NullCheck(L_8);
		BaseInvokableCall_t1293 * L_10 = (BaseInvokableCall_t1293 *)VirtFuncInvoker1< BaseInvokableCall_t1293 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32) */, L_8, L_9);
		NullCheck(L_7);
		VirtActionInvoker1< BaseInvokableCall_t1293 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0) */, L_7, L_10);
	}

IL_0037:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_12 = V_1;
		List_1_t1300 * L_13 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count() */, L_13);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t1300 * L_15 = (__this->___m_RuntimeCalls_1);
		List_1_t1300 * L_16 = V_0;
		List_1_t1300 * L_17 = L_16;
		IntPtr_t L_18 = { (void*)GetVirtualMethodInfo(L_17, 24) };
		Predicate_1_t1467 * L_19 = (Predicate_1_t1467 *)il2cpp_codegen_object_new (Predicate_1_t1467_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m8157(L_19, L_17, L_18, /*hidden argument*/Predicate_1__ctor_m8157_MethodInfo_var);
		NullCheck(L_15);
		List_1_RemoveAll_m8158(L_15, L_19, /*hidden argument*/List_1_RemoveAll_m8158_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C" void InvokableCallList_ClearPersistent_m7084 (InvokableCallList_t1299 * __this, const MethodInfo* method)
{
	{
		List_1_t1300 * L_0 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear() */, L_0);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern const MethodInfo* List_1_AddRange_m8159_MethodInfo_var;
extern "C" void InvokableCallList_Invoke_m7085 (InvokableCallList_t1299 * __this, ObjectU5BU5D_t34* ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_AddRange_m8159_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484712);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t1300 * L_0 = (__this->___m_ExecutingCalls_2);
		List_1_t1300 * L_1 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		List_1_AddRange_m8159(L_0, L_1, /*hidden argument*/List_1_AddRange_m8159_MethodInfo_var);
		List_1_t1300 * L_2 = (__this->___m_ExecutingCalls_2);
		List_1_t1300 * L_3 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_2);
		List_1_AddRange_m8159(L_2, L_3, /*hidden argument*/List_1_AddRange_m8159_MethodInfo_var);
		V_0 = 0;
		goto IL_003f;
	}

IL_0029:
	{
		List_1_t1300 * L_4 = (__this->___m_ExecutingCalls_2);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		BaseInvokableCall_t1293 * L_6 = (BaseInvokableCall_t1293 *)VirtFuncInvoker1< BaseInvokableCall_t1293 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32) */, L_4, L_5);
		ObjectU5BU5D_t34* L_7 = ___parameters;
		NullCheck(L_6);
		VirtActionInvoker1< ObjectU5BU5D_t34* >::Invoke(4 /* System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[]) */, L_6, L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_0;
		List_1_t1300 * L_10 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count() */, L_10);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0029;
		}
	}
	{
		List_1_t1300 * L_12 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear() */, L_12);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern TypeInfo* InvokableCallList_t1299_il2cpp_TypeInfo_var;
extern TypeInfo* PersistentCallGroup_t1297_il2cpp_TypeInfo_var;
extern "C" void UnityEventBase__ctor_m7086 (UnityEventBase_t1301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvokableCallList_t1299_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(938);
		PersistentCallGroup_t1297_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(939);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_CallsDirty_3 = 1;
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		InvokableCallList_t1299 * L_0 = (InvokableCallList_t1299 *)il2cpp_codegen_object_new (InvokableCallList_t1299_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m7080(L_0, /*hidden argument*/NULL);
		__this->___m_Calls_0 = L_0;
		PersistentCallGroup_t1297 * L_1 = (PersistentCallGroup_t1297 *)il2cpp_codegen_object_new (PersistentCallGroup_t1297_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m7078(L_1, /*hidden argument*/NULL);
		__this->___m_PersistentCalls_1 = L_1;
		Type_t * L_2 = Object_GetType_m303(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->___m_TypeName_2 = L_3;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m7087 (UnityEventBase_t1301 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m7088 (UnityEventBase_t1301 * __this, const MethodInfo* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m7091(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m303(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->___m_TypeName_2 = L_1;
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern const Il2CppType* Object_t53_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEventBase_FindMethod_m7089 (UnityEventBase_t1301 * __this, PersistentCall_t1296 * ___call, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t53_0_0_0_var = il2cpp_codegen_type_from_index(506);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Type_t * G_B3_0 = {0};
	Type_t * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Object_t53_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		PersistentCall_t1296 * L_1 = ___call;
		NullCheck(L_1);
		ArgumentCache_t1292 * L_2 = PersistentCall_get_arguments_m7074(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7059(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2149(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PersistentCall_t1296 * L_5 = ___call;
		NullCheck(L_5);
		ArgumentCache_t1292 * L_6 = PersistentCall_get_arguments_m7074(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m7059(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m8150, L_7, 0, "UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_9 = L_8;
		G_B2_0 = L_9;
		if (L_9)
		{
			G_B3_0 = L_9;
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Object_t53_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0042:
	{
		V_0 = G_B3_0;
	}

IL_0043:
	{
		PersistentCall_t1296 * L_11 = ___call;
		NullCheck(L_11);
		String_t* L_12 = PersistentCall_get_methodName_m7072(L_11, /*hidden argument*/NULL);
		PersistentCall_t1296 * L_13 = ___call;
		NullCheck(L_13);
		Object_t53 * L_14 = PersistentCall_get_target_m7071(L_13, /*hidden argument*/NULL);
		PersistentCall_t1296 * L_15 = ___call;
		NullCheck(L_15);
		int32_t L_16 = PersistentCall_get_mode_m7073(L_15, /*hidden argument*/NULL);
		Type_t * L_17 = V_0;
		MethodInfo_t * L_18 = UnityEventBase_FindMethod_m7090(__this, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern const Il2CppType* Single_t36_0_0_0_var;
extern const Il2CppType* Int32_t59_0_0_0_var;
extern const Il2CppType* Boolean_t41_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Object_t53_0_0_0_var;
extern TypeInfo* TypeU5BU5D_t1218_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEventBase_FindMethod_m7090 (UnityEventBase_t1301 * __this, String_t* ___name, Object_t * ___listener, int32_t ___mode, Type_t * ___argumentType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t36_0_0_0_var = il2cpp_codegen_type_from_index(4);
		Int32_t59_0_0_0_var = il2cpp_codegen_type_from_index(24);
		Boolean_t41_0_0_0_var = il2cpp_codegen_type_from_index(18);
		String_t_0_0_0_var = il2cpp_codegen_type_from_index(23);
		Object_t53_0_0_0_var = il2cpp_codegen_type_from_index(506);
		TypeU5BU5D_t1218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(844);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Type_t * G_B10_0 = {0};
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t1218* G_B10_2 = {0};
	TypeU5BU5D_t1218* G_B10_3 = {0};
	String_t* G_B10_4 = {0};
	Object_t * G_B10_5 = {0};
	Type_t * G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t1218* G_B9_2 = {0};
	TypeU5BU5D_t1218* G_B9_3 = {0};
	String_t* G_B9_4 = {0};
	Object_t * G_B9_5 = {0};
	{
		int32_t L_0 = ___mode;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_0032;
		}
		if (L_1 == 2)
		{
			goto IL_00ac;
		}
		if (L_1 == 3)
		{
			goto IL_005b;
		}
		if (L_1 == 4)
		{
			goto IL_0040;
		}
		if (L_1 == 5)
		{
			goto IL_0091;
		}
		if (L_1 == 6)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_00d0;
	}

IL_0029:
	{
		String_t* L_2 = ___name;
		Object_t * L_3 = ___listener;
		MethodInfo_t * L_4 = (MethodInfo_t *)VirtFuncInvoker2< MethodInfo_t *, String_t*, Object_t * >::Invoke(6 /* System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object) */, __this, L_2, L_3);
		return L_4;
	}

IL_0032:
	{
		Object_t * L_5 = ___listener;
		String_t* L_6 = ___name;
		MethodInfo_t * L_7 = UnityEventBase_GetValidMethodInfo_m7097(NULL /*static, unused*/, L_5, L_6, ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return L_7;
	}

IL_0040:
	{
		Object_t * L_8 = ___listener;
		String_t* L_9 = ___name;
		TypeU5BU5D_t1218* L_10 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Single_t36_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_10, 0, sizeof(Type_t *))) = (Type_t *)L_11;
		MethodInfo_t * L_12 = UnityEventBase_GetValidMethodInfo_m7097(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_12;
	}

IL_005b:
	{
		Object_t * L_13 = ___listener;
		String_t* L_14 = ___name;
		TypeU5BU5D_t1218* L_15 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Int32_t59_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_16);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_15, 0, sizeof(Type_t *))) = (Type_t *)L_16;
		MethodInfo_t * L_17 = UnityEventBase_GetValidMethodInfo_m7097(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_17;
	}

IL_0076:
	{
		Object_t * L_18 = ___listener;
		String_t* L_19 = ___name;
		TypeU5BU5D_t1218* L_20 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Boolean_t41_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, L_21);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_20, 0, sizeof(Type_t *))) = (Type_t *)L_21;
		MethodInfo_t * L_22 = UnityEventBase_GetValidMethodInfo_m7097(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		return L_22;
	}

IL_0091:
	{
		Object_t * L_23 = ___listener;
		String_t* L_24 = ___name;
		TypeU5BU5D_t1218* L_25 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		ArrayElementTypeCheck (L_25, L_26);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_25, 0, sizeof(Type_t *))) = (Type_t *)L_26;
		MethodInfo_t * L_27 = UnityEventBase_GetValidMethodInfo_m7097(NULL /*static, unused*/, L_23, L_24, L_25, /*hidden argument*/NULL);
		return L_27;
	}

IL_00ac:
	{
		Object_t * L_28 = ___listener;
		String_t* L_29 = ___name;
		TypeU5BU5D_t1218* L_30 = ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 1));
		Type_t * L_31 = ___argumentType;
		Type_t * L_32 = L_31;
		G_B9_0 = L_32;
		G_B9_1 = 0;
		G_B9_2 = L_30;
		G_B9_3 = L_30;
		G_B9_4 = L_29;
		G_B9_5 = L_28;
		if (L_32)
		{
			G_B10_0 = L_32;
			G_B10_1 = 0;
			G_B10_2 = L_30;
			G_B10_3 = L_30;
			G_B10_4 = L_29;
			G_B10_5 = L_28;
			goto IL_00c9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Object_t53_0_0_0_var), /*hidden argument*/NULL);
		G_B10_0 = L_33;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00c9:
	{
		NullCheck(G_B10_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B10_2, G_B10_1);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		*((Type_t **)(Type_t **)SZArrayLdElema(G_B10_2, G_B10_1, sizeof(Type_t *))) = (Type_t *)G_B10_0;
		MethodInfo_t * L_34 = UnityEventBase_GetValidMethodInfo_m7097(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/NULL);
		return L_34;
	}

IL_00d0:
	{
		return (MethodInfo_t *)NULL;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C" void UnityEventBase_DirtyPersistentCalls_m7091 (UnityEventBase_t1301 * __this, const MethodInfo* method)
{
	{
		InvokableCallList_t1299 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m7084(L_0, /*hidden argument*/NULL);
		__this->___m_CallsDirty_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C" void UnityEventBase_RebuildPersistentCallsIfNeeded_m7092 (UnityEventBase_t1301 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CallsDirty_3);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		PersistentCallGroup_t1297 * L_1 = (__this->___m_PersistentCalls_1);
		InvokableCallList_t1299 * L_2 = (__this->___m_Calls_0);
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m7079(L_1, L_2, __this, /*hidden argument*/NULL);
		__this->___m_CallsDirty_3 = 0;
	}

IL_0024:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C" void UnityEventBase_AddCall_m7093 (UnityEventBase_t1301 * __this, BaseInvokableCall_t1293 * ___call, const MethodInfo* method)
{
	{
		InvokableCallList_t1299 * L_0 = (__this->___m_Calls_0);
		BaseInvokableCall_t1293 * L_1 = ___call;
		NullCheck(L_0);
		InvokableCallList_AddListener_m7082(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C" void UnityEventBase_RemoveListener_m7094 (UnityEventBase_t1301 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method)
{
	{
		InvokableCallList_t1299 * L_0 = (__this->___m_Calls_0);
		Object_t * L_1 = ___targetObj;
		MethodInfo_t * L_2 = ___method;
		NullCheck(L_0);
		InvokableCallList_RemoveListener_m7083(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C" void UnityEventBase_Invoke_m7095 (UnityEventBase_t1301 * __this, ObjectU5BU5D_t34* ___parameters, const MethodInfo* method)
{
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m7092(__this, /*hidden argument*/NULL);
		InvokableCallList_t1299 * L_0 = (__this->___m_Calls_0);
		ObjectU5BU5D_t34* L_1 = ___parameters;
		NullCheck(L_0);
		InvokableCallList_Invoke_m7085(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral937;
extern "C" String_t* UnityEventBase_ToString_m7096 (UnityEventBase_t1301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral937 = il2cpp_codegen_string_literal_from_index(937);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Object_ToString_m8160(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m303(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2152(NULL /*static, unused*/, L_0, _stringLiteral937, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern const Il2CppType* Object_t_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEventBase_GetValidMethodInfo_m7097 (Object_t * __this /* static, unused */, Object_t * ___obj, String_t* ___functionName, TypeU5BU5D_t1218* ___argumentTypes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_0_0_0_var = il2cpp_codegen_type_from_index(0);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	MethodInfo_t * V_1 = {0};
	ParameterInfoU5BU5D_t1458* V_2 = {0};
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t1459 * V_5 = {0};
	ParameterInfoU5BU5D_t1458* V_6 = {0};
	int32_t V_7 = 0;
	Type_t * V_8 = {0};
	Type_t * V_9 = {0};
	{
		Object_t * L_0 = ___obj;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m303(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_008e;
	}

IL_000c:
	{
		Type_t * L_2 = V_0;
		String_t* L_3 = ___functionName;
		TypeU5BU5D_t1218* L_4 = ___argumentTypes;
		NullCheck(L_2);
		MethodInfo_t * L_5 = (MethodInfo_t *)VirtFuncInvoker5< MethodInfo_t *, String_t*, int32_t, Binder_t1449 *, TypeU5BU5D_t1218*, ParameterModifierU5BU5D_t1450* >::Invoke(48 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[]) */, L_2, L_3, ((int32_t)52), (Binder_t1449 *)NULL, L_4, (ParameterModifierU5BU5D_t1450*)(ParameterModifierU5BU5D_t1450*)NULL);
		V_1 = L_5;
		MethodInfo_t * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0087;
		}
	}
	{
		MethodInfo_t * L_7 = V_1;
		NullCheck(L_7);
		ParameterInfoU5BU5D_t1458* L_8 = (ParameterInfoU5BU5D_t1458*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1458* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_7);
		V_2 = L_8;
		V_3 = 1;
		V_4 = 0;
		ParameterInfoU5BU5D_t1458* L_9 = V_2;
		V_6 = L_9;
		V_7 = 0;
		goto IL_0074;
	}

IL_0036:
	{
		ParameterInfoU5BU5D_t1458* L_10 = V_6;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_5 = (*(ParameterInfo_t1459 **)(ParameterInfo_t1459 **)SZArrayLdElema(L_10, L_12, sizeof(ParameterInfo_t1459 *)));
		TypeU5BU5D_t1218* L_13 = ___argumentTypes;
		int32_t L_14 = V_4;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		V_8 = (*(Type_t **)(Type_t **)SZArrayLdElema(L_13, L_15, sizeof(Type_t *)));
		ParameterInfo_t1459 * L_16 = V_5;
		NullCheck(L_16);
		Type_t * L_17 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_16);
		V_9 = L_17;
		Type_t * L_18 = V_8;
		NullCheck(L_18);
		bool L_19 = (bool)VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Type::get_IsPrimitive() */, L_18);
		Type_t * L_20 = V_9;
		NullCheck(L_20);
		bool L_21 = (bool)VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Type::get_IsPrimitive() */, L_20);
		V_3 = ((((int32_t)L_19) == ((int32_t)L_21))? 1 : 0);
		bool L_22 = V_3;
		if (L_22)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_007f;
	}

IL_0068:
	{
		int32_t L_23 = V_4;
		V_4 = ((int32_t)((int32_t)L_23+(int32_t)1));
		int32_t L_24 = V_7;
		V_7 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_25 = V_7;
		ParameterInfoU5BU5D_t1458* L_26 = V_6;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_007f:
	{
		bool L_27 = V_3;
		if (!L_27)
		{
			goto IL_0087;
		}
	}
	{
		MethodInfo_t * L_28 = V_1;
		return L_28;
	}

IL_0087:
	{
		Type_t * L_29 = V_0;
		NullCheck(L_29);
		Type_t * L_30 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_29);
		V_0 = L_30;
	}

IL_008e:
	{
		Type_t * L_31 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Object_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_31) == ((Object_t*)(Type_t *)L_32)))
		{
			goto IL_00a4;
		}
	}
	{
		Type_t * L_33 = V_0;
		if (L_33)
		{
			goto IL_000c;
		}
	}

IL_00a4:
	{
		return (MethodInfo_t *)NULL;
	}
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern "C" void UnityEvent__ctor_m4107 (UnityEvent_t673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 0));
		UnityEventBase__ctor_m7086(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern TypeInfo* TypeU5BU5D_t1218_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEvent_FindMethod_Impl_m7098 (UnityEvent_t673 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeU5BU5D_t1218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(844);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___targetObj;
		String_t* L_1 = ___name;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m7097(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t1218*)SZArrayNew(TypeU5BU5D_t1218_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern TypeInfo* InvokableCall_t1294_il2cpp_TypeInfo_var;
extern "C" BaseInvokableCall_t1293 * UnityEvent_GetDelegate_m7099 (UnityEvent_t673 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvokableCall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(930);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___target;
		MethodInfo_t * L_1 = ___theFunction;
		InvokableCall_t1294 * L_2 = (InvokableCall_t1294 *)il2cpp_codegen_object_new (InvokableCall_t1294_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m7067(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C" void UnityEvent_Invoke_m4109 (UnityEvent_t673 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t34* L_0 = (__this->___m_InvokeArray_4);
		UnityEventBase_Invoke_m7095(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
extern "C" void UserAuthorizationDialog__ctor_m7100 (UserAuthorizationDialog_t1302 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::Start()
extern const Il2CppType* Texture2D_t33_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1138;
extern Il2CppCodeGenString* _stringLiteral1139;
extern "C" void UserAuthorizationDialog_Start_m7101 (UserAuthorizationDialog_t1302 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t33_0_0_0_var = il2cpp_codegen_type_from_index(39);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Texture2D_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		_stringLiteral1138 = il2cpp_codegen_string_literal_from_index(1138);
		_stringLiteral1139 = il2cpp_codegen_string_literal_from_index(1139);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Texture2D_t33_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_1 = Resources_GetBuiltinResource_m7394(NULL /*static, unused*/, L_0, _stringLiteral1138, /*hidden argument*/NULL);
		__this->___warningIcon_4 = ((Texture2D_t33 *)IsInstSealed(L_1, Texture2D_t33_il2cpp_TypeInfo_var));
		int32_t L_2 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) < ((int32_t)((int32_t)385))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_3 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)155))))
		{
			goto IL_004d;
		}
	}

IL_003d:
	{
		Debug_LogError_m302(NULL /*static, unused*/, _stringLiteral1139, /*hidden argument*/NULL);
		Application_ReplyToUserAuthorizationRequest_m7998(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_004d:
	{
		int32_t L_4 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_6 = {0};
		Rect__ctor_m387(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)192)))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_5/(int32_t)2))-(int32_t)((int32_t)77)))))), (385.0f), (155.0f), /*hidden argument*/NULL);
		__this->___windowRect_3 = L_6;
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
extern const Il2CppType* Texture2D_t33_0_0_0_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* GUISkin_t547_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t33_il2cpp_TypeInfo_var;
extern TypeInfo* WindowFunction_t548_il2cpp_TypeInfo_var;
extern const MethodInfo* UserAuthorizationDialog_DoUserAuthorizationDialog_m7103_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1140;
extern Il2CppCodeGenString* _stringLiteral1141;
extern Il2CppCodeGenString* _stringLiteral1142;
extern Il2CppCodeGenString* _stringLiteral1143;
extern Il2CppCodeGenString* _stringLiteral1144;
extern Il2CppCodeGenString* _stringLiteral1145;
extern Il2CppCodeGenString* _stringLiteral1146;
extern "C" void UserAuthorizationDialog_OnGUI_m7102 (UserAuthorizationDialog_t1302 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t33_0_0_0_var = il2cpp_codegen_type_from_index(39);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		GUISkin_t547_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Texture2D_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		WindowFunction_t548_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(286);
		UserAuthorizationDialog_DoUserAuthorizationDialog_m7103_MethodInfo_var = il2cpp_codegen_method_info_from_index(1065);
		_stringLiteral1140 = il2cpp_codegen_string_literal_from_index(1140);
		_stringLiteral1141 = il2cpp_codegen_string_literal_from_index(1141);
		_stringLiteral1142 = il2cpp_codegen_string_literal_from_index(1142);
		_stringLiteral1143 = il2cpp_codegen_string_literal_from_index(1143);
		_stringLiteral1144 = il2cpp_codegen_string_literal_from_index(1144);
		_stringLiteral1145 = il2cpp_codegen_string_literal_from_index(1145);
		_stringLiteral1146 = il2cpp_codegen_string_literal_from_index(1146);
		s_Il2CppMethodIntialized = true;
	}
	GUISkin_t547 * V_0 = {0};
	GUISkin_t547 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUISkin_t547 * L_0 = GUI_get_skin_m2469(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		ScriptableObject_t77 * L_1 = ScriptableObject_CreateInstance_m7388(NULL /*static, unused*/, _stringLiteral1140, /*hidden argument*/NULL);
		V_1 = ((GUISkin_t547 *)IsInstSealed(L_1, GUISkin_t547_il2cpp_TypeInfo_var));
		GUISkin_t547 * L_2 = V_1;
		NullCheck(L_2);
		GUIStyle_t184 * L_3 = GUISkin_get_box_m7633(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyleState_t504 * L_4 = GUIStyle_get_normal_m2291(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Texture2D_t33_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_6 = Resources_GetBuiltinResource_m7394(NULL /*static, unused*/, L_5, _stringLiteral1141, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyleState_set_background_m2472(L_4, ((Texture2D_t33 *)CastclassSealed(L_6, Texture2D_t33_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GUISkin_t547 * L_7 = V_1;
		NullCheck(L_7);
		GUIStyle_t184 * L_8 = GUISkin_get_box_m7633(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GUIStyleState_t504 * L_9 = GUIStyle_get_normal_m2291(L_8, /*hidden argument*/NULL);
		Color_t5  L_10 = {0};
		Color__ctor_m405(&L_10, (0.9f), (0.9f), (0.9f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		GUIStyleState_set_textColor_m2292(L_9, L_10, /*hidden argument*/NULL);
		GUISkin_t547 * L_11 = V_1;
		NullCheck(L_11);
		GUIStyle_t184 * L_12 = GUISkin_get_box_m7633(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		RectOffset_t780 * L_13 = GUIStyle_get_padding_m7728(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectOffset_set_left_m7701(L_13, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_14 = V_1;
		NullCheck(L_14);
		GUIStyle_t184 * L_15 = GUISkin_get_box_m7633(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		RectOffset_t780 * L_16 = GUIStyle_get_padding_m7728(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		RectOffset_set_right_m7703(L_16, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_17 = V_1;
		NullCheck(L_17);
		GUIStyle_t184 * L_18 = GUISkin_get_box_m7633(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		RectOffset_t780 * L_19 = GUIStyle_get_padding_m7728(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		RectOffset_set_top_m7704(L_19, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_20 = V_1;
		NullCheck(L_20);
		GUIStyle_t184 * L_21 = GUISkin_get_box_m7633(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		RectOffset_t780 * L_22 = GUIStyle_get_padding_m7728(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectOffset_set_bottom_m7706(L_22, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_23 = V_1;
		NullCheck(L_23);
		GUIStyle_t184 * L_24 = GUISkin_get_box_m7633(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		RectOffset_t780 * L_25 = GUIStyle_get_border_m7726(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		RectOffset_set_left_m7701(L_25, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_26 = V_1;
		NullCheck(L_26);
		GUIStyle_t184 * L_27 = GUISkin_get_box_m7633(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		RectOffset_t780 * L_28 = GUIStyle_get_border_m7726(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		RectOffset_set_right_m7703(L_28, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_29 = V_1;
		NullCheck(L_29);
		GUIStyle_t184 * L_30 = GUISkin_get_box_m7633(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectOffset_t780 * L_31 = GUIStyle_get_border_m7726(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		RectOffset_set_top_m7704(L_31, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_32 = V_1;
		NullCheck(L_32);
		GUIStyle_t184 * L_33 = GUISkin_get_box_m7633(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		RectOffset_t780 * L_34 = GUIStyle_get_border_m7726(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		RectOffset_set_bottom_m7706(L_34, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_35 = V_1;
		NullCheck(L_35);
		GUIStyle_t184 * L_36 = GUISkin_get_box_m7633(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		RectOffset_t780 * L_37 = GUIStyle_get_margin_m7727(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		RectOffset_set_left_m7701(L_37, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_38 = V_1;
		NullCheck(L_38);
		GUIStyle_t184 * L_39 = GUISkin_get_box_m7633(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		RectOffset_t780 * L_40 = GUIStyle_get_margin_m7727(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		RectOffset_set_right_m7703(L_40, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_41 = V_1;
		NullCheck(L_41);
		GUIStyle_t184 * L_42 = GUISkin_get_box_m7633(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		RectOffset_t780 * L_43 = GUIStyle_get_margin_m7727(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		RectOffset_set_top_m7704(L_43, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_44 = V_1;
		NullCheck(L_44);
		GUIStyle_t184 * L_45 = GUISkin_get_box_m7633(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		RectOffset_t780 * L_46 = GUIStyle_get_margin_m7727(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		RectOffset_set_bottom_m7706(L_46, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_47 = V_1;
		NullCheck(L_47);
		GUIStyle_t184 * L_48 = GUISkin_get_button_m2482(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		GUIStyleState_t504 * L_49 = GUIStyle_get_normal_m2291(L_48, /*hidden argument*/NULL);
		Type_t * L_50 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Texture2D_t33_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_51 = Resources_GetBuiltinResource_m7394(NULL /*static, unused*/, L_50, _stringLiteral1142, /*hidden argument*/NULL);
		NullCheck(L_49);
		GUIStyleState_set_background_m2472(L_49, ((Texture2D_t33 *)CastclassSealed(L_51, Texture2D_t33_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GUISkin_t547 * L_52 = V_1;
		NullCheck(L_52);
		GUIStyle_t184 * L_53 = GUISkin_get_button_m2482(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		GUIStyleState_t504 * L_54 = GUIStyle_get_normal_m2291(L_53, /*hidden argument*/NULL);
		Color_t5  L_55 = {0};
		Color__ctor_m405(&L_55, (0.9f), (0.9f), (0.9f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_54);
		GUIStyleState_set_textColor_m2292(L_54, L_55, /*hidden argument*/NULL);
		GUISkin_t547 * L_56 = V_1;
		NullCheck(L_56);
		GUIStyle_t184 * L_57 = GUISkin_get_button_m2482(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		GUIStyleState_t504 * L_58 = GUIStyle_get_hover_m7718(L_57, /*hidden argument*/NULL);
		Type_t * L_59 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Texture2D_t33_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_60 = Resources_GetBuiltinResource_m7394(NULL /*static, unused*/, L_59, _stringLiteral1143, /*hidden argument*/NULL);
		NullCheck(L_58);
		GUIStyleState_set_background_m2472(L_58, ((Texture2D_t33 *)CastclassSealed(L_60, Texture2D_t33_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GUISkin_t547 * L_61 = V_1;
		NullCheck(L_61);
		GUIStyle_t184 * L_62 = GUISkin_get_button_m2482(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		GUIStyleState_t504 * L_63 = GUIStyle_get_hover_m7718(L_62, /*hidden argument*/NULL);
		Color_t5  L_64 = Color_get_white_m283(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_63);
		GUIStyleState_set_textColor_m2292(L_63, L_64, /*hidden argument*/NULL);
		GUISkin_t547 * L_65 = V_1;
		NullCheck(L_65);
		GUIStyle_t184 * L_66 = GUISkin_get_button_m2482(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		GUIStyleState_t504 * L_67 = GUIStyle_get_active_m7719(L_66, /*hidden argument*/NULL);
		Type_t * L_68 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Texture2D_t33_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_69 = Resources_GetBuiltinResource_m7394(NULL /*static, unused*/, L_68, _stringLiteral1144, /*hidden argument*/NULL);
		NullCheck(L_67);
		GUIStyleState_set_background_m2472(L_67, ((Texture2D_t33 *)CastclassSealed(L_69, Texture2D_t33_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GUISkin_t547 * L_70 = V_1;
		NullCheck(L_70);
		GUIStyle_t184 * L_71 = GUISkin_get_button_m2482(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		GUIStyleState_t504 * L_72 = GUIStyle_get_active_m7719(L_71, /*hidden argument*/NULL);
		Color_t5  L_73 = {0};
		Color__ctor_m405(&L_73, (0.9f), (0.9f), (0.9f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_72);
		GUIStyleState_set_textColor_m2292(L_72, L_73, /*hidden argument*/NULL);
		GUISkin_t547 * L_74 = V_1;
		NullCheck(L_74);
		GUIStyle_t184 * L_75 = GUISkin_get_button_m2482(L_74, /*hidden argument*/NULL);
		NullCheck(L_75);
		RectOffset_t780 * L_76 = GUIStyle_get_border_m7726(L_75, /*hidden argument*/NULL);
		NullCheck(L_76);
		RectOffset_set_left_m7701(L_76, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_77 = V_1;
		NullCheck(L_77);
		GUIStyle_t184 * L_78 = GUISkin_get_button_m2482(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		RectOffset_t780 * L_79 = GUIStyle_get_border_m7726(L_78, /*hidden argument*/NULL);
		NullCheck(L_79);
		RectOffset_set_right_m7703(L_79, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_80 = V_1;
		NullCheck(L_80);
		GUIStyle_t184 * L_81 = GUISkin_get_button_m2482(L_80, /*hidden argument*/NULL);
		NullCheck(L_81);
		RectOffset_t780 * L_82 = GUIStyle_get_border_m7726(L_81, /*hidden argument*/NULL);
		NullCheck(L_82);
		RectOffset_set_top_m7704(L_82, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_83 = V_1;
		NullCheck(L_83);
		GUIStyle_t184 * L_84 = GUISkin_get_button_m2482(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		RectOffset_t780 * L_85 = GUIStyle_get_border_m7726(L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		RectOffset_set_bottom_m7706(L_85, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_86 = V_1;
		NullCheck(L_86);
		GUIStyle_t184 * L_87 = GUISkin_get_button_m2482(L_86, /*hidden argument*/NULL);
		NullCheck(L_87);
		RectOffset_t780 * L_88 = GUIStyle_get_padding_m7728(L_87, /*hidden argument*/NULL);
		NullCheck(L_88);
		RectOffset_set_left_m7701(L_88, 8, /*hidden argument*/NULL);
		GUISkin_t547 * L_89 = V_1;
		NullCheck(L_89);
		GUIStyle_t184 * L_90 = GUISkin_get_button_m2482(L_89, /*hidden argument*/NULL);
		NullCheck(L_90);
		RectOffset_t780 * L_91 = GUIStyle_get_padding_m7728(L_90, /*hidden argument*/NULL);
		NullCheck(L_91);
		RectOffset_set_right_m7703(L_91, 8, /*hidden argument*/NULL);
		GUISkin_t547 * L_92 = V_1;
		NullCheck(L_92);
		GUIStyle_t184 * L_93 = GUISkin_get_button_m2482(L_92, /*hidden argument*/NULL);
		NullCheck(L_93);
		RectOffset_t780 * L_94 = GUIStyle_get_padding_m7728(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		RectOffset_set_top_m7704(L_94, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_95 = V_1;
		NullCheck(L_95);
		GUIStyle_t184 * L_96 = GUISkin_get_button_m2482(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		RectOffset_t780 * L_97 = GUIStyle_get_padding_m7728(L_96, /*hidden argument*/NULL);
		NullCheck(L_97);
		RectOffset_set_bottom_m7706(L_97, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_98 = V_1;
		NullCheck(L_98);
		GUIStyle_t184 * L_99 = GUISkin_get_button_m2482(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		RectOffset_t780 * L_100 = GUIStyle_get_margin_m7727(L_99, /*hidden argument*/NULL);
		NullCheck(L_100);
		RectOffset_set_left_m7701(L_100, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_101 = V_1;
		NullCheck(L_101);
		GUIStyle_t184 * L_102 = GUISkin_get_button_m2482(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		RectOffset_t780 * L_103 = GUIStyle_get_margin_m7727(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		RectOffset_set_right_m7703(L_103, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_104 = V_1;
		NullCheck(L_104);
		GUIStyle_t184 * L_105 = GUISkin_get_button_m2482(L_104, /*hidden argument*/NULL);
		NullCheck(L_105);
		RectOffset_t780 * L_106 = GUIStyle_get_margin_m7727(L_105, /*hidden argument*/NULL);
		NullCheck(L_106);
		RectOffset_set_top_m7704(L_106, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_107 = V_1;
		NullCheck(L_107);
		GUIStyle_t184 * L_108 = GUISkin_get_button_m2482(L_107, /*hidden argument*/NULL);
		NullCheck(L_108);
		RectOffset_t780 * L_109 = GUIStyle_get_margin_m7727(L_108, /*hidden argument*/NULL);
		NullCheck(L_109);
		RectOffset_set_bottom_m7706(L_109, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_110 = V_1;
		NullCheck(L_110);
		GUIStyle_t184 * L_111 = GUISkin_get_label_m7635(L_110, /*hidden argument*/NULL);
		NullCheck(L_111);
		GUIStyleState_t504 * L_112 = GUIStyle_get_normal_m2291(L_111, /*hidden argument*/NULL);
		Color_t5  L_113 = {0};
		Color__ctor_m405(&L_113, (0.9f), (0.9f), (0.9f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_112);
		GUIStyleState_set_textColor_m2292(L_112, L_113, /*hidden argument*/NULL);
		GUISkin_t547 * L_114 = V_1;
		NullCheck(L_114);
		GUIStyle_t184 * L_115 = GUISkin_get_label_m7635(L_114, /*hidden argument*/NULL);
		NullCheck(L_115);
		RectOffset_t780 * L_116 = GUIStyle_get_padding_m7728(L_115, /*hidden argument*/NULL);
		NullCheck(L_116);
		RectOffset_set_left_m7701(L_116, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_117 = V_1;
		NullCheck(L_117);
		GUIStyle_t184 * L_118 = GUISkin_get_label_m7635(L_117, /*hidden argument*/NULL);
		NullCheck(L_118);
		RectOffset_t780 * L_119 = GUIStyle_get_padding_m7728(L_118, /*hidden argument*/NULL);
		NullCheck(L_119);
		RectOffset_set_right_m7703(L_119, 6, /*hidden argument*/NULL);
		GUISkin_t547 * L_120 = V_1;
		NullCheck(L_120);
		GUIStyle_t184 * L_121 = GUISkin_get_label_m7635(L_120, /*hidden argument*/NULL);
		NullCheck(L_121);
		RectOffset_t780 * L_122 = GUIStyle_get_padding_m7728(L_121, /*hidden argument*/NULL);
		NullCheck(L_122);
		RectOffset_set_top_m7704(L_122, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_123 = V_1;
		NullCheck(L_123);
		GUIStyle_t184 * L_124 = GUISkin_get_label_m7635(L_123, /*hidden argument*/NULL);
		NullCheck(L_124);
		RectOffset_t780 * L_125 = GUIStyle_get_padding_m7728(L_124, /*hidden argument*/NULL);
		NullCheck(L_125);
		RectOffset_set_bottom_m7706(L_125, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_126 = V_1;
		NullCheck(L_126);
		GUIStyle_t184 * L_127 = GUISkin_get_label_m7635(L_126, /*hidden argument*/NULL);
		NullCheck(L_127);
		RectOffset_t780 * L_128 = GUIStyle_get_margin_m7727(L_127, /*hidden argument*/NULL);
		NullCheck(L_128);
		RectOffset_set_left_m7701(L_128, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_129 = V_1;
		NullCheck(L_129);
		GUIStyle_t184 * L_130 = GUISkin_get_label_m7635(L_129, /*hidden argument*/NULL);
		NullCheck(L_130);
		RectOffset_t780 * L_131 = GUIStyle_get_margin_m7727(L_130, /*hidden argument*/NULL);
		NullCheck(L_131);
		RectOffset_set_right_m7703(L_131, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_132 = V_1;
		NullCheck(L_132);
		GUIStyle_t184 * L_133 = GUISkin_get_label_m7635(L_132, /*hidden argument*/NULL);
		NullCheck(L_133);
		RectOffset_t780 * L_134 = GUIStyle_get_margin_m7727(L_133, /*hidden argument*/NULL);
		NullCheck(L_134);
		RectOffset_set_top_m7704(L_134, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_135 = V_1;
		NullCheck(L_135);
		GUIStyle_t184 * L_136 = GUISkin_get_label_m7635(L_135, /*hidden argument*/NULL);
		NullCheck(L_136);
		RectOffset_t780 * L_137 = GUIStyle_get_margin_m7727(L_136, /*hidden argument*/NULL);
		NullCheck(L_137);
		RectOffset_set_bottom_m7706(L_137, 4, /*hidden argument*/NULL);
		GUISkin_t547 * L_138 = V_1;
		NullCheck(L_138);
		GUIStyle_t184 * L_139 = GUISkin_get_label_m7635(L_138, /*hidden argument*/NULL);
		NullCheck(L_139);
		GUIStyle_set_alignment_m2295(L_139, 0, /*hidden argument*/NULL);
		GUISkin_t547 * L_140 = V_1;
		NullCheck(L_140);
		GUIStyle_t184 * L_141 = GUISkin_get_window_m2470(L_140, /*hidden argument*/NULL);
		NullCheck(L_141);
		GUIStyleState_t504 * L_142 = GUIStyle_get_normal_m2291(L_141, /*hidden argument*/NULL);
		Type_t * L_143 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Texture2D_t33_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_144 = Resources_GetBuiltinResource_m7394(NULL /*static, unused*/, L_143, _stringLiteral1145, /*hidden argument*/NULL);
		NullCheck(L_142);
		GUIStyleState_set_background_m2472(L_142, ((Texture2D_t33 *)CastclassSealed(L_144, Texture2D_t33_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GUISkin_t547 * L_145 = V_1;
		NullCheck(L_145);
		GUIStyle_t184 * L_146 = GUISkin_get_window_m2470(L_145, /*hidden argument*/NULL);
		NullCheck(L_146);
		GUIStyleState_t504 * L_147 = GUIStyle_get_normal_m2291(L_146, /*hidden argument*/NULL);
		Color_t5  L_148 = Color_get_white_m283(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_147);
		GUIStyleState_set_textColor_m2292(L_147, L_148, /*hidden argument*/NULL);
		GUISkin_t547 * L_149 = V_1;
		NullCheck(L_149);
		GUIStyle_t184 * L_150 = GUISkin_get_window_m2470(L_149, /*hidden argument*/NULL);
		NullCheck(L_150);
		RectOffset_t780 * L_151 = GUIStyle_get_border_m7726(L_150, /*hidden argument*/NULL);
		NullCheck(L_151);
		RectOffset_set_left_m7701(L_151, 8, /*hidden argument*/NULL);
		GUISkin_t547 * L_152 = V_1;
		NullCheck(L_152);
		GUIStyle_t184 * L_153 = GUISkin_get_window_m2470(L_152, /*hidden argument*/NULL);
		NullCheck(L_153);
		RectOffset_t780 * L_154 = GUIStyle_get_border_m7726(L_153, /*hidden argument*/NULL);
		NullCheck(L_154);
		RectOffset_set_right_m7703(L_154, 8, /*hidden argument*/NULL);
		GUISkin_t547 * L_155 = V_1;
		NullCheck(L_155);
		GUIStyle_t184 * L_156 = GUISkin_get_window_m2470(L_155, /*hidden argument*/NULL);
		NullCheck(L_156);
		RectOffset_t780 * L_157 = GUIStyle_get_border_m7726(L_156, /*hidden argument*/NULL);
		NullCheck(L_157);
		RectOffset_set_top_m7704(L_157, ((int32_t)18), /*hidden argument*/NULL);
		GUISkin_t547 * L_158 = V_1;
		NullCheck(L_158);
		GUIStyle_t184 * L_159 = GUISkin_get_window_m2470(L_158, /*hidden argument*/NULL);
		NullCheck(L_159);
		RectOffset_t780 * L_160 = GUIStyle_get_border_m7726(L_159, /*hidden argument*/NULL);
		NullCheck(L_160);
		RectOffset_set_bottom_m7706(L_160, 8, /*hidden argument*/NULL);
		GUISkin_t547 * L_161 = V_1;
		NullCheck(L_161);
		GUIStyle_t184 * L_162 = GUISkin_get_window_m2470(L_161, /*hidden argument*/NULL);
		NullCheck(L_162);
		RectOffset_t780 * L_163 = GUIStyle_get_padding_m7728(L_162, /*hidden argument*/NULL);
		NullCheck(L_163);
		RectOffset_set_left_m7701(L_163, 8, /*hidden argument*/NULL);
		GUISkin_t547 * L_164 = V_1;
		NullCheck(L_164);
		GUIStyle_t184 * L_165 = GUISkin_get_window_m2470(L_164, /*hidden argument*/NULL);
		NullCheck(L_165);
		RectOffset_t780 * L_166 = GUIStyle_get_padding_m7728(L_165, /*hidden argument*/NULL);
		NullCheck(L_166);
		RectOffset_set_right_m7703(L_166, 8, /*hidden argument*/NULL);
		GUISkin_t547 * L_167 = V_1;
		NullCheck(L_167);
		GUIStyle_t184 * L_168 = GUISkin_get_window_m2470(L_167, /*hidden argument*/NULL);
		NullCheck(L_168);
		RectOffset_t780 * L_169 = GUIStyle_get_padding_m7728(L_168, /*hidden argument*/NULL);
		NullCheck(L_169);
		RectOffset_set_top_m7704(L_169, ((int32_t)20), /*hidden argument*/NULL);
		GUISkin_t547 * L_170 = V_1;
		NullCheck(L_170);
		GUIStyle_t184 * L_171 = GUISkin_get_window_m2470(L_170, /*hidden argument*/NULL);
		NullCheck(L_171);
		RectOffset_t780 * L_172 = GUIStyle_get_padding_m7728(L_171, /*hidden argument*/NULL);
		NullCheck(L_172);
		RectOffset_set_bottom_m7706(L_172, 5, /*hidden argument*/NULL);
		GUISkin_t547 * L_173 = V_1;
		NullCheck(L_173);
		GUIStyle_t184 * L_174 = GUISkin_get_window_m2470(L_173, /*hidden argument*/NULL);
		NullCheck(L_174);
		GUIStyle_set_alignment_m2295(L_174, 1, /*hidden argument*/NULL);
		GUISkin_t547 * L_175 = V_1;
		NullCheck(L_175);
		GUIStyle_t184 * L_176 = GUISkin_get_window_m2470(L_175, /*hidden argument*/NULL);
		Vector2_t29  L_177 = {0};
		Vector2__ctor_m356(&L_177, (0.0f), (-18.0f), /*hidden argument*/NULL);
		NullCheck(L_176);
		GUIStyle_set_contentOffset_m7732(L_176, L_177, /*hidden argument*/NULL);
		GUISkin_t547 * L_178 = V_1;
		GUI_set_skin_m7510(NULL /*static, unused*/, L_178, /*hidden argument*/NULL);
		Rect_t30  L_179 = (__this->___windowRect_3);
		IntPtr_t L_180 = { (void*)UserAuthorizationDialog_DoUserAuthorizationDialog_m7103_MethodInfo_var };
		WindowFunction_t548 * L_181 = (WindowFunction_t548 *)il2cpp_codegen_object_new (WindowFunction_t548_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m2473(L_181, __this, L_180, /*hidden argument*/NULL);
		Rect_t30  L_182 = GUI_Window_m2701(NULL /*static, unused*/, 0, L_179, L_181, _stringLiteral1146, /*hidden argument*/NULL);
		__this->___windowRect_3 = L_182;
		GUISkin_t547 * L_183 = V_0;
		GUI_set_skin_m7510(NULL /*static, unused*/, L_183, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t184_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1147;
extern Il2CppCodeGenString* _stringLiteral1148;
extern Il2CppCodeGenString* _stringLiteral1149;
extern Il2CppCodeGenString* _stringLiteral1150;
extern Il2CppCodeGenString* _stringLiteral1151;
extern Il2CppCodeGenString* _stringLiteral1152;
extern Il2CppCodeGenString* _stringLiteral1153;
extern "C" void UserAuthorizationDialog_DoUserAuthorizationDialog_m7103 (UserAuthorizationDialog_t1302 * __this, int32_t ___windowID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		GUIStyle_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(287);
		_stringLiteral1147 = il2cpp_codegen_string_literal_from_index(1147);
		_stringLiteral1148 = il2cpp_codegen_string_literal_from_index(1148);
		_stringLiteral1149 = il2cpp_codegen_string_literal_from_index(1149);
		_stringLiteral1150 = il2cpp_codegen_string_literal_from_index(1150);
		_stringLiteral1151 = il2cpp_codegen_string_literal_from_index(1151);
		_stringLiteral1152 = il2cpp_codegen_string_literal_from_index(1152);
		_stringLiteral1153 = il2cpp_codegen_string_literal_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = Application_GetUserAuthorizationRequestMode_m8000(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t5  L_1 = {0};
		Color__ctor_m405(&L_1, (0.9f), (0.9f), (0.9f), (0.7f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_set_backgroundColor_m7513(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t184_il2cpp_TypeInfo_var);
		GUIStyle_t184 * L_2 = GUIStyle_op_Implicit_m7760(NULL /*static, unused*/, _stringLiteral1147, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m7539(NULL /*static, unused*/, L_2, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m2476(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t731 * L_3 = (__this->___warningIcon_4);
		GUILayout_Label_m7535(NULL /*static, unused*/, L_3, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m2478(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m2476(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_009f;
		}
	}
	{
		GUILayout_Label_m2477(NULL /*static, unused*/, _stringLiteral1148, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_009f:
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_00bb;
		}
	}
	{
		GUILayout_Label_m2477(NULL /*static, unused*/, _stringLiteral1149, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00bb:
	{
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_00d7;
		}
	}
	{
		GUILayout_Label_m2477(NULL /*static, unused*/, _stringLiteral1150, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00d7:
	{
		return;
	}

IL_00d8:
	{
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m2478(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m2486(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t5  L_7 = Color_get_white_m283(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_set_backgroundColor_m7513(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m2479(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_8 = GUILayout_Button_m2495(NULL /*static, unused*/, _stringLiteral1151, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0121;
		}
	}
	{
		Application_ReplyToUserAuthorizationRequest_m7998(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0121:
	{
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_9 = GUILayout_Button_m2495(NULL /*static, unused*/, _stringLiteral1152, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0142;
		}
	}
	{
		Application_ReplyToUserAuthorizationRequest_m7997(NULL /*static, unused*/, 1, 1, /*hidden argument*/NULL);
	}

IL_0142:
	{
		GUILayout_Space_m2475(NULL /*static, unused*/, (5.0f), /*hidden argument*/NULL);
		bool L_10 = GUILayout_Button_m2495(NULL /*static, unused*/, _stringLiteral1153, ((GUILayoutOptionU5BU5D_t550*)SZArrayNew(GUILayoutOptionU5BU5D_t550_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0167;
		}
	}
	{
		Application_ReplyToUserAuthorizationRequest_m7998(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0167:
	{
		GUILayout_EndHorizontal_m2486(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2480(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.WWWForm)
extern "C" void WWW__ctor_m2518 (WWW_t289 * __this, String_t* ___url, WWWForm_t293 * ___form, const MethodInfo* method)
{
	StringU5BU5D_t260* V_0 = {0};
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		WWWForm_t293 * L_0 = ___form;
		NullCheck(L_0);
		Hashtable_t19 * L_1 = WWWForm_get_headers_m2517(L_0, /*hidden argument*/NULL);
		StringU5BU5D_t260* L_2 = WWW_FlattenedHeadersFrom_m7104(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___url;
		WWWForm_t293 * L_4 = ___form;
		NullCheck(L_4);
		ByteU5BU5D_t119* L_5 = WWWForm_get_data_m7976(L_4, /*hidden argument*/NULL);
		StringU5BU5D_t260* L_6 = V_0;
		WWW_InitWWW_m7109(__this, L_3, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" void WWW__ctor_m2516 (WWW_t289 * __this, String_t* ___url, ByteU5BU5D_t119* ___postData, Dictionary_2_t288 * ___headers, const MethodInfo* method)
{
	StringU5BU5D_t260* V_0 = {0};
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		Dictionary_2_t288 * L_0 = ___headers;
		StringU5BU5D_t260* L_1 = WWW_FlattenedHeadersFrom_m7105(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___url;
		ByteU5BU5D_t119* L_3 = ___postData;
		StringU5BU5D_t260* L_4 = V_0;
		WWW_InitWWW_m7109(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.String[] UnityEngine.WWW::FlattenedHeadersFrom(System.Collections.Hashtable)
extern TypeInfo* StringU5BU5D_t260_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t35_il2cpp_TypeInfo_var;
extern TypeInfo* DictionaryEntry_t58_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern "C" StringU5BU5D_t260* WWW_FlattenedHeadersFrom_m7104 (Object_t * __this /* static, unused */, Hashtable_t19 * ___headers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		IEnumerator_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		DictionaryEntry_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t260* V_0 = {0};
	int32_t V_1 = 0;
	DictionaryEntry_t58  V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Hashtable_t19 * L_0 = ___headers;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (StringU5BU5D_t260*)NULL;
	}

IL_0008:
	{
		Hashtable_t19 * L_1 = ___headers;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_1);
		V_0 = ((StringU5BU5D_t260*)SZArrayNew(StringU5BU5D_t260_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_2*(int32_t)2))));
		V_1 = 0;
		Hashtable_t19 * L_3 = ___headers;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(30 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_3);
		V_3 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0056;
		}

IL_0024:
		{
			Object_t * L_5 = V_3;
			NullCheck(L_5);
			Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t35_il2cpp_TypeInfo_var, L_5);
			V_2 = ((*(DictionaryEntry_t58 *)((DictionaryEntry_t58 *)UnBox (L_6, DictionaryEntry_t58_il2cpp_TypeInfo_var))));
			StringU5BU5D_t260* L_7 = V_0;
			int32_t L_8 = V_1;
			int32_t L_9 = L_8;
			V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
			Object_t * L_10 = DictionaryEntry_get_Key_m421((&V_2), /*hidden argument*/NULL);
			NullCheck(L_10);
			String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_9);
			ArrayElementTypeCheck (L_7, L_11);
			*((String_t**)(String_t**)SZArrayLdElema(L_7, L_9, sizeof(String_t*))) = (String_t*)L_11;
			StringU5BU5D_t260* L_12 = V_0;
			int32_t L_13 = V_1;
			int32_t L_14 = L_13;
			V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
			Object_t * L_15 = DictionaryEntry_get_Value_m422((&V_2), /*hidden argument*/NULL);
			NullCheck(L_15);
			String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
			ArrayElementTypeCheck (L_12, L_16);
			*((String_t**)(String_t**)SZArrayLdElema(L_12, L_14, sizeof(String_t*))) = (String_t*)L_16;
		}

IL_0056:
		{
			Object_t * L_17 = V_3;
			NullCheck(L_17);
			bool L_18 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t35_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0024;
			}
		}

IL_0061:
		{
			IL2CPP_LEAVE(0x7B, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		{
			Object_t * L_19 = V_3;
			V_4 = ((Object_t *)IsInst(L_19, IDisposable_t42_il2cpp_TypeInfo_var));
			Object_t * L_20 = V_4;
			if (L_20)
			{
				goto IL_0073;
			}
		}

IL_0072:
		{
			IL2CPP_END_FINALLY(102)
		}

IL_0073:
		{
			Object_t * L_21 = V_4;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_21);
			IL2CPP_END_FINALLY(102)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_007b:
	{
		StringU5BU5D_t260* L_22 = V_0;
		return L_22;
	}
}
// System.String[] UnityEngine.WWW::FlattenedHeadersFrom(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* StringU5BU5D_t260_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t545_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2456_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2457_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2410_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2411_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2458_MethodInfo_var;
extern "C" StringU5BU5D_t260* WWW_FlattenedHeadersFrom_m7105 (Object_t * __this /* static, unused */, Dictionary_2_t288 * ___headers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		Enumerator_t545_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(276);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		Dictionary_2_GetEnumerator_m2456_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484013);
		Enumerator_get_Current_m2457_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484014);
		KeyValuePair_2_get_Key_m2410_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483975);
		KeyValuePair_2_get_Value_m2411_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483976);
		Enumerator_MoveNext_m2458_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484015);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t260* V_0 = {0};
	int32_t V_1 = 0;
	KeyValuePair_2_t287  V_2 = {0};
	Enumerator_t545  V_3 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t288 * L_0 = ___headers;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (StringU5BU5D_t260*)NULL;
	}

IL_0008:
	{
		Dictionary_2_t288 * L_1 = ___headers;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count() */, L_1);
		V_0 = ((StringU5BU5D_t260*)SZArrayNew(StringU5BU5D_t260_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_2*(int32_t)2))));
		V_1 = 0;
		Dictionary_2_t288 * L_3 = ___headers;
		NullCheck(L_3);
		Enumerator_t545  L_4 = Dictionary_2_GetEnumerator_m2456(L_3, /*hidden argument*/Dictionary_2_GetEnumerator_m2456_MethodInfo_var);
		V_3 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0024:
		{
			KeyValuePair_2_t287  L_5 = Enumerator_get_Current_m2457((&V_3), /*hidden argument*/Enumerator_get_Current_m2457_MethodInfo_var);
			V_2 = L_5;
			StringU5BU5D_t260* L_6 = V_0;
			int32_t L_7 = V_1;
			int32_t L_8 = L_7;
			V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
			String_t* L_9 = KeyValuePair_2_get_Key_m2410((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m2410_MethodInfo_var);
			NullCheck(L_9);
			String_t* L_10 = String_ToString_m8133(L_9, /*hidden argument*/NULL);
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
			ArrayElementTypeCheck (L_6, L_10);
			*((String_t**)(String_t**)SZArrayLdElema(L_6, L_8, sizeof(String_t*))) = (String_t*)L_10;
			StringU5BU5D_t260* L_11 = V_0;
			int32_t L_12 = V_1;
			int32_t L_13 = L_12;
			V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
			String_t* L_14 = KeyValuePair_2_get_Value_m2411((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m2411_MethodInfo_var);
			NullCheck(L_14);
			String_t* L_15 = String_ToString_m8133(L_14, /*hidden argument*/NULL);
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_13);
			ArrayElementTypeCheck (L_11, L_15);
			*((String_t**)(String_t**)SZArrayLdElema(L_11, L_13, sizeof(String_t*))) = (String_t*)L_15;
		}

IL_0052:
		{
			bool L_16 = Enumerator_MoveNext_m2458((&V_3), /*hidden argument*/Enumerator_MoveNext_m2458_MethodInfo_var);
			if (L_16)
			{
				goto IL_0024;
			}
		}

IL_005e:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0063);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		Enumerator_t545  L_17 = V_3;
		Enumerator_t545  L_18 = L_17;
		Object_t * L_19 = Box(Enumerator_t545_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_19);
		IL2CPP_END_FINALLY(99)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_006f:
	{
		StringU5BU5D_t260* L_20 = V_0;
		return L_20;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::ParseHTTPHeaderString(System.String)
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t288_il2cpp_TypeInfo_var;
extern TypeInfo* StringReader_t303_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2350_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1154;
extern Il2CppCodeGenString* _stringLiteral1155;
extern Il2CppCodeGenString* _stringLiteral1156;
extern Il2CppCodeGenString* _stringLiteral1114;
extern "C" Dictionary_2_t288 * WWW_ParseHTTPHeaderString_m7106 (Object_t * __this /* static, unused */, String_t* ___input, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		Dictionary_2_t288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(195);
		StringReader_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(308);
		Dictionary_2__ctor_m2350_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		_stringLiteral1154 = il2cpp_codegen_string_literal_from_index(1154);
		_stringLiteral1155 = il2cpp_codegen_string_literal_from_index(1155);
		_stringLiteral1156 = il2cpp_codegen_string_literal_from_index(1156);
		_stringLiteral1114 = il2cpp_codegen_string_literal_from_index(1114);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t288 * V_0 = {0};
	StringReader_t303 * V_1 = {0};
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	String_t* V_5 = {0};
	String_t* V_6 = {0};
	{
		String_t* L_0 = ___input;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t513 * L_1 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_1, _stringLiteral1154, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Dictionary_2_t288 * L_2 = (Dictionary_2_t288 *)il2cpp_codegen_object_new (Dictionary_2_t288_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2350(L_2, /*hidden argument*/Dictionary_2__ctor_m2350_MethodInfo_var);
		V_0 = L_2;
		String_t* L_3 = ___input;
		StringReader_t303 * L_4 = (StringReader_t303 *)il2cpp_codegen_object_new (StringReader_t303_il2cpp_TypeInfo_var);
		StringReader__ctor_m2523(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
	}

IL_0020:
	{
		StringReader_t303 * L_5 = V_1;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.IO.StringReader::ReadLine() */, L_5);
		V_3 = L_6;
		String_t* L_7 = V_3;
		if (L_7)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_00a2;
	}

IL_0032:
	{
		int32_t L_8 = V_2;
		int32_t L_9 = L_8;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		if (L_9)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_10 = V_3;
		NullCheck(L_10);
		bool L_11 = String_StartsWith_m2414(L_10, _stringLiteral1155, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005d;
		}
	}
	{
		Dictionary_2_t288 * L_12 = V_0;
		String_t* L_13 = V_3;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_12, _stringLiteral1156, L_13);
		goto IL_0020;
	}

IL_005d:
	{
		String_t* L_14 = V_3;
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m2278(L_14, _stringLiteral1114, /*hidden argument*/NULL);
		V_4 = L_15;
		int32_t L_16 = V_4;
		if ((!(((uint32_t)L_16) == ((uint32_t)(-1)))))
		{
			goto IL_0077;
		}
	}
	{
		goto IL_0020;
	}

IL_0077:
	{
		String_t* L_17 = V_3;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m411(L_17, 0, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = String_ToUpper_m8161(L_19, /*hidden argument*/NULL);
		V_5 = L_20;
		String_t* L_21 = V_3;
		int32_t L_22 = V_4;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m2415(L_21, ((int32_t)((int32_t)L_22+(int32_t)2)), /*hidden argument*/NULL);
		V_6 = L_23;
		Dictionary_2_t288 * L_24 = V_0;
		String_t* L_25 = V_5;
		String_t* L_26 = V_6;
		NullCheck(L_24);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_24, L_25, L_26);
		goto IL_0020;
	}

IL_00a2:
	{
		Dictionary_2_t288 * L_27 = V_0;
		return L_27;
	}
}
// System.Void UnityEngine.WWW::Dispose()
extern "C" void WWW_Dispose_m2520 (WWW_t289 * __this, const MethodInfo* method)
{
	{
		WWW_DestroyWWW_m7108(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Finalize()
extern "C" void WWW_Finalize_m7107 (WWW_t289 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		WWW_DestroyWWW_m7108(__this, 0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
extern "C" void WWW_DestroyWWW_m7108 (WWW_t289 * __this, bool ___cancel, const MethodInfo* method)
{
	typedef void (*WWW_DestroyWWW_m7108_ftn) (WWW_t289 *, bool);
	static WWW_DestroyWWW_m7108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_DestroyWWW_m7108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::DestroyWWW(System.Boolean)");
	_il2cpp_icall_func(__this, ___cancel);
}
// System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
extern "C" void WWW_InitWWW_m7109 (WWW_t289 * __this, String_t* ___url, ByteU5BU5D_t119* ___postData, StringU5BU5D_t260* ___iHeaders, const MethodInfo* method)
{
	typedef void (*WWW_InitWWW_m7109_ftn) (WWW_t289 *, String_t*, ByteU5BU5D_t119*, StringU5BU5D_t260*);
	static WWW_InitWWW_m7109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_InitWWW_m7109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])");
	_il2cpp_icall_func(__this, ___url, ___postData, ___iHeaders);
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::get_responseHeaders()
extern TypeInfo* UnityException_t853_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1157;
extern "C" Dictionary_2_t288 * WWW_get_responseHeaders_m7110 (WWW_t289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityException_t853_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(548);
		_stringLiteral1157 = il2cpp_codegen_string_literal_from_index(1157);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = WWW_get_isDone_m7116(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityException_t853 * L_1 = (UnityException_t853 *)il2cpp_codegen_object_new (UnityException_t853_il2cpp_TypeInfo_var);
		UnityException__ctor_m7018(L_1, _stringLiteral1157, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		String_t* L_2 = WWW_get_responseHeadersString_m7111(__this, /*hidden argument*/NULL);
		Dictionary_2_t288 * L_3 = WWW_ParseHTTPHeaderString_m7106(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.WWW::get_responseHeadersString()
extern "C" String_t* WWW_get_responseHeadersString_m7111 (WWW_t289 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_responseHeadersString_m7111_ftn) (WWW_t289 *);
	static WWW_get_responseHeadersString_m7111_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_responseHeadersString_m7111_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_responseHeadersString()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_text()
extern TypeInfo* UnityException_t853_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1158;
extern "C" String_t* WWW_get_text_m2500 (WWW_t289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityException_t853_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(548);
		_stringLiteral1158 = il2cpp_codegen_string_literal_from_index(1158);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = WWW_get_isDone_m7116(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityException_t853 * L_1 = (UnityException_t853 *)il2cpp_codegen_object_new (UnityException_t853_il2cpp_TypeInfo_var);
		UnityException__ctor_m7018(L_1, _stringLiteral1158, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Encoding_t1442 * L_2 = WWW_GetTextEncoder_m7113(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t119* L_3 = WWW_get_bytes_m7114(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t119* L_4 = WWW_get_bytes_m7114(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		NullCheck(L_2);
		String_t* L_5 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, (((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))));
		return L_5;
	}
}
// System.Text.Encoding UnityEngine.WWW::get_DefaultEncoding()
extern TypeInfo* Encoding_t1442_il2cpp_TypeInfo_var;
extern "C" Encoding_t1442 * WWW_get_DefaultEncoding_m7112 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
		Encoding_t1442 * L_0 = Encoding_get_ASCII_m8162(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Text.Encoding UnityEngine.WWW::GetTextEncoder()
extern TypeInfo* CharU5BU5D_t493_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1159;
extern Il2CppCodeGenString* _stringLiteral1160;
extern Il2CppCodeGenString* _stringLiteral1161;
extern Il2CppCodeGenString* _stringLiteral878;
extern "C" Encoding_t1442 * WWW_GetTextEncoder_m7113 (WWW_t289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(66);
		Encoding_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1159 = il2cpp_codegen_string_literal_from_index(1159);
		_stringLiteral1160 = il2cpp_codegen_string_literal_from_index(1160);
		_stringLiteral1161 = il2cpp_codegen_string_literal_from_index(1161);
		_stringLiteral878 = il2cpp_codegen_string_literal_from_index(878);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	Encoding_t1442 * V_5 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
		Dictionary_2_t288 * L_0 = WWW_get_responseHeaders_m7110(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_0, _stringLiteral1159, (&V_0));
		if (!L_1)
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m8163(L_2, _stringLiteral1160, 5, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) <= ((int32_t)(-1))))
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = String_IndexOf_m8164(L_5, ((int32_t)61), L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) <= ((int32_t)(-1))))
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2415(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = String_Trim_m8134(L_11, /*hidden argument*/NULL);
		CharU5BU5D_t493* L_13 = ((CharU5BU5D_t493*)SZArrayNew(CharU5BU5D_t493_il2cpp_TypeInfo_var, 2));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_13, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)39);
		CharU5BU5D_t493* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_14, 1, sizeof(uint16_t))) = (uint16_t)((int32_t)34);
		NullCheck(L_12);
		String_t* L_15 = String_Trim_m8165(L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = String_Trim_m8134(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		String_t* L_17 = V_3;
		NullCheck(L_17);
		int32_t L_18 = String_IndexOf_m2527(L_17, ((int32_t)59), /*hidden argument*/NULL);
		V_4 = L_18;
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) <= ((int32_t)(-1))))
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_20 = V_3;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		String_t* L_22 = String_Substring_m411(L_20, 0, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_23 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
			Encoding_t1442 * L_24 = Encoding_GetEncoding_m8166(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			V_5 = L_24;
			goto IL_00b6;
		}

IL_0090:
		{
			; // IL_0090: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t40 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t40_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0095;
		throw e;
	}

CATCH_0095:
	{ // begin catch(System.Exception)
		String_t* L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m2152(NULL /*static, unused*/, _stringLiteral1161, L_25, _stringLiteral878, /*hidden argument*/NULL);
		Debug_Log_m2193(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		goto IL_00b0;
	} // end catch (depth: 1)

IL_00b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
		Encoding_t1442 * L_27 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_27;
	}

IL_00b6:
	{
		Encoding_t1442 * L_28 = V_5;
		return L_28;
	}
}
// System.Byte[] UnityEngine.WWW::get_bytes()
extern "C" ByteU5BU5D_t119* WWW_get_bytes_m7114 (WWW_t289 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t119* (*WWW_get_bytes_m7114_ftn) (WWW_t289 *);
	static WWW_get_bytes_m7114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_bytes_m7114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_bytes()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_error()
extern "C" String_t* WWW_get_error_m2501 (WWW_t289 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_error_m2501_ftn) (WWW_t289 *);
	static WWW_get_error_m2501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_error_m2501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_error()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.WWW::GetTexture(System.Boolean)
extern "C" Texture2D_t33 * WWW_GetTexture_m7115 (WWW_t289 * __this, bool ___markNonReadable, const MethodInfo* method)
{
	typedef Texture2D_t33 * (*WWW_GetTexture_m7115_ftn) (WWW_t289 *, bool);
	static WWW_GetTexture_m7115_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_GetTexture_m7115_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::GetTexture(System.Boolean)");
	return _il2cpp_icall_func(__this, ___markNonReadable);
}
// UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern "C" Texture2D_t33 * WWW_get_texture_m2502 (WWW_t289 * __this, const MethodInfo* method)
{
	{
		Texture2D_t33 * L_0 = WWW_GetTexture_m7115(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.WWW::get_isDone()
extern "C" bool WWW_get_isDone_m7116 (WWW_t289 * __this, const MethodInfo* method)
{
	typedef bool (*WWW_get_isDone_m7116_ftn) (WWW_t289 *);
	static WWW_get_isDone_m7116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_isDone_m7116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C" void DefaultValueAttribute__ctor_m7117 (DefaultValueAttribute_t1303 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value;
		__this->___DefaultValue_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C" Object_t * DefaultValueAttribute_get_Value_m7118 (DefaultValueAttribute_t1303 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern TypeInfo* DefaultValueAttribute_t1303_il2cpp_TypeInfo_var;
extern "C" bool DefaultValueAttribute_Equals_m7119 (DefaultValueAttribute_t1303 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(942);
		s_Il2CppMethodIntialized = true;
	}
	DefaultValueAttribute_t1303 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		V_0 = ((DefaultValueAttribute_t1303 *)IsInstClass(L_0, DefaultValueAttribute_t1303_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t1303 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		DefaultValueAttribute_t1303 * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = DefaultValueAttribute_get_Value_m7118(L_3, /*hidden argument*/NULL);
		return ((((Object_t*)(Object_t *)L_4) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0024:
	{
		Object_t * L_5 = (__this->___DefaultValue_0);
		DefaultValueAttribute_t1303 * L_6 = V_0;
		NullCheck(L_6);
		Object_t * L_7 = DefaultValueAttribute_get_Value_m7118(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		return L_8;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern "C" int32_t DefaultValueAttribute_GetHashCode_m7120 (DefaultValueAttribute_t1303 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m8168(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_0012:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		return L_3;
	}
}
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern "C" void ExcludeFromDocsAttribute__ctor_m7121 (ExcludeFromDocsAttribute_t1304 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.iPhoneGeneration UnityEngine.iPhone::get_generation()
extern "C" int32_t iPhone_get_generation_m2277 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*iPhone_get_generation_m2277_ftn) ();
	static iPhone_get_generation_m2277_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (iPhone_get_generation_m2277_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iPhone::get_generation()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C" void FormerlySerializedAsAttribute__ctor_m7122 (FormerlySerializedAsAttribute_t1306 * __this, String_t* ___oldName, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName;
		__this->___m_oldName_0 = L_0;
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern TypeInfo* TypeInferenceRules_t1307_il2cpp_TypeInfo_var;
extern "C" void TypeInferenceRuleAttribute__ctor_m7123 (TypeInferenceRuleAttribute_t1308 * __this, int32_t ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRules_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(943);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___rule;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(TypeInferenceRules_t1307_il2cpp_TypeInfo_var, &L_1);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_2);
		TypeInferenceRuleAttribute__ctor_m7124(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C" void TypeInferenceRuleAttribute__ctor_m7124 (TypeInferenceRuleAttribute_t1308 * __this, String_t* ___rule, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rule;
		__this->____rule_0 = L_0;
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C" String_t* TypeInferenceRuleAttribute_ToString_m7125 (TypeInferenceRuleAttribute_t1308 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->____rule_0);
		return L_0;
	}
}
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C" void GenericStack__ctor_m7126 (GenericStack_t1309 * __this, const MethodInfo* method)
{
	{
		Stack__ctor_m8169(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C" Vector3_t6  Physics_get_gravity_m2576 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Physics_INTERNAL_get_gravity_m7127(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C" void Physics_INTERNAL_get_gravity_m7127 (Object_t * __this /* static, unused */, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_get_gravity_m7127_ftn) (Vector3_t6 *);
	static Physics_INTERNAL_get_gravity_m7127_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_get_gravity_m7127_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Internal_Raycast_m7128 (Object_t * __this /* static, unused */, Vector3_t6  ___origin, Vector3_t6  ___direction, RaycastHit_t566 * ___hitInfo, float ___distance, int32_t ___layermask, const MethodInfo* method)
{
	{
		RaycastHit_t566 * L_0 = ___hitInfo;
		float L_1 = ___distance;
		int32_t L_2 = ___layermask;
		bool L_3 = Physics_INTERNAL_CALL_Internal_Raycast_m7129(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_INTERNAL_CALL_Internal_Raycast_m7129 (Object_t * __this /* static, unused */, Vector3_t6 * ___origin, Vector3_t6 * ___direction, RaycastHit_t566 * ___hitInfo, float ___distance, int32_t ___layermask, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m7129_ftn) (Vector3_t6 *, Vector3_t6 *, RaycastHit_t566 *, float, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m7129_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m7129_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___origin, ___direction, ___hitInfo, ___distance, ___layermask);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m7130 (Object_t * __this /* static, unused */, Vector3_t6  ___origin, Vector3_t6  ___direction, RaycastHit_t566 * ___hitInfo, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___origin;
		Vector3_t6  L_1 = ___direction;
		RaycastHit_t566 * L_2 = ___hitInfo;
		float L_3 = ___distance;
		int32_t L_4 = ___layerMask;
		bool L_5 = Physics_Internal_Raycast_m7128(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m4164 (Object_t * __this /* static, unused */, Ray_t567  ___ray, RaycastHit_t566 * ___hitInfo, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Ray_get_origin_m4085((&___ray), /*hidden argument*/NULL);
		Vector3_t6  L_1 = Ray_get_direction_m4086((&___ray), /*hidden argument*/NULL);
		RaycastHit_t566 * L_2 = ___hitInfo;
		float L_3 = ___distance;
		int32_t L_4 = ___layerMask;
		bool L_5 = Physics_Raycast_m7130(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t840* Physics_RaycastAll_m4098 (Object_t * __this /* static, unused */, Ray_t567  ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Ray_get_origin_m4085((&___ray), /*hidden argument*/NULL);
		Vector3_t6  L_1 = Ray_get_direction_m4086((&___ray), /*hidden argument*/NULL);
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		RaycastHitU5BU5D_t840* L_4 = Physics_RaycastAll_m7131(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t840* Physics_RaycastAll_m7131 (Object_t * __this /* static, unused */, Vector3_t6  ___origin, Vector3_t6  ___direction, float ___distance, int32_t ___layermask, const MethodInfo* method)
{
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layermask;
		RaycastHitU5BU5D_t840* L_2 = Physics_INTERNAL_CALL_RaycastAll_m7132(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t840* Physics_INTERNAL_CALL_RaycastAll_m7132 (Object_t * __this /* static, unused */, Vector3_t6 * ___origin, Vector3_t6 * ___direction, float ___distance, int32_t ___layermask, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t840* (*Physics_INTERNAL_CALL_RaycastAll_m7132_ftn) (Vector3_t6 *, Vector3_t6 *, float, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m7132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m7132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___origin, ___direction, ___distance, ___layermask);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C" void Rigidbody_MovePosition_m378 (Rigidbody_t50 * __this, Vector3_t6  ___position, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m7133(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_CALL_MovePosition_m7133 (Object_t * __this /* static, unused */, Rigidbody_t50 * ___self, Vector3_t6 * ___position, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m7133_ftn) (Rigidbody_t50 *, Vector3_t6 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m7133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m7133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___position);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C" void Rigidbody_MoveRotation_m382 (Rigidbody_t50 * __this, Quaternion_t51  ___rot, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m7134(NULL /*static, unused*/, __this, (&___rot), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C" void Rigidbody_INTERNAL_CALL_MoveRotation_m7134 (Object_t * __this /* static, unused */, Rigidbody_t50 * ___self, Quaternion_t51 * ___rot, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m7134_ftn) (Rigidbody_t50 *, Quaternion_t51 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m7134_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m7134_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self, ___rot);
}
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern "C" void Collider_set_enabled_m2717 (Collider_t316 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Collider_set_enabled_m2717_ftn) (Collider_t316 *, bool);
	static Collider_set_enabled_m2717_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_set_enabled_m2717_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C" Rigidbody_t50 * Collider_get_attachedRigidbody_m7135 (Collider_t316 * __this, const MethodInfo* method)
{
	typedef Rigidbody_t50 * (*Collider_get_attachedRigidbody_m7135_ftn) (Collider_t316 *);
	static Collider_get_attachedRigidbody_m7135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_attachedRigidbody_m7135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Collider::Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Collider_Internal_Raycast_m7136 (Object_t * __this /* static, unused */, Collider_t316 * ___col, Ray_t567  ___ray, RaycastHit_t566 * ___hitInfo, float ___distance, const MethodInfo* method)
{
	{
		Collider_t316 * L_0 = ___col;
		RaycastHit_t566 * L_1 = ___hitInfo;
		float L_2 = ___distance;
		bool L_3 = Collider_INTERNAL_CALL_Internal_Raycast_m7137(NULL /*static, unused*/, L_0, (&___ray), L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Collider_INTERNAL_CALL_Internal_Raycast_m7137 (Object_t * __this /* static, unused */, Collider_t316 * ___col, Ray_t567 * ___ray, RaycastHit_t566 * ___hitInfo, float ___distance, const MethodInfo* method)
{
	typedef bool (*Collider_INTERNAL_CALL_Internal_Raycast_m7137_ftn) (Collider_t316 *, Ray_t567 *, RaycastHit_t566 *, float);
	static Collider_INTERNAL_CALL_Internal_Raycast_m7137_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_INTERNAL_CALL_Internal_Raycast_m7137_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)");
	return _il2cpp_icall_func(___col, ___ray, ___hitInfo, ___distance);
}
// System.Boolean UnityEngine.Collider::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Collider_Raycast_m2545 (Collider_t316 * __this, Ray_t567  ___ray, RaycastHit_t566 * ___hitInfo, float ___distance, const MethodInfo* method)
{
	{
		Ray_t567  L_0 = ___ray;
		RaycastHit_t566 * L_1 = ___hitInfo;
		float L_2 = ___distance;
		bool L_3 = Collider_Internal_Raycast_m7136(NULL /*static, unused*/, __this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshCollider_set_sharedMesh_m6507 (MeshCollider_t918 * __this, Mesh_t601 * ___value, const MethodInfo* method)
{
	typedef void (*MeshCollider_set_sharedMesh_m6507_ftn) (MeshCollider_t918 *, Mesh_t601 *);
	static MeshCollider_set_sharedMesh_m6507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshCollider_set_sharedMesh_m6507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t6  RaycastHit_get_point_m2546 (RaycastHit_t566 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Point_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t6  RaycastHit_get_normal_m4103 (RaycastHit_t566 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Normal_1);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m4102 (RaycastHit_t566 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_3);
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t316 * RaycastHit_get_collider_m4101 (RaycastHit_t566 * __this, const MethodInfo* method)
{
	{
		Collider_t316 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C" Rigidbody_t50 * RaycastHit_get_rigidbody_m7138 (RaycastHit_t566 * __this, const MethodInfo* method)
{
	Rigidbody_t50 * G_B3_0 = {0};
	{
		Collider_t316 * L_0 = RaycastHit_get_collider_m4101(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider_t316 * L_2 = RaycastHit_get_collider_m4101(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t50 * L_3 = Collider_get_attachedRigidbody_m7135(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody_t50 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TerrainCollider::set_terrainData(UnityEngine.TerrainData)
extern "C" void TerrainCollider_set_terrainData_m7139 (TerrainCollider_t1312 * __this, TerrainData_t1331 * ___value, const MethodInfo* method)
{
	typedef void (*TerrainCollider_set_terrainData_m7139_ftn) (TerrainCollider_t1312 *, TerrainData_t1331 *);
	static TerrainCollider_set_terrainData_m7139_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainCollider_set_terrainData_m7139_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainCollider::set_terrainData(UnityEngine.TerrainData)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Physics2D::.cctor()
extern TypeInfo* List_1_t1313_il2cpp_TypeInfo_var;
extern TypeInfo* Physics2D_t837_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8170_MethodInfo_var;
extern "C" void Physics2D__cctor_m7140 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1313_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(945);
		Physics2D_t837_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		List_1__ctor_m8170_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484714);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1313 * L_0 = (List_1_t1313 *)il2cpp_codegen_object_new (List_1_t1313_il2cpp_TypeInfo_var);
		List_1__ctor_m8170(L_0, /*hidden argument*/List_1__ctor_m8170_MethodInfo_var);
		((Physics2D_t837_StaticFields*)Physics2D_t837_il2cpp_TypeInfo_var->static_fields)->___m_LastDisabledRigidbody2D_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t837_il2cpp_TypeInfo_var;
extern "C" void Physics2D_Internal_Raycast_m7141 (Object_t * __this /* static, unused */, Vector2_t29  ___origin, Vector2_t29  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t838 * ___raycastHit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t837_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = ___minDepth;
		float L_3 = ___maxDepth;
		RaycastHit2D_t838 * L_4 = ___raycastHit;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t837_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m7142(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m7142 (Object_t * __this /* static, unused */, Vector2_t29 * ___origin, Vector2_t29 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t838 * ___raycastHit, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m7142_ftn) (Vector2_t29 *, Vector2_t29 *, float, int32_t, float, float, RaycastHit2D_t838 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m7142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m7142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth, ___raycastHit);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t837_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t838  Physics2D_Raycast_m4165 (Object_t * __this /* static, unused */, Vector2_t29  ___origin, Vector2_t29  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t837_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t29  L_0 = ___origin;
		Vector2_t29  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t837_il2cpp_TypeInfo_var);
		RaycastHit2D_t838  L_6 = Physics2D_Raycast_m7143(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern TypeInfo* Physics2D_t837_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t838  Physics2D_Raycast_m7143 (Object_t * __this /* static, unused */, Vector2_t29  ___origin, Vector2_t29  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t837_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t838  V_0 = {0};
	{
		Vector2_t29  L_0 = ___origin;
		Vector2_t29  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = ___minDepth;
		float L_5 = ___maxDepth;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t837_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m7141(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t838  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t837_il2cpp_TypeInfo_var;
extern "C" RaycastHit2DU5BU5D_t835* Physics2D_RaycastAll_m4087 (Object_t * __this /* static, unused */, Vector2_t29  ___origin, Vector2_t29  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t837_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t837_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t835* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m7144(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C" RaycastHit2DU5BU5D_t835* Physics2D_INTERNAL_CALL_RaycastAll_m7144 (Object_t * __this /* static, unused */, Vector2_t29 * ___origin, Vector2_t29 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t835* (*Physics2D_INTERNAL_CALL_RaycastAll_m7144_ftn) (Vector2_t29 *, Vector2_t29 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_RaycastAll_m7144_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_RaycastAll_m7144_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth);
}
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern TypeInfo* Physics2D_t837_il2cpp_TypeInfo_var;
extern "C" int32_t Physics2D_GetRayIntersectionNonAlloc_m7145 (Object_t * __this /* static, unused */, Ray_t567  ___ray, RaycastHit2DU5BU5D_t835* ___results, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t837_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		s_Il2CppMethodIntialized = true;
	}
	{
		RaycastHit2DU5BU5D_t835* L_0 = ___results;
		float L_1 = ___distance;
		int32_t L_2 = ___layerMask;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t837_il2cpp_TypeInfo_var);
		int32_t L_3 = Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc_m7146(NULL /*static, unused*/, (&___ray), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionNonAlloc(UnityEngine.Ray&,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern "C" int32_t Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc_m7146 (Object_t * __this /* static, unused */, Ray_t567 * ___ray, RaycastHit2DU5BU5D_t835* ___results, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	typedef int32_t (*Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc_m7146_ftn) (Ray_t567 *, RaycastHit2DU5BU5D_t835*, float, int32_t);
	static Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc_m7146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc_m7146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionNonAlloc(UnityEngine.Ray&,UnityEngine.RaycastHit2D[],System.Single,System.Int32)");
	return _il2cpp_icall_func(___ray, ___results, ___distance, ___layerMask);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" Vector2_t29  RaycastHit2D_get_point_m4091 (RaycastHit2D_t838 * __this, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = (__this->___m_Point_1);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" Vector2_t29  RaycastHit2D_get_normal_m4092 (RaycastHit2D_t838 * __this, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = (__this->___m_Normal_2);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" float RaycastHit2D_get_fraction_m4166 (RaycastHit2D_t838 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Fraction_4);
		return L_0;
	}
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t839 * RaycastHit2D_get_collider_m4088 (RaycastHit2D_t838 * __this, const MethodInfo* method)
{
	{
		Collider2D_t839 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" Rigidbody2D_t1314 * RaycastHit2D_get_rigidbody_m7147 (RaycastHit2D_t838 * __this, const MethodInfo* method)
{
	Rigidbody2D_t1314 * G_B3_0 = {0};
	{
		Collider2D_t839 * L_0 = RaycastHit2D_get_collider_m4088(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t839 * L_2 = RaycastHit2D_get_collider_m4088(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t1314 * L_3 = Collider2D_get_attachedRigidbody_m7148(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t1314 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" Transform_t25 * RaycastHit2D_get_transform_m4090 (RaycastHit2D_t838 * __this, const MethodInfo* method)
{
	Rigidbody2D_t1314 * V_0 = {0};
	{
		Rigidbody2D_t1314 * L_0 = RaycastHit2D_get_rigidbody_m7147(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t1314 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m395(NULL /*static, unused*/, L_1, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t1314 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t25 * L_4 = Component_get_transform_m416(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t839 * L_5 = RaycastHit2D_get_collider_m4088(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m395(NULL /*static, unused*/, L_5, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t839 * L_7 = RaycastHit2D_get_collider_m4088(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t25 * L_8 = Component_get_transform_m416(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t25 *)NULL;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" Rigidbody2D_t1314 * Collider2D_get_attachedRigidbody_m7148 (Collider2D_t839 * __this, const MethodInfo* method)
{
	typedef Rigidbody2D_t1314 * (*Collider2D_get_attachedRigidbody_m7148_ftn) (Collider2D_t839 *);
	static Collider2D_get_attachedRigidbody_m7148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m7148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMReaderCallback__ctor_m7149 (PCMReaderCallback_t1316 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C" void PCMReaderCallback_Invoke_m7150 (PCMReaderCallback_t1316 * __this, SingleU5BU5D_t23* ___data, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMReaderCallback_Invoke_m7150((PCMReaderCallback_t1316 *)__this->___prev_9,___data, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, SingleU5BU5D_t23* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, SingleU5BU5D_t23* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t1316(Il2CppObject* delegate, SingleU5BU5D_t23* ___data)
{
	typedef void (STDCALL *native_function_ptr_type)(float*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___data' to native representation
	float* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data);

	// Native function invocation
	_il2cpp_pinvoke_func(____data_marshaled);

	// Marshaling cleanup of parameter '___data' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C" Object_t * PCMReaderCallback_BeginInvoke_m7151 (PCMReaderCallback_t1316 * __this, SingleU5BU5D_t23* ___data, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMReaderCallback_EndInvoke_m7152 (PCMReaderCallback_t1316 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMSetPositionCallback__ctor_m7153 (PCMSetPositionCallback_t1317 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C" void PCMSetPositionCallback_Invoke_m7154 (PCMSetPositionCallback_t1317 * __this, int32_t ___position, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMSetPositionCallback_Invoke_m7154((PCMSetPositionCallback_t1317 *)__this->___prev_9,___position, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t1317(Il2CppObject* delegate, int32_t ___position)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___position' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___position);

	// Marshaling cleanup of parameter '___position' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern "C" Object_t * PCMSetPositionCallback_BeginInvoke_m7155 (PCMSetPositionCallback_t1317 * __this, int32_t ___position, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t59_il2cpp_TypeInfo_var, &___position);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMSetPositionCallback_EndInvoke_m7156 (PCMSetPositionCallback_t1317 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Single UnityEngine.AudioClip::get_length()
extern "C" float AudioClip_get_length_m361 (AudioClip_t31 * __this, const MethodInfo* method)
{
	typedef float (*AudioClip_get_length_m361_ftn) (AudioClip_t31 *);
	static AudioClip_get_length_m361_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_length_m361_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m7157 (AudioClip_t31 * __this, SingleU5BU5D_t23* ___data, const MethodInfo* method)
{
	{
		PCMReaderCallback_t1316 * L_0 = (__this->___m_PCMReaderCallback_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t1316 * L_1 = (__this->___m_PCMReaderCallback_1);
		SingleU5BU5D_t23* L_2 = ___data;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m7150(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m7158 (AudioClip_t31 * __this, int32_t ___position, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t1317 * L_0 = (__this->___m_PCMSetPositionCallback_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t1317 * L_1 = (__this->___m_PCMSetPositionCallback_2);
		int32_t L_2 = ___position;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m7154(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.AudioSource::get_volume()
extern "C" float AudioSource_get_volume_m323 (AudioSource_t20 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_volume_m323_ftn) (AudioSource_t20 *);
	static AudioSource_get_volume_m323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_volume_m323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_volume()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C" void AudioSource_set_volume_m325 (AudioSource_t20 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_volume_m325_ftn) (AudioSource_t20 *, float);
	static AudioSource_set_volume_m325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_volume_m325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_volume(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.AudioSource::get_pitch()
extern "C" float AudioSource_get_pitch_m324 (AudioSource_t20 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_pitch_m324_ftn) (AudioSource_t20 *);
	static AudioSource_get_pitch_m324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_pitch_m324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_pitch()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C" void AudioSource_set_pitch_m326 (AudioSource_t20 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_pitch_m326_ftn) (AudioSource_t20 *, float);
	static AudioSource_set_pitch_m326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_pitch_m326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_pitch(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C" AudioClip_t31 * AudioSource_get_clip_m360 (AudioSource_t20 * __this, const MethodInfo* method)
{
	typedef AudioClip_t31 * (*AudioSource_get_clip_m360_ftn) (AudioSource_t20 *);
	static AudioSource_get_clip_m360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_clip_m360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_clip()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C" void AudioSource_set_clip_m359 (AudioSource_t20 * __this, AudioClip_t31 * ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_clip_m359_ftn) (AudioSource_t20 *, AudioClip_t31 *);
	static AudioSource_set_clip_m359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C" void AudioSource_Play_m7159 (AudioSource_t20 * __this, uint64_t ___delay, const MethodInfo* method)
{
	typedef void (*AudioSource_Play_m7159_ftn) (AudioSource_t20 *, uint64_t);
	static AudioSource_Play_m7159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Play_m7159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Play(System.UInt64)");
	_il2cpp_icall_func(__this, ___delay);
}
// System.Void UnityEngine.AudioSource::Play()
extern "C" void AudioSource_Play_m2616 (AudioSource_t20 * __this, const MethodInfo* method)
{
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		uint64_t L_0 = V_0;
		AudioSource_Play_m7159(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C" void AudioSource_PlayOneShot_m7160 (AudioSource_t20 * __this, AudioClip_t31 * ___clip, float ___volumeScale, const MethodInfo* method)
{
	typedef void (*AudioSource_PlayOneShot_m7160_ftn) (AudioSource_t20 *, AudioClip_t31 *, float);
	static AudioSource_PlayOneShot_m7160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_PlayOneShot_m7160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)");
	_il2cpp_icall_func(__this, ___clip, ___volumeScale);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern "C" void AudioSource_PlayOneShot_m386 (AudioSource_t20 * __this, AudioClip_t31 * ___clip, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		AudioClip_t31 * L_0 = ___clip;
		float L_1 = V_0;
		AudioSource_PlayOneShot_m7160(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::PlayClipAtPoint(UnityEngine.AudioClip,UnityEngine.Vector3)
extern "C" void AudioSource_PlayClipAtPoint_m2609 (Object_t * __this /* static, unused */, AudioClip_t31 * ___clip, Vector3_t6  ___position, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		AudioClip_t31 * L_0 = ___clip;
		Vector3_t6  L_1 = ___position;
		float L_2 = V_0;
		AudioSource_PlayClipAtPoint_m7161(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::PlayClipAtPoint(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern const Il2CppType* AudioSource_t20_0_0_0_var;
extern TypeInfo* GameObject_t27_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* AudioSource_t20_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1162;
extern "C" void AudioSource_PlayClipAtPoint_m7161 (Object_t * __this /* static, unused */, AudioClip_t31 * ___clip, Vector3_t6  ___position, float ___volume, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioSource_t20_0_0_0_var = il2cpp_codegen_type_from_index(22);
		GameObject_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		AudioSource_t20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral1162 = il2cpp_codegen_string_literal_from_index(1162);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t27 * V_0 = {0};
	AudioSource_t20 * V_1 = {0};
	{
		GameObject_t27 * L_0 = (GameObject_t27 *)il2cpp_codegen_object_new (GameObject_t27_il2cpp_TypeInfo_var);
		GameObject__ctor_m403(L_0, _stringLiteral1162, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t27 * L_1 = V_0;
		NullCheck(L_1);
		Transform_t25 * L_2 = GameObject_get_transform_m305(L_1, /*hidden argument*/NULL);
		Vector3_t6  L_3 = ___position;
		NullCheck(L_2);
		Transform_set_position_m340(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t27 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(AudioSource_t20_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		Component_t56 * L_6 = GameObject_AddComponent_m7197(L_4, L_5, /*hidden argument*/NULL);
		V_1 = ((AudioSource_t20 *)CastclassSealed(L_6, AudioSource_t20_il2cpp_TypeInfo_var));
		AudioSource_t20 * L_7 = V_1;
		AudioClip_t31 * L_8 = ___clip;
		NullCheck(L_7);
		AudioSource_set_clip_m359(L_7, L_8, /*hidden argument*/NULL);
		AudioSource_t20 * L_9 = V_1;
		float L_10 = ___volume;
		NullCheck(L_9);
		AudioSource_set_volume_m325(L_9, L_10, /*hidden argument*/NULL);
		AudioSource_t20 * L_11 = V_1;
		NullCheck(L_11);
		AudioSource_Play_m2616(L_11, /*hidden argument*/NULL);
		GameObject_t27 * L_12 = V_0;
		AudioClip_t31 * L_13 = ___clip;
		NullCheck(L_13);
		float L_14 = AudioClip_get_length_m361(L_13, /*hidden argument*/NULL);
		float L_15 = Time_get_timeScale_m8104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_Destroy_m2617(NULL /*static, unused*/, L_12, ((float)((float)L_14*(float)L_15)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern "C" void AudioSource_set_loop_m2615 (AudioSource_t20 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_loop_m2615_ftn) (AudioSource_t20 *, bool);
	static AudioSource_set_loop_m2615_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_loop_m2615_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_loop(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern "C" void AudioSource_set_playOnAwake_m358 (AudioSource_t20 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_playOnAwake_m358_ftn) (AudioSource_t20 *, bool);
	static AudioSource_set_playOnAwake_m358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_playOnAwake_m358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_playOnAwake(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m6781 (WebCamDevice_t1219 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m7162 (WebCamDevice_t1219 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Flags_1);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t1219_marshal(const WebCamDevice_t1219& unmarshaled, WebCamDevice_t1219_marshaled& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Name_0);
	marshaled.___m_Flags_1 = unmarshaled.___m_Flags_1;
}
extern "C" void WebCamDevice_t1219_marshal_back(const WebCamDevice_t1219_marshaled& marshaled, WebCamDevice_t1219& unmarshaled)
{
	unmarshaled.___m_Name_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0);
	unmarshaled.___m_Flags_1 = marshaled.___m_Flags_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t1219_marshal_cleanup(WebCamDevice_t1219_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// System.Void UnityEngine.WebCamTexture::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void WebCamTexture__ctor_m6614 (WebCamTexture_t1012 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m7458(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		WebCamTexture_Internal_CreateWebCamTexture_m7163(NULL /*static, unused*/, __this, L_0, 0, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
extern "C" void WebCamTexture_Internal_CreateWebCamTexture_m7163 (Object_t * __this /* static, unused */, WebCamTexture_t1012 * ___self, String_t* ___scriptingDevice, int32_t ___requestedWidth, int32_t ___requestedHeight, int32_t ___maxFramerate, const MethodInfo* method)
{
	typedef void (*WebCamTexture_Internal_CreateWebCamTexture_m7163_ftn) (WebCamTexture_t1012 *, String_t*, int32_t, int32_t, int32_t);
	static WebCamTexture_Internal_CreateWebCamTexture_m7163_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_Internal_CreateWebCamTexture_m7163_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___self, ___scriptingDevice, ___requestedWidth, ___requestedHeight, ___maxFramerate);
}
// System.Void UnityEngine.WebCamTexture::Play()
extern "C" void WebCamTexture_Play_m6619 (WebCamTexture_t1012 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Play_m7164(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
extern "C" void WebCamTexture_INTERNAL_CALL_Play_m7164 (Object_t * __this /* static, unused */, WebCamTexture_t1012 * ___self, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Play_m7164_ftn) (WebCamTexture_t1012 *);
	static WebCamTexture_INTERNAL_CALL_Play_m7164_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Play_m7164_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self);
}
// System.Void UnityEngine.WebCamTexture::Stop()
extern "C" void WebCamTexture_Stop_m6620 (WebCamTexture_t1012 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Stop_m7165(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
extern "C" void WebCamTexture_INTERNAL_CALL_Stop_m7165 (Object_t * __this /* static, unused */, WebCamTexture_t1012 * ___self, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Stop_m7165_ftn) (WebCamTexture_t1012 *);
	static WebCamTexture_INTERNAL_CALL_Stop_m7165_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Stop_m7165_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self);
}
// System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
extern "C" bool WebCamTexture_get_isPlaying_m6613 (WebCamTexture_t1012 * __this, const MethodInfo* method)
{
	typedef bool (*WebCamTexture_get_isPlaying_m6613_ftn) (WebCamTexture_t1012 *);
	static WebCamTexture_get_isPlaying_m6613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_isPlaying_m6613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.WebCamTexture::set_deviceName(System.String)
extern "C" void WebCamTexture_set_deviceName_m6615 (WebCamTexture_t1012 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_deviceName_m6615_ftn) (WebCamTexture_t1012 *, String_t*);
	static WebCamTexture_set_deviceName_m6615_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_deviceName_m6615_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_deviceName(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
extern "C" void WebCamTexture_set_requestedFPS_m6616 (WebCamTexture_t1012 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedFPS_m6616_ftn) (WebCamTexture_t1012 *, float);
	static WebCamTexture_set_requestedFPS_m6616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedFPS_m6616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedFPS(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)
extern "C" void WebCamTexture_set_requestedWidth_m6617 (WebCamTexture_t1012 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedWidth_m6617_ftn) (WebCamTexture_t1012 *, int32_t);
	static WebCamTexture_set_requestedWidth_m6617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedWidth_m6617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)
extern "C" void WebCamTexture_set_requestedHeight_m6618 (WebCamTexture_t1012 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedHeight_m6618_ftn) (WebCamTexture_t1012 *, int32_t);
	static WebCamTexture_set_requestedHeight_m6618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedHeight_m6618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
extern "C" WebCamDeviceU5BU5D_t575* WebCamTexture_get_devices_m2634 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef WebCamDeviceU5BU5D_t575* (*WebCamTexture_get_devices_m2634_ftn) ();
	static WebCamTexture_get_devices_m2634_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_devices_m2634_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_devices()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.WebCamTexture::get_didUpdateThisFrame()
extern "C" bool WebCamTexture_get_didUpdateThisFrame_m6612 (WebCamTexture_t1012 * __this, const MethodInfo* method)
{
	typedef bool (*WebCamTexture_get_didUpdateThisFrame_m6612_ftn) (WebCamTexture_t1012 *);
	static WebCamTexture_get_didUpdateThisFrame_m6612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_didUpdateThisFrame_m6612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_didUpdateThisFrame()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern "C" void AnimationEvent__ctor_m7166 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		__this->___m_OwnsData_1 = 1;
		AnimationEvent_Create_m7167(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationEvent::Create()
extern "C" void AnimationEvent_Create_m7167 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef void (*AnimationEvent_Create_m7167_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_Create_m7167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_Create_m7167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::Create()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::Finalize()
extern "C" void AnimationEvent_Finalize_m7168 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (__this->___m_OwnsData_1);
			if (!L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			AnimationEvent_Destroy_m7169(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationEvent::Destroy()
extern "C" void AnimationEvent_Destroy_m7169 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef void (*AnimationEvent_Destroy_m7169_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_Destroy_m7169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_Destroy_m7169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C" String_t* AnimationEvent_get_data_m7170 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef String_t* (*AnimationEvent_get_data_m7170_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_data_m7170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_data_m7170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_data()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C" void AnimationEvent_set_data_m7171 (AnimationEvent_t1318 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*AnimationEvent_set_data_m7171_ftn) (AnimationEvent_t1318 *, String_t*);
	static AnimationEvent_set_data_m7171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_set_data_m7171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::set_data(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C" String_t* AnimationEvent_get_stringParameter_m7172 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef String_t* (*AnimationEvent_get_stringParameter_m7172_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_stringParameter_m7172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_stringParameter_m7172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_stringParameter()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C" void AnimationEvent_set_stringParameter_m7173 (AnimationEvent_t1318 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*AnimationEvent_set_stringParameter_m7173_ftn) (AnimationEvent_t1318 *, String_t*);
	static AnimationEvent_set_stringParameter_m7173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_set_stringParameter_m7173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::set_stringParameter(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C" float AnimationEvent_get_floatParameter_m7174 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef float (*AnimationEvent_get_floatParameter_m7174_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_floatParameter_m7174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_floatParameter_m7174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_floatParameter()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C" void AnimationEvent_set_floatParameter_m7175 (AnimationEvent_t1318 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AnimationEvent_set_floatParameter_m7175_ftn) (AnimationEvent_t1318 *, float);
	static AnimationEvent_set_floatParameter_m7175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_set_floatParameter_m7175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::set_floatParameter(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C" int32_t AnimationEvent_get_intParameter_m7176 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationEvent_get_intParameter_m7176_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_intParameter_m7176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_intParameter_m7176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_intParameter()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C" void AnimationEvent_set_intParameter_m7177 (AnimationEvent_t1318 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*AnimationEvent_set_intParameter_m7177_ftn) (AnimationEvent_t1318 *, int32_t);
	static AnimationEvent_set_intParameter_m7177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_set_intParameter_m7177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::set_intParameter(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C" Object_t53 * AnimationEvent_get_objectReferenceParameter_m7178 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef Object_t53 * (*AnimationEvent_get_objectReferenceParameter_m7178_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_objectReferenceParameter_m7178_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_objectReferenceParameter_m7178_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_objectReferenceParameter()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C" void AnimationEvent_set_objectReferenceParameter_m7179 (AnimationEvent_t1318 * __this, Object_t53 * ___value, const MethodInfo* method)
{
	typedef void (*AnimationEvent_set_objectReferenceParameter_m7179_ftn) (AnimationEvent_t1318 *, Object_t53 *);
	static AnimationEvent_set_objectReferenceParameter_m7179_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_set_objectReferenceParameter_m7179_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C" String_t* AnimationEvent_get_functionName_m7180 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef String_t* (*AnimationEvent_get_functionName_m7180_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_functionName_m7180_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_functionName_m7180_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_functionName()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C" void AnimationEvent_set_functionName_m7181 (AnimationEvent_t1318 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*AnimationEvent_set_functionName_m7181_ftn) (AnimationEvent_t1318 *, String_t*);
	static AnimationEvent_set_functionName_m7181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_set_functionName_m7181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::set_functionName(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C" float AnimationEvent_get_time_m7182 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef float (*AnimationEvent_get_time_m7182_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_time_m7182_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_time_m7182_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_time()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C" void AnimationEvent_set_time_m7183 (AnimationEvent_t1318 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AnimationEvent_set_time_m7183_ftn) (AnimationEvent_t1318 *, float);
	static AnimationEvent_set_time_m7183_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_set_time_m7183_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C" int32_t AnimationEvent_get_messageOptions_m7184 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationEvent_get_messageOptions_m7184_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_messageOptions_m7184_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_messageOptions_m7184_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_messageOptions()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C" void AnimationEvent_set_messageOptions_m7185 (AnimationEvent_t1318 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*AnimationEvent_set_messageOptions_m7185_ftn) (AnimationEvent_t1318 *, int32_t);
	static AnimationEvent_set_messageOptions_m7185_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_set_messageOptions_m7185_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern "C" AnimationState_t1323 * AnimationEvent_get_animationState_m7186 (AnimationEvent_t1318 * __this, const MethodInfo* method)
{
	typedef AnimationState_t1323 * (*AnimationEvent_get_animationState_m7186_ftn) (AnimationEvent_t1318 *);
	static AnimationEvent_get_animationState_m7186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationEvent_get_animationState_m7186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationEvent::get_animationState()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1318_marshal(const AnimationEvent_t1318& unmarshaled, AnimationEvent_t1318_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
	marshaled.___m_OwnsData_1 = unmarshaled.___m_OwnsData_1;
}
extern "C" void AnimationEvent_t1318_marshal_back(const AnimationEvent_t1318_marshaled& marshaled, AnimationEvent_t1318& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
	unmarshaled.___m_OwnsData_1 = marshaled.___m_OwnsData_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1318_marshal_cleanup(AnimationEvent_t1318_marshaled& marshaled)
{
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m7187 (AnimationCurve_t1322 * __this, KeyframeU5BU5D_t1443* ___keys, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t1443* L_0 = ___keys;
		AnimationCurve_Init_m7191(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m7188 (AnimationCurve_t1322 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m7191(__this, (KeyframeU5BU5D_t1443*)(KeyframeU5BU5D_t1443*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m7189 (AnimationCurve_t1322 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m7189_ftn) (AnimationCurve_t1322 *);
	static AnimationCurve_Cleanup_m7189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m7189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m7190 (AnimationCurve_t1322 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m7189(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m7191 (AnimationCurve_t1322 * __this, KeyframeU5BU5D_t1443* ___keys, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m7191_ftn) (AnimationCurve_t1322 *, KeyframeU5BU5D_t1443*);
	static AnimationCurve_Init_m7191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m7191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t1322_marshal(const AnimationCurve_t1322& unmarshaled, AnimationCurve_t1322_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void AnimationCurve_t1322_marshal_back(const AnimationCurve_t1322_marshaled& marshaled, AnimationCurve_t1322& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t1322_marshal_cleanup(AnimationCurve_t1322_marshaled& marshaled)
{
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" void GameObject__ctor_m403 (GameObject_t27 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m7199(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C" void GameObject__ctor_m6754 (GameObject_t27 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m7199(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C" void GameObject__ctor_m6775 (GameObject_t27 * __this, String_t* ___name, TypeU5BU5D_t1218* ___components, const MethodInfo* method)
{
	Type_t * V_0 = {0};
	TypeU5BU5D_t1218* V_1 = {0};
	int32_t V_2 = 0;
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m7199(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		TypeU5BU5D_t1218* L_1 = ___components;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0026;
	}

IL_0016:
	{
		TypeU5BU5D_t1218* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(Type_t **)(Type_t **)SZArrayLdElema(L_2, L_4, sizeof(Type_t *)));
		Type_t * L_5 = V_0;
		GameObject_AddComponent_m7197(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_7 = V_2;
		TypeU5BU5D_t1218* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}
}
// UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
extern "C" GameObject_t27 * GameObject_CreatePrimitive_m6490 (Object_t * __this /* static, unused */, int32_t ___type, const MethodInfo* method)
{
	typedef GameObject_t27 * (*GameObject_CreatePrimitive_m6490_ftn) (int32_t);
	static GameObject_CreatePrimitive_m6490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_CreatePrimitive_m6490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)");
	return _il2cpp_icall_func(___type);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C" Component_t56 * GameObject_GetComponent_m7192 (GameObject_t27 * __this, Type_t * ___type, const MethodInfo* method)
{
	typedef Component_t56 * (*GameObject_GetComponent_m7192_ftn) (GameObject_t27 *, Type_t *);
	static GameObject_GetComponent_m7192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m7192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
extern TypeInfo* IEnumerator_t35_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t25_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern "C" Component_t56 * GameObject_GetComponentInChildren_m7193 (GameObject_t27 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Transform_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	Component_t56 * V_0 = {0};
	Transform_t25 * V_1 = {0};
	Transform_t25 * V_2 = {0};
	Object_t * V_3 = {0};
	Component_t56 * V_4 = {0};
	Component_t56 * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameObject_get_activeInHierarchy_m4031(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t56 * L_2 = GameObject_GetComponent_m7192(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t56 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m395(NULL /*static, unused*/, L_3, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t56 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t25 * L_6 = GameObject_get_transform_m305(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		Transform_t25 * L_7 = V_1;
		bool L_8 = Object_op_Inequality_m395(NULL /*static, unused*/, L_7, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0095;
		}
	}
	{
		Transform_t25 * L_9 = V_1;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_9);
		V_3 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_0040:
		{
			Object_t * L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t35_il2cpp_TypeInfo_var, L_11);
			V_2 = ((Transform_t25 *)CastclassClass(L_12, Transform_t25_il2cpp_TypeInfo_var));
			Transform_t25 * L_13 = V_2;
			NullCheck(L_13);
			GameObject_t27 * L_14 = Component_get_gameObject_m306(L_13, /*hidden argument*/NULL);
			Type_t * L_15 = ___type;
			NullCheck(L_14);
			Component_t56 * L_16 = GameObject_GetComponentInChildren_m7193(L_14, L_15, /*hidden argument*/NULL);
			V_4 = L_16;
			Component_t56 * L_17 = V_4;
			bool L_18 = Object_op_Inequality_m395(NULL /*static, unused*/, L_17, (Object_t53 *)NULL, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0070;
			}
		}

IL_0067:
		{
			Component_t56 * L_19 = V_4;
			V_5 = L_19;
			IL2CPP_LEAVE(0x97, FINALLY_0080);
		}

IL_0070:
		{
			Object_t * L_20 = V_3;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t35_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0040;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x95, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Object_t * L_22 = V_3;
			V_6 = ((Object_t *)IsInst(L_22, IDisposable_t42_il2cpp_TypeInfo_var));
			Object_t * L_23 = V_6;
			if (L_23)
			{
				goto IL_008d;
			}
		}

IL_008c:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_008d:
		{
			Object_t * L_24 = V_6;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0095:
	{
		return (Component_t56 *)NULL;
	}

IL_0097:
	{
		Component_t56 * L_25 = V_5;
		return L_25;
	}
}
// System.Void UnityEngine.GameObject::set_isStatic(System.Boolean)
extern "C" void GameObject_set_isStatic_m7194 (GameObject_t27 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GameObject_set_isStatic_m7194_ftn) (GameObject_t27 *, bool);
	static GameObject_set_isStatic_m7194_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_isStatic_m7194_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_isStatic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GameObject::GetComponentsForListInternal(System.Type,System.Type,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C" void GameObject_GetComponentsForListInternal_m7195 (GameObject_t27 * __this, Type_t * ___searchType, Type_t * ___listElementType, bool ___recursive, bool ___includeInactive, bool ___reverse, Object_t * ___resultList, const MethodInfo* method)
{
	typedef void (*GameObject_GetComponentsForListInternal_m7195_ftn) (GameObject_t27 *, Type_t *, Type_t *, bool, bool, bool, Object_t *);
	static GameObject_GetComponentsForListInternal_m7195_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsForListInternal_m7195_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsForListInternal(System.Type,System.Type,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	_il2cpp_icall_func(__this, ___searchType, ___listElementType, ___recursive, ___includeInactive, ___reverse, ___resultList);
}
// UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C" ComponentU5BU5D_t54* GameObject_GetComponentsInternal_m7196 (GameObject_t27 * __this, Type_t * ___type, bool ___isGenericTypeArray, bool ___recursive, bool ___includeInactive, bool ___reverse, const MethodInfo* method)
{
	typedef ComponentU5BU5D_t54* (*GameObject_GetComponentsInternal_m7196_ftn) (GameObject_t27 *, Type_t *, bool, bool, bool, bool);
	static GameObject_GetComponentsInternal_m7196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m7196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(__this, ___type, ___isGenericTypeArray, ___recursive, ___includeInactive, ___reverse);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" Transform_t25 * GameObject_get_transform_m305 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef Transform_t25 * (*GameObject_get_transform_m305_ftn) (GameObject_t27 *);
	static GameObject_get_transform_m305_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m305_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rigidbody UnityEngine.GameObject::get_rigidbody()
extern "C" Rigidbody_t50 * GameObject_get_rigidbody_m394 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef Rigidbody_t50 * (*GameObject_get_rigidbody_m394_ftn) (GameObject_t27 *);
	static GameObject_get_rigidbody_m394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_rigidbody_m394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_rigidbody()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Light UnityEngine.GameObject::get_light()
extern "C" Light_t47 * GameObject_get_light_m316 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef Light_t47 * (*GameObject_get_light_m316_ftn) (GameObject_t27 *);
	static GameObject_get_light_m316_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_light_m316_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_light()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Renderer UnityEngine.GameObject::get_renderer()
extern "C" Renderer_t46 * GameObject_get_renderer_m314 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef Renderer_t46 * (*GameObject_get_renderer_m314_ftn) (GameObject_t27 *);
	static GameObject_get_renderer_m314_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_renderer_m314_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_renderer()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.AudioSource UnityEngine.GameObject::get_audio()
extern "C" AudioSource_t20 * GameObject_get_audio_m322 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef AudioSource_t20 * (*GameObject_get_audio_m322_ftn) (GameObject_t27 *);
	static GameObject_get_audio_m322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_audio_m322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_audio()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GUIText UnityEngine.GameObject::get_guiText()
extern "C" GUIText_t44 * GameObject_get_guiText_m311 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef GUIText_t44 * (*GameObject_get_guiText_m311_ftn) (GameObject_t27 *);
	static GameObject_get_guiText_m311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_guiText_m311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_guiText()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GUITexture UnityEngine.GameObject::get_guiTexture()
extern "C" GUITexture_t43 * GameObject_get_guiTexture_m308 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef GUITexture_t43 * (*GameObject_get_guiTexture_m308_ftn) (GameObject_t27 *);
	static GameObject_get_guiTexture_m308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_guiTexture_m308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_guiTexture()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C" int32_t GameObject_get_layer_m4273 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef int32_t (*GameObject_get_layer_m4273_ftn) (GameObject_t27 *);
	static GameObject_get_layer_m4273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m4273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C" void GameObject_set_layer_m4274 (GameObject_t27 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GameObject_set_layer_m4274_ftn) (GameObject_t27 *, int32_t);
	static GameObject_set_layer_m4274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m4274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" void GameObject_SetActive_m2344 (GameObject_t27 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GameObject_SetActive_m2344_ftn) (GameObject_t27 *, bool);
	static GameObject_SetActive_m2344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m2344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" bool GameObject_get_activeInHierarchy_m4031 (GameObject_t27 * __this, const MethodInfo* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m4031_ftn) (GameObject_t27 *);
	static GameObject_get_activeInHierarchy_m4031_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m4031_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void GameObject_SendMessage_m431 (GameObject_t27 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method)
{
	typedef void (*GameObject_SendMessage_m431_ftn) (GameObject_t27 *, String_t*, Object_t *, int32_t);
	static GameObject_SendMessage_m431_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m431_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String)
extern "C" void GameObject_SendMessage_m2680 (GameObject_t27 * __this, String_t* ___methodName, const MethodInfo* method)
{
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = V_1;
		int32_t L_2 = V_0;
		GameObject_SendMessage_m431(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" Component_t56 * GameObject_AddComponent_m7197 (GameObject_t27 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___componentType;
		Component_t56 * L_1 = GameObject_Internal_AddComponentWithType_m7198(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C" Component_t56 * GameObject_Internal_AddComponentWithType_m7198 (GameObject_t27 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	typedef Component_t56 * (*GameObject_Internal_AddComponentWithType_m7198_ftn) (GameObject_t27 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m7198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m7198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	return _il2cpp_icall_func(__this, ___componentType);
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C" void GameObject_Internal_CreateGameObject_m7199 (Object_t * __this /* static, unused */, GameObject_t27 * ___mono, String_t* ___name, const MethodInfo* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m7199_ftn) (GameObject_t27 *, String_t*);
	static GameObject_Internal_CreateGameObject_m7199_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m7199_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono, ___name);
}
// UnityEngine.AnimationClip UnityEngine.AnimationInfo::get_clip()
extern "C" AnimationClip_t1319 * AnimationInfo_get_clip_m7200 (AnimationInfo_t1324 * __this, const MethodInfo* method)
{
	AnimationClip_t1319 * G_B3_0 = {0};
	{
		int32_t L_0 = (__this->___m_ClipInstanceID_0);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = (__this->___m_ClipInstanceID_0);
		AnimationClip_t1319 * L_2 = AnimationInfo_ClipInstanceToScriptingObject_m7202(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = ((AnimationClip_t1319 *)(NULL));
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.AnimationInfo::get_weight()
extern "C" float AnimationInfo_get_weight_m7201 (AnimationInfo_t1324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight_1);
		return L_0;
	}
}
// UnityEngine.AnimationClip UnityEngine.AnimationInfo::ClipInstanceToScriptingObject(System.Int32)
extern "C" AnimationClip_t1319 * AnimationInfo_ClipInstanceToScriptingObject_m7202 (Object_t * __this /* static, unused */, int32_t ___instanceID, const MethodInfo* method)
{
	typedef AnimationClip_t1319 * (*AnimationInfo_ClipInstanceToScriptingObject_m7202_ftn) (int32_t);
	static AnimationInfo_ClipInstanceToScriptingObject_m7202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationInfo_ClipInstanceToScriptingObject_m7202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationInfo::ClipInstanceToScriptingObject(System.Int32)");
	return _il2cpp_icall_func(___instanceID);
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" bool AnimatorStateInfo_IsName_m7203 (AnimatorStateInfo_t1325 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m7215(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = (__this->___m_Name_0);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (__this->___m_Path_1);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C" int32_t AnimatorStateInfo_get_nameHash_m7204 (AnimatorStateInfo_t1325 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Path_1);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" float AnimatorStateInfo_get_normalizedTime_m7205 (AnimatorStateInfo_t1325 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_2);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" float AnimatorStateInfo_get_length_m7206 (AnimatorStateInfo_t1325 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Length_3);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" int32_t AnimatorStateInfo_get_tagHash_m7207 (AnimatorStateInfo_t1325 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Tag_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" bool AnimatorStateInfo_IsTag_m7208 (AnimatorStateInfo_t1325 * __this, String_t* ___tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tag;
		int32_t L_1 = Animator_StringToHash_m7215(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Tag_4);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" bool AnimatorStateInfo_get_loop_m7209 (AnimatorStateInfo_t1325 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Loop_5);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C" bool AnimatorTransitionInfo_IsName_m7210 (AnimatorTransitionInfo_t1326 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m7215(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Name_0);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" bool AnimatorTransitionInfo_IsUserName_m7211 (AnimatorTransitionInfo_t1326 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m7215(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_UserName_1);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" int32_t AnimatorTransitionInfo_get_nameHash_m7212 (AnimatorTransitionInfo_t1326 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" int32_t AnimatorTransitionInfo_get_userNameHash_m7213 (AnimatorTransitionInfo_t1326 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UserName_1);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" float AnimatorTransitionInfo_get_normalizedTime_m7214 (AnimatorTransitionInfo_t1326 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_2);
		return L_0;
	}
}
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C" void Animator_SetBool_m2646 (Animator_t360 * __this, String_t* ___name, bool ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		bool L_1 = ___value;
		Animator_SetBoolString_m7216(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" void Animator_SetTrigger_m2610 (Animator_t360 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_SetTriggerString_m7217(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C" void Animator_ResetTrigger_m4358 (Animator_t360 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_ResetTriggerString_m7218(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C" RuntimeAnimatorController_t864 * Animator_get_runtimeAnimatorController_m4357 (Animator_t360 * __this, const MethodInfo* method)
{
	typedef RuntimeAnimatorController_t864 * (*Animator_get_runtimeAnimatorController_m4357_ftn) (Animator_t360 *);
	static Animator_get_runtimeAnimatorController_m4357_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_runtimeAnimatorController_m4357_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" int32_t Animator_StringToHash_m7215 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m7215_ftn) (String_t*);
	static Animator_StringToHash_m7215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m7215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
extern "C" void Animator_SetBoolString_m7216 (Animator_t360 * __this, String_t* ___name, bool ___value, const MethodInfo* method)
{
	typedef void (*Animator_SetBoolString_m7216_ftn) (Animator_t360 *, String_t*, bool);
	static Animator_SetBoolString_m7216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetBoolString_m7216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetBoolString(System.String,System.Boolean)");
	_il2cpp_icall_func(__this, ___name, ___value);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" void Animator_SetTriggerString_m7217 (Animator_t360 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m7217_ftn) (Animator_t360 *, String_t*);
	static Animator_SetTriggerString_m7217_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m7217_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" void Animator_ResetTriggerString_m7218 (Animator_t360 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m7218_ftn) (Animator_t360 *, String_t*);
	static Animator_ResetTriggerString_m7218_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m7218_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t1327_marshal(const SkeletonBone_t1327& unmarshaled, SkeletonBone_t1327_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___position_1 = unmarshaled.___position_1;
	marshaled.___rotation_2 = unmarshaled.___rotation_2;
	marshaled.___scale_3 = unmarshaled.___scale_3;
	marshaled.___transformModified_4 = unmarshaled.___transformModified_4;
}
extern "C" void SkeletonBone_t1327_marshal_back(const SkeletonBone_t1327_marshaled& marshaled, SkeletonBone_t1327& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___position_1 = marshaled.___position_1;
	unmarshaled.___rotation_2 = marshaled.___rotation_2;
	unmarshaled.___scale_3 = marshaled.___scale_3;
	unmarshaled.___transformModified_4 = marshaled.___transformModified_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t1327_marshal_cleanup(SkeletonBone_t1327_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// System.String UnityEngine.HumanBone::get_boneName()
extern "C" String_t* HumanBone_get_boneName_m7219 (HumanBone_t1329 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_BoneName_0);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
extern "C" void HumanBone_set_boneName_m7220 (HumanBone_t1329 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_BoneName_0 = L_0;
		return;
	}
}
// System.String UnityEngine.HumanBone::get_humanName()
extern "C" String_t* HumanBone_get_humanName_m7221 (HumanBone_t1329 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_HumanName_1);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
extern "C" void HumanBone_set_humanName_m7222 (HumanBone_t1329 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_HumanName_1 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t1329_marshal(const HumanBone_t1329& unmarshaled, HumanBone_t1329_marshaled& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_BoneName_0);
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.___m_HumanName_1);
	marshaled.___limit_2 = unmarshaled.___limit_2;
}
extern "C" void HumanBone_t1329_marshal_back(const HumanBone_t1329_marshaled& marshaled, HumanBone_t1329& unmarshaled)
{
	unmarshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0);
	unmarshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1);
	unmarshaled.___limit_2 = marshaled.___limit_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t1329_marshal_cleanup(HumanBone_t1329_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// System.Boolean UnityEngine.TerrainData::HasUser(UnityEngine.GameObject)
extern "C" bool TerrainData_HasUser_m7223 (TerrainData_t1331 * __this, GameObject_t27 * ___user, const MethodInfo* method)
{
	typedef bool (*TerrainData_HasUser_m7223_ftn) (TerrainData_t1331 *, GameObject_t27 *);
	static TerrainData_HasUser_m7223_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_HasUser_m7223_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::HasUser(UnityEngine.GameObject)");
	return _il2cpp_icall_func(__this, ___user);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
