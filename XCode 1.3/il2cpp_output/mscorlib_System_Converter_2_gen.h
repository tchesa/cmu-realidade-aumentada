﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "mscorlib_System_MulticastDelegate.h"

// System.Converter`2<System.Object,System.Object>
struct  Converter_2_t3513  : public MulticastDelegate_t10
{
};
