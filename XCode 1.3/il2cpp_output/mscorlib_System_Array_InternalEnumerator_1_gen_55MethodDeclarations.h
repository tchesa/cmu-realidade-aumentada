﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_55.h"

// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24245_gshared (InternalEnumerator_1_t3153 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24245(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3153 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24245_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24246_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24246(__this, method) (( void (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24246_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24247_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24247(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24247_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24248_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24248(__this, method) (( void (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24248_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24249_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24249(__this, method) (( bool (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24249_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C" uint16_t InternalEnumerator_1_get_Current_m24250_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24250(__this, method) (( uint16_t (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24250_gshared)(__this, method)
