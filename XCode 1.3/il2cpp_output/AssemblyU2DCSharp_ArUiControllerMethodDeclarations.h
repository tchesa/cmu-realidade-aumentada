﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ArUiController
struct ArUiController_t374;
// System.String
struct String_t;
// Facebook.Unity.ILoginResult
struct ILoginResult_t489;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// Facebook.Unity.IGraphResult
struct IGraphResult_t484;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void ArUiController::.ctor()
extern "C" void ArUiController__ctor_m1908 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ArUiController ArUiController::get_Instance()
extern "C" ArUiController_t374 * ArUiController_get_Instance_m1909 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::InitializeSingleton()
extern "C" void ArUiController_InitializeSingleton_m1910 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ArUiController::get_tempFileName()
extern "C" String_t* ArUiController_get_tempFileName_m1911 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Awake()
extern "C" void ArUiController_Awake_m1912 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Start()
extern "C" void ArUiController_Start_m1913 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Update()
extern "C" void ArUiController_Update_m1914 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Return_OnClick()
extern "C" void ArUiController_Return_OnClick_m1915 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Capture_OnClick()
extern "C" void ArUiController_Capture_OnClick_m1916 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::CaptureCancel_OnClick()
extern "C" void ArUiController_CaptureCancel_OnClick_m1917 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Info_OnClick()
extern "C" void ArUiController_Info_OnClick_m1918 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Info_Link_OnClick()
extern "C" void ArUiController_Info_Link_OnClick_m1919 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::InfoBack_OnClick()
extern "C" void ArUiController_InfoBack_OnClick_m1920 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Save_OnClick()
extern "C" void ArUiController_Save_OnClick_m1921 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Share_OnClick()
extern "C" void ArUiController_Share_OnClick_m1922 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::Login_OnClick()
extern "C" void ArUiController_Login_OnClick_m1923 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::CancelLogin_OnClick()
extern "C" void ArUiController_CancelLogin_OnClick_m1924 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::LoginCallback(System.Boolean,Facebook.Unity.ILoginResult)
extern "C" void ArUiController_LoginCallback_m1925 (ArUiController_t374 * __this, bool ___successful, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ArUiController::ShareAfterLogin()
extern "C" Object_t * ArUiController_ShareAfterLogin_m1926 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::ApiCallback(Facebook.Unity.IGraphResult)
extern "C" void ArUiController_ApiCallback_m1927 (ArUiController_t374 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ArUiController::ITakeScreenshot()
extern "C" Object_t * ArUiController_ITakeScreenshot_m1928 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::OnImageSaved(ISN_Result)
extern "C" void ArUiController_OnImageSaved_m1929 (ArUiController_t374 * __this, ISN_Result_t114 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController::OnGUI()
extern "C" void ArUiController_OnGUI_m1930 (ArUiController_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
