﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m17935(__this, ___dictionary, method) (( void (*) (Enumerator_t2751 *, Dictionary_2_t177 *, const MethodInfo*))Enumerator__ctor_m16145_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17936(__this, method) (( Object_t * (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16146_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m17937(__this, method) (( void (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16147_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17938(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16148_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17939(__this, method) (( Object_t * (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16149_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17940(__this, method) (( Object_t * (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::MoveNext()
#define Enumerator_MoveNext_m17941(__this, method) (( bool (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_MoveNext_m16151_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::get_Current()
#define Enumerator_get_Current_m17942(__this, method) (( KeyValuePair_2_t2748  (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_get_Current_m16152_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17943(__this, method) (( String_t* (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_get_CurrentKey_m16153_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17944(__this, method) (( iAdBanner_t172 * (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_get_CurrentValue_m16154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::Reset()
#define Enumerator_Reset_m17945(__this, method) (( void (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_Reset_m16155_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::VerifyState()
#define Enumerator_VerifyState_m17946(__this, method) (( void (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_VerifyState_m16156_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17947(__this, method) (( void (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_VerifyCurrent_m16157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iAdBanner>::Dispose()
#define Enumerator_Dispose_m17948(__this, method) (( void (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_Dispose_m16158_gshared)(__this, method)
