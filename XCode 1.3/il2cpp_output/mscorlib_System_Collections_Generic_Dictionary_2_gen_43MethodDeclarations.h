﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2632;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2606;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3610;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3611;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t2636;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t2640;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m16020_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m16020(__this, method) (( void (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2__ctor_m16020_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16022_gshared (Dictionary_2_t2632 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16022(__this, ___comparer, method) (( void (*) (Dictionary_2_t2632 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16022_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m16024_gshared (Dictionary_2_t2632 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m16024(__this, ___capacity, method) (( void (*) (Dictionary_2_t2632 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16024_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m16026_gshared (Dictionary_2_t2632 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m16026(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2632 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m16026_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m16028_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m16028(__this, method) (( Object_t * (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16028_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m16030_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16030(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16030_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16032_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16032(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2632 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16032_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16034_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m16034(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2632 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16034_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m16036_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m16036(__this, ___key, method) (( bool (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16036_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16038_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m16038(__this, ___key, method) (( void (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16038_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16040_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16040(__this, method) (( bool (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16040_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16042_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16042(__this, method) (( Object_t * (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16042_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16044_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16044(__this, method) (( bool (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16044_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16046_gshared (Dictionary_2_t2632 * __this, KeyValuePair_2_t2634  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16046(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2632 *, KeyValuePair_2_t2634 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16046_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16048_gshared (Dictionary_2_t2632 * __this, KeyValuePair_2_t2634  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16048(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2632 *, KeyValuePair_2_t2634 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16048_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16050_gshared (Dictionary_2_t2632 * __this, KeyValuePair_2U5BU5D_t3610* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16050(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2632 *, KeyValuePair_2U5BU5D_t3610*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16050_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16052_gshared (Dictionary_2_t2632 * __this, KeyValuePair_2_t2634  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16052(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2632 *, KeyValuePair_2_t2634 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16052_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16054_gshared (Dictionary_2_t2632 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16054(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2632 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16054_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16056_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16056(__this, method) (( Object_t * (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16056_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16058_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16058(__this, method) (( Object_t* (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16058_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16060_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16060(__this, method) (( Object_t * (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16060_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m16062_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m16062(__this, method) (( int32_t (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_get_Count_m16062_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m16064_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m16064(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m16064_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m16066_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m16066(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2632 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m16066_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m16068_gshared (Dictionary_2_t2632 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m16068(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2632 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16068_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m16070_gshared (Dictionary_2_t2632 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m16070(__this, ___size, method) (( void (*) (Dictionary_2_t2632 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16070_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m16072_gshared (Dictionary_2_t2632 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m16072(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2632 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16072_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2634  Dictionary_2_make_pair_m16074_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m16074(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2634  (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m16074_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m16076_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m16076(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m16076_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m16078_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m16078(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m16078_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m16080_gshared (Dictionary_2_t2632 * __this, KeyValuePair_2U5BU5D_t3610* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m16080(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2632 *, KeyValuePair_2U5BU5D_t3610*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16080_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m16082_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m16082(__this, method) (( void (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_Resize_m16082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m16084_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m16084(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2632 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_Add_m16084_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m16086_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m16086(__this, method) (( void (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_Clear_m16086_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m16088_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m16088(__this, ___key, method) (( bool (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m16088_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m16090_gshared (Dictionary_2_t2632 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m16090(__this, ___value, method) (( bool (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m16090_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m16092_gshared (Dictionary_2_t2632 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m16092(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2632 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m16092_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m16094_gshared (Dictionary_2_t2632 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m16094(__this, ___sender, method) (( void (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16094_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m16096_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m16096(__this, ___key, method) (( bool (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m16096_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m16098_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m16098(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2632 *, Object_t *, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m16098_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Keys()
extern "C" KeyCollection_t2636 * Dictionary_2_get_Keys_m16100_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m16100(__this, method) (( KeyCollection_t2636 * (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_get_Keys_m16100_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C" ValueCollection_t2640 * Dictionary_2_get_Values_m16102_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m16102(__this, method) (( ValueCollection_t2640 * (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_get_Values_m16102_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m16104_gshared (Dictionary_2_t2632 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m16104(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16104_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m16106_gshared (Dictionary_2_t2632 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m16106(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2632 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16106_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m16108_gshared (Dictionary_2_t2632 * __this, KeyValuePair_2_t2634  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m16108(__this, ___pair, method) (( bool (*) (Dictionary_2_t2632 *, KeyValuePair_2_t2634 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16108_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Enumerator_t2638  Dictionary_2_GetEnumerator_m16110_gshared (Dictionary_2_t2632 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m16110(__this, method) (( Enumerator_t2638  (*) (Dictionary_2_t2632 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16110_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m16112_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m16112(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16112_gshared)(__this /* static, unused */, ___key, ___value, method)
