﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialUseExample/<PostFBScreenshot>c__Iterator4
struct U3CPostFBScreenshotU3Ec__Iterator4_t206;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::.ctor()
extern "C" void U3CPostFBScreenshotU3Ec__Iterator4__ctor_m1143 (U3CPostFBScreenshotU3Ec__Iterator4_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPostFBScreenshotU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1144 (U3CPostFBScreenshotU3Ec__Iterator4_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPostFBScreenshotU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1145 (U3CPostFBScreenshotU3Ec__Iterator4_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::MoveNext()
extern "C" bool U3CPostFBScreenshotU3Ec__Iterator4_MoveNext_m1146 (U3CPostFBScreenshotU3Ec__Iterator4_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::Dispose()
extern "C" void U3CPostFBScreenshotU3Ec__Iterator4_Dispose_m1147 (U3CPostFBScreenshotU3Ec__Iterator4_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostFBScreenshot>c__Iterator4::Reset()
extern "C" void U3CPostFBScreenshotU3Ec__Iterator4_Reset_m1148 (U3CPostFBScreenshotU3Ec__Iterator4_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
