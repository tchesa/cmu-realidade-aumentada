﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PTPGameController
struct PTPGameController_t197;
// ISN_Result
struct ISN_Result_t114;
// UnionAssets.FLE.CEvent
struct CEvent_t74;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void PTPGameController::.ctor()
extern "C" void PTPGameController__ctor_m1110 (PTPGameController_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::Awake()
extern "C" void PTPGameController_Awake_m1111 (PTPGameController_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::createRedSphere(UnityEngine.Vector3)
extern "C" void PTPGameController_createRedSphere_m1112 (PTPGameController_t197 * __this, Vector3_t6  ___pos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::createGreenSphere(UnityEngine.Vector3)
extern "C" void PTPGameController_createGreenSphere_m1113 (PTPGameController_t197 * __this, Vector3_t6  ___pos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::OnAuthFinished(ISN_Result)
extern "C" void PTPGameController_OnAuthFinished_m1114 (PTPGameController_t197 * __this, ISN_Result_t114 * ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::OnGCPlayerDisconnected(UnionAssets.FLE.CEvent)
extern "C" void PTPGameController_OnGCPlayerDisconnected_m1115 (PTPGameController_t197 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::OnGCMatchStart(UnionAssets.FLE.CEvent)
extern "C" void PTPGameController_OnGCMatchStart_m1116 (PTPGameController_t197 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::cleanUpScene()
extern "C" void PTPGameController_cleanUpScene_m1117 (PTPGameController_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
