﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ActivationContext
struct ActivationContext_t2406;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ActivationContext::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m14558 (ActivationContext_t2406 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Finalize()
extern "C" void ActivationContext_Finalize_m14559 (ActivationContext_t2406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose()
extern "C" void ActivationContext_Dispose_m14560 (ActivationContext_t2406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose(System.Boolean)
extern "C" void ActivationContext_Dispose_m14561 (ActivationContext_t2406 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
