﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FBServices
struct FBServices_t381;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// FBServices
struct  FBServices_t381  : public MonoBehaviour_t18
{
	// System.Collections.Generic.List`1<System.String> FBServices::requested_permissions
	List_1_t78 * ___requested_permissions_2;
	// System.Boolean FBServices::isInit
	bool ___isInit_3;
};
struct FBServices_t381_StaticFields{
	// FBServices FBServices::current
	FBServices_t381 * ___current_1;
};
