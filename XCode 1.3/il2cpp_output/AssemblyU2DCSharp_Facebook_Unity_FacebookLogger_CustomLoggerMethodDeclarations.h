﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FacebookLogger/CustomLogger
struct CustomLogger_t296;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.FacebookLogger/CustomLogger::.ctor()
extern "C" void CustomLogger__ctor_m1651 (CustomLogger_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/CustomLogger::Log(System.String)
extern "C" void CustomLogger_Log_m1652 (CustomLogger_t296 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/CustomLogger::Info(System.String)
extern "C" void CustomLogger_Info_m1653 (CustomLogger_t296 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/CustomLogger::Warn(System.String)
extern "C" void CustomLogger_Warn_m1654 (CustomLogger_t296 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/CustomLogger::Error(System.String)
extern "C" void CustomLogger_Error_m1655 (CustomLogger_t296 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
