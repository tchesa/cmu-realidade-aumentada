﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m22921(__this, ___dictionary, method) (( void (*) (Enumerator_t3076 *, Dictionary_2_t937 *, const MethodInfo*))Enumerator__ctor_m22819_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22922(__this, method) (( Object_t * (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m22923(__this, method) (( void (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m22821_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22924(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22822_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22925(__this, method) (( Object_t * (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22823_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22926(__this, method) (( Object_t * (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22824_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
#define Enumerator_MoveNext_m22927(__this, method) (( bool (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_MoveNext_m22825_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
#define Enumerator_get_Current_m22928(__this, method) (( KeyValuePair_2_t3074  (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_get_Current_m22826_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22929(__this, method) (( int32_t (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_get_CurrentKey_m22827_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22930(__this, method) (( Image_t943 * (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_get_CurrentValue_m22828_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Reset()
#define Enumerator_Reset_m22931(__this, method) (( void (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_Reset_m22829_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::VerifyState()
#define Enumerator_VerifyState_m22932(__this, method) (( void (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_VerifyState_m22830_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22933(__this, method) (( void (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_VerifyCurrent_m22831_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Dispose()
#define Enumerator_Dispose_m22934(__this, method) (( void (*) (Enumerator_t3076 *, const MethodInfo*))Enumerator_Dispose_m22832_gshared)(__this, method)
