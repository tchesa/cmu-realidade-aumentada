﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>
struct ShimEnumerator_t3162;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3149;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m24321_gshared (ShimEnumerator_t3162 * __this, Dictionary_2_t3149 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m24321(__this, ___host, method) (( void (*) (ShimEnumerator_t3162 *, Dictionary_2_t3149 *, const MethodInfo*))ShimEnumerator__ctor_m24321_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m24322_gshared (ShimEnumerator_t3162 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m24322(__this, method) (( bool (*) (ShimEnumerator_t3162 *, const MethodInfo*))ShimEnumerator_MoveNext_m24322_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Entry()
extern "C" DictionaryEntry_t58  ShimEnumerator_get_Entry_m24323_gshared (ShimEnumerator_t3162 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m24323(__this, method) (( DictionaryEntry_t58  (*) (ShimEnumerator_t3162 *, const MethodInfo*))ShimEnumerator_get_Entry_m24323_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m24324_gshared (ShimEnumerator_t3162 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m24324(__this, method) (( Object_t * (*) (ShimEnumerator_t3162 *, const MethodInfo*))ShimEnumerator_get_Key_m24324_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m24325_gshared (ShimEnumerator_t3162 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m24325(__this, method) (( Object_t * (*) (ShimEnumerator_t3162 *, const MethodInfo*))ShimEnumerator_get_Value_m24325_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m24326_gshared (ShimEnumerator_t3162 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m24326(__this, method) (( Object_t * (*) (ShimEnumerator_t3162 *, const MethodInfo*))ShimEnumerator_get_Current_m24326_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::Reset()
extern "C" void ShimEnumerator_Reset_m24327_gshared (ShimEnumerator_t3162 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m24327(__this, method) (( void (*) (ShimEnumerator_t3162 *, const MethodInfo*))ShimEnumerator_Reset_m24327_gshared)(__this, method)
