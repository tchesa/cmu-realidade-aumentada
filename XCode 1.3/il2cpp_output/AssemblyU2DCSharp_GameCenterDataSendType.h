﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_GameCenterDataSendType.h"

// GameCenterDataSendType
struct  GameCenterDataSendType_t88 
{
	// System.Int32 GameCenterDataSendType::value__
	int32_t ___value___1;
};
