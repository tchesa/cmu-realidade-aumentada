﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_73.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27558_gshared (InternalEnumerator_1_t3361 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27558(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3361 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27558_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27559_gshared (InternalEnumerator_1_t3361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27559(__this, method) (( void (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27559_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27560_gshared (InternalEnumerator_1_t3361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27560(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27560_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27561_gshared (InternalEnumerator_1_t3361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27561(__this, method) (( void (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27561_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27562_gshared (InternalEnumerator_1_t3361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27562(__this, method) (( bool (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27562_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C" GcAchievementData_t1244  InternalEnumerator_1_get_Current_m27563_gshared (InternalEnumerator_1_t3361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27563(__this, method) (( GcAchievementData_t1244  (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27563_gshared)(__this, method)
