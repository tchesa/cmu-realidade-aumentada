﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1209;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_62.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m26028_gshared (Enumerator_t3247 * __this, Dictionary_2_t1209 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m26028(__this, ___host, method) (( void (*) (Enumerator_t3247 *, Dictionary_2_t1209 *, const MethodInfo*))Enumerator__ctor_m26028_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26029_gshared (Enumerator_t3247 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26029(__this, method) (( Object_t * (*) (Enumerator_t3247 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26029_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26030_gshared (Enumerator_t3247 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m26030(__this, method) (( void (*) (Enumerator_t3247 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m26030_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m26031_gshared (Enumerator_t3247 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26031(__this, method) (( void (*) (Enumerator_t3247 *, const MethodInfo*))Enumerator_Dispose_m26031_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26032_gshared (Enumerator_t3247 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26032(__this, method) (( bool (*) (Enumerator_t3247 *, const MethodInfo*))Enumerator_MoveNext_m26032_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t969  Enumerator_get_Current_m26033_gshared (Enumerator_t3247 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26033(__this, method) (( TrackableResultData_t969  (*) (Enumerator_t3247 *, const MethodInfo*))Enumerator_get_Current_m26033_gshared)(__this, method)
