﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Facebook.Unity.IFacebookLogger
struct IFacebookLogger_t297;

#include "mscorlib_System_Object.h"

// Facebook.Unity.FacebookLogger
struct  FacebookLogger_t299  : public Object_t
{
};
struct FacebookLogger_t299_StaticFields{
	// Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger::<Instance>k__BackingField
	Object_t * ___U3CInstanceU3Ek__BackingField_1;
};
