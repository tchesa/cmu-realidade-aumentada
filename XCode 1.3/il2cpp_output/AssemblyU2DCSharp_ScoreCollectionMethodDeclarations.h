﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreCollection
struct ScoreCollection_t122;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreCollection::.ctor()
extern "C" void ScoreCollection__ctor_m712 (ScoreCollection_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
