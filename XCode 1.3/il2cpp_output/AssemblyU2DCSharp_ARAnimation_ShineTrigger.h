﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BrilhoAnimation
struct BrilhoAnimation_t344;

#include "mscorlib_System_Object.h"

// ARAnimation/ShineTrigger
struct  ShineTrigger_t343  : public Object_t
{
	// System.Single ARAnimation/ShineTrigger::turnOn
	float ___turnOn_0;
	// BrilhoAnimation ARAnimation/ShineTrigger::obj
	BrilhoAnimation_t344 * ___obj_1;
};
