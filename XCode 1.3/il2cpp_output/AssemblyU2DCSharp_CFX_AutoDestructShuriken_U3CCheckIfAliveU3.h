﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_t326;

#include "mscorlib_System_Object.h"

// CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB
struct  U3CCheckIfAliveU3Ec__IteratorB_t325  : public Object_t
{
	// System.Int32 CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::$PC
	int32_t ___U24PC_0;
	// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::$current
	Object_t * ___U24current_1;
	// CFX_AutoDestructShuriken CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::<>f__this
	CFX_AutoDestructShuriken_t326 * ___U3CU3Ef__this_2;
};
