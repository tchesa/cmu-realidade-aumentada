﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t428;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C" void ObjectTargetBehaviour__ctor_m2103 (ObjectTargetBehaviour_t428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
