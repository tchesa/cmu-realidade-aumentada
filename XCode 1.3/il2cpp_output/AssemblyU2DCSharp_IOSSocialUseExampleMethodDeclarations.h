﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialUseExample
struct IOSSocialUseExample_t207;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSSocialUseExample::.ctor()
extern "C" void IOSSocialUseExample__ctor_m1149 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::Awake()
extern "C" void IOSSocialUseExample_Awake_m1150 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::InitStyles()
extern "C" void IOSSocialUseExample_InitStyles_m1151 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::OnGUI()
extern "C" void IOSSocialUseExample_OnGUI_m1152 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSSocialUseExample::PostScreenshot()
extern "C" Object_t * IOSSocialUseExample_PostScreenshot_m1153 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSSocialUseExample::PostTwitterScreenshot()
extern "C" Object_t * IOSSocialUseExample_PostTwitterScreenshot_m1154 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSSocialUseExample::PostFBScreenshot()
extern "C" Object_t * IOSSocialUseExample_PostFBScreenshot_m1155 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::OnPostFailed()
extern "C" void IOSSocialUseExample_OnPostFailed_m1156 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::OnPostSuccses()
extern "C" void IOSSocialUseExample_OnPostSuccses_m1157 (IOSSocialUseExample_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::OnMailResult(ISN_Result)
extern "C" void IOSSocialUseExample_OnMailResult_m1158 (IOSSocialUseExample_t207 * __this, ISN_Result_t114 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
