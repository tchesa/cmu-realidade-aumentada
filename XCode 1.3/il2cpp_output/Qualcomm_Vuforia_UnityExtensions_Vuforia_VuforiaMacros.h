﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// Vuforia.VuforiaMacros
struct  VuforiaMacros_t1080 
{
	union
	{
		struct
		{
		};
		uint8_t VuforiaMacros_t1080__padding[1];
	};
};
