﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct EqualityComparer_1_t3559;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void EqualityComparer_1__ctor_m29902_gshared (EqualityComparer_1_t3559 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m29902(__this, method) (( void (*) (EqualityComparer_1_t3559 *, const MethodInfo*))EqualityComparer_1__ctor_m29902_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C" void EqualityComparer_1__cctor_m29903_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m29903(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m29903_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29904_gshared (EqualityComparer_1_t3559 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29904(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3559 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29904_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29905_gshared (EqualityComparer_1_t3559 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29905(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3559 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29905_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::get_Default()
extern "C" EqualityComparer_1_t3559 * EqualityComparer_1_get_Default_m29906_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m29906(__this /* static, unused */, method) (( EqualityComparer_1_t3559 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m29906_gshared)(__this /* static, unused */, method)
