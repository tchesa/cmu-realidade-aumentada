﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>
struct Transform_1_t3265;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_40.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m26191_gshared (Transform_1_t3265 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m26191(__this, ___object, ___method, method) (( void (*) (Transform_1_t3265 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m26191_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t3255  Transform_1_Invoke_m26192_gshared (Transform_1_t3265 * __this, int32_t ___key, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m26192(__this, ___key, ___value, method) (( KeyValuePair_2_t3255  (*) (Transform_1_t3265 *, int32_t, VirtualButtonData_t970 , const MethodInfo*))Transform_1_Invoke_m26192_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m26193_gshared (Transform_1_t3265 * __this, int32_t ___key, VirtualButtonData_t970  ___value, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m26193(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3265 *, int32_t, VirtualButtonData_t970 , AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m26193_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3255  Transform_1_EndInvoke_m26194_gshared (Transform_1_t3265 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m26194(__this, ___result, method) (( KeyValuePair_2_t3255  (*) (Transform_1_t3265 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m26194_gshared)(__this, ___result, method)
