﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15710_gshared (InternalEnumerator_1_t2596 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15710(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2596 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15710_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15711_gshared (InternalEnumerator_1_t2596 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15711(__this, method) (( void (*) (InternalEnumerator_1_t2596 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15711_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15712_gshared (InternalEnumerator_1_t2596 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15712(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2596 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15712_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15713_gshared (InternalEnumerator_1_t2596 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15713(__this, method) (( void (*) (InternalEnumerator_1_t2596 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15713_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15714_gshared (InternalEnumerator_1_t2596 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15714(__this, method) (( bool (*) (InternalEnumerator_1_t2596 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15714_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
extern "C" Vector3_t6  InternalEnumerator_1_get_Current_m15715_gshared (InternalEnumerator_1_t2596 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15715(__this, method) (( Vector3_t6  (*) (InternalEnumerator_1_t2596 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15715_gshared)(__this, method)
