﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1546;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1552;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "mscorlib_System_MulticastDelegate.h"
#include "System_System_Net_Security_SslPolicyErrors.h"

// System.Net.Security.RemoteCertificateValidationCallback
struct  RemoteCertificateValidationCallback_t1514  : public MulticastDelegate_t10
{
};
