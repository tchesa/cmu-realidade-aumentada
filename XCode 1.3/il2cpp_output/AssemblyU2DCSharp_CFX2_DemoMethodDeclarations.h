﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX2_Demo
struct CFX2_Demo_t310;
// UnityEngine.GameObject
struct GameObject_t27;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX2_Demo::.ctor()
extern "C" void CFX2_Demo__ctor_m1706 (CFX2_Demo_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::OnMouseDown()
extern "C" void CFX2_Demo_OnMouseDown_m1707 (CFX2_Demo_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX2_Demo::spawnParticle()
extern "C" GameObject_t27 * CFX2_Demo_spawnParticle_m1708 (CFX2_Demo_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::OnGUI()
extern "C" void CFX2_Demo_OnGUI_m1709 (CFX2_Demo_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX2_Demo::RandomSpawnsCoroutine()
extern "C" Object_t * CFX2_Demo_RandomSpawnsCoroutine_m1710 (CFX2_Demo_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::Update()
extern "C" void CFX2_Demo_Update_m1711 (CFX2_Demo_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::prevParticle()
extern "C" void CFX2_Demo_prevParticle_m1712 (CFX2_Demo_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::nextParticle()
extern "C" void CFX2_Demo_nextParticle_m1713 (CFX2_Demo_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
