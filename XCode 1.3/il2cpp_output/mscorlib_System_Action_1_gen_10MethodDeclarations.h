﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_29MethodDeclarations.h"

// System.Void System.Action`1<IOSNotificationDeviceToken>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2239(__this, ___object, ___method, method) (( void (*) (Action_1_t142 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m17054_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<IOSNotificationDeviceToken>::Invoke(T)
#define Action_1_Invoke_m2243(__this, ___obj, method) (( void (*) (Action_1_t142 *, IOSNotificationDeviceToken_t143 *, const MethodInfo*))Action_1_Invoke_m17055_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<IOSNotificationDeviceToken>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m17624(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t142 *, IOSNotificationDeviceToken_t143 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m17057_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<IOSNotificationDeviceToken>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m17625(__this, ___result, method) (( void (*) (Action_1_t142 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m17059_gshared)(__this, ___result, method)
