﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t400;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C" void CylinderTargetBehaviour__ctor_m2027 (CylinderTargetBehaviour_t400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
