﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSVideoManager
struct IOSVideoManager_t161;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSVideoManager::.ctor()
extern "C" void IOSVideoManager__ctor_m873 (IOSVideoManager_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSVideoManager::_ISN_StreamVideo(System.String)
extern "C" void IOSVideoManager__ISN_StreamVideo_m874 (Object_t * __this /* static, unused */, String_t* ___videoUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSVideoManager::_ISN_OpenYouTubeVideo(System.String)
extern "C" void IOSVideoManager__ISN_OpenYouTubeVideo_m875 (Object_t * __this /* static, unused */, String_t* ___videoUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSVideoManager::PlayStreamingVideo(System.String)
extern "C" void IOSVideoManager_PlayStreamingVideo_m876 (IOSVideoManager_t161 * __this, String_t* ___videoUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSVideoManager::OpenYouTubeVideo(System.String)
extern "C" void IOSVideoManager_OpenYouTubeVideo_m877 (IOSVideoManager_t161 * __this, String_t* ___videoUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
