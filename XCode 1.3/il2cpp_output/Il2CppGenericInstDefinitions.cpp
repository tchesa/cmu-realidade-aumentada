﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType Hashtable_t19_0_0_0;
static const Il2CppType* GenInst_Hashtable_t19_0_0_0_Types[] = { &Hashtable_t19_0_0_0 };
extern const Il2CppGenericInst GenInst_Hashtable_t19_0_0_0 = { 1, GenInst_Hashtable_t19_0_0_0_Types };
extern const Il2CppType GUITexture_t43_0_0_0;
static const Il2CppType* GenInst_GUITexture_t43_0_0_0_Types[] = { &GUITexture_t43_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t43_0_0_0 = { 1, GenInst_GUITexture_t43_0_0_0_Types };
extern const Il2CppType GUIText_t44_0_0_0;
static const Il2CppType* GenInst_GUIText_t44_0_0_0_Types[] = { &GUIText_t44_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t44_0_0_0 = { 1, GenInst_GUIText_t44_0_0_0_Types };
extern const Il2CppType AudioSource_t20_0_0_0;
static const Il2CppType* GenInst_AudioSource_t20_0_0_0_Types[] = { &AudioSource_t20_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t20_0_0_0 = { 1, GenInst_AudioSource_t20_0_0_0_Types };
extern const Il2CppType Vector3_t6_0_0_0;
static const Il2CppType* GenInst_Vector3_t6_0_0_0_Types[] = { &Vector3_t6_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t6_0_0_0 = { 1, GenInst_Vector3_t6_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t59_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t59_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t59_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t59_0_0_0_Types };
extern const Il2CppType iTween_t15_0_0_0;
static const Il2CppType* GenInst_iTween_t15_0_0_0_Types[] = { &iTween_t15_0_0_0 };
extern const Il2CppGenericInst GenInst_iTween_t15_0_0_0 = { 1, GenInst_iTween_t15_0_0_0_Types };
extern const Il2CppType ComponentTracker_t67_0_0_0;
static const Il2CppType* GenInst_ComponentTracker_t67_0_0_0_Types[] = { &ComponentTracker_t67_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentTracker_t67_0_0_0 = { 1, GenInst_ComponentTracker_t67_0_0_0_Types };
extern const Il2CppType ComponentTrack_t66_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ComponentTrack_t66_0_0_0_Types[] = { &String_t_0_0_0, &ComponentTrack_t66_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ComponentTrack_t66_0_0_0 = { 2, GenInst_String_t_0_0_0_ComponentTrack_t66_0_0_0_Types };
extern const Il2CppType List_1_t460_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_List_1_t460_0_0_0_Types[] = { &Int32_t59_0_0_0, &List_1_t460_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_List_1_t460_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_List_1_t460_0_0_0_Types };
extern const Il2CppType EventHandlerFunction_t458_0_0_0;
static const Il2CppType* GenInst_EventHandlerFunction_t458_0_0_0_Types[] = { &EventHandlerFunction_t458_0_0_0 };
extern const Il2CppGenericInst GenInst_EventHandlerFunction_t458_0_0_0 = { 1, GenInst_EventHandlerFunction_t458_0_0_0_Types };
extern const Il2CppType List_1_t461_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_List_1_t461_0_0_0_Types[] = { &Int32_t59_0_0_0, &List_1_t461_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_List_1_t461_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_List_1_t461_0_0_0_Types };
extern const Il2CppType DataEventHandlerFunction_t459_0_0_0;
static const Il2CppType* GenInst_DataEventHandlerFunction_t459_0_0_0_Types[] = { &DataEventHandlerFunction_t459_0_0_0 };
extern const Il2CppGenericInst GenInst_DataEventHandlerFunction_t459_0_0_0 = { 1, GenInst_DataEventHandlerFunction_t459_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType ISDSettings_t76_0_0_0;
static const Il2CppType* GenInst_ISDSettings_t76_0_0_0_Types[] = { &ISDSettings_t76_0_0_0 };
extern const Il2CppGenericInst GenInst_ISDSettings_t76_0_0_0 = { 1, GenInst_ISDSettings_t76_0_0_0_Types };
extern const Il2CppType IOSNativeAppEvents_t80_0_0_0;
static const Il2CppType* GenInst_IOSNativeAppEvents_t80_0_0_0_Types[] = { &IOSNativeAppEvents_t80_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNativeAppEvents_t80_0_0_0 = { 1, GenInst_IOSNativeAppEvents_t80_0_0_0_Types };
extern const Il2CppType IOSNativeSettings_t82_0_0_0;
static const Il2CppType* GenInst_IOSNativeSettings_t82_0_0_0_Types[] = { &IOSNativeSettings_t82_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNativeSettings_t82_0_0_0 = { 1, GenInst_IOSNativeSettings_t82_0_0_0_Types };
extern const Il2CppType ISN_Result_t114_0_0_0;
static const Il2CppType* GenInst_ISN_Result_t114_0_0_0_Types[] = { &ISN_Result_t114_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_Result_t114_0_0_0 = { 1, GenInst_ISN_Result_t114_0_0_0_Types };
extern const Il2CppType ISN_PlayerScoreLoadedResult_t116_0_0_0;
static const Il2CppType* GenInst_ISN_PlayerScoreLoadedResult_t116_0_0_0_Types[] = { &ISN_PlayerScoreLoadedResult_t116_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_PlayerScoreLoadedResult_t116_0_0_0 = { 1, GenInst_ISN_PlayerScoreLoadedResult_t116_0_0_0_Types };
extern const Il2CppType ISN_AchievementProgressResult_t113_0_0_0;
static const Il2CppType* GenInst_ISN_AchievementProgressResult_t113_0_0_0_Types[] = { &ISN_AchievementProgressResult_t113_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_AchievementProgressResult_t113_0_0_0 = { 1, GenInst_ISN_AchievementProgressResult_t113_0_0_0_Types };
extern const Il2CppType ISN_UserInfoLoadResult_t120_0_0_0;
static const Il2CppType* GenInst_ISN_UserInfoLoadResult_t120_0_0_0_Types[] = { &ISN_UserInfoLoadResult_t120_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_UserInfoLoadResult_t120_0_0_0 = { 1, GenInst_ISN_UserInfoLoadResult_t120_0_0_0_Types };
extern const Il2CppType ISN_PlayerSignatureResult_t118_0_0_0;
static const Il2CppType* GenInst_ISN_PlayerSignatureResult_t118_0_0_0_Types[] = { &ISN_PlayerSignatureResult_t118_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_PlayerSignatureResult_t118_0_0_0 = { 1, GenInst_ISN_PlayerSignatureResult_t118_0_0_0_Types };
extern const Il2CppType AchievementTemplate_t115_0_0_0;
static const Il2CppType* GenInst_AchievementTemplate_t115_0_0_0_Types[] = { &AchievementTemplate_t115_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementTemplate_t115_0_0_0 = { 1, GenInst_AchievementTemplate_t115_0_0_0_Types };
extern const Il2CppType GCLeaderboard_t121_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GCLeaderboard_t121_0_0_0_Types[] = { &String_t_0_0_0, &GCLeaderboard_t121_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GCLeaderboard_t121_0_0_0 = { 2, GenInst_String_t_0_0_0_GCLeaderboard_t121_0_0_0_Types };
extern const Il2CppType GameCenterPlayerTemplate_t107_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GameCenterPlayerTemplate_t107_0_0_0_Types[] = { &String_t_0_0_0, &GameCenterPlayerTemplate_t107_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameCenterPlayerTemplate_t107_0_0_0 = { 2, GenInst_String_t_0_0_0_GameCenterPlayerTemplate_t107_0_0_0_Types };
extern const Il2CppType GameCenterManager_t98_0_0_0;
static const Il2CppType* GenInst_GameCenterManager_t98_0_0_0_Types[] = { &GameCenterManager_t98_0_0_0 };
extern const Il2CppGenericInst GenInst_GameCenterManager_t98_0_0_0 = { 1, GenInst_GameCenterManager_t98_0_0_0_Types };
extern const Il2CppType GameCenterMatchData_t109_0_0_0;
static const Il2CppType* GenInst_GameCenterMatchData_t109_0_0_0_Types[] = { &GameCenterMatchData_t109_0_0_0 };
extern const Il2CppGenericInst GenInst_GameCenterMatchData_t109_0_0_0 = { 1, GenInst_GameCenterMatchData_t109_0_0_0_Types };
extern const Il2CppType GameCenterDataPackage_t123_0_0_0;
static const Il2CppType* GenInst_GameCenterDataPackage_t123_0_0_0_Types[] = { &GameCenterDataPackage_t123_0_0_0 };
extern const Il2CppGenericInst GenInst_GameCenterDataPackage_t123_0_0_0 = { 1, GenInst_GameCenterDataPackage_t123_0_0_0_Types };
extern const Il2CppType GameCenterMultiplayer_t108_0_0_0;
static const Il2CppType* GenInst_GameCenterMultiplayer_t108_0_0_0_Types[] = { &GameCenterMultiplayer_t108_0_0_0 };
extern const Il2CppGenericInst GenInst_GameCenterMultiplayer_t108_0_0_0 = { 1, GenInst_GameCenterMultiplayer_t108_0_0_0_Types };
extern const Il2CppType Byte_t560_0_0_0;
static const Il2CppType* GenInst_Byte_t560_0_0_0_Types[] = { &Byte_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t560_0_0_0 = { 1, GenInst_Byte_t560_0_0_0_Types };
extern const Il2CppType GCScore_t117_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_GCScore_t117_0_0_0_Types[] = { &Int32_t59_0_0_0, &GCScore_t117_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_GCScore_t117_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_GCScore_t117_0_0_0_Types };
extern const Il2CppType IOSStoreKitResponse_t137_0_0_0;
static const Il2CppType* GenInst_IOSStoreKitResponse_t137_0_0_0_Types[] = { &IOSStoreKitResponse_t137_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSStoreKitResponse_t137_0_0_0 = { 1, GenInst_IOSStoreKitResponse_t137_0_0_0_Types };
extern const Il2CppType Boolean_t41_0_0_0;
static const Il2CppType* GenInst_Boolean_t41_0_0_0_Types[] = { &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t41_0_0_0 = { 1, GenInst_Boolean_t41_0_0_0_Types };
extern const Il2CppType IOSStoreKitVerificationResponse_t138_0_0_0;
static const Il2CppType* GenInst_IOSStoreKitVerificationResponse_t138_0_0_0_Types[] = { &IOSStoreKitVerificationResponse_t138_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSStoreKitVerificationResponse_t138_0_0_0 = { 1, GenInst_IOSStoreKitVerificationResponse_t138_0_0_0_Types };
extern const Il2CppType IOSProductTemplate_t135_0_0_0;
static const Il2CppType* GenInst_IOSProductTemplate_t135_0_0_0_Types[] = { &IOSProductTemplate_t135_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSProductTemplate_t135_0_0_0 = { 1, GenInst_IOSProductTemplate_t135_0_0_0_Types };
extern const Il2CppType IOSStoreProductView_t134_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_IOSStoreProductView_t134_0_0_0_Types[] = { &Int32_t59_0_0_0, &IOSStoreProductView_t134_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_IOSStoreProductView_t134_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_IOSStoreProductView_t134_0_0_0_Types };
extern const Il2CppType IOSInAppPurchaseManager_t127_0_0_0;
static const Il2CppType* GenInst_IOSInAppPurchaseManager_t127_0_0_0_Types[] = { &IOSInAppPurchaseManager_t127_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSInAppPurchaseManager_t127_0_0_0 = { 1, GenInst_IOSInAppPurchaseManager_t127_0_0_0_Types };
extern const Il2CppType DeviceTokenListener_t139_0_0_0;
static const Il2CppType* GenInst_DeviceTokenListener_t139_0_0_0_Types[] = { &DeviceTokenListener_t139_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceTokenListener_t139_0_0_0 = { 1, GenInst_DeviceTokenListener_t139_0_0_0_Types };
extern const Il2CppType IOSNotificationDeviceToken_t143_0_0_0;
static const Il2CppType* GenInst_IOSNotificationDeviceToken_t143_0_0_0_Types[] = { &IOSNotificationDeviceToken_t143_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNotificationDeviceToken_t143_0_0_0 = { 1, GenInst_IOSNotificationDeviceToken_t143_0_0_0_Types };
extern const Il2CppType IOSNotificationController_t140_0_0_0;
static const Il2CppType* GenInst_IOSNotificationController_t140_0_0_0_Types[] = { &IOSNotificationController_t140_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNotificationController_t140_0_0_0 = { 1, GenInst_IOSNotificationController_t140_0_0_0_Types };
extern const Il2CppType IOSImagePickResult_t149_0_0_0;
static const Il2CppType* GenInst_IOSImagePickResult_t149_0_0_0_Types[] = { &IOSImagePickResult_t149_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSImagePickResult_t149_0_0_0 = { 1, GenInst_IOSImagePickResult_t149_0_0_0_Types };
extern const Il2CppType IOSCamera_t145_0_0_0;
static const Il2CppType* GenInst_IOSCamera_t145_0_0_0_Types[] = { &IOSCamera_t145_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSCamera_t145_0_0_0 = { 1, GenInst_IOSCamera_t145_0_0_0_Types };
extern const Il2CppType DateTime_t220_0_0_0;
static const Il2CppType* GenInst_DateTime_t220_0_0_0_Types[] = { &DateTime_t220_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t220_0_0_0 = { 1, GenInst_DateTime_t220_0_0_0_Types };
extern const Il2CppType IOSDateTimePicker_t150_0_0_0;
static const Il2CppType* GenInst_IOSDateTimePicker_t150_0_0_0_Types[] = { &IOSDateTimePicker_t150_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSDateTimePicker_t150_0_0_0 = { 1, GenInst_IOSDateTimePicker_t150_0_0_0_Types };
extern const Il2CppType IOSSharedApplication_t153_0_0_0;
static const Il2CppType* GenInst_IOSSharedApplication_t153_0_0_0_Types[] = { &IOSSharedApplication_t153_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSSharedApplication_t153_0_0_0 = { 1, GenInst_IOSSharedApplication_t153_0_0_0_Types };
extern const Il2CppType ISN_CheckUrlResult_t156_0_0_0;
static const Il2CppType* GenInst_ISN_CheckUrlResult_t156_0_0_0_Types[] = { &ISN_CheckUrlResult_t156_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_CheckUrlResult_t156_0_0_0 = { 1, GenInst_ISN_CheckUrlResult_t156_0_0_0_Types };
extern const Il2CppType ISN_Security_t157_0_0_0;
static const Il2CppType* GenInst_ISN_Security_t157_0_0_0_Types[] = { &ISN_Security_t157_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_Security_t157_0_0_0 = { 1, GenInst_ISN_Security_t157_0_0_0_Types };
extern const Il2CppType ISN_LocalReceiptResult_t171_0_0_0;
static const Il2CppType* GenInst_ISN_LocalReceiptResult_t171_0_0_0_Types[] = { &ISN_LocalReceiptResult_t171_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_LocalReceiptResult_t171_0_0_0 = { 1, GenInst_ISN_LocalReceiptResult_t171_0_0_0_Types };
extern const Il2CppType ISN_DeviceGUID_t170_0_0_0;
static const Il2CppType* GenInst_ISN_DeviceGUID_t170_0_0_0_Types[] = { &ISN_DeviceGUID_t170_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_DeviceGUID_t170_0_0_0 = { 1, GenInst_ISN_DeviceGUID_t170_0_0_0_Types };
extern const Il2CppType IOSVideoManager_t161_0_0_0;
static const Il2CppType* GenInst_IOSVideoManager_t161_0_0_0_Types[] = { &IOSVideoManager_t161_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSVideoManager_t161_0_0_0 = { 1, GenInst_IOSVideoManager_t161_0_0_0_Types };
extern const Il2CppType IOSDialogResult_t90_0_0_0;
static const Il2CppType* GenInst_IOSDialogResult_t90_0_0_0_Types[] = { &IOSDialogResult_t90_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSDialogResult_t90_0_0_0 = { 1, GenInst_IOSDialogResult_t90_0_0_0_Types };
extern const Il2CppType IOSDialog_t163_0_0_0;
static const Il2CppType* GenInst_IOSDialog_t163_0_0_0_Types[] = { &IOSDialog_t163_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSDialog_t163_0_0_0 = { 1, GenInst_IOSDialog_t163_0_0_0_Types };
extern const Il2CppType IOSMessage_t166_0_0_0;
static const Il2CppType* GenInst_IOSMessage_t166_0_0_0_Types[] = { &IOSMessage_t166_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSMessage_t166_0_0_0 = { 1, GenInst_IOSMessage_t166_0_0_0_Types };
extern const Il2CppType IOSRateUsPopUp_t167_0_0_0;
static const Il2CppType* GenInst_IOSRateUsPopUp_t167_0_0_0_Types[] = { &IOSRateUsPopUp_t167_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSRateUsPopUp_t167_0_0_0 = { 1, GenInst_IOSRateUsPopUp_t167_0_0_0_Types };
extern const Il2CppType IOSSocialManager_t169_0_0_0;
static const Il2CppType* GenInst_IOSSocialManager_t169_0_0_0_Types[] = { &IOSSocialManager_t169_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSSocialManager_t169_0_0_0 = { 1, GenInst_IOSSocialManager_t169_0_0_0_Types };
extern const Il2CppType iAdBanner_t172_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_iAdBanner_t172_0_0_0_Types[] = { &Int32_t59_0_0_0, &iAdBanner_t172_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_iAdBanner_t172_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_iAdBanner_t172_0_0_0_Types };
extern const Il2CppType iAdBannerController_t173_0_0_0;
static const Il2CppType* GenInst_iAdBannerController_t173_0_0_0_Types[] = { &iAdBannerController_t173_0_0_0 };
extern const Il2CppGenericInst GenInst_iAdBannerController_t173_0_0_0 = { 1, GenInst_iAdBannerController_t173_0_0_0_Types };
static const Il2CppType* GenInst_iAdBanner_t172_0_0_0_Types[] = { &iAdBanner_t172_0_0_0 };
extern const Il2CppGenericInst GenInst_iAdBanner_t172_0_0_0 = { 1, GenInst_iAdBanner_t172_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_iAdBanner_t172_0_0_0_Types[] = { &String_t_0_0_0, &iAdBanner_t172_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_iAdBanner_t172_0_0_0 = { 2, GenInst_String_t_0_0_0_iAdBanner_t172_0_0_0_Types };
extern const Il2CppType iCloudData_t179_0_0_0;
static const Il2CppType* GenInst_iCloudData_t179_0_0_0_Types[] = { &iCloudData_t179_0_0_0 };
extern const Il2CppGenericInst GenInst_iCloudData_t179_0_0_0 = { 1, GenInst_iCloudData_t179_0_0_0_Types };
extern const Il2CppType iCloudManager_t180_0_0_0;
static const Il2CppType* GenInst_iCloudManager_t180_0_0_0_Types[] = { &iCloudManager_t180_0_0_0 };
extern const Il2CppGenericInst GenInst_iCloudManager_t180_0_0_0 = { 1, GenInst_iCloudManager_t180_0_0_0_Types };
extern const Il2CppType GameObject_t27_0_0_0;
static const Il2CppType* GenInst_GameObject_t27_0_0_0_Types[] = { &GameObject_t27_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t27_0_0_0 = { 1, GenInst_GameObject_t27_0_0_0_Types };
extern const Il2CppType ConnectionButton_t192_0_0_0;
static const Il2CppType* GenInst_ConnectionButton_t192_0_0_0_Types[] = { &ConnectionButton_t192_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionButton_t192_0_0_0 = { 1, GenInst_ConnectionButton_t192_0_0_0_Types };
extern const Il2CppType DisconnectButton_t193_0_0_0;
static const Il2CppType* GenInst_DisconnectButton_t193_0_0_0_Types[] = { &DisconnectButton_t193_0_0_0 };
extern const Il2CppGenericInst GenInst_DisconnectButton_t193_0_0_0 = { 1, GenInst_DisconnectButton_t193_0_0_0_Types };
extern const Il2CppType ClickManager_t195_0_0_0;
static const Il2CppType* GenInst_ClickManager_t195_0_0_0_Types[] = { &ClickManager_t195_0_0_0 };
extern const Il2CppGenericInst GenInst_ClickManager_t195_0_0_0 = { 1, GenInst_ClickManager_t195_0_0_0_Types };
extern const Il2CppType Renderer_t46_0_0_0;
static const Il2CppType* GenInst_Renderer_t46_0_0_0_Types[] = { &Renderer_t46_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t46_0_0_0 = { 1, GenInst_Renderer_t46_0_0_0_Types };
extern const Il2CppType IOSNativePreviewBackButton_t213_0_0_0;
static const Il2CppType* GenInst_IOSNativePreviewBackButton_t213_0_0_0_Types[] = { &IOSNativePreviewBackButton_t213_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNativePreviewBackButton_t213_0_0_0 = { 1, GenInst_IOSNativePreviewBackButton_t213_0_0_0_Types };
extern const Il2CppType Texture2D_t33_0_0_0;
static const Il2CppType* GenInst_Texture2D_t33_0_0_0_Types[] = { &Texture2D_t33_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t33_0_0_0 = { 1, GenInst_Texture2D_t33_0_0_0_Types };
extern const Il2CppType SA_ScreenShotMaker_t216_0_0_0;
static const Il2CppType* GenInst_SA_ScreenShotMaker_t216_0_0_0_Types[] = { &SA_ScreenShotMaker_t216_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_ScreenShotMaker_t216_0_0_0 = { 1, GenInst_SA_ScreenShotMaker_t216_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType IAppRequestResult_t514_0_0_0;
static const Il2CppType* GenInst_IAppRequestResult_t514_0_0_0_Types[] = { &IAppRequestResult_t514_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppRequestResult_t514_0_0_0 = { 1, GenInst_IAppRequestResult_t514_0_0_0_Types };
extern const Il2CppType IShareResult_t515_0_0_0;
static const Il2CppType* GenInst_IShareResult_t515_0_0_0_Types[] = { &IShareResult_t515_0_0_0 };
extern const Il2CppGenericInst GenInst_IShareResult_t515_0_0_0 = { 1, GenInst_IShareResult_t515_0_0_0_Types };
extern const Il2CppType IGroupCreateResult_t516_0_0_0;
static const Il2CppType* GenInst_IGroupCreateResult_t516_0_0_0_Types[] = { &IGroupCreateResult_t516_0_0_0 };
extern const Il2CppGenericInst GenInst_IGroupCreateResult_t516_0_0_0 = { 1, GenInst_IGroupCreateResult_t516_0_0_0_Types };
extern const Il2CppType IGroupJoinResult_t517_0_0_0;
static const Il2CppType* GenInst_IGroupJoinResult_t517_0_0_0_Types[] = { &IGroupJoinResult_t517_0_0_0 };
extern const Il2CppGenericInst GenInst_IGroupJoinResult_t517_0_0_0 = { 1, GenInst_IGroupJoinResult_t517_0_0_0_Types };
extern const Il2CppType IPayResult_t518_0_0_0;
static const Il2CppType* GenInst_IPayResult_t518_0_0_0_Types[] = { &IPayResult_t518_0_0_0 };
extern const Il2CppGenericInst GenInst_IPayResult_t518_0_0_0 = { 1, GenInst_IPayResult_t518_0_0_0_Types };
extern const Il2CppType IAppInviteResult_t519_0_0_0;
static const Il2CppType* GenInst_IAppInviteResult_t519_0_0_0_Types[] = { &IAppInviteResult_t519_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppInviteResult_t519_0_0_0 = { 1, GenInst_IAppInviteResult_t519_0_0_0_Types };
extern const Il2CppType IAppLinkResult_t520_0_0_0;
static const Il2CppType* GenInst_IAppLinkResult_t520_0_0_0_Types[] = { &IAppLinkResult_t520_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppLinkResult_t520_0_0_0 = { 1, GenInst_IAppLinkResult_t520_0_0_0_Types };
extern const Il2CppType ILoginResult_t489_0_0_0;
static const Il2CppType* GenInst_ILoginResult_t489_0_0_0_Types[] = { &ILoginResult_t489_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoginResult_t489_0_0_0 = { 1, GenInst_ILoginResult_t489_0_0_0_Types };
extern const Il2CppType OGActionType_t265_0_0_0;
static const Il2CppType* GenInst_OGActionType_t265_0_0_0_Types[] = { &OGActionType_t265_0_0_0 };
extern const Il2CppGenericInst GenInst_OGActionType_t265_0_0_0 = { 1, GenInst_OGActionType_t265_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Types[] = { &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0 = { 1, GenInst_Int32_t59_0_0_0_Types };
extern const Il2CppType Single_t36_0_0_0;
static const Il2CppType* GenInst_Single_t36_0_0_0_Types[] = { &Single_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t36_0_0_0 = { 1, GenInst_Single_t36_0_0_0_Types };
extern const Il2CppType IDictionary_2_t225_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t225_0_0_0_Types[] = { &IDictionary_2_t225_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t225_0_0_0 = { 1, GenInst_IDictionary_2_t225_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t529_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t529_0_0_0_Types[] = { &KeyValuePair_2_t529_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t529_0_0_0 = { 1, GenInst_KeyValuePair_2_t529_0_0_0_Types };
extern const Il2CppType JsBridge_t232_0_0_0;
static const Il2CppType* GenInst_JsBridge_t232_0_0_0_Types[] = { &JsBridge_t232_0_0_0 };
extern const Il2CppGenericInst GenInst_JsBridge_t232_0_0_0 = { 1, GenInst_JsBridge_t232_0_0_0_Types };
extern const Il2CppType CanvasFacebookGameObject_t228_0_0_0;
static const Il2CppType* GenInst_CanvasFacebookGameObject_t228_0_0_0_Types[] = { &CanvasFacebookGameObject_t228_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasFacebookGameObject_t228_0_0_0 = { 1, GenInst_CanvasFacebookGameObject_t228_0_0_0_Types };
extern const Il2CppType IOSFacebookLoader_t263_0_0_0;
static const Il2CppType* GenInst_IOSFacebookLoader_t263_0_0_0_Types[] = { &IOSFacebookLoader_t263_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebookLoader_t263_0_0_0 = { 1, GenInst_IOSFacebookLoader_t263_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t287_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t287_0_0_0_Types[] = { &KeyValuePair_2_t287_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t287_0_0_0 = { 1, GenInst_KeyValuePair_2_t287_0_0_0_Types };
extern const Il2CppType UrlSchemes_t245_0_0_0;
static const Il2CppType* GenInst_UrlSchemes_t245_0_0_0_Types[] = { &UrlSchemes_t245_0_0_0 };
extern const Il2CppGenericInst GenInst_UrlSchemes_t245_0_0_0 = { 1, GenInst_UrlSchemes_t245_0_0_0_Types };
extern const Il2CppType FacebookSettings_t246_0_0_0;
static const Il2CppType* GenInst_FacebookSettings_t246_0_0_0_Types[] = { &FacebookSettings_t246_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookSettings_t246_0_0_0 = { 1, GenInst_FacebookSettings_t246_0_0_0_Types };
extern const Il2CppType IResult_t464_0_0_0;
static const Il2CppType* GenInst_IResult_t464_0_0_0_Types[] = { &IResult_t464_0_0_0 };
extern const Il2CppGenericInst GenInst_IResult_t464_0_0_0 = { 1, GenInst_IResult_t464_0_0_0_Types };
extern const Il2CppType AndroidFacebookGameObject_t252_0_0_0;
static const Il2CppType* GenInst_AndroidFacebookGameObject_t252_0_0_0_Types[] = { &AndroidFacebookGameObject_t252_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidFacebookGameObject_t252_0_0_0 = { 1, GenInst_AndroidFacebookGameObject_t252_0_0_0_Types };
extern const Il2CppType IOSFacebookGameObject_t262_0_0_0;
static const Il2CppType* GenInst_IOSFacebookGameObject_t262_0_0_0_Types[] = { &IOSFacebookGameObject_t262_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebookGameObject_t262_0_0_0 = { 1, GenInst_IOSFacebookGameObject_t262_0_0_0_Types };
extern const Il2CppType EditorFacebookGameObject_t267_0_0_0;
static const Il2CppType* GenInst_EditorFacebookGameObject_t267_0_0_0_Types[] = { &EditorFacebookGameObject_t267_0_0_0 };
extern const Il2CppGenericInst GenInst_EditorFacebookGameObject_t267_0_0_0 = { 1, GenInst_EditorFacebookGameObject_t267_0_0_0_Types };
extern const Il2CppType MockLoginDialog_t273_0_0_0;
static const Il2CppType* GenInst_MockLoginDialog_t273_0_0_0_Types[] = { &MockLoginDialog_t273_0_0_0 };
extern const Il2CppGenericInst GenInst_MockLoginDialog_t273_0_0_0 = { 1, GenInst_MockLoginDialog_t273_0_0_0_Types };
extern const Il2CppType IGraphResult_t484_0_0_0;
static const Il2CppType* GenInst_IGraphResult_t484_0_0_0_Types[] = { &IGraphResult_t484_0_0_0 };
extern const Il2CppGenericInst GenInst_IGraphResult_t484_0_0_0 = { 1, GenInst_IGraphResult_t484_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t476_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t476_0_0_0_Types[] = { &IEnumerable_1_t476_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t476_0_0_0 = { 1, GenInst_IEnumerable_1_t476_0_0_0_Types };
extern const Il2CppType Int64_t507_0_0_0;
static const Il2CppType* GenInst_Int64_t507_0_0_0_Types[] = { &Int64_t507_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t507_0_0_0 = { 1, GenInst_Int64_t507_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_String_t_0_0_0_Types[] = { &Object_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_String_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType AsyncRequestString_t290_0_0_0;
static const Il2CppType* GenInst_AsyncRequestString_t290_0_0_0_Types[] = { &AsyncRequestString_t290_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncRequestString_t290_0_0_0 = { 1, GenInst_AsyncRequestString_t290_0_0_0_Types };
extern const Il2CppType Collider_t316_0_0_0;
static const Il2CppType* GenInst_Collider_t316_0_0_0_Types[] = { &Collider_t316_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t316_0_0_0 = { 1, GenInst_Collider_t316_0_0_0_Types };
extern const Il2CppType ParticleSystem_t332_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t332_0_0_0_Types[] = { &ParticleSystem_t332_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t332_0_0_0 = { 1, GenInst_ParticleSystem_t332_0_0_0_Types };
extern const Il2CppType CFX3_Demo_Translate_t317_0_0_0;
static const Il2CppType* GenInst_CFX3_Demo_Translate_t317_0_0_0_Types[] = { &CFX3_Demo_Translate_t317_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX3_Demo_Translate_t317_0_0_0 = { 1, GenInst_CFX3_Demo_Translate_t317_0_0_0_Types };
extern const Il2CppType CFX3_AutoStopLoopedEffect_t312_0_0_0;
static const Il2CppType* GenInst_CFX3_AutoStopLoopedEffect_t312_0_0_0_Types[] = { &CFX3_AutoStopLoopedEffect_t312_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX3_AutoStopLoopedEffect_t312_0_0_0 = { 1, GenInst_CFX3_AutoStopLoopedEffect_t312_0_0_0_Types };
extern const Il2CppType CFX_AutoDestructShuriken_t326_0_0_0;
static const Il2CppType* GenInst_CFX_AutoDestructShuriken_t326_0_0_0_Types[] = { &CFX_AutoDestructShuriken_t326_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_AutoDestructShuriken_t326_0_0_0 = { 1, GenInst_CFX_AutoDestructShuriken_t326_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t36_0_0_0_Types[] = { &String_t_0_0_0, &Single_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t36_0_0_0 = { 2, GenInst_String_t_0_0_0_Single_t36_0_0_0_Types };
extern const Il2CppType Light_t47_0_0_0;
static const Il2CppType* GenInst_Light_t47_0_0_0_Types[] = { &Light_t47_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t47_0_0_0 = { 1, GenInst_Light_t47_0_0_0_Types };
extern const Il2CppType List_1_t198_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_List_1_t198_0_0_0_Types[] = { &Int32_t59_0_0_0, &List_1_t198_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_List_1_t198_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_List_1_t198_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_Types[] = { &Int32_t59_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_Types };
extern const Il2CppType CFX_LightIntensityFade_t329_0_0_0;
static const Il2CppType* GenInst_CFX_LightIntensityFade_t329_0_0_0_Types[] = { &CFX_LightIntensityFade_t329_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_LightIntensityFade_t329_0_0_0 = { 1, GenInst_CFX_LightIntensityFade_t329_0_0_0_Types };
extern const Il2CppType TrailTrigger_t341_0_0_0;
static const Il2CppType* GenInst_TrailTrigger_t341_0_0_0_Types[] = { &TrailTrigger_t341_0_0_0 };
extern const Il2CppGenericInst GenInst_TrailTrigger_t341_0_0_0 = { 1, GenInst_TrailTrigger_t341_0_0_0_Types };
extern const Il2CppType ShineTrigger_t343_0_0_0;
static const Il2CppType* GenInst_ShineTrigger_t343_0_0_0_Types[] = { &ShineTrigger_t343_0_0_0 };
extern const Il2CppGenericInst GenInst_ShineTrigger_t343_0_0_0 = { 1, GenInst_ShineTrigger_t343_0_0_0_Types };
extern const Il2CppType TransparencyTrigger_t339_0_0_0;
static const Il2CppType* GenInst_TransparencyTrigger_t339_0_0_0_Types[] = { &TransparencyTrigger_t339_0_0_0 };
extern const Il2CppGenericInst GenInst_TransparencyTrigger_t339_0_0_0 = { 1, GenInst_TransparencyTrigger_t339_0_0_0_Types };
extern const Il2CppType BoltAnimation_t378_0_0_0;
static const Il2CppType* GenInst_BoltAnimation_t378_0_0_0_Types[] = { &BoltAnimation_t378_0_0_0 };
extern const Il2CppGenericInst GenInst_BoltAnimation_t378_0_0_0 = { 1, GenInst_BoltAnimation_t378_0_0_0_Types };
extern const Il2CppType SkinnedMeshRenderer_t359_0_0_0;
static const Il2CppType* GenInst_SkinnedMeshRenderer_t359_0_0_0_Types[] = { &SkinnedMeshRenderer_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_SkinnedMeshRenderer_t359_0_0_0 = { 1, GenInst_SkinnedMeshRenderer_t359_0_0_0_Types };
extern const Il2CppType Animator_t360_0_0_0;
static const Il2CppType* GenInst_Animator_t360_0_0_0_Types[] = { &Animator_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t360_0_0_0 = { 1, GenInst_Animator_t360_0_0_0_Types };
extern const Il2CppType AudioListener_t576_0_0_0;
static const Il2CppType* GenInst_AudioListener_t576_0_0_0_Types[] = { &AudioListener_t576_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioListener_t576_0_0_0 = { 1, GenInst_AudioListener_t576_0_0_0_Types };
extern const Il2CppType TimeEvent_t362_0_0_0;
static const Il2CppType* GenInst_TimeEvent_t362_0_0_0_Types[] = { &TimeEvent_t362_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeEvent_t362_0_0_0 = { 1, GenInst_TimeEvent_t362_0_0_0_Types };
extern const Il2CppType RawImage_t578_0_0_0;
static const Il2CppType* GenInst_RawImage_t578_0_0_0_Types[] = { &RawImage_t578_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t578_0_0_0 = { 1, GenInst_RawImage_t578_0_0_0_Types };
extern const Il2CppType CanvasGroup_t579_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t579_0_0_0_Types[] = { &CanvasGroup_t579_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t579_0_0_0 = { 1, GenInst_CanvasGroup_t579_0_0_0_Types };
extern const Il2CppType Text_t580_0_0_0;
static const Il2CppType* GenInst_Text_t580_0_0_0_Types[] = { &Text_t580_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t580_0_0_0 = { 1, GenInst_Text_t580_0_0_0_Types };
extern const Il2CppType Popup_t384_0_0_0;
static const Il2CppType* GenInst_Popup_t384_0_0_0_Types[] = { &Popup_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Popup_t384_0_0_0 = { 1, GenInst_Popup_t384_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t41_0_0_0_ILoginResult_t489_0_0_0_Types[] = { &Boolean_t41_0_0_0, &ILoginResult_t489_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t41_0_0_0_ILoginResult_t489_0_0_0 = { 2, GenInst_Boolean_t41_0_0_0_ILoginResult_t489_0_0_0_Types };
extern const Il2CppType FBServices_t381_0_0_0;
static const Il2CppType* GenInst_FBServices_t381_0_0_0_Types[] = { &FBServices_t381_0_0_0 };
extern const Il2CppGenericInst GenInst_FBServices_t381_0_0_0 = { 1, GenInst_FBServices_t381_0_0_0_Types };
extern const Il2CppType Image_t387_0_0_0;
static const Il2CppType* GenInst_Image_t387_0_0_0_Types[] = { &Image_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t387_0_0_0 = { 1, GenInst_Image_t387_0_0_0_Types };
extern const Il2CppType ScrollRect_t581_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t581_0_0_0_Types[] = { &ScrollRect_t581_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t581_0_0_0 = { 1, GenInst_ScrollRect_t581_0_0_0_Types };
extern const Il2CppType Camera_t510_0_0_0;
static const Il2CppType* GenInst_Camera_t510_0_0_0_Types[] = { &Camera_t510_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t510_0_0_0 = { 1, GenInst_Camera_t510_0_0_0_Types };
extern const Il2CppType InitError_t1071_0_0_0;
static const Il2CppType* GenInst_InitError_t1071_0_0_0_Types[] = { &InitError_t1071_0_0_0 };
extern const Il2CppGenericInst GenInst_InitError_t1071_0_0_0 = { 1, GenInst_InitError_t1071_0_0_0_Types };
extern const Il2CppType ReconstructionBehaviour_t406_0_0_0;
static const Il2CppType* GenInst_ReconstructionBehaviour_t406_0_0_0_Types[] = { &ReconstructionBehaviour_t406_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t406_0_0_0 = { 1, GenInst_ReconstructionBehaviour_t406_0_0_0_Types };
extern const Il2CppType Prop_t491_0_0_0;
static const Il2CppType* GenInst_Prop_t491_0_0_0_Types[] = { &Prop_t491_0_0_0 };
extern const Il2CppGenericInst GenInst_Prop_t491_0_0_0 = { 1, GenInst_Prop_t491_0_0_0_Types };
extern const Il2CppType Surface_t492_0_0_0;
static const Il2CppType* GenInst_Surface_t492_0_0_0_Types[] = { &Surface_t492_0_0_0 };
extern const Il2CppGenericInst GenInst_Surface_t492_0_0_0 = { 1, GenInst_Surface_t492_0_0_0_Types };
extern const Il2CppType TrackableBehaviour_t410_0_0_0;
static const Il2CppType* GenInst_TrackableBehaviour_t410_0_0_0_Types[] = { &TrackableBehaviour_t410_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t410_0_0_0 = { 1, GenInst_TrackableBehaviour_t410_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType MaskOutBehaviour_t424_0_0_0;
static const Il2CppType* GenInst_MaskOutBehaviour_t424_0_0_0_Types[] = { &MaskOutBehaviour_t424_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t424_0_0_0 = { 1, GenInst_MaskOutBehaviour_t424_0_0_0_Types };
extern const Il2CppType VirtualButtonBehaviour_t448_0_0_0;
static const Il2CppType* GenInst_VirtualButtonBehaviour_t448_0_0_0_Types[] = { &VirtualButtonBehaviour_t448_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t448_0_0_0 = { 1, GenInst_VirtualButtonBehaviour_t448_0_0_0_Types };
extern const Il2CppType TurnOffBehaviour_t439_0_0_0;
static const Il2CppType* GenInst_TurnOffBehaviour_t439_0_0_0_Types[] = { &TurnOffBehaviour_t439_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t439_0_0_0 = { 1, GenInst_TurnOffBehaviour_t439_0_0_0_Types };
extern const Il2CppType ImageTargetBehaviour_t414_0_0_0;
static const Il2CppType* GenInst_ImageTargetBehaviour_t414_0_0_0_Types[] = { &ImageTargetBehaviour_t414_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t414_0_0_0 = { 1, GenInst_ImageTargetBehaviour_t414_0_0_0_Types };
extern const Il2CppType MarkerBehaviour_t422_0_0_0;
static const Il2CppType* GenInst_MarkerBehaviour_t422_0_0_0_Types[] = { &MarkerBehaviour_t422_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerBehaviour_t422_0_0_0 = { 1, GenInst_MarkerBehaviour_t422_0_0_0_Types };
extern const Il2CppType MultiTargetBehaviour_t426_0_0_0;
static const Il2CppType* GenInst_MultiTargetBehaviour_t426_0_0_0_Types[] = { &MultiTargetBehaviour_t426_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t426_0_0_0 = { 1, GenInst_MultiTargetBehaviour_t426_0_0_0_Types };
extern const Il2CppType CylinderTargetBehaviour_t400_0_0_0;
static const Il2CppType* GenInst_CylinderTargetBehaviour_t400_0_0_0_Types[] = { &CylinderTargetBehaviour_t400_0_0_0 };
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t400_0_0_0 = { 1, GenInst_CylinderTargetBehaviour_t400_0_0_0_Types };
extern const Il2CppType WordBehaviour_t456_0_0_0;
static const Il2CppType* GenInst_WordBehaviour_t456_0_0_0_Types[] = { &WordBehaviour_t456_0_0_0 };
extern const Il2CppGenericInst GenInst_WordBehaviour_t456_0_0_0 = { 1, GenInst_WordBehaviour_t456_0_0_0_Types };
extern const Il2CppType TextRecoBehaviour_t437_0_0_0;
static const Il2CppType* GenInst_TextRecoBehaviour_t437_0_0_0_Types[] = { &TextRecoBehaviour_t437_0_0_0 };
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t437_0_0_0 = { 1, GenInst_TextRecoBehaviour_t437_0_0_0_Types };
extern const Il2CppType ObjectTargetBehaviour_t428_0_0_0;
static const Il2CppType* GenInst_ObjectTargetBehaviour_t428_0_0_0_Types[] = { &ObjectTargetBehaviour_t428_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t428_0_0_0 = { 1, GenInst_ObjectTargetBehaviour_t428_0_0_0_Types };
extern const Il2CppType MeshRenderer_t596_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t596_0_0_0_Types[] = { &MeshRenderer_t596_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t596_0_0_0 = { 1, GenInst_MeshRenderer_t596_0_0_0_Types };
extern const Il2CppType MeshFilter_t597_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t597_0_0_0_Types[] = { &MeshFilter_t597_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t597_0_0_0 = { 1, GenInst_MeshFilter_t597_0_0_0_Types };
extern const Il2CppType ComponentFactoryStarterBehaviour_t417_0_0_0;
static const Il2CppType* GenInst_ComponentFactoryStarterBehaviour_t417_0_0_0_Types[] = { &ComponentFactoryStarterBehaviour_t417_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t417_0_0_0 = { 1, GenInst_ComponentFactoryStarterBehaviour_t417_0_0_0_Types };
extern const Il2CppType VuforiaBehaviour_t450_0_0_0;
static const Il2CppType* GenInst_VuforiaBehaviour_t450_0_0_0_Types[] = { &VuforiaBehaviour_t450_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaBehaviour_t450_0_0_0 = { 1, GenInst_VuforiaBehaviour_t450_0_0_0_Types };
extern const Il2CppType WireframeBehaviour_t454_0_0_0;
static const Il2CppType* GenInst_WireframeBehaviour_t454_0_0_0_Types[] = { &WireframeBehaviour_t454_0_0_0 };
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t454_0_0_0 = { 1, GenInst_WireframeBehaviour_t454_0_0_0_Types };
extern const Il2CppType BaseInputModule_t614_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t614_0_0_0_Types[] = { &BaseInputModule_t614_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t614_0_0_0 = { 1, GenInst_BaseInputModule_t614_0_0_0_Types };
extern const Il2CppType RaycastResult_t647_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t647_0_0_0_Types[] = { &RaycastResult_t647_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t647_0_0_0 = { 1, GenInst_RaycastResult_t647_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t813_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t813_0_0_0_Types[] = { &IDeselectHandler_t813_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t813_0_0_0 = { 1, GenInst_IDeselectHandler_t813_0_0_0_Types };
extern const Il2CppType ISelectHandler_t812_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t812_0_0_0_Types[] = { &ISelectHandler_t812_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t812_0_0_0 = { 1, GenInst_ISelectHandler_t812_0_0_0_Types };
extern const Il2CppType BaseEventData_t615_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t615_0_0_0_Types[] = { &BaseEventData_t615_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t615_0_0_0 = { 1, GenInst_BaseEventData_t615_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t800_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t800_0_0_0_Types[] = { &IPointerEnterHandler_t800_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t800_0_0_0 = { 1, GenInst_IPointerEnterHandler_t800_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t801_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t801_0_0_0_Types[] = { &IPointerExitHandler_t801_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t801_0_0_0 = { 1, GenInst_IPointerExitHandler_t801_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t802_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t802_0_0_0_Types[] = { &IPointerDownHandler_t802_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t802_0_0_0 = { 1, GenInst_IPointerDownHandler_t802_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t803_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t803_0_0_0_Types[] = { &IPointerUpHandler_t803_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t803_0_0_0 = { 1, GenInst_IPointerUpHandler_t803_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t804_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t804_0_0_0_Types[] = { &IPointerClickHandler_t804_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t804_0_0_0 = { 1, GenInst_IPointerClickHandler_t804_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t805_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t805_0_0_0_Types[] = { &IInitializePotentialDragHandler_t805_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t805_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t805_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t806_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t806_0_0_0_Types[] = { &IBeginDragHandler_t806_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t806_0_0_0 = { 1, GenInst_IBeginDragHandler_t806_0_0_0_Types };
extern const Il2CppType IDragHandler_t807_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t807_0_0_0_Types[] = { &IDragHandler_t807_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t807_0_0_0 = { 1, GenInst_IDragHandler_t807_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t808_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t808_0_0_0_Types[] = { &IEndDragHandler_t808_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t808_0_0_0 = { 1, GenInst_IEndDragHandler_t808_0_0_0_Types };
extern const Il2CppType IDropHandler_t809_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t809_0_0_0_Types[] = { &IDropHandler_t809_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t809_0_0_0 = { 1, GenInst_IDropHandler_t809_0_0_0_Types };
extern const Il2CppType IScrollHandler_t810_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t810_0_0_0_Types[] = { &IScrollHandler_t810_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t810_0_0_0 = { 1, GenInst_IScrollHandler_t810_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t811_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t811_0_0_0_Types[] = { &IUpdateSelectedHandler_t811_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t811_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t811_0_0_0_Types };
extern const Il2CppType IMoveHandler_t814_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t814_0_0_0_Types[] = { &IMoveHandler_t814_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t814_0_0_0 = { 1, GenInst_IMoveHandler_t814_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t815_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t815_0_0_0_Types[] = { &ISubmitHandler_t815_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t815_0_0_0 = { 1, GenInst_ISubmitHandler_t815_0_0_0_Types };
extern const Il2CppType ICancelHandler_t816_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t816_0_0_0_Types[] = { &ICancelHandler_t816_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t816_0_0_0 = { 1, GenInst_ICancelHandler_t816_0_0_0_Types };
extern const Il2CppType List_1_t799_0_0_0;
static const Il2CppType* GenInst_List_1_t799_0_0_0_Types[] = { &List_1_t799_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t799_0_0_0 = { 1, GenInst_List_1_t799_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2864_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2864_0_0_0_Types[] = { &IEventSystemHandler_t2864_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2864_0_0_0 = { 1, GenInst_IEventSystemHandler_t2864_0_0_0_Types };
extern const Il2CppType Transform_t25_0_0_0;
static const Il2CppType* GenInst_Transform_t25_0_0_0_Types[] = { &Transform_t25_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t25_0_0_0 = { 1, GenInst_Transform_t25_0_0_0_Types };
extern const Il2CppType PointerEventData_t652_0_0_0;
static const Il2CppType* GenInst_PointerEventData_t652_0_0_0_Types[] = { &PointerEventData_t652_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t652_0_0_0 = { 1, GenInst_PointerEventData_t652_0_0_0_Types };
extern const Il2CppType AxisEventData_t649_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t649_0_0_0_Types[] = { &AxisEventData_t649_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t649_0_0_0 = { 1, GenInst_AxisEventData_t649_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t648_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t648_0_0_0_Types[] = { &BaseRaycaster_t648_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t648_0_0_0 = { 1, GenInst_BaseRaycaster_t648_0_0_0_Types };
extern const Il2CppType EventSystem_t611_0_0_0;
static const Il2CppType* GenInst_EventSystem_t611_0_0_0_Types[] = { &EventSystem_t611_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t611_0_0_0 = { 1, GenInst_EventSystem_t611_0_0_0_Types };
extern const Il2CppType ButtonState_t654_0_0_0;
static const Il2CppType* GenInst_ButtonState_t654_0_0_0_Types[] = { &ButtonState_t654_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t654_0_0_0 = { 1, GenInst_ButtonState_t654_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_PointerEventData_t652_0_0_0_Types[] = { &Int32_t59_0_0_0, &PointerEventData_t652_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_PointerEventData_t652_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_PointerEventData_t652_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t836_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t836_0_0_0_Types[] = { &SpriteRenderer_t836_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t836_0_0_0 = { 1, GenInst_SpriteRenderer_t836_0_0_0_Types };
extern const Il2CppType RaycastHit_t566_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t566_0_0_0_Types[] = { &RaycastHit_t566_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t566_0_0_0 = { 1, GenInst_RaycastHit_t566_0_0_0_Types };
extern const Il2CppType Color_t5_0_0_0;
static const Il2CppType* GenInst_Color_t5_0_0_0_Types[] = { &Color_t5_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t5_0_0_0 = { 1, GenInst_Color_t5_0_0_0_Types };
extern const Il2CppType ICanvasElement_t820_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t820_0_0_0_Types[] = { &ICanvasElement_t820_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t820_0_0_0 = { 1, GenInst_ICanvasElement_t820_0_0_0_Types };
extern const Il2CppType Font_t684_0_0_0;
extern const Il2CppType List_1_t842_0_0_0;
static const Il2CppType* GenInst_Font_t684_0_0_0_List_1_t842_0_0_0_Types[] = { &Font_t684_0_0_0, &List_1_t842_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t684_0_0_0_List_1_t842_0_0_0 = { 2, GenInst_Font_t684_0_0_0_List_1_t842_0_0_0_Types };
static const Il2CppType* GenInst_Font_t684_0_0_0_Types[] = { &Font_t684_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t684_0_0_0 = { 1, GenInst_Font_t684_0_0_0_Types };
extern const Il2CppType ColorTween_t670_0_0_0;
static const Il2CppType* GenInst_ColorTween_t670_0_0_0_Types[] = { &ColorTween_t670_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t670_0_0_0 = { 1, GenInst_ColorTween_t670_0_0_0_Types };
extern const Il2CppType List_1_t724_0_0_0;
static const Il2CppType* GenInst_List_1_t724_0_0_0_Types[] = { &List_1_t724_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t724_0_0_0 = { 1, GenInst_List_1_t724_0_0_0_Types };
extern const Il2CppType UIVertex_t727_0_0_0;
static const Il2CppType* GenInst_UIVertex_t727_0_0_0_Types[] = { &UIVertex_t727_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t727_0_0_0 = { 1, GenInst_UIVertex_t727_0_0_0_Types };
extern const Il2CppType RectTransform_t688_0_0_0;
static const Il2CppType* GenInst_RectTransform_t688_0_0_0_Types[] = { &RectTransform_t688_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t688_0_0_0 = { 1, GenInst_RectTransform_t688_0_0_0_Types };
extern const Il2CppType Canvas_t690_0_0_0;
static const Il2CppType* GenInst_Canvas_t690_0_0_0_Types[] = { &Canvas_t690_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t690_0_0_0 = { 1, GenInst_Canvas_t690_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t689_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t689_0_0_0_Types[] = { &CanvasRenderer_t689_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t689_0_0_0 = { 1, GenInst_CanvasRenderer_t689_0_0_0_Types };
extern const Il2CppType Component_t56_0_0_0;
static const Il2CppType* GenInst_Component_t56_0_0_0_Types[] = { &Component_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t56_0_0_0 = { 1, GenInst_Component_t56_0_0_0_Types };
extern const Il2CppType Graphic_t687_0_0_0;
static const Il2CppType* GenInst_Graphic_t687_0_0_0_Types[] = { &Graphic_t687_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t687_0_0_0 = { 1, GenInst_Graphic_t687_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t852_0_0_0;
static const Il2CppType* GenInst_Canvas_t690_0_0_0_IndexedSet_1_t852_0_0_0_Types[] = { &Canvas_t690_0_0_0, &IndexedSet_1_t852_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t690_0_0_0_IndexedSet_1_t852_0_0_0 = { 2, GenInst_Canvas_t690_0_0_0_IndexedSet_1_t852_0_0_0_Types };
extern const Il2CppType Sprite_t709_0_0_0;
static const Il2CppType* GenInst_Sprite_t709_0_0_0_Types[] = { &Sprite_t709_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t709_0_0_0 = { 1, GenInst_Sprite_t709_0_0_0_Types };
extern const Il2CppType Type_t701_0_0_0;
static const Il2CppType* GenInst_Type_t701_0_0_0_Types[] = { &Type_t701_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t701_0_0_0 = { 1, GenInst_Type_t701_0_0_0_Types };
extern const Il2CppType FillMethod_t702_0_0_0;
static const Il2CppType* GenInst_FillMethod_t702_0_0_0_Types[] = { &FillMethod_t702_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t702_0_0_0 = { 1, GenInst_FillMethod_t702_0_0_0_Types };
extern const Il2CppType SubmitEvent_t714_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t714_0_0_0_Types[] = { &SubmitEvent_t714_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t714_0_0_0 = { 1, GenInst_SubmitEvent_t714_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t716_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t716_0_0_0_Types[] = { &OnChangeEvent_t716_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t716_0_0_0 = { 1, GenInst_OnChangeEvent_t716_0_0_0_Types };
extern const Il2CppType OnValidateInput_t718_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t718_0_0_0_Types[] = { &OnValidateInput_t718_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t718_0_0_0 = { 1, GenInst_OnValidateInput_t718_0_0_0_Types };
extern const Il2CppType ContentType_t710_0_0_0;
static const Il2CppType* GenInst_ContentType_t710_0_0_0_Types[] = { &ContentType_t710_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t710_0_0_0 = { 1, GenInst_ContentType_t710_0_0_0_Types };
extern const Il2CppType LineType_t713_0_0_0;
static const Il2CppType* GenInst_LineType_t713_0_0_0_Types[] = { &LineType_t713_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t713_0_0_0 = { 1, GenInst_LineType_t713_0_0_0_Types };
extern const Il2CppType InputType_t711_0_0_0;
static const Il2CppType* GenInst_InputType_t711_0_0_0_Types[] = { &InputType_t711_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t711_0_0_0 = { 1, GenInst_InputType_t711_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t854_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t854_0_0_0_Types[] = { &TouchScreenKeyboardType_t854_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t854_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t854_0_0_0_Types };
extern const Il2CppType CharacterValidation_t712_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t712_0_0_0_Types[] = { &CharacterValidation_t712_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t712_0_0_0 = { 1, GenInst_CharacterValidation_t712_0_0_0_Types };
extern const Il2CppType Char_t556_0_0_0;
static const Il2CppType* GenInst_Char_t556_0_0_0_Types[] = { &Char_t556_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t556_0_0_0 = { 1, GenInst_Char_t556_0_0_0_Types };
extern const Il2CppType UILineInfo_t857_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t857_0_0_0_Types[] = { &UILineInfo_t857_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t857_0_0_0 = { 1, GenInst_UILineInfo_t857_0_0_0_Types };
extern const Il2CppType UICharInfo_t859_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t859_0_0_0_Types[] = { &UICharInfo_t859_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t859_0_0_0 = { 1, GenInst_UICharInfo_t859_0_0_0_Types };
extern const Il2CppType LayoutElement_t779_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t779_0_0_0_Types[] = { &LayoutElement_t779_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t779_0_0_0 = { 1, GenInst_LayoutElement_t779_0_0_0_Types };
extern const Il2CppType Direction_t732_0_0_0;
static const Il2CppType* GenInst_Direction_t732_0_0_0_Types[] = { &Direction_t732_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t732_0_0_0 = { 1, GenInst_Direction_t732_0_0_0_Types };
extern const Il2CppType Vector2_t29_0_0_0;
static const Il2CppType* GenInst_Vector2_t29_0_0_0_Types[] = { &Vector2_t29_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t29_0_0_0 = { 1, GenInst_Vector2_t29_0_0_0_Types };
extern const Il2CppType Selectable_t676_0_0_0;
static const Il2CppType* GenInst_Selectable_t676_0_0_0_Types[] = { &Selectable_t676_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t676_0_0_0 = { 1, GenInst_Selectable_t676_0_0_0_Types };
extern const Il2CppType Navigation_t730_0_0_0;
static const Il2CppType* GenInst_Navigation_t730_0_0_0_Types[] = { &Navigation_t730_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t730_0_0_0 = { 1, GenInst_Navigation_t730_0_0_0_Types };
extern const Il2CppType Transition_t743_0_0_0;
static const Il2CppType* GenInst_Transition_t743_0_0_0_Types[] = { &Transition_t743_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t743_0_0_0 = { 1, GenInst_Transition_t743_0_0_0_Types };
extern const Il2CppType ColorBlock_t682_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t682_0_0_0_Types[] = { &ColorBlock_t682_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t682_0_0_0 = { 1, GenInst_ColorBlock_t682_0_0_0_Types };
extern const Il2CppType SpriteState_t745_0_0_0;
static const Il2CppType* GenInst_SpriteState_t745_0_0_0_Types[] = { &SpriteState_t745_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t745_0_0_0 = { 1, GenInst_SpriteState_t745_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t671_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t671_0_0_0_Types[] = { &AnimationTriggers_t671_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t671_0_0_0 = { 1, GenInst_AnimationTriggers_t671_0_0_0_Types };
extern const Il2CppType Direction_t749_0_0_0;
static const Il2CppType* GenInst_Direction_t749_0_0_0_Types[] = { &Direction_t749_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t749_0_0_0 = { 1, GenInst_Direction_t749_0_0_0_Types };
extern const Il2CppType MatEntry_t753_0_0_0;
static const Il2CppType* GenInst_MatEntry_t753_0_0_0_Types[] = { &MatEntry_t753_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t753_0_0_0 = { 1, GenInst_MatEntry_t753_0_0_0_Types };
extern const Il2CppType Toggle_t759_0_0_0;
static const Il2CppType* GenInst_Toggle_t759_0_0_0_Types[] = { &Toggle_t759_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t759_0_0_0 = { 1, GenInst_Toggle_t759_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t759_0_0_0_Boolean_t41_0_0_0_Types[] = { &Toggle_t759_0_0_0, &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t759_0_0_0_Boolean_t41_0_0_0 = { 2, GenInst_Toggle_t759_0_0_0_Boolean_t41_0_0_0_Types };
extern const Il2CppType AspectMode_t764_0_0_0;
static const Il2CppType* GenInst_AspectMode_t764_0_0_0_Types[] = { &AspectMode_t764_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t764_0_0_0 = { 1, GenInst_AspectMode_t764_0_0_0_Types };
extern const Il2CppType FitMode_t770_0_0_0;
static const Il2CppType* GenInst_FitMode_t770_0_0_0_Types[] = { &FitMode_t770_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t770_0_0_0 = { 1, GenInst_FitMode_t770_0_0_0_Types };
extern const Il2CppType Corner_t772_0_0_0;
static const Il2CppType* GenInst_Corner_t772_0_0_0_Types[] = { &Corner_t772_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t772_0_0_0 = { 1, GenInst_Corner_t772_0_0_0_Types };
extern const Il2CppType Axis_t773_0_0_0;
static const Il2CppType* GenInst_Axis_t773_0_0_0_Types[] = { &Axis_t773_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t773_0_0_0 = { 1, GenInst_Axis_t773_0_0_0_Types };
extern const Il2CppType Constraint_t774_0_0_0;
static const Il2CppType* GenInst_Constraint_t774_0_0_0_Types[] = { &Constraint_t774_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t774_0_0_0 = { 1, GenInst_Constraint_t774_0_0_0_Types };
extern const Il2CppType RectOffset_t780_0_0_0;
static const Il2CppType* GenInst_RectOffset_t780_0_0_0_Types[] = { &RectOffset_t780_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t780_0_0_0 = { 1, GenInst_RectOffset_t780_0_0_0_Types };
extern const Il2CppType TextAnchor_t869_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t869_0_0_0_Types[] = { &TextAnchor_t869_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t869_0_0_0 = { 1, GenInst_TextAnchor_t869_0_0_0_Types };
extern const Il2CppType ILayoutElement_t827_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t827_0_0_0_Single_t36_0_0_0_Types[] = { &ILayoutElement_t827_0_0_0, &Single_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t827_0_0_0_Single_t36_0_0_0 = { 2, GenInst_ILayoutElement_t827_0_0_0_Single_t36_0_0_0_Types };
extern const Il2CppType List_1_t828_0_0_0;
static const Il2CppType* GenInst_List_1_t828_0_0_0_Types[] = { &List_1_t828_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t828_0_0_0 = { 1, GenInst_List_1_t828_0_0_0_Types };
extern const Il2CppType List_1_t826_0_0_0;
static const Il2CppType* GenInst_List_1_t826_0_0_0_Types[] = { &List_1_t826_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t826_0_0_0 = { 1, GenInst_List_1_t826_0_0_0_Types };
extern const Il2CppType VideoBackgroundAbstractBehaviour_t445_0_0_0;
static const Il2CppType* GenInst_Camera_t510_0_0_0_VideoBackgroundAbstractBehaviour_t445_0_0_0_Types[] = { &Camera_t510_0_0_0, &VideoBackgroundAbstractBehaviour_t445_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t510_0_0_0_VideoBackgroundAbstractBehaviour_t445_0_0_0 = { 2, GenInst_Camera_t510_0_0_0_VideoBackgroundAbstractBehaviour_t445_0_0_0_Types };
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t397_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneAbstractBehaviour_t397_0_0_0_Types[] = { &BackgroundPlaneAbstractBehaviour_t397_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t397_0_0_0 = { 1, GenInst_BackgroundPlaneAbstractBehaviour_t397_0_0_0_Types };
static const Il2CppType* GenInst_VideoBackgroundAbstractBehaviour_t445_0_0_0_Types[] = { &VideoBackgroundAbstractBehaviour_t445_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t445_0_0_0 = { 1, GenInst_VideoBackgroundAbstractBehaviour_t445_0_0_0_Types };
extern const Il2CppType ITrackableEventHandler_t1091_0_0_0;
static const Il2CppType* GenInst_ITrackableEventHandler_t1091_0_0_0_Types[] = { &ITrackableEventHandler_t1091_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t1091_0_0_0 = { 1, GenInst_ITrackableEventHandler_t1091_0_0_0_Types };
extern const Il2CppType SmartTerrainTracker_t1003_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTracker_t1003_0_0_0_Types[] = { &SmartTerrainTracker_t1003_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t1003_0_0_0 = { 1, GenInst_SmartTerrainTracker_t1003_0_0_0_Types };
extern const Il2CppType ReconstructionAbstractBehaviour_t431_0_0_0;
static const Il2CppType* GenInst_ReconstructionAbstractBehaviour_t431_0_0_0_Types[] = { &ReconstructionAbstractBehaviour_t431_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t431_0_0_0 = { 1, GenInst_ReconstructionAbstractBehaviour_t431_0_0_0_Types };
extern const Il2CppType ICloudRecoEventHandler_t1092_0_0_0;
static const Il2CppType* GenInst_ICloudRecoEventHandler_t1092_0_0_0_Types[] = { &ICloudRecoEventHandler_t1092_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t1092_0_0_0 = { 1, GenInst_ICloudRecoEventHandler_t1092_0_0_0_Types };
extern const Il2CppType TargetSearchResult_t1050_0_0_0;
static const Il2CppType* GenInst_TargetSearchResult_t1050_0_0_0_Types[] = { &TargetSearchResult_t1050_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t1050_0_0_0 = { 1, GenInst_TargetSearchResult_t1050_0_0_0_Types };
extern const Il2CppType ObjectTracker_t899_0_0_0;
static const Il2CppType* GenInst_ObjectTracker_t899_0_0_0_Types[] = { &ObjectTracker_t899_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTracker_t899_0_0_0 = { 1, GenInst_ObjectTracker_t899_0_0_0_Types };
extern const Il2CppType HideExcessAreaAbstractBehaviour_t413_0_0_0;
static const Il2CppType* GenInst_HideExcessAreaAbstractBehaviour_t413_0_0_0_Types[] = { &HideExcessAreaAbstractBehaviour_t413_0_0_0 };
extern const Il2CppGenericInst GenInst_HideExcessAreaAbstractBehaviour_t413_0_0_0 = { 1, GenInst_HideExcessAreaAbstractBehaviour_t413_0_0_0_Types };
extern const Il2CppType ReconstructionFromTarget_t910_0_0_0;
static const Il2CppType* GenInst_ReconstructionFromTarget_t910_0_0_0_Types[] = { &ReconstructionFromTarget_t910_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t910_0_0_0 = { 1, GenInst_ReconstructionFromTarget_t910_0_0_0_Types };
extern const Il2CppType VirtualButton_t1061_0_0_0;
static const Il2CppType* GenInst_VirtualButton_t1061_0_0_0_Types[] = { &VirtualButton_t1061_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t1061_0_0_0 = { 1, GenInst_VirtualButton_t1061_0_0_0_Types };
extern const Il2CppType PIXEL_FORMAT_t942_0_0_0;
extern const Il2CppType Image_t943_0_0_0;
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_Image_t943_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0, &Image_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0_Image_t943_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t942_0_0_0_Image_t943_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0 = { 1, GenInst_PIXEL_FORMAT_t942_0_0_0_Types };
extern const Il2CppType Trackable_t891_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_Trackable_t891_0_0_0_Types[] = { &Int32_t59_0_0_0, &Trackable_t891_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Trackable_t891_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_Trackable_t891_0_0_0_Types };
static const Il2CppType* GenInst_Trackable_t891_0_0_0_Types[] = { &Trackable_t891_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t891_0_0_0 = { 1, GenInst_Trackable_t891_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButton_t1061_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButton_t1061_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButton_t1061_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_VirtualButton_t1061_0_0_0_Types };
extern const Il2CppType DataSet_t922_0_0_0;
static const Il2CppType* GenInst_DataSet_t922_0_0_0_Types[] = { &DataSet_t922_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSet_t922_0_0_0 = { 1, GenInst_DataSet_t922_0_0_0_Types };
extern const Il2CppType DataSetImpl_t906_0_0_0;
static const Il2CppType* GenInst_DataSetImpl_t906_0_0_0_Types[] = { &DataSetImpl_t906_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetImpl_t906_0_0_0 = { 1, GenInst_DataSetImpl_t906_0_0_0_Types };
extern const Il2CppType Marker_t1069_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_Marker_t1069_0_0_0_Types[] = { &Int32_t59_0_0_0, &Marker_t1069_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Marker_t1069_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_Marker_t1069_0_0_0_Types };
static const Il2CppType* GenInst_Marker_t1069_0_0_0_Types[] = { &Marker_t1069_0_0_0 };
extern const Il2CppGenericInst GenInst_Marker_t1069_0_0_0 = { 1, GenInst_Marker_t1069_0_0_0_Types };
extern const Il2CppType TrackableResultData_t969_0_0_0;
static const Il2CppType* GenInst_TrackableResultData_t969_0_0_0_Types[] = { &TrackableResultData_t969_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t969_0_0_0 = { 1, GenInst_TrackableResultData_t969_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackable_t919_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackable_t919_0_0_0_Types[] = { &SmartTerrainTrackable_t919_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t919_0_0_0 = { 1, GenInst_SmartTerrainTrackable_t919_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackableBehaviour_t917_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackableBehaviour_t917_0_0_0_Types[] = { &SmartTerrainTrackableBehaviour_t917_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t917_0_0_0 = { 1, GenInst_SmartTerrainTrackableBehaviour_t917_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType UInt16_t562_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t562_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t562_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t562_0_0_0 = { 2, GenInst_Type_t_0_0_0_UInt16_t562_0_0_0_Types };
extern const Il2CppType WordResult_t1024_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_WordResult_t1024_0_0_0_Types[] = { &Int32_t59_0_0_0, &WordResult_t1024_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_WordResult_t1024_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_WordResult_t1024_0_0_0_Types };
extern const Il2CppType WordAbstractBehaviour_t457_0_0_0;
static const Il2CppType* GenInst_WordAbstractBehaviour_t457_0_0_0_Types[] = { &WordAbstractBehaviour_t457_0_0_0 };
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t457_0_0_0 = { 1, GenInst_WordAbstractBehaviour_t457_0_0_0_Types };
extern const Il2CppType List_1_t1022_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1022_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1022_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1022_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1022_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_WordAbstractBehaviour_t457_0_0_0_Types[] = { &Int32_t59_0_0_0, &WordAbstractBehaviour_t457_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_WordAbstractBehaviour_t457_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_WordAbstractBehaviour_t457_0_0_0_Types };
extern const Il2CppType WordData_t974_0_0_0;
static const Il2CppType* GenInst_WordData_t974_0_0_0_Types[] = { &WordData_t974_0_0_0 };
extern const Il2CppGenericInst GenInst_WordData_t974_0_0_0 = { 1, GenInst_WordData_t974_0_0_0_Types };
extern const Il2CppType WordResultData_t973_0_0_0;
static const Il2CppType* GenInst_WordResultData_t973_0_0_0_Types[] = { &WordResultData_t973_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResultData_t973_0_0_0 = { 1, GenInst_WordResultData_t973_0_0_0_Types };
extern const Il2CppType Word_t1026_0_0_0;
static const Il2CppType* GenInst_Word_t1026_0_0_0_Types[] = { &Word_t1026_0_0_0 };
extern const Il2CppGenericInst GenInst_Word_t1026_0_0_0 = { 1, GenInst_Word_t1026_0_0_0_Types };
static const Il2CppType* GenInst_WordResult_t1024_0_0_0_Types[] = { &WordResult_t1024_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResult_t1024_0_0_0 = { 1, GenInst_WordResult_t1024_0_0_0_Types };
extern const Il2CppType ILoadLevelEventHandler_t1107_0_0_0;
static const Il2CppType* GenInst_ILoadLevelEventHandler_t1107_0_0_0_Types[] = { &ILoadLevelEventHandler_t1107_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoadLevelEventHandler_t1107_0_0_0 = { 1, GenInst_ILoadLevelEventHandler_t1107_0_0_0_Types };
extern const Il2CppType SmartTerrainInitializationInfo_t916_0_0_0;
static const Il2CppType* GenInst_SmartTerrainInitializationInfo_t916_0_0_0_Types[] = { &SmartTerrainInitializationInfo_t916_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t916_0_0_0 = { 1, GenInst_SmartTerrainInitializationInfo_t916_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Prop_t491_0_0_0_Types[] = { &Int32_t59_0_0_0, &Prop_t491_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Prop_t491_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_Prop_t491_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Surface_t492_0_0_0_Types[] = { &Int32_t59_0_0_0, &Surface_t492_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Surface_t492_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_Surface_t492_0_0_0_Types };
extern const Il2CppType ISmartTerrainEventHandler_t1110_0_0_0;
static const Il2CppType* GenInst_ISmartTerrainEventHandler_t1110_0_0_0_Types[] = { &ISmartTerrainEventHandler_t1110_0_0_0 };
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t1110_0_0_0 = { 1, GenInst_ISmartTerrainEventHandler_t1110_0_0_0_Types };
extern const Il2CppType PropAbstractBehaviour_t430_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_PropAbstractBehaviour_t430_0_0_0_Types[] = { &Int32_t59_0_0_0, &PropAbstractBehaviour_t430_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_PropAbstractBehaviour_t430_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_PropAbstractBehaviour_t430_0_0_0_Types };
extern const Il2CppType SurfaceAbstractBehaviour_t436_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_SurfaceAbstractBehaviour_t436_0_0_0_Types[] = { &Int32_t59_0_0_0, &SurfaceAbstractBehaviour_t436_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_SurfaceAbstractBehaviour_t436_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_SurfaceAbstractBehaviour_t436_0_0_0_Types };
static const Il2CppType* GenInst_PropAbstractBehaviour_t430_0_0_0_Types[] = { &PropAbstractBehaviour_t430_0_0_0 };
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t430_0_0_0 = { 1, GenInst_PropAbstractBehaviour_t430_0_0_0_Types };
static const Il2CppType* GenInst_SurfaceAbstractBehaviour_t436_0_0_0_Types[] = { &SurfaceAbstractBehaviour_t436_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t436_0_0_0 = { 1, GenInst_SurfaceAbstractBehaviour_t436_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_TrackableBehaviour_t410_0_0_0_Types[] = { &Int32_t59_0_0_0, &TrackableBehaviour_t410_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_TrackableBehaviour_t410_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_TrackableBehaviour_t410_0_0_0_Types };
extern const Il2CppType MarkerTracker_t956_0_0_0;
static const Il2CppType* GenInst_MarkerTracker_t956_0_0_0_Types[] = { &MarkerTracker_t956_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerTracker_t956_0_0_0 = { 1, GenInst_MarkerTracker_t956_0_0_0_Types };
extern const Il2CppType DataSetTrackableBehaviour_t892_0_0_0;
static const Il2CppType* GenInst_DataSetTrackableBehaviour_t892_0_0_0_Types[] = { &DataSetTrackableBehaviour_t892_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t892_0_0_0 = { 1, GenInst_DataSetTrackableBehaviour_t892_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_Types[] = { &Int32_t59_0_0_0, &TrackableResultData_t969_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_Types };
extern const Il2CppType VirtualButtonData_t970_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButtonData_t970_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_Types };
extern const Il2CppType VirtualButtonAbstractBehaviour_t449_0_0_0;
static const Il2CppType* GenInst_VirtualButtonAbstractBehaviour_t449_0_0_0_Types[] = { &VirtualButtonAbstractBehaviour_t449_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t449_0_0_0 = { 1, GenInst_VirtualButtonAbstractBehaviour_t449_0_0_0_Types };
extern const Il2CppType ImageTarget_t1063_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_ImageTarget_t1063_0_0_0_Types[] = { &Int32_t59_0_0_0, &ImageTarget_t1063_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_ImageTarget_t1063_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_ImageTarget_t1063_0_0_0_Types };
static const Il2CppType* GenInst_ImageTarget_t1063_0_0_0_Types[] = { &ImageTarget_t1063_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTarget_t1063_0_0_0 = { 1, GenInst_ImageTarget_t1063_0_0_0_Types };
extern const Il2CppType ImageTargetAbstractBehaviour_t415_0_0_0;
static const Il2CppType* GenInst_ImageTargetAbstractBehaviour_t415_0_0_0_Types[] = { &ImageTargetAbstractBehaviour_t415_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t415_0_0_0 = { 1, GenInst_ImageTargetAbstractBehaviour_t415_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButtonAbstractBehaviour_t449_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButtonAbstractBehaviour_t449_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButtonAbstractBehaviour_t449_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_VirtualButtonAbstractBehaviour_t449_0_0_0_Types };
extern const Il2CppType ITrackerEventHandler_t1118_0_0_0;
static const Il2CppType* GenInst_ITrackerEventHandler_t1118_0_0_0_Types[] = { &ITrackerEventHandler_t1118_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t1118_0_0_0 = { 1, GenInst_ITrackerEventHandler_t1118_0_0_0_Types };
extern const Il2CppType TextTracker_t1005_0_0_0;
static const Il2CppType* GenInst_TextTracker_t1005_0_0_0_Types[] = { &TextTracker_t1005_0_0_0 };
extern const Il2CppGenericInst GenInst_TextTracker_t1005_0_0_0 = { 1, GenInst_TextTracker_t1005_0_0_0_Types };
extern const Il2CppType WebCamAbstractBehaviour_t453_0_0_0;
static const Il2CppType* GenInst_WebCamAbstractBehaviour_t453_0_0_0_Types[] = { &WebCamAbstractBehaviour_t453_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamAbstractBehaviour_t453_0_0_0 = { 1, GenInst_WebCamAbstractBehaviour_t453_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IVideoBackgroundEventHandler_t1119_0_0_0;
static const Il2CppType* GenInst_IVideoBackgroundEventHandler_t1119_0_0_0_Types[] = { &IVideoBackgroundEventHandler_t1119_0_0_0 };
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t1119_0_0_0 = { 1, GenInst_IVideoBackgroundEventHandler_t1119_0_0_0_Types };
extern const Il2CppType ITextRecoEventHandler_t1120_0_0_0;
static const Il2CppType* GenInst_ITextRecoEventHandler_t1120_0_0_0_Types[] = { &ITextRecoEventHandler_t1120_0_0_0 };
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t1120_0_0_0 = { 1, GenInst_ITextRecoEventHandler_t1120_0_0_0_Types };
extern const Il2CppType IUserDefinedTargetEventHandler_t1121_0_0_0;
static const Il2CppType* GenInst_IUserDefinedTargetEventHandler_t1121_0_0_0_Types[] = { &IUserDefinedTargetEventHandler_t1121_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t1121_0_0_0 = { 1, GenInst_IUserDefinedTargetEventHandler_t1121_0_0_0_Types };
extern const Il2CppType VuforiaAbstractBehaviour_t451_0_0_0;
static const Il2CppType* GenInst_VuforiaAbstractBehaviour_t451_0_0_0_Types[] = { &VuforiaAbstractBehaviour_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaAbstractBehaviour_t451_0_0_0 = { 1, GenInst_VuforiaAbstractBehaviour_t451_0_0_0_Types };
extern const Il2CppType IVirtualButtonEventHandler_t1122_0_0_0;
static const Il2CppType* GenInst_IVirtualButtonEventHandler_t1122_0_0_0_Types[] = { &IVirtualButtonEventHandler_t1122_0_0_0 };
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t1122_0_0_0 = { 1, GenInst_IVirtualButtonEventHandler_t1122_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t1358_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t1358_0_0_0_Types[] = { &GcLeaderboard_t1358_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t1358_0_0_0 = { 1, GenInst_GcLeaderboard_t1358_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t1451_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t1451_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t1451_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1451_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t1451_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t1453_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t1453_0_0_0_Types[] = { &IAchievementU5BU5D_t1453_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1453_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1453_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t1268_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t1268_0_0_0_Types[] = { &IScoreU5BU5D_t1268_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t1268_0_0_0 = { 1, GenInst_IScoreU5BU5D_t1268_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t1263_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t1263_0_0_0_Types[] = { &IUserProfileU5BU5D_t1263_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t1263_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t1263_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t59_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType GUILayer_t1375_0_0_0;
static const Il2CppType* GenInst_GUILayer_t1375_0_0_0_Types[] = { &GUILayer_t1375_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t1375_0_0_0 = { 1, GenInst_GUILayer_t1375_0_0_0_Types };
extern const Il2CppType PersistentCall_t1296_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t1296_0_0_0_Types[] = { &PersistentCall_t1296_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t1296_0_0_0 = { 1, GenInst_PersistentCall_t1296_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t1293_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t1293_0_0_0_Types[] = { &BaseInvokableCall_t1293_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1293_0_0_0 = { 1, GenInst_BaseInvokableCall_t1293_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t1314_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t1314_0_0_0_Types[] = { &Rigidbody2D_t1314_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t1314_0_0_0 = { 1, GenInst_Rigidbody2D_t1314_0_0_0_Types };
extern const Il2CppType Terrain_t1334_0_0_0;
static const Il2CppType* GenInst_Terrain_t1334_0_0_0_Types[] = { &Terrain_t1334_0_0_0 };
extern const Il2CppGenericInst GenInst_Terrain_t1334_0_0_0 = { 1, GenInst_Terrain_t1334_0_0_0_Types };
extern const Il2CppType LayoutCache_t1382_0_0_0;
static const Il2CppType* GenInst_Int32_t59_0_0_0_LayoutCache_t1382_0_0_0_Types[] = { &Int32_t59_0_0_0, &LayoutCache_t1382_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_LayoutCache_t1382_0_0_0 = { 2, GenInst_Int32_t59_0_0_0_LayoutCache_t1382_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t1385_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t1385_0_0_0_Types[] = { &GUILayoutEntry_t1385_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t1385_0_0_0 = { 1, GenInst_GUILayoutEntry_t1385_0_0_0_Types };
extern const Il2CppType GUIStyle_t184_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t184_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t184_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t184_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t184_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t119_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t119_0_0_0_Types[] = { &ByteU5BU5D_t119_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t119_0_0_0 = { 1, GenInst_ByteU5BU5D_t119_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t41_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t41_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t41_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t2101_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t2101_0_0_0_Types[] = { &CustomAttributeTypedArgument_t2101_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2101_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t2101_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t2100_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t2100_0_0_0_Types[] = { &CustomAttributeNamedArgument_t2100_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t2100_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t2100_0_0_0_Types };
extern const Il2CppType StrongName_t2348_0_0_0;
static const Il2CppType* GenInst_StrongName_t2348_0_0_0_Types[] = { &StrongName_t2348_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2348_0_0_0 = { 1, GenInst_StrongName_t2348_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t2423_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t2423_0_0_0_Types[] = { &DateTimeOffset_t2423_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t2423_0_0_0 = { 1, GenInst_DateTimeOffset_t2423_0_0_0_Types };
extern const Il2CppType TimeSpan_t565_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t565_0_0_0_Types[] = { &TimeSpan_t565_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t565_0_0_0 = { 1, GenInst_TimeSpan_t565_0_0_0_Types };
extern const Il2CppType Guid_t61_0_0_0;
static const Il2CppType* GenInst_Guid_t61_0_0_0_Types[] = { &Guid_t61_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t61_0_0_0 = { 1, GenInst_Guid_t61_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t2097_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t2097_0_0_0_Types[] = { &CustomAttributeData_t2097_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t2097_0_0_0 = { 1, GenInst_CustomAttributeData_t2097_0_0_0_Types };
extern const Il2CppType IReflect_t3957_0_0_0;
static const Il2CppType* GenInst_IReflect_t3957_0_0_0_Types[] = { &IReflect_t3957_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3957_0_0_0 = { 1, GenInst_IReflect_t3957_0_0_0_Types };
extern const Il2CppType _Type_t3955_0_0_0;
static const Il2CppType* GenInst__Type_t3955_0_0_0_Types[] = { &_Type_t3955_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t3955_0_0_0 = { 1, GenInst__Type_t3955_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t2530_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t2530_0_0_0_Types[] = { &ICustomAttributeProvider_t2530_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2530_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t2530_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3956_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3956_0_0_0_Types[] = { &_MemberInfo_t3956_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3956_0_0_0 = { 1, GenInst__MemberInfo_t3956_0_0_0_Types };
extern const Il2CppType IFormattable_t2532_0_0_0;
static const Il2CppType* GenInst_IFormattable_t2532_0_0_0_Types[] = { &IFormattable_t2532_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t2532_0_0_0 = { 1, GenInst_IFormattable_t2532_0_0_0_Types };
extern const Il2CppType IConvertible_t2535_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2535_0_0_0_Types[] = { &IConvertible_t2535_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2535_0_0_0 = { 1, GenInst_IConvertible_t2535_0_0_0_Types };
extern const Il2CppType IComparable_t2534_0_0_0;
static const Il2CppType* GenInst_IComparable_t2534_0_0_0_Types[] = { &IComparable_t2534_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t2534_0_0_0 = { 1, GenInst_IComparable_t2534_0_0_0_Types };
extern const Il2CppType IComparable_1_t4166_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4166_0_0_0_Types[] = { &IComparable_1_t4166_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4166_0_0_0 = { 1, GenInst_IComparable_1_t4166_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4171_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4171_0_0_0_Types[] = { &IEquatable_1_t4171_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4171_0_0_0 = { 1, GenInst_IEquatable_1_t4171_0_0_0_Types };
extern const Il2CppType ValueType_t1848_0_0_0;
static const Il2CppType* GenInst_ValueType_t1848_0_0_0_Types[] = { &ValueType_t1848_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1848_0_0_0 = { 1, GenInst_ValueType_t1848_0_0_0_Types };
extern const Il2CppType Double_t60_0_0_0;
static const Il2CppType* GenInst_Double_t60_0_0_0_Types[] = { &Double_t60_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t60_0_0_0 = { 1, GenInst_Double_t60_0_0_0_Types };
extern const Il2CppType IComparable_1_t4183_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4183_0_0_0_Types[] = { &IComparable_1_t4183_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4183_0_0_0 = { 1, GenInst_IComparable_1_t4183_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4188_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4188_0_0_0_Types[] = { &IEquatable_1_t4188_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4188_0_0_0 = { 1, GenInst_IEquatable_1_t4188_0_0_0_Types };
extern const Il2CppType IComparable_1_t4196_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4196_0_0_0_Types[] = { &IComparable_1_t4196_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4196_0_0_0 = { 1, GenInst_IComparable_1_t4196_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4201_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4201_0_0_0_Types[] = { &IEquatable_1_t4201_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4201_0_0_0 = { 1, GenInst_IEquatable_1_t4201_0_0_0_Types };
extern const Il2CppType IComparable_1_t4221_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4221_0_0_0_Types[] = { &IComparable_1_t4221_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4221_0_0_0 = { 1, GenInst_IComparable_1_t4221_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4226_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4226_0_0_0_Types[] = { &IEquatable_1_t4226_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4226_0_0_0 = { 1, GenInst_IEquatable_1_t4226_0_0_0_Types };
extern const Il2CppType Rect_t30_0_0_0;
static const Il2CppType* GenInst_Rect_t30_0_0_0_Types[] = { &Rect_t30_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t30_0_0_0 = { 1, GenInst_Rect_t30_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t59_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2607_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2607_0_0_0_Types[] = { &KeyValuePair_2_t2607_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2607_0_0_0 = { 1, GenInst_KeyValuePair_2_t2607_0_0_0_Types };
extern const Il2CppType IEnumerable_t557_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t557_0_0_0_Types[] = { &IEnumerable_t557_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t557_0_0_0 = { 1, GenInst_IEnumerable_t557_0_0_0_Types };
extern const Il2CppType ICloneable_t2545_0_0_0;
static const Il2CppType* GenInst_ICloneable_t2545_0_0_0_Types[] = { &ICloneable_t2545_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t2545_0_0_0 = { 1, GenInst_ICloneable_t2545_0_0_0_Types };
extern const Il2CppType IComparable_1_t4246_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4246_0_0_0_Types[] = { &IComparable_1_t4246_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4246_0_0_0 = { 1, GenInst_IComparable_1_t4246_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4251_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4251_0_0_0_Types[] = { &IEquatable_1_t4251_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4251_0_0_0 = { 1, GenInst_IEquatable_1_t4251_0_0_0_Types };
extern const Il2CppType Link_t1956_0_0_0;
static const Il2CppType* GenInst_Link_t1956_0_0_0_Types[] = { &Link_t1956_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1956_0_0_0 = { 1, GenInst_Link_t1956_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t59_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Int32_t59_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t59_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Int32_t59_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Int32_t59_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t58_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t59_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t58_0_0_0_Types[] = { &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t58_0_0_0 = { 1, GenInst_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t59_0_0_0_KeyValuePair_2_t2607_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t59_0_0_0, &KeyValuePair_2_t2607_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t59_0_0_0_KeyValuePair_2_t2607_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t59_0_0_0_KeyValuePair_2_t2607_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t59_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2624_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2624_0_0_0_Types[] = { &KeyValuePair_2_t2624_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2624_0_0_0 = { 1, GenInst_KeyValuePair_2_t2624_0_0_0_Types };
extern const Il2CppType Object_t53_0_0_0;
static const Il2CppType* GenInst_Object_t53_0_0_0_Types[] = { &Object_t53_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t53_0_0_0 = { 1, GenInst_Object_t53_0_0_0_Types };
extern const Il2CppType Material_t45_0_0_0;
static const Il2CppType* GenInst_Material_t45_0_0_0_Types[] = { &Material_t45_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t45_0_0_0 = { 1, GenInst_Material_t45_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2634_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2634_0_0_0_Types[] = { &KeyValuePair_2_t2634_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2634_0_0_0 = { 1, GenInst_KeyValuePair_2_t2634_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2634_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2634_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2634_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2634_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ComponentTrack_t66_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &ComponentTrack_t66_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ComponentTrack_t66_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_ComponentTrack_t66_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2644_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2644_0_0_0_Types[] = { &KeyValuePair_2_t2644_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2644_0_0_0 = { 1, GenInst_KeyValuePair_2_t2644_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2658_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2658_0_0_0_Types[] = { &KeyValuePair_2_t2658_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2658_0_0_0 = { 1, GenInst_KeyValuePair_2_t2658_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Int32_t59_0_0_0_Types[] = { &Int32_t59_0_0_0, &Object_t_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Int32_t59_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Int32_t59_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t59_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2658_0_0_0_Types[] = { &Int32_t59_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2658_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2658_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2658_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_List_1_t460_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &List_1_t460_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_List_1_t460_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_List_1_t460_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2669_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2669_0_0_0_Types[] = { &KeyValuePair_2_t2669_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2669_0_0_0 = { 1, GenInst_KeyValuePair_2_t2669_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_List_1_t461_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &List_1_t461_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_List_1_t461_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_List_1_t461_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2681_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2681_0_0_0_Types[] = { &KeyValuePair_2_t2681_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2681_0_0_0 = { 1, GenInst_KeyValuePair_2_t2681_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameCenterPlayerTemplate_t107_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &GameCenterPlayerTemplate_t107_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameCenterPlayerTemplate_t107_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_GameCenterPlayerTemplate_t107_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2697_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2697_0_0_0_Types[] = { &KeyValuePair_2_t2697_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2697_0_0_0 = { 1, GenInst_KeyValuePair_2_t2697_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GCLeaderboard_t121_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &GCLeaderboard_t121_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GCLeaderboard_t121_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_GCLeaderboard_t121_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2704_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2704_0_0_0_Types[] = { &KeyValuePair_2_t2704_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2704_0_0_0 = { 1, GenInst_KeyValuePair_2_t2704_0_0_0_Types };
extern const Il2CppType IComparable_1_t4299_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4299_0_0_0_Types[] = { &IComparable_1_t4299_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4299_0_0_0 = { 1, GenInst_IComparable_1_t4299_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4304_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4304_0_0_0_Types[] = { &IEquatable_1_t4304_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4304_0_0_0 = { 1, GenInst_IEquatable_1_t4304_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_GCScore_t117_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &GCScore_t117_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_GCScore_t117_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_GCScore_t117_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2723_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2723_0_0_0_Types[] = { &KeyValuePair_2_t2723_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2723_0_0_0 = { 1, GenInst_KeyValuePair_2_t2723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_IOSStoreProductView_t134_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &IOSStoreProductView_t134_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_IOSStoreProductView_t134_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_IOSStoreProductView_t134_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2734_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2734_0_0_0_Types[] = { &KeyValuePair_2_t2734_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2734_0_0_0 = { 1, GenInst_KeyValuePair_2_t2734_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_iAdBanner_t172_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &iAdBanner_t172_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_iAdBanner_t172_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_iAdBanner_t172_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t502_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t502_0_0_0_Types[] = { &KeyValuePair_2_t502_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t502_0_0_0 = { 1, GenInst_KeyValuePair_2_t502_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_iAdBanner_t172_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &iAdBanner_t172_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_iAdBanner_t172_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_iAdBanner_t172_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2748_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2748_0_0_0_Types[] = { &KeyValuePair_2_t2748_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2748_0_0_0 = { 1, GenInst_KeyValuePair_2_t2748_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t553_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t553_0_0_0_Types[] = { &GUILayoutOption_t553_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t553_0_0_0 = { 1, GenInst_GUILayoutOption_t553_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t36_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t36_0_0_0 = { 2, GenInst_Object_t_0_0_0_Single_t36_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2787_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2787_0_0_0_Types[] = { &KeyValuePair_2_t2787_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2787_0_0_0 = { 1, GenInst_KeyValuePair_2_t2787_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t36_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t36_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t36_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Single_t36_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t36_0_0_0_Single_t36_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t36_0_0_0, &Single_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t36_0_0_0_Single_t36_0_0_0 = { 3, GenInst_Object_t_0_0_0_Single_t36_0_0_0_Single_t36_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t36_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t36_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t36_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Object_t_0_0_0_Single_t36_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t36_0_0_0_KeyValuePair_2_t2787_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t36_0_0_0, &KeyValuePair_2_t2787_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t36_0_0_0_KeyValuePair_2_t2787_0_0_0 = { 3, GenInst_Object_t_0_0_0_Single_t36_0_0_0_KeyValuePair_2_t2787_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t36_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &Single_t36_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t36_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t36_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t569_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t569_0_0_0_Types[] = { &KeyValuePair_2_t569_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t569_0_0_0 = { 1, GenInst_KeyValuePair_2_t569_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_List_1_t198_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &List_1_t198_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_List_1_t198_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_List_1_t198_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2806_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2806_0_0_0_Types[] = { &KeyValuePair_2_t2806_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2806_0_0_0 = { 1, GenInst_KeyValuePair_2_t2806_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2811_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2811_0_0_0_Types[] = { &KeyValuePair_2_t2811_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2811_0_0_0 = { 1, GenInst_KeyValuePair_2_t2811_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_Int32_t59_0_0_0_Types[] = { &Int32_t59_0_0_0, &Int32_t59_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_Int32_t59_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_Int32_t59_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &Int32_t59_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_KeyValuePair_2_t2811_0_0_0_Types[] = { &Int32_t59_0_0_0, &Int32_t59_0_0_0, &KeyValuePair_2_t2811_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_KeyValuePair_2_t2811_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_KeyValuePair_2_t2811_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t18_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t18_0_0_0_Types[] = { &MonoBehaviour_t18_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t18_0_0_0 = { 1, GenInst_MonoBehaviour_t18_0_0_0_Types };
extern const Il2CppType Behaviour_t875_0_0_0;
static const Il2CppType* GenInst_Behaviour_t875_0_0_0_Types[] = { &Behaviour_t875_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t875_0_0_0 = { 1, GenInst_Behaviour_t875_0_0_0_Types };
extern const Il2CppType WebCamDevice_t1219_0_0_0;
static const Il2CppType* GenInst_WebCamDevice_t1219_0_0_0_Types[] = { &WebCamDevice_t1219_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamDevice_t1219_0_0_0 = { 1, GenInst_WebCamDevice_t1219_0_0_0_Types };
extern const Il2CppType Bezier_t376_0_0_0;
static const Il2CppType* GenInst_Bezier_t376_0_0_0_Types[] = { &Bezier_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_Bezier_t376_0_0_0 = { 1, GenInst_Bezier_t376_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t41_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t41_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t41_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t41_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType Shader_t583_0_0_0;
static const Il2CppType* GenInst_Shader_t583_0_0_0_Types[] = { &Shader_t583_0_0_0 };
extern const Il2CppGenericInst GenInst_Shader_t583_0_0_0 = { 1, GenInst_Shader_t583_0_0_0_Types };
extern const Il2CppType Entry_t619_0_0_0;
static const Il2CppType* GenInst_Entry_t619_0_0_0_Types[] = { &Entry_t619_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t619_0_0_0 = { 1, GenInst_Entry_t619_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_PointerEventData_t652_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &PointerEventData_t652_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_PointerEventData_t652_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_PointerEventData_t652_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t833_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t833_0_0_0_Types[] = { &KeyValuePair_2_t833_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t833_0_0_0 = { 1, GenInst_KeyValuePair_2_t833_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t838_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t838_0_0_0_Types[] = { &RaycastHit2D_t838_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t838_0_0_0 = { 1, GenInst_RaycastHit2D_t838_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t820_0_0_0_Int32_t59_0_0_0_Types[] = { &ICanvasElement_t820_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t820_0_0_0_Int32_t59_0_0_0 = { 2, GenInst_ICanvasElement_t820_0_0_0_Int32_t59_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t820_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &ICanvasElement_t820_0_0_0, &Int32_t59_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t820_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_ICanvasElement_t820_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Font_t684_0_0_0_List_1_t842_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Font_t684_0_0_0, &List_1_t842_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t684_0_0_0_List_1_t842_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Font_t684_0_0_0_List_1_t842_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2934_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2934_0_0_0_Types[] = { &KeyValuePair_2_t2934_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2934_0_0_0 = { 1, GenInst_KeyValuePair_2_t2934_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t687_0_0_0_Int32_t59_0_0_0_Types[] = { &Graphic_t687_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t687_0_0_0_Int32_t59_0_0_0 = { 2, GenInst_Graphic_t687_0_0_0_Int32_t59_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t687_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Graphic_t687_0_0_0, &Int32_t59_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t687_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Graphic_t687_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t690_0_0_0_IndexedSet_1_t852_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Canvas_t690_0_0_0, &IndexedSet_1_t852_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t690_0_0_0_IndexedSet_1_t852_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Canvas_t690_0_0_0_IndexedSet_1_t852_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2965_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2965_0_0_0_Types[] = { &KeyValuePair_2_t2965_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2965_0_0_0 = { 1, GenInst_KeyValuePair_2_t2965_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2969_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2969_0_0_0_Types[] = { &KeyValuePair_2_t2969_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2969_0_0_0 = { 1, GenInst_KeyValuePair_2_t2969_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2973_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2973_0_0_0_Types[] = { &KeyValuePair_2_t2973_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2973_0_0_0 = { 1, GenInst_KeyValuePair_2_t2973_0_0_0_Types };
extern const Il2CppType Enum_t63_0_0_0;
static const Il2CppType* GenInst_Enum_t63_0_0_0_Types[] = { &Enum_t63_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t63_0_0_0 = { 1, GenInst_Enum_t63_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t41_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t782_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t782_0_0_0_Types[] = { &LayoutRebuilder_t782_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t782_0_0_0 = { 1, GenInst_LayoutRebuilder_t782_0_0_0_Types };
extern const Il2CppType EyewearCalibrationReading_t913_0_0_0;
static const Il2CppType* GenInst_EyewearCalibrationReading_t913_0_0_0_Types[] = { &EyewearCalibrationReading_t913_0_0_0 };
extern const Il2CppGenericInst GenInst_EyewearCalibrationReading_t913_0_0_0 = { 1, GenInst_EyewearCalibrationReading_t913_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t510_0_0_0_VideoBackgroundAbstractBehaviour_t445_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Camera_t510_0_0_0, &VideoBackgroundAbstractBehaviour_t445_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t510_0_0_0_VideoBackgroundAbstractBehaviour_t445_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Camera_t510_0_0_0_VideoBackgroundAbstractBehaviour_t445_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3025_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3025_0_0_0_Types[] = { &KeyValuePair_2_t3025_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3025_0_0_0 = { 1, GenInst_KeyValuePair_2_t3025_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3060_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3060_0_0_0_Types[] = { &KeyValuePair_2_t3060_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3060_0_0_0 = { 1, GenInst_KeyValuePair_2_t3060_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t942_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0, &Object_t_0_0_0, &PIXEL_FORMAT_t942_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t942_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t942_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3060_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t3060_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3060_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3060_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_Image_t943_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0, &Image_t943_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0_Image_t943_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t942_0_0_0_Image_t943_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3074_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3074_0_0_0_Types[] = { &KeyValuePair_2_t3074_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3074_0_0_0 = { 1, GenInst_KeyValuePair_2_t3074_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Trackable_t891_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &Trackable_t891_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Trackable_t891_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Trackable_t891_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3087_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3087_0_0_0_Types[] = { &KeyValuePair_2_t3087_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3087_0_0_0 = { 1, GenInst_KeyValuePair_2_t3087_0_0_0_Types };
extern const Il2CppType Color32_t829_0_0_0;
static const Il2CppType* GenInst_Color32_t829_0_0_0_Types[] = { &Color32_t829_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t829_0_0_0 = { 1, GenInst_Color32_t829_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButton_t1061_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButton_t1061_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButton_t1061_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_VirtualButton_t1061_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3096_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3096_0_0_0_Types[] = { &KeyValuePair_2_t3096_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3096_0_0_0 = { 1, GenInst_KeyValuePair_2_t3096_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Marker_t1069_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &Marker_t1069_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Marker_t1069_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Marker_t1069_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3113_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3113_0_0_0_Types[] = { &KeyValuePair_2_t3113_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3113_0_0_0 = { 1, GenInst_KeyValuePair_2_t3113_0_0_0_Types };
extern const Il2CppType SmartTerrainRevisionData_t977_0_0_0;
static const Il2CppType* GenInst_SmartTerrainRevisionData_t977_0_0_0_Types[] = { &SmartTerrainRevisionData_t977_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t977_0_0_0 = { 1, GenInst_SmartTerrainRevisionData_t977_0_0_0_Types };
extern const Il2CppType SurfaceData_t978_0_0_0;
static const Il2CppType* GenInst_SurfaceData_t978_0_0_0_Types[] = { &SurfaceData_t978_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceData_t978_0_0_0 = { 1, GenInst_SurfaceData_t978_0_0_0_Types };
extern const Il2CppType PropData_t979_0_0_0;
static const Il2CppType* GenInst_PropData_t979_0_0_0_Types[] = { &PropData_t979_0_0_0 };
extern const Il2CppGenericInst GenInst_PropData_t979_0_0_0 = { 1, GenInst_PropData_t979_0_0_0_Types };
extern const Il2CppType IEditorTrackableBehaviour_t1155_0_0_0;
static const Il2CppType* GenInst_IEditorTrackableBehaviour_t1155_0_0_0_Types[] = { &IEditorTrackableBehaviour_t1155_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorTrackableBehaviour_t1155_0_0_0 = { 1, GenInst_IEditorTrackableBehaviour_t1155_0_0_0_Types };
static const Il2CppType* GenInst_Image_t943_0_0_0_Types[] = { &Image_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t943_0_0_0 = { 1, GenInst_Image_t943_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t562_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t562_0_0_0 = { 2, GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3151_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3151_0_0_0_Types[] = { &KeyValuePair_2_t3151_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3151_0_0_0 = { 1, GenInst_KeyValuePair_2_t3151_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t562_0_0_0_Types[] = { &UInt16_t562_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t562_0_0_0 = { 1, GenInst_UInt16_t562_0_0_0_Types };
extern const Il2CppType IComparable_1_t4535_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4535_0_0_0_Types[] = { &IComparable_1_t4535_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4535_0_0_0 = { 1, GenInst_IComparable_1_t4535_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4540_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4540_0_0_0_Types[] = { &IEquatable_1_t4540_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4540_0_0_0 = { 1, GenInst_IEquatable_1_t4540_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t562_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_UInt16_t562_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t562_0_0_0, &UInt16_t562_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_UInt16_t562_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_UInt16_t562_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t562_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_KeyValuePair_2_t3151_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t562_0_0_0, &KeyValuePair_2_t3151_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_KeyValuePair_2_t3151_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_KeyValuePair_2_t3151_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t562_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t562_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t562_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t562_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3166_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3166_0_0_0_Types[] = { &KeyValuePair_2_t3166_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3166_0_0_0 = { 1, GenInst_KeyValuePair_2_t3166_0_0_0_Types };
extern const Il2CppType RectangleData_t923_0_0_0;
static const Il2CppType* GenInst_RectangleData_t923_0_0_0_Types[] = { &RectangleData_t923_0_0_0 };
extern const Il2CppGenericInst GenInst_RectangleData_t923_0_0_0 = { 1, GenInst_RectangleData_t923_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_WordResult_t1024_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &WordResult_t1024_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_WordResult_t1024_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_WordResult_t1024_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3173_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3173_0_0_0_Types[] = { &KeyValuePair_2_t3173_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3173_0_0_0 = { 1, GenInst_KeyValuePair_2_t3173_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_WordAbstractBehaviour_t457_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &WordAbstractBehaviour_t457_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_WordAbstractBehaviour_t457_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_WordAbstractBehaviour_t457_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174_0_0_0_Types[] = { &KeyValuePair_2_t1174_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1022_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1022_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1022_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1022_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3192_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3192_0_0_0_Types[] = { &KeyValuePair_2_t3192_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3192_0_0_0 = { 1, GenInst_KeyValuePair_2_t3192_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1022_0_0_0_Types[] = { &List_1_t1022_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1022_0_0_0 = { 1, GenInst_List_1_t1022_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Surface_t492_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &Surface_t492_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Surface_t492_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Surface_t492_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1187_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1187_0_0_0_Types[] = { &KeyValuePair_2_t1187_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1187_0_0_0 = { 1, GenInst_KeyValuePair_2_t1187_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_SurfaceAbstractBehaviour_t436_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &SurfaceAbstractBehaviour_t436_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_SurfaceAbstractBehaviour_t436_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_SurfaceAbstractBehaviour_t436_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3213_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3213_0_0_0_Types[] = { &KeyValuePair_2_t3213_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3213_0_0_0 = { 1, GenInst_KeyValuePair_2_t3213_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_Prop_t491_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &Prop_t491_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_Prop_t491_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_Prop_t491_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1186_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1186_0_0_0_Types[] = { &KeyValuePair_2_t1186_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1186_0_0_0 = { 1, GenInst_KeyValuePair_2_t1186_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_PropAbstractBehaviour_t430_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &PropAbstractBehaviour_t430_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_PropAbstractBehaviour_t430_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_PropAbstractBehaviour_t430_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3219_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3219_0_0_0_Types[] = { &KeyValuePair_2_t3219_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3219_0_0_0 = { 1, GenInst_KeyValuePair_2_t3219_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_TrackableBehaviour_t410_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &TrackableBehaviour_t410_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_TrackableBehaviour_t410_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_TrackableBehaviour_t410_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3234_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3234_0_0_0_Types[] = { &KeyValuePair_2_t3234_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3234_0_0_0 = { 1, GenInst_KeyValuePair_2_t3234_0_0_0_Types };
extern const Il2CppType MarkerAbstractBehaviour_t423_0_0_0;
static const Il2CppType* GenInst_MarkerAbstractBehaviour_t423_0_0_0_Types[] = { &MarkerAbstractBehaviour_t423_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerAbstractBehaviour_t423_0_0_0 = { 1, GenInst_MarkerAbstractBehaviour_t423_0_0_0_Types };
extern const Il2CppType IEditorMarkerBehaviour_t1202_0_0_0;
static const Il2CppType* GenInst_IEditorMarkerBehaviour_t1202_0_0_0_Types[] = { &IEditorMarkerBehaviour_t1202_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorMarkerBehaviour_t1202_0_0_0 = { 1, GenInst_IEditorMarkerBehaviour_t1202_0_0_0_Types };
extern const Il2CppType WorldCenterTrackableBehaviour_t988_0_0_0;
static const Il2CppType* GenInst_WorldCenterTrackableBehaviour_t988_0_0_0_Types[] = { &WorldCenterTrackableBehaviour_t988_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t988_0_0_0 = { 1, GenInst_WorldCenterTrackableBehaviour_t988_0_0_0_Types };
extern const Il2CppType IEditorDataSetTrackableBehaviour_t1204_0_0_0;
static const Il2CppType* GenInst_IEditorDataSetTrackableBehaviour_t1204_0_0_0_Types[] = { &IEditorDataSetTrackableBehaviour_t1204_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorDataSetTrackableBehaviour_t1204_0_0_0 = { 1, GenInst_IEditorDataSetTrackableBehaviour_t1204_0_0_0_Types };
extern const Il2CppType IEditorVirtualButtonBehaviour_t1221_0_0_0;
static const Il2CppType* GenInst_IEditorVirtualButtonBehaviour_t1221_0_0_0_Types[] = { &IEditorVirtualButtonBehaviour_t1221_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorVirtualButtonBehaviour_t1221_0_0_0 = { 1, GenInst_IEditorVirtualButtonBehaviour_t1221_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3240_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3240_0_0_0_Types[] = { &KeyValuePair_2_t3240_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3240_0_0_0 = { 1, GenInst_KeyValuePair_2_t3240_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_Int32_t59_0_0_0_Types[] = { &Int32_t59_0_0_0, &TrackableResultData_t969_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_Int32_t59_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_Int32_t59_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_TrackableResultData_t969_0_0_0_Types[] = { &Int32_t59_0_0_0, &TrackableResultData_t969_0_0_0, &TrackableResultData_t969_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_TrackableResultData_t969_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_TrackableResultData_t969_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &TrackableResultData_t969_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_KeyValuePair_2_t3240_0_0_0_Types[] = { &Int32_t59_0_0_0, &TrackableResultData_t969_0_0_0, &KeyValuePair_2_t3240_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_KeyValuePair_2_t3240_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_KeyValuePair_2_t3240_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3255_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3255_0_0_0_Types[] = { &KeyValuePair_2_t3255_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3255_0_0_0 = { 1, GenInst_KeyValuePair_2_t3255_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t970_0_0_0_Types[] = { &VirtualButtonData_t970_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t970_0_0_0 = { 1, GenInst_VirtualButtonData_t970_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_Int32_t59_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButtonData_t970_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_Int32_t59_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_Int32_t59_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_VirtualButtonData_t970_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButtonData_t970_0_0_0, &VirtualButtonData_t970_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_VirtualButtonData_t970_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_VirtualButtonData_t970_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButtonData_t970_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_KeyValuePair_2_t3255_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButtonData_t970_0_0_0, &KeyValuePair_2_t3255_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_KeyValuePair_2_t3255_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_KeyValuePair_2_t3255_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_ImageTarget_t1063_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &ImageTarget_t1063_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_ImageTarget_t1063_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_ImageTarget_t1063_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3285_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3285_0_0_0_Types[] = { &KeyValuePair_2_t3285_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3285_0_0_0 = { 1, GenInst_KeyValuePair_2_t3285_0_0_0_Types };
extern const Il2CppType ProfileData_t1064_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1064_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1064_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1064_0_0_0 = { 2, GenInst_String_t_0_0_0_ProfileData_t1064_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1064_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0 = { 2, GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3292_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3292_0_0_0_Types[] = { &KeyValuePair_2_t3292_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3292_0_0_0 = { 1, GenInst_KeyValuePair_2_t3292_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1064_0_0_0_Types[] = { &ProfileData_t1064_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1064_0_0_0 = { 1, GenInst_ProfileData_t1064_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1064_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_ProfileData_t1064_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1064_0_0_0, &ProfileData_t1064_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_ProfileData_t1064_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_ProfileData_t1064_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1064_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_KeyValuePair_2_t3292_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1064_0_0_0, &KeyValuePair_2_t3292_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_KeyValuePair_2_t3292_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_KeyValuePair_2_t3292_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1064_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1064_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1064_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t1064_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3306_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3306_0_0_0_Types[] = { &KeyValuePair_2_t3306_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3306_0_0_0 = { 1, GenInst_KeyValuePair_2_t3306_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_VirtualButtonAbstractBehaviour_t449_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &VirtualButtonAbstractBehaviour_t449_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_VirtualButtonAbstractBehaviour_t449_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_VirtualButtonAbstractBehaviour_t449_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3311_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3311_0_0_0_Types[] = { &KeyValuePair_2_t3311_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3311_0_0_0 = { 1, GenInst_KeyValuePair_2_t3311_0_0_0_Types };
extern const Il2CppType Link_t3340_0_0_0;
static const Il2CppType* GenInst_Link_t3340_0_0_0_Types[] = { &Link_t3340_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3340_0_0_0 = { 1, GenInst_Link_t3340_0_0_0_Types };
extern const Il2CppType ParameterModifier_t2119_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t2119_0_0_0_Types[] = { &ParameterModifier_t2119_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t2119_0_0_0 = { 1, GenInst_ParameterModifier_t2119_0_0_0_Types };
extern const Il2CppType UserProfile_t1262_0_0_0;
static const Il2CppType* GenInst_UserProfile_t1262_0_0_0_Types[] = { &UserProfile_t1262_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t1262_0_0_0 = { 1, GenInst_UserProfile_t1262_0_0_0_Types };
extern const Il2CppType IUserProfile_t3914_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t3914_0_0_0_Types[] = { &IUserProfile_t3914_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t3914_0_0_0 = { 1, GenInst_IUserProfile_t3914_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3915_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3915_0_0_0_Types[] = { &IAchievementDescription_t3915_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3915_0_0_0 = { 1, GenInst_IAchievementDescription_t3915_0_0_0_Types };
extern const Il2CppType IAchievement_t1436_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1436_0_0_0_Types[] = { &IAchievement_t1436_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1436_0_0_0 = { 1, GenInst_IAchievement_t1436_0_0_0_Types };
extern const Il2CppType IScore_t1270_0_0_0;
static const Il2CppType* GenInst_IScore_t1270_0_0_0_Types[] = { &IScore_t1270_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t1270_0_0_0 = { 1, GenInst_IScore_t1270_0_0_0_Types };
extern const Il2CppType AchievementDescription_t1265_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t1265_0_0_0_Types[] = { &AchievementDescription_t1265_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t1265_0_0_0 = { 1, GenInst_AchievementDescription_t1265_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1244_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1244_0_0_0_Types[] = { &GcAchievementData_t1244_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1244_0_0_0 = { 1, GenInst_GcAchievementData_t1244_0_0_0_Types };
extern const Il2CppType Achievement_t1264_0_0_0;
static const Il2CppType* GenInst_Achievement_t1264_0_0_0_Types[] = { &Achievement_t1264_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1264_0_0_0 = { 1, GenInst_Achievement_t1264_0_0_0_Types };
extern const Il2CppType GcScoreData_t1245_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t1245_0_0_0_Types[] = { &GcScoreData_t1245_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t1245_0_0_0 = { 1, GenInst_GcScoreData_t1245_0_0_0_Types };
extern const Il2CppType Score_t1266_0_0_0;
static const Il2CppType* GenInst_Score_t1266_0_0_0_Types[] = { &Score_t1266_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t1266_0_0_0 = { 1, GenInst_Score_t1266_0_0_0_Types };
extern const Il2CppType HitInfo_t1271_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1271_0_0_0_Types[] = { &HitInfo_t1271_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1271_0_0_0 = { 1, GenInst_HitInfo_t1271_0_0_0_Types };
extern const Il2CppType ParameterInfo_t1459_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t1459_0_0_0_Types[] = { &ParameterInfo_t1459_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t1459_0_0_0 = { 1, GenInst_ParameterInfo_t1459_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t3998_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t3998_0_0_0_Types[] = { &_ParameterInfo_t3998_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t3998_0_0_0 = { 1, GenInst__ParameterInfo_t3998_0_0_0_Types };
extern const Il2CppType Event_t725_0_0_0;
extern const Il2CppType TextEditOp_t1286_0_0_0;
static const Il2CppType* GenInst_Event_t725_0_0_0_TextEditOp_t1286_0_0_0_Types[] = { &Event_t725_0_0_0, &TextEditOp_t1286_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t725_0_0_0_TextEditOp_t1286_0_0_0 = { 2, GenInst_Event_t725_0_0_0_TextEditOp_t1286_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1286_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3373_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3373_0_0_0_Types[] = { &KeyValuePair_2_t3373_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3373_0_0_0 = { 1, GenInst_KeyValuePair_2_t3373_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1286_0_0_0_Types[] = { &TextEditOp_t1286_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1286_0_0_0 = { 1, GenInst_TextEditOp_t1286_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1286_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_TextEditOp_t1286_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1286_0_0_0, &TextEditOp_t1286_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_TextEditOp_t1286_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_TextEditOp_t1286_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1286_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_KeyValuePair_2_t3373_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1286_0_0_0, &KeyValuePair_2_t3373_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_KeyValuePair_2_t3373_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_KeyValuePair_2_t3373_0_0_0_Types };
static const Il2CppType* GenInst_Event_t725_0_0_0_Types[] = { &Event_t725_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t725_0_0_0 = { 1, GenInst_Event_t725_0_0_0_Types };
static const Il2CppType* GenInst_Event_t725_0_0_0_TextEditOp_t1286_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Event_t725_0_0_0, &TextEditOp_t1286_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t725_0_0_0_TextEditOp_t1286_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Event_t725_0_0_0_TextEditOp_t1286_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3387_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3387_0_0_0_Types[] = { &KeyValuePair_2_t3387_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3387_0_0_0 = { 1, GenInst_KeyValuePair_2_t3387_0_0_0_Types };
extern const Il2CppType Keyframe_t1321_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1321_0_0_0_Types[] = { &Keyframe_t1321_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1321_0_0_0 = { 1, GenInst_Keyframe_t1321_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t59_0_0_0_LayoutCache_t1382_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Int32_t59_0_0_0, &LayoutCache_t1382_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t59_0_0_0_LayoutCache_t1382_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Int32_t59_0_0_0_LayoutCache_t1382_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3446_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3446_0_0_0_Types[] = { &KeyValuePair_2_t3446_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3446_0_0_0 = { 1, GenInst_KeyValuePair_2_t3446_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t184_0_0_0_Types[] = { &GUIStyle_t184_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t184_0_0_0 = { 1, GenInst_GUIStyle_t184_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t184_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t184_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t184_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t184_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3457_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3457_0_0_0_Types[] = { &KeyValuePair_2_t3457_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3457_0_0_0 = { 1, GenInst_KeyValuePair_2_t3457_0_0_0_Types };
extern const Il2CppType Display_t1421_0_0_0;
static const Il2CppType* GenInst_Display_t1421_0_0_0_Types[] = { &Display_t1421_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t1421_0_0_0 = { 1, GenInst_Display_t1421_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t2564_0_0_0;
static const Il2CppType* GenInst_ISerializable_t2564_0_0_0_Types[] = { &ISerializable_t2564_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t2564_0_0_0 = { 1, GenInst_ISerializable_t2564_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3478_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3478_0_0_0_Types[] = { &KeyValuePair_2_t3478_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3478_0_0_0 = { 1, GenInst_KeyValuePair_2_t3478_0_0_0_Types };
extern const Il2CppType IComparable_1_t4767_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4767_0_0_0_Types[] = { &IComparable_1_t4767_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4767_0_0_0 = { 1, GenInst_IComparable_1_t4767_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4772_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4772_0_0_0_Types[] = { &IEquatable_1_t4772_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4772_0_0_0 = { 1, GenInst_IEquatable_1_t4772_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t41_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Boolean_t41_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t41_0_0_0, &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Boolean_t41_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Boolean_t41_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t41_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_KeyValuePair_2_t3478_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t41_0_0_0, &KeyValuePair_2_t3478_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_KeyValuePair_2_t3478_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_KeyValuePair_2_t3478_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t41_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t41_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t41_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t41_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3493_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3493_0_0_0_Types[] = { &KeyValuePair_2_t3493_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3493_0_0_0 = { 1, GenInst_KeyValuePair_2_t3493_0_0_0_Types };
extern const Il2CppType X509Certificate_t1546_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t1546_0_0_0_Types[] = { &X509Certificate_t1546_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t1546_0_0_0 = { 1, GenInst_X509Certificate_t1546_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t2567_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t2567_0_0_0_Types[] = { &IDeserializationCallback_t2567_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t2567_0_0_0 = { 1, GenInst_IDeserializationCallback_t2567_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t1558_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t1558_0_0_0_Types[] = { &X509ChainStatus_t1558_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1558_0_0_0 = { 1, GenInst_X509ChainStatus_t1558_0_0_0_Types };
extern const Il2CppType Capture_t1579_0_0_0;
static const Il2CppType* GenInst_Capture_t1579_0_0_0_Types[] = { &Capture_t1579_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t1579_0_0_0 = { 1, GenInst_Capture_t1579_0_0_0_Types };
extern const Il2CppType Group_t1582_0_0_0;
static const Il2CppType* GenInst_Group_t1582_0_0_0_Types[] = { &Group_t1582_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t1582_0_0_0 = { 1, GenInst_Group_t1582_0_0_0_Types };
extern const Il2CppType Mark_t1605_0_0_0;
static const Il2CppType* GenInst_Mark_t1605_0_0_0_Types[] = { &Mark_t1605_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t1605_0_0_0 = { 1, GenInst_Mark_t1605_0_0_0_Types };
extern const Il2CppType UriScheme_t1641_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1641_0_0_0_Types[] = { &UriScheme_t1641_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1641_0_0_0 = { 1, GenInst_UriScheme_t1641_0_0_0_Types };
extern const Il2CppType UInt32_t558_0_0_0;
static const Il2CppType* GenInst_UInt32_t558_0_0_0_Types[] = { &UInt32_t558_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t558_0_0_0 = { 1, GenInst_UInt32_t558_0_0_0_Types };
extern const Il2CppType IComparable_1_t4807_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4807_0_0_0_Types[] = { &IComparable_1_t4807_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4807_0_0_0 = { 1, GenInst_IComparable_1_t4807_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4812_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4812_0_0_0_Types[] = { &IEquatable_1_t4812_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4812_0_0_0 = { 1, GenInst_IEquatable_1_t4812_0_0_0_Types };
extern const Il2CppType BigInteger_t1692_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1692_0_0_0_Types[] = { &BigInteger_t1692_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1692_0_0_0 = { 1, GenInst_BigInteger_t1692_0_0_0_Types };
extern const Il2CppType KeySizes_t1812_0_0_0;
static const Il2CppType* GenInst_KeySizes_t1812_0_0_0_Types[] = { &KeySizes_t1812_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t1812_0_0_0 = { 1, GenInst_KeySizes_t1812_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t1784_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t1784_0_0_0_Types[] = { &ClientCertificateType_t1784_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1784_0_0_0 = { 1, GenInst_ClientCertificateType_t1784_0_0_0_Types };
extern const Il2CppType UInt64_t563_0_0_0;
static const Il2CppType* GenInst_UInt64_t563_0_0_0_Types[] = { &UInt64_t563_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t563_0_0_0 = { 1, GenInst_UInt64_t563_0_0_0_Types };
extern const Il2CppType SByte_t559_0_0_0;
static const Il2CppType* GenInst_SByte_t559_0_0_0_Types[] = { &SByte_t559_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t559_0_0_0 = { 1, GenInst_SByte_t559_0_0_0_Types };
extern const Il2CppType Int16_t561_0_0_0;
static const Il2CppType* GenInst_Int16_t561_0_0_0_Types[] = { &Int16_t561_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t561_0_0_0 = { 1, GenInst_Int16_t561_0_0_0_Types };
extern const Il2CppType Decimal_t564_0_0_0;
static const Il2CppType* GenInst_Decimal_t564_0_0_0_Types[] = { &Decimal_t564_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t564_0_0_0 = { 1, GenInst_Decimal_t564_0_0_0_Types };
extern const Il2CppType Delegate_t506_0_0_0;
static const Il2CppType* GenInst_Delegate_t506_0_0_0_Types[] = { &Delegate_t506_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t506_0_0_0 = { 1, GenInst_Delegate_t506_0_0_0_Types };
extern const Il2CppType IComparable_1_t4829_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4829_0_0_0_Types[] = { &IComparable_1_t4829_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4829_0_0_0 = { 1, GenInst_IComparable_1_t4829_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4830_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4830_0_0_0_Types[] = { &IEquatable_1_t4830_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4830_0_0_0 = { 1, GenInst_IEquatable_1_t4830_0_0_0_Types };
extern const Il2CppType IComparable_1_t4833_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4833_0_0_0_Types[] = { &IComparable_1_t4833_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4833_0_0_0 = { 1, GenInst_IComparable_1_t4833_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4834_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4834_0_0_0_Types[] = { &IEquatable_1_t4834_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4834_0_0_0 = { 1, GenInst_IEquatable_1_t4834_0_0_0_Types };
extern const Il2CppType IComparable_1_t4831_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4831_0_0_0_Types[] = { &IComparable_1_t4831_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4831_0_0_0 = { 1, GenInst_IComparable_1_t4831_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4832_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4832_0_0_0_Types[] = { &IEquatable_1_t4832_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4832_0_0_0 = { 1, GenInst_IEquatable_1_t4832_0_0_0_Types };
extern const Il2CppType IComparable_1_t4827_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4827_0_0_0_Types[] = { &IComparable_1_t4827_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4827_0_0_0 = { 1, GenInst_IComparable_1_t4827_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4828_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4828_0_0_0_Types[] = { &IEquatable_1_t4828_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4828_0_0_0 = { 1, GenInst_IEquatable_1_t4828_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t3994_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t3994_0_0_0_Types[] = { &_FieldInfo_t3994_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t3994_0_0_0 = { 1, GenInst__FieldInfo_t3994_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t1464_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t1464_0_0_0_Types[] = { &ConstructorInfo_t1464_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1464_0_0_0 = { 1, GenInst_ConstructorInfo_t1464_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3992_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3992_0_0_0_Types[] = { &_ConstructorInfo_t3992_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3992_0_0_0 = { 1, GenInst__ConstructorInfo_t3992_0_0_0_Types };
extern const Il2CppType MethodBase_t1457_0_0_0;
static const Il2CppType* GenInst_MethodBase_t1457_0_0_0_Types[] = { &MethodBase_t1457_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t1457_0_0_0 = { 1, GenInst_MethodBase_t1457_0_0_0_Types };
extern const Il2CppType _MethodBase_t3995_0_0_0;
static const Il2CppType* GenInst__MethodBase_t3995_0_0_0_Types[] = { &_MethodBase_t3995_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t3995_0_0_0 = { 1, GenInst__MethodBase_t3995_0_0_0_Types };
extern const Il2CppType TableRange_t1889_0_0_0;
static const Il2CppType* GenInst_TableRange_t1889_0_0_0_Types[] = { &TableRange_t1889_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1889_0_0_0 = { 1, GenInst_TableRange_t1889_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1892_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1892_0_0_0_Types[] = { &TailoringInfo_t1892_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1892_0_0_0 = { 1, GenInst_TailoringInfo_t1892_0_0_0_Types };
extern const Il2CppType Contraction_t1893_0_0_0;
static const Il2CppType* GenInst_Contraction_t1893_0_0_0_Types[] = { &Contraction_t1893_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1893_0_0_0 = { 1, GenInst_Contraction_t1893_0_0_0_Types };
extern const Il2CppType Level2Map_t1895_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1895_0_0_0_Types[] = { &Level2Map_t1895_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1895_0_0_0 = { 1, GenInst_Level2Map_t1895_0_0_0_Types };
extern const Il2CppType BigInteger_t1916_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1916_0_0_0_Types[] = { &BigInteger_t1916_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1916_0_0_0 = { 1, GenInst_BigInteger_t1916_0_0_0_Types };
extern const Il2CppType Slot_t1966_0_0_0;
static const Il2CppType* GenInst_Slot_t1966_0_0_0_Types[] = { &Slot_t1966_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1966_0_0_0 = { 1, GenInst_Slot_t1966_0_0_0_Types };
extern const Il2CppType Slot_t1974_0_0_0;
static const Il2CppType* GenInst_Slot_t1974_0_0_0_Types[] = { &Slot_t1974_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1974_0_0_0 = { 1, GenInst_Slot_t1974_0_0_0_Types };
extern const Il2CppType StackFrame_t1456_0_0_0;
static const Il2CppType* GenInst_StackFrame_t1456_0_0_0_Types[] = { &StackFrame_t1456_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t1456_0_0_0 = { 1, GenInst_StackFrame_t1456_0_0_0_Types };
extern const Il2CppType Calendar_t1988_0_0_0;
static const Il2CppType* GenInst_Calendar_t1988_0_0_0_Types[] = { &Calendar_t1988_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1988_0_0_0 = { 1, GenInst_Calendar_t1988_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t2063_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t2063_0_0_0_Types[] = { &ModuleBuilder_t2063_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t2063_0_0_0 = { 1, GenInst_ModuleBuilder_t2063_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t3987_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t3987_0_0_0_Types[] = { &_ModuleBuilder_t3987_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t3987_0_0_0 = { 1, GenInst__ModuleBuilder_t3987_0_0_0_Types };
extern const Il2CppType Module_t2059_0_0_0;
static const Il2CppType* GenInst_Module_t2059_0_0_0_Types[] = { &Module_t2059_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t2059_0_0_0 = { 1, GenInst_Module_t2059_0_0_0_Types };
extern const Il2CppType _Module_t3997_0_0_0;
static const Il2CppType* GenInst__Module_t3997_0_0_0_Types[] = { &_Module_t3997_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t3997_0_0_0 = { 1, GenInst__Module_t3997_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t2069_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t2069_0_0_0_Types[] = { &ParameterBuilder_t2069_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t2069_0_0_0 = { 1, GenInst_ParameterBuilder_t2069_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t3988_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t3988_0_0_0_Types[] = { &_ParameterBuilder_t3988_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t3988_0_0_0 = { 1, GenInst__ParameterBuilder_t3988_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1218_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1218_0_0_0_Types[] = { &TypeU5BU5D_t1218_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1218_0_0_0 = { 1, GenInst_TypeU5BU5D_t1218_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t1653_0_0_0;
static const Il2CppType* GenInst_ICollection_t1653_0_0_0_Types[] = { &ICollection_t1653_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t1653_0_0_0 = { 1, GenInst_ICollection_t1653_0_0_0_Types };
extern const Il2CppType IList_t488_0_0_0;
static const Il2CppType* GenInst_IList_t488_0_0_0_Types[] = { &IList_t488_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t488_0_0_0 = { 1, GenInst_IList_t488_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t2053_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t2053_0_0_0_Types[] = { &ILTokenInfo_t2053_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t2053_0_0_0 = { 1, GenInst_ILTokenInfo_t2053_0_0_0_Types };
extern const Il2CppType LabelData_t2055_0_0_0;
static const Il2CppType* GenInst_LabelData_t2055_0_0_0_Types[] = { &LabelData_t2055_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t2055_0_0_0 = { 1, GenInst_LabelData_t2055_0_0_0_Types };
extern const Il2CppType LabelFixup_t2054_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t2054_0_0_0_Types[] = { &LabelFixup_t2054_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t2054_0_0_0 = { 1, GenInst_LabelFixup_t2054_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t2051_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t2051_0_0_0_Types[] = { &GenericTypeParameterBuilder_t2051_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t2051_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t2051_0_0_0_Types };
extern const Il2CppType TypeBuilder_t2045_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t2045_0_0_0_Types[] = { &TypeBuilder_t2045_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t2045_0_0_0 = { 1, GenInst_TypeBuilder_t2045_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t3989_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t3989_0_0_0_Types[] = { &_TypeBuilder_t3989_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t3989_0_0_0 = { 1, GenInst__TypeBuilder_t3989_0_0_0_Types };
extern const Il2CppType MethodBuilder_t2052_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t2052_0_0_0_Types[] = { &MethodBuilder_t2052_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t2052_0_0_0 = { 1, GenInst_MethodBuilder_t2052_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3986_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3986_0_0_0_Types[] = { &_MethodBuilder_t3986_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3986_0_0_0 = { 1, GenInst__MethodBuilder_t3986_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3996_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3996_0_0_0_Types[] = { &_MethodInfo_t3996_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3996_0_0_0 = { 1, GenInst__MethodInfo_t3996_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t2043_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t2043_0_0_0_Types[] = { &ConstructorBuilder_t2043_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t2043_0_0_0 = { 1, GenInst_ConstructorBuilder_t2043_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t3982_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t3982_0_0_0_Types[] = { &_ConstructorBuilder_t3982_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t3982_0_0_0 = { 1, GenInst__ConstructorBuilder_t3982_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2049_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2049_0_0_0_Types[] = { &FieldBuilder_t2049_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2049_0_0_0 = { 1, GenInst_FieldBuilder_t2049_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t3984_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t3984_0_0_0_Types[] = { &_FieldBuilder_t3984_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t3984_0_0_0 = { 1, GenInst__FieldBuilder_t3984_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t3999_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t3999_0_0_0_Types[] = { &_PropertyInfo_t3999_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t3999_0_0_0 = { 1, GenInst__PropertyInfo_t3999_0_0_0_Types };
extern const Il2CppType ResourceInfo_t2130_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t2130_0_0_0_Types[] = { &ResourceInfo_t2130_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t2130_0_0_0 = { 1, GenInst_ResourceInfo_t2130_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t2131_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t2131_0_0_0_Types[] = { &ResourceCacheItem_t2131_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t2131_0_0_0 = { 1, GenInst_ResourceCacheItem_t2131_0_0_0_Types };
extern const Il2CppType IContextProperty_t2524_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t2524_0_0_0_Types[] = { &IContextProperty_t2524_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t2524_0_0_0 = { 1, GenInst_IContextProperty_t2524_0_0_0_Types };
extern const Il2CppType Header_t2211_0_0_0;
static const Il2CppType* GenInst_Header_t2211_0_0_0_Types[] = { &Header_t2211_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2211_0_0_0 = { 1, GenInst_Header_t2211_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2559_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2559_0_0_0_Types[] = { &ITrackingHandler_t2559_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2559_0_0_0 = { 1, GenInst_ITrackingHandler_t2559_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2546_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2546_0_0_0_Types[] = { &IContextAttribute_t2546_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2546_0_0_0 = { 1, GenInst_IContextAttribute_t2546_0_0_0_Types };
extern const Il2CppType IComparable_1_t5071_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t5071_0_0_0_Types[] = { &IComparable_1_t5071_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t5071_0_0_0 = { 1, GenInst_IComparable_1_t5071_0_0_0_Types };
extern const Il2CppType IEquatable_1_t5076_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t5076_0_0_0_Types[] = { &IEquatable_1_t5076_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t5076_0_0_0 = { 1, GenInst_IEquatable_1_t5076_0_0_0_Types };
extern const Il2CppType IComparable_1_t4835_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4835_0_0_0_Types[] = { &IComparable_1_t4835_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4835_0_0_0 = { 1, GenInst_IComparable_1_t4835_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4836_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4836_0_0_0_Types[] = { &IEquatable_1_t4836_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4836_0_0_0 = { 1, GenInst_IEquatable_1_t4836_0_0_0_Types };
extern const Il2CppType IComparable_1_t5095_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t5095_0_0_0_Types[] = { &IComparable_1_t5095_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t5095_0_0_0 = { 1, GenInst_IComparable_1_t5095_0_0_0_Types };
extern const Il2CppType IEquatable_1_t5100_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t5100_0_0_0_Types[] = { &IEquatable_1_t5100_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t5100_0_0_0 = { 1, GenInst_IEquatable_1_t5100_0_0_0_Types };
extern const Il2CppType TypeTag_t2267_0_0_0;
static const Il2CppType* GenInst_TypeTag_t2267_0_0_0_Types[] = { &TypeTag_t2267_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t2267_0_0_0 = { 1, GenInst_TypeTag_t2267_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t1518_0_0_0;
static const Il2CppType* GenInst_Version_t1518_0_0_0_Types[] = { &Version_t1518_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1518_0_0_0 = { 1, GenInst_Version_t1518_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t58_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &DictionaryEntry_t58_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t58_0_0_0_DictionaryEntry_t58_0_0_0 = { 2, GenInst_DictionaryEntry_t58_0_0_0_DictionaryEntry_t58_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2607_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2607_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2607_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2607_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2607_0_0_0_KeyValuePair_2_t2607_0_0_0_Types[] = { &KeyValuePair_2_t2607_0_0_0, &KeyValuePair_2_t2607_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2607_0_0_0_KeyValuePair_2_t2607_0_0_0 = { 2, GenInst_KeyValuePair_2_t2607_0_0_0_KeyValuePair_2_t2607_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2634_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2634_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2634_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2634_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2634_0_0_0_KeyValuePair_2_t2634_0_0_0_Types[] = { &KeyValuePair_2_t2634_0_0_0, &KeyValuePair_2_t2634_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2634_0_0_0_KeyValuePair_2_t2634_0_0_0 = { 2, GenInst_KeyValuePair_2_t2634_0_0_0_KeyValuePair_2_t2634_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2658_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2658_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2658_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2658_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2658_0_0_0_KeyValuePair_2_t2658_0_0_0_Types[] = { &KeyValuePair_2_t2658_0_0_0, &KeyValuePair_2_t2658_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2658_0_0_0_KeyValuePair_2_t2658_0_0_0 = { 2, GenInst_KeyValuePair_2_t2658_0_0_0_KeyValuePair_2_t2658_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t560_0_0_0_Byte_t560_0_0_0_Types[] = { &Byte_t560_0_0_0, &Byte_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t560_0_0_0_Byte_t560_0_0_0 = { 2, GenInst_Byte_t560_0_0_0_Byte_t560_0_0_0_Types };
extern const Il2CppType EmptyMockDialog_t271_0_0_0;
static const Il2CppType* GenInst_EmptyMockDialog_t271_0_0_0_Types[] = { &EmptyMockDialog_t271_0_0_0 };
extern const Il2CppGenericInst GenInst_EmptyMockDialog_t271_0_0_0 = { 1, GenInst_EmptyMockDialog_t271_0_0_0_Types };
static const Il2CppType* GenInst_Single_t36_0_0_0_Object_t_0_0_0_Types[] = { &Single_t36_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t36_0_0_0_Object_t_0_0_0 = { 2, GenInst_Single_t36_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Single_t36_0_0_0_Single_t36_0_0_0_Types[] = { &Single_t36_0_0_0, &Single_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t36_0_0_0_Single_t36_0_0_0 = { 2, GenInst_Single_t36_0_0_0_Single_t36_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2787_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2787_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2787_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2787_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2787_0_0_0_KeyValuePair_2_t2787_0_0_0_Types[] = { &KeyValuePair_2_t2787_0_0_0, &KeyValuePair_2_t2787_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2787_0_0_0_KeyValuePair_2_t2787_0_0_0 = { 2, GenInst_KeyValuePair_2_t2787_0_0_0_KeyValuePair_2_t2787_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2811_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2811_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2811_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2811_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2811_0_0_0_KeyValuePair_2_t2811_0_0_0_Types[] = { &KeyValuePair_2_t2811_0_0_0, &KeyValuePair_2_t2811_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2811_0_0_0_KeyValuePair_2_t2811_0_0_0 = { 2, GenInst_KeyValuePair_2_t2811_0_0_0_KeyValuePair_2_t2811_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t647_0_0_0_RaycastResult_t647_0_0_0_Types[] = { &RaycastResult_t647_0_0_0, &RaycastResult_t647_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t647_0_0_0_RaycastResult_t647_0_0_0 = { 2, GenInst_RaycastResult_t647_0_0_0_RaycastResult_t647_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t727_0_0_0_UIVertex_t727_0_0_0_Types[] = { &UIVertex_t727_0_0_0, &UIVertex_t727_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t727_0_0_0_UIVertex_t727_0_0_0 = { 2, GenInst_UIVertex_t727_0_0_0_UIVertex_t727_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t942_0_0_0_PIXEL_FORMAT_t942_0_0_0_Types[] = { &PIXEL_FORMAT_t942_0_0_0, &PIXEL_FORMAT_t942_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t942_0_0_0_PIXEL_FORMAT_t942_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t942_0_0_0_PIXEL_FORMAT_t942_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3060_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3060_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3060_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3060_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3060_0_0_0_KeyValuePair_2_t3060_0_0_0_Types[] = { &KeyValuePair_2_t3060_0_0_0, &KeyValuePair_2_t3060_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3060_0_0_0_KeyValuePair_2_t3060_0_0_0 = { 2, GenInst_KeyValuePair_2_t3060_0_0_0_KeyValuePair_2_t3060_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t562_0_0_0_Object_t_0_0_0_Types[] = { &UInt16_t562_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t562_0_0_0_Object_t_0_0_0 = { 2, GenInst_UInt16_t562_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t562_0_0_0_UInt16_t562_0_0_0_Types[] = { &UInt16_t562_0_0_0, &UInt16_t562_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t562_0_0_0_UInt16_t562_0_0_0 = { 2, GenInst_UInt16_t562_0_0_0_UInt16_t562_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3151_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3151_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3151_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3151_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3151_0_0_0_KeyValuePair_2_t3151_0_0_0_Types[] = { &KeyValuePair_2_t3151_0_0_0, &KeyValuePair_2_t3151_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3151_0_0_0_KeyValuePair_2_t3151_0_0_0 = { 2, GenInst_KeyValuePair_2_t3151_0_0_0_KeyValuePair_2_t3151_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t969_0_0_0_Object_t_0_0_0_Types[] = { &TrackableResultData_t969_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t969_0_0_0_Object_t_0_0_0 = { 2, GenInst_TrackableResultData_t969_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t969_0_0_0_TrackableResultData_t969_0_0_0_Types[] = { &TrackableResultData_t969_0_0_0, &TrackableResultData_t969_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t969_0_0_0_TrackableResultData_t969_0_0_0 = { 2, GenInst_TrackableResultData_t969_0_0_0_TrackableResultData_t969_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3240_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3240_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3240_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3240_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3240_0_0_0_KeyValuePair_2_t3240_0_0_0_Types[] = { &KeyValuePair_2_t3240_0_0_0, &KeyValuePair_2_t3240_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3240_0_0_0_KeyValuePair_2_t3240_0_0_0 = { 2, GenInst_KeyValuePair_2_t3240_0_0_0_KeyValuePair_2_t3240_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t970_0_0_0_Object_t_0_0_0_Types[] = { &VirtualButtonData_t970_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t970_0_0_0_Object_t_0_0_0 = { 2, GenInst_VirtualButtonData_t970_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t970_0_0_0_VirtualButtonData_t970_0_0_0_Types[] = { &VirtualButtonData_t970_0_0_0, &VirtualButtonData_t970_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t970_0_0_0_VirtualButtonData_t970_0_0_0 = { 2, GenInst_VirtualButtonData_t970_0_0_0_VirtualButtonData_t970_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3255_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3255_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3255_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3255_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3255_0_0_0_KeyValuePair_2_t3255_0_0_0_Types[] = { &KeyValuePair_2_t3255_0_0_0, &KeyValuePair_2_t3255_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3255_0_0_0_KeyValuePair_2_t3255_0_0_0 = { 2, GenInst_KeyValuePair_2_t3255_0_0_0_KeyValuePair_2_t3255_0_0_0_Types };
static const Il2CppType* GenInst_TargetSearchResult_t1050_0_0_0_TargetSearchResult_t1050_0_0_0_Types[] = { &TargetSearchResult_t1050_0_0_0, &TargetSearchResult_t1050_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t1050_0_0_0_TargetSearchResult_t1050_0_0_0 = { 2, GenInst_TargetSearchResult_t1050_0_0_0_TargetSearchResult_t1050_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1064_0_0_0_Object_t_0_0_0_Types[] = { &ProfileData_t1064_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1064_0_0_0_Object_t_0_0_0 = { 2, GenInst_ProfileData_t1064_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1064_0_0_0_ProfileData_t1064_0_0_0_Types[] = { &ProfileData_t1064_0_0_0, &ProfileData_t1064_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1064_0_0_0_ProfileData_t1064_0_0_0 = { 2, GenInst_ProfileData_t1064_0_0_0_ProfileData_t1064_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3292_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3292_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3292_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3292_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3292_0_0_0_KeyValuePair_2_t3292_0_0_0_Types[] = { &KeyValuePair_2_t3292_0_0_0, &KeyValuePair_2_t3292_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3292_0_0_0_KeyValuePair_2_t3292_0_0_0 = { 2, GenInst_KeyValuePair_2_t3292_0_0_0_KeyValuePair_2_t3292_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1286_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t1286_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1286_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t1286_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1286_0_0_0_TextEditOp_t1286_0_0_0_Types[] = { &TextEditOp_t1286_0_0_0, &TextEditOp_t1286_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1286_0_0_0_TextEditOp_t1286_0_0_0 = { 2, GenInst_TextEditOp_t1286_0_0_0_TextEditOp_t1286_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3373_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3373_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3373_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3373_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3373_0_0_0_KeyValuePair_2_t3373_0_0_0_Types[] = { &KeyValuePair_2_t3373_0_0_0, &KeyValuePair_2_t3373_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3373_0_0_0_KeyValuePair_2_t3373_0_0_0 = { 2, GenInst_KeyValuePair_2_t3373_0_0_0_KeyValuePair_2_t3373_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t859_0_0_0_UICharInfo_t859_0_0_0_Types[] = { &UICharInfo_t859_0_0_0, &UICharInfo_t859_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t859_0_0_0_UICharInfo_t859_0_0_0 = { 2, GenInst_UICharInfo_t859_0_0_0_UICharInfo_t859_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t857_0_0_0_UILineInfo_t857_0_0_0_Types[] = { &UILineInfo_t857_0_0_0, &UILineInfo_t857_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t857_0_0_0_UILineInfo_t857_0_0_0 = { 2, GenInst_UILineInfo_t857_0_0_0_UILineInfo_t857_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t41_0_0_0_Boolean_t41_0_0_0_Types[] = { &Boolean_t41_0_0_0, &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t41_0_0_0_Boolean_t41_0_0_0 = { 2, GenInst_Boolean_t41_0_0_0_Boolean_t41_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3478_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3478_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3478_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3478_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3478_0_0_0_KeyValuePair_2_t3478_0_0_0_Types[] = { &KeyValuePair_2_t3478_0_0_0, &KeyValuePair_2_t3478_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3478_0_0_0_KeyValuePair_2_t3478_0_0_0 = { 2, GenInst_KeyValuePair_2_t3478_0_0_0_KeyValuePair_2_t3478_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t2101_0_0_0_CustomAttributeTypedArgument_t2101_0_0_0_Types[] = { &CustomAttributeTypedArgument_t2101_0_0_0, &CustomAttributeTypedArgument_t2101_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2101_0_0_0_CustomAttributeTypedArgument_t2101_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t2101_0_0_0_CustomAttributeTypedArgument_t2101_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t2100_0_0_0_CustomAttributeNamedArgument_t2100_0_0_0_Types[] = { &CustomAttributeNamedArgument_t2100_0_0_0, &CustomAttributeNamedArgument_t2100_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t2100_0_0_0_CustomAttributeNamedArgument_t2100_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t2100_0_0_0_CustomAttributeNamedArgument_t2100_0_0_0_Types };
extern const Il2CppType ISN_Singleton_1_t3898_gp_0_0_0_0;
static const Il2CppType* GenInst_ISN_Singleton_1_t3898_gp_0_0_0_0_Types[] = { &ISN_Singleton_1_t3898_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_Singleton_1_t3898_gp_0_0_0_0 = { 1, GenInst_ISN_Singleton_1_t3898_gp_0_0_0_0_Types };
extern const Il2CppType SAC_Singleton_1_t3899_gp_0_0_0_0;
static const Il2CppType* GenInst_SAC_Singleton_1_t3899_gp_0_0_0_0_Types[] = { &SAC_Singleton_1_t3899_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SAC_Singleton_1_t3899_gp_0_0_0_0 = { 1, GenInst_SAC_Singleton_1_t3899_gp_0_0_0_0_Types };
extern const Il2CppType CallbackManager_AddFacebookDelegate_m31401_gp_0_0_0_0;
static const Il2CppType* GenInst_CallbackManager_AddFacebookDelegate_m31401_gp_0_0_0_0_Types[] = { &CallbackManager_AddFacebookDelegate_m31401_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackManager_AddFacebookDelegate_m31401_gp_0_0_0_0 = { 1, GenInst_CallbackManager_AddFacebookDelegate_m31401_gp_0_0_0_0_Types };
extern const Il2CppType CallbackManager_TryCallCallback_m31402_gp_0_0_0_0;
static const Il2CppType* GenInst_CallbackManager_TryCallCallback_m31402_gp_0_0_0_0_Types[] = { &CallbackManager_TryCallCallback_m31402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackManager_TryCallCallback_m31402_gp_0_0_0_0 = { 1, GenInst_CallbackManager_TryCallCallback_m31402_gp_0_0_0_0_Types };
extern const Il2CppType CanvasUIMethodCall_1_t3900_gp_0_0_0_0;
static const Il2CppType* GenInst_CanvasUIMethodCall_1_t3900_gp_0_0_0_0_Types[] = { &CanvasUIMethodCall_1_t3900_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasUIMethodCall_1_t3900_gp_0_0_0_0 = { 1, GenInst_CanvasUIMethodCall_1_t3900_gp_0_0_0_0_Types };
extern const Il2CppType ComponentFactory_GetComponent_m31411_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentFactory_GetComponent_m31411_gp_0_0_0_0_Types[] = { &ComponentFactory_GetComponent_m31411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactory_GetComponent_m31411_gp_0_0_0_0 = { 1, GenInst_ComponentFactory_GetComponent_m31411_gp_0_0_0_0_Types };
extern const Il2CppType ComponentFactory_AddComponent_m31412_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentFactory_AddComponent_m31412_gp_0_0_0_0_Types[] = { &ComponentFactory_AddComponent_m31412_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactory_AddComponent_m31412_gp_0_0_0_0 = { 1, GenInst_ComponentFactory_AddComponent_m31412_gp_0_0_0_0_Types };
extern const Il2CppType MethodArguments_AddNullablePrimitive_m31466_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodArguments_AddNullablePrimitive_m31466_gp_0_0_0_0_Types[] = { &MethodArguments_AddNullablePrimitive_m31466_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodArguments_AddNullablePrimitive_m31466_gp_0_0_0_0 = { 1, GenInst_MethodArguments_AddNullablePrimitive_m31466_gp_0_0_0_0_Types };
extern const Il2CppType MethodArguments_AddList_m31467_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodArguments_AddList_m31467_gp_0_0_0_0_Types[] = { &MethodArguments_AddList_m31467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodArguments_AddList_m31467_gp_0_0_0_0 = { 1, GenInst_MethodArguments_AddList_m31467_gp_0_0_0_0_Types };
extern const Il2CppType MethodCall_1_t3901_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodCall_1_t3901_gp_0_0_0_0_Types[] = { &MethodCall_1_t3901_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodCall_1_t3901_gp_0_0_0_0 = { 1, GenInst_MethodCall_1_t3901_gp_0_0_0_0_Types };
extern const Il2CppType JavaMethodCall_1_t3902_gp_0_0_0_0;
static const Il2CppType* GenInst_JavaMethodCall_1_t3902_gp_0_0_0_0_Types[] = { &JavaMethodCall_1_t3902_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JavaMethodCall_1_t3902_gp_0_0_0_0 = { 1, GenInst_JavaMethodCall_1_t3902_gp_0_0_0_0_Types };
extern const Il2CppType FBJavaClass_CallStatic_m31480_gp_0_0_0_0;
static const Il2CppType* GenInst_FBJavaClass_CallStatic_m31480_gp_0_0_0_0_Types[] = { &FBJavaClass_CallStatic_m31480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_FBJavaClass_CallStatic_m31480_gp_0_0_0_0 = { 1, GenInst_FBJavaClass_CallStatic_m31480_gp_0_0_0_0_Types };
extern const Il2CppType IOSFacebook_AddCallback_m31490_gp_0_0_0_0;
static const Il2CppType* GenInst_IOSFacebook_AddCallback_m31490_gp_0_0_0_0_Types[] = { &IOSFacebook_AddCallback_m31490_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebook_AddCallback_m31490_gp_0_0_0_0 = { 1, GenInst_IOSFacebook_AddCallback_m31490_gp_0_0_0_0_Types };
extern const Il2CppType EditorFacebook_ShowEmptyMockDialog_m31494_gp_0_0_0_0;
static const Il2CppType* GenInst_EditorFacebook_ShowEmptyMockDialog_m31494_gp_0_0_0_0_Types[] = { &EditorFacebook_ShowEmptyMockDialog_m31494_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EditorFacebook_ShowEmptyMockDialog_m31494_gp_0_0_0_0 = { 1, GenInst_EditorFacebook_ShowEmptyMockDialog_m31494_gp_0_0_0_0_Types };
extern const Il2CppType Utilities_GetValueOrDefault_m31519_gp_0_0_0_0;
static const Il2CppType* GenInst_Utilities_GetValueOrDefault_m31519_gp_0_0_0_0_Types[] = { &Utilities_GetValueOrDefault_m31519_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Utilities_GetValueOrDefault_m31519_gp_0_0_0_0 = { 1, GenInst_Utilities_GetValueOrDefault_m31519_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m31542_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m31542_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m31542_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m31542_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m31542_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m31543_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m31543_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m31543_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m31543_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m31543_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m31545_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m31545_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m31545_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m31545_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m31545_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m31546_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m31546_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m31546_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m31546_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m31546_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m31547_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m31547_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m31547_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m31547_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m31547_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t3906_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t3906_gp_0_0_0_0_Types[] = { &TweenRunner_1_t3906_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t3906_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t3906_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t3908_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t3908_gp_0_0_0_0_Types[] = { &IndexedSet_1_t3908_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3908_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t3908_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t3908_gp_0_0_0_0_Int32_t59_0_0_0_Types[] = { &IndexedSet_1_t3908_gp_0_0_0_0, &Int32_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3908_gp_0_0_0_0_Int32_t59_0_0_0 = { 2, GenInst_IndexedSet_1_t3908_gp_0_0_0_0_Int32_t59_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t3909_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t3909_gp_0_0_0_0_Types[] = { &ObjectPool_1_t3909_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3909_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t3909_gp_0_0_0_0_Types };
extern const Il2CppType SmartTerrainBuilderImpl_CreateReconstruction_m31999_gp_0_0_0_0;
static const Il2CppType* GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m31999_gp_0_0_0_0_Types[] = { &SmartTerrainBuilderImpl_CreateReconstruction_m31999_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m31999_gp_0_0_0_0 = { 1, GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m31999_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t3916_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t3916_gp_0_0_0_0_Types[] = { &InvokableCall_1_t3916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3916_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3916_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t3917_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3917_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t3917_gp_0_0_0_0_InvokableCall_2_t3917_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3917_gp_0_0_0_0, &InvokableCall_2_t3917_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3917_gp_0_0_0_0_InvokableCall_2_t3917_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3917_gp_0_0_0_0_InvokableCall_2_t3917_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3917_gp_0_0_0_0_Types[] = { &InvokableCall_2_t3917_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3917_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3917_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3917_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3917_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3917_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3917_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3918_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3918_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3918_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3918_gp_0_0_0_0_InvokableCall_3_t3918_gp_1_0_0_0_InvokableCall_3_t3918_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3918_gp_0_0_0_0, &InvokableCall_3_t3918_gp_1_0_0_0, &InvokableCall_3_t3918_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3918_gp_0_0_0_0_InvokableCall_3_t3918_gp_1_0_0_0_InvokableCall_3_t3918_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3918_gp_0_0_0_0_InvokableCall_3_t3918_gp_1_0_0_0_InvokableCall_3_t3918_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3918_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3918_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3918_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3918_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3918_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3918_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3918_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3918_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3918_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3918_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3918_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3918_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t3919_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3919_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3919_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3919_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t3919_gp_0_0_0_0_InvokableCall_4_t3919_gp_1_0_0_0_InvokableCall_4_t3919_gp_2_0_0_0_InvokableCall_4_t3919_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3919_gp_0_0_0_0, &InvokableCall_4_t3919_gp_1_0_0_0, &InvokableCall_4_t3919_gp_2_0_0_0, &InvokableCall_4_t3919_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3919_gp_0_0_0_0_InvokableCall_4_t3919_gp_1_0_0_0_InvokableCall_4_t3919_gp_2_0_0_0_InvokableCall_4_t3919_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3919_gp_0_0_0_0_InvokableCall_4_t3919_gp_1_0_0_0_InvokableCall_4_t3919_gp_2_0_0_0_InvokableCall_4_t3919_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3919_gp_0_0_0_0_Types[] = { &InvokableCall_4_t3919_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3919_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3919_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3919_gp_1_0_0_0_Types[] = { &InvokableCall_4_t3919_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3919_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3919_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3919_gp_2_0_0_0_Types[] = { &InvokableCall_4_t3919_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3919_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3919_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3919_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3919_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3919_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3919_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t1465_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t1465_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t1465_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1465_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t1465_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t3920_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t3920_gp_0_0_0_0_Types[] = { &UnityEvent_1_t3920_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t3920_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t3920_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t3921_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t3921_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t3921_gp_0_0_0_0_UnityEvent_2_t3921_gp_1_0_0_0_Types[] = { &UnityEvent_2_t3921_gp_0_0_0_0, &UnityEvent_2_t3921_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t3921_gp_0_0_0_0_UnityEvent_2_t3921_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t3921_gp_0_0_0_0_UnityEvent_2_t3921_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t3922_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t3922_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3922_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t3922_gp_0_0_0_0_UnityEvent_3_t3922_gp_1_0_0_0_UnityEvent_3_t3922_gp_2_0_0_0_Types[] = { &UnityEvent_3_t3922_gp_0_0_0_0, &UnityEvent_3_t3922_gp_1_0_0_0, &UnityEvent_3_t3922_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t3922_gp_0_0_0_0_UnityEvent_3_t3922_gp_1_0_0_0_UnityEvent_3_t3922_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t3922_gp_0_0_0_0_UnityEvent_3_t3922_gp_1_0_0_0_UnityEvent_3_t3922_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t3923_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t3923_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t3923_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t3923_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t3923_gp_0_0_0_0_UnityEvent_4_t3923_gp_1_0_0_0_UnityEvent_4_t3923_gp_2_0_0_0_UnityEvent_4_t3923_gp_3_0_0_0_Types[] = { &UnityEvent_4_t3923_gp_0_0_0_0, &UnityEvent_4_t3923_gp_1_0_0_0, &UnityEvent_4_t3923_gp_2_0_0_0, &UnityEvent_4_t3923_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t3923_gp_0_0_0_0_UnityEvent_4_t3923_gp_1_0_0_0_UnityEvent_4_t3923_gp_2_0_0_0_UnityEvent_4_t3923_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t3923_gp_0_0_0_0_UnityEvent_4_t3923_gp_1_0_0_0_UnityEvent_4_t3923_gp_2_0_0_0_UnityEvent_4_t3923_gp_3_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m32295_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m32295_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m32295_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m32295_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m32295_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m32297_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m32297_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m32297_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m32297_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m32297_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m32299_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m32299_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m32299_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m32299_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m32299_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m32300_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m32300_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m32300_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m32300_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m32300_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m32307_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m32307_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m32307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m32307_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m32307_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m32311_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m32311_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m32311_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m32311_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m32311_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m32312_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m32312_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m32312_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m32312_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m32312_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m32313_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m32313_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m32313_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m32313_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m32313_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m32314_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m32314_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m32314_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m32314_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m32314_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m32316_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m32316_gp_0_0_0_0_Types[] = { &Component_GetComponents_m32316_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m32316_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m32316_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3930_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3930_gp_0_0_0_0_Types[] = { &LinkedList_1_t3930_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3930_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3930_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3931_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3931_gp_0_0_0_0_Types[] = { &Enumerator_t3931_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3931_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3931_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3932_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t3932_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t3932_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3932_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t3932_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t3933_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t3933_gp_0_0_0_0_Types[] = { &Stack_1_t3933_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t3933_gp_0_0_0_0 = { 1, GenInst_Stack_1_t3933_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3934_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3934_gp_0_0_0_0_Types[] = { &Enumerator_t3934_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3934_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3934_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t3936_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t3936_gp_0_0_0_0_Types[] = { &HashSet_1_t3936_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t3936_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t3936_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3938_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3938_gp_0_0_0_0_Types[] = { &Enumerator_t3938_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3938_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3938_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3939_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3939_gp_0_0_0_0_Types[] = { &PrimeHelper_t3939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3939_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3939_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m32483_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m32483_gp_0_0_0_0_Types[] = { &Enumerable_Any_m32483_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m32483_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m32483_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m32484_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m32484_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m32484_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m32484_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m32484_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m32485_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m32485_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m32485_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m32485_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m32485_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m32486_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m32486_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m32486_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m32486_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m32486_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m32487_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m32487_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m32487_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m32487_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m32487_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m32488_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m32488_gp_0_0_0_0_Types[] = { &Enumerable_Count_m32488_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m32488_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m32488_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m32489_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m32489_gp_0_0_0_0_Types[] = { &Enumerable_First_m32489_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m32489_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m32489_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m32490_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m32490_gp_0_0_0_0_Types[] = { &Enumerable_Select_m32490_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m32490_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m32490_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m32490_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m32490_gp_0_0_0_0_Enumerable_Select_m32490_gp_1_0_0_0_Types[] = { &Enumerable_Select_m32490_gp_0_0_0_0, &Enumerable_Select_m32490_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m32490_gp_0_0_0_0_Enumerable_Select_m32490_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m32490_gp_0_0_0_0_Enumerable_Select_m32490_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m32490_gp_1_0_0_0_Types[] = { &Enumerable_Select_m32490_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m32490_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m32490_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0_Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0_Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0_Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m32492_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m32492_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m32492_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m32492_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m32492_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m32493_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m32493_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m32493_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m32493_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m32493_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m32494_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m32494_gp_0_0_0_0_Types[] = { &Enumerable_Where_m32494_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m32494_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m32494_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m32494_gp_0_0_0_0_Boolean_t41_0_0_0_Types[] = { &Enumerable_Where_m32494_gp_0_0_0_0, &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m32494_gp_0_0_0_0_Boolean_t41_0_0_0 = { 2, GenInst_Enumerable_Where_m32494_gp_0_0_0_0_Boolean_t41_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0_Boolean_t41_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0, &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0_Boolean_t41_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0_Boolean_t41_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t3940_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3940_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t3940_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3940_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3940_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0_Boolean_t41_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0, &Boolean_t41_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0_Boolean_t41_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0_Boolean_t41_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3949_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3949_gp_0_0_0_0_Types[] = { &IEnumerable_1_t3949_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3949_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t3949_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m32557_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m32557_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m32557_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m32557_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m32557_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32569_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32569_gp_0_0_0_0_Array_Sort_m32569_gp_0_0_0_0_Types[] = { &Array_Sort_m32569_gp_0_0_0_0, &Array_Sort_m32569_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32569_gp_0_0_0_0_Array_Sort_m32569_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m32569_gp_0_0_0_0_Array_Sort_m32569_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32570_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m32570_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32570_gp_0_0_0_0_Array_Sort_m32570_gp_1_0_0_0_Types[] = { &Array_Sort_m32570_gp_0_0_0_0, &Array_Sort_m32570_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32570_gp_0_0_0_0_Array_Sort_m32570_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m32570_gp_0_0_0_0_Array_Sort_m32570_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m32571_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32571_gp_0_0_0_0_Types[] = { &Array_Sort_m32571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32571_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32571_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m32571_gp_0_0_0_0_Array_Sort_m32571_gp_0_0_0_0_Types[] = { &Array_Sort_m32571_gp_0_0_0_0, &Array_Sort_m32571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32571_gp_0_0_0_0_Array_Sort_m32571_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m32571_gp_0_0_0_0_Array_Sort_m32571_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32572_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32572_gp_0_0_0_0_Types[] = { &Array_Sort_m32572_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32572_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32572_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32572_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32572_gp_0_0_0_0_Array_Sort_m32572_gp_1_0_0_0_Types[] = { &Array_Sort_m32572_gp_0_0_0_0, &Array_Sort_m32572_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32572_gp_0_0_0_0_Array_Sort_m32572_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m32572_gp_0_0_0_0_Array_Sort_m32572_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m32573_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32573_gp_0_0_0_0_Array_Sort_m32573_gp_0_0_0_0_Types[] = { &Array_Sort_m32573_gp_0_0_0_0, &Array_Sort_m32573_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32573_gp_0_0_0_0_Array_Sort_m32573_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m32573_gp_0_0_0_0_Array_Sort_m32573_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32574_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m32574_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32574_gp_0_0_0_0_Array_Sort_m32574_gp_1_0_0_0_Types[] = { &Array_Sort_m32574_gp_0_0_0_0, &Array_Sort_m32574_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32574_gp_0_0_0_0_Array_Sort_m32574_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m32574_gp_0_0_0_0_Array_Sort_m32574_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m32575_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32575_gp_0_0_0_0_Types[] = { &Array_Sort_m32575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32575_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32575_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m32575_gp_0_0_0_0_Array_Sort_m32575_gp_0_0_0_0_Types[] = { &Array_Sort_m32575_gp_0_0_0_0, &Array_Sort_m32575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32575_gp_0_0_0_0_Array_Sort_m32575_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m32575_gp_0_0_0_0_Array_Sort_m32575_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32576_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32576_gp_0_0_0_0_Types[] = { &Array_Sort_m32576_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32576_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32576_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32576_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32576_gp_1_0_0_0_Types[] = { &Array_Sort_m32576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32576_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m32576_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m32576_gp_0_0_0_0_Array_Sort_m32576_gp_1_0_0_0_Types[] = { &Array_Sort_m32576_gp_0_0_0_0, &Array_Sort_m32576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32576_gp_0_0_0_0_Array_Sort_m32576_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m32576_gp_0_0_0_0_Array_Sort_m32576_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m32577_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32577_gp_0_0_0_0_Types[] = { &Array_Sort_m32577_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32577_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32577_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32578_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32578_gp_0_0_0_0_Types[] = { &Array_Sort_m32578_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32578_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32578_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m32579_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m32579_gp_0_0_0_0_Types[] = { &Array_qsort_m32579_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m32579_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m32579_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m32579_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m32579_gp_0_0_0_0_Array_qsort_m32579_gp_1_0_0_0_Types[] = { &Array_qsort_m32579_gp_0_0_0_0, &Array_qsort_m32579_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m32579_gp_0_0_0_0_Array_qsort_m32579_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m32579_gp_0_0_0_0_Array_qsort_m32579_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m32580_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m32580_gp_0_0_0_0_Types[] = { &Array_compare_m32580_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m32580_gp_0_0_0_0 = { 1, GenInst_Array_compare_m32580_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m32581_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m32581_gp_0_0_0_0_Types[] = { &Array_qsort_m32581_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m32581_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m32581_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m32584_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m32584_gp_0_0_0_0_Types[] = { &Array_Resize_m32584_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m32584_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m32584_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m32586_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m32586_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m32586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m32586_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m32586_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m32587_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m32587_gp_0_0_0_0_Types[] = { &Array_ForEach_m32587_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m32587_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m32587_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m32588_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m32588_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m32588_gp_0_0_0_0_Array_ConvertAll_m32588_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m32588_gp_0_0_0_0, &Array_ConvertAll_m32588_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m32588_gp_0_0_0_0_Array_ConvertAll_m32588_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m32588_gp_0_0_0_0_Array_ConvertAll_m32588_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m32589_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m32589_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m32589_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m32589_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m32589_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m32590_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m32590_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m32590_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m32590_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m32590_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m32591_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m32591_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m32591_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m32591_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m32591_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m32592_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m32592_gp_0_0_0_0_Types[] = { &Array_FindIndex_m32592_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m32592_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m32592_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m32593_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m32593_gp_0_0_0_0_Types[] = { &Array_FindIndex_m32593_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m32593_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m32593_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m32594_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m32594_gp_0_0_0_0_Types[] = { &Array_FindIndex_m32594_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m32594_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m32594_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m32595_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m32595_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m32595_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m32595_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m32595_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m32596_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m32596_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m32596_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m32596_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m32596_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m32597_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m32597_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m32597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m32597_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m32597_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m32598_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m32598_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m32598_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m32598_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m32598_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m32599_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m32599_gp_0_0_0_0_Types[] = { &Array_IndexOf_m32599_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m32599_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m32599_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m32600_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m32600_gp_0_0_0_0_Types[] = { &Array_IndexOf_m32600_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m32600_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m32600_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m32601_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m32601_gp_0_0_0_0_Types[] = { &Array_IndexOf_m32601_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m32601_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m32601_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m32602_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m32602_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m32602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m32602_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m32602_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m32603_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m32603_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m32603_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m32603_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m32603_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m32604_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m32604_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m32604_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m32604_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m32604_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m32605_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m32605_gp_0_0_0_0_Types[] = { &Array_FindAll_m32605_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m32605_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m32605_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m32606_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m32606_gp_0_0_0_0_Types[] = { &Array_Exists_m32606_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m32606_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m32606_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m32607_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m32607_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m32607_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m32607_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m32607_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m32608_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m32608_gp_0_0_0_0_Types[] = { &Array_Find_m32608_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m32608_gp_0_0_0_0 = { 1, GenInst_Array_Find_m32608_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m32609_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m32609_gp_0_0_0_0_Types[] = { &Array_FindLast_m32609_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m32609_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m32609_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3950_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3950_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3950_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3950_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3950_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3951_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3951_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3951_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3951_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3951_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3952_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3952_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t3952_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3952_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3952_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3953_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3953_gp_0_0_0_0_Types[] = { &IList_1_t3953_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3953_gp_0_0_0_0 = { 1, GenInst_IList_1_t3953_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t3954_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3954_gp_0_0_0_0_Types[] = { &ICollection_1_t3954_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3954_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t3954_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t2536_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t2536_gp_0_0_0_0_Types[] = { &Nullable_1_t2536_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t2536_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t2536_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t3961_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t3961_gp_0_0_0_0_Types[] = { &Comparer_1_t3961_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t3961_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t3961_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3962_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3962_gp_0_0_0_0_Types[] = { &DefaultComparer_t3962_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3962_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3962_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t3897_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t3897_gp_0_0_0_0_Types[] = { &GenericComparer_1_t3897_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t3897_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t3897_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3963_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3963_gp_0_0_0_0_Types[] = { &Dictionary_2_t3963_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3963_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t3963_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3963_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Types[] = { &Dictionary_2_t3963_gp_0_0_0_0, &Dictionary_2_t3963_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t5308_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t5308_0_0_0_Types[] = { &KeyValuePair_2_t5308_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t5308_0_0_0 = { 1, GenInst_KeyValuePair_2_t5308_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m32760_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m32760_gp_0_0_0_0_Types[] = { &Dictionary_2_t3963_gp_0_0_0_0, &Dictionary_2_t3963_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m32760_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m32760_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m32760_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0_Types[] = { &Dictionary_2_t3963_gp_0_0_0_0, &Dictionary_2_t3963_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_DictionaryEntry_t58_0_0_0_Types[] = { &Dictionary_2_t3963_gp_0_0_0_0, &Dictionary_2_t3963_gp_1_0_0_0, &DictionaryEntry_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_DictionaryEntry_t58_0_0_0 = { 3, GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_DictionaryEntry_t58_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3964_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3964_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3964_gp_0_0_0_0_ShimEnumerator_t3964_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3964_gp_0_0_0_0, &ShimEnumerator_t3964_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3964_gp_0_0_0_0_ShimEnumerator_t3964_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3964_gp_0_0_0_0_ShimEnumerator_t3964_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3965_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3965_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3965_gp_0_0_0_0_Enumerator_t3965_gp_1_0_0_0_Types[] = { &Enumerator_t3965_gp_0_0_0_0, &Enumerator_t3965_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3965_gp_0_0_0_0_Enumerator_t3965_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3965_gp_0_0_0_0_Enumerator_t3965_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t5322_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t5322_0_0_0_Types[] = { &KeyValuePair_2_t5322_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t5322_0_0_0 = { 1, GenInst_KeyValuePair_2_t5322_0_0_0_Types };
extern const Il2CppType KeyCollection_t3966_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3966_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_1_0_0_0_Types[] = { &KeyCollection_t3966_gp_0_0_0_0, &KeyCollection_t3966_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3966_gp_0_0_0_0_Types[] = { &KeyCollection_t3966_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3966_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3966_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3967_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3967_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3967_gp_0_0_0_0_Enumerator_t3967_gp_1_0_0_0_Types[] = { &Enumerator_t3967_gp_0_0_0_0, &Enumerator_t3967_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3967_gp_0_0_0_0_Enumerator_t3967_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3967_gp_0_0_0_0_Enumerator_t3967_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3967_gp_0_0_0_0_Types[] = { &Enumerator_t3967_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3967_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3967_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_1_0_0_0_KeyCollection_t3966_gp_0_0_0_0_Types[] = { &KeyCollection_t3966_gp_0_0_0_0, &KeyCollection_t3966_gp_1_0_0_0, &KeyCollection_t3966_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_1_0_0_0_KeyCollection_t3966_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_1_0_0_0_KeyCollection_t3966_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_0_0_0_0_Types[] = { &KeyCollection_t3966_gp_0_0_0_0, &KeyCollection_t3966_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t3968_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t3968_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t3968_gp_0_0_0_0_ValueCollection_t3968_gp_1_0_0_0_Types[] = { &ValueCollection_t3968_gp_0_0_0_0, &ValueCollection_t3968_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3968_gp_0_0_0_0_ValueCollection_t3968_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3968_gp_0_0_0_0_ValueCollection_t3968_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3968_gp_1_0_0_0_Types[] = { &ValueCollection_t3968_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3968_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t3968_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3969_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3969_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3969_gp_0_0_0_0_Enumerator_t3969_gp_1_0_0_0_Types[] = { &Enumerator_t3969_gp_0_0_0_0, &Enumerator_t3969_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3969_gp_0_0_0_0_Enumerator_t3969_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3969_gp_0_0_0_0_Enumerator_t3969_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3969_gp_1_0_0_0_Types[] = { &Enumerator_t3969_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3969_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3969_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3968_gp_0_0_0_0_ValueCollection_t3968_gp_1_0_0_0_ValueCollection_t3968_gp_1_0_0_0_Types[] = { &ValueCollection_t3968_gp_0_0_0_0, &ValueCollection_t3968_gp_1_0_0_0, &ValueCollection_t3968_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3968_gp_0_0_0_0_ValueCollection_t3968_gp_1_0_0_0_ValueCollection_t3968_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t3968_gp_0_0_0_0_ValueCollection_t3968_gp_1_0_0_0_ValueCollection_t3968_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3968_gp_1_0_0_0_ValueCollection_t3968_gp_1_0_0_0_Types[] = { &ValueCollection_t3968_gp_1_0_0_0, &ValueCollection_t3968_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3968_gp_1_0_0_0_ValueCollection_t3968_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3968_gp_1_0_0_0_ValueCollection_t3968_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_KeyValuePair_2_t5308_0_0_0_Types[] = { &Dictionary_2_t3963_gp_0_0_0_0, &Dictionary_2_t3963_gp_1_0_0_0, &KeyValuePair_2_t5308_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_KeyValuePair_2_t5308_0_0_0 = { 3, GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_KeyValuePair_2_t5308_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t5308_0_0_0_KeyValuePair_2_t5308_0_0_0_Types[] = { &KeyValuePair_2_t5308_0_0_0, &KeyValuePair_2_t5308_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t5308_0_0_0_KeyValuePair_2_t5308_0_0_0 = { 2, GenInst_KeyValuePair_2_t5308_0_0_0_KeyValuePair_2_t5308_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3963_gp_1_0_0_0_Types[] = { &Dictionary_2_t3963_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3963_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t3963_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t3971_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t3971_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t3971_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t3971_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t3971_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3972_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3972_gp_0_0_0_0_Types[] = { &DefaultComparer_t3972_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3972_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3972_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t3896_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t3896_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t3896_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t3896_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t3896_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t5358_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t5358_0_0_0_Types[] = { &KeyValuePair_2_t5358_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t5358_0_0_0 = { 1, GenInst_KeyValuePair_2_t5358_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3974_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3974_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3974_gp_0_0_0_0_IDictionary_2_t3974_gp_1_0_0_0_Types[] = { &IDictionary_2_t3974_gp_0_0_0_0, &IDictionary_2_t3974_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3974_gp_0_0_0_0_IDictionary_2_t3974_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3974_gp_0_0_0_0_IDictionary_2_t3974_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3976_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t3976_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3976_gp_0_0_0_0_KeyValuePair_2_t3976_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t3976_gp_0_0_0_0, &KeyValuePair_2_t3976_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3976_gp_0_0_0_0_KeyValuePair_2_t3976_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t3976_gp_0_0_0_0_KeyValuePair_2_t3976_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t3977_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t3977_gp_0_0_0_0_Types[] = { &List_1_t3977_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3977_gp_0_0_0_0 = { 1, GenInst_List_1_t3977_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3978_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3978_gp_0_0_0_0_Types[] = { &Enumerator_t3978_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3978_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3978_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t3979_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t3979_gp_0_0_0_0_Types[] = { &Collection_1_t3979_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t3979_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3979_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3980_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3980_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3980_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3980_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3980_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m33050_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m33050_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m33050_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m33050_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m33050_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m33050_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m33050_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m33050_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m33050_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m33050_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m33051_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m33051_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m33051_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m33051_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m33051_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[853] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Hashtable_t19_0_0_0,
	&GenInst_GUITexture_t43_0_0_0,
	&GenInst_GUIText_t44_0_0_0,
	&GenInst_AudioSource_t20_0_0_0,
	&GenInst_Vector3_t6_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t59_0_0_0,
	&GenInst_iTween_t15_0_0_0,
	&GenInst_ComponentTracker_t67_0_0_0,
	&GenInst_String_t_0_0_0_ComponentTrack_t66_0_0_0,
	&GenInst_Int32_t59_0_0_0_List_1_t460_0_0_0,
	&GenInst_EventHandlerFunction_t458_0_0_0,
	&GenInst_Int32_t59_0_0_0_List_1_t461_0_0_0,
	&GenInst_DataEventHandlerFunction_t459_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_ISDSettings_t76_0_0_0,
	&GenInst_IOSNativeAppEvents_t80_0_0_0,
	&GenInst_IOSNativeSettings_t82_0_0_0,
	&GenInst_ISN_Result_t114_0_0_0,
	&GenInst_ISN_PlayerScoreLoadedResult_t116_0_0_0,
	&GenInst_ISN_AchievementProgressResult_t113_0_0_0,
	&GenInst_ISN_UserInfoLoadResult_t120_0_0_0,
	&GenInst_ISN_PlayerSignatureResult_t118_0_0_0,
	&GenInst_AchievementTemplate_t115_0_0_0,
	&GenInst_String_t_0_0_0_GCLeaderboard_t121_0_0_0,
	&GenInst_String_t_0_0_0_GameCenterPlayerTemplate_t107_0_0_0,
	&GenInst_GameCenterManager_t98_0_0_0,
	&GenInst_GameCenterMatchData_t109_0_0_0,
	&GenInst_GameCenterDataPackage_t123_0_0_0,
	&GenInst_GameCenterMultiplayer_t108_0_0_0,
	&GenInst_Byte_t560_0_0_0,
	&GenInst_Int32_t59_0_0_0_GCScore_t117_0_0_0,
	&GenInst_IOSStoreKitResponse_t137_0_0_0,
	&GenInst_Boolean_t41_0_0_0,
	&GenInst_IOSStoreKitVerificationResponse_t138_0_0_0,
	&GenInst_IOSProductTemplate_t135_0_0_0,
	&GenInst_Int32_t59_0_0_0_IOSStoreProductView_t134_0_0_0,
	&GenInst_IOSInAppPurchaseManager_t127_0_0_0,
	&GenInst_DeviceTokenListener_t139_0_0_0,
	&GenInst_IOSNotificationDeviceToken_t143_0_0_0,
	&GenInst_IOSNotificationController_t140_0_0_0,
	&GenInst_IOSImagePickResult_t149_0_0_0,
	&GenInst_IOSCamera_t145_0_0_0,
	&GenInst_DateTime_t220_0_0_0,
	&GenInst_IOSDateTimePicker_t150_0_0_0,
	&GenInst_IOSSharedApplication_t153_0_0_0,
	&GenInst_ISN_CheckUrlResult_t156_0_0_0,
	&GenInst_ISN_Security_t157_0_0_0,
	&GenInst_ISN_LocalReceiptResult_t171_0_0_0,
	&GenInst_ISN_DeviceGUID_t170_0_0_0,
	&GenInst_IOSVideoManager_t161_0_0_0,
	&GenInst_IOSDialogResult_t90_0_0_0,
	&GenInst_IOSDialog_t163_0_0_0,
	&GenInst_IOSMessage_t166_0_0_0,
	&GenInst_IOSRateUsPopUp_t167_0_0_0,
	&GenInst_IOSSocialManager_t169_0_0_0,
	&GenInst_Int32_t59_0_0_0_iAdBanner_t172_0_0_0,
	&GenInst_iAdBannerController_t173_0_0_0,
	&GenInst_iAdBanner_t172_0_0_0,
	&GenInst_String_t_0_0_0_iAdBanner_t172_0_0_0,
	&GenInst_iCloudData_t179_0_0_0,
	&GenInst_iCloudManager_t180_0_0_0,
	&GenInst_GameObject_t27_0_0_0,
	&GenInst_ConnectionButton_t192_0_0_0,
	&GenInst_DisconnectButton_t193_0_0_0,
	&GenInst_ClickManager_t195_0_0_0,
	&GenInst_Renderer_t46_0_0_0,
	&GenInst_IOSNativePreviewBackButton_t213_0_0_0,
	&GenInst_Texture2D_t33_0_0_0,
	&GenInst_SA_ScreenShotMaker_t216_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0,
	&GenInst_IAppRequestResult_t514_0_0_0,
	&GenInst_IShareResult_t515_0_0_0,
	&GenInst_IGroupCreateResult_t516_0_0_0,
	&GenInst_IGroupJoinResult_t517_0_0_0,
	&GenInst_IPayResult_t518_0_0_0,
	&GenInst_IAppInviteResult_t519_0_0_0,
	&GenInst_IAppLinkResult_t520_0_0_0,
	&GenInst_ILoginResult_t489_0_0_0,
	&GenInst_OGActionType_t265_0_0_0,
	&GenInst_Int32_t59_0_0_0,
	&GenInst_Single_t36_0_0_0,
	&GenInst_IDictionary_2_t225_0_0_0,
	&GenInst_KeyValuePair_2_t529_0_0_0,
	&GenInst_JsBridge_t232_0_0_0,
	&GenInst_CanvasFacebookGameObject_t228_0_0_0,
	&GenInst_IOSFacebookLoader_t263_0_0_0,
	&GenInst_KeyValuePair_2_t287_0_0_0,
	&GenInst_UrlSchemes_t245_0_0_0,
	&GenInst_FacebookSettings_t246_0_0_0,
	&GenInst_IResult_t464_0_0_0,
	&GenInst_AndroidFacebookGameObject_t252_0_0_0,
	&GenInst_IOSFacebookGameObject_t262_0_0_0,
	&GenInst_EditorFacebookGameObject_t267_0_0_0,
	&GenInst_MockLoginDialog_t273_0_0_0,
	&GenInst_IGraphResult_t484_0_0_0,
	&GenInst_IEnumerable_1_t476_0_0_0,
	&GenInst_Int64_t507_0_0_0,
	&GenInst_Object_t_0_0_0_String_t_0_0_0,
	&GenInst_AsyncRequestString_t290_0_0_0,
	&GenInst_Collider_t316_0_0_0,
	&GenInst_ParticleSystem_t332_0_0_0,
	&GenInst_CFX3_Demo_Translate_t317_0_0_0,
	&GenInst_CFX3_AutoStopLoopedEffect_t312_0_0_0,
	&GenInst_CFX_AutoDestructShuriken_t326_0_0_0,
	&GenInst_String_t_0_0_0_Single_t36_0_0_0,
	&GenInst_Light_t47_0_0_0,
	&GenInst_Int32_t59_0_0_0_List_1_t198_0_0_0,
	&GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0,
	&GenInst_CFX_LightIntensityFade_t329_0_0_0,
	&GenInst_TrailTrigger_t341_0_0_0,
	&GenInst_ShineTrigger_t343_0_0_0,
	&GenInst_TransparencyTrigger_t339_0_0_0,
	&GenInst_BoltAnimation_t378_0_0_0,
	&GenInst_SkinnedMeshRenderer_t359_0_0_0,
	&GenInst_Animator_t360_0_0_0,
	&GenInst_AudioListener_t576_0_0_0,
	&GenInst_TimeEvent_t362_0_0_0,
	&GenInst_RawImage_t578_0_0_0,
	&GenInst_CanvasGroup_t579_0_0_0,
	&GenInst_Text_t580_0_0_0,
	&GenInst_Popup_t384_0_0_0,
	&GenInst_Boolean_t41_0_0_0_ILoginResult_t489_0_0_0,
	&GenInst_FBServices_t381_0_0_0,
	&GenInst_Image_t387_0_0_0,
	&GenInst_ScrollRect_t581_0_0_0,
	&GenInst_Camera_t510_0_0_0,
	&GenInst_InitError_t1071_0_0_0,
	&GenInst_ReconstructionBehaviour_t406_0_0_0,
	&GenInst_Prop_t491_0_0_0,
	&GenInst_Surface_t492_0_0_0,
	&GenInst_TrackableBehaviour_t410_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst_MaskOutBehaviour_t424_0_0_0,
	&GenInst_VirtualButtonBehaviour_t448_0_0_0,
	&GenInst_TurnOffBehaviour_t439_0_0_0,
	&GenInst_ImageTargetBehaviour_t414_0_0_0,
	&GenInst_MarkerBehaviour_t422_0_0_0,
	&GenInst_MultiTargetBehaviour_t426_0_0_0,
	&GenInst_CylinderTargetBehaviour_t400_0_0_0,
	&GenInst_WordBehaviour_t456_0_0_0,
	&GenInst_TextRecoBehaviour_t437_0_0_0,
	&GenInst_ObjectTargetBehaviour_t428_0_0_0,
	&GenInst_MeshRenderer_t596_0_0_0,
	&GenInst_MeshFilter_t597_0_0_0,
	&GenInst_ComponentFactoryStarterBehaviour_t417_0_0_0,
	&GenInst_VuforiaBehaviour_t450_0_0_0,
	&GenInst_WireframeBehaviour_t454_0_0_0,
	&GenInst_BaseInputModule_t614_0_0_0,
	&GenInst_RaycastResult_t647_0_0_0,
	&GenInst_IDeselectHandler_t813_0_0_0,
	&GenInst_ISelectHandler_t812_0_0_0,
	&GenInst_BaseEventData_t615_0_0_0,
	&GenInst_IPointerEnterHandler_t800_0_0_0,
	&GenInst_IPointerExitHandler_t801_0_0_0,
	&GenInst_IPointerDownHandler_t802_0_0_0,
	&GenInst_IPointerUpHandler_t803_0_0_0,
	&GenInst_IPointerClickHandler_t804_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t805_0_0_0,
	&GenInst_IBeginDragHandler_t806_0_0_0,
	&GenInst_IDragHandler_t807_0_0_0,
	&GenInst_IEndDragHandler_t808_0_0_0,
	&GenInst_IDropHandler_t809_0_0_0,
	&GenInst_IScrollHandler_t810_0_0_0,
	&GenInst_IUpdateSelectedHandler_t811_0_0_0,
	&GenInst_IMoveHandler_t814_0_0_0,
	&GenInst_ISubmitHandler_t815_0_0_0,
	&GenInst_ICancelHandler_t816_0_0_0,
	&GenInst_List_1_t799_0_0_0,
	&GenInst_IEventSystemHandler_t2864_0_0_0,
	&GenInst_Transform_t25_0_0_0,
	&GenInst_PointerEventData_t652_0_0_0,
	&GenInst_AxisEventData_t649_0_0_0,
	&GenInst_BaseRaycaster_t648_0_0_0,
	&GenInst_EventSystem_t611_0_0_0,
	&GenInst_ButtonState_t654_0_0_0,
	&GenInst_Int32_t59_0_0_0_PointerEventData_t652_0_0_0,
	&GenInst_SpriteRenderer_t836_0_0_0,
	&GenInst_RaycastHit_t566_0_0_0,
	&GenInst_Color_t5_0_0_0,
	&GenInst_ICanvasElement_t820_0_0_0,
	&GenInst_Font_t684_0_0_0_List_1_t842_0_0_0,
	&GenInst_Font_t684_0_0_0,
	&GenInst_ColorTween_t670_0_0_0,
	&GenInst_List_1_t724_0_0_0,
	&GenInst_UIVertex_t727_0_0_0,
	&GenInst_RectTransform_t688_0_0_0,
	&GenInst_Canvas_t690_0_0_0,
	&GenInst_CanvasRenderer_t689_0_0_0,
	&GenInst_Component_t56_0_0_0,
	&GenInst_Graphic_t687_0_0_0,
	&GenInst_Canvas_t690_0_0_0_IndexedSet_1_t852_0_0_0,
	&GenInst_Sprite_t709_0_0_0,
	&GenInst_Type_t701_0_0_0,
	&GenInst_FillMethod_t702_0_0_0,
	&GenInst_SubmitEvent_t714_0_0_0,
	&GenInst_OnChangeEvent_t716_0_0_0,
	&GenInst_OnValidateInput_t718_0_0_0,
	&GenInst_ContentType_t710_0_0_0,
	&GenInst_LineType_t713_0_0_0,
	&GenInst_InputType_t711_0_0_0,
	&GenInst_TouchScreenKeyboardType_t854_0_0_0,
	&GenInst_CharacterValidation_t712_0_0_0,
	&GenInst_Char_t556_0_0_0,
	&GenInst_UILineInfo_t857_0_0_0,
	&GenInst_UICharInfo_t859_0_0_0,
	&GenInst_LayoutElement_t779_0_0_0,
	&GenInst_Direction_t732_0_0_0,
	&GenInst_Vector2_t29_0_0_0,
	&GenInst_Selectable_t676_0_0_0,
	&GenInst_Navigation_t730_0_0_0,
	&GenInst_Transition_t743_0_0_0,
	&GenInst_ColorBlock_t682_0_0_0,
	&GenInst_SpriteState_t745_0_0_0,
	&GenInst_AnimationTriggers_t671_0_0_0,
	&GenInst_Direction_t749_0_0_0,
	&GenInst_MatEntry_t753_0_0_0,
	&GenInst_Toggle_t759_0_0_0,
	&GenInst_Toggle_t759_0_0_0_Boolean_t41_0_0_0,
	&GenInst_AspectMode_t764_0_0_0,
	&GenInst_FitMode_t770_0_0_0,
	&GenInst_Corner_t772_0_0_0,
	&GenInst_Axis_t773_0_0_0,
	&GenInst_Constraint_t774_0_0_0,
	&GenInst_RectOffset_t780_0_0_0,
	&GenInst_TextAnchor_t869_0_0_0,
	&GenInst_ILayoutElement_t827_0_0_0_Single_t36_0_0_0,
	&GenInst_List_1_t828_0_0_0,
	&GenInst_List_1_t826_0_0_0,
	&GenInst_Camera_t510_0_0_0_VideoBackgroundAbstractBehaviour_t445_0_0_0,
	&GenInst_BackgroundPlaneAbstractBehaviour_t397_0_0_0,
	&GenInst_VideoBackgroundAbstractBehaviour_t445_0_0_0,
	&GenInst_ITrackableEventHandler_t1091_0_0_0,
	&GenInst_SmartTerrainTracker_t1003_0_0_0,
	&GenInst_ReconstructionAbstractBehaviour_t431_0_0_0,
	&GenInst_ICloudRecoEventHandler_t1092_0_0_0,
	&GenInst_TargetSearchResult_t1050_0_0_0,
	&GenInst_ObjectTracker_t899_0_0_0,
	&GenInst_HideExcessAreaAbstractBehaviour_t413_0_0_0,
	&GenInst_ReconstructionFromTarget_t910_0_0_0,
	&GenInst_VirtualButton_t1061_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0_Image_t943_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0,
	&GenInst_Int32_t59_0_0_0_Trackable_t891_0_0_0,
	&GenInst_Trackable_t891_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButton_t1061_0_0_0,
	&GenInst_DataSet_t922_0_0_0,
	&GenInst_DataSetImpl_t906_0_0_0,
	&GenInst_Int32_t59_0_0_0_Marker_t1069_0_0_0,
	&GenInst_Marker_t1069_0_0_0,
	&GenInst_TrackableResultData_t969_0_0_0,
	&GenInst_SmartTerrainTrackable_t919_0_0_0,
	&GenInst_SmartTerrainTrackableBehaviour_t917_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t562_0_0_0,
	&GenInst_Int32_t59_0_0_0_WordResult_t1024_0_0_0,
	&GenInst_WordAbstractBehaviour_t457_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1022_0_0_0,
	&GenInst_Int32_t59_0_0_0_WordAbstractBehaviour_t457_0_0_0,
	&GenInst_WordData_t974_0_0_0,
	&GenInst_WordResultData_t973_0_0_0,
	&GenInst_Word_t1026_0_0_0,
	&GenInst_WordResult_t1024_0_0_0,
	&GenInst_ILoadLevelEventHandler_t1107_0_0_0,
	&GenInst_SmartTerrainInitializationInfo_t916_0_0_0,
	&GenInst_Int32_t59_0_0_0_Prop_t491_0_0_0,
	&GenInst_Int32_t59_0_0_0_Surface_t492_0_0_0,
	&GenInst_ISmartTerrainEventHandler_t1110_0_0_0,
	&GenInst_Int32_t59_0_0_0_PropAbstractBehaviour_t430_0_0_0,
	&GenInst_Int32_t59_0_0_0_SurfaceAbstractBehaviour_t436_0_0_0,
	&GenInst_PropAbstractBehaviour_t430_0_0_0,
	&GenInst_SurfaceAbstractBehaviour_t436_0_0_0,
	&GenInst_Int32_t59_0_0_0_TrackableBehaviour_t410_0_0_0,
	&GenInst_MarkerTracker_t956_0_0_0,
	&GenInst_DataSetTrackableBehaviour_t892_0_0_0,
	&GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0,
	&GenInst_VirtualButtonAbstractBehaviour_t449_0_0_0,
	&GenInst_Int32_t59_0_0_0_ImageTarget_t1063_0_0_0,
	&GenInst_ImageTarget_t1063_0_0_0,
	&GenInst_ImageTargetAbstractBehaviour_t415_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButtonAbstractBehaviour_t449_0_0_0,
	&GenInst_ITrackerEventHandler_t1118_0_0_0,
	&GenInst_TextTracker_t1005_0_0_0,
	&GenInst_WebCamAbstractBehaviour_t453_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IVideoBackgroundEventHandler_t1119_0_0_0,
	&GenInst_ITextRecoEventHandler_t1120_0_0_0,
	&GenInst_IUserDefinedTargetEventHandler_t1121_0_0_0,
	&GenInst_VuforiaAbstractBehaviour_t451_0_0_0,
	&GenInst_IVirtualButtonEventHandler_t1122_0_0_0,
	&GenInst_GcLeaderboard_t1358_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t1451_0_0_0,
	&GenInst_IAchievementU5BU5D_t1453_0_0_0,
	&GenInst_IScoreU5BU5D_t1268_0_0_0,
	&GenInst_IUserProfileU5BU5D_t1263_0_0_0,
	&GenInst_Int32_t59_0_0_0_Object_t_0_0_0,
	&GenInst_GUILayer_t1375_0_0_0,
	&GenInst_PersistentCall_t1296_0_0_0,
	&GenInst_BaseInvokableCall_t1293_0_0_0,
	&GenInst_Rigidbody2D_t1314_0_0_0,
	&GenInst_Terrain_t1334_0_0_0,
	&GenInst_Int32_t59_0_0_0_LayoutCache_t1382_0_0_0,
	&GenInst_GUILayoutEntry_t1385_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t184_0_0_0,
	&GenInst_ByteU5BU5D_t119_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t41_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2101_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t2100_0_0_0,
	&GenInst_StrongName_t2348_0_0_0,
	&GenInst_DateTimeOffset_t2423_0_0_0,
	&GenInst_TimeSpan_t565_0_0_0,
	&GenInst_Guid_t61_0_0_0,
	&GenInst_CustomAttributeData_t2097_0_0_0,
	&GenInst_IReflect_t3957_0_0_0,
	&GenInst__Type_t3955_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t2530_0_0_0,
	&GenInst__MemberInfo_t3956_0_0_0,
	&GenInst_IFormattable_t2532_0_0_0,
	&GenInst_IConvertible_t2535_0_0_0,
	&GenInst_IComparable_t2534_0_0_0,
	&GenInst_IComparable_1_t4166_0_0_0,
	&GenInst_IEquatable_1_t4171_0_0_0,
	&GenInst_ValueType_t1848_0_0_0,
	&GenInst_Double_t60_0_0_0,
	&GenInst_IComparable_1_t4183_0_0_0,
	&GenInst_IEquatable_1_t4188_0_0_0,
	&GenInst_IComparable_1_t4196_0_0_0,
	&GenInst_IEquatable_1_t4201_0_0_0,
	&GenInst_IComparable_1_t4221_0_0_0,
	&GenInst_IEquatable_1_t4226_0_0_0,
	&GenInst_Rect_t30_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t59_0_0_0,
	&GenInst_KeyValuePair_2_t2607_0_0_0,
	&GenInst_IEnumerable_t557_0_0_0,
	&GenInst_ICloneable_t2545_0_0_0,
	&GenInst_IComparable_1_t4246_0_0_0,
	&GenInst_IEquatable_1_t4251_0_0_0,
	&GenInst_Link_t1956_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t59_0_0_0_Int32_t59_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_DictionaryEntry_t58_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t59_0_0_0_KeyValuePair_2_t2607_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2624_0_0_0,
	&GenInst_Object_t53_0_0_0,
	&GenInst_Material_t45_0_0_0,
	&GenInst_KeyValuePair_2_t2634_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2634_0_0_0,
	&GenInst_String_t_0_0_0_ComponentTrack_t66_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2644_0_0_0,
	&GenInst_KeyValuePair_2_t2658_0_0_0,
	&GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Int32_t59_0_0_0,
	&GenInst_Int32_t59_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t59_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Int32_t59_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2658_0_0_0,
	&GenInst_Int32_t59_0_0_0_List_1_t460_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2669_0_0_0,
	&GenInst_Int32_t59_0_0_0_List_1_t461_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2681_0_0_0,
	&GenInst_String_t_0_0_0_GameCenterPlayerTemplate_t107_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2697_0_0_0,
	&GenInst_String_t_0_0_0_GCLeaderboard_t121_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2704_0_0_0,
	&GenInst_IComparable_1_t4299_0_0_0,
	&GenInst_IEquatable_1_t4304_0_0_0,
	&GenInst_Int32_t59_0_0_0_GCScore_t117_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2723_0_0_0,
	&GenInst_Int32_t59_0_0_0_IOSStoreProductView_t134_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2734_0_0_0,
	&GenInst_Int32_t59_0_0_0_iAdBanner_t172_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t502_0_0_0,
	&GenInst_String_t_0_0_0_iAdBanner_t172_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2748_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_GUILayoutOption_t553_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t36_0_0_0,
	&GenInst_KeyValuePair_2_t2787_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t36_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t36_0_0_0_Single_t36_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t36_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t36_0_0_0_KeyValuePair_2_t2787_0_0_0,
	&GenInst_String_t_0_0_0_Single_t36_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t569_0_0_0,
	&GenInst_Int32_t59_0_0_0_List_1_t198_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2806_0_0_0,
	&GenInst_KeyValuePair_2_t2811_0_0_0,
	&GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_Int32_t59_0_0_0,
	&GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Int32_t59_0_0_0_Int32_t59_0_0_0_KeyValuePair_2_t2811_0_0_0,
	&GenInst_MonoBehaviour_t18_0_0_0,
	&GenInst_Behaviour_t875_0_0_0,
	&GenInst_WebCamDevice_t1219_0_0_0,
	&GenInst_Bezier_t376_0_0_0,
	&GenInst_Boolean_t41_0_0_0_Object_t_0_0_0,
	&GenInst_Shader_t583_0_0_0,
	&GenInst_Entry_t619_0_0_0,
	&GenInst_Int32_t59_0_0_0_PointerEventData_t652_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t833_0_0_0,
	&GenInst_RaycastHit2D_t838_0_0_0,
	&GenInst_ICanvasElement_t820_0_0_0_Int32_t59_0_0_0,
	&GenInst_ICanvasElement_t820_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Font_t684_0_0_0_List_1_t842_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2934_0_0_0,
	&GenInst_Graphic_t687_0_0_0_Int32_t59_0_0_0,
	&GenInst_Graphic_t687_0_0_0_Int32_t59_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Canvas_t690_0_0_0_IndexedSet_1_t852_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2965_0_0_0,
	&GenInst_KeyValuePair_2_t2969_0_0_0,
	&GenInst_KeyValuePair_2_t2973_0_0_0,
	&GenInst_Enum_t63_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t41_0_0_0,
	&GenInst_LayoutRebuilder_t782_0_0_0,
	&GenInst_EyewearCalibrationReading_t913_0_0_0,
	&GenInst_Camera_t510_0_0_0_VideoBackgroundAbstractBehaviour_t445_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3025_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3060_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t942_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3060_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0_Image_t943_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3074_0_0_0,
	&GenInst_Int32_t59_0_0_0_Trackable_t891_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3087_0_0_0,
	&GenInst_Color32_t829_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButton_t1061_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3096_0_0_0,
	&GenInst_Int32_t59_0_0_0_Marker_t1069_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3113_0_0_0,
	&GenInst_SmartTerrainRevisionData_t977_0_0_0,
	&GenInst_SurfaceData_t978_0_0_0,
	&GenInst_PropData_t979_0_0_0,
	&GenInst_IEditorTrackableBehaviour_t1155_0_0_0,
	&GenInst_Image_t943_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t562_0_0_0,
	&GenInst_KeyValuePair_2_t3151_0_0_0,
	&GenInst_UInt16_t562_0_0_0,
	&GenInst_IComparable_1_t4535_0_0_0,
	&GenInst_IEquatable_1_t4540_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_UInt16_t562_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t562_0_0_0_KeyValuePair_2_t3151_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t562_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3166_0_0_0,
	&GenInst_RectangleData_t923_0_0_0,
	&GenInst_Int32_t59_0_0_0_WordResult_t1024_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3173_0_0_0,
	&GenInst_Int32_t59_0_0_0_WordAbstractBehaviour_t457_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t1174_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1022_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3192_0_0_0,
	&GenInst_List_1_t1022_0_0_0,
	&GenInst_Int32_t59_0_0_0_Surface_t492_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t1187_0_0_0,
	&GenInst_Int32_t59_0_0_0_SurfaceAbstractBehaviour_t436_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3213_0_0_0,
	&GenInst_Int32_t59_0_0_0_Prop_t491_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t1186_0_0_0,
	&GenInst_Int32_t59_0_0_0_PropAbstractBehaviour_t430_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3219_0_0_0,
	&GenInst_Int32_t59_0_0_0_TrackableBehaviour_t410_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3234_0_0_0,
	&GenInst_MarkerAbstractBehaviour_t423_0_0_0,
	&GenInst_IEditorMarkerBehaviour_t1202_0_0_0,
	&GenInst_WorldCenterTrackableBehaviour_t988_0_0_0,
	&GenInst_IEditorDataSetTrackableBehaviour_t1204_0_0_0,
	&GenInst_IEditorVirtualButtonBehaviour_t1221_0_0_0,
	&GenInst_KeyValuePair_2_t3240_0_0_0,
	&GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_Int32_t59_0_0_0,
	&GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_TrackableResultData_t969_0_0_0,
	&GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Int32_t59_0_0_0_TrackableResultData_t969_0_0_0_KeyValuePair_2_t3240_0_0_0,
	&GenInst_KeyValuePair_2_t3255_0_0_0,
	&GenInst_VirtualButtonData_t970_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_Int32_t59_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_VirtualButtonData_t970_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButtonData_t970_0_0_0_KeyValuePair_2_t3255_0_0_0,
	&GenInst_Int32_t59_0_0_0_ImageTarget_t1063_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3285_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1064_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0,
	&GenInst_KeyValuePair_2_t3292_0_0_0,
	&GenInst_ProfileData_t1064_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_ProfileData_t1064_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1064_0_0_0_KeyValuePair_2_t3292_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1064_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3306_0_0_0,
	&GenInst_Int32_t59_0_0_0_VirtualButtonAbstractBehaviour_t449_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3311_0_0_0,
	&GenInst_Link_t3340_0_0_0,
	&GenInst_ParameterModifier_t2119_0_0_0,
	&GenInst_UserProfile_t1262_0_0_0,
	&GenInst_IUserProfile_t3914_0_0_0,
	&GenInst_IAchievementDescription_t3915_0_0_0,
	&GenInst_IAchievement_t1436_0_0_0,
	&GenInst_IScore_t1270_0_0_0,
	&GenInst_AchievementDescription_t1265_0_0_0,
	&GenInst_GcAchievementData_t1244_0_0_0,
	&GenInst_Achievement_t1264_0_0_0,
	&GenInst_GcScoreData_t1245_0_0_0,
	&GenInst_Score_t1266_0_0_0,
	&GenInst_HitInfo_t1271_0_0_0,
	&GenInst_ParameterInfo_t1459_0_0_0,
	&GenInst__ParameterInfo_t3998_0_0_0,
	&GenInst_Event_t725_0_0_0_TextEditOp_t1286_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0,
	&GenInst_KeyValuePair_2_t3373_0_0_0,
	&GenInst_TextEditOp_t1286_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_TextEditOp_t1286_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1286_0_0_0_KeyValuePair_2_t3373_0_0_0,
	&GenInst_Event_t725_0_0_0,
	&GenInst_Event_t725_0_0_0_TextEditOp_t1286_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3387_0_0_0,
	&GenInst_Keyframe_t1321_0_0_0,
	&GenInst_Int32_t59_0_0_0_LayoutCache_t1382_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3446_0_0_0,
	&GenInst_GUIStyle_t184_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t184_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3457_0_0_0,
	&GenInst_Display_t1421_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t2564_0_0_0,
	&GenInst_KeyValuePair_2_t3478_0_0_0,
	&GenInst_IComparable_1_t4767_0_0_0,
	&GenInst_IEquatable_1_t4772_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_Boolean_t41_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t41_0_0_0_KeyValuePair_2_t3478_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t41_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t3493_0_0_0,
	&GenInst_X509Certificate_t1546_0_0_0,
	&GenInst_IDeserializationCallback_t2567_0_0_0,
	&GenInst_X509ChainStatus_t1558_0_0_0,
	&GenInst_Capture_t1579_0_0_0,
	&GenInst_Group_t1582_0_0_0,
	&GenInst_Mark_t1605_0_0_0,
	&GenInst_UriScheme_t1641_0_0_0,
	&GenInst_UInt32_t558_0_0_0,
	&GenInst_IComparable_1_t4807_0_0_0,
	&GenInst_IEquatable_1_t4812_0_0_0,
	&GenInst_BigInteger_t1692_0_0_0,
	&GenInst_KeySizes_t1812_0_0_0,
	&GenInst_ClientCertificateType_t1784_0_0_0,
	&GenInst_UInt64_t563_0_0_0,
	&GenInst_SByte_t559_0_0_0,
	&GenInst_Int16_t561_0_0_0,
	&GenInst_Decimal_t564_0_0_0,
	&GenInst_Delegate_t506_0_0_0,
	&GenInst_IComparable_1_t4829_0_0_0,
	&GenInst_IEquatable_1_t4830_0_0_0,
	&GenInst_IComparable_1_t4833_0_0_0,
	&GenInst_IEquatable_1_t4834_0_0_0,
	&GenInst_IComparable_1_t4831_0_0_0,
	&GenInst_IEquatable_1_t4832_0_0_0,
	&GenInst_IComparable_1_t4827_0_0_0,
	&GenInst_IEquatable_1_t4828_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t3994_0_0_0,
	&GenInst_ConstructorInfo_t1464_0_0_0,
	&GenInst__ConstructorInfo_t3992_0_0_0,
	&GenInst_MethodBase_t1457_0_0_0,
	&GenInst__MethodBase_t3995_0_0_0,
	&GenInst_TableRange_t1889_0_0_0,
	&GenInst_TailoringInfo_t1892_0_0_0,
	&GenInst_Contraction_t1893_0_0_0,
	&GenInst_Level2Map_t1895_0_0_0,
	&GenInst_BigInteger_t1916_0_0_0,
	&GenInst_Slot_t1966_0_0_0,
	&GenInst_Slot_t1974_0_0_0,
	&GenInst_StackFrame_t1456_0_0_0,
	&GenInst_Calendar_t1988_0_0_0,
	&GenInst_ModuleBuilder_t2063_0_0_0,
	&GenInst__ModuleBuilder_t3987_0_0_0,
	&GenInst_Module_t2059_0_0_0,
	&GenInst__Module_t3997_0_0_0,
	&GenInst_ParameterBuilder_t2069_0_0_0,
	&GenInst__ParameterBuilder_t3988_0_0_0,
	&GenInst_TypeU5BU5D_t1218_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t1653_0_0_0,
	&GenInst_IList_t488_0_0_0,
	&GenInst_ILTokenInfo_t2053_0_0_0,
	&GenInst_LabelData_t2055_0_0_0,
	&GenInst_LabelFixup_t2054_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t2051_0_0_0,
	&GenInst_TypeBuilder_t2045_0_0_0,
	&GenInst__TypeBuilder_t3989_0_0_0,
	&GenInst_MethodBuilder_t2052_0_0_0,
	&GenInst__MethodBuilder_t3986_0_0_0,
	&GenInst__MethodInfo_t3996_0_0_0,
	&GenInst_ConstructorBuilder_t2043_0_0_0,
	&GenInst__ConstructorBuilder_t3982_0_0_0,
	&GenInst_FieldBuilder_t2049_0_0_0,
	&GenInst__FieldBuilder_t3984_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t3999_0_0_0,
	&GenInst_ResourceInfo_t2130_0_0_0,
	&GenInst_ResourceCacheItem_t2131_0_0_0,
	&GenInst_IContextProperty_t2524_0_0_0,
	&GenInst_Header_t2211_0_0_0,
	&GenInst_ITrackingHandler_t2559_0_0_0,
	&GenInst_IContextAttribute_t2546_0_0_0,
	&GenInst_IComparable_1_t5071_0_0_0,
	&GenInst_IEquatable_1_t5076_0_0_0,
	&GenInst_IComparable_1_t4835_0_0_0,
	&GenInst_IEquatable_1_t4836_0_0_0,
	&GenInst_IComparable_1_t5095_0_0_0,
	&GenInst_IEquatable_1_t5100_0_0_0,
	&GenInst_TypeTag_t2267_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t1518_0_0_0,
	&GenInst_DictionaryEntry_t58_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_KeyValuePair_2_t2607_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2607_0_0_0_KeyValuePair_2_t2607_0_0_0,
	&GenInst_KeyValuePair_2_t2634_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2634_0_0_0_KeyValuePair_2_t2634_0_0_0,
	&GenInst_KeyValuePair_2_t2658_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2658_0_0_0_KeyValuePair_2_t2658_0_0_0,
	&GenInst_Byte_t560_0_0_0_Byte_t560_0_0_0,
	&GenInst_EmptyMockDialog_t271_0_0_0,
	&GenInst_Single_t36_0_0_0_Object_t_0_0_0,
	&GenInst_Single_t36_0_0_0_Single_t36_0_0_0,
	&GenInst_KeyValuePair_2_t2787_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2787_0_0_0_KeyValuePair_2_t2787_0_0_0,
	&GenInst_KeyValuePair_2_t2811_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2811_0_0_0_KeyValuePair_2_t2811_0_0_0,
	&GenInst_RaycastResult_t647_0_0_0_RaycastResult_t647_0_0_0,
	&GenInst_UIVertex_t727_0_0_0_UIVertex_t727_0_0_0,
	&GenInst_PIXEL_FORMAT_t942_0_0_0_PIXEL_FORMAT_t942_0_0_0,
	&GenInst_KeyValuePair_2_t3060_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3060_0_0_0_KeyValuePair_2_t3060_0_0_0,
	&GenInst_UInt16_t562_0_0_0_Object_t_0_0_0,
	&GenInst_UInt16_t562_0_0_0_UInt16_t562_0_0_0,
	&GenInst_KeyValuePair_2_t3151_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3151_0_0_0_KeyValuePair_2_t3151_0_0_0,
	&GenInst_TrackableResultData_t969_0_0_0_Object_t_0_0_0,
	&GenInst_TrackableResultData_t969_0_0_0_TrackableResultData_t969_0_0_0,
	&GenInst_KeyValuePair_2_t3240_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3240_0_0_0_KeyValuePair_2_t3240_0_0_0,
	&GenInst_VirtualButtonData_t970_0_0_0_Object_t_0_0_0,
	&GenInst_VirtualButtonData_t970_0_0_0_VirtualButtonData_t970_0_0_0,
	&GenInst_KeyValuePair_2_t3255_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3255_0_0_0_KeyValuePair_2_t3255_0_0_0,
	&GenInst_TargetSearchResult_t1050_0_0_0_TargetSearchResult_t1050_0_0_0,
	&GenInst_ProfileData_t1064_0_0_0_Object_t_0_0_0,
	&GenInst_ProfileData_t1064_0_0_0_ProfileData_t1064_0_0_0,
	&GenInst_KeyValuePair_2_t3292_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3292_0_0_0_KeyValuePair_2_t3292_0_0_0,
	&GenInst_TextEditOp_t1286_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t1286_0_0_0_TextEditOp_t1286_0_0_0,
	&GenInst_KeyValuePair_2_t3373_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3373_0_0_0_KeyValuePair_2_t3373_0_0_0,
	&GenInst_UICharInfo_t859_0_0_0_UICharInfo_t859_0_0_0,
	&GenInst_UILineInfo_t857_0_0_0_UILineInfo_t857_0_0_0,
	&GenInst_Boolean_t41_0_0_0_Boolean_t41_0_0_0,
	&GenInst_KeyValuePair_2_t3478_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3478_0_0_0_KeyValuePair_2_t3478_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2101_0_0_0_CustomAttributeTypedArgument_t2101_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t2100_0_0_0_CustomAttributeNamedArgument_t2100_0_0_0,
	&GenInst_ISN_Singleton_1_t3898_gp_0_0_0_0,
	&GenInst_SAC_Singleton_1_t3899_gp_0_0_0_0,
	&GenInst_CallbackManager_AddFacebookDelegate_m31401_gp_0_0_0_0,
	&GenInst_CallbackManager_TryCallCallback_m31402_gp_0_0_0_0,
	&GenInst_CanvasUIMethodCall_1_t3900_gp_0_0_0_0,
	&GenInst_ComponentFactory_GetComponent_m31411_gp_0_0_0_0,
	&GenInst_ComponentFactory_AddComponent_m31412_gp_0_0_0_0,
	&GenInst_MethodArguments_AddNullablePrimitive_m31466_gp_0_0_0_0,
	&GenInst_MethodArguments_AddList_m31467_gp_0_0_0_0,
	&GenInst_MethodCall_1_t3901_gp_0_0_0_0,
	&GenInst_JavaMethodCall_1_t3902_gp_0_0_0_0,
	&GenInst_FBJavaClass_CallStatic_m31480_gp_0_0_0_0,
	&GenInst_IOSFacebook_AddCallback_m31490_gp_0_0_0_0,
	&GenInst_EditorFacebook_ShowEmptyMockDialog_m31494_gp_0_0_0_0,
	&GenInst_Utilities_GetValueOrDefault_m31519_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m31542_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m31543_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m31545_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m31546_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m31547_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t3906_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t3908_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t3908_gp_0_0_0_0_Int32_t59_0_0_0,
	&GenInst_ObjectPool_1_t3909_gp_0_0_0_0,
	&GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m31999_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t3916_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3917_gp_0_0_0_0_InvokableCall_2_t3917_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t3917_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3917_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3918_gp_0_0_0_0_InvokableCall_3_t3918_gp_1_0_0_0_InvokableCall_3_t3918_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3918_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3918_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3918_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3919_gp_0_0_0_0_InvokableCall_4_t3919_gp_1_0_0_0_InvokableCall_4_t3919_gp_2_0_0_0_InvokableCall_4_t3919_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3919_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3919_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3919_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3919_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t1465_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t3920_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t3921_gp_0_0_0_0_UnityEvent_2_t3921_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t3922_gp_0_0_0_0_UnityEvent_3_t3922_gp_1_0_0_0_UnityEvent_3_t3922_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t3923_gp_0_0_0_0_UnityEvent_4_t3923_gp_1_0_0_0_UnityEvent_4_t3923_gp_2_0_0_0_UnityEvent_4_t3923_gp_3_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m32295_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m32297_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m32299_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m32300_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m32307_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m32311_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m32312_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m32313_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m32314_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m32316_gp_0_0_0_0,
	&GenInst_LinkedList_1_t3930_gp_0_0_0_0,
	&GenInst_Enumerator_t3931_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t3932_gp_0_0_0_0,
	&GenInst_Stack_1_t3933_gp_0_0_0_0,
	&GenInst_Enumerator_t3934_gp_0_0_0_0,
	&GenInst_HashSet_1_t3936_gp_0_0_0_0,
	&GenInst_Enumerator_t3938_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3939_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m32483_gp_0_0_0_0,
	&GenInst_Enumerable_Cast_m32484_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m32485_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m32486_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m32487_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m32488_gp_0_0_0_0,
	&GenInst_Enumerable_First_m32489_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m32490_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m32490_gp_0_0_0_0_Enumerable_Select_m32490_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m32490_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m32491_gp_0_0_0_0_Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m32491_gp_1_0_0_0,
	&GenInst_Enumerable_ToArray_m32492_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m32493_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m32494_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m32494_gp_0_0_0_0_Boolean_t41_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m32495_gp_0_0_0_0_Boolean_t41_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3940_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3941_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3942_gp_0_0_0_0_Boolean_t41_0_0_0,
	&GenInst_IEnumerable_1_t3949_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m32557_gp_0_0_0_0,
	&GenInst_Array_Sort_m32569_gp_0_0_0_0_Array_Sort_m32569_gp_0_0_0_0,
	&GenInst_Array_Sort_m32570_gp_0_0_0_0_Array_Sort_m32570_gp_1_0_0_0,
	&GenInst_Array_Sort_m32571_gp_0_0_0_0,
	&GenInst_Array_Sort_m32571_gp_0_0_0_0_Array_Sort_m32571_gp_0_0_0_0,
	&GenInst_Array_Sort_m32572_gp_0_0_0_0,
	&GenInst_Array_Sort_m32572_gp_0_0_0_0_Array_Sort_m32572_gp_1_0_0_0,
	&GenInst_Array_Sort_m32573_gp_0_0_0_0_Array_Sort_m32573_gp_0_0_0_0,
	&GenInst_Array_Sort_m32574_gp_0_0_0_0_Array_Sort_m32574_gp_1_0_0_0,
	&GenInst_Array_Sort_m32575_gp_0_0_0_0,
	&GenInst_Array_Sort_m32575_gp_0_0_0_0_Array_Sort_m32575_gp_0_0_0_0,
	&GenInst_Array_Sort_m32576_gp_0_0_0_0,
	&GenInst_Array_Sort_m32576_gp_1_0_0_0,
	&GenInst_Array_Sort_m32576_gp_0_0_0_0_Array_Sort_m32576_gp_1_0_0_0,
	&GenInst_Array_Sort_m32577_gp_0_0_0_0,
	&GenInst_Array_Sort_m32578_gp_0_0_0_0,
	&GenInst_Array_qsort_m32579_gp_0_0_0_0,
	&GenInst_Array_qsort_m32579_gp_0_0_0_0_Array_qsort_m32579_gp_1_0_0_0,
	&GenInst_Array_compare_m32580_gp_0_0_0_0,
	&GenInst_Array_qsort_m32581_gp_0_0_0_0,
	&GenInst_Array_Resize_m32584_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m32586_gp_0_0_0_0,
	&GenInst_Array_ForEach_m32587_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m32588_gp_0_0_0_0_Array_ConvertAll_m32588_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m32589_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m32590_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m32591_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m32592_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m32593_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m32594_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m32595_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m32596_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m32597_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m32598_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m32599_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m32600_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m32601_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m32602_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m32603_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m32604_gp_0_0_0_0,
	&GenInst_Array_FindAll_m32605_gp_0_0_0_0,
	&GenInst_Array_Exists_m32606_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m32607_gp_0_0_0_0,
	&GenInst_Array_Find_m32608_gp_0_0_0_0,
	&GenInst_Array_FindLast_m32609_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3950_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3951_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3952_gp_0_0_0_0,
	&GenInst_IList_1_t3953_gp_0_0_0_0,
	&GenInst_ICollection_1_t3954_gp_0_0_0_0,
	&GenInst_Nullable_1_t2536_gp_0_0_0_0,
	&GenInst_Comparer_1_t3961_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3962_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t3897_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3963_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t5308_0_0_0,
	&GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m32760_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m32765_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_DictionaryEntry_t58_0_0_0,
	&GenInst_ShimEnumerator_t3964_gp_0_0_0_0_ShimEnumerator_t3964_gp_1_0_0_0,
	&GenInst_Enumerator_t3965_gp_0_0_0_0_Enumerator_t3965_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t5322_0_0_0,
	&GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_1_0_0_0,
	&GenInst_KeyCollection_t3966_gp_0_0_0_0,
	&GenInst_Enumerator_t3967_gp_0_0_0_0_Enumerator_t3967_gp_1_0_0_0,
	&GenInst_Enumerator_t3967_gp_0_0_0_0,
	&GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_1_0_0_0_KeyCollection_t3966_gp_0_0_0_0,
	&GenInst_KeyCollection_t3966_gp_0_0_0_0_KeyCollection_t3966_gp_0_0_0_0,
	&GenInst_ValueCollection_t3968_gp_0_0_0_0_ValueCollection_t3968_gp_1_0_0_0,
	&GenInst_ValueCollection_t3968_gp_1_0_0_0,
	&GenInst_Enumerator_t3969_gp_0_0_0_0_Enumerator_t3969_gp_1_0_0_0,
	&GenInst_Enumerator_t3969_gp_1_0_0_0,
	&GenInst_ValueCollection_t3968_gp_0_0_0_0_ValueCollection_t3968_gp_1_0_0_0_ValueCollection_t3968_gp_1_0_0_0,
	&GenInst_ValueCollection_t3968_gp_1_0_0_0_ValueCollection_t3968_gp_1_0_0_0,
	&GenInst_Dictionary_2_t3963_gp_0_0_0_0_Dictionary_2_t3963_gp_1_0_0_0_KeyValuePair_2_t5308_0_0_0,
	&GenInst_KeyValuePair_2_t5308_0_0_0_KeyValuePair_2_t5308_0_0_0,
	&GenInst_Dictionary_2_t3963_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t3971_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3972_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t3896_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t5358_0_0_0,
	&GenInst_IDictionary_2_t3974_gp_0_0_0_0_IDictionary_2_t3974_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3976_gp_0_0_0_0_KeyValuePair_2_t3976_gp_1_0_0_0,
	&GenInst_List_1_t3977_gp_0_0_0_0,
	&GenInst_Enumerator_t3978_gp_0_0_0_0,
	&GenInst_Collection_1_t3979_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3980_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m33050_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m33050_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m33051_gp_0_0_0_0,
};
