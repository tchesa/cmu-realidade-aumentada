﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX2_AutoRotate
struct CFX2_AutoRotate_t324;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX2_AutoRotate::.ctor()
extern "C" void CFX2_AutoRotate__ctor_m1764 (CFX2_AutoRotate_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_AutoRotate::Update()
extern "C" void CFX2_AutoRotate_Update_m1765 (CFX2_AutoRotate_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
