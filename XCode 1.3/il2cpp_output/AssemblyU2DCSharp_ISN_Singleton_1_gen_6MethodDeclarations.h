﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_Singleton`1<System.Object>
struct ISN_Singleton_1_t2689;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_Singleton`1<System.Object>::.ctor()
extern "C" void ISN_Singleton_1__ctor_m16858_gshared (ISN_Singleton_1_t2689 * __this, const MethodInfo* method);
#define ISN_Singleton_1__ctor_m16858(__this, method) (( void (*) (ISN_Singleton_1_t2689 *, const MethodInfo*))ISN_Singleton_1__ctor_m16858_gshared)(__this, method)
// System.Void ISN_Singleton`1<System.Object>::.cctor()
extern "C" void ISN_Singleton_1__cctor_m16859_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ISN_Singleton_1__cctor_m16859(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1__cctor_m16859_gshared)(__this /* static, unused */, method)
// T ISN_Singleton`1<System.Object>::get_instance()
extern "C" Object_t * ISN_Singleton_1_get_instance_m16860_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ISN_Singleton_1_get_instance_m16860(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_instance_m16860_gshared)(__this /* static, unused */, method)
// System.Boolean ISN_Singleton`1<System.Object>::get_HasInstance()
extern "C" bool ISN_Singleton_1_get_HasInstance_m16861_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ISN_Singleton_1_get_HasInstance_m16861(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_HasInstance_m16861_gshared)(__this /* static, unused */, method)
// System.Boolean ISN_Singleton`1<System.Object>::get_IsDestroyed()
extern "C" bool ISN_Singleton_1_get_IsDestroyed_m16862_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ISN_Singleton_1_get_IsDestroyed_m16862(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_IsDestroyed_m16862_gshared)(__this /* static, unused */, method)
// System.Void ISN_Singleton`1<System.Object>::OnDestroy()
extern "C" void ISN_Singleton_1_OnDestroy_m16863_gshared (ISN_Singleton_1_t2689 * __this, const MethodInfo* method);
#define ISN_Singleton_1_OnDestroy_m16863(__this, method) (( void (*) (ISN_Singleton_1_t2689 *, const MethodInfo*))ISN_Singleton_1_OnDestroy_m16863_gshared)(__this, method)
// System.Void ISN_Singleton`1<System.Object>::OnApplicationQuit()
extern "C" void ISN_Singleton_1_OnApplicationQuit_m16864_gshared (ISN_Singleton_1_t2689 * __this, const MethodInfo* method);
#define ISN_Singleton_1_OnApplicationQuit_m16864(__this, method) (( void (*) (ISN_Singleton_1_t2689 *, const MethodInfo*))ISN_Singleton_1_OnApplicationQuit_m16864_gshared)(__this, method)
