﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FacebookSettings/UrlSchemes
struct UrlSchemes_t245;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.FacebookSettings/UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
extern "C" void UrlSchemes__ctor_m1368 (UrlSchemes_t245 * __this, List_1_t78 * ___schemes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Facebook.Unity.FacebookSettings/UrlSchemes::get_Schemes()
extern "C" List_1_t78 * UrlSchemes_get_Schemes_m1369 (UrlSchemes_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookSettings/UrlSchemes::set_Schemes(System.Collections.Generic.List`1<System.String>)
extern "C" void UrlSchemes_set_Schemes_m1370 (UrlSchemes_t245 * __this, List_1_t78 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
