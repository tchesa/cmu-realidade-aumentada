﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcherBase.h"

// IOSStoreProductView
struct  IOSStoreProductView_t134  : public EventDispatcherBase_t72
{
	// System.Collections.Generic.List`1<System.String> IOSStoreProductView::_ids
	List_1_t78 * ____ids_6;
	// System.Int32 IOSStoreProductView::_id
	int32_t ____id_7;
};
