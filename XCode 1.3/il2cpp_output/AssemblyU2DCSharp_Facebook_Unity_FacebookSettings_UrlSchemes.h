﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t78;

#include "mscorlib_System_Object.h"

// Facebook.Unity.FacebookSettings/UrlSchemes
struct  UrlSchemes_t245  : public Object_t
{
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.FacebookSettings/UrlSchemes::list
	List_1_t78 * ___list_0;
};
