﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iAdIOSBanner
struct iAdIOSBanner_t176;
// System.Collections.Generic.Dictionary`2<System.String,iAdBanner>
struct Dictionary_2_t177;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void iAdIOSBanner::.ctor()
extern "C" void iAdIOSBanner__ctor_m997 (iAdIOSBanner_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdIOSBanner::.cctor()
extern "C" void iAdIOSBanner__cctor_m998 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdIOSBanner::Start()
extern "C" void iAdIOSBanner_Start_m999 (iAdIOSBanner_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdIOSBanner::OnDestroy()
extern "C" void iAdIOSBanner_OnDestroy_m1000 (iAdIOSBanner_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdIOSBanner::ShowBanner()
extern "C" void iAdIOSBanner_ShowBanner_m1001 (iAdIOSBanner_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdIOSBanner::HideBanner()
extern "C" void iAdIOSBanner_HideBanner_m1002 (iAdIOSBanner_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,iAdBanner> iAdIOSBanner::get_registeredBanners()
extern "C" Dictionary_2_t177 * iAdIOSBanner_get_registeredBanners_m1003 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String iAdIOSBanner::get_sceneBannerId()
extern "C" String_t* iAdIOSBanner_get_sceneBannerId_m1004 (iAdIOSBanner_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
