﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARAnimation/ShineTrigger
struct ShineTrigger_t343;

#include "codegen/il2cpp-codegen.h"

// System.Void ARAnimation/ShineTrigger::.ctor()
extern "C" void ShineTrigger__ctor_m1809 (ShineTrigger_t343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
