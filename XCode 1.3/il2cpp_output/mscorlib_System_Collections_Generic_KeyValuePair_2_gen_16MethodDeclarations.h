﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m17376(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2723 *, int32_t, GCScore_t117 *, const MethodInfo*))KeyValuePair_2__ctor_m16393_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>::get_Key()
#define KeyValuePair_2_get_Key_m17377(__this, method) (( int32_t (*) (KeyValuePair_2_t2723 *, const MethodInfo*))KeyValuePair_2_get_Key_m16394_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17378(__this, ___value, method) (( void (*) (KeyValuePair_2_t2723 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16395_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>::get_Value()
#define KeyValuePair_2_get_Value_m17379(__this, method) (( GCScore_t117 * (*) (KeyValuePair_2_t2723 *, const MethodInfo*))KeyValuePair_2_get_Value_m16396_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17380(__this, ___value, method) (( void (*) (KeyValuePair_2_t2723 *, GCScore_t117 *, const MethodInfo*))KeyValuePair_2_set_Value_m16397_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>::ToString()
#define KeyValuePair_2_ToString_m17381(__this, method) (( String_t* (*) (KeyValuePair_2_t2723 *, const MethodInfo*))KeyValuePair_2_ToString_m16398_gshared)(__this, method)
