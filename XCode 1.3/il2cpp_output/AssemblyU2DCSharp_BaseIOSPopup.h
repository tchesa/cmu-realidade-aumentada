﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// BaseIOSPopup
struct  BaseIOSPopup_t164  : public EventDispatcher_t69
{
	// System.String BaseIOSPopup::title
	String_t* ___title_3;
	// System.String BaseIOSPopup::message
	String_t* ___message_4;
};
