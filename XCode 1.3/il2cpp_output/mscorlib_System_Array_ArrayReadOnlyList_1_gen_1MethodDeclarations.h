﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t3565;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2538;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3767;
// System.Exception
struct Exception_t40;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m29924_gshared (ArrayReadOnlyList_1_t3565 * __this, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m29924(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t3565 *, CustomAttributeNamedArgumentU5BU5D_t2538*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m29924_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29925_gshared (ArrayReadOnlyList_1_t3565 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29925(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3565 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29925_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t2100  ArrayReadOnlyList_1_get_Item_m29926_gshared (ArrayReadOnlyList_1_t3565 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m29926(__this, ___index, method) (( CustomAttributeNamedArgument_t2100  (*) (ArrayReadOnlyList_1_t3565 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m29926_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m29927_gshared (ArrayReadOnlyList_1_t3565 * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m29927(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t3565 *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m29927_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m29928_gshared (ArrayReadOnlyList_1_t3565 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m29928(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t3565 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m29928_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m29929_gshared (ArrayReadOnlyList_1_t3565 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m29929(__this, method) (( bool (*) (ArrayReadOnlyList_1_t3565 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m29929_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m29930_gshared (ArrayReadOnlyList_1_t3565 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m29930(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3565 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))ArrayReadOnlyList_1_Add_m29930_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m29931_gshared (ArrayReadOnlyList_1_t3565 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m29931(__this, method) (( void (*) (ArrayReadOnlyList_1_t3565 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m29931_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m29932_gshared (ArrayReadOnlyList_1_t3565 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m29932(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3565 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m29932_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m29933_gshared (ArrayReadOnlyList_1_t3565 * __this, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m29933(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3565 *, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m29933_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m29934_gshared (ArrayReadOnlyList_1_t3565 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m29934(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t3565 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m29934_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m29935_gshared (ArrayReadOnlyList_1_t3565 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m29935(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t3565 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m29935_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m29936_gshared (ArrayReadOnlyList_1_t3565 * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m29936(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3565 *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m29936_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m29937_gshared (ArrayReadOnlyList_1_t3565 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m29937(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3565 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m29937_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m29938_gshared (ArrayReadOnlyList_1_t3565 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m29938(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3565 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m29938_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern "C" Exception_t40 * ArrayReadOnlyList_1_ReadOnlyError_m29939_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m29939(__this /* static, unused */, method) (( Exception_t40 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m29939_gshared)(__this /* static, unused */, method)
