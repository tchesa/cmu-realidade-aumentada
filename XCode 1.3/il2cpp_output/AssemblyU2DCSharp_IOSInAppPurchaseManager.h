﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<IOSStoreKitResponse>
struct Action_1_t128;
// System.Action`1<ISN_Result>
struct Action_1_t99;
// System.Action`1<System.Boolean>
struct Action_1_t129;
// System.Action`1<IOSStoreKitVerificationResponse>
struct Action_1_t130;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;
// System.Collections.Generic.List`1<IOSProductTemplate>
struct List_1_t131;
// System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>
struct Dictionary_2_t132;
// IOSInAppPurchaseManager
struct IOSInAppPurchaseManager_t127;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// IOSInAppPurchaseManager
struct  IOSInAppPurchaseManager_t127  : public EventDispatcher_t69
{
	// System.Action`1<IOSStoreKitResponse> IOSInAppPurchaseManager::OnTransactionComplete
	Action_1_t128 * ___OnTransactionComplete_11;
	// System.Action`1<ISN_Result> IOSInAppPurchaseManager::OnRestoreComplete
	Action_1_t99 * ___OnRestoreComplete_12;
	// System.Action`1<ISN_Result> IOSInAppPurchaseManager::OnStoreKitInitComplete
	Action_1_t99 * ___OnStoreKitInitComplete_13;
	// System.Action`1<System.Boolean> IOSInAppPurchaseManager::OnPurchasesStateSettingsLoaded
	Action_1_t129 * ___OnPurchasesStateSettingsLoaded_14;
	// System.Action`1<IOSStoreKitVerificationResponse> IOSInAppPurchaseManager::OnVerificationComplete
	Action_1_t130 * ___OnVerificationComplete_15;
	// System.Boolean IOSInAppPurchaseManager::_IsStoreLoaded
	bool ____IsStoreLoaded_16;
	// System.Boolean IOSInAppPurchaseManager::_IsWaitingLoadResult
	bool ____IsWaitingLoadResult_17;
	// System.Boolean IOSInAppPurchaseManager::_IsInAppPurchasesEnabled
	bool ____IsInAppPurchasesEnabled_18;
	// System.Collections.Generic.List`1<System.String> IOSInAppPurchaseManager::_productsIds
	List_1_t78 * ____productsIds_20;
	// System.Collections.Generic.List`1<IOSProductTemplate> IOSInAppPurchaseManager::_products
	List_1_t131 * ____products_21;
	// System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView> IOSInAppPurchaseManager::_productsView
	Dictionary_2_t132 * ____productsView_22;
};
struct IOSInAppPurchaseManager_t127_StaticFields{
	// System.Int32 IOSInAppPurchaseManager::_nextId
	int32_t ____nextId_19;
	// IOSInAppPurchaseManager IOSInAppPurchaseManager::_instance
	IOSInAppPurchaseManager_t127 * ____instance_23;
	// System.String IOSInAppPurchaseManager::lastPurchasedProduct
	String_t* ___lastPurchasedProduct_24;
	// System.Action`1<IOSStoreKitResponse> IOSInAppPurchaseManager::<>f__am$cacheE
	Action_1_t128 * ___U3CU3Ef__amU24cacheE_25;
	// System.Action`1<ISN_Result> IOSInAppPurchaseManager::<>f__am$cacheF
	Action_1_t99 * ___U3CU3Ef__amU24cacheF_26;
	// System.Action`1<ISN_Result> IOSInAppPurchaseManager::<>f__am$cache10
	Action_1_t99 * ___U3CU3Ef__amU24cache10_27;
	// System.Action`1<System.Boolean> IOSInAppPurchaseManager::<>f__am$cache11
	Action_1_t129 * ___U3CU3Ef__amU24cache11_28;
	// System.Action`1<IOSStoreKitVerificationResponse> IOSInAppPurchaseManager::<>f__am$cache12
	Action_1_t130 * ___U3CU3Ef__amU24cache12_29;
};
