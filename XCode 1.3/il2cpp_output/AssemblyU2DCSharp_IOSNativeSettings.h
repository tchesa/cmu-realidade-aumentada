﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;
// IOSNativeSettings
struct IOSNativeSettings_t82;

#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "AssemblyU2DCSharp_IOSGalleryLoadImageFormat.h"

// IOSNativeSettings
struct  IOSNativeSettings_t82  : public ScriptableObject_t77
{
	// System.String IOSNativeSettings::AppleId
	String_t* ___AppleId_5;
	// System.Collections.Generic.List`1<System.String> IOSNativeSettings::InAppProducts
	List_1_t78 * ___InAppProducts_6;
	// System.Boolean IOSNativeSettings::SendFakeEventsInEditor
	bool ___SendFakeEventsInEditor_7;
	// System.Collections.Generic.List`1<System.String> IOSNativeSettings::RegisteredAchievementsIds
	List_1_t78 * ___RegisteredAchievementsIds_8;
	// System.Collections.Generic.List`1<System.String> IOSNativeSettings::DefaultStoreProductsView
	List_1_t78 * ___DefaultStoreProductsView_9;
	// System.Boolean IOSNativeSettings::checkInternetBeforeLoadRequest
	bool ___checkInternetBeforeLoadRequest_10;
	// System.Boolean IOSNativeSettings::ShowStoreKitParams
	bool ___ShowStoreKitParams_11;
	// System.Boolean IOSNativeSettings::ShowGCParams
	bool ___ShowGCParams_12;
	// System.Boolean IOSNativeSettings::ShowAchievementsParams
	bool ___ShowAchievementsParams_13;
	// System.Boolean IOSNativeSettings::ShowOtherParams
	bool ___ShowOtherParams_14;
	// System.Boolean IOSNativeSettings::ShowCameraAndGalleryParams
	bool ___ShowCameraAndGalleryParams_15;
	// System.Boolean IOSNativeSettings::DisablePluginLogs
	bool ___DisablePluginLogs_16;
	// System.Boolean IOSNativeSettings::UseGCRequestCaching
	bool ___UseGCRequestCaching_17;
	// System.Boolean IOSNativeSettings::UsePPForAchievements
	bool ___UsePPForAchievements_18;
	// System.Boolean IOSNativeSettings::EnablePushNotificationsAPI
	bool ___EnablePushNotificationsAPI_19;
	// System.Int32 IOSNativeSettings::MaxImageLoadSize
	int32_t ___MaxImageLoadSize_20;
	// System.Single IOSNativeSettings::JPegCompressionRate
	float ___JPegCompressionRate_21;
	// IOSGalleryLoadImageFormat IOSNativeSettings::GalleryImageFormat
	int32_t ___GalleryImageFormat_22;
};
struct IOSNativeSettings_t82_StaticFields{
	// IOSNativeSettings IOSNativeSettings::instance
	IOSNativeSettings_t82 * ___instance_23;
};
