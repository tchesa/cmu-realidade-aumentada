﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSSharedApplication
struct IOSSharedApplication_t153;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// ISN_Singleton`1<IOSSharedApplication>
struct  ISN_Singleton_1_t154  : public EventDispatcher_t69
{
};
struct ISN_Singleton_1_t154_StaticFields{
	// T ISN_Singleton`1<IOSSharedApplication>::_instance
	IOSSharedApplication_t153 * ____instance_3;
	// System.Boolean ISN_Singleton`1<IOSSharedApplication>::applicationIsQuitting
	bool ___applicationIsQuitting_4;
};
