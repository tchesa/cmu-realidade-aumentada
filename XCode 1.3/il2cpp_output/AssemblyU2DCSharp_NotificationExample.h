﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_BaseIOSFeaturePreview.h"

// NotificationExample
struct  NotificationExample_t209  : public BaseIOSFeaturePreview_t183
{
	// System.Int32 NotificationExample::lastNotificationId
	int32_t ___lastNotificationId_11;
};
