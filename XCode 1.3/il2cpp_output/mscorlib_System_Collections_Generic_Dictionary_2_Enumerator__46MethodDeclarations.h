﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__45MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m27829(__this, ___dictionary, method) (( void (*) (Enumerator_t3390 *, Dictionary_2_t1287 *, const MethodInfo*))Enumerator__ctor_m27726_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27830(__this, method) (( Object_t * (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27727_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m27831(__this, method) (( void (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m27728_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27832(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27729_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27833(__this, method) (( Object_t * (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27730_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27834(__this, method) (( Object_t * (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27731_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m27835(__this, method) (( bool (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_MoveNext_m27732_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m27836(__this, method) (( KeyValuePair_2_t3387  (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_get_Current_m27733_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m27837(__this, method) (( Event_t725 * (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_get_CurrentKey_m27734_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m27838(__this, method) (( int32_t (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_get_CurrentValue_m27735_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Reset()
#define Enumerator_Reset_m27839(__this, method) (( void (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_Reset_m27736_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m27840(__this, method) (( void (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_VerifyState_m27737_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m27841(__this, method) (( void (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_VerifyCurrent_m27738_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m27842(__this, method) (( void (*) (Enumerator_t3390 *, const MethodInfo*))Enumerator_Dispose_m27739_gshared)(__this, method)
