﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB
struct U3CCheckIfAliveU3Ec__IteratorB_t325;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::.ctor()
extern "C" void U3CCheckIfAliveU3Ec__IteratorB__ctor_m1766 (U3CCheckIfAliveU3Ec__IteratorB_t325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCheckIfAliveU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1767 (U3CCheckIfAliveU3Ec__IteratorB_t325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCheckIfAliveU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1768 (U3CCheckIfAliveU3Ec__IteratorB_t325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::MoveNext()
extern "C" bool U3CCheckIfAliveU3Ec__IteratorB_MoveNext_m1769 (U3CCheckIfAliveU3Ec__IteratorB_t325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::Dispose()
extern "C" void U3CCheckIfAliveU3Ec__IteratorB_Dispose_m1770 (U3CCheckIfAliveU3Ec__IteratorB_t325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__IteratorB::Reset()
extern "C" void U3CCheckIfAliveU3Ec__IteratorB_Reset_m1771 (U3CCheckIfAliveU3Ec__IteratorB_t325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
