﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_85.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29046_gshared (InternalEnumerator_1_t3471 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29046(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3471 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29046_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29047_gshared (InternalEnumerator_1_t3471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29047(__this, method) (( void (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29047_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29048_gshared (InternalEnumerator_1_t3471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29048(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29048_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29049_gshared (InternalEnumerator_1_t3471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29049(__this, method) (( void (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29049_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29050_gshared (InternalEnumerator_1_t3471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29050(__this, method) (( bool (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29050_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C" IntPtr_t InternalEnumerator_1_get_Current_m29051_gshared (InternalEnumerator_1_t3471 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29051(__this, method) (( IntPtr_t (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29051_gshared)(__this, method)
