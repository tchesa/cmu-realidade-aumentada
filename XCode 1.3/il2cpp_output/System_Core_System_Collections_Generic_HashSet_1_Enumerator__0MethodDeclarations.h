﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3338;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m27300_gshared (Enumerator_t3342 * __this, HashSet_1_t3338 * ___hashset, const MethodInfo* method);
#define Enumerator__ctor_m27300(__this, ___hashset, method) (( void (*) (Enumerator_t3342 *, HashSet_1_t3338 *, const MethodInfo*))Enumerator__ctor_m27300_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27301_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27301(__this, method) (( Object_t * (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27301_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m27302_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m27302(__this, method) (( void (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m27302_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m27303_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m27303(__this, method) (( bool (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_MoveNext_m27303_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m27304_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m27304(__this, method) (( Object_t * (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_get_Current_m27304_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m27305_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27305(__this, method) (( void (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_Dispose_m27305_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C" void Enumerator_CheckState_m27306_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m27306(__this, method) (( void (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_CheckState_m27306_gshared)(__this, method)
