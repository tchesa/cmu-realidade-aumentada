﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t3557;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerable_1_t3768;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3767;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ICollection_1_t2542;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t2540;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2538;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t3561;
// System.Comparison`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparison_1_t3564;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_71.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void List_1__ctor_m29845_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1__ctor_m29845(__this, method) (( void (*) (List_1_t3557 *, const MethodInfo*))List_1__ctor_m29845_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m29846_gshared (List_1_t3557 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m29846(__this, ___collection, method) (( void (*) (List_1_t3557 *, Object_t*, const MethodInfo*))List_1__ctor_m29846_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m29847_gshared (List_1_t3557 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m29847(__this, ___capacity, method) (( void (*) (List_1_t3557 *, int32_t, const MethodInfo*))List_1__ctor_m29847_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C" void List_1__cctor_m29848_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m29848(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m29848_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29849_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29849(__this, method) (( Object_t* (*) (List_1_t3557 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29849_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m29850_gshared (List_1_t3557 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m29850(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3557 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m29850_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m29851_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m29851(__this, method) (( Object_t * (*) (List_1_t3557 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m29851_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m29852_gshared (List_1_t3557 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m29852(__this, ___item, method) (( int32_t (*) (List_1_t3557 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m29852_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m29853_gshared (List_1_t3557 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m29853(__this, ___item, method) (( bool (*) (List_1_t3557 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m29853_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m29854_gshared (List_1_t3557 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m29854(__this, ___item, method) (( int32_t (*) (List_1_t3557 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m29854_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m29855_gshared (List_1_t3557 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m29855(__this, ___index, ___item, method) (( void (*) (List_1_t3557 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m29855_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m29856_gshared (List_1_t3557 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m29856(__this, ___item, method) (( void (*) (List_1_t3557 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m29856_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29857_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29857(__this, method) (( bool (*) (List_1_t3557 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29857_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m29858_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m29858(__this, method) (( bool (*) (List_1_t3557 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m29858_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m29859_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m29859(__this, method) (( Object_t * (*) (List_1_t3557 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m29859_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m29860_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m29860(__this, method) (( bool (*) (List_1_t3557 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m29860_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m29861_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m29861(__this, method) (( bool (*) (List_1_t3557 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m29861_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m29862_gshared (List_1_t3557 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m29862(__this, ___index, method) (( Object_t * (*) (List_1_t3557 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m29862_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m29863_gshared (List_1_t3557 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m29863(__this, ___index, ___value, method) (( void (*) (List_1_t3557 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m29863_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void List_1_Add_m29864_gshared (List_1_t3557 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define List_1_Add_m29864(__this, ___item, method) (( void (*) (List_1_t3557 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))List_1_Add_m29864_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m29865_gshared (List_1_t3557 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m29865(__this, ___newCount, method) (( void (*) (List_1_t3557 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m29865_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m29866_gshared (List_1_t3557 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m29866(__this, ___collection, method) (( void (*) (List_1_t3557 *, Object_t*, const MethodInfo*))List_1_AddCollection_m29866_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m29867_gshared (List_1_t3557 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m29867(__this, ___enumerable, method) (( void (*) (List_1_t3557 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m29867_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m29868_gshared (List_1_t3557 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m29868(__this, ___collection, method) (( void (*) (List_1_t3557 *, Object_t*, const MethodInfo*))List_1_AddRange_m29868_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2540 * List_1_AsReadOnly_m29869_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m29869(__this, method) (( ReadOnlyCollection_1_t2540 * (*) (List_1_t3557 *, const MethodInfo*))List_1_AsReadOnly_m29869_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void List_1_Clear_m29870_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_Clear_m29870(__this, method) (( void (*) (List_1_t3557 *, const MethodInfo*))List_1_Clear_m29870_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool List_1_Contains_m29871_gshared (List_1_t3557 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define List_1_Contains_m29871(__this, ___item, method) (( bool (*) (List_1_t3557 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))List_1_Contains_m29871_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m29872_gshared (List_1_t3557 * __this, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m29872(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3557 *, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, const MethodInfo*))List_1_CopyTo_m29872_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Find(System.Predicate`1<T>)
extern "C" CustomAttributeNamedArgument_t2100  List_1_Find_m29873_gshared (List_1_t3557 * __this, Predicate_1_t3561 * ___match, const MethodInfo* method);
#define List_1_Find_m29873(__this, ___match, method) (( CustomAttributeNamedArgument_t2100  (*) (List_1_t3557 *, Predicate_1_t3561 *, const MethodInfo*))List_1_Find_m29873_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m29874_gshared (Object_t * __this /* static, unused */, Predicate_1_t3561 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m29874(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3561 *, const MethodInfo*))List_1_CheckMatch_m29874_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m29875_gshared (List_1_t3557 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3561 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m29875(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3557 *, int32_t, int32_t, Predicate_1_t3561 *, const MethodInfo*))List_1_GetIndex_m29875_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Enumerator_t3558  List_1_GetEnumerator_m29876_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m29876(__this, method) (( Enumerator_t3558  (*) (List_1_t3557 *, const MethodInfo*))List_1_GetEnumerator_m29876_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m29877_gshared (List_1_t3557 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define List_1_IndexOf_m29877(__this, ___item, method) (( int32_t (*) (List_1_t3557 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))List_1_IndexOf_m29877_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m29878_gshared (List_1_t3557 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m29878(__this, ___start, ___delta, method) (( void (*) (List_1_t3557 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m29878_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m29879_gshared (List_1_t3557 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m29879(__this, ___index, method) (( void (*) (List_1_t3557 *, int32_t, const MethodInfo*))List_1_CheckIndex_m29879_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m29880_gshared (List_1_t3557 * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define List_1_Insert_m29880(__this, ___index, ___item, method) (( void (*) (List_1_t3557 *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))List_1_Insert_m29880_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m29881_gshared (List_1_t3557 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m29881(__this, ___collection, method) (( void (*) (List_1_t3557 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m29881_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool List_1_Remove_m29882_gshared (List_1_t3557 * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define List_1_Remove_m29882(__this, ___item, method) (( bool (*) (List_1_t3557 *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))List_1_Remove_m29882_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m29883_gshared (List_1_t3557 * __this, Predicate_1_t3561 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m29883(__this, ___match, method) (( int32_t (*) (List_1_t3557 *, Predicate_1_t3561 *, const MethodInfo*))List_1_RemoveAll_m29883_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m29884_gshared (List_1_t3557 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m29884(__this, ___index, method) (( void (*) (List_1_t3557 *, int32_t, const MethodInfo*))List_1_RemoveAt_m29884_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Reverse()
extern "C" void List_1_Reverse_m29885_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_Reverse_m29885(__this, method) (( void (*) (List_1_t3557 *, const MethodInfo*))List_1_Reverse_m29885_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort()
extern "C" void List_1_Sort_m29886_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_Sort_m29886(__this, method) (( void (*) (List_1_t3557 *, const MethodInfo*))List_1_Sort_m29886_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m29887_gshared (List_1_t3557 * __this, Comparison_1_t3564 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m29887(__this, ___comparison, method) (( void (*) (List_1_t3557 *, Comparison_1_t3564 *, const MethodInfo*))List_1_Sort_m29887_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C" CustomAttributeNamedArgumentU5BU5D_t2538* List_1_ToArray_m29888_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_ToArray_m29888(__this, method) (( CustomAttributeNamedArgumentU5BU5D_t2538* (*) (List_1_t3557 *, const MethodInfo*))List_1_ToArray_m29888_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::TrimExcess()
extern "C" void List_1_TrimExcess_m29889_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m29889(__this, method) (( void (*) (List_1_t3557 *, const MethodInfo*))List_1_TrimExcess_m29889_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m29890_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m29890(__this, method) (( int32_t (*) (List_1_t3557 *, const MethodInfo*))List_1_get_Capacity_m29890_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m29891_gshared (List_1_t3557 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m29891(__this, ___value, method) (( void (*) (List_1_t3557 *, int32_t, const MethodInfo*))List_1_set_Capacity_m29891_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m29892_gshared (List_1_t3557 * __this, const MethodInfo* method);
#define List_1_get_Count_m29892(__this, method) (( int32_t (*) (List_1_t3557 *, const MethodInfo*))List_1_get_Count_m29892_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t2100  List_1_get_Item_m29893_gshared (List_1_t3557 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m29893(__this, ___index, method) (( CustomAttributeNamedArgument_t2100  (*) (List_1_t3557 *, int32_t, const MethodInfo*))List_1_get_Item_m29893_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m29894_gshared (List_1_t3557 * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___value, const MethodInfo* method);
#define List_1_set_Item_m29894(__this, ___index, ___value, method) (( void (*) (List_1_t3557 *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))List_1_set_Item_m29894_gshared)(__this, ___index, ___value, method)
