﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterDataPackage
struct GameCenterDataPackage_t123;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "codegen/il2cpp-codegen.h"

// System.Void GameCenterDataPackage::.ctor(System.String,System.String)
extern "C" void GameCenterDataPackage__ctor_m701 (GameCenterDataPackage_t123 * __this, String_t* ___player, String_t* ___receivedData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameCenterDataPackage::get_playerID()
extern "C" String_t* GameCenterDataPackage_get_playerID_m702 (GameCenterDataPackage_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GameCenterDataPackage::get_buffer()
extern "C" ByteU5BU5D_t119* GameCenterDataPackage_get_buffer_m703 (GameCenterDataPackage_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
