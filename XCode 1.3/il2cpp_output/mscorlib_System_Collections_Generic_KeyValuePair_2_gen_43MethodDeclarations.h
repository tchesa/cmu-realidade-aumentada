﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_42MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m26746(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3306 *, String_t*, ProfileData_t1064 , const MethodInfo*))KeyValuePair_2__ctor_m26645_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Key()
#define KeyValuePair_2_get_Key_m26747(__this, method) (( String_t* (*) (KeyValuePair_2_t3306 *, const MethodInfo*))KeyValuePair_2_get_Key_m26646_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26748(__this, ___value, method) (( void (*) (KeyValuePair_2_t3306 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m26647_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Value()
#define KeyValuePair_2_get_Value_m26749(__this, method) (( ProfileData_t1064  (*) (KeyValuePair_2_t3306 *, const MethodInfo*))KeyValuePair_2_get_Value_m26648_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26750(__this, ___value, method) (( void (*) (KeyValuePair_2_t3306 *, ProfileData_t1064 , const MethodInfo*))KeyValuePair_2_set_Value_m26649_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToString()
#define KeyValuePair_2_ToString_m26751(__this, method) (( String_t* (*) (KeyValuePair_2_t3306 *, const MethodInfo*))KeyValuePair_2_ToString_m26650_gshared)(__this, method)
