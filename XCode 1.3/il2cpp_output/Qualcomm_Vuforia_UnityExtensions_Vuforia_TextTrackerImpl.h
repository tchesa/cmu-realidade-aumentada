﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.WordList
struct WordList_t1008;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"

// Vuforia.TextTrackerImpl
struct  TextTrackerImpl_t1007  : public TextTracker_t1005
{
	// Vuforia.WordList Vuforia.TextTrackerImpl::mWordList
	WordList_t1008 * ___mWordList_1;
};
