﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_Security
struct ISN_Security_t157;
// System.String
struct String_t;
// ISN_LocalReceiptResult
struct ISN_LocalReceiptResult_t171;
// ISN_DeviceGUID
struct ISN_DeviceGUID_t170;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_Security::.ctor()
extern "C" void ISN_Security__ctor_m859 (ISN_Security_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::.cctor()
extern "C" void ISN_Security__cctor_m860 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::_ISN_RetrieveLocalReceipt()
extern "C" void ISN_Security__ISN_RetrieveLocalReceipt_m861 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::_ISN_RetrieveDeviceGUID()
extern "C" void ISN_Security__ISN_RetrieveDeviceGUID_m862 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::_ISN_ReceiptRefreshRequest()
extern "C" void ISN_Security__ISN_ReceiptRefreshRequest_m863 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::RetrieveLocalReceipt()
extern "C" void ISN_Security_RetrieveLocalReceipt_m864 (ISN_Security_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::RetrieveDeviceGUID()
extern "C" void ISN_Security_RetrieveDeviceGUID_m865 (ISN_Security_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::StartReceiptRefreshRequest()
extern "C" void ISN_Security_StartReceiptRefreshRequest_m866 (ISN_Security_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::Event_GUIDLoaded(System.String)
extern "C" void ISN_Security_Event_GUIDLoaded_m867 (ISN_Security_t157 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::Event_ReceiptLoaded(System.String)
extern "C" void ISN_Security_Event_ReceiptLoaded_m868 (ISN_Security_t157 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::Event_ReceiptRefreshRequestReceived(System.String)
extern "C" void ISN_Security_Event_ReceiptRefreshRequestReceived_m869 (ISN_Security_t157 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::<OnReceiptLoaded>m__20(ISN_LocalReceiptResult)
extern "C" void ISN_Security_U3COnReceiptLoadedU3Em__20_m870 (Object_t * __this /* static, unused */, ISN_LocalReceiptResult_t171 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::<OnGUIDLoaded>m__21(ISN_DeviceGUID)
extern "C" void ISN_Security_U3COnGUIDLoadedU3Em__21_m871 (Object_t * __this /* static, unused */, ISN_DeviceGUID_t170 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::<OnReceiptRefreshComplete>m__22(ISN_Result)
extern "C" void ISN_Security_U3COnReceiptRefreshCompleteU3Em__22_m872 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
