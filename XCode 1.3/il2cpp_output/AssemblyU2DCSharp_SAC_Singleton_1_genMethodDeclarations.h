﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SAC_Singleton_1_gen_0MethodDeclarations.h"

// System.Void SAC_Singleton`1<SA_ScreenShotMaker>::.ctor()
#define SAC_Singleton_1__ctor_m2346(__this, method) (( void (*) (SAC_Singleton_1_t217 *, const MethodInfo*))SAC_Singleton_1__ctor_m18045_gshared)(__this, method)
// System.Void SAC_Singleton`1<SA_ScreenShotMaker>::.cctor()
#define SAC_Singleton_1__cctor_m18049(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SAC_Singleton_1__cctor_m18046_gshared)(__this /* static, unused */, method)
// T SAC_Singleton`1<SA_ScreenShotMaker>::get_instance()
#define SAC_Singleton_1_get_instance_m18050(__this /* static, unused */, method) (( SA_ScreenShotMaker_t216 * (*) (Object_t * /* static, unused */, const MethodInfo*))SAC_Singleton_1_get_instance_m18047_gshared)(__this /* static, unused */, method)
// System.Boolean SAC_Singleton`1<SA_ScreenShotMaker>::get_IsDestroyed()
#define SAC_Singleton_1_get_IsDestroyed_m18051(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))SAC_Singleton_1_get_IsDestroyed_m18048_gshared)(__this /* static, unused */, method)
