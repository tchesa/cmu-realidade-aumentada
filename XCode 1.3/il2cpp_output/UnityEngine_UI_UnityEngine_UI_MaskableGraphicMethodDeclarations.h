﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t708;
// UnityEngine.Material
struct Material_t45;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.MaskableGraphic::.ctor()
extern "C" void MaskableGraphic__ctor_m3437 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.MaskableGraphic::get_maskable()
extern "C" bool MaskableGraphic_get_maskable_m3438 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::set_maskable(System.Boolean)
extern "C" void MaskableGraphic_set_maskable_m3439 (MaskableGraphic_t708 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.UI.MaskableGraphic::get_material()
extern "C" Material_t45 * MaskableGraphic_get_material_m3440 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::set_material(UnityEngine.Material)
extern "C" void MaskableGraphic_set_material_m3441 (MaskableGraphic_t708 * __this, Material_t45 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::UpdateInternalState()
extern "C" void MaskableGraphic_UpdateInternalState_m3442 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnEnable()
extern "C" void MaskableGraphic_OnEnable_m3443 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnDisable()
extern "C" void MaskableGraphic_OnDisable_m3444 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnTransformParentChanged()
extern "C" void MaskableGraphic_OnTransformParentChanged_m3445 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::ParentMaskStateChanged()
extern "C" void MaskableGraphic_ParentMaskStateChanged_m3446 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::ClearMaskMaterial()
extern "C" void MaskableGraphic_ClearMaskMaterial_m3447 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::SetMaterialDirty()
extern "C" void MaskableGraphic_SetMaterialDirty_m3448 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.MaskableGraphic::GetStencilForGraphic()
extern "C" int32_t MaskableGraphic_GetStencilForGraphic_m3449 (MaskableGraphic_t708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
