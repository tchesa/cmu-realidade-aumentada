﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "UnityEngine_UnityEngine_NetworkReachability.h"

// UnityEngine.NetworkReachability
struct  NetworkReachability_t1415 
{
	// System.Int32 UnityEngine.NetworkReachability::value__
	int32_t ___value___1;
};
