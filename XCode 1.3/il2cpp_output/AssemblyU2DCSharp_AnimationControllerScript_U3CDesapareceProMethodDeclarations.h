﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript/<DesapareceProveta>c__Iterator11
struct U3CDesapareceProvetaU3Ec__Iterator11_t366;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript/<DesapareceProveta>c__Iterator11::.ctor()
extern "C" void U3CDesapareceProvetaU3Ec__Iterator11__ctor_m1852 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<DesapareceProveta>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDesapareceProvetaU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1853 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<DesapareceProveta>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDesapareceProvetaU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m1854 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationControllerScript/<DesapareceProveta>c__Iterator11::MoveNext()
extern "C" bool U3CDesapareceProvetaU3Ec__Iterator11_MoveNext_m1855 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<DesapareceProveta>c__Iterator11::Dispose()
extern "C" void U3CDesapareceProvetaU3Ec__Iterator11_Dispose_m1856 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<DesapareceProveta>c__Iterator11::Reset()
extern "C" void U3CDesapareceProvetaU3Ec__Iterator11_Reset_m1857 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
