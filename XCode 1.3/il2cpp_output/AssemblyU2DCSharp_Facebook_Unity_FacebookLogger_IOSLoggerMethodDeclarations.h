﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FacebookLogger/IOSLogger
struct IOSLogger_t298;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.FacebookLogger/IOSLogger::.ctor()
extern "C" void IOSLogger__ctor_m1656 (IOSLogger_t298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/IOSLogger::Log(System.String)
extern "C" void IOSLogger_Log_m1657 (IOSLogger_t298 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/IOSLogger::Info(System.String)
extern "C" void IOSLogger_Info_m1658 (IOSLogger_t298 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/IOSLogger::Warn(System.String)
extern "C" void IOSLogger_Warn_m1659 (IOSLogger_t298 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/IOSLogger::Error(System.String)
extern "C" void IOSLogger_Error_m1660 (IOSLogger_t298 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
