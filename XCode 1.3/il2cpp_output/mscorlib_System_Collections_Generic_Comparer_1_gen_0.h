﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Comparer`1<System.Byte>
struct Comparer_1_t2717;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Comparer`1<System.Byte>
struct  Comparer_1_t2717  : public Object_t
{
};
struct Comparer_1_t2717_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Byte>::_default
	Comparer_1_t2717 * ____default_0;
};
