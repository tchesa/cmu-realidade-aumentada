﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.Eyewear
struct Eyewear_t914;
// Vuforia.EyewearCalibrationProfileManager
struct EyewearCalibrationProfileManager_t878;
// Vuforia.EyewearUserCalibrator
struct EyewearUserCalibrator_t881;

#include "codegen/il2cpp-codegen.h"

// Vuforia.Eyewear Vuforia.Eyewear::get_Instance()
extern "C" Eyewear_t914 * Eyewear_get_Instance_m4710 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.EyewearCalibrationProfileManager Vuforia.Eyewear::getProfileManager()
extern "C" EyewearCalibrationProfileManager_t878 * Eyewear_getProfileManager_m4711 (Eyewear_t914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.EyewearUserCalibrator Vuforia.Eyewear::getCalibrator()
extern "C" EyewearUserCalibrator_t881 * Eyewear_getCalibrator_m4712 (Eyewear_t914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Eyewear::.ctor()
extern "C" void Eyewear__ctor_m4713 (Eyewear_t914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Eyewear::.cctor()
extern "C" void Eyewear__cctor_m4714 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
