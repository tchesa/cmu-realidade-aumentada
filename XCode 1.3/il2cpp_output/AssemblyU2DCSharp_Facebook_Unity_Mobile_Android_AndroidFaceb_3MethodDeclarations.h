﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_AndroidFaceb_10MethodDeclarations.h"

// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<Facebook.Unity.ILoginResult>::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
#define JavaMethodCall_1__ctor_m2424(__this, ___androidImpl, ___methodName, method) (( void (*) (JavaMethodCall_1_t537 *, AndroidFacebook_t249 *, String_t*, const MethodInfo*))JavaMethodCall_1__ctor_m18448_gshared)(__this, ___androidImpl, ___methodName, method)
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<Facebook.Unity.ILoginResult>::Call(Facebook.Unity.MethodArguments)
#define JavaMethodCall_1_Call_m18460(__this, ___args, method) (( void (*) (JavaMethodCall_1_t537 *, MethodArguments_t248 *, const MethodInfo*))JavaMethodCall_1_Call_m18450_gshared)(__this, ___args, method)
