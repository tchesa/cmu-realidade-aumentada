﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BezierTraveler
struct BezierTraveler_t342;

#include "mscorlib_System_Object.h"

// ARAnimation/TrailTrigger
struct  TrailTrigger_t341  : public Object_t
{
	// System.Single ARAnimation/TrailTrigger::turnOn
	float ___turnOn_0;
	// BezierTraveler ARAnimation/TrailTrigger::obj
	BezierTraveler_t342 * ___obj_1;
};
