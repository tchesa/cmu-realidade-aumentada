﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m21377(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2965 *, Canvas_t690 *, IndexedSet_1_t852 *, const MethodInfo*))KeyValuePair_2__ctor_m16119_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Key()
#define KeyValuePair_2_get_Key_m21378(__this, method) (( Canvas_t690 * (*) (KeyValuePair_2_t2965 *, const MethodInfo*))KeyValuePair_2_get_Key_m16120_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21379(__this, ___value, method) (( void (*) (KeyValuePair_2_t2965 *, Canvas_t690 *, const MethodInfo*))KeyValuePair_2_set_Key_m16121_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Value()
#define KeyValuePair_2_get_Value_m21380(__this, method) (( IndexedSet_1_t852 * (*) (KeyValuePair_2_t2965 *, const MethodInfo*))KeyValuePair_2_get_Value_m16122_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21381(__this, ___value, method) (( void (*) (KeyValuePair_2_t2965 *, IndexedSet_1_t852 *, const MethodInfo*))KeyValuePair_2_set_Value_m16123_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::ToString()
#define KeyValuePair_2_ToString_m21382(__this, method) (( String_t* (*) (KeyValuePair_2_t2965 *, const MethodInfo*))KeyValuePair_2_ToString_m16124_gshared)(__this, method)
