﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_0.h"
#include "AssemblyU2DCSharp_Facebook_Unity_OGActionType.h"

// System.Void System.Nullable`1<Facebook.Unity.OGActionType>::.ctor(T)
extern "C" void Nullable_1__ctor_m2401_gshared (Nullable_1_t466 * __this, int32_t ___value, const MethodInfo* method);
#define Nullable_1__ctor_m2401(__this, ___value, method) (( void (*) (Nullable_1_t466 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2401_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m2373_gshared (Nullable_1_t466 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2373(__this, method) (( bool (*) (Nullable_1_t466 *, const MethodInfo*))Nullable_1_get_HasValue_m2373_gshared)(__this, method)
// T System.Nullable`1<Facebook.Unity.OGActionType>::get_Value()
extern "C" int32_t Nullable_1_get_Value_m18247_gshared (Nullable_1_t466 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m18247(__this, method) (( int32_t (*) (Nullable_1_t466 *, const MethodInfo*))Nullable_1_get_Value_m18247_gshared)(__this, method)
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m18248_gshared (Nullable_1_t466 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18248(__this, ___other, method) (( bool (*) (Nullable_1_t466 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m18248_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m18249_gshared (Nullable_1_t466 * __this, Nullable_1_t466  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18249(__this, ___other, method) (( bool (*) (Nullable_1_t466 *, Nullable_1_t466 , const MethodInfo*))Nullable_1_Equals_m18249_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<Facebook.Unity.OGActionType>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m18250_gshared (Nullable_1_t466 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m18250(__this, method) (( int32_t (*) (Nullable_1_t466 *, const MethodInfo*))Nullable_1_GetHashCode_m18250_gshared)(__this, method)
// T System.Nullable`1<Facebook.Unity.OGActionType>::GetValueOrDefault()
extern "C" int32_t Nullable_1_GetValueOrDefault_m2408_gshared (Nullable_1_t466 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2408(__this, method) (( int32_t (*) (Nullable_1_t466 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2408_gshared)(__this, method)
// System.String System.Nullable`1<Facebook.Unity.OGActionType>::ToString()
extern "C" String_t* Nullable_1_ToString_m2374_gshared (Nullable_1_t466 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2374(__this, method) (( String_t* (*) (Nullable_1_t466 *, const MethodInfo*))Nullable_1_ToString_m2374_gshared)(__this, method)
