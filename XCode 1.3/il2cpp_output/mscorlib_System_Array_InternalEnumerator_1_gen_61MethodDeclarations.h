﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25964_gshared (InternalEnumerator_1_t3241 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m25964(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3241 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m25964_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m25965_gshared (InternalEnumerator_1_t3241 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m25965(__this, method) (( void (*) (InternalEnumerator_1_t3241 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m25965_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25966_gshared (InternalEnumerator_1_t3241 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25966(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3241 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25966_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25967_gshared (InternalEnumerator_1_t3241 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25967(__this, method) (( void (*) (InternalEnumerator_1_t3241 *, const MethodInfo*))InternalEnumerator_1_Dispose_m25967_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25968_gshared (InternalEnumerator_1_t3241 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25968(__this, method) (( bool (*) (InternalEnumerator_1_t3241 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m25968_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::get_Current()
extern "C" KeyValuePair_2_t3240  InternalEnumerator_1_get_Current_m25969_gshared (InternalEnumerator_1_t3241 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25969(__this, method) (( KeyValuePair_2_t3240  (*) (InternalEnumerator_1_t3241 *, const MethodInfo*))InternalEnumerator_1_get_Current_m25969_gshared)(__this, method)
