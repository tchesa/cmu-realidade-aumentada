﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t119;

#include "mscorlib_System_Object.h"

// ISN_LocalReceiptResult
struct  ISN_LocalReceiptResult_t171  : public Object_t
{
	// System.Byte[] ISN_LocalReceiptResult::_Receipt
	ByteU5BU5D_t119* ____Receipt_0;
};
