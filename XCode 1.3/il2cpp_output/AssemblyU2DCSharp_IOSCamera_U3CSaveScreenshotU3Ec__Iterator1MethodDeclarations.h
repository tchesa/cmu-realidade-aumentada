﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSCamera/<SaveScreenshot>c__Iterator1
struct U3CSaveScreenshotU3Ec__Iterator1_t144;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSCamera/<SaveScreenshot>c__Iterator1::.ctor()
extern "C" void U3CSaveScreenshotU3Ec__Iterator1__ctor_m814 (U3CSaveScreenshotU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSCamera/<SaveScreenshot>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSaveScreenshotU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m815 (U3CSaveScreenshotU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSCamera/<SaveScreenshot>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSaveScreenshotU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m816 (U3CSaveScreenshotU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSCamera/<SaveScreenshot>c__Iterator1::MoveNext()
extern "C" bool U3CSaveScreenshotU3Ec__Iterator1_MoveNext_m817 (U3CSaveScreenshotU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera/<SaveScreenshot>c__Iterator1::Dispose()
extern "C" void U3CSaveScreenshotU3Ec__Iterator1_Dispose_m818 (U3CSaveScreenshotU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera/<SaveScreenshot>c__Iterator1::Reset()
extern "C" void U3CSaveScreenshotU3Ec__Iterator1_Reset_m819 (U3CSaveScreenshotU3Ec__Iterator1_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
