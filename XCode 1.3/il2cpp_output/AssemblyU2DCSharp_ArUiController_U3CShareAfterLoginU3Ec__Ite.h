﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// ArUiController
struct ArUiController_t374;

#include "mscorlib_System_Object.h"

// ArUiController/<ShareAfterLogin>c__Iterator16
struct  U3CShareAfterLoginU3Ec__Iterator16_t373  : public Object_t
{
	// System.Int32 ArUiController/<ShareAfterLogin>c__Iterator16::$PC
	int32_t ___U24PC_0;
	// System.Object ArUiController/<ShareAfterLogin>c__Iterator16::$current
	Object_t * ___U24current_1;
	// ArUiController ArUiController/<ShareAfterLogin>c__Iterator16::<>f__this
	ArUiController_t374 * ___U3CU3Ef__this_2;
};
