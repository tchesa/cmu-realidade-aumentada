﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSBillingInitChecker/BillingInitListener
struct BillingInitListener_t125;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void IOSBillingInitChecker/BillingInitListener::.ctor(System.Object,System.IntPtr)
extern "C" void BillingInitListener__ctor_m713 (BillingInitListener_t125 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSBillingInitChecker/BillingInitListener::Invoke()
extern "C" void BillingInitListener_Invoke_m714 (BillingInitListener_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_BillingInitListener_t125(Il2CppObject* delegate);
// System.IAsyncResult IOSBillingInitChecker/BillingInitListener::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * BillingInitListener_BeginInvoke_m715 (BillingInitListener_t125 * __this, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSBillingInitChecker/BillingInitListener::EndInvoke(System.IAsyncResult)
extern "C" void BillingInitListener_EndInvoke_m716 (BillingInitListener_t125 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
