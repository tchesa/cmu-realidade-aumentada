﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m16755(__this, ___dictionary, method) (( void (*) (Enumerator_t2684 *, Dictionary_2_t71 *, const MethodInfo*))Enumerator__ctor_m16419_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16756(__this, method) (( Object_t * (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16420_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16757(__this, method) (( void (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16421_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16758(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16422_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16759(__this, method) (( Object_t * (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16423_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16760(__this, method) (( Object_t * (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16424_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::MoveNext()
#define Enumerator_MoveNext_m16761(__this, method) (( bool (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_MoveNext_m16425_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::get_Current()
#define Enumerator_get_Current_m16762(__this, method) (( KeyValuePair_2_t2681  (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_get_Current_m16426_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16763(__this, method) (( int32_t (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_get_CurrentKey_m16427_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16764(__this, method) (( List_1_t461 * (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_get_CurrentValue_m16428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::Reset()
#define Enumerator_Reset_m16765(__this, method) (( void (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_Reset_m16429_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::VerifyState()
#define Enumerator_VerifyState_m16766(__this, method) (( void (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_VerifyState_m16430_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16767(__this, method) (( void (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_VerifyCurrent_m16431_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::Dispose()
#define Enumerator_Dispose_m16768(__this, method) (( void (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_Dispose_m16432_gshared)(__this, method)
