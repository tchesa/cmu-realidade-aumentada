﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,GCLeaderboard>
struct Dictionary_2_t105;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>
struct  KeyCollection_t2705  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::dictionary
	Dictionary_2_t105 * ___dictionary_0;
};
