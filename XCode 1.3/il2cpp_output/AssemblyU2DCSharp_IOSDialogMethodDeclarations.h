﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSDialog
struct IOSDialog_t163;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_IOSDialogResult.h"

// System.Void IOSDialog::.ctor()
extern "C" void IOSDialog__ctor_m878 (IOSDialog_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSDialog IOSDialog::Create(System.String,System.String)
extern "C" IOSDialog_t163 * IOSDialog_Create_m879 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSDialog IOSDialog::Create(System.String,System.String,System.String,System.String)
extern "C" IOSDialog_t163 * IOSDialog_Create_m880 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDialog::init()
extern "C" void IOSDialog_init_m881 (IOSDialog_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDialog::onPopUpCallBack(System.String)
extern "C" void IOSDialog_onPopUpCallBack_m882 (IOSDialog_t163 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDialog::<OnComplete>m__23(IOSDialogResult)
extern "C" void IOSDialog_U3COnCompleteU3Em__23_m883 (Object_t * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
