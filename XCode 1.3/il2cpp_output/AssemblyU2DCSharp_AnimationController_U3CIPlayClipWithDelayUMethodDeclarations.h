﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationController/<IPlayClipWithDelay>c__IteratorE
struct U3CIPlayClipWithDelayU3Ec__IteratorE_t358;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationController/<IPlayClipWithDelay>c__IteratorE::.ctor()
extern "C" void U3CIPlayClipWithDelayU3Ec__IteratorE__ctor_m1824 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationController/<IPlayClipWithDelay>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CIPlayClipWithDelayU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1825 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationController/<IPlayClipWithDelay>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CIPlayClipWithDelayU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m1826 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationController/<IPlayClipWithDelay>c__IteratorE::MoveNext()
extern "C" bool U3CIPlayClipWithDelayU3Ec__IteratorE_MoveNext_m1827 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController/<IPlayClipWithDelay>c__IteratorE::Dispose()
extern "C" void U3CIPlayClipWithDelayU3Ec__IteratorE_Dispose_m1828 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController/<IPlayClipWithDelay>c__IteratorE::Reset()
extern "C" void U3CIPlayClipWithDelayU3Ec__IteratorE_Reset_m1829 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
