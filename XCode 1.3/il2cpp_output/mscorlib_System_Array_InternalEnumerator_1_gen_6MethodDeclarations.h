﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_6.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15722_gshared (InternalEnumerator_1_t2598 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15722(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2598 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15722_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15723_gshared (InternalEnumerator_1_t2598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15723(__this, method) (( void (*) (InternalEnumerator_1_t2598 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15723_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15724_gshared (InternalEnumerator_1_t2598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15724(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2598 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15724_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15725_gshared (InternalEnumerator_1_t2598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15725(__this, method) (( void (*) (InternalEnumerator_1_t2598 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15725_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15726_gshared (InternalEnumerator_1_t2598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15726(__this, method) (( bool (*) (InternalEnumerator_1_t2598 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15726_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern "C" Color_t5  InternalEnumerator_1_get_Current_m15727_gshared (InternalEnumerator_1_t2598 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15727(__this, method) (( Color_t5  (*) (InternalEnumerator_1_t2598 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15727_gshared)(__this, method)
