﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t3371;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__45.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m27726_gshared (Enumerator_t3378 * __this, Dictionary_2_t3371 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m27726(__this, ___dictionary, method) (( void (*) (Enumerator_t3378 *, Dictionary_2_t3371 *, const MethodInfo*))Enumerator__ctor_m27726_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27727_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27727(__this, method) (( Object_t * (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27727_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m27728_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m27728(__this, method) (( void (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m27728_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t58  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27729_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27729(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27729_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27730_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27730(__this, method) (( Object_t * (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27730_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27731_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27731(__this, method) (( Object_t * (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27731_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m27732_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m27732(__this, method) (( bool (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_MoveNext_m27732_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" KeyValuePair_2_t3373  Enumerator_get_Current_m27733_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m27733(__this, method) (( KeyValuePair_2_t3373  (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_get_Current_m27733_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m27734_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m27734(__this, method) (( Object_t * (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_get_CurrentKey_m27734_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m27735_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m27735(__this, method) (( int32_t (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_get_CurrentValue_m27735_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void Enumerator_Reset_m27736_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_Reset_m27736(__this, method) (( void (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_Reset_m27736_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern "C" void Enumerator_VerifyState_m27737_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m27737(__this, method) (( void (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_VerifyState_m27737_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m27738_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m27738(__this, method) (( void (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_VerifyCurrent_m27738_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m27739_gshared (Enumerator_t3378 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27739(__this, method) (( void (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_Dispose_m27739_gshared)(__this, method)
