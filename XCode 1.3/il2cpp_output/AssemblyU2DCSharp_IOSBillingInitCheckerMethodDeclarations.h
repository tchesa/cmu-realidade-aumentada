﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSBillingInitChecker
struct IOSBillingInitChecker_t126;
// IOSBillingInitChecker/BillingInitListener
struct BillingInitListener_t125;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSBillingInitChecker::.ctor(IOSBillingInitChecker/BillingInitListener)
extern "C" void IOSBillingInitChecker__ctor_m717 (IOSBillingInitChecker_t126 * __this, BillingInitListener_t125 * ___listener, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSBillingInitChecker::OnStoreKitInit()
extern "C" void IOSBillingInitChecker_OnStoreKitInit_m718 (IOSBillingInitChecker_t126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
