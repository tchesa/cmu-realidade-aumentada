﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodCall_1_gen_7MethodDeclarations.h"

// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::.ctor(Facebook.Unity.FacebookBase,System.String)
#define MethodCall_1__ctor_m18331(__this, ___facebookImpl, ___methodName, method) (( void (*) (MethodCall_1_t2769 *, FacebookBase_t227 *, String_t*, const MethodInfo*))MethodCall_1__ctor_m18266_gshared)(__this, ___facebookImpl, ___methodName, method)
// System.String Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::get_MethodName()
#define MethodCall_1_get_MethodName_m18332(__this, method) (( String_t* (*) (MethodCall_1_t2769 *, const MethodInfo*))MethodCall_1_get_MethodName_m18267_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::set_MethodName(System.String)
#define MethodCall_1_set_MethodName_m18333(__this, ___value, method) (( void (*) (MethodCall_1_t2769 *, String_t*, const MethodInfo*))MethodCall_1_set_MethodName_m18268_gshared)(__this, ___value, method)
// Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::get_Callback()
#define MethodCall_1_get_Callback_m18334(__this, method) (( FacebookDelegate_1_t472 * (*) (MethodCall_1_t2769 *, const MethodInfo*))MethodCall_1_get_Callback_m18269_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
#define MethodCall_1_set_Callback_m2388(__this, ___value, method) (( void (*) (MethodCall_1_t2769 *, FacebookDelegate_1_t472 *, const MethodInfo*))MethodCall_1_set_Callback_m18270_gshared)(__this, ___value, method)
// Facebook.Unity.FacebookBase Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::get_FacebookImpl()
#define MethodCall_1_get_FacebookImpl_m18335(__this, method) (( FacebookBase_t227 * (*) (MethodCall_1_t2769 *, const MethodInfo*))MethodCall_1_get_FacebookImpl_m18271_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::set_FacebookImpl(Facebook.Unity.FacebookBase)
#define MethodCall_1_set_FacebookImpl_m18336(__this, ___value, method) (( void (*) (MethodCall_1_t2769 *, FacebookBase_t227 *, const MethodInfo*))MethodCall_1_set_FacebookImpl_m18272_gshared)(__this, ___value, method)
// Facebook.Unity.MethodArguments Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::get_Parameters()
#define MethodCall_1_get_Parameters_m18337(__this, method) (( MethodArguments_t248 * (*) (MethodCall_1_t2769 *, const MethodInfo*))MethodCall_1_get_Parameters_m18273_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IGroupJoinResult>::set_Parameters(Facebook.Unity.MethodArguments)
#define MethodCall_1_set_Parameters_m18338(__this, ___value, method) (( void (*) (MethodCall_1_t2769 *, MethodArguments_t248 *, const MethodInfo*))MethodCall_1_set_Parameters_m18274_gshared)(__this, ___value, method)
