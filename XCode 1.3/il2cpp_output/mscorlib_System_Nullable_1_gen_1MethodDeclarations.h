﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_1.h"

// System.Void System.Nullable`1<System.Int32>::.ctor(T)
extern "C" void Nullable_1__ctor_m18251_gshared (Nullable_1_t467 * __this, int32_t ___value, const MethodInfo* method);
#define Nullable_1__ctor_m18251(__this, ___value, method) (( void (*) (Nullable_1_t467 *, int32_t, const MethodInfo*))Nullable_1__ctor_m18251_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m2443_gshared (Nullable_1_t467 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2443(__this, method) (( bool (*) (Nullable_1_t467 *, const MethodInfo*))Nullable_1_get_HasValue_m2443_gshared)(__this, method)
// T System.Nullable`1<System.Int32>::get_Value()
extern "C" int32_t Nullable_1_get_Value_m2444_gshared (Nullable_1_t467 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2444(__this, method) (( int32_t (*) (Nullable_1_t467 *, const MethodInfo*))Nullable_1_get_Value_m2444_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m18252_gshared (Nullable_1_t467 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18252(__this, ___other, method) (( bool (*) (Nullable_1_t467 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m18252_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m18253_gshared (Nullable_1_t467 * __this, Nullable_1_t467  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18253(__this, ___other, method) (( bool (*) (Nullable_1_t467 *, Nullable_1_t467 , const MethodInfo*))Nullable_1_Equals_m18253_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.Int32>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m18254_gshared (Nullable_1_t467 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m18254(__this, method) (( int32_t (*) (Nullable_1_t467 *, const MethodInfo*))Nullable_1_GetHashCode_m18254_gshared)(__this, method)
// T System.Nullable`1<System.Int32>::GetValueOrDefault()
extern "C" int32_t Nullable_1_GetValueOrDefault_m18255_gshared (Nullable_1_t467 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m18255(__this, method) (( int32_t (*) (Nullable_1_t467 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m18255_gshared)(__this, method)
// System.String System.Nullable`1<System.Int32>::ToString()
extern "C" String_t* Nullable_1_ToString_m18256_gshared (Nullable_1_t467 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m18256(__this, method) (( String_t* (*) (Nullable_1_t467 *, const MethodInfo*))Nullable_1_ToString_m18256_gshared)(__this, method)
