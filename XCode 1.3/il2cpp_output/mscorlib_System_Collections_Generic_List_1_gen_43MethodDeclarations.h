﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor()
#define List_1__ctor_m6610(__this, method) (( void (*) (List_1_t1001 *, const MethodInfo*))List_1__ctor_m2529_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m24045(__this, ___collection, method) (( void (*) (List_1_t1001 *, Object_t*, const MethodInfo*))List_1__ctor_m15449_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m24046(__this, ___capacity, method) (( void (*) (List_1_t1001 *, int32_t, const MethodInfo*))List_1__ctor_m15451_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.cctor()
#define List_1__cctor_m24047(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15453_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24048(__this, method) (( Object_t* (*) (List_1_t1001 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m24049(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1001 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15457_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m24050(__this, method) (( Object_t * (*) (List_1_t1001 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15459_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m24051(__this, ___item, method) (( int32_t (*) (List_1_t1001 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m15461_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m24052(__this, ___item, method) (( bool (*) (List_1_t1001 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m15463_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m24053(__this, ___item, method) (( int32_t (*) (List_1_t1001 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m15465_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m24054(__this, ___index, ___item, method) (( void (*) (List_1_t1001 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m15467_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m24055(__this, ___item, method) (( void (*) (List_1_t1001 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m15469_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24056(__this, method) (( bool (*) (List_1_t1001 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15471_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m24057(__this, method) (( bool (*) (List_1_t1001 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15473_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m24058(__this, method) (( Object_t * (*) (List_1_t1001 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m24059(__this, method) (( bool (*) (List_1_t1001 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15477_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m24060(__this, method) (( bool (*) (List_1_t1001 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15479_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m24061(__this, ___index, method) (( Object_t * (*) (List_1_t1001 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m15481_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m24062(__this, ___index, ___value, method) (( void (*) (List_1_t1001 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m15483_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Add(T)
#define List_1_Add_m24063(__this, ___item, method) (( void (*) (List_1_t1001 *, ReconstructionAbstractBehaviour_t431 *, const MethodInfo*))List_1_Add_m15485_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m24064(__this, ___newCount, method) (( void (*) (List_1_t1001 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15487_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m24065(__this, ___collection, method) (( void (*) (List_1_t1001 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15489_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m24066(__this, ___enumerable, method) (( void (*) (List_1_t1001 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15491_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m24067(__this, ___collection, method) (( void (*) (List_1_t1001 *, Object_t*, const MethodInfo*))List_1_AddRange_m15493_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m24068(__this, method) (( ReadOnlyCollection_1_t3142 * (*) (List_1_t1001 *, const MethodInfo*))List_1_AsReadOnly_m15495_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Clear()
#define List_1_Clear_m24069(__this, method) (( void (*) (List_1_t1001 *, const MethodInfo*))List_1_Clear_m15497_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Contains(T)
#define List_1_Contains_m24070(__this, ___item, method) (( bool (*) (List_1_t1001 *, ReconstructionAbstractBehaviour_t431 *, const MethodInfo*))List_1_Contains_m15499_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m24071(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1001 *, ReconstructionAbstractBehaviourU5BU5D_t1158*, int32_t, const MethodInfo*))List_1_CopyTo_m15501_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m24072(__this, ___match, method) (( ReconstructionAbstractBehaviour_t431 * (*) (List_1_t1001 *, Predicate_1_t3144 *, const MethodInfo*))List_1_Find_m15503_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m24073(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3144 *, const MethodInfo*))List_1_CheckMatch_m15505_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m24074(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1001 *, int32_t, int32_t, Predicate_1_t3144 *, const MethodInfo*))List_1_GetIndex_m15507_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator()
#define List_1_GetEnumerator_m6606(__this, method) (( Enumerator_t1160  (*) (List_1_t1001 *, const MethodInfo*))List_1_GetEnumerator_m2487_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::IndexOf(T)
#define List_1_IndexOf_m24075(__this, ___item, method) (( int32_t (*) (List_1_t1001 *, ReconstructionAbstractBehaviour_t431 *, const MethodInfo*))List_1_IndexOf_m15509_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m24076(__this, ___start, ___delta, method) (( void (*) (List_1_t1001 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15511_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m24077(__this, ___index, method) (( void (*) (List_1_t1001 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15513_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m24078(__this, ___index, ___item, method) (( void (*) (List_1_t1001 *, int32_t, ReconstructionAbstractBehaviour_t431 *, const MethodInfo*))List_1_Insert_m15515_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m24079(__this, ___collection, method) (( void (*) (List_1_t1001 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15517_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Remove(T)
#define List_1_Remove_m24080(__this, ___item, method) (( bool (*) (List_1_t1001 *, ReconstructionAbstractBehaviour_t431 *, const MethodInfo*))List_1_Remove_m15519_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m24081(__this, ___match, method) (( int32_t (*) (List_1_t1001 *, Predicate_1_t3144 *, const MethodInfo*))List_1_RemoveAll_m15521_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m24082(__this, ___index, method) (( void (*) (List_1_t1001 *, int32_t, const MethodInfo*))List_1_RemoveAt_m15523_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Reverse()
#define List_1_Reverse_m24083(__this, method) (( void (*) (List_1_t1001 *, const MethodInfo*))List_1_Reverse_m15525_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Sort()
#define List_1_Sort_m24084(__this, method) (( void (*) (List_1_t1001 *, const MethodInfo*))List_1_Sort_m15527_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m24085(__this, ___comparison, method) (( void (*) (List_1_t1001 *, Comparison_1_t3145 *, const MethodInfo*))List_1_Sort_m15529_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::ToArray()
#define List_1_ToArray_m6604(__this, method) (( ReconstructionAbstractBehaviourU5BU5D_t1158* (*) (List_1_t1001 *, const MethodInfo*))List_1_ToArray_m15531_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::TrimExcess()
#define List_1_TrimExcess_m24086(__this, method) (( void (*) (List_1_t1001 *, const MethodInfo*))List_1_TrimExcess_m15533_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Capacity()
#define List_1_get_Capacity_m24087(__this, method) (( int32_t (*) (List_1_t1001 *, const MethodInfo*))List_1_get_Capacity_m15535_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m24088(__this, ___value, method) (( void (*) (List_1_t1001 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15537_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Count()
#define List_1_get_Count_m24089(__this, method) (( int32_t (*) (List_1_t1001 *, const MethodInfo*))List_1_get_Count_m15539_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m24090(__this, ___index, method) (( ReconstructionAbstractBehaviour_t431 * (*) (List_1_t1001 *, int32_t, const MethodInfo*))List_1_get_Item_m15541_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m24091(__this, ___index, ___value, method) (( void (*) (List_1_t1001 *, int32_t, ReconstructionAbstractBehaviour_t431 *, const MethodInfo*))List_1_set_Item_m15543_gshared)(__this, ___index, ___value, method)
