﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIStyle
struct GUIStyle_t184;
// iAdBanner
struct iAdBanner_t172;
// UnityEngine.GameObject
struct GameObject_t27;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// iAdUseExample
struct  iAdUseExample_t214  : public MonoBehaviour_t18
{
	// UnityEngine.GUIStyle iAdUseExample::style
	GUIStyle_t184 * ___style_1;
	// UnityEngine.GUIStyle iAdUseExample::style2
	GUIStyle_t184 * ___style2_2;
	// iAdBanner iAdUseExample::banner1
	iAdBanner_t172 * ___banner1_3;
	// iAdBanner iAdUseExample::banner2
	iAdBanner_t172 * ___banner2_4;
	// System.Boolean iAdUseExample::IsInterstisialsAdReady
	bool ___IsInterstisialsAdReady_5;
	// UnityEngine.GameObject iAdUseExample::Quad
	GameObject_t27 * ___Quad_6;
};
