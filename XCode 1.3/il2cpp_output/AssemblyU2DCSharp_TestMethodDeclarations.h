﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Test
struct Test_t392;
// System.Action`1<System.Int32>
struct Action_1_t490;

#include "codegen/il2cpp-codegen.h"

// System.Void Test::.ctor()
extern "C" void Test__ctor_m2003 (Test_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Test::Start()
extern "C" void Test_Start_m2004 (Test_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Test::Request(System.Int32,System.Action`1<System.Int32>)
extern "C" void Test_Request_m2005 (Test_t392 * __this, int32_t ___param, Action_1_t490 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Test::Callback(System.Int32)
extern "C" void Test_Callback_m2006 (Test_t392 * __this, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
