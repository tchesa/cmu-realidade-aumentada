﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_35MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m25601(__this, ___host, method) (( void (*) (Enumerator_t1182 *, Dictionary_2_t1039 *, const MethodInfo*))Enumerator__ctor_m16451_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25602(__this, method) (( Object_t * (*) (Enumerator_t1182 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16452_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m25603(__this, method) (( void (*) (Enumerator_t1182 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16453_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m6688(__this, method) (( void (*) (Enumerator_t1182 *, const MethodInfo*))Enumerator_Dispose_m16454_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m6687(__this, method) (( bool (*) (Enumerator_t1182 *, const MethodInfo*))Enumerator_MoveNext_m16455_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m6686(__this, method) (( PropAbstractBehaviour_t430 * (*) (Enumerator_t1182 *, const MethodInfo*))Enumerator_get_Current_m16456_gshared)(__this, method)
