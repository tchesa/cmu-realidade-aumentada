﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_GCCollectionType.h"

// GCCollectionType
struct  GCCollectionType_t87 
{
	// System.Int32 GCCollectionType::value__
	int32_t ___value___1;
};
