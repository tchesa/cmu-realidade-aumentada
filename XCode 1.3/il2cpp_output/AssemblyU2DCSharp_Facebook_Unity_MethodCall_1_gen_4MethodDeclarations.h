﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodCall_1_gen_7MethodDeclarations.h"

// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::.ctor(Facebook.Unity.FacebookBase,System.String)
#define MethodCall_1__ctor_m18461(__this, ___facebookImpl, ___methodName, method) (( void (*) (MethodCall_1_t2779 *, FacebookBase_t227 *, String_t*, const MethodInfo*))MethodCall_1__ctor_m18266_gshared)(__this, ___facebookImpl, ___methodName, method)
// System.String Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::get_MethodName()
#define MethodCall_1_get_MethodName_m18462(__this, method) (( String_t* (*) (MethodCall_1_t2779 *, const MethodInfo*))MethodCall_1_get_MethodName_m18267_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::set_MethodName(System.String)
#define MethodCall_1_set_MethodName_m18463(__this, ___value, method) (( void (*) (MethodCall_1_t2779 *, String_t*, const MethodInfo*))MethodCall_1_set_MethodName_m18268_gshared)(__this, ___value, method)
// Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::get_Callback()
#define MethodCall_1_get_Callback_m18464(__this, method) (( FacebookDelegate_1_t465 * (*) (MethodCall_1_t2779 *, const MethodInfo*))MethodCall_1_get_Callback_m18269_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
#define MethodCall_1_set_Callback_m2425(__this, ___value, method) (( void (*) (MethodCall_1_t2779 *, FacebookDelegate_1_t465 *, const MethodInfo*))MethodCall_1_set_Callback_m18270_gshared)(__this, ___value, method)
// Facebook.Unity.FacebookBase Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::get_FacebookImpl()
#define MethodCall_1_get_FacebookImpl_m18465(__this, method) (( FacebookBase_t227 * (*) (MethodCall_1_t2779 *, const MethodInfo*))MethodCall_1_get_FacebookImpl_m18271_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::set_FacebookImpl(Facebook.Unity.FacebookBase)
#define MethodCall_1_set_FacebookImpl_m18466(__this, ___value, method) (( void (*) (MethodCall_1_t2779 *, FacebookBase_t227 *, const MethodInfo*))MethodCall_1_set_FacebookImpl_m18272_gshared)(__this, ___value, method)
// Facebook.Unity.MethodArguments Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::get_Parameters()
#define MethodCall_1_get_Parameters_m18467(__this, method) (( MethodArguments_t248 * (*) (MethodCall_1_t2779 *, const MethodInfo*))MethodCall_1_get_Parameters_m18273_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.ILoginResult>::set_Parameters(Facebook.Unity.MethodArguments)
#define MethodCall_1_set_Parameters_m18468(__this, ___value, method) (( void (*) (MethodCall_1_t2779 *, MethodArguments_t248 *, const MethodInfo*))MethodCall_1_set_Parameters_m18274_gshared)(__this, ___value, method)
