﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1289;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
struct IEnumerable_1_t3752;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3753;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
struct ICollection_1_t861;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t3401;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1441;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t3405;
// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t3408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_65.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m27987_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1__ctor_m27987(__this, method) (( void (*) (List_1_t1289 *, const MethodInfo*))List_1__ctor_m27987_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m27988_gshared (List_1_t1289 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m27988(__this, ___collection, method) (( void (*) (List_1_t1289 *, Object_t*, const MethodInfo*))List_1__ctor_m27988_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m8143_gshared (List_1_t1289 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m8143(__this, ___capacity, method) (( void (*) (List_1_t1289 *, int32_t, const MethodInfo*))List_1__ctor_m8143_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m27989_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m27989(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m27989_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27990_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27990(__this, method) (( Object_t* (*) (List_1_t1289 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27990_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m27991_gshared (List_1_t1289 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m27991(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1289 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m27991_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m27992_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m27992(__this, method) (( Object_t * (*) (List_1_t1289 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m27992_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m27993_gshared (List_1_t1289 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m27993(__this, ___item, method) (( int32_t (*) (List_1_t1289 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m27993_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m27994_gshared (List_1_t1289 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m27994(__this, ___item, method) (( bool (*) (List_1_t1289 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m27994_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m27995_gshared (List_1_t1289 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m27995(__this, ___item, method) (( int32_t (*) (List_1_t1289 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m27995_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m27996_gshared (List_1_t1289 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m27996(__this, ___index, ___item, method) (( void (*) (List_1_t1289 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m27996_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m27997_gshared (List_1_t1289 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m27997(__this, ___item, method) (( void (*) (List_1_t1289 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m27997_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27998_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27998(__this, method) (( bool (*) (List_1_t1289 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27998_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m27999_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m27999(__this, method) (( bool (*) (List_1_t1289 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m27999_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m28000_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m28000(__this, method) (( Object_t * (*) (List_1_t1289 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m28000_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m28001_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m28001(__this, method) (( bool (*) (List_1_t1289 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m28001_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m28002_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m28002(__this, method) (( bool (*) (List_1_t1289 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m28002_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m28003_gshared (List_1_t1289 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m28003(__this, ___index, method) (( Object_t * (*) (List_1_t1289 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m28003_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m28004_gshared (List_1_t1289 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m28004(__this, ___index, ___value, method) (( void (*) (List_1_t1289 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m28004_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m28005_gshared (List_1_t1289 * __this, UILineInfo_t857  ___item, const MethodInfo* method);
#define List_1_Add_m28005(__this, ___item, method) (( void (*) (List_1_t1289 *, UILineInfo_t857 , const MethodInfo*))List_1_Add_m28005_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m28006_gshared (List_1_t1289 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m28006(__this, ___newCount, method) (( void (*) (List_1_t1289 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m28006_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m28007_gshared (List_1_t1289 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m28007(__this, ___collection, method) (( void (*) (List_1_t1289 *, Object_t*, const MethodInfo*))List_1_AddCollection_m28007_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m28008_gshared (List_1_t1289 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m28008(__this, ___enumerable, method) (( void (*) (List_1_t1289 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m28008_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m28009_gshared (List_1_t1289 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m28009(__this, ___collection, method) (( void (*) (List_1_t1289 *, Object_t*, const MethodInfo*))List_1_AddRange_m28009_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3401 * List_1_AsReadOnly_m28010_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m28010(__this, method) (( ReadOnlyCollection_1_t3401 * (*) (List_1_t1289 *, const MethodInfo*))List_1_AsReadOnly_m28010_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m28011_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_Clear_m28011(__this, method) (( void (*) (List_1_t1289 *, const MethodInfo*))List_1_Clear_m28011_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m28012_gshared (List_1_t1289 * __this, UILineInfo_t857  ___item, const MethodInfo* method);
#define List_1_Contains_m28012(__this, ___item, method) (( bool (*) (List_1_t1289 *, UILineInfo_t857 , const MethodInfo*))List_1_Contains_m28012_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m28013_gshared (List_1_t1289 * __this, UILineInfoU5BU5D_t1441* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m28013(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1289 *, UILineInfoU5BU5D_t1441*, int32_t, const MethodInfo*))List_1_CopyTo_m28013_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Find(System.Predicate`1<T>)
extern "C" UILineInfo_t857  List_1_Find_m28014_gshared (List_1_t1289 * __this, Predicate_1_t3405 * ___match, const MethodInfo* method);
#define List_1_Find_m28014(__this, ___match, method) (( UILineInfo_t857  (*) (List_1_t1289 *, Predicate_1_t3405 *, const MethodInfo*))List_1_Find_m28014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m28015_gshared (Object_t * __this /* static, unused */, Predicate_1_t3405 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m28015(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3405 *, const MethodInfo*))List_1_CheckMatch_m28015_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m28016_gshared (List_1_t1289 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3405 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m28016(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1289 *, int32_t, int32_t, Predicate_1_t3405 *, const MethodInfo*))List_1_GetIndex_m28016_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t3400  List_1_GetEnumerator_m28017_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m28017(__this, method) (( Enumerator_t3400  (*) (List_1_t1289 *, const MethodInfo*))List_1_GetEnumerator_m28017_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m28018_gshared (List_1_t1289 * __this, UILineInfo_t857  ___item, const MethodInfo* method);
#define List_1_IndexOf_m28018(__this, ___item, method) (( int32_t (*) (List_1_t1289 *, UILineInfo_t857 , const MethodInfo*))List_1_IndexOf_m28018_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m28019_gshared (List_1_t1289 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m28019(__this, ___start, ___delta, method) (( void (*) (List_1_t1289 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m28019_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m28020_gshared (List_1_t1289 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m28020(__this, ___index, method) (( void (*) (List_1_t1289 *, int32_t, const MethodInfo*))List_1_CheckIndex_m28020_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m28021_gshared (List_1_t1289 * __this, int32_t ___index, UILineInfo_t857  ___item, const MethodInfo* method);
#define List_1_Insert_m28021(__this, ___index, ___item, method) (( void (*) (List_1_t1289 *, int32_t, UILineInfo_t857 , const MethodInfo*))List_1_Insert_m28021_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m28022_gshared (List_1_t1289 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m28022(__this, ___collection, method) (( void (*) (List_1_t1289 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m28022_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m28023_gshared (List_1_t1289 * __this, UILineInfo_t857  ___item, const MethodInfo* method);
#define List_1_Remove_m28023(__this, ___item, method) (( bool (*) (List_1_t1289 *, UILineInfo_t857 , const MethodInfo*))List_1_Remove_m28023_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m28024_gshared (List_1_t1289 * __this, Predicate_1_t3405 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m28024(__this, ___match, method) (( int32_t (*) (List_1_t1289 *, Predicate_1_t3405 *, const MethodInfo*))List_1_RemoveAll_m28024_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m28025_gshared (List_1_t1289 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m28025(__this, ___index, method) (( void (*) (List_1_t1289 *, int32_t, const MethodInfo*))List_1_RemoveAt_m28025_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Reverse()
extern "C" void List_1_Reverse_m28026_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_Reverse_m28026(__this, method) (( void (*) (List_1_t1289 *, const MethodInfo*))List_1_Reverse_m28026_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort()
extern "C" void List_1_Sort_m28027_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_Sort_m28027(__this, method) (( void (*) (List_1_t1289 *, const MethodInfo*))List_1_Sort_m28027_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m28028_gshared (List_1_t1289 * __this, Comparison_1_t3408 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m28028(__this, ___comparison, method) (( void (*) (List_1_t1289 *, Comparison_1_t3408 *, const MethodInfo*))List_1_Sort_m28028_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t1441* List_1_ToArray_m28029_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_ToArray_m28029(__this, method) (( UILineInfoU5BU5D_t1441* (*) (List_1_t1289 *, const MethodInfo*))List_1_ToArray_m28029_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m28030_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m28030(__this, method) (( void (*) (List_1_t1289 *, const MethodInfo*))List_1_TrimExcess_m28030_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m28031_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m28031(__this, method) (( int32_t (*) (List_1_t1289 *, const MethodInfo*))List_1_get_Capacity_m28031_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m28032_gshared (List_1_t1289 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m28032(__this, ___value, method) (( void (*) (List_1_t1289 *, int32_t, const MethodInfo*))List_1_set_Capacity_m28032_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m28033_gshared (List_1_t1289 * __this, const MethodInfo* method);
#define List_1_get_Count_m28033(__this, method) (( int32_t (*) (List_1_t1289 *, const MethodInfo*))List_1_get_Count_m28033_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t857  List_1_get_Item_m28034_gshared (List_1_t1289 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m28034(__this, ___index, method) (( UILineInfo_t857  (*) (List_1_t1289 *, int32_t, const MethodInfo*))List_1_get_Item_m28034_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m28035_gshared (List_1_t1289 * __this, int32_t ___index, UILineInfo_t857  ___value, const MethodInfo* method);
#define List_1_set_Item_m28035(__this, ___index, ___value, method) (( void (*) (List_1_t1289 *, int32_t, UILineInfo_t857 , const MethodInfo*))List_1_set_Item_m28035_gshared)(__this, ___index, ___value, method)
