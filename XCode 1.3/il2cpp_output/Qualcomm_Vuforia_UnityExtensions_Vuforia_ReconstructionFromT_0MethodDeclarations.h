﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ReconstructionFromTargetImpl
struct ReconstructionFromTargetImpl_t909;
// Vuforia.CylinderTarget
struct CylinderTarget_t920;
// Vuforia.ImageTarget
struct ImageTarget_t1063;
// Vuforia.MultiTarget
struct MultiTarget_t1070;
// Vuforia.Trackable
struct Trackable_t891;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void Vuforia.ReconstructionFromTargetImpl::.ctor(System.IntPtr)
extern "C" void ReconstructionFromTargetImpl__ctor_m4689 (ReconstructionFromTargetImpl_t909 * __this, IntPtr_t ___nativeReconstructionPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m4690 (ReconstructionFromTargetImpl_t909 * __this, Object_t * ___cylinderTarget, Vector3_t6  ___occluderMin, Vector3_t6  ___occluderMax, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m4691 (ReconstructionFromTargetImpl_t909 * __this, Object_t * ___cylinderTarget, Vector3_t6  ___occluderMin, Vector3_t6  ___occluderMax, Vector3_t6  ___offsetToOccluderOrigin, Quaternion_t51  ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m4692 (ReconstructionFromTargetImpl_t909 * __this, Object_t * ___imageTarget, Vector3_t6  ___occluderMin, Vector3_t6  ___occluderMax, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m4693 (ReconstructionFromTargetImpl_t909 * __this, Object_t * ___imageTarget, Vector3_t6  ___occluderMin, Vector3_t6  ___occluderMax, Vector3_t6  ___offsetToOccluderOrigin, Quaternion_t51  ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m4694 (ReconstructionFromTargetImpl_t909 * __this, Object_t * ___multiTarget, Vector3_t6  ___occluderMin, Vector3_t6  ___occluderMax, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m4695 (ReconstructionFromTargetImpl_t909 * __this, Object_t * ___multiTarget, Vector3_t6  ___occluderMin, Vector3_t6  ___occluderMax, Vector3_t6  ___offsetToOccluderOrigin, Quaternion_t51  ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m4696 (ReconstructionFromTargetImpl_t909 * __this, Vector3_t6 * ___occluderMin, Vector3_t6 * ___occluderMax, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m4697 (ReconstructionFromTargetImpl_t909 * __this, Vector3_t6 * ___occluderMin, Vector3_t6 * ___occluderMax, Vector3_t6 * ___offsetToOccluderOrigin, Quaternion_t51 * ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Reset()
extern "C" bool ReconstructionFromTargetImpl_Reset_m4698 (ReconstructionFromTargetImpl_t909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Start()
extern "C" bool ReconstructionFromTargetImpl_Start_m4699 (ReconstructionFromTargetImpl_t909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(System.IntPtr,Vuforia.Trackable,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m4700 (ReconstructionFromTargetImpl_t909 * __this, IntPtr_t ___datasetPtr, Object_t * ___trackable, Vector3_t6  ___occluderMin, Vector3_t6  ___occluderMax, Vector3_t6  ___offsetToOccluderOrigin, Quaternion_t51  ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::get_CanAutoSetInitializationTarget()
extern "C" bool ReconstructionFromTargetImpl_get_CanAutoSetInitializationTarget_m4701 (ReconstructionFromTargetImpl_t909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetImpl::set_CanAutoSetInitializationTarget(System.Boolean)
extern "C" void ReconstructionFromTargetImpl_set_CanAutoSetInitializationTarget_m4702 (ReconstructionFromTargetImpl_t909 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
