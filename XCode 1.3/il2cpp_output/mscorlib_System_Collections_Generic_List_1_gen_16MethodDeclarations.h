﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t653;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t3661;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t3662;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t3663;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t2880;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2877;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2885;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t616;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_44.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m4033_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1__ctor_m4033(__this, method) (( void (*) (List_1_t653 *, const MethodInfo*))List_1__ctor_m4033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m19877_gshared (List_1_t653 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m19877(__this, ___collection, method) (( void (*) (List_1_t653 *, Object_t*, const MethodInfo*))List_1__ctor_m19877_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m19878_gshared (List_1_t653 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m19878(__this, ___capacity, method) (( void (*) (List_1_t653 *, int32_t, const MethodInfo*))List_1__ctor_m19878_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m19879_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m19879(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m19879_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19880_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19880(__this, method) (( Object_t* (*) (List_1_t653 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19880_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m19881_gshared (List_1_t653 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m19881(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t653 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m19881_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m19882_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19882(__this, method) (( Object_t * (*) (List_1_t653 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m19882_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m19883_gshared (List_1_t653 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m19883(__this, ___item, method) (( int32_t (*) (List_1_t653 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m19883_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m19884_gshared (List_1_t653 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m19884(__this, ___item, method) (( bool (*) (List_1_t653 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m19884_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m19885_gshared (List_1_t653 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m19885(__this, ___item, method) (( int32_t (*) (List_1_t653 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m19885_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m19886_gshared (List_1_t653 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m19886(__this, ___index, ___item, method) (( void (*) (List_1_t653 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m19886_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m19887_gshared (List_1_t653 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m19887(__this, ___item, method) (( void (*) (List_1_t653 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m19887_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19888_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19888(__this, method) (( bool (*) (List_1_t653 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m19889_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19889(__this, method) (( bool (*) (List_1_t653 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m19889_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m19890_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m19890(__this, method) (( Object_t * (*) (List_1_t653 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m19890_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m19891_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m19891(__this, method) (( bool (*) (List_1_t653 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m19891_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m19892_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m19892(__this, method) (( bool (*) (List_1_t653 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m19892_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m19893_gshared (List_1_t653 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m19893(__this, ___index, method) (( Object_t * (*) (List_1_t653 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m19893_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m19894_gshared (List_1_t653 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m19894(__this, ___index, ___value, method) (( void (*) (List_1_t653 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m19894_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m19895_gshared (List_1_t653 * __this, RaycastResult_t647  ___item, const MethodInfo* method);
#define List_1_Add_m19895(__this, ___item, method) (( void (*) (List_1_t653 *, RaycastResult_t647 , const MethodInfo*))List_1_Add_m19895_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m19896_gshared (List_1_t653 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m19896(__this, ___newCount, method) (( void (*) (List_1_t653 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m19896_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m19897_gshared (List_1_t653 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m19897(__this, ___collection, method) (( void (*) (List_1_t653 *, Object_t*, const MethodInfo*))List_1_AddCollection_m19897_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m19898_gshared (List_1_t653 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m19898(__this, ___enumerable, method) (( void (*) (List_1_t653 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m19898_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m19899_gshared (List_1_t653 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m19899(__this, ___collection, method) (( void (*) (List_1_t653 *, Object_t*, const MethodInfo*))List_1_AddRange_m19899_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2880 * List_1_AsReadOnly_m19900_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m19900(__this, method) (( ReadOnlyCollection_1_t2880 * (*) (List_1_t653 *, const MethodInfo*))List_1_AsReadOnly_m19900_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m19901_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_Clear_m19901(__this, method) (( void (*) (List_1_t653 *, const MethodInfo*))List_1_Clear_m19901_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m19902_gshared (List_1_t653 * __this, RaycastResult_t647  ___item, const MethodInfo* method);
#define List_1_Contains_m19902(__this, ___item, method) (( bool (*) (List_1_t653 *, RaycastResult_t647 , const MethodInfo*))List_1_Contains_m19902_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m19903_gshared (List_1_t653 * __this, RaycastResultU5BU5D_t2877* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m19903(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t653 *, RaycastResultU5BU5D_t2877*, int32_t, const MethodInfo*))List_1_CopyTo_m19903_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t647  List_1_Find_m19904_gshared (List_1_t653 * __this, Predicate_1_t2885 * ___match, const MethodInfo* method);
#define List_1_Find_m19904(__this, ___match, method) (( RaycastResult_t647  (*) (List_1_t653 *, Predicate_1_t2885 *, const MethodInfo*))List_1_Find_m19904_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m19905_gshared (Object_t * __this /* static, unused */, Predicate_1_t2885 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m19905(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2885 *, const MethodInfo*))List_1_CheckMatch_m19905_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m19906_gshared (List_1_t653 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2885 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m19906(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t653 *, int32_t, int32_t, Predicate_1_t2885 *, const MethodInfo*))List_1_GetIndex_m19906_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t2879  List_1_GetEnumerator_m19907_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m19907(__this, method) (( Enumerator_t2879  (*) (List_1_t653 *, const MethodInfo*))List_1_GetEnumerator_m19907_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m19908_gshared (List_1_t653 * __this, RaycastResult_t647  ___item, const MethodInfo* method);
#define List_1_IndexOf_m19908(__this, ___item, method) (( int32_t (*) (List_1_t653 *, RaycastResult_t647 , const MethodInfo*))List_1_IndexOf_m19908_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m19909_gshared (List_1_t653 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m19909(__this, ___start, ___delta, method) (( void (*) (List_1_t653 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m19909_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m19910_gshared (List_1_t653 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m19910(__this, ___index, method) (( void (*) (List_1_t653 *, int32_t, const MethodInfo*))List_1_CheckIndex_m19910_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m19911_gshared (List_1_t653 * __this, int32_t ___index, RaycastResult_t647  ___item, const MethodInfo* method);
#define List_1_Insert_m19911(__this, ___index, ___item, method) (( void (*) (List_1_t653 *, int32_t, RaycastResult_t647 , const MethodInfo*))List_1_Insert_m19911_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m19912_gshared (List_1_t653 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m19912(__this, ___collection, method) (( void (*) (List_1_t653 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m19912_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m19913_gshared (List_1_t653 * __this, RaycastResult_t647  ___item, const MethodInfo* method);
#define List_1_Remove_m19913(__this, ___item, method) (( bool (*) (List_1_t653 *, RaycastResult_t647 , const MethodInfo*))List_1_Remove_m19913_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m19914_gshared (List_1_t653 * __this, Predicate_1_t2885 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m19914(__this, ___match, method) (( int32_t (*) (List_1_t653 *, Predicate_1_t2885 *, const MethodInfo*))List_1_RemoveAll_m19914_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m19915_gshared (List_1_t653 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m19915(__this, ___index, method) (( void (*) (List_1_t653 *, int32_t, const MethodInfo*))List_1_RemoveAt_m19915_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m19916_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_Reverse_m19916(__this, method) (( void (*) (List_1_t653 *, const MethodInfo*))List_1_Reverse_m19916_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m19917_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_Sort_m19917(__this, method) (( void (*) (List_1_t653 *, const MethodInfo*))List_1_Sort_m19917_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m4000_gshared (List_1_t653 * __this, Comparison_1_t616 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m4000(__this, ___comparison, method) (( void (*) (List_1_t653 *, Comparison_1_t616 *, const MethodInfo*))List_1_Sort_m4000_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t2877* List_1_ToArray_m19918_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_ToArray_m19918(__this, method) (( RaycastResultU5BU5D_t2877* (*) (List_1_t653 *, const MethodInfo*))List_1_ToArray_m19918_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m19919_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m19919(__this, method) (( void (*) (List_1_t653 *, const MethodInfo*))List_1_TrimExcess_m19919_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m19920_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m19920(__this, method) (( int32_t (*) (List_1_t653 *, const MethodInfo*))List_1_get_Capacity_m19920_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m19921_gshared (List_1_t653 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m19921(__this, ___value, method) (( void (*) (List_1_t653 *, int32_t, const MethodInfo*))List_1_set_Capacity_m19921_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m19922_gshared (List_1_t653 * __this, const MethodInfo* method);
#define List_1_get_Count_m19922(__this, method) (( int32_t (*) (List_1_t653 *, const MethodInfo*))List_1_get_Count_m19922_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t647  List_1_get_Item_m19923_gshared (List_1_t653 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m19923(__this, ___index, method) (( RaycastResult_t647  (*) (List_1_t653 *, int32_t, const MethodInfo*))List_1_get_Item_m19923_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m19924_gshared (List_1_t653 * __this, int32_t ___index, RaycastResult_t647  ___value, const MethodInfo* method);
#define List_1_set_Item_m19924(__this, ___index, ___value, method) (( void (*) (List_1_t653 *, int32_t, RaycastResult_t647 , const MethodInfo*))List_1_set_Item_m19924_gshared)(__this, ___index, ___value, method)
