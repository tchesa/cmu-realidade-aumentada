﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityScript.Lang.Array
struct Array_t608;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityScript_Lang_U3CModuleU3E.h"
#include "UnityScript_Lang_U3CModuleU3EMethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_Array.h"
#include "UnityScript_Lang_UnityScript_Lang_ArrayMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Collections_CollectionBaseMethodDeclarations.h"
#include "mscorlib_System_Collections_CollectionBase.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Collections_ArrayList.h"
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "Boo_Lang_Boo_Lang_BuiltinsMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityScript.Lang.Array::.ctor()
extern "C" void Array__ctor_m2806 (Array_t608 * __this, const MethodInfo* method)
{
	{
		CollectionBase__ctor_m8211(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityScript.Lang.Array::push(System.Object)
extern "C" int32_t Array_push_m2807 (Array_t608 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		ArrayList_t57 * L_0 = CollectionBase_get_InnerList_m8212(__this, /*hidden argument*/NULL);
		Object_t * L_1 = ___value;
		NullCheck(L_0);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_1);
		ArrayList_t57 * L_2 = CollectionBase_get_InnerList_m8212(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_2);
		return L_3;
	}
}
// System.String UnityScript.Lang.Array::ToString()
extern Il2CppCodeGenString* _stringLiteral104;
extern "C" String_t* Array_ToString_m8208 (Array_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral104 = il2cpp_codegen_string_literal_from_index(104);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Array_Join_m8209(__this, _stringLiteral104, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityScript.Lang.Array::Join(System.String)
extern "C" String_t* Array_Join_m8209 (Array_t608 * __this, String_t* ___seperator, const MethodInfo* method)
{
	{
		ArrayList_t57 * L_0 = CollectionBase_get_InnerList_m8212(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___seperator;
		String_t* L_2 = Builtins_join_m8213(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String UnityScript.Lang.Array::join(System.String)
extern "C" String_t* Array_join_m2808 (Array_t608 * __this, String_t* ___seperator, const MethodInfo* method)
{
	{
		ArrayList_t57 * L_0 = CollectionBase_get_InnerList_m8212(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___seperator;
		String_t* L_2 = Builtins_join_m8213(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityScript.Lang.Array::OnValidate(System.Object)
extern "C" void Array_OnValidate_m8210 (Array_t608 * __this, Object_t * ___newValue, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
