﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.AppDomainSetup
struct AppDomainSetup_t2412;

#include "codegen/il2cpp-codegen.h"

// System.Void System.AppDomainSetup::.ctor()
extern "C" void AppDomainSetup__ctor_m14581 (AppDomainSetup_t2412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
