﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen_7MethodDeclarations.h"

// System.Void System.Comparison`1<Vuforia.DataSet>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m23541(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3109 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m15663_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.DataSet>::Invoke(T,T)
#define Comparison_1_Invoke_m23542(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3109 *, DataSet_t922 *, DataSet_t922 *, const MethodInfo*))Comparison_1_Invoke_m15664_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.DataSet>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m23543(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3109 *, DataSet_t922 *, DataSet_t922 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m15665_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.DataSet>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m23544(__this, ___result, method) (( int32_t (*) (Comparison_1_t3109 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m15666_gshared)(__this, ___result, method)
