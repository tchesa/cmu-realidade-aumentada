﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConnectionButton
struct ConnectionButton_t192;

#include "codegen/il2cpp-codegen.h"

// System.Void ConnectionButton::.ctor()
extern "C" void ConnectionButton__ctor_m1095 (ConnectionButton_t192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionButton::Start()
extern "C" void ConnectionButton_Start_m1096 (ConnectionButton_t192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionButton::OnGUI()
extern "C" void ConnectionButton_OnGUI_m1097 (ConnectionButton_t192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
