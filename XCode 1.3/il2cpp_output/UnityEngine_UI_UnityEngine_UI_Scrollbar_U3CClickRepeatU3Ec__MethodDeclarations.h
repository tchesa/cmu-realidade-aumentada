﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4
struct U3CClickRepeatU3Ec__Iterator4_t736;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::.ctor()
extern "C" void U3CClickRepeatU3Ec__Iterator4__ctor_m3471 (U3CClickRepeatU3Ec__Iterator4_t736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3472 (U3CClickRepeatU3Ec__Iterator4_t736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3473 (U3CClickRepeatU3Ec__Iterator4_t736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::MoveNext()
extern "C" bool U3CClickRepeatU3Ec__Iterator4_MoveNext_m3474 (U3CClickRepeatU3Ec__Iterator4_t736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::Dispose()
extern "C" void U3CClickRepeatU3Ec__Iterator4_Dispose_m3475 (U3CClickRepeatU3Ec__Iterator4_t736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::Reset()
extern "C" void U3CClickRepeatU3Ec__Iterator4_Reset_m3476 (U3CClickRepeatU3Ec__Iterator4_t736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
