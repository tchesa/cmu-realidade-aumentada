﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX3_AutoStopLoopedEffect
struct CFX3_AutoStopLoopedEffect_t312;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX3_AutoStopLoopedEffect::.ctor()
extern "C" void CFX3_AutoStopLoopedEffect__ctor_m1714 (CFX3_AutoStopLoopedEffect_t312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_AutoStopLoopedEffect::OnEnable()
extern "C" void CFX3_AutoStopLoopedEffect_OnEnable_m1715 (CFX3_AutoStopLoopedEffect_t312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX3_AutoStopLoopedEffect::Update()
extern "C" void CFX3_AutoStopLoopedEffect_Update_m1716 (CFX3_AutoStopLoopedEffect_t312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
