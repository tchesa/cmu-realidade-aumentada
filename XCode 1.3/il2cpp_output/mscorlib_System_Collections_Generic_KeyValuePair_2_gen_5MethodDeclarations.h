﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m25462(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1186 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m16393_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::get_Key()
#define KeyValuePair_2_get_Key_m25463(__this, method) (( int32_t (*) (KeyValuePair_2_t1186 *, const MethodInfo*))KeyValuePair_2_get_Key_m16394_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m25464(__this, ___value, method) (( void (*) (KeyValuePair_2_t1186 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16395_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::get_Value()
#define KeyValuePair_2_get_Value_m6698(__this, method) (( Object_t * (*) (KeyValuePair_2_t1186 *, const MethodInfo*))KeyValuePair_2_get_Value_m16396_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m25465(__this, ___value, method) (( void (*) (KeyValuePair_2_t1186 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m16397_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::ToString()
#define KeyValuePair_2_ToString_m25466(__this, method) (( String_t* (*) (KeyValuePair_2_t1186 *, const MethodInfo*))KeyValuePair_2_ToString_m16398_gshared)(__this, method)
