﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationExample
struct NotificationExample_t209;
// UnionAssets.FLE.CEvent
struct CEvent_t74;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void NotificationExample::.ctor()
extern "C" void NotificationExample__ctor_m1167 (NotificationExample_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::Awake()
extern "C" void NotificationExample_Awake_m1168 (NotificationExample_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::OnGUI()
extern "C" void NotificationExample_OnGUI_m1169 (NotificationExample_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::OnTokenReceived(UnionAssets.FLE.CEvent)
extern "C" void NotificationExample_OnTokenReceived_m1170 (NotificationExample_t209 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::OnNotificationScheduleResult(ISN_Result)
extern "C" void NotificationExample_OnNotificationScheduleResult_m1171 (NotificationExample_t209 * __this, ISN_Result_t114 * ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
