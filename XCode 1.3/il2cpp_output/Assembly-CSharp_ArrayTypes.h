﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// ComponentTrack[]
// ComponentTrack[]
struct ComponentTrackU5BU5D_t2630  : public Array_t { };
// UnionAssets.FLE.EventHandlerFunction[]
// UnionAssets.FLE.EventHandlerFunction[]
struct EventHandlerFunctionU5BU5D_t2651  : public Array_t { };
// UnionAssets.FLE.DataEventHandlerFunction[]
// UnionAssets.FLE.DataEventHandlerFunction[]
struct DataEventHandlerFunctionU5BU5D_t2675  : public Array_t { };
// AchievementTemplate[]
// AchievementTemplate[]
struct AchievementTemplateU5BU5D_t2690  : public Array_t { };
// GameCenterPlayerTemplate[]
// GameCenterPlayerTemplate[]
struct GameCenterPlayerTemplateU5BU5D_t2695  : public Array_t { };
// GCLeaderboard[]
// GCLeaderboard[]
struct GCLeaderboardU5BU5D_t2702  : public Array_t { };
// GCScore[]
// GCScore[]
struct GCScoreU5BU5D_t2721  : public Array_t { };
// IOSProductTemplate[]
// IOSProductTemplate[]
struct IOSProductTemplateU5BU5D_t2727  : public Array_t { };
// IOSStoreProductView[]
// IOSStoreProductView[]
struct IOSStoreProductViewU5BU5D_t2732  : public Array_t { };
// iAdBanner[]
// iAdBanner[]
struct iAdBannerU5BU5D_t2738  : public Array_t { };
// Facebook.Unity.FacebookSettings/UrlSchemes[]
// Facebook.Unity.FacebookSettings/UrlSchemes[]
struct UrlSchemesU5BU5D_t2770  : public Array_t { };
// CFX_AutoDestructShuriken[]
// CFX_AutoDestructShuriken[]
struct CFX_AutoDestructShurikenU5BU5D_t571  : public Array_t { };
// CFX_LightIntensityFade[]
// CFX_LightIntensityFade[]
struct CFX_LightIntensityFadeU5BU5D_t572  : public Array_t { };
// ARAnimation/TransparencyTrigger[]
// ARAnimation/TransparencyTrigger[]
struct TransparencyTriggerU5BU5D_t346  : public Array_t { };
// ARAnimation/TrailTrigger[]
// ARAnimation/TrailTrigger[]
struct TrailTriggerU5BU5D_t348  : public Array_t { };
// ARAnimation/ShineTrigger[]
// ARAnimation/ShineTrigger[]
struct ShineTriggerU5BU5D_t350  : public Array_t { };
// Bezier[]
// Bezier[]
struct BezierU5BU5D_t377  : public Array_t { };
// AnimationControllerScript/TimeEvent[]
// AnimationControllerScript/TimeEvent[]
struct TimeEventU5BU5D_t577  : public Array_t { };
// Vuforia.WireframeBehaviour[]
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t604  : public Array_t { };
