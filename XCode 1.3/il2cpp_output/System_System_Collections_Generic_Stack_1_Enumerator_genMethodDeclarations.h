﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2869;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m19757_gshared (Enumerator_t2871 * __this, Stack_1_t2869 * ___t, const MethodInfo* method);
#define Enumerator__ctor_m19757(__this, ___t, method) (( void (*) (Enumerator_t2871 *, Stack_1_t2869 *, const MethodInfo*))Enumerator__ctor_m19757_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m19758_gshared (Enumerator_t2871 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m19758(__this, method) (( void (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m19758_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19759_gshared (Enumerator_t2871 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19759(__this, method) (( Object_t * (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19759_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m19760_gshared (Enumerator_t2871 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m19760(__this, method) (( void (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_Dispose_m19760_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m19761_gshared (Enumerator_t2871 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m19761(__this, method) (( bool (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_MoveNext_m19761_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m19762_gshared (Enumerator_t2871 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m19762(__this, method) (( Object_t * (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_get_Current_m19762_gshared)(__this, method)
