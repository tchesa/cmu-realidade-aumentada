﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_AchievementProgressResult
struct ISN_AchievementProgressResult_t113;
// AchievementTemplate
struct AchievementTemplate_t115;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_AchievementProgressResult::.ctor(AchievementTemplate)
extern "C" void ISN_AchievementProgressResult__ctor_m667 (ISN_AchievementProgressResult_t113 * __this, AchievementTemplate_t115 * ___tpl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AchievementTemplate ISN_AchievementProgressResult::get_info()
extern "C" AchievementTemplate_t115 * ISN_AchievementProgressResult_get_info_m668 (ISN_AchievementProgressResult_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
