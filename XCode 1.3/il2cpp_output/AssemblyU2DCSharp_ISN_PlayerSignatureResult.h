﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "AssemblyU2DCSharp_ISN_Result.h"

// ISN_PlayerSignatureResult
struct  ISN_PlayerSignatureResult_t118  : public ISN_Result_t114
{
	// System.String ISN_PlayerSignatureResult::_PublicKeyUrl
	String_t* ____PublicKeyUrl_2;
	// System.Byte[] ISN_PlayerSignatureResult::_Signature
	ByteU5BU5D_t119* ____Signature_3;
	// System.Byte[] ISN_PlayerSignatureResult::_Salt
	ByteU5BU5D_t119* ____Salt_4;
	// System.Int64 ISN_PlayerSignatureResult::_Timestamp
	int64_t ____Timestamp_5;
};
