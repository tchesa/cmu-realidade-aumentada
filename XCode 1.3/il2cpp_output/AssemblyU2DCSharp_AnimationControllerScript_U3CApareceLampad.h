﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t27;
// System.Object
struct Object_t;
// AnimationControllerScript
struct AnimationControllerScript_t364;

#include "mscorlib_System_Object.h"

// AnimationControllerScript/<ApareceLampada>c__IteratorF
struct  U3CApareceLampadaU3Ec__IteratorF_t363  : public Object_t
{
	// UnityEngine.GameObject AnimationControllerScript/<ApareceLampada>c__IteratorF::<lamp>__0
	GameObject_t27 * ___U3ClampU3E__0_0;
	// System.Int32 AnimationControllerScript/<ApareceLampada>c__IteratorF::$PC
	int32_t ___U24PC_1;
	// System.Object AnimationControllerScript/<ApareceLampada>c__IteratorF::$current
	Object_t * ___U24current_2;
	// AnimationControllerScript AnimationControllerScript/<ApareceLampada>c__IteratorF::<>f__this
	AnimationControllerScript_t364 * ___U3CU3Ef__this_3;
};
