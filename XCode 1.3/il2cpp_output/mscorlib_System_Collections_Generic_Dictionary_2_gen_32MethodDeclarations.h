﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_38MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::.ctor()
#define Dictionary_2__ctor_m6757(__this, method) (( void (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2__ctor_m8129_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m25805(__this, ___comparer, method) (( void (*) (Dictionary_2_t1044 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16251_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::.ctor(System.Int32)
#define Dictionary_2__ctor_m25806(__this, ___capacity, method) (( void (*) (Dictionary_2_t1044 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16253_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m25807(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1044 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m16255_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m25808(__this, method) (( Object_t * (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16257_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25809(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1044 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16259_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25810(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1044 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16261_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m25811(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1044 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16263_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m25812(__this, ___key, method) (( bool (*) (Dictionary_2_t1044 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16265_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m25813(__this, ___key, method) (( void (*) (Dictionary_2_t1044 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16267_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25814(__this, method) (( bool (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16269_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25815(__this, method) (( Object_t * (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16271_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25816(__this, method) (( bool (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16273_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25817(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1044 *, KeyValuePair_2_t3234 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16275_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25818(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1044 *, KeyValuePair_2_t3234 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16277_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25819(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1044 *, KeyValuePair_2U5BU5D_t3723*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16279_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25820(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1044 *, KeyValuePair_2_t3234 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16281_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25821(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1044 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16283_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25822(__this, method) (( Object_t * (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16285_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25823(__this, method) (( Object_t* (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16287_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25824(__this, method) (( Object_t * (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16289_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::get_Count()
#define Dictionary_2_get_Count_m25825(__this, method) (( int32_t (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_get_Count_m16291_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::get_Item(TKey)
#define Dictionary_2_get_Item_m25826(__this, ___key, method) (( TrackableBehaviour_t410 * (*) (Dictionary_2_t1044 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m16293_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m25827(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1044 *, int32_t, TrackableBehaviour_t410 *, const MethodInfo*))Dictionary_2_set_Item_m16295_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m25828(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1044 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16297_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m25829(__this, ___size, method) (( void (*) (Dictionary_2_t1044 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16299_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m25830(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1044 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16301_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m25831(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3234  (*) (Object_t * /* static, unused */, int32_t, TrackableBehaviour_t410 *, const MethodInfo*))Dictionary_2_make_pair_m16303_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m25832(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, TrackableBehaviour_t410 *, const MethodInfo*))Dictionary_2_pick_key_m16305_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m25833(__this /* static, unused */, ___key, ___value, method) (( TrackableBehaviour_t410 * (*) (Object_t * /* static, unused */, int32_t, TrackableBehaviour_t410 *, const MethodInfo*))Dictionary_2_pick_value_m16307_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m25834(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1044 *, KeyValuePair_2U5BU5D_t3723*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16309_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::Resize()
#define Dictionary_2_Resize_m25835(__this, method) (( void (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_Resize_m16311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::Add(TKey,TValue)
#define Dictionary_2_Add_m25836(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1044 *, int32_t, TrackableBehaviour_t410 *, const MethodInfo*))Dictionary_2_Add_m16313_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::Clear()
#define Dictionary_2_Clear_m25837(__this, method) (( void (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_Clear_m16315_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m25838(__this, ___key, method) (( bool (*) (Dictionary_2_t1044 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m16317_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m25839(__this, ___value, method) (( bool (*) (Dictionary_2_t1044 *, TrackableBehaviour_t410 *, const MethodInfo*))Dictionary_2_ContainsValue_m16319_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m25840(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1044 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m16321_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m25841(__this, ___sender, method) (( void (*) (Dictionary_2_t1044 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16323_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::Remove(TKey)
#define Dictionary_2_Remove_m25842(__this, ___key, method) (( bool (*) (Dictionary_2_t1044 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m16325_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m25843(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1044 *, int32_t, TrackableBehaviour_t410 **, const MethodInfo*))Dictionary_2_TryGetValue_m16327_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::get_Keys()
#define Dictionary_2_get_Keys_m6737(__this, method) (( KeyCollection_t1208 * (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_get_Keys_m16329_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::get_Values()
#define Dictionary_2_get_Values_m6734(__this, method) (( ValueCollection_t1200 * (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_get_Values_m16331_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m25844(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1044 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16333_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m25845(__this, ___value, method) (( TrackableBehaviour_t410 * (*) (Dictionary_2_t1044 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16335_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m25846(__this, ___pair, method) (( bool (*) (Dictionary_2_t1044 *, KeyValuePair_2_t3234 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16337_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m25847(__this, method) (( Enumerator_t3235  (*) (Dictionary_2_t1044 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16339_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m25848(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, int32_t, TrackableBehaviour_t410 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16341_gshared)(__this /* static, unused */, ___key, ___value, method)
