﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
#define IndexedSet_1__ctor_m4176(__this, method) (( void (*) (IndexedSet_1_t852 *, const MethodInfo*))IndexedSet_1__ctor_m20584_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m21361(__this, method) (( Object_t * (*) (IndexedSet_1_t852 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20586_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m21362(__this, ___item, method) (( void (*) (IndexedSet_1_t852 *, Graphic_t687 *, const MethodInfo*))IndexedSet_1_Add_m20588_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m21363(__this, ___item, method) (( bool (*) (IndexedSet_1_t852 *, Graphic_t687 *, const MethodInfo*))IndexedSet_1_Remove_m20590_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m21364(__this, method) (( Object_t* (*) (IndexedSet_1_t852 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m20592_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m21365(__this, method) (( void (*) (IndexedSet_1_t852 *, const MethodInfo*))IndexedSet_1_Clear_m20594_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m21366(__this, ___item, method) (( bool (*) (IndexedSet_1_t852 *, Graphic_t687 *, const MethodInfo*))IndexedSet_1_Contains_m20596_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m21367(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t852 *, GraphicU5BU5D_t2956*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m20598_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m21368(__this, method) (( int32_t (*) (IndexedSet_1_t852 *, const MethodInfo*))IndexedSet_1_get_Count_m20600_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m21369(__this, method) (( bool (*) (IndexedSet_1_t852 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m20602_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m21370(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t852 *, Graphic_t687 *, const MethodInfo*))IndexedSet_1_IndexOf_m20604_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m21371(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t852 *, int32_t, Graphic_t687 *, const MethodInfo*))IndexedSet_1_Insert_m20606_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m21372(__this, ___index, method) (( void (*) (IndexedSet_1_t852 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m20608_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m21373(__this, ___index, method) (( Graphic_t687 * (*) (IndexedSet_1_t852 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m20610_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m21374(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t852 *, int32_t, Graphic_t687 *, const MethodInfo*))IndexedSet_1_set_Item_m20612_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m21375(__this, ___match, method) (( void (*) (IndexedSet_1_t852 *, Predicate_1_t2958 *, const MethodInfo*))IndexedSet_1_RemoveAll_m20613_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m21376(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t852 *, Comparison_1_t698 *, const MethodInfo*))IndexedSet_1_Sort_m20614_gshared)(__this, ___sortLayoutFunction, method)
