﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t2900;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t2870;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1293;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C" void UnityEvent_1__ctor_m20211_gshared (UnityEvent_1_t2900 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m20211(__this, method) (( void (*) (UnityEvent_1_t2900 *, const MethodInfo*))UnityEvent_1__ctor_m20211_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m20213_gshared (UnityEvent_1_t2900 * __this, UnityAction_1_t2870 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m20213(__this, ___call, method) (( void (*) (UnityEvent_1_t2900 *, UnityAction_1_t2870 *, const MethodInfo*))UnityEvent_1_AddListener_m20213_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m20215_gshared (UnityEvent_1_t2900 * __this, UnityAction_1_t2870 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m20215(__this, ___call, method) (( void (*) (UnityEvent_1_t2900 *, UnityAction_1_t2870 *, const MethodInfo*))UnityEvent_1_RemoveListener_m20215_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m20217_gshared (UnityEvent_1_t2900 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m20217(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t2900 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m20217_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t1293 * UnityEvent_1_GetDelegate_m20219_gshared (UnityEvent_1_t2900 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m20219(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t1293 * (*) (UnityEvent_1_t2900 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m20219_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t1293 * UnityEvent_1_GetDelegate_m20221_gshared (Object_t * __this /* static, unused */, UnityAction_1_t2870 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m20221(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t1293 * (*) (Object_t * /* static, unused */, UnityAction_1_t2870 *, const MethodInfo*))UnityEvent_1_GetDelegate_m20221_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m20222_gshared (UnityEvent_1_t2900 * __this, Object_t * ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m20222(__this, ___arg0, method) (( void (*) (UnityEvent_1_t2900 *, Object_t *, const MethodInfo*))UnityEvent_1_Invoke_m20222_gshared)(__this, ___arg0, method)
