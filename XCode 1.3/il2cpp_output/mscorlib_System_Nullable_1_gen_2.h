﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Nullable`1<System.Single>
struct  Nullable_1_t474 
{
	// T System.Nullable`1<System.Single>::value
	float ___value_0;
	// System.Boolean System.Nullable`1<System.Single>::has_value
	bool ___has_value_1;
};
