﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>
struct Transform_1_t3261;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m26159_gshared (Transform_1_t3261 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m26159(__this, ___object, ___method, method) (( void (*) (Transform_1_t3261 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m26159_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::Invoke(TKey,TValue)
extern "C" int32_t Transform_1_Invoke_m26160_gshared (Transform_1_t3261 * __this, int32_t ___key, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m26160(__this, ___key, ___value, method) (( int32_t (*) (Transform_1_t3261 *, int32_t, VirtualButtonData_t970 , const MethodInfo*))Transform_1_Invoke_m26160_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m26161_gshared (Transform_1_t3261 * __this, int32_t ___key, VirtualButtonData_t970  ___value, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m26161(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3261 *, int32_t, VirtualButtonData_t970 , AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m26161_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m26162_gshared (Transform_1_t3261 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m26162(__this, ___result, method) (( int32_t (*) (Transform_1_t3261 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m26162_gshared)(__this, ___result, method)
