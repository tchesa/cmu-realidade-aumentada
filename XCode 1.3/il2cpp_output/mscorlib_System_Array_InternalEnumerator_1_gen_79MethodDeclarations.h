﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_79.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27688_gshared (InternalEnumerator_1_t3374 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27688(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3374 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27688_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27689_gshared (InternalEnumerator_1_t3374 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27689(__this, method) (( void (*) (InternalEnumerator_1_t3374 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27689_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27690_gshared (InternalEnumerator_1_t3374 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27690(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3374 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27690_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27691_gshared (InternalEnumerator_1_t3374 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27691(__this, method) (( void (*) (InternalEnumerator_1_t3374 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27691_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27692_gshared (InternalEnumerator_1_t3374 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27692(__this, method) (( bool (*) (InternalEnumerator_1_t3374 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27692_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Current()
extern "C" KeyValuePair_2_t3373  InternalEnumerator_1_get_Current_m27693_gshared (InternalEnumerator_1_t3374 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27693(__this, method) (( KeyValuePair_2_t3373  (*) (InternalEnumerator_1_t3374 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27693_gshared)(__this, method)
