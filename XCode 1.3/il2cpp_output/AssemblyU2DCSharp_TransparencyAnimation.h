﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader[]
struct ShaderU5BU5D_t395;
// RotateScale
struct RotateScale_t385;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// TransparencyAnimation
struct  TransparencyAnimation_t340  : public MonoBehaviour_t18
{
	// UnityEngine.Shader[] TransparencyAnimation::shaders
	ShaderU5BU5D_t395* ___shaders_1;
	// System.Boolean TransparencyAnimation::show
	bool ___show_2;
	// System.Boolean TransparencyAnimation::hide
	bool ___hide_3;
	// System.Single TransparencyAnimation::fade
	float ___fade_4;
	// System.Boolean TransparencyAnimation::showing
	bool ___showing_5;
	// System.Boolean TransparencyAnimation::hiding
	bool ___hiding_6;
	// System.Single TransparencyAnimation::startTime
	float ___startTime_7;
	// RotateScale TransparencyAnimation::rotateScale
	RotateScale_t385 * ___rotateScale_8;
};
