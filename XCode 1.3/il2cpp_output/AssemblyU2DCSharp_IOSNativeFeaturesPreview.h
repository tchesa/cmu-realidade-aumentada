﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSNativePreviewBackButton
struct IOSNativePreviewBackButton_t213;

#include "AssemblyU2DCSharp_BaseIOSFeaturePreview.h"

// IOSNativeFeaturesPreview
struct  IOSNativeFeaturesPreview_t212  : public BaseIOSFeaturePreview_t183
{
};
struct IOSNativeFeaturesPreview_t212_StaticFields{
	// IOSNativePreviewBackButton IOSNativeFeaturesPreview::back
	IOSNativePreviewBackButton_t213 * ___back_11;
};
