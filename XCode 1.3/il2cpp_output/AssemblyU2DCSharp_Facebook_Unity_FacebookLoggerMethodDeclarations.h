﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.IFacebookLogger
struct IFacebookLogger_t297;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t260;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.FacebookLogger::.cctor()
extern "C" void FacebookLogger__cctor_m1661 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger::get_Instance()
extern "C" Object_t * FacebookLogger_get_Instance_m1662 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::set_Instance(Facebook.Unity.IFacebookLogger)
extern "C" void FacebookLogger_set_Instance_m1663 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::Log(System.String)
extern "C" void FacebookLogger_Log_m1664 (Object_t * __this /* static, unused */, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::Log(System.String,System.String[])
extern "C" void FacebookLogger_Log_m1665 (Object_t * __this /* static, unused */, String_t* ___format, StringU5BU5D_t260* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::Info(System.String)
extern "C" void FacebookLogger_Info_m1666 (Object_t * __this /* static, unused */, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::Info(System.String,System.String[])
extern "C" void FacebookLogger_Info_m1667 (Object_t * __this /* static, unused */, String_t* ___format, StringU5BU5D_t260* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::Warn(System.String)
extern "C" void FacebookLogger_Warn_m1668 (Object_t * __this /* static, unused */, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::Warn(System.String,System.String[])
extern "C" void FacebookLogger_Warn_m1669 (Object_t * __this /* static, unused */, String_t* ___format, StringU5BU5D_t260* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::Error(System.String)
extern "C" void FacebookLogger_Error_m1670 (Object_t * __this /* static, unused */, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger::Error(System.String,System.String[])
extern "C" void FacebookLogger_Error_m1671 (Object_t * __this /* static, unused */, String_t* ___format, StringU5BU5D_t260* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
