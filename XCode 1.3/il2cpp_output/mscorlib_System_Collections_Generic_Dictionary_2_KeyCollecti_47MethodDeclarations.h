﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_11MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m25793(__this, ___host, method) (( void (*) (Enumerator_t3231 *, Dictionary_2_t1038 *, const MethodInfo*))Enumerator__ctor_m16413_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25794(__this, method) (( Object_t * (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16414_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m25795(__this, method) (( void (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16415_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::Dispose()
#define Enumerator_Dispose_m25796(__this, method) (( void (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_Dispose_m16416_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::MoveNext()
#define Enumerator_MoveNext_m25797(__this, method) (( bool (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_MoveNext_m16417_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::get_Current()
#define Enumerator_get_Current_m25798(__this, method) (( int32_t (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_get_Current_m16418_gshared)(__this, method)
