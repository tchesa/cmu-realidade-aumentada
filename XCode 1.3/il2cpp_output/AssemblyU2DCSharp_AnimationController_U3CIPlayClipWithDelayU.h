﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t31;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// AnimationController/<IPlayClipWithDelay>c__IteratorE
struct  U3CIPlayClipWithDelayU3Ec__IteratorE_t358  : public Object_t
{
	// System.Single AnimationController/<IPlayClipWithDelay>c__IteratorE::delay
	float ___delay_0;
	// UnityEngine.AudioClip AnimationController/<IPlayClipWithDelay>c__IteratorE::clip
	AudioClip_t31 * ___clip_1;
	// UnityEngine.Vector3 AnimationController/<IPlayClipWithDelay>c__IteratorE::position
	Vector3_t6  ___position_2;
	// System.Int32 AnimationController/<IPlayClipWithDelay>c__IteratorE::$PC
	int32_t ___U24PC_3;
	// System.Object AnimationController/<IPlayClipWithDelay>c__IteratorE::$current
	Object_t * ___U24current_4;
	// System.Single AnimationController/<IPlayClipWithDelay>c__IteratorE::<$>delay
	float ___U3CU24U3Edelay_5;
	// UnityEngine.AudioClip AnimationController/<IPlayClipWithDelay>c__IteratorE::<$>clip
	AudioClip_t31 * ___U3CU24U3Eclip_6;
	// UnityEngine.Vector3 AnimationController/<IPlayClipWithDelay>c__IteratorE::<$>position
	Vector3_t6  ___U3CU24U3Eposition_7;
};
