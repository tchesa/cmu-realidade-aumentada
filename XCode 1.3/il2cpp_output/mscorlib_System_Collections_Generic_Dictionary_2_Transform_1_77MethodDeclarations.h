﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_75MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m27791(__this, ___object, ___method, method) (( void (*) (Transform_1_t3369 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m27768_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m27792(__this, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Transform_1_t3369 *, Event_t725 *, int32_t, const MethodInfo*))Transform_1_Invoke_m27769_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m27793(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3369 *, Event_t725 *, int32_t, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m27770_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m27794(__this, ___result, method) (( DictionaryEntry_t58  (*) (Transform_1_t3369 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m27771_gshared)(__this, ___result, method)
