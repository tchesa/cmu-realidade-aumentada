﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen_2MethodDeclarations.h"

// System.Void System.Func`2<System.Object,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2507(__this, ___object, ___method, method) (( void (*) (Func_2_t283 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m18497_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.Object,System.String>::Invoke(T)
#define Func_2_Invoke_m18498(__this, ___arg1, method) (( String_t* (*) (Func_2_t283 *, Object_t *, const MethodInfo*))Func_2_Invoke_m18499_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.Object,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m18500(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t283 *, Object_t *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m18501_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.Object,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m18502(__this, ___result, method) (( String_t* (*) (Func_2_t283 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m18503_gshared)(__this, ___result, method)
