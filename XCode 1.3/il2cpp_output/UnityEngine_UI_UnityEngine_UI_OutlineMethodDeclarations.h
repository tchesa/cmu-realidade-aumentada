﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Outline
struct Outline_t796;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t724;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m3980 (Outline_t796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void Outline_ModifyVertices_m3981 (Outline_t796 * __this, List_1_t724 * ___verts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
