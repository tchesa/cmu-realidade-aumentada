﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_CheckUrlResult
struct ISN_CheckUrlResult_t156;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_CheckUrlResult::.ctor(System.String,System.Boolean)
extern "C" void ISN_CheckUrlResult__ctor_m857 (ISN_CheckUrlResult_t156 * __this, String_t* ___checkedUrl, bool ___IsResultSucceeded, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_CheckUrlResult::get_url()
extern "C" String_t* ISN_CheckUrlResult_get_url_m858 (ISN_CheckUrlResult_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
