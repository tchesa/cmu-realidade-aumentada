﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.MarshalDirectiveException
struct MarshalDirectiveException_t2161;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor()
extern "C" void MarshalDirectiveException__ctor_m13166 (MarshalDirectiveException_t2161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MarshalDirectiveException__ctor_m13167 (MarshalDirectiveException_t2161 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
