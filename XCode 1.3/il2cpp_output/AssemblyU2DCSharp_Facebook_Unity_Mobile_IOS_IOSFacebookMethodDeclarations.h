﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.IOS.IOSFacebook
struct IOSFacebook_t261;
// Facebook.Unity.CallbackManager
struct CallbackManager_t224;
// System.String
struct String_t;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t242;
// Facebook.Unity.InitDelegate
struct InitDelegate_t241;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t221;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>
struct FacebookDelegate_1_t465;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t476;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>
struct FacebookDelegate_1_t468;
// System.Uri
struct Uri_t292;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppInviteResult>
struct FacebookDelegate_1_t479;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>
struct FacebookDelegate_1_t469;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupCreateResult>
struct FacebookDelegate_1_t471;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupJoinResult>
struct FacebookDelegate_1_t472;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t475;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>
struct FacebookDelegate_1_t473;
// System.String[]
struct StringU5BU5D_t260;
// Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict
struct NativeDict_t259;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t288;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_0.h"
#include "mscorlib_System_Nullable_1_gen_1.h"
#include "mscorlib_System_Nullable_1_gen_2.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode.h"

// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::.ctor()
extern "C" void IOSFacebook__ctor_m1450 (IOSFacebook_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::.ctor(Facebook.Unity.CallbackManager)
extern "C" void IOSFacebook__ctor_m1451 (IOSFacebook_t261 * __this, CallbackManager_t224 * ___callbackManager, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::get_LimitEventUsage()
extern "C" bool IOSFacebook_get_LimitEventUsage_m1452 (IOSFacebook_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::set_LimitEventUsage(System.Boolean)
extern "C" void IOSFacebook_set_LimitEventUsage_m1453 (IOSFacebook_t261 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Mobile.IOS.IOSFacebook::get_SDKName()
extern "C" String_t* IOSFacebook_get_SDKName_m1454 (IOSFacebook_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Mobile.IOS.IOSFacebook::get_SDKVersion()
extern "C" String_t* IOSFacebook_get_SDKVersion_m1455 (IOSFacebook_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::Init(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.Boolean,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern "C" void IOSFacebook_Init_m1456 (IOSFacebook_t261 * __this, String_t* ___appId, bool ___cookie, bool ___logging, bool ___status, bool ___xfbml, String_t* ___channelUrl, String_t* ___authResponse, bool ___frictionlessRequests, HideUnityDelegate_t242 * ___hideUnityDelegate, InitDelegate_t241 * ___onInitComplete, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern "C" void IOSFacebook_LogInWithReadPermissions_m1457 (IOSFacebook_t261 * __this, Object_t* ___permissions, FacebookDelegate_1_t465 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern "C" void IOSFacebook_LogInWithPublishPermissions_m1458 (IOSFacebook_t261 * __this, Object_t* ___permissions, FacebookDelegate_1_t465 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogOut()
extern "C" void IOSFacebook_LogOut_m1459 (IOSFacebook_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern "C" void IOSFacebook_AppRequest_m1460 (IOSFacebook_t261 * __this, String_t* ___message, Nullable_1_t466  ___actionType, String_t* ___objectId, Object_t* ___to, Object_t* ___filters, Object_t* ___excludeIds, Nullable_1_t467  ___maxRecipients, String_t* ___data, String_t* ___title, FacebookDelegate_1_t468 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppInvite(System.Uri,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppInviteResult>)
extern "C" void IOSFacebook_AppInvite_m1461 (IOSFacebook_t261 * __this, Uri_t292 * ___appLinkUrl, Uri_t292 * ___previewImageUrl, FacebookDelegate_1_t479 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern "C" void IOSFacebook_ShareLink_m1462 (IOSFacebook_t261 * __this, Uri_t292 * ___contentURL, String_t* ___contentTitle, String_t* ___contentDescription, Uri_t292 * ___photoURL, FacebookDelegate_1_t469 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern "C" void IOSFacebook_FeedShare_m1463 (IOSFacebook_t261 * __this, String_t* ___toId, Uri_t292 * ___link, String_t* ___linkName, String_t* ___linkCaption, String_t* ___linkDescription, Uri_t292 * ___picture, String_t* ___mediaSource, FacebookDelegate_1_t469 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::GameGroupCreate(System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupCreateResult>)
extern "C" void IOSFacebook_GameGroupCreate_m1464 (IOSFacebook_t261 * __this, String_t* ___name, String_t* ___description, String_t* ___privacy, FacebookDelegate_1_t471 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::GameGroupJoin(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupJoinResult>)
extern "C" void IOSFacebook_GameGroupJoin_m1465 (IOSFacebook_t261 * __this, String_t* ___id, FacebookDelegate_1_t472 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" void IOSFacebook_AppEventsLogEvent_m1466 (IOSFacebook_t261 * __this, String_t* ___logEvent, Nullable_1_t474  ___valueToSum, Dictionary_2_t475 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" void IOSFacebook_AppEventsLogPurchase_m1467 (IOSFacebook_t261 * __this, float ___logPurchase, String_t* ___currency, Dictionary_2_t475 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::ActivateApp(System.String)
extern "C" void IOSFacebook_ActivateApp_m1468 (IOSFacebook_t261 * __this, String_t* ___appId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern "C" void IOSFacebook_FetchDeferredAppLink_m1469 (IOSFacebook_t261 * __this, FacebookDelegate_1_t473 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern "C" void IOSFacebook_GetAppLink_m1470 (IOSFacebook_t261 * __this, FacebookDelegate_1_t473 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::SetShareDialogMode(Facebook.Unity.ShareDialogMode)
extern "C" void IOSFacebook_SetShareDialogMode_m1471 (IOSFacebook_t261 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSInit(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String)
extern "C" void IOSFacebook_IOSInit_m1472 (Object_t * __this /* static, unused */, String_t* ___appId, bool ___cookie, bool ___logging, bool ___status, bool ___frictionlessRequests, String_t* ___urlSuffix, String_t* ___unityUserAgentSuffix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSLogInWithReadPermissions(System.Int32,System.String)
extern "C" void IOSFacebook_IOSLogInWithReadPermissions_m1473 (Object_t * __this /* static, unused */, int32_t ___requestId, String_t* ___scope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSLogInWithPublishPermissions(System.Int32,System.String)
extern "C" void IOSFacebook_IOSLogInWithPublishPermissions_m1474 (Object_t * __this /* static, unused */, int32_t ___requestId, String_t* ___scope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSLogOut()
extern "C" void IOSFacebook_IOSLogOut_m1475 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSSetShareDialogMode(System.Int32)
extern "C" void IOSFacebook_IOSSetShareDialogMode_m1476 (Object_t * __this /* static, unused */, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSShareLink(System.Int32,System.String,System.String,System.String,System.String)
extern "C" void IOSFacebook_IOSShareLink_m1477 (Object_t * __this /* static, unused */, int32_t ___requestId, String_t* ___contentURL, String_t* ___contentTitle, String_t* ___contentDescription, String_t* ___photoURL, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSFeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C" void IOSFacebook_IOSFeedShare_m1478 (Object_t * __this /* static, unused */, int32_t ___requestId, String_t* ___toId, String_t* ___link, String_t* ___linkName, String_t* ___linkCaption, String_t* ___linkDescription, String_t* ___picture, String_t* ___mediaSource, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSAppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
extern "C" void IOSFacebook_IOSAppRequest_m1479 (Object_t * __this /* static, unused */, int32_t ___requestId, String_t* ___message, String_t* ___actionType, String_t* ___objectId, StringU5BU5D_t260* ___to, int32_t ___toLength, String_t* ___filters, StringU5BU5D_t260* ___excludeIds, int32_t ___excludeIdsLength, bool ___hasMaxRecipients, int32_t ___maxRecipients, String_t* ___data, String_t* ___title, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSAppInvite(System.Int32,System.String,System.String)
extern "C" void IOSFacebook_IOSAppInvite_m1480 (Object_t * __this /* static, unused */, int32_t ___requestId, String_t* ___appLinkUrl, String_t* ___previewImageUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSCreateGameGroup(System.Int32,System.String,System.String,System.String)
extern "C" void IOSFacebook_IOSCreateGameGroup_m1481 (Object_t * __this /* static, unused */, int32_t ___requestId, String_t* ___name, String_t* ___description, String_t* ___privacy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSJoinGameGroup(System.Int32,System.String)
extern "C" void IOSFacebook_IOSJoinGameGroup_m1482 (Object_t * __this /* static, unused */, int32_t ___requestId, String_t* ___groupId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSFBSettingsActivateApp(System.String)
extern "C" void IOSFacebook_IOSFBSettingsActivateApp_m1483 (Object_t * __this /* static, unused */, String_t* ___appId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSFBAppEventsLogEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
extern "C" void IOSFacebook_IOSFBAppEventsLogEvent_m1484 (Object_t * __this /* static, unused */, String_t* ___logEvent, double ___valueToSum, int32_t ___numParams, StringU5BU5D_t260* ___paramKeys, StringU5BU5D_t260* ___paramVals, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSFBAppEventsLogPurchase(System.Double,System.String,System.Int32,System.String[],System.String[])
extern "C" void IOSFacebook_IOSFBAppEventsLogPurchase_m1485 (Object_t * __this /* static, unused */, double ___logPurchase, String_t* ___currency, int32_t ___numParams, StringU5BU5D_t260* ___paramKeys, StringU5BU5D_t260* ___paramVals, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSFBAppEventsSetLimitEventUsage(System.Boolean)
extern "C" void IOSFacebook_IOSFBAppEventsSetLimitEventUsage_m1486 (Object_t * __this /* static, unused */, bool ___limitEventUsage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSGetAppLink(System.Int32)
extern "C" void IOSFacebook_IOSGetAppLink_m1487 (Object_t * __this /* static, unused */, int32_t ___requestID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Mobile.IOS.IOSFacebook::IOSFBSdkVersion()
extern "C" String_t* IOSFacebook_IOSFBSdkVersion_m1488 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::IOSFetchDeferredAppLink(System.Int32)
extern "C" void IOSFacebook_IOSFetchDeferredAppLink_m1489 (Object_t * __this /* static, unused */, int32_t ___requestID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict Facebook.Unity.Mobile.IOS.IOSFacebook::MarshallDict(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" NativeDict_t259 * IOSFacebook_MarshallDict_m1490 (Object_t * __this /* static, unused */, Dictionary_2_t475 * ___dict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict Facebook.Unity.Mobile.IOS.IOSFacebook::MarshallDict(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" NativeDict_t259 * IOSFacebook_MarshallDict_m1491 (Object_t * __this /* static, unused */, Dictionary_2_t288 * ___dict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
