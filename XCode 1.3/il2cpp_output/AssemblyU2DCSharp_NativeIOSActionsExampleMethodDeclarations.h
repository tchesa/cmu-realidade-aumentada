﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NativeIOSActionsExample
struct NativeIOSActionsExample_t208;
// IOSImagePickResult
struct IOSImagePickResult_t149;
// ISN_Result
struct ISN_Result_t114;
// ISN_CheckUrlResult
struct ISN_CheckUrlResult_t156;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void NativeIOSActionsExample::.ctor()
extern "C" void NativeIOSActionsExample__ctor_m1159 (NativeIOSActionsExample_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::Awake()
extern "C" void NativeIOSActionsExample_Awake_m1160 (NativeIOSActionsExample_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnGUI()
extern "C" void NativeIOSActionsExample_OnGUI_m1161 (NativeIOSActionsExample_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnDateChanged(System.DateTime)
extern "C" void NativeIOSActionsExample_OnDateChanged_m1162 (NativeIOSActionsExample_t208 * __this, DateTime_t220  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnPickerClosed(System.DateTime)
extern "C" void NativeIOSActionsExample_OnPickerClosed_m1163 (NativeIOSActionsExample_t208 * __this, DateTime_t220  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnImage(IOSImagePickResult)
extern "C" void NativeIOSActionsExample_OnImage_m1164 (NativeIOSActionsExample_t208 * __this, IOSImagePickResult_t149 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnImageSaved(ISN_Result)
extern "C" void NativeIOSActionsExample_OnImageSaved_m1165 (NativeIOSActionsExample_t208 * __this, ISN_Result_t114 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnUrlCheckResultAction(ISN_CheckUrlResult)
extern "C" void NativeIOSActionsExample_OnUrlCheckResultAction_m1166 (NativeIOSActionsExample_t208 * __this, ISN_CheckUrlResult_t156 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
