﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t2213;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
extern "C" void CallContextRemotingData__ctor_m13340 (CallContextRemotingData_t2213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CallContextRemotingData::Clone()
extern "C" Object_t * CallContextRemotingData_Clone_m13341 (CallContextRemotingData_t2213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
