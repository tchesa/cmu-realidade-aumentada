﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_ScreenShotMaker
struct SA_ScreenShotMaker_t216;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_ScreenShotMaker::.ctor()
extern "C" void SA_ScreenShotMaker__ctor_m1209 (SA_ScreenShotMaker_t216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_ScreenShotMaker::GetScreenshot()
extern "C" void SA_ScreenShotMaker_GetScreenshot_m1210 (SA_ScreenShotMaker_t216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA_ScreenShotMaker::SaveScreenshot()
extern "C" Object_t * SA_ScreenShotMaker_SaveScreenshot_m1211 (SA_ScreenShotMaker_t216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
