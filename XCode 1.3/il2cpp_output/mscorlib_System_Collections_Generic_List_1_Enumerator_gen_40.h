﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>
struct List_1_t371;
// AnimationControllerScript/TimeEvent
struct TimeEvent_t362;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<AnimationControllerScript/TimeEvent>
struct  Enumerator_t2843 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<AnimationControllerScript/TimeEvent>::l
	List_1_t371 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<AnimationControllerScript/TimeEvent>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<AnimationControllerScript/TimeEvent>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<AnimationControllerScript/TimeEvent>::current
	TimeEvent_t362 * ___current_3;
};
