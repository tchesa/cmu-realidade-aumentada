﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ResolveEventHandler
struct ResolveEventHandler_t2411;
// System.Object
struct Object_t;
// System.Reflection.Assembly
struct Assembly_t1669;
// System.ResolveEventArgs
struct ResolveEventArgs_t2466;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void ResolveEventHandler__ctor_m15419 (ResolveEventHandler_t2411 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern "C" Assembly_t1669 * ResolveEventHandler_Invoke_m15420 (ResolveEventHandler_t2411 * __this, Object_t * ___sender, ResolveEventArgs_t2466 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" Assembly_t1669 * pinvoke_delegate_wrapper_ResolveEventHandler_t2411(Il2CppObject* delegate, Object_t * ___sender, ResolveEventArgs_t2466 * ___args);
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * ResolveEventHandler_BeginInvoke_m15421 (ResolveEventHandler_t2411 * __this, Object_t * ___sender, ResolveEventArgs_t2466 * ___args, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
extern "C" Assembly_t1669 * ResolveEventHandler_EndInvoke_m15422 (ResolveEventHandler_t2411 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
