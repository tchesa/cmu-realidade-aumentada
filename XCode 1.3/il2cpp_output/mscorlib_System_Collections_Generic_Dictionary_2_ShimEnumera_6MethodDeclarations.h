﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ShimEnumerator_t3250;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1209;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m26046_gshared (ShimEnumerator_t3250 * __this, Dictionary_2_t1209 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m26046(__this, ___host, method) (( void (*) (ShimEnumerator_t3250 *, Dictionary_2_t1209 *, const MethodInfo*))ShimEnumerator__ctor_m26046_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m26047_gshared (ShimEnumerator_t3250 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m26047(__this, method) (( bool (*) (ShimEnumerator_t3250 *, const MethodInfo*))ShimEnumerator_MoveNext_m26047_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Entry()
extern "C" DictionaryEntry_t58  ShimEnumerator_get_Entry_m26048_gshared (ShimEnumerator_t3250 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m26048(__this, method) (( DictionaryEntry_t58  (*) (ShimEnumerator_t3250 *, const MethodInfo*))ShimEnumerator_get_Entry_m26048_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m26049_gshared (ShimEnumerator_t3250 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m26049(__this, method) (( Object_t * (*) (ShimEnumerator_t3250 *, const MethodInfo*))ShimEnumerator_get_Key_m26049_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m26050_gshared (ShimEnumerator_t3250 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m26050(__this, method) (( Object_t * (*) (ShimEnumerator_t3250 *, const MethodInfo*))ShimEnumerator_get_Value_m26050_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m26051_gshared (ShimEnumerator_t3250 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m26051(__this, method) (( Object_t * (*) (ShimEnumerator_t3250 *, const MethodInfo*))ShimEnumerator_get_Current_m26051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Reset()
extern "C" void ShimEnumerator_Reset_m26052_gshared (ShimEnumerator_t3250 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m26052(__this, method) (( void (*) (ShimEnumerator_t3250 *, const MethodInfo*))ShimEnumerator_Reset_m26052_gshared)(__this, method)
