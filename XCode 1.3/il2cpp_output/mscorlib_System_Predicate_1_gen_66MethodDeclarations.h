﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.Events.PersistentCall>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m28248(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3422 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15635_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Events.PersistentCall>::Invoke(T)
#define Predicate_1_Invoke_m28249(__this, ___obj, method) (( bool (*) (Predicate_1_t3422 *, PersistentCall_t1296 *, const MethodInfo*))Predicate_1_Invoke_m15636_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Events.PersistentCall>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m28250(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3422 *, PersistentCall_t1296 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15637_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Events.PersistentCall>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m28251(__this, ___result, method) (( bool (*) (Predicate_1_t3422 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15638_gshared)(__this, ___result, method)
