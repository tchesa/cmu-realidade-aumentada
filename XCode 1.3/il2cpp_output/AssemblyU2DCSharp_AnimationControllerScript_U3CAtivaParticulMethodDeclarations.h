﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14
struct U3CAtivaParticulaPortalU3Ec__Iterator14_t369;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::.ctor()
extern "C" void U3CAtivaParticulaPortalU3Ec__Iterator14__ctor_m1870 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAtivaParticulaPortalU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1871 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAtivaParticulaPortalU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m1872 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::MoveNext()
extern "C" bool U3CAtivaParticulaPortalU3Ec__Iterator14_MoveNext_m1873 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::Dispose()
extern "C" void U3CAtivaParticulaPortalU3Ec__Iterator14_Dispose_m1874 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::Reset()
extern "C" void U3CAtivaParticulaPortalU3Ec__Iterator14_Reset_m1875 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
