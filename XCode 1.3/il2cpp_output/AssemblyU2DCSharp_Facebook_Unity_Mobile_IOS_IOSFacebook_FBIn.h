﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_IOS_IOSFacebook_FBIn.h"

// Facebook.Unity.Mobile.IOS.IOSFacebook/FBInsightsFlushBehavior
struct  FBInsightsFlushBehavior_t258 
{
	// System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook/FBInsightsFlushBehavior::value__
	int32_t ___value___1;
};
