﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowHideSprite
struct ShowHideSprite_t386;

#include "codegen/il2cpp-codegen.h"

// System.Void ShowHideSprite::.ctor()
extern "C" void ShowHideSprite__ctor_m1975 (ShowHideSprite_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowHideSprite::Show()
extern "C" void ShowHideSprite_Show_m1976 (ShowHideSprite_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowHideSprite::Hide()
extern "C" void ShowHideSprite_Hide_m1977 (ShowHideSprite_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowHideSprite::Start()
extern "C" void ShowHideSprite_Start_m1978 (ShowHideSprite_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowHideSprite::Update()
extern "C" void ShowHideSprite_Update_m1979 (ShowHideSprite_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
