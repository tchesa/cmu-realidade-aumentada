﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.GroupCreateResult
struct GroupCreateResult_t280;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.GroupCreateResult::.ctor(System.String)
extern "C" void GroupCreateResult__ctor_m1598 (GroupCreateResult_t280 * __this, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.GroupCreateResult::get_GroupId()
extern "C" String_t* GroupCreateResult_get_GroupId_m1599 (GroupCreateResult_t280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.GroupCreateResult::set_GroupId(System.String)
extern "C" void GroupCreateResult_set_GroupId_m1600 (GroupCreateResult_t280 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
