﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSUIUserNotificationType
struct IOSUIUserNotificationType_t94;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSUIUserNotificationType::.ctor()
extern "C" void IOSUIUserNotificationType__ctor_m551 (IOSUIUserNotificationType_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
