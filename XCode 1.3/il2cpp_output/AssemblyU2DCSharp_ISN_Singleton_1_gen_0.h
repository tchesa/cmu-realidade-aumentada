﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSCamera
struct IOSCamera_t145;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// ISN_Singleton`1<IOSCamera>
struct  ISN_Singleton_1_t146  : public EventDispatcher_t69
{
};
struct ISN_Singleton_1_t146_StaticFields{
	// T ISN_Singleton`1<IOSCamera>::_instance
	IOSCamera_t145 * ____instance_3;
	// System.Boolean ISN_Singleton`1<IOSCamera>::applicationIsQuitting
	bool ___applicationIsQuitting_4;
};
