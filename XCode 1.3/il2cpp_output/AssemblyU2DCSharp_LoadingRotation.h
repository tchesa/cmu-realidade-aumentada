﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// LoadingRotation
struct  LoadingRotation_t383  : public MonoBehaviour_t18
{
	// System.Single LoadingRotation::rotationStep
	float ___rotationStep_1;
	// System.Single LoadingRotation::speed
	float ___speed_2;
	// System.Single LoadingRotation::realRotation
	float ___realRotation_3;
};
