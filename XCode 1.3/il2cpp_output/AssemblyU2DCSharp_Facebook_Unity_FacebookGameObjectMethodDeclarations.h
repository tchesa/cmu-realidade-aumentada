﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FacebookGameObject
struct FacebookGameObject_t229;
// Facebook.Unity.IFacebookImplementation
struct IFacebookImplementation_t243;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.FacebookGameObject::.ctor()
extern "C" void FacebookGameObject__ctor_m1351 (FacebookGameObject_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::get_Facebook()
extern "C" Object_t * FacebookGameObject_get_Facebook_m1352 (FacebookGameObject_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::set_Facebook(Facebook.Unity.IFacebookImplementation)
extern "C" void FacebookGameObject_set_Facebook_m1353 (FacebookGameObject_t229 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.FacebookGameObject::get_Initialized()
extern "C" bool FacebookGameObject_get_Initialized_m1354 (FacebookGameObject_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::set_Initialized(System.Boolean)
extern "C" void FacebookGameObject_set_Initialized_m1355 (FacebookGameObject_t229 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::Awake()
extern "C" void FacebookGameObject_Awake_m1356 (FacebookGameObject_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnInitComplete(System.String)
extern "C" void FacebookGameObject_OnInitComplete_m1357 (FacebookGameObject_t229 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnLoginComplete(System.String)
extern "C" void FacebookGameObject_OnLoginComplete_m1358 (FacebookGameObject_t229 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnLogoutComplete(System.String)
extern "C" void FacebookGameObject_OnLogoutComplete_m1359 (FacebookGameObject_t229 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnGetAppLinkComplete(System.String)
extern "C" void FacebookGameObject_OnGetAppLinkComplete_m1360 (FacebookGameObject_t229 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnGroupCreateComplete(System.String)
extern "C" void FacebookGameObject_OnGroupCreateComplete_m1361 (FacebookGameObject_t229 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnGroupJoinComplete(System.String)
extern "C" void FacebookGameObject_OnGroupJoinComplete_m1362 (FacebookGameObject_t229 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnAppRequestsComplete(System.String)
extern "C" void FacebookGameObject_OnAppRequestsComplete_m1363 (FacebookGameObject_t229 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnShareLinkComplete(System.String)
extern "C" void FacebookGameObject_OnShareLinkComplete_m1364 (FacebookGameObject_t229 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookGameObject::OnAwake()
extern "C" void FacebookGameObject_OnAwake_m1365 (FacebookGameObject_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
