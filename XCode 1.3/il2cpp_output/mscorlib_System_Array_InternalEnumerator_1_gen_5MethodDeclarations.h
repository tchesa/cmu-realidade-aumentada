﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_5.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15716_gshared (InternalEnumerator_1_t2597 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15716(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2597 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15716_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15717_gshared (InternalEnumerator_1_t2597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15717(__this, method) (( void (*) (InternalEnumerator_1_t2597 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15717_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15718_gshared (InternalEnumerator_1_t2597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15718(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2597 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15718_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15719_gshared (InternalEnumerator_1_t2597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15719(__this, method) (( void (*) (InternalEnumerator_1_t2597 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15719_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15720_gshared (InternalEnumerator_1_t2597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15720(__this, method) (( bool (*) (InternalEnumerator_1_t2597 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15720_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t29  InternalEnumerator_1_get_Current_m15721_gshared (InternalEnumerator_1_t2597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15721(__this, method) (( Vector2_t29  (*) (InternalEnumerator_1_t2597 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15721_gshared)(__this, method)
