﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>
struct Collection_1_t3080;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3054;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t3690;
// System.Collections.Generic.IList`1<Vuforia.Image/PIXEL_FORMAT>
struct IList_1_t3079;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void Collection_1__ctor_m23021_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1__ctor_m23021(__this, method) (( void (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1__ctor_m23021_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23022_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23022(__this, method) (( bool (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23022_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23023_gshared (Collection_1_t3080 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m23023(__this, ___array, ___index, method) (( void (*) (Collection_1_t3080 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m23023_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m23024_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m23024(__this, method) (( Object_t * (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m23024_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m23025_gshared (Collection_1_t3080 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m23025(__this, ___value, method) (( int32_t (*) (Collection_1_t3080 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m23025_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m23026_gshared (Collection_1_t3080 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m23026(__this, ___value, method) (( bool (*) (Collection_1_t3080 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m23026_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m23027_gshared (Collection_1_t3080 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m23027(__this, ___value, method) (( int32_t (*) (Collection_1_t3080 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m23027_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m23028_gshared (Collection_1_t3080 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m23028(__this, ___index, ___value, method) (( void (*) (Collection_1_t3080 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m23028_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m23029_gshared (Collection_1_t3080 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m23029(__this, ___value, method) (( void (*) (Collection_1_t3080 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m23029_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m23030_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m23030(__this, method) (( bool (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m23030_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m23031_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m23031(__this, method) (( Object_t * (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m23031_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m23032_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m23032(__this, method) (( bool (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m23032_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m23033_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m23033(__this, method) (( bool (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m23033_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m23034_gshared (Collection_1_t3080 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m23034(__this, ___index, method) (( Object_t * (*) (Collection_1_t3080 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m23034_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m23035_gshared (Collection_1_t3080 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m23035(__this, ___index, ___value, method) (( void (*) (Collection_1_t3080 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m23035_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
extern "C" void Collection_1_Add_m23036_gshared (Collection_1_t3080 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m23036(__this, ___item, method) (( void (*) (Collection_1_t3080 *, int32_t, const MethodInfo*))Collection_1_Add_m23036_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
extern "C" void Collection_1_Clear_m23037_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_Clear_m23037(__this, method) (( void (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_Clear_m23037_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::ClearItems()
extern "C" void Collection_1_ClearItems_m23038_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m23038(__this, method) (( void (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_ClearItems_m23038_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
extern "C" bool Collection_1_Contains_m23039_gshared (Collection_1_t3080 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m23039(__this, ___item, method) (( bool (*) (Collection_1_t3080 *, int32_t, const MethodInfo*))Collection_1_Contains_m23039_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m23040_gshared (Collection_1_t3080 * __this, PIXEL_FORMATU5BU5D_t3054* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m23040(__this, ___array, ___index, method) (( void (*) (Collection_1_t3080 *, PIXEL_FORMATU5BU5D_t3054*, int32_t, const MethodInfo*))Collection_1_CopyTo_m23040_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m23041_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m23041(__this, method) (( Object_t* (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_GetEnumerator_m23041_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m23042_gshared (Collection_1_t3080 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m23042(__this, ___item, method) (( int32_t (*) (Collection_1_t3080 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m23042_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m23043_gshared (Collection_1_t3080 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m23043(__this, ___index, ___item, method) (( void (*) (Collection_1_t3080 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m23043_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m23044_gshared (Collection_1_t3080 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m23044(__this, ___index, ___item, method) (( void (*) (Collection_1_t3080 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m23044_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
extern "C" bool Collection_1_Remove_m23045_gshared (Collection_1_t3080 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m23045(__this, ___item, method) (( bool (*) (Collection_1_t3080 *, int32_t, const MethodInfo*))Collection_1_Remove_m23045_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m23046_gshared (Collection_1_t3080 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m23046(__this, ___index, method) (( void (*) (Collection_1_t3080 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m23046_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m23047_gshared (Collection_1_t3080 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m23047(__this, ___index, method) (( void (*) (Collection_1_t3080 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m23047_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
extern "C" int32_t Collection_1_get_Count_m23048_gshared (Collection_1_t3080 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m23048(__this, method) (( int32_t (*) (Collection_1_t3080 *, const MethodInfo*))Collection_1_get_Count_m23048_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m23049_gshared (Collection_1_t3080 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m23049(__this, ___index, method) (( int32_t (*) (Collection_1_t3080 *, int32_t, const MethodInfo*))Collection_1_get_Item_m23049_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m23050_gshared (Collection_1_t3080 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m23050(__this, ___index, ___value, method) (( void (*) (Collection_1_t3080 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m23050_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m23051_gshared (Collection_1_t3080 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m23051(__this, ___index, ___item, method) (( void (*) (Collection_1_t3080 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m23051_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m23052_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m23052(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m23052_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m23053_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m23053(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m23053_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m23054_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m23054(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m23054_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m23055_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m23055(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m23055_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m23056_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m23056(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m23056_gshared)(__this /* static, unused */, ___list, method)
