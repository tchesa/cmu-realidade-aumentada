﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t1110;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Predicate`1<Vuforia.ISmartTerrainEventHandler>
struct  Predicate_1_t3207  : public MulticastDelegate_t10
{
};
