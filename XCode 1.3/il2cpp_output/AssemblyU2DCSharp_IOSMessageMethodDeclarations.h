﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSMessage
struct IOSMessage_t166;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSMessage::.ctor()
extern "C" void IOSMessage__ctor_m884 (IOSMessage_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSMessage IOSMessage::Create(System.String,System.String)
extern "C" IOSMessage_t166 * IOSMessage_Create_m885 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSMessage IOSMessage::Create(System.String,System.String,System.String)
extern "C" IOSMessage_t166 * IOSMessage_Create_m886 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSMessage::init()
extern "C" void IOSMessage_init_m887 (IOSMessage_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSMessage::onPopUpCallBack(System.String)
extern "C" void IOSMessage_onPopUpCallBack_m888 (IOSMessage_t166 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSMessage::<OnComplete>m__24()
extern "C" void IOSMessage_U3COnCompleteU3Em__24_m889 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
