﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>
struct Transform_1_t2796;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m18688_gshared (Transform_1_t2796 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m18688(__this, ___object, ___method, method) (( void (*) (Transform_1_t2796 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m18688_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t2787  Transform_1_Invoke_m18689_gshared (Transform_1_t2796 * __this, Object_t * ___key, float ___value, const MethodInfo* method);
#define Transform_1_Invoke_m18689(__this, ___key, ___value, method) (( KeyValuePair_2_t2787  (*) (Transform_1_t2796 *, Object_t *, float, const MethodInfo*))Transform_1_Invoke_m18689_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m18690_gshared (Transform_1_t2796 * __this, Object_t * ___key, float ___value, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m18690(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2796 *, Object_t *, float, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m18690_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t2787  Transform_1_EndInvoke_m18691_gshared (Transform_1_t2796 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m18691(__this, ___result, method) (( KeyValuePair_2_t2787  (*) (Transform_1_t2796 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m18691_gshared)(__this, ___result, method)
