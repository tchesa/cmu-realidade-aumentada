﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARAnimation/ShineTrigger[]
struct ShineTriggerU5BU5D_t350;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<ARAnimation/ShineTrigger>
struct  List_1_t351  : public Object_t
{
	// T[] System.Collections.Generic.List`1<ARAnimation/ShineTrigger>::_items
	ShineTriggerU5BU5D_t350* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<ARAnimation/ShineTrigger>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<ARAnimation/ShineTrigger>::_version
	int32_t ____version_3;
};
struct List_1_t351_StaticFields{
	// T[] System.Collections.Generic.List`1<ARAnimation/ShineTrigger>::EmptyArray
	ShineTriggerU5BU5D_t350* ___EmptyArray_4;
};
