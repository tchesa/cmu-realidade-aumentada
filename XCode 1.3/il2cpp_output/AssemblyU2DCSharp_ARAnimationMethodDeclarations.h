﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARAnimation
struct ARAnimation_t345;
// ARAnimation/TransparencyTrigger
struct TransparencyTrigger_t339;
// ARAnimation/TrailTrigger
struct TrailTrigger_t341;
// ARAnimation/ShineTrigger
struct ShineTrigger_t343;

#include "codegen/il2cpp-codegen.h"

// System.Void ARAnimation::.ctor()
extern "C" void ARAnimation__ctor_m1810 (ARAnimation_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARAnimation::Update()
extern "C" void ARAnimation_Update_m1811 (ARAnimation_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARAnimation::Play()
extern "C" void ARAnimation_Play_m1812 (ARAnimation_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARAnimation::Stop()
extern "C" void ARAnimation_Stop_m1813 (ARAnimation_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARAnimation::<Play>m__36(ARAnimation/TransparencyTrigger,ARAnimation/TransparencyTrigger)
extern "C" int32_t ARAnimation_U3CPlayU3Em__36_m1814 (Object_t * __this /* static, unused */, TransparencyTrigger_t339 * ___x, TransparencyTrigger_t339 * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARAnimation::<Play>m__37(ARAnimation/TransparencyTrigger,ARAnimation/TransparencyTrigger)
extern "C" int32_t ARAnimation_U3CPlayU3Em__37_m1815 (Object_t * __this /* static, unused */, TransparencyTrigger_t339 * ___x, TransparencyTrigger_t339 * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARAnimation::<Play>m__38(ARAnimation/TrailTrigger,ARAnimation/TrailTrigger)
extern "C" int32_t ARAnimation_U3CPlayU3Em__38_m1816 (Object_t * __this /* static, unused */, TrailTrigger_t341 * ___x, TrailTrigger_t341 * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARAnimation::<Play>m__39(ARAnimation/ShineTrigger,ARAnimation/ShineTrigger)
extern "C" int32_t ARAnimation_U3CPlayU3Em__39_m1817 (Object_t * __this /* static, unused */, ShineTrigger_t343 * ___x, ShineTrigger_t343 * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
