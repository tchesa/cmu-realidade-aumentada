﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Canvas.CanvasFacebook
struct CanvasFacebook_t226;
// Facebook.Unity.CallbackManager
struct CallbackManager_t224;
// System.String
struct String_t;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t242;
// Facebook.Unity.InitDelegate
struct InitDelegate_t241;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t221;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>
struct FacebookDelegate_1_t465;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t476;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>
struct FacebookDelegate_1_t468;
// System.Uri
struct Uri_t292;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>
struct FacebookDelegate_1_t469;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>
struct FacebookDelegate_1_t470;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupCreateResult>
struct FacebookDelegate_1_t471;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupJoinResult>
struct FacebookDelegate_1_t472;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>
struct FacebookDelegate_1_t473;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t475;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t225;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_0.h"
#include "mscorlib_System_Nullable_1_gen_1.h"
#include "mscorlib_System_Nullable_1_gen_2.h"

// System.Void Facebook.Unity.Canvas.CanvasFacebook::.ctor()
extern "C" void CanvasFacebook__ctor_m1227 (CanvasFacebook_t226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::.ctor(Facebook.Unity.CallbackManager)
extern "C" void CanvasFacebook__ctor_m1228 (CanvasFacebook_t226 * __this, CallbackManager_t224 * ___callbackManager, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.Canvas.CanvasFacebook::get_LimitEventUsage()
extern "C" bool CanvasFacebook_get_LimitEventUsage_m1229 (CanvasFacebook_t226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::set_LimitEventUsage(System.Boolean)
extern "C" void CanvasFacebook_set_LimitEventUsage_m1230 (CanvasFacebook_t226 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKName()
extern "C" String_t* CanvasFacebook_get_SDKName_m1231 (CanvasFacebook_t226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKVersion()
extern "C" String_t* CanvasFacebook_get_SDKVersion_m1232 (CanvasFacebook_t226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKUserAgent()
extern "C" String_t* CanvasFacebook_get_SDKUserAgent_m1233 (CanvasFacebook_t226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Canvas.CanvasFacebook::get_IntegrationMethodJs()
extern "C" String_t* CanvasFacebook_get_IntegrationMethodJs_m1234 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::Init(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.Boolean,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern "C" void CanvasFacebook_Init_m1235 (CanvasFacebook_t226 * __this, String_t* ___appId, bool ___cookie, bool ___logging, bool ___status, bool ___xfbml, String_t* ___channelUrl, String_t* ___authResponse, bool ___frictionlessRequests, HideUnityDelegate_t242 * ___hideUnityDelegate, InitDelegate_t241 * ___onInitComplete, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern "C" void CanvasFacebook_LogInWithPublishPermissions_m1236 (CanvasFacebook_t226 * __this, Object_t* ___permissions, FacebookDelegate_1_t465 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern "C" void CanvasFacebook_LogInWithReadPermissions_m1237 (CanvasFacebook_t226 * __this, Object_t* ___permissions, FacebookDelegate_1_t465 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::LogOut()
extern "C" void CanvasFacebook_LogOut_m1238 (CanvasFacebook_t226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern "C" void CanvasFacebook_AppRequest_m1239 (CanvasFacebook_t226 * __this, String_t* ___message, Nullable_1_t466  ___actionType, String_t* ___objectId, Object_t* ___to, Object_t* ___filters, Object_t* ___excludeIds, Nullable_1_t467  ___maxRecipients, String_t* ___data, String_t* ___title, FacebookDelegate_1_t468 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::ActivateApp(System.String)
extern "C" void CanvasFacebook_ActivateApp_m1240 (CanvasFacebook_t226 * __this, String_t* ___appId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern "C" void CanvasFacebook_ShareLink_m1241 (CanvasFacebook_t226 * __this, Uri_t292 * ___contentURL, String_t* ___contentTitle, String_t* ___contentDescription, Uri_t292 * ___photoURL, FacebookDelegate_1_t469 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern "C" void CanvasFacebook_FeedShare_m1242 (CanvasFacebook_t226 * __this, String_t* ___toId, Uri_t292 * ___link, String_t* ___linkName, String_t* ___linkCaption, String_t* ___linkDescription, Uri_t292 * ___picture, String_t* ___mediaSource, FacebookDelegate_1_t469 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern "C" void CanvasFacebook_Pay_m1243 (CanvasFacebook_t226 * __this, String_t* ___product, String_t* ___action, int32_t ___quantity, Nullable_1_t467  ___quantityMin, Nullable_1_t467  ___quantityMax, String_t* ___requestId, String_t* ___pricepointId, String_t* ___testCurrency, FacebookDelegate_1_t470 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::GameGroupCreate(System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupCreateResult>)
extern "C" void CanvasFacebook_GameGroupCreate_m1244 (CanvasFacebook_t226 * __this, String_t* ___name, String_t* ___description, String_t* ___privacy, FacebookDelegate_1_t471 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::GameGroupJoin(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupJoinResult>)
extern "C" void CanvasFacebook_GameGroupJoin_m1245 (CanvasFacebook_t226 * __this, String_t* ___id, FacebookDelegate_1_t472 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern "C" void CanvasFacebook_GetAppLink_m1246 (CanvasFacebook_t226 * __this, FacebookDelegate_1_t473 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" void CanvasFacebook_AppEventsLogEvent_m1247 (CanvasFacebook_t226 * __this, String_t* ___logEvent, Nullable_1_t474  ___valueToSum, Dictionary_2_t475 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" void CanvasFacebook_AppEventsLogPurchase_m1248 (CanvasFacebook_t226 * __this, float ___logPurchase, String_t* ___currency, Dictionary_2_t475 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnLoginComplete(System.String)
extern "C" void CanvasFacebook_OnLoginComplete_m1249 (CanvasFacebook_t226 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnGetAppLinkComplete(System.String)
extern "C" void CanvasFacebook_OnGetAppLinkComplete_m1250 (CanvasFacebook_t226 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnFacebookAuthResponseChange(System.String)
extern "C" void CanvasFacebook_OnFacebookAuthResponseChange_m1251 (CanvasFacebook_t226 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnPayComplete(System.String)
extern "C" void CanvasFacebook_OnPayComplete_m1252 (CanvasFacebook_t226 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnAppRequestsComplete(System.String)
extern "C" void CanvasFacebook_OnAppRequestsComplete_m1253 (CanvasFacebook_t226 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnShareLinkComplete(System.String)
extern "C" void CanvasFacebook_OnShareLinkComplete_m1254 (CanvasFacebook_t226 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnGroupCreateComplete(System.String)
extern "C" void CanvasFacebook_OnGroupCreateComplete_m1255 (CanvasFacebook_t226 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnGroupJoinComplete(System.String)
extern "C" void CanvasFacebook_OnGroupJoinComplete_m1256 (CanvasFacebook_t226 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook::OnUrlResponse(System.String)
extern "C" void CanvasFacebook_OnUrlResponse_m1257 (CanvasFacebook_t226 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Canvas.CanvasFacebook::FormatAuthResponse(System.String)
extern "C" String_t* CanvasFacebook_FormatAuthResponse_m1258 (Object_t * __this /* static, unused */, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Canvas.CanvasFacebook::FormatResult(System.String)
extern "C" String_t* CanvasFacebook_FormatResult_m1259 (Object_t * __this /* static, unused */, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.Canvas.CanvasFacebook::GetFormattedResponseDictionary(System.String)
extern "C" Object_t* CanvasFacebook_GetFormattedResponseDictionary_m1260 (Object_t * __this /* static, unused */, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
