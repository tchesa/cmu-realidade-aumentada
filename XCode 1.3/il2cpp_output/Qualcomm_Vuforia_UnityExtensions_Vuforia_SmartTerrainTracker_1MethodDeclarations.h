﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t1004;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t911;

#include "codegen/il2cpp-codegen.h"

// System.Single Vuforia.SmartTerrainTrackerImpl::get_ScaleToMillimeter()
extern "C" float SmartTerrainTrackerImpl_get_ScaleToMillimeter_m5073 (SmartTerrainTrackerImpl_t1004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::SetScaleToMillimeter(System.Single)
extern "C" bool SmartTerrainTrackerImpl_SetScaleToMillimeter_m5074 (SmartTerrainTrackerImpl_t1004 * __this, float ___scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTrackerImpl::get_SmartTerrainBuilder()
extern "C" SmartTerrainBuilder_t911 * SmartTerrainTrackerImpl_get_SmartTerrainBuilder_m5075 (SmartTerrainTrackerImpl_t1004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::Start()
extern "C" bool SmartTerrainTrackerImpl_Start_m5076 (SmartTerrainTrackerImpl_t1004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::Stop()
extern "C" void SmartTerrainTrackerImpl_Stop_m5077 (SmartTerrainTrackerImpl_t1004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::.ctor()
extern "C" void SmartTerrainTrackerImpl__ctor_m5078 (SmartTerrainTrackerImpl_t1004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
