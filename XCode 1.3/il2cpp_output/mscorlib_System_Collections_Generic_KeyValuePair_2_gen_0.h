﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// iAdBanner
struct iAdBanner_t172;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,iAdBanner>
struct  KeyValuePair_2_t502 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,iAdBanner>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,iAdBanner>::value
	iAdBanner_t172 * ___value_1;
};
