﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.SortedList/ListKeys
struct ListKeys_t1977;
// System.Collections.SortedList
struct SortedList_t1668;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.SortedList/ListKeys::.ctor(System.Collections.SortedList)
extern "C" void ListKeys__ctor_m11902 (ListKeys_t1977 * __this, SortedList_t1668 * ___host, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::get_Count()
extern "C" int32_t ListKeys_get_Count_m11903 (ListKeys_t1977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsSynchronized()
extern "C" bool ListKeys_get_IsSynchronized_m11904 (ListKeys_t1977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/ListKeys::get_SyncRoot()
extern "C" Object_t * ListKeys_get_SyncRoot_m11905 (ListKeys_t1977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::CopyTo(System.Array,System.Int32)
extern "C" void ListKeys_CopyTo_m11906 (ListKeys_t1977 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsFixedSize()
extern "C" bool ListKeys_get_IsFixedSize_m11907 (ListKeys_t1977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsReadOnly()
extern "C" bool ListKeys_get_IsReadOnly_m11908 (ListKeys_t1977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/ListKeys::get_Item(System.Int32)
extern "C" Object_t * ListKeys_get_Item_m11909 (ListKeys_t1977 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::set_Item(System.Int32,System.Object)
extern "C" void ListKeys_set_Item_m11910 (ListKeys_t1977 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::Add(System.Object)
extern "C" int32_t ListKeys_Add_m11911 (ListKeys_t1977 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Clear()
extern "C" void ListKeys_Clear_m11912 (ListKeys_t1977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::Contains(System.Object)
extern "C" bool ListKeys_Contains_m11913 (ListKeys_t1977 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::IndexOf(System.Object)
extern "C" int32_t ListKeys_IndexOf_m11914 (ListKeys_t1977 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Insert(System.Int32,System.Object)
extern "C" void ListKeys_Insert_m11915 (ListKeys_t1977 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Remove(System.Object)
extern "C" void ListKeys_Remove_m11916 (ListKeys_t1977 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::RemoveAt(System.Int32)
extern "C" void ListKeys_RemoveAt_m11917 (ListKeys_t1977 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.SortedList/ListKeys::GetEnumerator()
extern "C" Object_t * ListKeys_GetEnumerator_m11918 (ListKeys_t1977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
