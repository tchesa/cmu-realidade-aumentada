﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_40.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_EyewearCali.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22119_gshared (InternalEnumerator_1_t3021 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22119(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3021 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22119_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22120_gshared (InternalEnumerator_1_t3021 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22120(__this, method) (( void (*) (InternalEnumerator_1_t3021 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22120_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22121_gshared (InternalEnumerator_1_t3021 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22121(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3021 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22121_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22122_gshared (InternalEnumerator_1_t3021 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22122(__this, method) (( void (*) (InternalEnumerator_1_t3021 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22122_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22123_gshared (InternalEnumerator_1_t3021 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22123(__this, method) (( bool (*) (InternalEnumerator_1_t3021 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22123_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::get_Current()
extern "C" EyewearCalibrationReading_t913  InternalEnumerator_1_get_Current_m22124_gshared (InternalEnumerator_1_t3021 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22124(__this, method) (( EyewearCalibrationReading_t913  (*) (InternalEnumerator_1_t3021 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22124_gshared)(__this, method)
