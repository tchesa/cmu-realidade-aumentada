﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBServices
struct FBServices_t381;
// System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>
struct Action_2_t380;
// Facebook.Unity.ILoginResult
struct ILoginResult_t489;
// System.Byte[]
struct ByteU5BU5D_t119;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t294;

#include "codegen/il2cpp-codegen.h"

// System.Void FBServices::.ctor()
extern "C" void FBServices__ctor_m1948 (FBServices_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FBServices FBServices::get_Current()
extern "C" FBServices_t381 * FBServices_get_Current_m1949 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::Instantiate()
extern "C" void FBServices_Instantiate_m1950 (FBServices_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::Awake()
extern "C" void FBServices_Awake_m1951 (FBServices_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::Init()
extern "C" void FBServices_Init_m1952 (FBServices_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::OnHideUnity()
extern "C" void FBServices_OnHideUnity_m1953 (FBServices_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::Login(System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>)
extern "C" void FBServices_Login_m1954 (FBServices_t381 * __this, Action_2_t380 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::LoginCallback(Facebook.Unity.ILoginResult)
extern "C" void FBServices_LoginCallback_m1955 (FBServices_t381 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::Logout()
extern "C" void FBServices_Logout_m1956 (FBServices_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::API(System.Byte[],Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern "C" void FBServices_API_m1957 (FBServices_t381 * __this, ByteU5BU5D_t119* ___image, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices::OnGUI()
extern "C" void FBServices_OnGUI_m1958 (FBServices_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
