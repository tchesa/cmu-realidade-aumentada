﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// CFX3_Demo_Translate
struct  CFX3_Demo_Translate_t317  : public MonoBehaviour_t18
{
	// System.Single CFX3_Demo_Translate::speed
	float ___speed_1;
	// UnityEngine.Vector3 CFX3_Demo_Translate::rotation
	Vector3_t6  ___rotation_2;
	// UnityEngine.Vector3 CFX3_Demo_Translate::axis
	Vector3_t6  ___axis_3;
	// System.Boolean CFX3_Demo_Translate::gravity
	bool ___gravity_4;
	// UnityEngine.Vector3 CFX3_Demo_Translate::dir
	Vector3_t6  ___dir_5;
};
