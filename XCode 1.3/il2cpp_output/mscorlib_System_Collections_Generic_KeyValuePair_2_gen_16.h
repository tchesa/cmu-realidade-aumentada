﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GCScore
struct GCScore_t117;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>
struct  KeyValuePair_2_t2723 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>::value
	GCScore_t117 * ___value_1;
};
