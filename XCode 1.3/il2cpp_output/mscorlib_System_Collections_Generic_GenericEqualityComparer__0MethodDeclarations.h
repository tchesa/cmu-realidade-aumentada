﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2572;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m15438_gshared (GenericEqualityComparer_1_t2572 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m15438(__this, method) (( void (*) (GenericEqualityComparer_1_t2572 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m15438_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m30138_gshared (GenericEqualityComparer_1_t2572 * __this, DateTimeOffset_t2423  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m30138(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2572 *, DateTimeOffset_t2423 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m30138_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m30139_gshared (GenericEqualityComparer_1_t2572 * __this, DateTimeOffset_t2423  ___x, DateTimeOffset_t2423  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m30139(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2572 *, DateTimeOffset_t2423 , DateTimeOffset_t2423 , const MethodInfo*))GenericEqualityComparer_1_Equals_m30139_gshared)(__this, ___x, ___y, method)
