﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSStoreProductView
struct IOSStoreProductView_t134;
// System.String[]
struct StringU5BU5D_t260;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSStoreProductView::.ctor()
extern "C" void IOSStoreProductView__ctor_m767 (IOSStoreProductView_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::.ctor(System.String[])
extern "C" void IOSStoreProductView__ctor_m768 (IOSStoreProductView_t134 * __this, StringU5BU5D_t260* ___ids, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::_createProductView(System.Int32,System.String)
extern "C" void IOSStoreProductView__createProductView_m769 (Object_t * __this /* static, unused */, int32_t ___viewId, String_t* ___productsId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::_showProductView(System.Int32)
extern "C" void IOSStoreProductView__showProductView_m770 (Object_t * __this /* static, unused */, int32_t ___viewId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::addProductId(System.String)
extern "C" void IOSStoreProductView_addProductId_m771 (IOSStoreProductView_t134 * __this, String_t* ___productId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::Load()
extern "C" void IOSStoreProductView_Load_m772 (IOSStoreProductView_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::Show()
extern "C" void IOSStoreProductView_Show_m773 (IOSStoreProductView_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSStoreProductView::get_id()
extern "C" int32_t IOSStoreProductView_get_id_m774 (IOSStoreProductView_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::OnProductViewAppeard()
extern "C" void IOSStoreProductView_OnProductViewAppeard_m775 (IOSStoreProductView_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::OnProductViewDismissed()
extern "C" void IOSStoreProductView_OnProductViewDismissed_m776 (IOSStoreProductView_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::OnContentLoaded()
extern "C" void IOSStoreProductView_OnContentLoaded_m777 (IOSStoreProductView_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::OnContentLoadFailed()
extern "C" void IOSStoreProductView_OnContentLoadFailed_m778 (IOSStoreProductView_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::SetId(System.Int32)
extern "C" void IOSStoreProductView_SetId_m779 (IOSStoreProductView_t134 * __this, int32_t ___viewId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
