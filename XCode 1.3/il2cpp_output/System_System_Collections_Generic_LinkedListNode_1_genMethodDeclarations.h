﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t1151;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t987;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C" void LinkedListNode_1__ctor_m23778_gshared (LinkedListNode_1_t1151 * __this, LinkedList_1_t987 * ___list, int32_t ___value, const MethodInfo* method);
#define LinkedListNode_1__ctor_m23778(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t1151 *, LinkedList_1_t987 *, int32_t, const MethodInfo*))LinkedListNode_1__ctor_m23778_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedListNode_1__ctor_m23779_gshared (LinkedListNode_1_t1151 * __this, LinkedList_1_t987 * ___list, int32_t ___value, LinkedListNode_1_t1151 * ___previousNode, LinkedListNode_1_t1151 * ___nextNode, const MethodInfo* method);
#define LinkedListNode_1__ctor_m23779(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t1151 *, LinkedList_1_t987 *, int32_t, LinkedListNode_1_t1151 *, LinkedListNode_1_t1151 *, const MethodInfo*))LinkedListNode_1__ctor_m23779_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::Detach()
extern "C" void LinkedListNode_1_Detach_m23780_gshared (LinkedListNode_1_t1151 * __this, const MethodInfo* method);
#define LinkedListNode_1_Detach_m23780(__this, method) (( void (*) (LinkedListNode_1_t1151 *, const MethodInfo*))LinkedListNode_1_Detach_m23780_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_List()
extern "C" LinkedList_1_t987 * LinkedListNode_1_get_List_m23781_gshared (LinkedListNode_1_t1151 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_List_m23781(__this, method) (( LinkedList_1_t987 * (*) (LinkedListNode_1_t1151 *, const MethodInfo*))LinkedListNode_1_get_List_m23781_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Next()
extern "C" LinkedListNode_1_t1151 * LinkedListNode_1_get_Next_m6740_gshared (LinkedListNode_1_t1151 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Next_m6740(__this, method) (( LinkedListNode_1_t1151 * (*) (LinkedListNode_1_t1151 *, const MethodInfo*))LinkedListNode_1_get_Next_m6740_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Value()
extern "C" int32_t LinkedListNode_1_get_Value_m6581_gshared (LinkedListNode_1_t1151 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Value_m6581(__this, method) (( int32_t (*) (LinkedListNode_1_t1151 *, const MethodInfo*))LinkedListNode_1_get_Value_m6581_gshared)(__this, method)
