﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<IOSDialogResult>
struct Action_1_t165;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_IOSDialogResult.h"

// System.Void System.Action`1<IOSDialogResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m2268_gshared (Action_1_t165 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m2268(__this, ___object, ___method, method) (( void (*) (Action_1_t165 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2268_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<IOSDialogResult>::Invoke(T)
extern "C" void Action_1_Invoke_m2271_gshared (Action_1_t165 * __this, int32_t ___obj, const MethodInfo* method);
#define Action_1_Invoke_m2271(__this, ___obj, method) (( void (*) (Action_1_t165 *, int32_t, const MethodInfo*))Action_1_Invoke_m2271_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<IOSDialogResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m17661_gshared (Action_1_t165 * __this, int32_t ___obj, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m17661(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t165 *, int32_t, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m17661_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<IOSDialogResult>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m17662_gshared (Action_1_t165 * __this, Object_t * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m17662(__this, ___result, method) (( void (*) (Action_1_t165 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m17662_gshared)(__this, ___result, method)
