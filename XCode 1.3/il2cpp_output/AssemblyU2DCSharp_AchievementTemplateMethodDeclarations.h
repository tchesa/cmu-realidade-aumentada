﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementTemplate
struct AchievementTemplate_t115;

#include "codegen/il2cpp-codegen.h"

// System.Void AchievementTemplate::.ctor()
extern "C" void AchievementTemplate__ctor_m682 (AchievementTemplate_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AchievementTemplate::get_progress()
extern "C" float AchievementTemplate_get_progress_m683 (AchievementTemplate_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementTemplate::set_progress(System.Single)
extern "C" void AchievementTemplate_set_progress_m684 (AchievementTemplate_t115 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
