﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativeFeaturesPreview
struct IOSNativeFeaturesPreview_t212;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNativeFeaturesPreview::.ctor()
extern "C" void IOSNativeFeaturesPreview__ctor_m1187 (IOSNativeFeaturesPreview_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeFeaturesPreview::.cctor()
extern "C" void IOSNativeFeaturesPreview__cctor_m1188 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeFeaturesPreview::Awake()
extern "C" void IOSNativeFeaturesPreview_Awake_m1189 (IOSNativeFeaturesPreview_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeFeaturesPreview::OnGUI()
extern "C" void IOSNativeFeaturesPreview_OnGUI_m1190 (IOSNativeFeaturesPreview_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
