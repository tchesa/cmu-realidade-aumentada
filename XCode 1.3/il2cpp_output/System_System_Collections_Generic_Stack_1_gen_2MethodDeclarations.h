﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
#define Stack_1__ctor_m20961(__this, method) (( void (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1__ctor_m19746_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m20962(__this, method) (( bool (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m19747_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m20963(__this, method) (( Object_t * (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m19748_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m20964(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2938 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m19749_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20965(__this, method) (( Object_t* (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19750_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m20966(__this, method) (( Object_t * (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m19751_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m20967(__this, method) (( List_1_t724 * (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1_Peek_m19752_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m20968(__this, method) (( List_1_t724 * (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1_Pop_m19753_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m20969(__this, ___t, method) (( void (*) (Stack_1_t2938 *, List_1_t724 *, const MethodInfo*))Stack_1_Push_m19754_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m20970(__this, method) (( int32_t (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1_get_Count_m19755_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
#define Stack_1_GetEnumerator_m20971(__this, method) (( Enumerator_t3673  (*) (Stack_1_t2938 *, const MethodInfo*))Stack_1_GetEnumerator_m19756_gshared)(__this, method)
