﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_42.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m26645_gshared (KeyValuePair_2_t3292 * __this, Object_t * ___key, ProfileData_t1064  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m26645(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3292 *, Object_t *, ProfileData_t1064 , const MethodInfo*))KeyValuePair_2__ctor_m26645_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m26646_gshared (KeyValuePair_2_t3292 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m26646(__this, method) (( Object_t * (*) (KeyValuePair_2_t3292 *, const MethodInfo*))KeyValuePair_2_get_Key_m26646_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m26647_gshared (KeyValuePair_2_t3292 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m26647(__this, ___value, method) (( void (*) (KeyValuePair_2_t3292 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m26647_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" ProfileData_t1064  KeyValuePair_2_get_Value_m26648_gshared (KeyValuePair_2_t3292 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m26648(__this, method) (( ProfileData_t1064  (*) (KeyValuePair_2_t3292 *, const MethodInfo*))KeyValuePair_2_get_Value_m26648_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m26649_gshared (KeyValuePair_2_t3292 * __this, ProfileData_t1064  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m26649(__this, ___value, method) (( void (*) (KeyValuePair_2_t3292 *, ProfileData_t1064 , const MethodInfo*))KeyValuePair_2_set_Value_m26649_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m26650_gshared (KeyValuePair_2_t3292 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m26650(__this, method) (( String_t* (*) (KeyValuePair_2_t3292 *, const MethodInfo*))KeyValuePair_2_ToString_m26650_gshared)(__this, method)
