﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t3546;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t3766;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3765;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t2541;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t2539;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2537;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t3550;
// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t3553;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_70.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void List_1__ctor_m29678_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1__ctor_m29678(__this, method) (( void (*) (List_1_t3546 *, const MethodInfo*))List_1__ctor_m29678_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m29679_gshared (List_1_t3546 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m29679(__this, ___collection, method) (( void (*) (List_1_t3546 *, Object_t*, const MethodInfo*))List_1__ctor_m29679_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m29680_gshared (List_1_t3546 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m29680(__this, ___capacity, method) (( void (*) (List_1_t3546 *, int32_t, const MethodInfo*))List_1__ctor_m29680_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C" void List_1__cctor_m29681_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m29681(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m29681_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29682_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29682(__this, method) (( Object_t* (*) (List_1_t3546 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29682_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m29683_gshared (List_1_t3546 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m29683(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3546 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m29683_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m29684_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m29684(__this, method) (( Object_t * (*) (List_1_t3546 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m29684_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m29685_gshared (List_1_t3546 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m29685(__this, ___item, method) (( int32_t (*) (List_1_t3546 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m29685_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m29686_gshared (List_1_t3546 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m29686(__this, ___item, method) (( bool (*) (List_1_t3546 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m29686_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m29687_gshared (List_1_t3546 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m29687(__this, ___item, method) (( int32_t (*) (List_1_t3546 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m29687_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m29688_gshared (List_1_t3546 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m29688(__this, ___index, ___item, method) (( void (*) (List_1_t3546 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m29688_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m29689_gshared (List_1_t3546 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m29689(__this, ___item, method) (( void (*) (List_1_t3546 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m29689_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29690_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29690(__this, method) (( bool (*) (List_1_t3546 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29690_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m29691_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m29691(__this, method) (( bool (*) (List_1_t3546 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m29691_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m29692_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m29692(__this, method) (( Object_t * (*) (List_1_t3546 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m29692_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m29693_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m29693(__this, method) (( bool (*) (List_1_t3546 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m29693_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m29694_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m29694(__this, method) (( bool (*) (List_1_t3546 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m29694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m29695_gshared (List_1_t3546 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m29695(__this, ___index, method) (( Object_t * (*) (List_1_t3546 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m29695_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m29696_gshared (List_1_t3546 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m29696(__this, ___index, ___value, method) (( void (*) (List_1_t3546 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m29696_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void List_1_Add_m29697_gshared (List_1_t3546 * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define List_1_Add_m29697(__this, ___item, method) (( void (*) (List_1_t3546 *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))List_1_Add_m29697_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m29698_gshared (List_1_t3546 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m29698(__this, ___newCount, method) (( void (*) (List_1_t3546 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m29698_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m29699_gshared (List_1_t3546 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m29699(__this, ___collection, method) (( void (*) (List_1_t3546 *, Object_t*, const MethodInfo*))List_1_AddCollection_m29699_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m29700_gshared (List_1_t3546 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m29700(__this, ___enumerable, method) (( void (*) (List_1_t3546 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m29700_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m29701_gshared (List_1_t3546 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m29701(__this, ___collection, method) (( void (*) (List_1_t3546 *, Object_t*, const MethodInfo*))List_1_AddRange_m29701_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2539 * List_1_AsReadOnly_m29702_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m29702(__this, method) (( ReadOnlyCollection_1_t2539 * (*) (List_1_t3546 *, const MethodInfo*))List_1_AsReadOnly_m29702_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void List_1_Clear_m29703_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_Clear_m29703(__this, method) (( void (*) (List_1_t3546 *, const MethodInfo*))List_1_Clear_m29703_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool List_1_Contains_m29704_gshared (List_1_t3546 * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define List_1_Contains_m29704(__this, ___item, method) (( bool (*) (List_1_t3546 *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))List_1_Contains_m29704_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m29705_gshared (List_1_t3546 * __this, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m29705(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3546 *, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, const MethodInfo*))List_1_CopyTo_m29705_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Find(System.Predicate`1<T>)
extern "C" CustomAttributeTypedArgument_t2101  List_1_Find_m29706_gshared (List_1_t3546 * __this, Predicate_1_t3550 * ___match, const MethodInfo* method);
#define List_1_Find_m29706(__this, ___match, method) (( CustomAttributeTypedArgument_t2101  (*) (List_1_t3546 *, Predicate_1_t3550 *, const MethodInfo*))List_1_Find_m29706_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m29707_gshared (Object_t * __this /* static, unused */, Predicate_1_t3550 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m29707(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3550 *, const MethodInfo*))List_1_CheckMatch_m29707_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m29708_gshared (List_1_t3546 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3550 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m29708(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3546 *, int32_t, int32_t, Predicate_1_t3550 *, const MethodInfo*))List_1_GetIndex_m29708_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Enumerator_t3547  List_1_GetEnumerator_m29709_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m29709(__this, method) (( Enumerator_t3547  (*) (List_1_t3546 *, const MethodInfo*))List_1_GetEnumerator_m29709_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m29710_gshared (List_1_t3546 * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define List_1_IndexOf_m29710(__this, ___item, method) (( int32_t (*) (List_1_t3546 *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))List_1_IndexOf_m29710_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m29711_gshared (List_1_t3546 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m29711(__this, ___start, ___delta, method) (( void (*) (List_1_t3546 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m29711_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m29712_gshared (List_1_t3546 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m29712(__this, ___index, method) (( void (*) (List_1_t3546 *, int32_t, const MethodInfo*))List_1_CheckIndex_m29712_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m29713_gshared (List_1_t3546 * __this, int32_t ___index, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define List_1_Insert_m29713(__this, ___index, ___item, method) (( void (*) (List_1_t3546 *, int32_t, CustomAttributeTypedArgument_t2101 , const MethodInfo*))List_1_Insert_m29713_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m29714_gshared (List_1_t3546 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m29714(__this, ___collection, method) (( void (*) (List_1_t3546 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m29714_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool List_1_Remove_m29715_gshared (List_1_t3546 * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define List_1_Remove_m29715(__this, ___item, method) (( bool (*) (List_1_t3546 *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))List_1_Remove_m29715_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m29716_gshared (List_1_t3546 * __this, Predicate_1_t3550 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m29716(__this, ___match, method) (( int32_t (*) (List_1_t3546 *, Predicate_1_t3550 *, const MethodInfo*))List_1_RemoveAll_m29716_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m29717_gshared (List_1_t3546 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m29717(__this, ___index, method) (( void (*) (List_1_t3546 *, int32_t, const MethodInfo*))List_1_RemoveAt_m29717_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Reverse()
extern "C" void List_1_Reverse_m29718_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_Reverse_m29718(__this, method) (( void (*) (List_1_t3546 *, const MethodInfo*))List_1_Reverse_m29718_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort()
extern "C" void List_1_Sort_m29719_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_Sort_m29719(__this, method) (( void (*) (List_1_t3546 *, const MethodInfo*))List_1_Sort_m29719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m29720_gshared (List_1_t3546 * __this, Comparison_1_t3553 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m29720(__this, ___comparison, method) (( void (*) (List_1_t3546 *, Comparison_1_t3553 *, const MethodInfo*))List_1_Sort_m29720_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C" CustomAttributeTypedArgumentU5BU5D_t2537* List_1_ToArray_m29721_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_ToArray_m29721(__this, method) (( CustomAttributeTypedArgumentU5BU5D_t2537* (*) (List_1_t3546 *, const MethodInfo*))List_1_ToArray_m29721_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::TrimExcess()
extern "C" void List_1_TrimExcess_m29722_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m29722(__this, method) (( void (*) (List_1_t3546 *, const MethodInfo*))List_1_TrimExcess_m29722_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m29723_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m29723(__this, method) (( int32_t (*) (List_1_t3546 *, const MethodInfo*))List_1_get_Capacity_m29723_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m29724_gshared (List_1_t3546 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m29724(__this, ___value, method) (( void (*) (List_1_t3546 *, int32_t, const MethodInfo*))List_1_set_Capacity_m29724_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m29725_gshared (List_1_t3546 * __this, const MethodInfo* method);
#define List_1_get_Count_m29725(__this, method) (( int32_t (*) (List_1_t3546 *, const MethodInfo*))List_1_get_Count_m29725_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t2101  List_1_get_Item_m29726_gshared (List_1_t3546 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m29726(__this, ___index, method) (( CustomAttributeTypedArgument_t2101  (*) (List_1_t3546 *, int32_t, const MethodInfo*))List_1_get_Item_m29726_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m29727_gshared (List_1_t3546 * __this, int32_t ___index, CustomAttributeTypedArgument_t2101  ___value, const MethodInfo* method);
#define List_1_set_Item_m29727(__this, ___index, ___value, method) (( void (*) (List_1_t3546 *, int32_t, CustomAttributeTypedArgument_t2101 , const MethodInfo*))List_1_set_Item_m29727_gshared)(__this, ___index, ___value, method)
