﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_PlayerScoreLoadedResult
struct ISN_PlayerScoreLoadedResult_t116;
// GCScore
struct GCScore_t117;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_PlayerScoreLoadedResult::.ctor()
extern "C" void ISN_PlayerScoreLoadedResult__ctor_m669 (ISN_PlayerScoreLoadedResult_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_PlayerScoreLoadedResult::.ctor(GCScore)
extern "C" void ISN_PlayerScoreLoadedResult__ctor_m670 (ISN_PlayerScoreLoadedResult_t116 * __this, GCScore_t117 * ___score, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GCScore ISN_PlayerScoreLoadedResult::get_loadedScore()
extern "C" GCScore_t117 * ISN_PlayerScoreLoadedResult_get_loadedScore_m671 (ISN_PlayerScoreLoadedResult_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
