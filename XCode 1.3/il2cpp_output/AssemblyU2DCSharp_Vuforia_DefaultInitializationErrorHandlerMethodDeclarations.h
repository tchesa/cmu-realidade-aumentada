﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t404;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"

// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern "C" void DefaultInitializationErrorHandler__ctor_m2030 (DefaultInitializationErrorHandler_t404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern "C" void DefaultInitializationErrorHandler_Awake_m2031 (DefaultInitializationErrorHandler_t404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern "C" void DefaultInitializationErrorHandler_OnGUI_m2032 (DefaultInitializationErrorHandler_t404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern "C" void DefaultInitializationErrorHandler_OnDestroy_m2033 (DefaultInitializationErrorHandler_t404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern "C" void DefaultInitializationErrorHandler_DrawWindowContent_m2034 (DefaultInitializationErrorHandler_t404 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_SetErrorCode_m2035 (DefaultInitializationErrorHandler_t404 * __this, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C" void DefaultInitializationErrorHandler_SetErrorOccurred_m2036 (DefaultInitializationErrorHandler_t404 * __this, bool ___errorOccurred, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2037 (DefaultInitializationErrorHandler_t404 * __this, int32_t ___initError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
