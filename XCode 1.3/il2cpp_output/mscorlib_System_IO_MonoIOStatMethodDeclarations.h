﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void MonoIOStat_t2020_marshal(const MonoIOStat_t2020& unmarshaled, MonoIOStat_t2020_marshaled& marshaled);
extern "C" void MonoIOStat_t2020_marshal_back(const MonoIOStat_t2020_marshaled& marshaled, MonoIOStat_t2020& unmarshaled);
extern "C" void MonoIOStat_t2020_marshal_cleanup(MonoIOStat_t2020_marshaled& marshaled);
