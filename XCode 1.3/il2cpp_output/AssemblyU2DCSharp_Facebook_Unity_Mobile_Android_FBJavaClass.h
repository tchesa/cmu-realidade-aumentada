﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJavaClass
struct AndroidJavaClass_t256;

#include "mscorlib_System_Object.h"

// Facebook.Unity.Mobile.Android.FBJavaClass
struct  FBJavaClass_t257  : public Object_t
{
	// Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJavaClass Facebook.Unity.Mobile.Android.FBJavaClass::facebookJavaClass
	AndroidJavaClass_t256 * ___facebookJavaClass_1;
};
