﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.Canvas.CanvasFacebook
struct CanvasFacebook_t226;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_MethodCall_1_gen_2.h"

// Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IGroupCreateResult>
struct  CanvasUIMethodCall_1_t526  : public MethodCall_1_t2768
{
	// Facebook.Unity.Canvas.CanvasFacebook Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IGroupCreateResult>::canvasImpl
	CanvasFacebook_t226 * ___canvasImpl_4;
	// System.String Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IGroupCreateResult>::callbackMethod
	String_t* ___callbackMethod_5;
};
