﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSHelper
struct JSHelper_t190;
// System.String
struct String_t;
// UnionAssets.FLE.CEvent
struct CEvent_t74;

#include "codegen/il2cpp-codegen.h"

// System.Void JSHelper::.ctor()
extern "C" void JSHelper__ctor_m1079 (JSHelper_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::InitGameCneter()
extern "C" void JSHelper_InitGameCneter_m1080 (JSHelper_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::SubmitScore(System.Int32)
extern "C" void JSHelper_SubmitScore_m1081 (JSHelper_t190 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::SubmitAchievement(System.String)
extern "C" void JSHelper_SubmitAchievement_m1082 (JSHelper_t190 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::OnAchievementsLoaded()
extern "C" void JSHelper_OnAchievementsLoaded_m1083 (JSHelper_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::OnAchievementsReset()
extern "C" void JSHelper_OnAchievementsReset_m1084 (JSHelper_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::OnAchievementProgress(UnionAssets.FLE.CEvent)
extern "C" void JSHelper_OnAchievementProgress_m1085 (JSHelper_t190 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::OnLeaderboardScoreLoaded(UnionAssets.FLE.CEvent)
extern "C" void JSHelper_OnLeaderboardScoreLoaded_m1086 (JSHelper_t190 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::OnAuth(UnionAssets.FLE.CEvent)
extern "C" void JSHelper_OnAuth_m1087 (JSHelper_t190 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
