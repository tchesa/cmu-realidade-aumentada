﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m18746(__this, ___dictionary, method) (( void (*) (Enumerator_t570 *, Dictionary_2_t320 *, const MethodInfo*))Enumerator__ctor_m18642_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18747(__this, method) (( Object_t * (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18643_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m18748(__this, method) (( void (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m18644_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18749(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18645_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18750(__this, method) (( Object_t * (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18646_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18751(__this, method) (( Object_t * (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18647_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::MoveNext()
#define Enumerator_MoveNext_m2573(__this, method) (( bool (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_MoveNext_m18648_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_Current()
#define Enumerator_get_Current_m2570(__this, method) (( KeyValuePair_2_t569  (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_get_Current_m18649_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18752(__this, method) (( String_t* (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_get_CurrentKey_m18650_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18753(__this, method) (( float (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_get_CurrentValue_m18651_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::Reset()
#define Enumerator_Reset_m18754(__this, method) (( void (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_Reset_m18652_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::VerifyState()
#define Enumerator_VerifyState_m18755(__this, method) (( void (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_VerifyState_m18653_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18756(__this, method) (( void (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_VerifyCurrent_m18654_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::Dispose()
#define Enumerator_Dispose_m18757(__this, method) (( void (*) (Enumerator_t570 *, const MethodInfo*))Enumerator_Dispose_m18655_gshared)(__this, method)
