﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3149;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2606;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t3703;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct IEnumerator_1_t3704;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt16>
struct KeyCollection_t3154;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt16>
struct ValueCollection_t3158;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor()
extern "C" void Dictionary_2__ctor_m24140_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m24140(__this, method) (( void (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2__ctor_m24140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m24142_gshared (Dictionary_2_t3149 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m24142(__this, ___comparer, method) (( void (*) (Dictionary_2_t3149 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m24142_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m24144_gshared (Dictionary_2_t3149 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m24144(__this, ___capacity, method) (( void (*) (Dictionary_2_t3149 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m24144_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m24146_gshared (Dictionary_2_t3149 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m24146(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3149 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m24146_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m24148_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m24148(__this, method) (( Object_t * (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m24148_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m24150_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m24150(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m24150_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m24152_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m24152(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3149 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m24152_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m24154_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m24154(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3149 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m24154_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m24156_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m24156(__this, ___key, method) (( bool (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m24156_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m24158_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m24158(__this, ___key, method) (( void (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m24158_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24160_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24160(__this, method) (( bool (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24160_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24162_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24162(__this, method) (( Object_t * (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24162_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24164_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24164(__this, method) (( bool (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24166_gshared (Dictionary_2_t3149 * __this, KeyValuePair_2_t3151  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24166(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3149 *, KeyValuePair_2_t3151 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24166_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24168_gshared (Dictionary_2_t3149 * __this, KeyValuePair_2_t3151  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24168(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3149 *, KeyValuePair_2_t3151 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24168_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24170_gshared (Dictionary_2_t3149 * __this, KeyValuePair_2U5BU5D_t3703* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24170(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3149 *, KeyValuePair_2U5BU5D_t3703*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24170_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24172_gshared (Dictionary_2_t3149 * __this, KeyValuePair_2_t3151  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24172(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3149 *, KeyValuePair_2_t3151 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24172_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m24174_gshared (Dictionary_2_t3149 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m24174(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3149 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m24174_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24176_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24176(__this, method) (( Object_t * (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24176_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24178_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24178(__this, method) (( Object_t* (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24178_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24180_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24180(__this, method) (( Object_t * (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24180_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m24182_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m24182(__this, method) (( int32_t (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_get_Count_m24182_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Item(TKey)
extern "C" uint16_t Dictionary_2_get_Item_m24184_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m24184(__this, ___key, method) (( uint16_t (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m24184_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m24186_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m24186(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3149 *, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_set_Item_m24186_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m24188_gshared (Dictionary_2_t3149 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m24188(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3149 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m24188_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m24190_gshared (Dictionary_2_t3149 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m24190(__this, ___size, method) (( void (*) (Dictionary_2_t3149 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m24190_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m24192_gshared (Dictionary_2_t3149 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m24192(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3149 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m24192_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3151  Dictionary_2_make_pair_m24194_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m24194(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3151  (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_make_pair_m24194_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m24196_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m24196(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_key_m24196_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::pick_value(TKey,TValue)
extern "C" uint16_t Dictionary_2_pick_value_m24198_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m24198(__this /* static, unused */, ___key, ___value, method) (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_value_m24198_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m24200_gshared (Dictionary_2_t3149 * __this, KeyValuePair_2U5BU5D_t3703* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m24200(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3149 *, KeyValuePair_2U5BU5D_t3703*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m24200_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Resize()
extern "C" void Dictionary_2_Resize_m24202_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m24202(__this, method) (( void (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_Resize_m24202_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m24204_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m24204(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3149 *, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_Add_m24204_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Clear()
extern "C" void Dictionary_2_Clear_m24206_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m24206(__this, method) (( void (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_Clear_m24206_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m24208_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m24208(__this, ___key, method) (( bool (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m24208_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m24210_gshared (Dictionary_2_t3149 * __this, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m24210(__this, ___value, method) (( bool (*) (Dictionary_2_t3149 *, uint16_t, const MethodInfo*))Dictionary_2_ContainsValue_m24210_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m24212_gshared (Dictionary_2_t3149 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m24212(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3149 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m24212_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m24214_gshared (Dictionary_2_t3149 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m24214(__this, ___sender, method) (( void (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m24214_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m24216_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m24216(__this, ___key, method) (( bool (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m24216_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m24218_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, uint16_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m24218(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3149 *, Object_t *, uint16_t*, const MethodInfo*))Dictionary_2_TryGetValue_m24218_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Keys()
extern "C" KeyCollection_t3154 * Dictionary_2_get_Keys_m24220_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m24220(__this, method) (( KeyCollection_t3154 * (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_get_Keys_m24220_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Values()
extern "C" ValueCollection_t3158 * Dictionary_2_get_Values_m24222_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m24222(__this, method) (( ValueCollection_t3158 * (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_get_Values_m24222_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m24224_gshared (Dictionary_2_t3149 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m24224(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m24224_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ToTValue(System.Object)
extern "C" uint16_t Dictionary_2_ToTValue_m24226_gshared (Dictionary_2_t3149 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m24226(__this, ___value, method) (( uint16_t (*) (Dictionary_2_t3149 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m24226_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m24228_gshared (Dictionary_2_t3149 * __this, KeyValuePair_2_t3151  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m24228(__this, ___pair, method) (( bool (*) (Dictionary_2_t3149 *, KeyValuePair_2_t3151 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m24228_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::GetEnumerator()
extern "C" Enumerator_t3156  Dictionary_2_GetEnumerator_m24230_gshared (Dictionary_2_t3149 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m24230(__this, method) (( Enumerator_t3156  (*) (Dictionary_2_t3149 *, const MethodInfo*))Dictionary_2_GetEnumerator_m24230_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m24232_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m24232(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m24232_gshared)(__this /* static, unused */, ___key, ___value, method)
