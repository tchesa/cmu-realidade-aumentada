﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.CallbackManager
struct CallbackManager_t224;
// Facebook.Unity.IInternalResult
struct IInternalResult_t463;
// System.Object
struct Object_t;
// Facebook.Unity.IResult
struct IResult_t464;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.CallbackManager::.ctor()
extern "C" void CallbackManager__ctor_m1224 (CallbackManager_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.CallbackManager::OnFacebookResponse(Facebook.Unity.IInternalResult)
extern "C" void CallbackManager_OnFacebookResponse_m1225 (CallbackManager_t224 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.CallbackManager::CallCallback(System.Object,Facebook.Unity.IResult)
extern "C" void CallbackManager_CallCallback_m1226 (Object_t * __this /* static, unused */, Object_t * ___callback, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
