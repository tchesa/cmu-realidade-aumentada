﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Canvas.CanvasFacebookLoader
struct CanvasFacebookLoader_t230;
// Facebook.Unity.FacebookGameObject
struct FacebookGameObject_t229;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Canvas.CanvasFacebookLoader::.ctor()
extern "C" void CanvasFacebookLoader__ctor_m1268 (CanvasFacebookLoader_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.FacebookGameObject Facebook.Unity.Canvas.CanvasFacebookLoader::get_FBGameObject()
extern "C" FacebookGameObject_t229 * CanvasFacebookLoader_get_FBGameObject_m1269 (CanvasFacebookLoader_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
