﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m24946(__this, ___host, method) (( void (*) (Enumerator_t1162 *, Dictionary_2_t1023 *, const MethodInfo*))Enumerator__ctor_m16177_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24947(__this, method) (( Object_t * (*) (Enumerator_t1162 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16178_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m24948(__this, method) (( void (*) (Enumerator_t1162 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m6628(__this, method) (( void (*) (Enumerator_t1162 *, const MethodInfo*))Enumerator_Dispose_m16180_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m6627(__this, method) (( bool (*) (Enumerator_t1162 *, const MethodInfo*))Enumerator_MoveNext_m16181_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m6625(__this, method) (( List_1_t1022 * (*) (Enumerator_t1162 *, const MethodInfo*))Enumerator_get_Current_m16182_gshared)(__this, method)
