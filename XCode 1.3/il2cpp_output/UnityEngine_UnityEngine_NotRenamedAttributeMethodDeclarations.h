﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NotRenamedAttribute
struct NotRenamedAttribute_t1424;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.NotRenamedAttribute::.ctor()
extern "C" void NotRenamedAttribute__ctor_m8044 (NotRenamedAttribute_t1424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
