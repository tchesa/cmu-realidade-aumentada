﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t3596;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m30167_gshared (DefaultComparer_t3596 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m30167(__this, method) (( void (*) (DefaultComparer_t3596 *, const MethodInfo*))DefaultComparer__ctor_m30167_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m30168_gshared (DefaultComparer_t3596 * __this, Guid_t61  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m30168(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3596 *, Guid_t61 , const MethodInfo*))DefaultComparer_GetHashCode_m30168_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m30169_gshared (DefaultComparer_t3596 * __this, Guid_t61  ___x, Guid_t61  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m30169(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3596 *, Guid_t61 , Guid_t61 , const MethodInfo*))DefaultComparer_Equals_m30169_gshared)(__this, ___x, ___y, method)
