﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardCustomGUIExample
struct LeaderboardCustomGUIExample_t189;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardCustomGUIExample::.ctor()
extern "C" void LeaderboardCustomGUIExample__ctor_m1074 (LeaderboardCustomGUIExample_t189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardCustomGUIExample::Awake()
extern "C" void LeaderboardCustomGUIExample_Awake_m1075 (LeaderboardCustomGUIExample_t189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardCustomGUIExample::OnGUI()
extern "C" void LeaderboardCustomGUIExample_OnGUI_m1076 (LeaderboardCustomGUIExample_t189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardCustomGUIExample::OnScoresListLoaded(ISN_Result)
extern "C" void LeaderboardCustomGUIExample_OnScoresListLoaded_m1077 (LeaderboardCustomGUIExample_t189 * __this, ISN_Result_t114 * ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardCustomGUIExample::OnAuthFinished(ISN_Result)
extern "C" void LeaderboardCustomGUIExample_OnAuthFinished_m1078 (LeaderboardCustomGUIExample_t189 * __this, ISN_Result_t114 * ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
