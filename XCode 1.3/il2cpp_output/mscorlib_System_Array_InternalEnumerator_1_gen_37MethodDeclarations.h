﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_37.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21529_gshared (InternalEnumerator_1_t2977 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21529(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2977 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21529_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21530_gshared (InternalEnumerator_1_t2977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21530(__this, method) (( void (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21530_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21531_gshared (InternalEnumerator_1_t2977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21531(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21531_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21532_gshared (InternalEnumerator_1_t2977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21532(__this, method) (( void (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21532_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21533_gshared (InternalEnumerator_1_t2977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21533(__this, method) (( bool (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21533_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m21534_gshared (InternalEnumerator_1_t2977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21534(__this, method) (( int32_t (*) (InternalEnumerator_1_t2977 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21534_gshared)(__this, method)
