﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t337;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_51.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18972_gshared (Enumerator_t2818 * __this, Dictionary_2_t337 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m18972(__this, ___host, method) (( void (*) (Enumerator_t2818 *, Dictionary_2_t337 *, const MethodInfo*))Enumerator__ctor_m18972_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18973_gshared (Enumerator_t2818 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18973(__this, method) (( Object_t * (*) (Enumerator_t2818 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18973_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18974_gshared (Enumerator_t2818 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m18974(__this, method) (( void (*) (Enumerator_t2818 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m18974_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m18975_gshared (Enumerator_t2818 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18975(__this, method) (( void (*) (Enumerator_t2818 *, const MethodInfo*))Enumerator_Dispose_m18975_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18976_gshared (Enumerator_t2818 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18976(__this, method) (( bool (*) (Enumerator_t2818 *, const MethodInfo*))Enumerator_MoveNext_m18976_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m18977_gshared (Enumerator_t2818 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18977(__this, method) (( int32_t (*) (Enumerator_t2818 *, const MethodInfo*))Enumerator_get_Current_m18977_gshared)(__this, method)
