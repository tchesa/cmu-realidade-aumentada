﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Space.h"

// CFX_AutoRotate
struct  CFX_AutoRotate_t327  : public MonoBehaviour_t18
{
	// UnityEngine.Vector3 CFX_AutoRotate::rotation
	Vector3_t6  ___rotation_1;
	// UnityEngine.Space CFX_AutoRotate::space
	int32_t ___space_2;
};
