﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterPlayerTemplate
struct GameCenterPlayerTemplate_t107;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t33;

#include "codegen/il2cpp-codegen.h"

// System.Void GameCenterPlayerTemplate::.ctor(System.String,System.String,System.String)
extern "C" void GameCenterPlayerTemplate__ctor_m706 (GameCenterPlayerTemplate_t107 * __this, String_t* ___pId, String_t* ___pName, String_t* ___pAlias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterPlayerTemplate::SetAvatar(System.String)
extern "C" void GameCenterPlayerTemplate_SetAvatar_m707 (GameCenterPlayerTemplate_t107 * __this, String_t* ___base64String, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameCenterPlayerTemplate::get_PlayerId()
extern "C" String_t* GameCenterPlayerTemplate_get_PlayerId_m708 (GameCenterPlayerTemplate_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameCenterPlayerTemplate::get_Alias()
extern "C" String_t* GameCenterPlayerTemplate_get_Alias_m709 (GameCenterPlayerTemplate_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameCenterPlayerTemplate::get_DisplayName()
extern "C" String_t* GameCenterPlayerTemplate_get_DisplayName_m710 (GameCenterPlayerTemplate_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GameCenterPlayerTemplate::get_Avatar()
extern "C" Texture2D_t33 * GameCenterPlayerTemplate_get_Avatar_m711 (GameCenterPlayerTemplate_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
