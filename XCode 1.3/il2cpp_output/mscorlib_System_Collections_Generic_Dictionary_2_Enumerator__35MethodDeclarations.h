﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m24932(__this, ___dictionary, method) (( void (*) (Enumerator_t3193 *, Dictionary_2_t1023 *, const MethodInfo*))Enumerator__ctor_m16145_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24933(__this, method) (( Object_t * (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16146_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m24934(__this, method) (( void (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16147_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24935(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16148_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24936(__this, method) (( Object_t * (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16149_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24937(__this, method) (( Object_t * (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m24938(__this, method) (( bool (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_MoveNext_m16151_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m24939(__this, method) (( KeyValuePair_2_t3192  (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_get_Current_m16152_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24940(__this, method) (( String_t* (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_get_CurrentKey_m16153_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24941(__this, method) (( List_1_t1022 * (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_get_CurrentValue_m16154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Reset()
#define Enumerator_Reset_m24942(__this, method) (( void (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_Reset_m16155_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::VerifyState()
#define Enumerator_VerifyState_m24943(__this, method) (( void (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_VerifyState_m16156_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24944(__this, method) (( void (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_VerifyCurrent_m16157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m24945(__this, method) (( void (*) (Enumerator_t3193 *, const MethodInfo*))Enumerator_Dispose_m16158_gshared)(__this, method)
