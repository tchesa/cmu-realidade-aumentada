﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t32;
// UnityEngine.Transform
struct Transform_t25;
// System.Object
struct Object_t;
// AnimationController
struct AnimationController_t357;

#include "mscorlib_System_Object.h"

// AnimationController/<IPlay>c__IteratorD
struct  U3CIPlayU3Ec__IteratorD_t356  : public Object_t
{
	// UnityEngine.Transform[] AnimationController/<IPlay>c__IteratorD::<$s_58>__0
	TransformU5BU5D_t32* ___U3CU24s_58U3E__0_0;
	// System.Int32 AnimationController/<IPlay>c__IteratorD::<$s_59>__1
	int32_t ___U3CU24s_59U3E__1_1;
	// UnityEngine.Transform AnimationController/<IPlay>c__IteratorD::<t>__2
	Transform_t25 * ___U3CtU3E__2_2;
	// System.Single AnimationController/<IPlay>c__IteratorD::<_time>__3
	float ___U3C_timeU3E__3_3;
	// System.Single AnimationController/<IPlay>c__IteratorD::<i>__4
	float ___U3CiU3E__4_4;
	// System.Single AnimationController/<IPlay>c__IteratorD::<i>__5
	float ___U3CiU3E__5_5;
	// System.Single AnimationController/<IPlay>c__IteratorD::<timeAviso>__6
	float ___U3CtimeAvisoU3E__6_6;
	// System.Single AnimationController/<IPlay>c__IteratorD::<i>__7
	float ___U3CiU3E__7_7;
	// System.Single AnimationController/<IPlay>c__IteratorD::<i>__8
	float ___U3CiU3E__8_8;
	// System.Single AnimationController/<IPlay>c__IteratorD::<i>__9
	float ___U3CiU3E__9_9;
	// System.Single AnimationController/<IPlay>c__IteratorD::<i>__10
	float ___U3CiU3E__10_10;
	// System.Int32 AnimationController/<IPlay>c__IteratorD::$PC
	int32_t ___U24PC_11;
	// System.Object AnimationController/<IPlay>c__IteratorD::$current
	Object_t * ___U24current_12;
	// AnimationController AnimationController/<IPlay>c__IteratorD::<>f__this
	AnimationController_t357 * ___U3CU3Ef__this_13;
};
