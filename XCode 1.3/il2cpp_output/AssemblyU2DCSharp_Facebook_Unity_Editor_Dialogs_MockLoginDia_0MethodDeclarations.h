﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct MockLoginDialog_t273;
// System.String
struct String_t;
// Facebook.Unity.IGraphResult
struct IGraphResult_t484;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::.ctor()
extern "C" void MockLoginDialog__ctor_m1572 (MockLoginDialog_t273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::get_DialogTitle()
extern "C" String_t* MockLoginDialog_get_DialogTitle_m1573 (MockLoginDialog_t273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::DoGui()
extern "C" void MockLoginDialog_DoGui_m1574 (MockLoginDialog_t273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::SendSuccessResult()
extern "C" void MockLoginDialog_SendSuccessResult_m1575 (MockLoginDialog_t273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::<SendSuccessResult>m__32(Facebook.Unity.IGraphResult)
extern "C" void MockLoginDialog_U3CSendSuccessResultU3Em__32_m1576 (MockLoginDialog_t273 * __this, Object_t * ___graphResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
