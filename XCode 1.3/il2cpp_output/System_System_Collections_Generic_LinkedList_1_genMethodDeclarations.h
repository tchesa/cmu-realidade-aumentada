﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t987;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3606;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t1151;
// System.Int32[]
struct Int32U5BU5D_t335;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern "C" void LinkedList_1__ctor_m6567_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m6567(__this, method) (( void (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1__ctor_m6567_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m23760_gshared (LinkedList_1_t987 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m23760(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t987 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))LinkedList_1__ctor_m23760_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23761_gshared (LinkedList_1_t987 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23761(__this, ___value, method) (( void (*) (LinkedList_1_t987 *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23761_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m23762_gshared (LinkedList_1_t987 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m23762(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t987 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m23762_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23763_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23763(__this, method) (( Object_t* (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23763_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m23764_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m23764(__this, method) (( Object_t * (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m23764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23765_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23765(__this, method) (( bool (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23765_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m23766_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m23766(__this, method) (( bool (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m23766_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m23767_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m23767(__this, method) (( Object_t * (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m23767_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m23768_gshared (LinkedList_1_t987 * __this, LinkedListNode_1_t1151 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m23768(__this, ___node, method) (( void (*) (LinkedList_1_t987 *, LinkedListNode_1_t1151 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m23768_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C" LinkedListNode_1_t1151 * LinkedList_1_AddLast_m6572_gshared (LinkedList_1_t987 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m6572(__this, ___value, method) (( LinkedListNode_1_t1151 * (*) (LinkedList_1_t987 *, int32_t, const MethodInfo*))LinkedList_1_AddLast_m6572_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C" void LinkedList_1_Clear_m23769_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m23769(__this, method) (( void (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_Clear_m23769_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C" bool LinkedList_1_Contains_m23770_gshared (LinkedList_1_t987 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m23770(__this, ___value, method) (( bool (*) (LinkedList_1_t987 *, int32_t, const MethodInfo*))LinkedList_1_Contains_m23770_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m23771_gshared (LinkedList_1_t987 * __this, Int32U5BU5D_t335* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m23771(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t987 *, Int32U5BU5D_t335*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m23771_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C" LinkedListNode_1_t1151 * LinkedList_1_Find_m23772_gshared (LinkedList_1_t987 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Find_m23772(__this, ___value, method) (( LinkedListNode_1_t1151 * (*) (LinkedList_1_t987 *, int32_t, const MethodInfo*))LinkedList_1_Find_m23772_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t3123  LinkedList_1_GetEnumerator_m23773_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m23773(__this, method) (( Enumerator_t3123  (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_GetEnumerator_m23773_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m23774_gshared (LinkedList_1_t987 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m23774(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t987 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))LinkedList_1_GetObjectData_m23774_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m23775_gshared (LinkedList_1_t987 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m23775(__this, ___sender, method) (( void (*) (LinkedList_1_t987 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m23775_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C" bool LinkedList_1_Remove_m23776_gshared (LinkedList_1_t987 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m23776(__this, ___value, method) (( bool (*) (LinkedList_1_t987 *, int32_t, const MethodInfo*))LinkedList_1_Remove_m23776_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m6741_gshared (LinkedList_1_t987 * __this, LinkedListNode_1_t1151 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m6741(__this, ___node, method) (( void (*) (LinkedList_1_t987 *, LinkedListNode_1_t1151 *, const MethodInfo*))LinkedList_1_Remove_m6741_gshared)(__this, ___node, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m23777_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m23777(__this, method) (( int32_t (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_get_Count_m23777_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C" LinkedListNode_1_t1151 * LinkedList_1_get_First_m6580_gshared (LinkedList_1_t987 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m6580(__this, method) (( LinkedListNode_1_t1151 * (*) (LinkedList_1_t987 *, const MethodInfo*))LinkedList_1_get_First_m6580_gshared)(__this, method)
