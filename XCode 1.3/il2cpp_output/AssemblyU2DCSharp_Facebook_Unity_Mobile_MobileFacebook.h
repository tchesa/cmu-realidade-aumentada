﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_FacebookBase.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode.h"

// Facebook.Unity.Mobile.MobileFacebook
struct  MobileFacebook_t250  : public FacebookBase_t227
{
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Mobile.MobileFacebook::shareDialogMode
	int32_t ___shareDialogMode_4;
};
