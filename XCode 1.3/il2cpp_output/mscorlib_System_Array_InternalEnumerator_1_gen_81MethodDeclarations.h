﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_81.h"
#include "UnityEngine_UnityEngine_Keyframe.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m28455_gshared (InternalEnumerator_1_t3438 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m28455(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3438 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m28455_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m28456_gshared (InternalEnumerator_1_t3438 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m28456(__this, method) (( void (*) (InternalEnumerator_1_t3438 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m28456_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28457_gshared (InternalEnumerator_1_t3438 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28457(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3438 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28457_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m28458_gshared (InternalEnumerator_1_t3438 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m28458(__this, method) (( void (*) (InternalEnumerator_1_t3438 *, const MethodInfo*))InternalEnumerator_1_Dispose_m28458_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m28459_gshared (InternalEnumerator_1_t3438 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m28459(__this, method) (( bool (*) (InternalEnumerator_1_t3438 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m28459_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern "C" Keyframe_t1321  InternalEnumerator_1_get_Current_m28460_gshared (InternalEnumerator_1_t3438 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m28460(__this, method) (( Keyframe_t1321  (*) (InternalEnumerator_1_t3438 *, const MethodInfo*))InternalEnumerator_1_get_Current_m28460_gshared)(__this, method)
