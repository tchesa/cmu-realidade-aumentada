﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"


extern "C" void ComponentTracker_GetElement_TisObject_t_m2797_gshared ();
extern "C" void ISN_Singleton_1_get_instance_m16860_gshared ();
extern "C" void ISN_Singleton_1_get_HasInstance_m16861_gshared ();
extern "C" void ISN_Singleton_1_get_IsDestroyed_m16862_gshared ();
extern "C" void ISN_Singleton_1__ctor_m16858_gshared ();
extern "C" void ISN_Singleton_1__cctor_m16859_gshared ();
extern "C" void ISN_Singleton_1_OnDestroy_m16863_gshared ();
extern "C" void ISN_Singleton_1_OnApplicationQuit_m16864_gshared ();
extern "C" void SAC_Singleton_1_get_instance_m18047_gshared ();
extern "C" void SAC_Singleton_1_get_IsDestroyed_m18048_gshared ();
extern "C" void SAC_Singleton_1__ctor_m18045_gshared ();
extern "C" void SAC_Singleton_1__cctor_m18046_gshared ();
extern "C" void CallbackManager_AddFacebookDelegate_TisObject_t_m2621_gshared ();
extern "C" void CallbackManager_TryCallCallback_TisObject_t_m2620_gshared ();
extern "C" void CanvasUIMethodCall_1__ctor_m18261_gshared ();
extern "C" void CanvasUIMethodCall_1_Call_m18263_gshared ();
extern "C" void CanvasUIMethodCall_1_UI_m18265_gshared ();
extern "C" void ComponentFactory_GetComponent_TisObject_t_m2623_gshared ();
extern "C" void ComponentFactory_AddComponent_TisObject_t_m2631_gshared ();
extern "C" void MethodArguments_AddList_TisObject_t_m2375_gshared ();
extern "C" void MethodCall_1_get_MethodName_m18267_gshared ();
extern "C" void MethodCall_1_set_MethodName_m18268_gshared ();
extern "C" void MethodCall_1_get_Callback_m18269_gshared ();
extern "C" void MethodCall_1_set_Callback_m18270_gshared ();
extern "C" void MethodCall_1_get_FacebookImpl_m18271_gshared ();
extern "C" void MethodCall_1_set_FacebookImpl_m18272_gshared ();
extern "C" void MethodCall_1_get_Parameters_m18273_gshared ();
extern "C" void MethodCall_1_set_Parameters_m18274_gshared ();
extern "C" void MethodCall_1__ctor_m18266_gshared ();
extern "C" void JavaMethodCall_1__ctor_m18448_gshared ();
extern "C" void JavaMethodCall_1_Call_m18450_gshared ();
extern "C" void FBJavaClass_CallStatic_TisObject_t_m30405_gshared ();
extern "C" void AndroidJavaClass_CallStatic_TisObject_t_m30406_gshared ();
extern "C" void IOSFacebook_AddCallback_TisObject_t_m2625_gshared ();
extern "C" void EditorFacebook_ShowEmptyMockDialog_TisObject_t_m2627_gshared ();
extern "C" void Utilities_TryGetValue_TisObject_t_m2622_gshared ();
extern "C" void Utilities_GetValueOrDefault_TisObject_t_m2628_gshared ();
extern "C" void FacebookDelegate_1__ctor_m18240_gshared ();
extern "C" void FacebookDelegate_1_Invoke_m18241_gshared ();
extern "C" void FacebookDelegate_1_BeginInvoke_m18242_gshared ();
extern "C" void FacebookDelegate_1_EndInvoke_m18243_gshared ();
extern "C" void ExecuteEvents_ValidateEventData_TisObject_t_m4393_gshared ();
extern "C" void ExecuteEvents_Execute_TisObject_t_m4392_gshared ();
extern "C" void ExecuteEvents_ExecuteHierarchy_TisObject_t_m4395_gshared ();
extern "C" void ExecuteEvents_ShouldSendToComponent_TisObject_t_m30457_gshared ();
extern "C" void ExecuteEvents_GetEventList_TisObject_t_m30454_gshared ();
extern "C" void ExecuteEvents_CanHandleEvent_TisObject_t_m30480_gshared ();
extern "C" void ExecuteEvents_GetEventHandler_TisObject_t_m4394_gshared ();
extern "C" void EventFunction_1__ctor_m19631_gshared ();
extern "C" void EventFunction_1_Invoke_m19633_gshared ();
extern "C" void EventFunction_1_BeginInvoke_m19635_gshared ();
extern "C" void EventFunction_1_EndInvoke_m19637_gshared ();
extern "C" void SetPropertyUtility_SetClass_TisObject_t_m4397_gshared ();
extern "C" void LayoutGroup_SetProperty_TisObject_t_m4437_gshared ();
extern "C" void IndexedSet_1_get_Count_m20600_gshared ();
extern "C" void IndexedSet_1_get_IsReadOnly_m20602_gshared ();
extern "C" void IndexedSet_1_get_Item_m20610_gshared ();
extern "C" void IndexedSet_1_set_Item_m20612_gshared ();
extern "C" void IndexedSet_1__ctor_m20584_gshared ();
extern "C" void IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20586_gshared ();
extern "C" void IndexedSet_1_Add_m20588_gshared ();
extern "C" void IndexedSet_1_Remove_m20590_gshared ();
extern "C" void IndexedSet_1_GetEnumerator_m20592_gshared ();
extern "C" void IndexedSet_1_Clear_m20594_gshared ();
extern "C" void IndexedSet_1_Contains_m20596_gshared ();
extern "C" void IndexedSet_1_CopyTo_m20598_gshared ();
extern "C" void IndexedSet_1_IndexOf_m20604_gshared ();
extern "C" void IndexedSet_1_Insert_m20606_gshared ();
extern "C" void IndexedSet_1_RemoveAt_m20608_gshared ();
extern "C" void IndexedSet_1_RemoveAll_m20613_gshared ();
extern "C" void IndexedSet_1_Sort_m20614_gshared ();
extern "C" void ObjectPool_1_get_countAll_m19735_gshared ();
extern "C" void ObjectPool_1_set_countAll_m19737_gshared ();
extern "C" void ObjectPool_1_get_countActive_m19739_gshared ();
extern "C" void ObjectPool_1_get_countInactive_m19741_gshared ();
extern "C" void ObjectPool_1__ctor_m19733_gshared ();
extern "C" void ObjectPool_1_Get_m19743_gshared ();
extern "C" void ObjectPool_1_Release_m19745_gshared ();
extern "C" void NullPremiumObjectFactory_CreateReconstruction_TisObject_t_m30617_gshared ();
extern "C" void SmartTerrainBuilderImpl_CreateReconstruction_TisObject_t_m30684_gshared ();
extern "C" void TrackerManagerImpl_GetTracker_TisObject_t_m30780_gshared ();
extern "C" void TrackerManagerImpl_InitTracker_TisObject_t_m30781_gshared ();
extern "C" void TrackerManagerImpl_DeinitTracker_TisObject_t_m30782_gshared ();
extern "C" void VuforiaAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m6594_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m30479_gshared ();
extern "C" void InvokableCall_1__ctor_m20223_gshared ();
extern "C" void InvokableCall_1__ctor_m20224_gshared ();
extern "C" void InvokableCall_1_Invoke_m20225_gshared ();
extern "C" void InvokableCall_1_Find_m20226_gshared ();
extern "C" void InvokableCall_2__ctor_m28131_gshared ();
extern "C" void InvokableCall_2_Invoke_m28132_gshared ();
extern "C" void InvokableCall_2_Find_m28133_gshared ();
extern "C" void InvokableCall_3__ctor_m28138_gshared ();
extern "C" void InvokableCall_3_Invoke_m28139_gshared ();
extern "C" void InvokableCall_3_Find_m28140_gshared ();
extern "C" void InvokableCall_4__ctor_m28145_gshared ();
extern "C" void InvokableCall_4_Invoke_m28146_gshared ();
extern "C" void InvokableCall_4_Find_m28147_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m28152_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m28153_gshared ();
extern "C" void UnityEvent_1__ctor_m20211_gshared ();
extern "C" void UnityEvent_1_AddListener_m20213_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m20215_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m20217_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m20219_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m20221_gshared ();
extern "C" void UnityEvent_1_Invoke_m20222_gshared ();
extern "C" void UnityEvent_2__ctor_m28352_gshared ();
extern "C" void UnityEvent_2_FindMethod_Impl_m28353_gshared ();
extern "C" void UnityEvent_2_GetDelegate_m28354_gshared ();
extern "C" void UnityEvent_3__ctor_m28355_gshared ();
extern "C" void UnityEvent_3_FindMethod_Impl_m28356_gshared ();
extern "C" void UnityEvent_3_GetDelegate_m28357_gshared ();
extern "C" void UnityEvent_4__ctor_m28358_gshared ();
extern "C" void UnityEvent_4_FindMethod_Impl_m28359_gshared ();
extern "C" void UnityEvent_4_GetDelegate_m28360_gshared ();
extern "C" void GameObject_GetComponent_TisObject_t_m436_gshared ();
extern "C" void GameObject_GetComponentInChildren_TisObject_t_m2795_gshared ();
extern "C" void GameObject_GetComponentsInParent_TisObject_t_m4396_gshared ();
extern "C" void GameObject_GetComponents_TisObject_t_m439_gshared ();
extern "C" void GameObject_GetComponents_TisObject_t_m30456_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m2633_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m30547_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m2799_gshared ();
extern "C" void GameObject_AddComponent_TisObject_t_m438_gshared ();
extern "C" void ScriptableObject_CreateInstance_TisObject_t_m2339_gshared ();
extern "C" void Resources_ConvertObjects_TisObject_t_m30567_gshared ();
extern "C" void Object_FindObjectsOfType_TisObject_t_m6592_gshared ();
extern "C" void Object_FindObjectOfType_TisObject_t_m2796_gshared ();
extern "C" void Component_GetComponent_TisObject_t_m437_gshared ();
extern "C" void Component_GetComponentInChildren_TisObject_t_m6589_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m2798_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m30546_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m2632_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m4438_gshared ();
extern "C" void Component_GetComponents_TisObject_t_m440_gshared ();
extern "C" void Component_GetComponents_TisObject_t_m4391_gshared ();
extern "C" void UnityAction_1__ctor_m19763_gshared ();
extern "C" void UnityAction_1_Invoke_m19764_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m19765_gshared ();
extern "C" void UnityAction_1_EndInvoke_m19766_gshared ();
extern "C" void UnityAction_2__ctor_m28134_gshared ();
extern "C" void UnityAction_2_Invoke_m28135_gshared ();
extern "C" void UnityAction_2_BeginInvoke_m28136_gshared ();
extern "C" void UnityAction_2_EndInvoke_m28137_gshared ();
extern "C" void UnityAction_3__ctor_m28141_gshared ();
extern "C" void UnityAction_3_Invoke_m28142_gshared ();
extern "C" void UnityAction_3_BeginInvoke_m28143_gshared ();
extern "C" void UnityAction_3_EndInvoke_m28144_gshared ();
extern "C" void UnityAction_4__ctor_m28148_gshared ();
extern "C" void UnityAction_4_Invoke_m28149_gshared ();
extern "C" void UnityAction_4_BeginInvoke_m28150_gshared ();
extern "C" void UnityAction_4_EndInvoke_m28151_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29058_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29059_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29060_gshared ();
extern "C" void LinkedList_1_get_Count_m29072_gshared ();
extern "C" void LinkedList_1_get_First_m29073_gshared ();
extern "C" void LinkedList_1__ctor_m29052_gshared ();
extern "C" void LinkedList_1__ctor_m29053_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29054_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m29055_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29056_gshared ();
extern "C" void LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29057_gshared ();
extern "C" void LinkedList_1_VerifyReferencedNode_m29061_gshared ();
extern "C" void LinkedList_1_AddLast_m29062_gshared ();
extern "C" void LinkedList_1_Clear_m29063_gshared ();
extern "C" void LinkedList_1_Contains_m29064_gshared ();
extern "C" void LinkedList_1_CopyTo_m29065_gshared ();
extern "C" void LinkedList_1_Find_m29066_gshared ();
extern "C" void LinkedList_1_GetEnumerator_m29067_gshared ();
extern "C" void LinkedList_1_GetObjectData_m29068_gshared ();
extern "C" void LinkedList_1_OnDeserialization_m29069_gshared ();
extern "C" void LinkedList_1_Remove_m29070_gshared ();
extern "C" void LinkedList_1_Remove_m29071_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m29081_gshared ();
extern "C" void Enumerator_get_Current_m29083_gshared ();
extern "C" void Enumerator__ctor_m29080_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m29082_gshared ();
extern "C" void Enumerator_MoveNext_m29084_gshared ();
extern "C" void Enumerator_Dispose_m29085_gshared ();
extern "C" void LinkedListNode_1_get_List_m29077_gshared ();
extern "C" void LinkedListNode_1_get_Next_m29078_gshared ();
extern "C" void LinkedListNode_1_get_Value_m29079_gshared ();
extern "C" void LinkedListNode_1__ctor_m29074_gshared ();
extern "C" void LinkedListNode_1__ctor_m29075_gshared ();
extern "C" void LinkedListNode_1_Detach_m29076_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_IsSynchronized_m19747_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_SyncRoot_m19748_gshared ();
extern "C" void Stack_1_get_Count_m19755_gshared ();
extern "C" void Stack_1__ctor_m19746_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m19749_gshared ();
extern "C" void Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19750_gshared ();
extern "C" void Stack_1_System_Collections_IEnumerable_GetEnumerator_m19751_gshared ();
extern "C" void Stack_1_Peek_m19752_gshared ();
extern "C" void Stack_1_Pop_m19753_gshared ();
extern "C" void Stack_1_Push_m19754_gshared ();
extern "C" void Stack_1_GetEnumerator_m19756_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19759_gshared ();
extern "C" void Enumerator_get_Current_m19762_gshared ();
extern "C" void Enumerator__ctor_m19757_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m19758_gshared ();
extern "C" void Enumerator_Dispose_m19760_gshared ();
extern "C" void Enumerator_MoveNext_m19761_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27257_gshared ();
extern "C" void HashSet_1_get_Count_m27265_gshared ();
extern "C" void HashSet_1__ctor_m27251_gshared ();
extern "C" void HashSet_1__ctor_m27253_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27255_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m27259_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27261_gshared ();
extern "C" void HashSet_1_System_Collections_IEnumerable_GetEnumerator_m27263_gshared ();
extern "C" void HashSet_1_Init_m27267_gshared ();
extern "C" void HashSet_1_InitArrays_m27269_gshared ();
extern "C" void HashSet_1_SlotsContainsAt_m27271_gshared ();
extern "C" void HashSet_1_CopyTo_m27273_gshared ();
extern "C" void HashSet_1_CopyTo_m27275_gshared ();
extern "C" void HashSet_1_Resize_m27277_gshared ();
extern "C" void HashSet_1_GetLinkHashCode_m27279_gshared ();
extern "C" void HashSet_1_GetItemHashCode_m27281_gshared ();
extern "C" void HashSet_1_Add_m27282_gshared ();
extern "C" void HashSet_1_Clear_m27284_gshared ();
extern "C" void HashSet_1_Contains_m27286_gshared ();
extern "C" void HashSet_1_Remove_m27288_gshared ();
extern "C" void HashSet_1_GetObjectData_m27290_gshared ();
extern "C" void HashSet_1_OnDeserialization_m27292_gshared ();
extern "C" void HashSet_1_GetEnumerator_m27293_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27301_gshared ();
extern "C" void Enumerator_get_Current_m27304_gshared ();
extern "C" void Enumerator__ctor_m27300_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m27302_gshared ();
extern "C" void Enumerator_MoveNext_m27303_gshared ();
extern "C" void Enumerator_Dispose_m27305_gshared ();
extern "C" void Enumerator_CheckState_m27306_gshared ();
extern "C" void PrimeHelper__cctor_m27307_gshared ();
extern "C" void PrimeHelper_TestPrime_m27308_gshared ();
extern "C" void PrimeHelper_CalcPrime_m27309_gshared ();
extern "C" void PrimeHelper_ToPrime_m27310_gshared ();
extern "C" void Enumerable_Any_TisObject_t_m2427_gshared ();
extern "C" void Enumerable_Cast_TisObject_t_m6596_gshared ();
extern "C" void Enumerable_CreateCastIterator_TisObject_t_m30616_gshared ();
extern "C" void Enumerable_Contains_TisObject_t_m6591_gshared ();
extern "C" void Enumerable_Contains_TisObject_t_m30557_gshared ();
extern "C" void Enumerable_Count_TisObject_t_m2626_gshared ();
extern "C" void Enumerable_First_TisObject_t_m2428_gshared ();
extern "C" void Enumerable_Select_TisObject_t_TisObject_t_m2629_gshared ();
extern "C" void Enumerable_CreateSelectIterator_TisObject_t_TisObject_t_m30409_gshared ();
extern "C" void Enumerable_ToArray_TisObject_t_m2619_gshared ();
extern "C" void Enumerable_ToList_TisObject_t_m2630_gshared ();
extern "C" void Enumerable_Where_TisObject_t_m4398_gshared ();
extern "C" void Enumerable_CreateWhereIterator_TisObject_t_m30545_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m23546_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m23547_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m23545_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m23548_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m23549_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m23550_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m23551_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m23552_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m18505_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m18506_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m18504_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m18507_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m18508_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m18509_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m18510_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m18511_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m21974_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m21975_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m21973_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m21976_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m21977_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m21978_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m21979_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m21980_gshared ();
extern "C" void Action_2__ctor_m29397_gshared ();
extern "C" void Action_2_Invoke_m29398_gshared ();
extern "C" void Action_2_BeginInvoke_m29399_gshared ();
extern "C" void Action_2_EndInvoke_m29400_gshared ();
extern "C" void Func_2__ctor_m18497_gshared ();
extern "C" void Func_2_Invoke_m18499_gshared ();
extern "C" void Func_2_BeginInvoke_m18501_gshared ();
extern "C" void Func_2_EndInvoke_m18503_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m30231_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisObject_t_m30224_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisObject_t_m30227_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisObject_t_m30225_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisObject_t_m30226_gshared ();
extern "C" void Array_InternalArray__Insert_TisObject_t_m30229_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisObject_t_m30228_gshared ();
extern "C" void Array_InternalArray__get_Item_TisObject_t_m30223_gshared ();
extern "C" void Array_InternalArray__set_Item_TisObject_t_m30230_gshared ();
extern "C" void Array_get_swapper_TisObject_t_m30236_gshared ();
extern "C" void Array_Sort_TisObject_t_m31157_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31158_gshared ();
extern "C" void Array_Sort_TisObject_t_m31159_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31160_gshared ();
extern "C" void Array_Sort_TisObject_t_m15427_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31161_gshared ();
extern "C" void Array_Sort_TisObject_t_m30234_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m30235_gshared ();
extern "C" void Array_Sort_TisObject_t_m31162_gshared ();
extern "C" void Array_Sort_TisObject_t_m30267_gshared ();
extern "C" void Array_qsort_TisObject_t_TisObject_t_m30264_gshared ();
extern "C" void Array_compare_TisObject_t_m30265_gshared ();
extern "C" void Array_qsort_TisObject_t_m30268_gshared ();
extern "C" void Array_swap_TisObject_t_TisObject_t_m30266_gshared ();
extern "C" void Array_swap_TisObject_t_m30269_gshared ();
extern "C" void Array_Resize_TisObject_t_m30232_gshared ();
extern "C" void Array_Resize_TisObject_t_m30233_gshared ();
extern "C" void Array_TrueForAll_TisObject_t_m31163_gshared ();
extern "C" void Array_ForEach_TisObject_t_m31164_gshared ();
extern "C" void Array_ConvertAll_TisObject_t_TisObject_t_m31165_gshared ();
extern "C" void Array_FindLastIndex_TisObject_t_m31166_gshared ();
extern "C" void Array_FindLastIndex_TisObject_t_m31168_gshared ();
extern "C" void Array_FindLastIndex_TisObject_t_m31167_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m31169_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m31171_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m31170_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m31172_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m31174_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m31175_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m31173_gshared ();
extern "C" void Array_IndexOf_TisObject_t_m15433_gshared ();
extern "C" void Array_IndexOf_TisObject_t_m31176_gshared ();
extern "C" void Array_IndexOf_TisObject_t_m15426_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m31177_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m31178_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m31179_gshared ();
extern "C" void Array_FindAll_TisObject_t_m31180_gshared ();
extern "C" void Array_Exists_TisObject_t_m31181_gshared ();
extern "C" void Array_AsReadOnly_TisObject_t_m15447_gshared ();
extern "C" void Array_Find_TisObject_t_m31182_gshared ();
extern "C" void Array_FindLast_TisObject_t_m31183_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15546_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15549_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15544_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15545_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15547_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15548_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Item_m29437_gshared ();
extern "C" void ArrayReadOnlyList_1_set_Item_m29438_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Count_m29439_gshared ();
extern "C" void ArrayReadOnlyList_1_get_IsReadOnly_m29440_gshared ();
extern "C" void ArrayReadOnlyList_1__ctor_m29435_gshared ();
extern "C" void ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29436_gshared ();
extern "C" void ArrayReadOnlyList_1_Add_m29441_gshared ();
extern "C" void ArrayReadOnlyList_1_Clear_m29442_gshared ();
extern "C" void ArrayReadOnlyList_1_Contains_m29443_gshared ();
extern "C" void ArrayReadOnlyList_1_CopyTo_m29444_gshared ();
extern "C" void ArrayReadOnlyList_1_GetEnumerator_m29445_gshared ();
extern "C" void ArrayReadOnlyList_1_IndexOf_m29446_gshared ();
extern "C" void ArrayReadOnlyList_1_Insert_m29447_gshared ();
extern "C" void ArrayReadOnlyList_1_Remove_m29448_gshared ();
extern "C" void ArrayReadOnlyList_1_RemoveAt_m29449_gshared ();
extern "C" void ArrayReadOnlyList_1_ReadOnlyError_m29450_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29452_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29453_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m29451_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29454_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29455_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m29456_gshared ();
extern "C" void Comparer_1_get_Default_m15642_gshared ();
extern "C" void Comparer_1__ctor_m15639_gshared ();
extern "C" void Comparer_1__cctor_m15640_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15641_gshared ();
extern "C" void DefaultComparer__ctor_m15643_gshared ();
extern "C" void DefaultComparer_Compare_m15644_gshared ();
extern "C" void GenericComparer_1__ctor_m29499_gshared ();
extern "C" void GenericComparer_1_Compare_m29500_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m16028_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16030_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16032_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16040_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16042_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16044_gshared ();
extern "C" void Dictionary_2_get_Count_m16062_gshared ();
extern "C" void Dictionary_2_get_Item_m16064_gshared ();
extern "C" void Dictionary_2_set_Item_m16066_gshared ();
extern "C" void Dictionary_2_get_Keys_m16100_gshared ();
extern "C" void Dictionary_2_get_Values_m16102_gshared ();
extern "C" void Dictionary_2__ctor_m16020_gshared ();
extern "C" void Dictionary_2__ctor_m16022_gshared ();
extern "C" void Dictionary_2__ctor_m16024_gshared ();
extern "C" void Dictionary_2__ctor_m16026_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16034_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16036_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16038_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16046_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16048_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16050_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16052_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16054_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16056_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16058_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16060_gshared ();
extern "C" void Dictionary_2_Init_m16068_gshared ();
extern "C" void Dictionary_2_InitArrays_m16070_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16072_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30361_gshared ();
extern "C" void Dictionary_2_make_pair_m16074_gshared ();
extern "C" void Dictionary_2_pick_key_m16076_gshared ();
extern "C" void Dictionary_2_pick_value_m16078_gshared ();
extern "C" void Dictionary_2_CopyTo_m16080_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30360_gshared ();
extern "C" void Dictionary_2_Resize_m16082_gshared ();
extern "C" void Dictionary_2_Add_m16084_gshared ();
extern "C" void Dictionary_2_Clear_m16086_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16088_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16090_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16092_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16094_gshared ();
extern "C" void Dictionary_2_Remove_m16096_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16098_gshared ();
extern "C" void Dictionary_2_ToTKey_m16104_gshared ();
extern "C" void Dictionary_2_ToTValue_m16106_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16108_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16110_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16112_gshared ();
extern "C" void ShimEnumerator_get_Entry_m16193_gshared ();
extern "C" void ShimEnumerator_get_Key_m16194_gshared ();
extern "C" void ShimEnumerator_get_Value_m16195_gshared ();
extern "C" void ShimEnumerator_get_Current_m16196_gshared ();
extern "C" void ShimEnumerator__ctor_m16191_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16192_gshared ();
extern "C" void ShimEnumerator_Reset_m16197_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16146_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16148_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16149_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16150_gshared ();
extern "C" void Enumerator_get_Current_m16152_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16153_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16154_gshared ();
extern "C" void Enumerator__ctor_m16145_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m16147_gshared ();
extern "C" void Enumerator_MoveNext_m16151_gshared ();
extern "C" void Enumerator_Reset_m16155_gshared ();
extern "C" void Enumerator_VerifyState_m16156_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16157_gshared ();
extern "C" void Enumerator_Dispose_m16158_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16133_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16134_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16135_gshared ();
extern "C" void KeyCollection_get_Count_m16138_gshared ();
extern "C" void KeyCollection__ctor_m16125_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16126_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16127_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16128_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16129_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16130_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16131_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16132_gshared ();
extern "C" void KeyCollection_CopyTo_m16136_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16137_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16140_gshared ();
extern "C" void Enumerator_get_Current_m16144_gshared ();
extern "C" void Enumerator__ctor_m16139_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m16141_gshared ();
extern "C" void Enumerator_Dispose_m16142_gshared ();
extern "C" void Enumerator_MoveNext_m16143_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16171_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16172_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16173_gshared ();
extern "C" void ValueCollection_get_Count_m16176_gshared ();
extern "C" void ValueCollection__ctor_m16163_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16165_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16166_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16167_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16168_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16169_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16170_gshared ();
extern "C" void ValueCollection_CopyTo_m16174_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16175_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16178_gshared ();
extern "C" void Enumerator_get_Current_m16182_gshared ();
extern "C" void Enumerator__ctor_m16177_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m16179_gshared ();
extern "C" void Enumerator_Dispose_m16180_gshared ();
extern "C" void Enumerator_MoveNext_m16181_gshared ();
extern "C" void Transform_1__ctor_m16159_gshared ();
extern "C" void Transform_1_Invoke_m16160_gshared ();
extern "C" void Transform_1_BeginInvoke_m16161_gshared ();
extern "C" void Transform_1_EndInvoke_m16162_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15625_gshared ();
extern "C" void EqualityComparer_1__ctor_m15621_gshared ();
extern "C" void EqualityComparer_1__cctor_m15622_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15623_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15624_gshared ();
extern "C" void DefaultComparer__ctor_m15632_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15633_gshared ();
extern "C" void DefaultComparer_Equals_m15634_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m29501_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m29502_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m29503_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16120_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16121_gshared ();
extern "C" void KeyValuePair_2_get_Value_m16122_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16123_gshared ();
extern "C" void KeyValuePair_2__ctor_m16119_gshared ();
extern "C" void KeyValuePair_2_ToString_m16124_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15471_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m15473_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m15475_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m15477_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m15479_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m15481_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m15483_gshared ();
extern "C" void List_1_get_Capacity_m15535_gshared ();
extern "C" void List_1_set_Capacity_m15537_gshared ();
extern "C" void List_1_get_Count_m15539_gshared ();
extern "C" void List_1_get_Item_m15541_gshared ();
extern "C" void List_1_set_Item_m15543_gshared ();
extern "C" void List_1__ctor_m2529_gshared ();
extern "C" void List_1__ctor_m15449_gshared ();
extern "C" void List_1__ctor_m15451_gshared ();
extern "C" void List_1__cctor_m15453_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15455_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15457_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m15459_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m15461_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m15463_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m15465_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m15467_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m15469_gshared ();
extern "C" void List_1_Add_m15485_gshared ();
extern "C" void List_1_GrowIfNeeded_m15487_gshared ();
extern "C" void List_1_AddCollection_m15489_gshared ();
extern "C" void List_1_AddEnumerable_m15491_gshared ();
extern "C" void List_1_AddRange_m15493_gshared ();
extern "C" void List_1_AsReadOnly_m15495_gshared ();
extern "C" void List_1_Clear_m15497_gshared ();
extern "C" void List_1_Contains_m15499_gshared ();
extern "C" void List_1_CopyTo_m15501_gshared ();
extern "C" void List_1_Find_m15503_gshared ();
extern "C" void List_1_CheckMatch_m15505_gshared ();
extern "C" void List_1_GetIndex_m15507_gshared ();
extern "C" void List_1_GetEnumerator_m2487_gshared ();
extern "C" void List_1_IndexOf_m15509_gshared ();
extern "C" void List_1_Shift_m15511_gshared ();
extern "C" void List_1_CheckIndex_m15513_gshared ();
extern "C" void List_1_Insert_m15515_gshared ();
extern "C" void List_1_CheckCollection_m15517_gshared ();
extern "C" void List_1_Remove_m15519_gshared ();
extern "C" void List_1_RemoveAll_m15521_gshared ();
extern "C" void List_1_RemoveAt_m15523_gshared ();
extern "C" void List_1_Reverse_m15525_gshared ();
extern "C" void List_1_Sort_m15527_gshared ();
extern "C" void List_1_Sort_m15529_gshared ();
extern "C" void List_1_ToArray_m15531_gshared ();
extern "C" void List_1_TrimExcess_m15533_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15552_gshared ();
extern "C" void Enumerator_get_Current_m2488_gshared ();
extern "C" void Enumerator__ctor_m15550_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15551_gshared ();
extern "C" void Enumerator_Dispose_m15553_gshared ();
extern "C" void Enumerator_VerifyState_m15554_gshared ();
extern "C" void Enumerator_MoveNext_m2489_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15586_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15594_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15595_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15596_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15597_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15598_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15599_gshared ();
extern "C" void Collection_1_get_Count_m15612_gshared ();
extern "C" void Collection_1_get_Item_m15613_gshared ();
extern "C" void Collection_1_set_Item_m15614_gshared ();
extern "C" void Collection_1__ctor_m15585_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15587_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15588_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15589_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15590_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15591_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15592_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15593_gshared ();
extern "C" void Collection_1_Add_m15600_gshared ();
extern "C" void Collection_1_Clear_m15601_gshared ();
extern "C" void Collection_1_ClearItems_m15602_gshared ();
extern "C" void Collection_1_Contains_m15603_gshared ();
extern "C" void Collection_1_CopyTo_m15604_gshared ();
extern "C" void Collection_1_GetEnumerator_m15605_gshared ();
extern "C" void Collection_1_IndexOf_m15606_gshared ();
extern "C" void Collection_1_Insert_m15607_gshared ();
extern "C" void Collection_1_InsertItem_m15608_gshared ();
extern "C" void Collection_1_Remove_m15609_gshared ();
extern "C" void Collection_1_RemoveAt_m15610_gshared ();
extern "C" void Collection_1_RemoveItem_m15611_gshared ();
extern "C" void Collection_1_SetItem_m15615_gshared ();
extern "C" void Collection_1_IsValidItem_m15616_gshared ();
extern "C" void Collection_1_ConvertItem_m15617_gshared ();
extern "C" void Collection_1_CheckWritable_m15618_gshared ();
extern "C" void Collection_1_IsSynchronized_m15619_gshared ();
extern "C" void Collection_1_IsFixedSize_m15620_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15561_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15562_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15563_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15573_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15574_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15575_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15576_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15577_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15578_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15583_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15584_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15555_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15556_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15557_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15558_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15559_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15560_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15564_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15565_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15566_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15567_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15568_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15569_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15570_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15571_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15572_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15579_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15580_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15581_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15582_gshared ();
extern "C" void CustomAttributeData_UnboxValues_TisObject_t_m31282_gshared ();
extern "C" void MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m31283_gshared ();
extern "C" void MonoProperty_StaticGetterAdapterFrame_TisObject_t_m31284_gshared ();
extern "C" void Getter_2__ctor_m29946_gshared ();
extern "C" void Getter_2_Invoke_m29947_gshared ();
extern "C" void Getter_2_BeginInvoke_m29948_gshared ();
extern "C" void Getter_2_EndInvoke_m29949_gshared ();
extern "C" void StaticGetter_1__ctor_m29950_gshared ();
extern "C" void StaticGetter_1_Invoke_m29951_gshared ();
extern "C" void StaticGetter_1_BeginInvoke_m29952_gshared ();
extern "C" void StaticGetter_1_EndInvoke_m29953_gshared ();
extern "C" void Activator_CreateInstance_TisObject_t_m30453_gshared ();
extern "C" void Action_1__ctor_m17054_gshared ();
extern "C" void Action_1_Invoke_m17055_gshared ();
extern "C" void Action_1_BeginInvoke_m17057_gshared ();
extern "C" void Action_1_EndInvoke_m17059_gshared ();
extern "C" void Comparison_1__ctor_m15663_gshared ();
extern "C" void Comparison_1_Invoke_m15664_gshared ();
extern "C" void Comparison_1_BeginInvoke_m15665_gshared ();
extern "C" void Comparison_1_EndInvoke_m15666_gshared ();
extern "C" void Converter_2__ctor_m29431_gshared ();
extern "C" void Converter_2_Invoke_m29432_gshared ();
extern "C" void Converter_2_BeginInvoke_m29433_gshared ();
extern "C" void Converter_2_EndInvoke_m29434_gshared ();
extern "C" void Predicate_1__ctor_m15635_gshared ();
extern "C" void Predicate_1_Invoke_m15636_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15637_gshared ();
extern "C" void Predicate_1_EndInvoke_m15638_gshared ();
extern "C" void Nullable_1_get_HasValue_m328_gshared ();
extern "C" void Nullable_1_get_Value_m329_gshared ();
extern "C" void Dictionary_2__ctor_m15743_gshared ();
extern "C" void Dictionary_2__ctor_m8129_gshared ();
extern "C" void List_1__ctor_m2216_gshared ();
extern "C" void List_1_ToArray_m2218_gshared ();
extern "C" void Dictionary_2__ctor_m15740_gshared ();
extern "C" void Action_1__ctor_m2226_gshared ();
extern "C" void Action_1_Invoke_m2235_gshared ();
extern "C" void Action_1__ctor_m2255_gshared ();
extern "C" void Action_1_Invoke_m2258_gshared ();
extern "C" void Action_1__ctor_m2268_gshared ();
extern "C" void Action_1_Invoke_m2271_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16339_gshared ();
extern "C" void Enumerator_get_Current_m16426_gshared ();
extern "C" void KeyValuePair_2_get_Value_m16396_gshared ();
extern "C" void Enumerator_MoveNext_m16425_gshared ();
extern "C" void MethodArguments_AddPrimative_TisBoolean_t41_m2368_gshared ();
extern "C" void Nullable_1_get_HasValue_m2373_gshared ();
extern "C" void Nullable_1_ToString_m2374_gshared ();
extern "C" void MethodArguments_AddNullablePrimitive_TisInt32_t59_m2377_gshared ();
extern "C" void MethodArguments_AddPrimative_TisInt32_t59_m2382_gshared ();
extern "C" void Nullable_1__ctor_m2401_gshared ();
extern "C" void Nullable_1_GetValueOrDefault_m2408_gshared ();
extern "C" void MethodArguments_AddNullablePrimitive_TisOGActionType_t265_m2426_gshared ();
extern "C" void MethodArguments_AddNullablePrimitive_TisSingle_t36_m2437_gshared ();
extern "C" void MethodArguments_AddPrimative_TisSingle_t36_m2438_gshared ();
extern "C" void Nullable_1_get_HasValue_m2443_gshared ();
extern "C" void Nullable_1_get_Value_m2444_gshared ();
extern "C" void Nullable_1_get_HasValue_m2449_gshared ();
extern "C" void Nullable_1_get_Value_m2450_gshared ();
extern "C" void Utilities_GetValueOrDefault_TisInt64_t507_m2504_gshared ();
extern "C" void Nullable_1_get_HasValue_m2512_gshared ();
extern "C" void Nullable_1_get_Value_m2513_gshared ();
extern "C" void Dictionary_2__ctor_m18518_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m18607_gshared ();
extern "C" void Enumerator_get_Current_m18649_gshared ();
extern "C" void KeyValuePair_2_get_Key_m18617_gshared ();
extern "C" void KeyValuePair_2_get_Value_m18619_gshared ();
extern "C" void Enumerator_MoveNext_m18648_gshared ();
extern "C" void Dictionary_2__ctor_m2587_gshared ();
extern "C" void Action_2__ctor_m19399_gshared ();
extern "C" void Action_2_Invoke_m19400_gshared ();
extern "C" void Action_1__ctor_m2681_gshared ();
extern "C" void Action_1_Invoke_m2682_gshared ();
extern "C" void Action_1__ctor_m2699_gshared ();
extern "C" void Comparison_1__ctor_m3994_gshared ();
extern "C" void List_1_Sort_m4000_gshared ();
extern "C" void List_1__ctor_m4033_gshared ();
extern "C" void Dictionary_2_get_Values_m16331_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16449_gshared ();
extern "C" void Enumerator_get_Current_m16456_gshared ();
extern "C" void Enumerator_MoveNext_m16455_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16394_gshared ();
extern "C" void KeyValuePair_2_ToString_m16398_gshared ();
extern "C" void Comparison_1__ctor_m4099_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t566_m4100_gshared ();
extern "C" void UnityEvent_1__ctor_m4104_gshared ();
extern "C" void UnityEvent_1_Invoke_m4105_gshared ();
extern "C" void UnityEvent_1_AddListener_m4106_gshared ();
extern "C" void TweenRunner_1__ctor_m4125_gshared ();
extern "C" void TweenRunner_1_Init_m4126_gshared ();
extern "C" void UnityAction_1__ctor_m4154_gshared ();
extern "C" void TweenRunner_1_StartTween_m4155_gshared ();
extern "C" void List_1_get_Capacity_m4156_gshared ();
extern "C" void List_1_set_Capacity_m4157_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisType_t701_m4178_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisBoolean_t41_m4179_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisFillMethod_t702_m4180_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisSingle_t36_m4181_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisInt32_t59_m4182_gshared ();
extern "C" void List_1__ctor_m4218_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisContentType_t710_m4229_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisLineType_t713_m4230_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisInputType_t711_m4231_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisTouchScreenKeyboardType_t854_m4232_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisCharacterValidation_t712_m4233_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisChar_t556_m4234_gshared ();
extern "C" void List_1_ToArray_m4278_gshared ();
extern "C" void UnityEvent_1__ctor_m4305_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisDirection_t732_m4308_gshared ();
extern "C" void UnityEvent_1_Invoke_m4310_gshared ();
extern "C" void UnityEvent_1__ctor_m4315_gshared ();
extern "C" void UnityAction_1__ctor_m4316_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m4317_gshared ();
extern "C" void UnityEvent_1_AddListener_m4318_gshared ();
extern "C" void UnityEvent_1_Invoke_m4323_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisNavigation_t730_m4341_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisTransition_t743_m4342_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisColorBlock_t682_m4343_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisSpriteState_t745_m4344_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisDirection_t749_m4359_gshared ();
extern "C" void UnityEvent_1__ctor_m4376_gshared ();
extern "C" void UnityEvent_1_Invoke_m4377_gshared ();
extern "C" void Func_2__ctor_m21966_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisAspectMode_t764_m4383_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisFitMode_t770_m4390_gshared ();
extern "C" void LayoutGroup_SetProperty_TisCorner_t772_m4399_gshared ();
extern "C" void LayoutGroup_SetProperty_TisAxis_t773_m4400_gshared ();
extern "C" void LayoutGroup_SetProperty_TisVector2_t29_m4401_gshared ();
extern "C" void LayoutGroup_SetProperty_TisConstraint_t774_m4402_gshared ();
extern "C" void LayoutGroup_SetProperty_TisInt32_t59_m4403_gshared ();
extern "C" void LayoutGroup_SetProperty_TisSingle_t36_m4408_gshared ();
extern "C" void LayoutGroup_SetProperty_TisBoolean_t41_m4409_gshared ();
extern "C" void LayoutGroup_SetProperty_TisTextAnchor_t869_m4415_gshared ();
extern "C" void Func_2__ctor_m22077_gshared ();
extern "C" void Func_2_Invoke_m22078_gshared ();
extern "C" void Dictionary_2__ctor_m22689_gshared ();
extern "C" void List_1__ctor_m6516_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16319_gshared ();
extern "C" void Enumerator_Dispose_m16454_gshared ();
extern "C" void LinkedList_1__ctor_m6567_gshared ();
extern "C" void LinkedList_1_AddLast_m6572_gshared ();
extern "C" void List_1__ctor_m6573_gshared ();
extern "C" void List_1_GetEnumerator_m6574_gshared ();
extern "C" void Enumerator_get_Current_m6575_gshared ();
extern "C" void Predicate_1__ctor_m6576_gshared ();
extern "C" void Array_Exists_TisTrackableResultData_t969_m6577_gshared ();
extern "C" void Enumerator_MoveNext_m6578_gshared ();
extern "C" void Enumerator_Dispose_m6579_gshared ();
extern "C" void LinkedList_1_get_First_m6580_gshared ();
extern "C" void LinkedListNode_1_get_Value_m6581_gshared ();
extern "C" void Dictionary_2_get_Values_m22770_gshared ();
extern "C" void ValueCollection_GetEnumerator_m22849_gshared ();
extern "C" void Enumerator_get_Current_m22856_gshared ();
extern "C" void Enumerator_MoveNext_m22855_gshared ();
extern "C" void Enumerator_Dispose_m22854_gshared ();
extern "C" void Dictionary_2__ctor_m24140_gshared ();
extern "C" void List_1__ctor_m6641_gshared ();
extern "C" void Dictionary_2_get_Keys_m16329_gshared ();
extern "C" void Enumerable_ToList_TisInt32_t59_m6643_gshared ();
extern "C" void Enumerator_Dispose_m16432_gshared ();
extern "C" void Action_1_Invoke_m6677_gshared ();
extern "C" void KeyCollection_CopyTo_m16410_gshared ();
extern "C" void Enumerable_ToArray_TisInt32_t59_m6738_gshared ();
extern "C" void LinkedListNode_1_get_Next_m6740_gshared ();
extern "C" void LinkedList_1_Remove_m6741_gshared ();
extern "C" void Dictionary_2__ctor_m6742_gshared ();
extern "C" void Dictionary_2__ctor_m6744_gshared ();
extern "C" void List_1__ctor_m6759_gshared ();
extern "C" void Action_1_Invoke_m6793_gshared ();
extern "C" void List_1__ctor_m8141_gshared ();
extern "C" void List_1__ctor_m8142_gshared ();
extern "C" void List_1__ctor_m8143_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m8146_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m8147_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m8149_gshared ();
extern "C" void Dictionary_2__ctor_m29088_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t59_m9267_gshared ();
extern "C" void CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t2101_m15429_gshared ();
extern "C" void Array_AsReadOnly_TisCustomAttributeTypedArgument_t2101_m15430_gshared ();
extern "C" void CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t2100_m15431_gshared ();
extern "C" void Array_AsReadOnly_TisCustomAttributeNamedArgument_t2100_m15432_gshared ();
extern "C" void GenericComparer_1__ctor_m15435_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m15436_gshared ();
extern "C" void GenericComparer_1__ctor_m15437_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m15438_gshared ();
extern "C" void Nullable_1__ctor_m15439_gshared ();
extern "C" void Nullable_1_get_HasValue_m15440_gshared ();
extern "C" void Nullable_1_get_Value_m15441_gshared ();
extern "C" void GenericComparer_1__ctor_m15442_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m15443_gshared ();
extern "C" void GenericComparer_1__ctor_m15445_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m15446_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt32_t59_m30237_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisInt32_t59_m30238_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt32_t59_m30239_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt32_t59_m30240_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt32_t59_m30241_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt32_t59_m30242_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt32_t59_m30243_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt32_t59_m30244_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t59_m30245_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDouble_t60_m30246_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisDouble_t60_m30247_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisDouble_t60_m30248_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDouble_t60_m30249_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDouble_t60_m30250_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDouble_t60_m30251_gshared ();
extern "C" void Array_InternalArray__Insert_TisDouble_t60_m30252_gshared ();
extern "C" void Array_InternalArray__set_Item_TisDouble_t60_m30253_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t60_m30254_gshared ();
extern "C" void Array_InternalArray__get_Item_TisChar_t556_m30255_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisChar_t556_m30256_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisChar_t556_m30257_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisChar_t556_m30258_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisChar_t556_m30259_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisChar_t556_m30260_gshared ();
extern "C" void Array_InternalArray__Insert_TisChar_t556_m30261_gshared ();
extern "C" void Array_InternalArray__set_Item_TisChar_t556_m30262_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisChar_t556_m30263_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector3_t6_m30270_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisVector3_t6_m30271_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisVector3_t6_m30272_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector3_t6_m30273_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector3_t6_m30274_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector3_t6_m30275_gshared ();
extern "C" void Array_InternalArray__Insert_TisVector3_t6_m30276_gshared ();
extern "C" void Array_InternalArray__set_Item_TisVector3_t6_m30277_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t6_m30278_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector2_t29_m30279_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisVector2_t29_m30280_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisVector2_t29_m30281_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector2_t29_m30282_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector2_t29_m30283_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector2_t29_m30284_gshared ();
extern "C" void Array_InternalArray__Insert_TisVector2_t29_m30285_gshared ();
extern "C" void Array_InternalArray__set_Item_TisVector2_t29_m30286_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t29_m30287_gshared ();
extern "C" void Array_InternalArray__get_Item_TisColor_t5_m30288_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisColor_t5_m30289_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisColor_t5_m30290_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisColor_t5_m30291_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisColor_t5_m30292_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisColor_t5_m30293_gshared ();
extern "C" void Array_InternalArray__Insert_TisColor_t5_m30294_gshared ();
extern "C" void Array_InternalArray__set_Item_TisColor_t5_m30295_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisColor_t5_m30296_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSingle_t36_m30297_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisSingle_t36_m30298_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSingle_t36_m30299_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSingle_t36_m30300_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSingle_t36_m30301_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSingle_t36_m30302_gshared ();
extern "C" void Array_InternalArray__Insert_TisSingle_t36_m30303_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSingle_t36_m30304_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t36_m30305_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRect_t30_m30306_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisRect_t30_m30307_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisRect_t30_m30308_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRect_t30_m30309_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRect_t30_m30310_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRect_t30_m30311_gshared ();
extern "C" void Array_InternalArray__Insert_TisRect_t30_m30312_gshared ();
extern "C" void Array_InternalArray__set_Item_TisRect_t30_m30313_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRect_t30_m30314_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2607_m30315_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2607_m30316_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2607_m30317_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2607_m30318_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2607_m30319_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2607_m30320_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2607_m30321_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2607_m30322_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2607_m30323_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t1956_m30324_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t1956_m30325_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t1956_m30326_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t1956_m30327_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t1956_m30328_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t1956_m30329_gshared ();
extern "C" void Array_InternalArray__Insert_TisLink_t1956_m30330_gshared ();
extern "C" void Array_InternalArray__set_Item_TisLink_t1956_m30331_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t1956_m30332_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30333_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30334_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30335_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30336_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30337_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDictionaryEntry_t58_m30338_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisDictionaryEntry_t58_m30339_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t58_m30340_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t58_m30341_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t58_m30342_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDictionaryEntry_t58_m30343_gshared ();
extern "C" void Array_InternalArray__Insert_TisDictionaryEntry_t58_m30344_gshared ();
extern "C" void Array_InternalArray__set_Item_TisDictionaryEntry_t58_m30345_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t58_m30346_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30347_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2607_m30348_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2607_TisObject_t_m30349_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2607_TisKeyValuePair_2_t2607_m30350_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2634_m30351_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2634_m30352_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2634_m30353_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2634_m30354_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2634_m30355_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2634_m30356_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2634_m30357_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2634_m30358_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2634_m30359_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30362_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2634_m30363_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2634_TisObject_t_m30364_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2634_TisKeyValuePair_2_t2634_m30365_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2658_m30366_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2658_m30367_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2658_m30368_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2658_m30369_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2658_m30370_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2658_m30371_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2658_m30372_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2658_m30373_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2658_m30374_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30375_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30376_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30377_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30378_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30379_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30380_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2658_m30381_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2658_TisObject_t_m30382_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2658_TisKeyValuePair_2_t2658_m30383_gshared ();
extern "C" void Array_InternalArray__get_Item_TisByte_t560_m30384_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisByte_t560_m30385_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisByte_t560_m30386_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisByte_t560_m30387_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisByte_t560_m30388_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisByte_t560_m30389_gshared ();
extern "C" void Array_InternalArray__Insert_TisByte_t560_m30390_gshared ();
extern "C" void Array_InternalArray__set_Item_TisByte_t560_m30391_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t560_m30392_gshared ();
extern "C" void Array_Resize_TisByte_t560_m30393_gshared ();
extern "C" void Array_Resize_TisByte_t560_m30394_gshared ();
extern "C" void Array_IndexOf_TisByte_t560_m30395_gshared ();
extern "C" void Array_Sort_TisByte_t560_m30396_gshared ();
extern "C" void Array_Sort_TisByte_t560_TisByte_t560_m30397_gshared ();
extern "C" void Array_get_swapper_TisByte_t560_m30398_gshared ();
extern "C" void Array_qsort_TisByte_t560_TisByte_t560_m30399_gshared ();
extern "C" void Array_compare_TisByte_t560_m30400_gshared ();
extern "C" void Array_swap_TisByte_t560_TisByte_t560_m30401_gshared ();
extern "C" void Array_Sort_TisByte_t560_m30402_gshared ();
extern "C" void Array_qsort_TisByte_t560_m30403_gshared ();
extern "C" void Array_swap_TisByte_t560_m30404_gshared ();
extern "C" void Utilities_TryGetValue_TisInt64_t507_m30408_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2787_m30410_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2787_m30411_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2787_m30412_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2787_m30413_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2787_m30414_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2787_m30415_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2787_m30416_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2787_m30417_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2787_m30418_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30419_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30420_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisSingle_t36_m30421_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisSingle_t36_TisObject_t_m30422_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisSingle_t36_TisSingle_t36_m30423_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30424_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2787_m30425_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2787_TisObject_t_m30426_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2787_TisKeyValuePair_2_t2787_m30427_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2811_m30428_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2811_m30429_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2811_m30430_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2811_m30431_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2811_m30432_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2811_m30433_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2811_m30434_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2811_m30435_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2811_m30436_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30437_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30438_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30439_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30440_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2811_m30441_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2811_TisObject_t_m30442_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2811_TisKeyValuePair_2_t2811_m30443_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWebCamDevice_t1219_m30444_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisWebCamDevice_t1219_m30445_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisWebCamDevice_t1219_m30446_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWebCamDevice_t1219_m30447_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWebCamDevice_t1219_m30448_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWebCamDevice_t1219_m30449_gshared ();
extern "C" void Array_InternalArray__Insert_TisWebCamDevice_t1219_m30450_gshared ();
extern "C" void Array_InternalArray__set_Item_TisWebCamDevice_t1219_m30451_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamDevice_t1219_m30452_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastResult_t647_m30458_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastResult_t647_m30459_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastResult_t647_m30460_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t647_m30461_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastResult_t647_m30462_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastResult_t647_m30463_gshared ();
extern "C" void Array_InternalArray__Insert_TisRaycastResult_t647_m30464_gshared ();
extern "C" void Array_InternalArray__set_Item_TisRaycastResult_t647_m30465_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t647_m30466_gshared ();
extern "C" void Array_Resize_TisRaycastResult_t647_m30467_gshared ();
extern "C" void Array_Resize_TisRaycastResult_t647_m30468_gshared ();
extern "C" void Array_IndexOf_TisRaycastResult_t647_m30469_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t647_m30470_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t647_TisRaycastResult_t647_m30471_gshared ();
extern "C" void Array_get_swapper_TisRaycastResult_t647_m30472_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t647_TisRaycastResult_t647_m30473_gshared ();
extern "C" void Array_compare_TisRaycastResult_t647_m30474_gshared ();
extern "C" void Array_swap_TisRaycastResult_t647_TisRaycastResult_t647_m30475_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t647_m30476_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t647_m30477_gshared ();
extern "C" void Array_swap_TisRaycastResult_t647_m30478_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit2D_t838_m30481_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit2D_t838_m30482_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t838_m30483_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t838_m30484_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t838_m30485_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit2D_t838_m30486_gshared ();
extern "C" void Array_InternalArray__Insert_TisRaycastHit2D_t838_m30487_gshared ();
extern "C" void Array_InternalArray__set_Item_TisRaycastHit2D_t838_m30488_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t838_m30489_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit_t566_m30490_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit_t566_m30491_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit_t566_m30492_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t566_m30493_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit_t566_m30494_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit_t566_m30495_gshared ();
extern "C" void Array_InternalArray__Insert_TisRaycastHit_t566_m30496_gshared ();
extern "C" void Array_InternalArray__set_Item_TisRaycastHit_t566_m30497_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t566_m30498_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t566_m30499_gshared ();
extern "C" void Array_qsort_TisRaycastHit_t566_m30500_gshared ();
extern "C" void Array_swap_TisRaycastHit_t566_m30501_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t5_m30502_gshared ();
extern "C" void Array_Resize_TisUIVertex_t727_m30503_gshared ();
extern "C" void Array_Resize_TisUIVertex_t727_m30504_gshared ();
extern "C" void Array_IndexOf_TisUIVertex_t727_m30505_gshared ();
extern "C" void Array_Sort_TisUIVertex_t727_m30506_gshared ();
extern "C" void Array_Sort_TisUIVertex_t727_TisUIVertex_t727_m30507_gshared ();
extern "C" void Array_get_swapper_TisUIVertex_t727_m30508_gshared ();
extern "C" void Array_qsort_TisUIVertex_t727_TisUIVertex_t727_m30509_gshared ();
extern "C" void Array_compare_TisUIVertex_t727_m30510_gshared ();
extern "C" void Array_swap_TisUIVertex_t727_TisUIVertex_t727_m30511_gshared ();
extern "C" void Array_Sort_TisUIVertex_t727_m30512_gshared ();
extern "C" void Array_qsort_TisUIVertex_t727_m30513_gshared ();
extern "C" void Array_swap_TisUIVertex_t727_m30514_gshared ();
extern "C" void Array_InternalArray__get_Item_TisContentType_t710_m30515_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisContentType_t710_m30516_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisContentType_t710_m30517_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisContentType_t710_m30518_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisContentType_t710_m30519_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisContentType_t710_m30520_gshared ();
extern "C" void Array_InternalArray__Insert_TisContentType_t710_m30521_gshared ();
extern "C" void Array_InternalArray__set_Item_TisContentType_t710_m30522_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisContentType_t710_m30523_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUILineInfo_t857_m30524_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisUILineInfo_t857_m30525_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUILineInfo_t857_m30526_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t857_m30527_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUILineInfo_t857_m30528_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUILineInfo_t857_m30529_gshared ();
extern "C" void Array_InternalArray__Insert_TisUILineInfo_t857_m30530_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUILineInfo_t857_m30531_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t857_m30532_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUICharInfo_t859_m30533_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisUICharInfo_t859_m30534_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUICharInfo_t859_m30535_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t859_m30536_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUICharInfo_t859_m30537_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUICharInfo_t859_m30538_gshared ();
extern "C" void Array_InternalArray__Insert_TisUICharInfo_t859_m30539_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUICharInfo_t859_m30540_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t859_m30541_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t36_m30542_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t29_m30543_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t41_m30544_gshared ();
extern "C" void Array_InternalArray__get_Item_TisEyewearCalibrationReading_t913_m30548_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisEyewearCalibrationReading_t913_m30549_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisEyewearCalibrationReading_t913_m30550_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisEyewearCalibrationReading_t913_m30551_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisEyewearCalibrationReading_t913_m30552_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisEyewearCalibrationReading_t913_m30553_gshared ();
extern "C" void Array_InternalArray__Insert_TisEyewearCalibrationReading_t913_m30554_gshared ();
extern "C" void Array_InternalArray__set_Item_TisEyewearCalibrationReading_t913_m30555_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisEyewearCalibrationReading_t913_m30556_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTargetSearchResult_t1050_m30558_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisTargetSearchResult_t1050_m30559_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTargetSearchResult_t1050_m30560_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTargetSearchResult_t1050_m30561_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTargetSearchResult_t1050_m30562_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTargetSearchResult_t1050_m30563_gshared ();
extern "C" void Array_InternalArray__Insert_TisTargetSearchResult_t1050_m30564_gshared ();
extern "C" void Array_InternalArray__set_Item_TisTargetSearchResult_t1050_m30565_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTargetSearchResult_t1050_m30566_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3060_m30568_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3060_m30569_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3060_m30570_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3060_m30571_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3060_m30572_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3060_m30573_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3060_m30574_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3060_m30575_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3060_m30576_gshared ();
extern "C" void Array_InternalArray__get_Item_TisPIXEL_FORMAT_t942_m30577_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisPIXEL_FORMAT_t942_m30578_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisPIXEL_FORMAT_t942_m30579_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisPIXEL_FORMAT_t942_m30580_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisPIXEL_FORMAT_t942_m30581_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisPIXEL_FORMAT_t942_m30582_gshared ();
extern "C" void Array_InternalArray__Insert_TisPIXEL_FORMAT_t942_m30583_gshared ();
extern "C" void Array_InternalArray__set_Item_TisPIXEL_FORMAT_t942_m30584_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisPIXEL_FORMAT_t942_m30585_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisPIXEL_FORMAT_t942_m30586_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisPIXEL_FORMAT_t942_TisObject_t_m30587_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisPIXEL_FORMAT_t942_TisPIXEL_FORMAT_t942_m30588_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30589_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30590_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30591_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3060_m30592_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3060_TisObject_t_m30593_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3060_TisKeyValuePair_2_t3060_m30594_gshared ();
extern "C" void Array_Resize_TisPIXEL_FORMAT_t942_m30595_gshared ();
extern "C" void Array_Resize_TisPIXEL_FORMAT_t942_m30596_gshared ();
extern "C" void Array_IndexOf_TisPIXEL_FORMAT_t942_m30597_gshared ();
extern "C" void Array_Sort_TisPIXEL_FORMAT_t942_m30598_gshared ();
extern "C" void Array_Sort_TisPIXEL_FORMAT_t942_TisPIXEL_FORMAT_t942_m30599_gshared ();
extern "C" void Array_get_swapper_TisPIXEL_FORMAT_t942_m30600_gshared ();
extern "C" void Array_qsort_TisPIXEL_FORMAT_t942_TisPIXEL_FORMAT_t942_m30601_gshared ();
extern "C" void Array_compare_TisPIXEL_FORMAT_t942_m30602_gshared ();
extern "C" void Array_swap_TisPIXEL_FORMAT_t942_TisPIXEL_FORMAT_t942_m30603_gshared ();
extern "C" void Array_Sort_TisPIXEL_FORMAT_t942_m30604_gshared ();
extern "C" void Array_qsort_TisPIXEL_FORMAT_t942_m30605_gshared ();
extern "C" void Array_swap_TisPIXEL_FORMAT_t942_m30606_gshared ();
extern "C" void Array_InternalArray__get_Item_TisColor32_t829_m30607_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisColor32_t829_m30608_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisColor32_t829_m30609_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisColor32_t829_m30610_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisColor32_t829_m30611_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisColor32_t829_m30612_gshared ();
extern "C" void Array_InternalArray__Insert_TisColor32_t829_m30613_gshared ();
extern "C" void Array_InternalArray__set_Item_TisColor32_t829_m30614_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisColor32_t829_m30615_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTrackableResultData_t969_m30618_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisTrackableResultData_t969_m30619_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTrackableResultData_t969_m30620_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTrackableResultData_t969_m30621_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTrackableResultData_t969_m30622_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTrackableResultData_t969_m30623_gshared ();
extern "C" void Array_InternalArray__Insert_TisTrackableResultData_t969_m30624_gshared ();
extern "C" void Array_InternalArray__set_Item_TisTrackableResultData_t969_m30625_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTrackableResultData_t969_m30626_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWordData_t974_m30627_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisWordData_t974_m30628_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisWordData_t974_m30629_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWordData_t974_m30630_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWordData_t974_m30631_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWordData_t974_m30632_gshared ();
extern "C" void Array_InternalArray__Insert_TisWordData_t974_m30633_gshared ();
extern "C" void Array_InternalArray__set_Item_TisWordData_t974_m30634_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWordData_t974_m30635_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWordResultData_t973_m30636_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisWordResultData_t973_m30637_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisWordResultData_t973_m30638_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWordResultData_t973_m30639_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWordResultData_t973_m30640_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWordResultData_t973_m30641_gshared ();
extern "C" void Array_InternalArray__Insert_TisWordResultData_t973_m30642_gshared ();
extern "C" void Array_InternalArray__set_Item_TisWordResultData_t973_m30643_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWordResultData_t973_m30644_gshared ();
extern "C" void Array_Resize_TisInt32_t59_m30645_gshared ();
extern "C" void Array_Resize_TisInt32_t59_m30646_gshared ();
extern "C" void Array_IndexOf_TisInt32_t59_m30647_gshared ();
extern "C" void Array_Sort_TisInt32_t59_m30648_gshared ();
extern "C" void Array_Sort_TisInt32_t59_TisInt32_t59_m30649_gshared ();
extern "C" void Array_get_swapper_TisInt32_t59_m30650_gshared ();
extern "C" void Array_qsort_TisInt32_t59_TisInt32_t59_m30651_gshared ();
extern "C" void Array_compare_TisInt32_t59_m30652_gshared ();
extern "C" void Array_swap_TisInt32_t59_TisInt32_t59_m30653_gshared ();
extern "C" void Array_Sort_TisInt32_t59_m30654_gshared ();
extern "C" void Array_qsort_TisInt32_t59_m30655_gshared ();
extern "C" void Array_swap_TisInt32_t59_m30656_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t977_m30657_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisSmartTerrainRevisionData_t977_m30658_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSmartTerrainRevisionData_t977_m30659_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSmartTerrainRevisionData_t977_m30660_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSmartTerrainRevisionData_t977_m30661_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSmartTerrainRevisionData_t977_m30662_gshared ();
extern "C" void Array_InternalArray__Insert_TisSmartTerrainRevisionData_t977_m30663_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSmartTerrainRevisionData_t977_m30664_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainRevisionData_t977_m30665_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSurfaceData_t978_m30666_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisSurfaceData_t978_m30667_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSurfaceData_t978_m30668_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSurfaceData_t978_m30669_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSurfaceData_t978_m30670_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSurfaceData_t978_m30671_gshared ();
extern "C" void Array_InternalArray__Insert_TisSurfaceData_t978_m30672_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSurfaceData_t978_m30673_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSurfaceData_t978_m30674_gshared ();
extern "C" void Array_InternalArray__get_Item_TisPropData_t979_m30675_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisPropData_t979_m30676_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisPropData_t979_m30677_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisPropData_t979_m30678_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisPropData_t979_m30679_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisPropData_t979_m30680_gshared ();
extern "C" void Array_InternalArray__Insert_TisPropData_t979_m30681_gshared ();
extern "C" void Array_InternalArray__set_Item_TisPropData_t979_m30682_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisPropData_t979_m30683_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3151_m30685_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3151_m30686_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3151_m30687_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3151_m30688_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3151_m30689_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3151_m30690_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3151_m30691_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3151_m30692_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3151_m30693_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt16_t562_m30694_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisUInt16_t562_m30695_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt16_t562_m30696_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt16_t562_m30697_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt16_t562_m30698_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt16_t562_m30699_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt16_t562_m30700_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt16_t562_m30701_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t562_m30702_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30703_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30704_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t562_m30705_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt16_t562_TisObject_t_m30706_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt16_t562_TisUInt16_t562_m30707_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30708_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3151_m30709_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3151_TisObject_t_m30710_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3151_TisKeyValuePair_2_t3151_m30711_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRectangleData_t923_m30712_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisRectangleData_t923_m30713_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisRectangleData_t923_m30714_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRectangleData_t923_m30715_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRectangleData_t923_m30716_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRectangleData_t923_m30717_gshared ();
extern "C" void Array_InternalArray__Insert_TisRectangleData_t923_m30718_gshared ();
extern "C" void Array_InternalArray__set_Item_TisRectangleData_t923_m30719_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRectangleData_t923_m30720_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3240_m30721_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3240_m30722_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3240_m30723_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3240_m30724_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3240_m30725_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3240_m30726_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3240_m30727_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3240_m30728_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3240_m30729_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30730_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30731_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30732_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisTrackableResultData_t969_m30733_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisTrackableResultData_t969_TisObject_t_m30734_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisTrackableResultData_t969_TisTrackableResultData_t969_m30735_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30736_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3240_m30737_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3240_TisObject_t_m30738_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3240_TisKeyValuePair_2_t3240_m30739_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3255_m30740_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3255_m30741_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3255_m30742_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3255_m30743_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3255_m30744_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3255_m30745_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3255_m30746_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3255_m30747_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3255_m30748_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVirtualButtonData_t970_m30749_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisVirtualButtonData_t970_m30750_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisVirtualButtonData_t970_m30751_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVirtualButtonData_t970_m30752_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVirtualButtonData_t970_m30753_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVirtualButtonData_t970_m30754_gshared ();
extern "C" void Array_InternalArray__Insert_TisVirtualButtonData_t970_m30755_gshared ();
extern "C" void Array_InternalArray__set_Item_TisVirtualButtonData_t970_m30756_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVirtualButtonData_t970_m30757_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30758_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30759_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30760_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisVirtualButtonData_t970_m30761_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisVirtualButtonData_t970_TisObject_t_m30762_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisVirtualButtonData_t970_TisVirtualButtonData_t970_m30763_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30764_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3255_m30765_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3255_TisObject_t_m30766_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3255_TisKeyValuePair_2_t3255_m30767_gshared ();
extern "C" void Array_Resize_TisTargetSearchResult_t1050_m30768_gshared ();
extern "C" void Array_Resize_TisTargetSearchResult_t1050_m30769_gshared ();
extern "C" void Array_IndexOf_TisTargetSearchResult_t1050_m30770_gshared ();
extern "C" void Array_Sort_TisTargetSearchResult_t1050_m30771_gshared ();
extern "C" void Array_Sort_TisTargetSearchResult_t1050_TisTargetSearchResult_t1050_m30772_gshared ();
extern "C" void Array_get_swapper_TisTargetSearchResult_t1050_m30773_gshared ();
extern "C" void Array_qsort_TisTargetSearchResult_t1050_TisTargetSearchResult_t1050_m30774_gshared ();
extern "C" void Array_compare_TisTargetSearchResult_t1050_m30775_gshared ();
extern "C" void Array_swap_TisTargetSearchResult_t1050_TisTargetSearchResult_t1050_m30776_gshared ();
extern "C" void Array_Sort_TisTargetSearchResult_t1050_m30777_gshared ();
extern "C" void Array_qsort_TisTargetSearchResult_t1050_m30778_gshared ();
extern "C" void Array_swap_TisTargetSearchResult_t1050_m30779_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3292_m30783_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3292_m30784_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3292_m30785_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3292_m30786_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3292_m30787_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3292_m30788_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3292_m30789_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3292_m30790_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3292_m30791_gshared ();
extern "C" void Array_InternalArray__get_Item_TisProfileData_t1064_m30792_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisProfileData_t1064_m30793_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisProfileData_t1064_m30794_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisProfileData_t1064_m30795_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisProfileData_t1064_m30796_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisProfileData_t1064_m30797_gshared ();
extern "C" void Array_InternalArray__Insert_TisProfileData_t1064_m30798_gshared ();
extern "C" void Array_InternalArray__set_Item_TisProfileData_t1064_m30799_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisProfileData_t1064_m30800_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30801_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30802_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t1064_m30803_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisProfileData_t1064_TisObject_t_m30804_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisProfileData_t1064_TisProfileData_t1064_m30805_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30806_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3292_m30807_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3292_TisObject_t_m30808_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3292_TisKeyValuePair_2_t3292_m30809_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t3340_m30810_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t3340_m30811_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t3340_m30812_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t3340_m30813_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t3340_m30814_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t3340_m30815_gshared ();
extern "C" void Array_InternalArray__Insert_TisLink_t3340_m30816_gshared ();
extern "C" void Array_InternalArray__set_Item_TisLink_t3340_m30817_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3340_m30818_gshared ();
extern "C" void Array_InternalArray__get_Item_TisParameterModifier_t2119_m30819_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisParameterModifier_t2119_m30820_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisParameterModifier_t2119_m30821_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t2119_m30822_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisParameterModifier_t2119_m30823_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisParameterModifier_t2119_m30824_gshared ();
extern "C" void Array_InternalArray__Insert_TisParameterModifier_t2119_m30825_gshared ();
extern "C" void Array_InternalArray__set_Item_TisParameterModifier_t2119_m30826_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t2119_m30827_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcAchievementData_t1244_m30828_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisGcAchievementData_t1244_m30829_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisGcAchievementData_t1244_m30830_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t1244_m30831_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcAchievementData_t1244_m30832_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcAchievementData_t1244_m30833_gshared ();
extern "C" void Array_InternalArray__Insert_TisGcAchievementData_t1244_m30834_gshared ();
extern "C" void Array_InternalArray__set_Item_TisGcAchievementData_t1244_m30835_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t1244_m30836_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcScoreData_t1245_m30837_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisGcScoreData_t1245_m30838_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisGcScoreData_t1245_m30839_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t1245_m30840_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcScoreData_t1245_m30841_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcScoreData_t1245_m30842_gshared ();
extern "C" void Array_InternalArray__Insert_TisGcScoreData_t1245_m30843_gshared ();
extern "C" void Array_InternalArray__set_Item_TisGcScoreData_t1245_m30844_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t1245_m30845_gshared ();
extern "C" void Array_InternalArray__get_Item_TisHitInfo_t1271_m30846_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisHitInfo_t1271_m30847_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisHitInfo_t1271_m30848_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisHitInfo_t1271_m30849_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisHitInfo_t1271_m30850_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisHitInfo_t1271_m30851_gshared ();
extern "C" void Array_InternalArray__Insert_TisHitInfo_t1271_m30852_gshared ();
extern "C" void Array_InternalArray__set_Item_TisHitInfo_t1271_m30853_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t1271_m30854_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3373_m30855_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3373_m30856_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3373_m30857_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3373_m30858_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3373_m30859_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3373_m30860_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3373_m30861_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3373_m30862_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3373_m30863_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTextEditOp_t1286_m30864_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisTextEditOp_t1286_m30865_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTextEditOp_t1286_m30866_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTextEditOp_t1286_m30867_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTextEditOp_t1286_m30868_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTextEditOp_t1286_m30869_gshared ();
extern "C" void Array_InternalArray__Insert_TisTextEditOp_t1286_m30870_gshared ();
extern "C" void Array_InternalArray__set_Item_TisTextEditOp_t1286_m30871_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTextEditOp_t1286_m30872_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30873_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30874_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisTextEditOp_t1286_m30875_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisTextEditOp_t1286_TisObject_t_m30876_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisTextEditOp_t1286_TisTextEditOp_t1286_m30877_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30878_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3373_m30879_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3373_TisObject_t_m30880_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3373_TisKeyValuePair_2_t3373_m30881_gshared ();
extern "C" void Array_Resize_TisUICharInfo_t859_m30882_gshared ();
extern "C" void Array_Resize_TisUICharInfo_t859_m30883_gshared ();
extern "C" void Array_IndexOf_TisUICharInfo_t859_m30884_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t859_m30885_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t859_TisUICharInfo_t859_m30886_gshared ();
extern "C" void Array_get_swapper_TisUICharInfo_t859_m30887_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t859_TisUICharInfo_t859_m30888_gshared ();
extern "C" void Array_compare_TisUICharInfo_t859_m30889_gshared ();
extern "C" void Array_swap_TisUICharInfo_t859_TisUICharInfo_t859_m30890_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t859_m30891_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t859_m30892_gshared ();
extern "C" void Array_swap_TisUICharInfo_t859_m30893_gshared ();
extern "C" void Array_Resize_TisUILineInfo_t857_m30894_gshared ();
extern "C" void Array_Resize_TisUILineInfo_t857_m30895_gshared ();
extern "C" void Array_IndexOf_TisUILineInfo_t857_m30896_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t857_m30897_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t857_TisUILineInfo_t857_m30898_gshared ();
extern "C" void Array_get_swapper_TisUILineInfo_t857_m30899_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t857_TisUILineInfo_t857_m30900_gshared ();
extern "C" void Array_compare_TisUILineInfo_t857_m30901_gshared ();
extern "C" void Array_swap_TisUILineInfo_t857_TisUILineInfo_t857_m30902_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t857_m30903_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t857_m30904_gshared ();
extern "C" void Array_swap_TisUILineInfo_t857_m30905_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t59_m30906_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyframe_t1321_m30907_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyframe_t1321_m30908_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyframe_t1321_m30909_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyframe_t1321_m30910_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyframe_t1321_m30911_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyframe_t1321_m30912_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyframe_t1321_m30913_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyframe_t1321_m30914_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t1321_m30915_gshared ();
extern "C" void Array_InternalArray__get_Item_TisIntPtr_t_m30916_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisIntPtr_t_m30917_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisIntPtr_t_m30918_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m30919_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisIntPtr_t_m30920_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisIntPtr_t_m30921_gshared ();
extern "C" void Array_InternalArray__Insert_TisIntPtr_t_m30922_gshared ();
extern "C" void Array_InternalArray__set_Item_TisIntPtr_t_m30923_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m30924_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3478_m30925_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3478_m30926_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3478_m30927_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3478_m30928_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3478_m30929_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3478_m30930_gshared ();
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3478_m30931_gshared ();
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3478_m30932_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3478_m30933_gshared ();
extern "C" void Array_InternalArray__get_Item_TisBoolean_t41_m30934_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisBoolean_t41_m30935_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisBoolean_t41_m30936_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisBoolean_t41_m30937_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisBoolean_t41_m30938_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisBoolean_t41_m30939_gshared ();
extern "C" void Array_InternalArray__Insert_TisBoolean_t41_m30940_gshared ();
extern "C" void Array_InternalArray__set_Item_TisBoolean_t41_m30941_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisBoolean_t41_m30942_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30943_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30944_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisBoolean_t41_m30945_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisBoolean_t41_TisObject_t_m30946_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisBoolean_t41_TisBoolean_t41_m30947_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30948_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3478_m30949_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3478_TisObject_t_m30950_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3478_TisKeyValuePair_2_t3478_m30951_gshared ();
extern "C" void Array_InternalArray__get_Item_TisX509ChainStatus_t1558_m30952_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1558_m30953_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1558_m30954_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1558_m30955_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1558_m30956_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisX509ChainStatus_t1558_m30957_gshared ();
extern "C" void Array_InternalArray__Insert_TisX509ChainStatus_t1558_m30958_gshared ();
extern "C" void Array_InternalArray__set_Item_TisX509ChainStatus_t1558_m30959_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1558_m30960_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t59_m30961_gshared ();
extern "C" void Array_InternalArray__get_Item_TisMark_t1605_m30962_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisMark_t1605_m30963_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisMark_t1605_m30964_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisMark_t1605_m30965_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisMark_t1605_m30966_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisMark_t1605_m30967_gshared ();
extern "C" void Array_InternalArray__Insert_TisMark_t1605_m30968_gshared ();
extern "C" void Array_InternalArray__set_Item_TisMark_t1605_m30969_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1605_m30970_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUriScheme_t1641_m30971_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisUriScheme_t1641_m30972_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUriScheme_t1641_m30973_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1641_m30974_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUriScheme_t1641_m30975_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUriScheme_t1641_m30976_gshared ();
extern "C" void Array_InternalArray__Insert_TisUriScheme_t1641_m30977_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUriScheme_t1641_m30978_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1641_m30979_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt32_t558_m30980_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisUInt32_t558_m30981_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt32_t558_m30982_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt32_t558_m30983_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt32_t558_m30984_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt32_t558_m30985_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt32_t558_m30986_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt32_t558_m30987_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t558_m30988_gshared ();
extern "C" void Array_InternalArray__get_Item_TisClientCertificateType_t1784_m30989_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisClientCertificateType_t1784_m30990_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisClientCertificateType_t1784_m30991_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisClientCertificateType_t1784_m30992_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisClientCertificateType_t1784_m30993_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisClientCertificateType_t1784_m30994_gshared ();
extern "C" void Array_InternalArray__Insert_TisClientCertificateType_t1784_m30995_gshared ();
extern "C" void Array_InternalArray__set_Item_TisClientCertificateType_t1784_m30996_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisClientCertificateType_t1784_m30997_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt64_t563_m30998_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisUInt64_t563_m30999_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt64_t563_m31000_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt64_t563_m31001_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt64_t563_m31002_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt64_t563_m31003_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt64_t563_m31004_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt64_t563_m31005_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t563_m31006_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt16_t561_m31007_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisInt16_t561_m31008_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt16_t561_m31009_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt16_t561_m31010_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt16_t561_m31011_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt16_t561_m31012_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt16_t561_m31013_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt16_t561_m31014_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t561_m31138_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSByte_t559_m31139_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisSByte_t559_m31140_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSByte_t559_m31141_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSByte_t559_m31142_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSByte_t559_m31143_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSByte_t559_m31144_gshared ();
extern "C" void Array_InternalArray__Insert_TisSByte_t559_m31145_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSByte_t559_m31146_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t559_m31147_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt64_t507_m31148_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t507_m31149_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt64_t507_m31150_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t507_m31151_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt64_t507_m31152_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt64_t507_m31153_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt64_t507_m31154_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt64_t507_m31155_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t507_m31156_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTableRange_t1889_m31184_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t1889_m31185_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTableRange_t1889_m31186_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t1889_m31187_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTableRange_t1889_m31188_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTableRange_t1889_m31189_gshared ();
extern "C" void Array_InternalArray__Insert_TisTableRange_t1889_m31190_gshared ();
extern "C" void Array_InternalArray__set_Item_TisTableRange_t1889_m31191_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1889_m31192_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t1966_m31193_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1966_m31194_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t1966_m31195_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1966_m31196_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t1966_m31197_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t1966_m31198_gshared ();
extern "C" void Array_InternalArray__Insert_TisSlot_t1966_m31199_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSlot_t1966_m31200_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1966_m31201_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t1974_m31202_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1974_m31203_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t1974_m31204_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1974_m31205_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t1974_m31206_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t1974_m31207_gshared ();
extern "C" void Array_InternalArray__Insert_TisSlot_t1974_m31208_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSlot_t1974_m31209_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1974_m31210_gshared ();
extern "C" void Array_InternalArray__get_Item_TisILTokenInfo_t2053_m31211_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisILTokenInfo_t2053_m31212_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisILTokenInfo_t2053_m31213_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t2053_m31214_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisILTokenInfo_t2053_m31215_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisILTokenInfo_t2053_m31216_gshared ();
extern "C" void Array_InternalArray__Insert_TisILTokenInfo_t2053_m31217_gshared ();
extern "C" void Array_InternalArray__set_Item_TisILTokenInfo_t2053_m31218_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t2053_m31219_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLabelData_t2055_m31220_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisLabelData_t2055_m31221_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisLabelData_t2055_m31222_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelData_t2055_m31223_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLabelData_t2055_m31224_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLabelData_t2055_m31225_gshared ();
extern "C" void Array_InternalArray__Insert_TisLabelData_t2055_m31226_gshared ();
extern "C" void Array_InternalArray__set_Item_TisLabelData_t2055_m31227_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t2055_m31228_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLabelFixup_t2054_m31229_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisLabelFixup_t2054_m31230_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisLabelFixup_t2054_m31231_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t2054_m31232_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLabelFixup_t2054_m31233_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLabelFixup_t2054_m31234_gshared ();
extern "C" void Array_InternalArray__Insert_TisLabelFixup_t2054_m31235_gshared ();
extern "C" void Array_InternalArray__set_Item_TisLabelFixup_t2054_m31236_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t2054_m31237_gshared ();
extern "C" void Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2101_m31238_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t2101_m31239_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t2101_m31240_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t2101_m31241_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t2101_m31242_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t2101_m31243_gshared ();
extern "C" void Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t2101_m31244_gshared ();
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t2101_m31245_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t2101_m31246_gshared ();
extern "C" void Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t2100_m31247_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t2100_m31248_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t2100_m31249_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t2100_m31250_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t2100_m31251_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t2100_m31252_gshared ();
extern "C" void Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t2100_m31253_gshared ();
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t2100_m31254_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t2100_m31255_gshared ();
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t2101_m31256_gshared ();
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t2101_m31257_gshared ();
extern "C" void Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31258_gshared ();
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_m31259_gshared ();
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31260_gshared ();
extern "C" void Array_get_swapper_TisCustomAttributeTypedArgument_t2101_m31261_gshared ();
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31262_gshared ();
extern "C" void Array_compare_TisCustomAttributeTypedArgument_t2101_m31263_gshared ();
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31264_gshared ();
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_m31265_gshared ();
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t2101_m31266_gshared ();
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t2101_m31267_gshared ();
extern "C" void Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31268_gshared ();
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t2100_m31269_gshared ();
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t2100_m31270_gshared ();
extern "C" void Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31271_gshared ();
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_m31272_gshared ();
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31273_gshared ();
extern "C" void Array_get_swapper_TisCustomAttributeNamedArgument_t2100_m31274_gshared ();
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31275_gshared ();
extern "C" void Array_compare_TisCustomAttributeNamedArgument_t2100_m31276_gshared ();
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31277_gshared ();
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_m31278_gshared ();
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t2100_m31279_gshared ();
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t2100_m31280_gshared ();
extern "C" void Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31281_gshared ();
extern "C" void Array_InternalArray__get_Item_TisResourceInfo_t2130_m31285_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisResourceInfo_t2130_m31286_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisResourceInfo_t2130_m31287_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t2130_m31288_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisResourceInfo_t2130_m31289_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisResourceInfo_t2130_m31290_gshared ();
extern "C" void Array_InternalArray__Insert_TisResourceInfo_t2130_m31291_gshared ();
extern "C" void Array_InternalArray__set_Item_TisResourceInfo_t2130_m31292_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t2130_m31293_gshared ();
extern "C" void Array_InternalArray__get_Item_TisResourceCacheItem_t2131_m31294_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisResourceCacheItem_t2131_m31295_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t2131_m31296_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t2131_m31297_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t2131_m31298_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisResourceCacheItem_t2131_m31299_gshared ();
extern "C" void Array_InternalArray__Insert_TisResourceCacheItem_t2131_m31300_gshared ();
extern "C" void Array_InternalArray__set_Item_TisResourceCacheItem_t2131_m31301_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t2131_m31302_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDateTime_t220_m31303_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t220_m31304_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisDateTime_t220_m31305_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t220_m31306_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDateTime_t220_m31307_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDateTime_t220_m31308_gshared ();
extern "C" void Array_InternalArray__Insert_TisDateTime_t220_m31309_gshared ();
extern "C" void Array_InternalArray__set_Item_TisDateTime_t220_m31310_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t220_m31311_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDecimal_t564_m31312_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t564_m31313_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisDecimal_t564_m31314_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t564_m31315_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDecimal_t564_m31316_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDecimal_t564_m31317_gshared ();
extern "C" void Array_InternalArray__Insert_TisDecimal_t564_m31318_gshared ();
extern "C" void Array_InternalArray__set_Item_TisDecimal_t564_m31319_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t564_m31320_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTimeSpan_t565_m31321_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t565_m31322_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTimeSpan_t565_m31323_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t565_m31324_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTimeSpan_t565_m31325_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTimeSpan_t565_m31326_gshared ();
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t565_m31327_gshared ();
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t565_m31328_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t565_m31329_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTypeTag_t2267_m31330_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisTypeTag_t2267_m31331_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTypeTag_t2267_m31332_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTypeTag_t2267_m31333_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTypeTag_t2267_m31334_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTypeTag_t2267_m31335_gshared ();
extern "C" void Array_InternalArray__Insert_TisTypeTag_t2267_m31336_gshared ();
extern "C" void Array_InternalArray__set_Item_TisTypeTag_t2267_m31337_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t2267_m31338_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15645_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15646_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15647_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15648_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15649_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15650_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15651_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15652_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15653_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15654_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15655_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15656_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15657_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15658_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15659_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15660_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15661_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15662_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15710_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15711_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15712_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15713_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15714_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15715_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15716_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15717_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15718_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15719_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15720_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15721_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15722_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15723_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15724_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15725_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15726_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15727_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15728_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15729_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15730_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15731_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15732_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15733_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15734_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15735_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15736_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15737_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15738_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15739_gshared ();
extern "C" void Dictionary_2__ctor_m15742_gshared ();
extern "C" void Dictionary_2__ctor_m15745_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m15747_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m15749_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m15751_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m15753_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m15755_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m15757_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15759_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15761_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15763_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15765_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15767_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15769_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15771_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m15773_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15775_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15777_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15779_gshared ();
extern "C" void Dictionary_2_get_Count_m15781_gshared ();
extern "C" void Dictionary_2_get_Item_m15783_gshared ();
extern "C" void Dictionary_2_set_Item_m15785_gshared ();
extern "C" void Dictionary_2_Init_m15787_gshared ();
extern "C" void Dictionary_2_InitArrays_m15789_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m15791_gshared ();
extern "C" void Dictionary_2_make_pair_m15793_gshared ();
extern "C" void Dictionary_2_pick_key_m15795_gshared ();
extern "C" void Dictionary_2_pick_value_m15797_gshared ();
extern "C" void Dictionary_2_CopyTo_m15799_gshared ();
extern "C" void Dictionary_2_Resize_m15801_gshared ();
extern "C" void Dictionary_2_Add_m15803_gshared ();
extern "C" void Dictionary_2_Clear_m15805_gshared ();
extern "C" void Dictionary_2_ContainsKey_m15807_gshared ();
extern "C" void Dictionary_2_ContainsValue_m15809_gshared ();
extern "C" void Dictionary_2_GetObjectData_m15811_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m15813_gshared ();
extern "C" void Dictionary_2_Remove_m15815_gshared ();
extern "C" void Dictionary_2_TryGetValue_m15817_gshared ();
extern "C" void Dictionary_2_get_Keys_m15819_gshared ();
extern "C" void Dictionary_2_get_Values_m15821_gshared ();
extern "C" void Dictionary_2_ToTKey_m15823_gshared ();
extern "C" void Dictionary_2_ToTValue_m15825_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m15827_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m15829_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m15831_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15832_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15833_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15834_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15835_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15836_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15837_gshared ();
extern "C" void KeyValuePair_2__ctor_m15838_gshared ();
extern "C" void KeyValuePair_2_get_Key_m15839_gshared ();
extern "C" void KeyValuePair_2_set_Key_m15840_gshared ();
extern "C" void KeyValuePair_2_get_Value_m15841_gshared ();
extern "C" void KeyValuePair_2_set_Value_m15842_gshared ();
extern "C" void KeyValuePair_2_ToString_m15843_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15850_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15851_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15852_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15853_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15854_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15855_gshared ();
extern "C" void KeyCollection__ctor_m15856_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15857_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15858_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15859_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15860_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15861_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m15862_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15863_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15864_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15865_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m15866_gshared ();
extern "C" void KeyCollection_CopyTo_m15867_gshared ();
extern "C" void KeyCollection_GetEnumerator_m15868_gshared ();
extern "C" void KeyCollection_get_Count_m15869_gshared ();
extern "C" void Enumerator__ctor_m15870_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15871_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15872_gshared ();
extern "C" void Enumerator_Dispose_m15873_gshared ();
extern "C" void Enumerator_MoveNext_m15874_gshared ();
extern "C" void Enumerator_get_Current_m15875_gshared ();
extern "C" void Enumerator__ctor_m15876_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15877_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15878_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15879_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15880_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15881_gshared ();
extern "C" void Enumerator_MoveNext_m15882_gshared ();
extern "C" void Enumerator_get_Current_m15883_gshared ();
extern "C" void Enumerator_get_CurrentKey_m15884_gshared ();
extern "C" void Enumerator_get_CurrentValue_m15885_gshared ();
extern "C" void Enumerator_Reset_m15886_gshared ();
extern "C" void Enumerator_VerifyState_m15887_gshared ();
extern "C" void Enumerator_VerifyCurrent_m15888_gshared ();
extern "C" void Enumerator_Dispose_m15889_gshared ();
extern "C" void Transform_1__ctor_m15890_gshared ();
extern "C" void Transform_1_Invoke_m15891_gshared ();
extern "C" void Transform_1_BeginInvoke_m15892_gshared ();
extern "C" void Transform_1_EndInvoke_m15893_gshared ();
extern "C" void ValueCollection__ctor_m15894_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15895_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15896_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15897_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15898_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15899_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m15900_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15901_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15902_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15903_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m15904_gshared ();
extern "C" void ValueCollection_CopyTo_m15905_gshared ();
extern "C" void ValueCollection_GetEnumerator_m15906_gshared ();
extern "C" void ValueCollection_get_Count_m15907_gshared ();
extern "C" void Enumerator__ctor_m15908_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15909_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15910_gshared ();
extern "C" void Enumerator_Dispose_m15911_gshared ();
extern "C" void Enumerator_MoveNext_m15912_gshared ();
extern "C" void Enumerator_get_Current_m15913_gshared ();
extern "C" void Transform_1__ctor_m15914_gshared ();
extern "C" void Transform_1_Invoke_m15915_gshared ();
extern "C" void Transform_1_BeginInvoke_m15916_gshared ();
extern "C" void Transform_1_EndInvoke_m15917_gshared ();
extern "C" void Transform_1__ctor_m15918_gshared ();
extern "C" void Transform_1_Invoke_m15919_gshared ();
extern "C" void Transform_1_BeginInvoke_m15920_gshared ();
extern "C" void Transform_1_EndInvoke_m15921_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15922_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15923_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15925_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15926_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15927_gshared ();
extern "C" void Transform_1__ctor_m15928_gshared ();
extern "C" void Transform_1_Invoke_m15929_gshared ();
extern "C" void Transform_1_BeginInvoke_m15930_gshared ();
extern "C" void Transform_1_EndInvoke_m15931_gshared ();
extern "C" void ShimEnumerator__ctor_m15932_gshared ();
extern "C" void ShimEnumerator_MoveNext_m15933_gshared ();
extern "C" void ShimEnumerator_get_Entry_m15934_gshared ();
extern "C" void ShimEnumerator_get_Key_m15935_gshared ();
extern "C" void ShimEnumerator_get_Value_m15936_gshared ();
extern "C" void ShimEnumerator_get_Current_m15937_gshared ();
extern "C" void ShimEnumerator_Reset_m15938_gshared ();
extern "C" void EqualityComparer_1__ctor_m15939_gshared ();
extern "C" void EqualityComparer_1__cctor_m15940_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15941_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15942_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15943_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m15944_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m15945_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m15946_gshared ();
extern "C" void DefaultComparer__ctor_m15947_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15948_gshared ();
extern "C" void DefaultComparer_Equals_m15949_gshared ();
extern "C" void Nullable_1__ctor_m16002_gshared ();
extern "C" void Nullable_1_Equals_m16003_gshared ();
extern "C" void Nullable_1_Equals_m16004_gshared ();
extern "C" void Nullable_1_GetHashCode_m16005_gshared ();
extern "C" void Nullable_1_GetValueOrDefault_m16006_gshared ();
extern "C" void Nullable_1_ToString_m16007_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16113_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16114_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16115_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16116_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16117_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16118_gshared ();
extern "C" void Transform_1__ctor_m16183_gshared ();
extern "C" void Transform_1_Invoke_m16184_gshared ();
extern "C" void Transform_1_BeginInvoke_m16185_gshared ();
extern "C" void Transform_1_EndInvoke_m16186_gshared ();
extern "C" void Transform_1__ctor_m16187_gshared ();
extern "C" void Transform_1_Invoke_m16188_gshared ();
extern "C" void Transform_1_BeginInvoke_m16189_gshared ();
extern "C" void Transform_1_EndInvoke_m16190_gshared ();
extern "C" void Dictionary_2__ctor_m16251_gshared ();
extern "C" void Dictionary_2__ctor_m16253_gshared ();
extern "C" void Dictionary_2__ctor_m16255_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m16257_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16259_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16261_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16263_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16265_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16267_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16269_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16271_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16273_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16275_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16277_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16279_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16281_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16283_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16285_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16287_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16289_gshared ();
extern "C" void Dictionary_2_get_Count_m16291_gshared ();
extern "C" void Dictionary_2_get_Item_m16293_gshared ();
extern "C" void Dictionary_2_set_Item_m16295_gshared ();
extern "C" void Dictionary_2_Init_m16297_gshared ();
extern "C" void Dictionary_2_InitArrays_m16299_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16301_gshared ();
extern "C" void Dictionary_2_make_pair_m16303_gshared ();
extern "C" void Dictionary_2_pick_key_m16305_gshared ();
extern "C" void Dictionary_2_pick_value_m16307_gshared ();
extern "C" void Dictionary_2_CopyTo_m16309_gshared ();
extern "C" void Dictionary_2_Resize_m16311_gshared ();
extern "C" void Dictionary_2_Add_m16313_gshared ();
extern "C" void Dictionary_2_Clear_m16315_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16317_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16321_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16323_gshared ();
extern "C" void Dictionary_2_Remove_m16325_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16327_gshared ();
extern "C" void Dictionary_2_ToTKey_m16333_gshared ();
extern "C" void Dictionary_2_ToTValue_m16335_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16337_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16341_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16387_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16388_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16389_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16390_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16391_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16392_gshared ();
extern "C" void KeyValuePair_2__ctor_m16393_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16395_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16397_gshared ();
extern "C" void KeyCollection__ctor_m16399_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16400_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16401_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16402_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16403_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16404_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16405_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16406_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16407_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16408_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16409_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16411_gshared ();
extern "C" void KeyCollection_get_Count_m16412_gshared ();
extern "C" void Enumerator__ctor_m16413_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16414_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m16415_gshared ();
extern "C" void Enumerator_Dispose_m16416_gshared ();
extern "C" void Enumerator_MoveNext_m16417_gshared ();
extern "C" void Enumerator_get_Current_m16418_gshared ();
extern "C" void Enumerator__ctor_m16419_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16420_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m16421_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16422_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16423_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16424_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16427_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16428_gshared ();
extern "C" void Enumerator_Reset_m16429_gshared ();
extern "C" void Enumerator_VerifyState_m16430_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16431_gshared ();
extern "C" void Transform_1__ctor_m16433_gshared ();
extern "C" void Transform_1_Invoke_m16434_gshared ();
extern "C" void Transform_1_BeginInvoke_m16435_gshared ();
extern "C" void Transform_1_EndInvoke_m16436_gshared ();
extern "C" void ValueCollection__ctor_m16437_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16438_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16439_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16440_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16441_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16442_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16443_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16444_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16445_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16446_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16447_gshared ();
extern "C" void ValueCollection_CopyTo_m16448_gshared ();
extern "C" void ValueCollection_get_Count_m16450_gshared ();
extern "C" void Enumerator__ctor_m16451_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16452_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m16453_gshared ();
extern "C" void Transform_1__ctor_m16457_gshared ();
extern "C" void Transform_1_Invoke_m16458_gshared ();
extern "C" void Transform_1_BeginInvoke_m16459_gshared ();
extern "C" void Transform_1_EndInvoke_m16460_gshared ();
extern "C" void Transform_1__ctor_m16461_gshared ();
extern "C" void Transform_1_Invoke_m16462_gshared ();
extern "C" void Transform_1_BeginInvoke_m16463_gshared ();
extern "C" void Transform_1_EndInvoke_m16464_gshared ();
extern "C" void Transform_1__ctor_m16465_gshared ();
extern "C" void Transform_1_Invoke_m16466_gshared ();
extern "C" void Transform_1_BeginInvoke_m16467_gshared ();
extern "C" void Transform_1_EndInvoke_m16468_gshared ();
extern "C" void ShimEnumerator__ctor_m16469_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16470_gshared ();
extern "C" void ShimEnumerator_get_Entry_m16471_gshared ();
extern "C" void ShimEnumerator_get_Key_m16472_gshared ();
extern "C" void ShimEnumerator_get_Value_m16473_gshared ();
extern "C" void ShimEnumerator_get_Current_m16474_gshared ();
extern "C" void ShimEnumerator_Reset_m16475_gshared ();
extern "C" void InternalEnumerator_1__ctor_m17172_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17173_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17174_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17175_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17176_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17177_gshared ();
extern "C" void List_1__ctor_m17178_gshared ();
extern "C" void List_1__ctor_m17179_gshared ();
extern "C" void List_1__cctor_m17180_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17181_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17182_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m17183_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m17184_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m17185_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m17186_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m17187_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m17188_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17189_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m17190_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m17191_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m17192_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m17193_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m17194_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m17195_gshared ();
extern "C" void List_1_Add_m17196_gshared ();
extern "C" void List_1_GrowIfNeeded_m17197_gshared ();
extern "C" void List_1_AddCollection_m17198_gshared ();
extern "C" void List_1_AddEnumerable_m17199_gshared ();
extern "C" void List_1_AddRange_m17200_gshared ();
extern "C" void List_1_AsReadOnly_m17201_gshared ();
extern "C" void List_1_Clear_m17202_gshared ();
extern "C" void List_1_Contains_m17203_gshared ();
extern "C" void List_1_CopyTo_m17204_gshared ();
extern "C" void List_1_Find_m17205_gshared ();
extern "C" void List_1_CheckMatch_m17206_gshared ();
extern "C" void List_1_GetIndex_m17207_gshared ();
extern "C" void List_1_GetEnumerator_m17208_gshared ();
extern "C" void List_1_IndexOf_m17209_gshared ();
extern "C" void List_1_Shift_m17210_gshared ();
extern "C" void List_1_CheckIndex_m17211_gshared ();
extern "C" void List_1_Insert_m17212_gshared ();
extern "C" void List_1_CheckCollection_m17213_gshared ();
extern "C" void List_1_Remove_m17214_gshared ();
extern "C" void List_1_RemoveAll_m17215_gshared ();
extern "C" void List_1_RemoveAt_m17216_gshared ();
extern "C" void List_1_Reverse_m17217_gshared ();
extern "C" void List_1_Sort_m17218_gshared ();
extern "C" void List_1_Sort_m17219_gshared ();
extern "C" void List_1_TrimExcess_m17220_gshared ();
extern "C" void List_1_get_Capacity_m17221_gshared ();
extern "C" void List_1_set_Capacity_m17222_gshared ();
extern "C" void List_1_get_Count_m17223_gshared ();
extern "C" void List_1_get_Item_m17224_gshared ();
extern "C" void List_1_set_Item_m17225_gshared ();
extern "C" void Enumerator__ctor_m17226_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17227_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17228_gshared ();
extern "C" void Enumerator_Dispose_m17229_gshared ();
extern "C" void Enumerator_VerifyState_m17230_gshared ();
extern "C" void Enumerator_MoveNext_m17231_gshared ();
extern "C" void Enumerator_get_Current_m17232_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m17233_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17234_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17235_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17236_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17237_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17238_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17239_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17240_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17241_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17242_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17243_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m17244_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m17245_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m17246_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17247_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m17248_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m17249_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17250_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17251_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17252_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17253_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17254_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m17255_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m17256_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m17257_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m17258_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m17259_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m17260_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m17261_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m17262_gshared ();
extern "C" void Collection_1__ctor_m17263_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17264_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17265_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m17266_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m17267_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m17268_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m17269_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m17270_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m17271_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m17272_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m17273_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m17274_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m17275_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m17276_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m17277_gshared ();
extern "C" void Collection_1_Add_m17278_gshared ();
extern "C" void Collection_1_Clear_m17279_gshared ();
extern "C" void Collection_1_ClearItems_m17280_gshared ();
extern "C" void Collection_1_Contains_m17281_gshared ();
extern "C" void Collection_1_CopyTo_m17282_gshared ();
extern "C" void Collection_1_GetEnumerator_m17283_gshared ();
extern "C" void Collection_1_IndexOf_m17284_gshared ();
extern "C" void Collection_1_Insert_m17285_gshared ();
extern "C" void Collection_1_InsertItem_m17286_gshared ();
extern "C" void Collection_1_Remove_m17287_gshared ();
extern "C" void Collection_1_RemoveAt_m17288_gshared ();
extern "C" void Collection_1_RemoveItem_m17289_gshared ();
extern "C" void Collection_1_get_Count_m17290_gshared ();
extern "C" void Collection_1_get_Item_m17291_gshared ();
extern "C" void Collection_1_set_Item_m17292_gshared ();
extern "C" void Collection_1_SetItem_m17293_gshared ();
extern "C" void Collection_1_IsValidItem_m17294_gshared ();
extern "C" void Collection_1_ConvertItem_m17295_gshared ();
extern "C" void Collection_1_CheckWritable_m17296_gshared ();
extern "C" void Collection_1_IsSynchronized_m17297_gshared ();
extern "C" void Collection_1_IsFixedSize_m17298_gshared ();
extern "C" void EqualityComparer_1__ctor_m17299_gshared ();
extern "C" void EqualityComparer_1__cctor_m17300_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17301_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17302_gshared ();
extern "C" void EqualityComparer_1_get_Default_m17303_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m17304_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m17305_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m17306_gshared ();
extern "C" void DefaultComparer__ctor_m17307_gshared ();
extern "C" void DefaultComparer_GetHashCode_m17308_gshared ();
extern "C" void DefaultComparer_Equals_m17309_gshared ();
extern "C" void Predicate_1__ctor_m17310_gshared ();
extern "C" void Predicate_1_Invoke_m17311_gshared ();
extern "C" void Predicate_1_BeginInvoke_m17312_gshared ();
extern "C" void Predicate_1_EndInvoke_m17313_gshared ();
extern "C" void Comparer_1__ctor_m17314_gshared ();
extern "C" void Comparer_1__cctor_m17315_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m17316_gshared ();
extern "C" void Comparer_1_get_Default_m17317_gshared ();
extern "C" void GenericComparer_1__ctor_m17318_gshared ();
extern "C" void GenericComparer_1_Compare_m17319_gshared ();
extern "C" void DefaultComparer__ctor_m17320_gshared ();
extern "C" void DefaultComparer_Compare_m17321_gshared ();
extern "C" void Comparison_1__ctor_m17322_gshared ();
extern "C" void Comparison_1_Invoke_m17323_gshared ();
extern "C" void Comparison_1_BeginInvoke_m17324_gshared ();
extern "C" void Comparison_1_EndInvoke_m17325_gshared ();
extern "C" void Action_1_BeginInvoke_m17517_gshared ();
extern "C" void Action_1_EndInvoke_m17518_gshared ();
extern "C" void Action_1_BeginInvoke_m17638_gshared ();
extern "C" void Action_1_EndInvoke_m17639_gshared ();
extern "C" void Action_1_BeginInvoke_m17661_gshared ();
extern "C" void Action_1_EndInvoke_m17662_gshared ();
extern "C" void Nullable_1_get_Value_m18247_gshared ();
extern "C" void Nullable_1_Equals_m18248_gshared ();
extern "C" void Nullable_1_Equals_m18249_gshared ();
extern "C" void Nullable_1_GetHashCode_m18250_gshared ();
extern "C" void Nullable_1__ctor_m18251_gshared ();
extern "C" void Nullable_1_Equals_m18252_gshared ();
extern "C" void Nullable_1_Equals_m18253_gshared ();
extern "C" void Nullable_1_GetHashCode_m18254_gshared ();
extern "C" void Nullable_1_GetValueOrDefault_m18255_gshared ();
extern "C" void Nullable_1_ToString_m18256_gshared ();
extern "C" void Nullable_1__ctor_m18342_gshared ();
extern "C" void Nullable_1_Equals_m18343_gshared ();
extern "C" void Nullable_1_Equals_m18344_gshared ();
extern "C" void Nullable_1_GetHashCode_m18345_gshared ();
extern "C" void Nullable_1_GetValueOrDefault_m18346_gshared ();
extern "C" void Nullable_1_ToString_m18347_gshared ();
extern "C" void Nullable_1__ctor_m18512_gshared ();
extern "C" void Nullable_1_Equals_m18513_gshared ();
extern "C" void Nullable_1_Equals_m18514_gshared ();
extern "C" void Nullable_1_GetHashCode_m18515_gshared ();
extern "C" void Nullable_1_GetValueOrDefault_m18516_gshared ();
extern "C" void Nullable_1_ToString_m18517_gshared ();
extern "C" void Dictionary_2__ctor_m18520_gshared ();
extern "C" void Dictionary_2__ctor_m18522_gshared ();
extern "C" void Dictionary_2__ctor_m18524_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m18526_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m18528_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m18530_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m18532_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m18534_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m18536_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18538_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18540_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18542_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18544_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18546_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18548_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18550_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m18552_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18554_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18556_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18558_gshared ();
extern "C" void Dictionary_2_get_Count_m18560_gshared ();
extern "C" void Dictionary_2_get_Item_m18562_gshared ();
extern "C" void Dictionary_2_set_Item_m18564_gshared ();
extern "C" void Dictionary_2_Init_m18566_gshared ();
extern "C" void Dictionary_2_InitArrays_m18568_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m18570_gshared ();
extern "C" void Dictionary_2_make_pair_m18572_gshared ();
extern "C" void Dictionary_2_pick_key_m18574_gshared ();
extern "C" void Dictionary_2_pick_value_m18576_gshared ();
extern "C" void Dictionary_2_CopyTo_m18578_gshared ();
extern "C" void Dictionary_2_Resize_m18580_gshared ();
extern "C" void Dictionary_2_Add_m18582_gshared ();
extern "C" void Dictionary_2_Clear_m18584_gshared ();
extern "C" void Dictionary_2_ContainsKey_m18586_gshared ();
extern "C" void Dictionary_2_ContainsValue_m18588_gshared ();
extern "C" void Dictionary_2_GetObjectData_m18590_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m18592_gshared ();
extern "C" void Dictionary_2_Remove_m18594_gshared ();
extern "C" void Dictionary_2_TryGetValue_m18596_gshared ();
extern "C" void Dictionary_2_get_Keys_m18598_gshared ();
extern "C" void Dictionary_2_get_Values_m18600_gshared ();
extern "C" void Dictionary_2_ToTKey_m18602_gshared ();
extern "C" void Dictionary_2_ToTValue_m18604_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m18606_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m18609_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18610_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18611_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18612_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18613_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18614_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18615_gshared ();
extern "C" void KeyValuePair_2__ctor_m18616_gshared ();
extern "C" void KeyValuePair_2_set_Key_m18618_gshared ();
extern "C" void KeyValuePair_2_set_Value_m18620_gshared ();
extern "C" void KeyValuePair_2_ToString_m18621_gshared ();
extern "C" void KeyCollection__ctor_m18622_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18623_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18624_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18625_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18626_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18627_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m18628_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18629_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18630_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18631_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m18632_gshared ();
extern "C" void KeyCollection_CopyTo_m18633_gshared ();
extern "C" void KeyCollection_GetEnumerator_m18634_gshared ();
extern "C" void KeyCollection_get_Count_m18635_gshared ();
extern "C" void Enumerator__ctor_m18636_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18637_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18638_gshared ();
extern "C" void Enumerator_Dispose_m18639_gshared ();
extern "C" void Enumerator_MoveNext_m18640_gshared ();
extern "C" void Enumerator_get_Current_m18641_gshared ();
extern "C" void Enumerator__ctor_m18642_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18643_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18644_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18645_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18646_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18647_gshared ();
extern "C" void Enumerator_get_CurrentKey_m18650_gshared ();
extern "C" void Enumerator_get_CurrentValue_m18651_gshared ();
extern "C" void Enumerator_Reset_m18652_gshared ();
extern "C" void Enumerator_VerifyState_m18653_gshared ();
extern "C" void Enumerator_VerifyCurrent_m18654_gshared ();
extern "C" void Enumerator_Dispose_m18655_gshared ();
extern "C" void Transform_1__ctor_m18656_gshared ();
extern "C" void Transform_1_Invoke_m18657_gshared ();
extern "C" void Transform_1_BeginInvoke_m18658_gshared ();
extern "C" void Transform_1_EndInvoke_m18659_gshared ();
extern "C" void ValueCollection__ctor_m18660_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18661_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18662_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18663_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18664_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18665_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m18666_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18667_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18668_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18669_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m18670_gshared ();
extern "C" void ValueCollection_CopyTo_m18671_gshared ();
extern "C" void ValueCollection_GetEnumerator_m18672_gshared ();
extern "C" void ValueCollection_get_Count_m18673_gshared ();
extern "C" void Enumerator__ctor_m18674_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18675_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18676_gshared ();
extern "C" void Enumerator_Dispose_m18677_gshared ();
extern "C" void Enumerator_MoveNext_m18678_gshared ();
extern "C" void Enumerator_get_Current_m18679_gshared ();
extern "C" void Transform_1__ctor_m18680_gshared ();
extern "C" void Transform_1_Invoke_m18681_gshared ();
extern "C" void Transform_1_BeginInvoke_m18682_gshared ();
extern "C" void Transform_1_EndInvoke_m18683_gshared ();
extern "C" void Transform_1__ctor_m18684_gshared ();
extern "C" void Transform_1_Invoke_m18685_gshared ();
extern "C" void Transform_1_BeginInvoke_m18686_gshared ();
extern "C" void Transform_1_EndInvoke_m18687_gshared ();
extern "C" void Transform_1__ctor_m18688_gshared ();
extern "C" void Transform_1_Invoke_m18689_gshared ();
extern "C" void Transform_1_BeginInvoke_m18690_gshared ();
extern "C" void Transform_1_EndInvoke_m18691_gshared ();
extern "C" void ShimEnumerator__ctor_m18692_gshared ();
extern "C" void ShimEnumerator_MoveNext_m18693_gshared ();
extern "C" void ShimEnumerator_get_Entry_m18694_gshared ();
extern "C" void ShimEnumerator_get_Key_m18695_gshared ();
extern "C" void ShimEnumerator_get_Value_m18696_gshared ();
extern "C" void ShimEnumerator_get_Current_m18697_gshared ();
extern "C" void ShimEnumerator_Reset_m18698_gshared ();
extern "C" void EqualityComparer_1__ctor_m18699_gshared ();
extern "C" void EqualityComparer_1__cctor_m18700_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18701_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18702_gshared ();
extern "C" void EqualityComparer_1_get_Default_m18703_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m18704_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m18705_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m18706_gshared ();
extern "C" void DefaultComparer__ctor_m18707_gshared ();
extern "C" void DefaultComparer_GetHashCode_m18708_gshared ();
extern "C" void DefaultComparer_Equals_m18709_gshared ();
extern "C" void Dictionary_2__ctor_m18862_gshared ();
extern "C" void Dictionary_2__ctor_m18863_gshared ();
extern "C" void Dictionary_2__ctor_m18864_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m18865_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m18866_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m18867_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m18868_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m18869_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m18870_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18871_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18872_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18873_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18874_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18875_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18876_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18877_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m18878_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18879_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18880_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18881_gshared ();
extern "C" void Dictionary_2_get_Count_m18882_gshared ();
extern "C" void Dictionary_2_get_Item_m18883_gshared ();
extern "C" void Dictionary_2_set_Item_m18884_gshared ();
extern "C" void Dictionary_2_Init_m18885_gshared ();
extern "C" void Dictionary_2_InitArrays_m18886_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m18887_gshared ();
extern "C" void Dictionary_2_make_pair_m18888_gshared ();
extern "C" void Dictionary_2_pick_key_m18889_gshared ();
extern "C" void Dictionary_2_pick_value_m18890_gshared ();
extern "C" void Dictionary_2_CopyTo_m18891_gshared ();
extern "C" void Dictionary_2_Resize_m18892_gshared ();
extern "C" void Dictionary_2_Add_m18893_gshared ();
extern "C" void Dictionary_2_Clear_m18894_gshared ();
extern "C" void Dictionary_2_ContainsKey_m18895_gshared ();
extern "C" void Dictionary_2_ContainsValue_m18896_gshared ();
extern "C" void Dictionary_2_GetObjectData_m18897_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m18898_gshared ();
extern "C" void Dictionary_2_Remove_m18899_gshared ();
extern "C" void Dictionary_2_TryGetValue_m18900_gshared ();
extern "C" void Dictionary_2_get_Keys_m18901_gshared ();
extern "C" void Dictionary_2_get_Values_m18902_gshared ();
extern "C" void Dictionary_2_ToTKey_m18903_gshared ();
extern "C" void Dictionary_2_ToTValue_m18904_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m18905_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m18906_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m18907_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18908_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18909_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18910_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18911_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18912_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18913_gshared ();
extern "C" void KeyValuePair_2__ctor_m18914_gshared ();
extern "C" void KeyValuePair_2_get_Key_m18915_gshared ();
extern "C" void KeyValuePair_2_set_Key_m18916_gshared ();
extern "C" void KeyValuePair_2_get_Value_m18917_gshared ();
extern "C" void KeyValuePair_2_set_Value_m18918_gshared ();
extern "C" void KeyValuePair_2_ToString_m18919_gshared ();
extern "C" void KeyCollection__ctor_m18920_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18921_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18922_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18923_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18924_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18925_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m18926_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18927_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18928_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18929_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m18930_gshared ();
extern "C" void KeyCollection_CopyTo_m18931_gshared ();
extern "C" void KeyCollection_GetEnumerator_m18932_gshared ();
extern "C" void KeyCollection_get_Count_m18933_gshared ();
extern "C" void Enumerator__ctor_m18934_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18935_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18936_gshared ();
extern "C" void Enumerator_Dispose_m18937_gshared ();
extern "C" void Enumerator_MoveNext_m18938_gshared ();
extern "C" void Enumerator_get_Current_m18939_gshared ();
extern "C" void Enumerator__ctor_m18940_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18941_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18942_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18943_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18944_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18945_gshared ();
extern "C" void Enumerator_MoveNext_m18946_gshared ();
extern "C" void Enumerator_get_Current_m18947_gshared ();
extern "C" void Enumerator_get_CurrentKey_m18948_gshared ();
extern "C" void Enumerator_get_CurrentValue_m18949_gshared ();
extern "C" void Enumerator_Reset_m18950_gshared ();
extern "C" void Enumerator_VerifyState_m18951_gshared ();
extern "C" void Enumerator_VerifyCurrent_m18952_gshared ();
extern "C" void Enumerator_Dispose_m18953_gshared ();
extern "C" void Transform_1__ctor_m18954_gshared ();
extern "C" void Transform_1_Invoke_m18955_gshared ();
extern "C" void Transform_1_BeginInvoke_m18956_gshared ();
extern "C" void Transform_1_EndInvoke_m18957_gshared ();
extern "C" void ValueCollection__ctor_m18958_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18959_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18960_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18961_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18962_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18963_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m18964_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18965_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18966_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18967_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m18968_gshared ();
extern "C" void ValueCollection_CopyTo_m18969_gshared ();
extern "C" void ValueCollection_GetEnumerator_m18970_gshared ();
extern "C" void ValueCollection_get_Count_m18971_gshared ();
extern "C" void Enumerator__ctor_m18972_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18973_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18974_gshared ();
extern "C" void Enumerator_Dispose_m18975_gshared ();
extern "C" void Enumerator_MoveNext_m18976_gshared ();
extern "C" void Enumerator_get_Current_m18977_gshared ();
extern "C" void Transform_1__ctor_m18978_gshared ();
extern "C" void Transform_1_Invoke_m18979_gshared ();
extern "C" void Transform_1_BeginInvoke_m18980_gshared ();
extern "C" void Transform_1_EndInvoke_m18981_gshared ();
extern "C" void Transform_1__ctor_m18982_gshared ();
extern "C" void Transform_1_Invoke_m18983_gshared ();
extern "C" void Transform_1_BeginInvoke_m18984_gshared ();
extern "C" void Transform_1_EndInvoke_m18985_gshared ();
extern "C" void ShimEnumerator__ctor_m18986_gshared ();
extern "C" void ShimEnumerator_MoveNext_m18987_gshared ();
extern "C" void ShimEnumerator_get_Entry_m18988_gshared ();
extern "C" void ShimEnumerator_get_Key_m18989_gshared ();
extern "C" void ShimEnumerator_get_Value_m18990_gshared ();
extern "C" void ShimEnumerator_get_Current_m18991_gshared ();
extern "C" void ShimEnumerator_Reset_m18992_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19297_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19298_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19299_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19300_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19301_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19302_gshared ();
extern "C" void Action_2_BeginInvoke_m19402_gshared ();
extern "C" void Action_2_EndInvoke_m19404_gshared ();
extern "C" void Action_1_BeginInvoke_m19405_gshared ();
extern "C" void Action_1_EndInvoke_m19406_gshared ();
extern "C" void Action_1_BeginInvoke_m19413_gshared ();
extern "C" void Action_1_EndInvoke_m19414_gshared ();
extern "C" void Comparison_1_Invoke_m19628_gshared ();
extern "C" void Comparison_1_BeginInvoke_m19629_gshared ();
extern "C" void Comparison_1_EndInvoke_m19630_gshared ();
extern "C" void List_1__ctor_m19877_gshared ();
extern "C" void List_1__ctor_m19878_gshared ();
extern "C" void List_1__cctor_m19879_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19880_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m19881_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m19882_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m19883_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m19884_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m19885_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m19886_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m19887_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19888_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m19889_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m19890_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m19891_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m19892_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m19893_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m19894_gshared ();
extern "C" void List_1_Add_m19895_gshared ();
extern "C" void List_1_GrowIfNeeded_m19896_gshared ();
extern "C" void List_1_AddCollection_m19897_gshared ();
extern "C" void List_1_AddEnumerable_m19898_gshared ();
extern "C" void List_1_AddRange_m19899_gshared ();
extern "C" void List_1_AsReadOnly_m19900_gshared ();
extern "C" void List_1_Clear_m19901_gshared ();
extern "C" void List_1_Contains_m19902_gshared ();
extern "C" void List_1_CopyTo_m19903_gshared ();
extern "C" void List_1_Find_m19904_gshared ();
extern "C" void List_1_CheckMatch_m19905_gshared ();
extern "C" void List_1_GetIndex_m19906_gshared ();
extern "C" void List_1_GetEnumerator_m19907_gshared ();
extern "C" void List_1_IndexOf_m19908_gshared ();
extern "C" void List_1_Shift_m19909_gshared ();
extern "C" void List_1_CheckIndex_m19910_gshared ();
extern "C" void List_1_Insert_m19911_gshared ();
extern "C" void List_1_CheckCollection_m19912_gshared ();
extern "C" void List_1_Remove_m19913_gshared ();
extern "C" void List_1_RemoveAll_m19914_gshared ();
extern "C" void List_1_RemoveAt_m19915_gshared ();
extern "C" void List_1_Reverse_m19916_gshared ();
extern "C" void List_1_Sort_m19917_gshared ();
extern "C" void List_1_ToArray_m19918_gshared ();
extern "C" void List_1_TrimExcess_m19919_gshared ();
extern "C" void List_1_get_Capacity_m19920_gshared ();
extern "C" void List_1_set_Capacity_m19921_gshared ();
extern "C" void List_1_get_Count_m19922_gshared ();
extern "C" void List_1_get_Item_m19923_gshared ();
extern "C" void List_1_set_Item_m19924_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19925_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19926_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19927_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19928_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19929_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19930_gshared ();
extern "C" void Enumerator__ctor_m19931_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m19932_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19933_gshared ();
extern "C" void Enumerator_Dispose_m19934_gshared ();
extern "C" void Enumerator_VerifyState_m19935_gshared ();
extern "C" void Enumerator_MoveNext_m19936_gshared ();
extern "C" void Enumerator_get_Current_m19937_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m19938_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19939_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19940_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19941_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19942_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19943_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19944_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19945_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19946_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19947_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19948_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m19949_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m19950_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m19951_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19952_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m19953_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m19954_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19955_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19956_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19957_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19958_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19959_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m19960_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m19961_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m19962_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m19963_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m19964_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m19965_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m19966_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m19967_gshared ();
extern "C" void Collection_1__ctor_m19968_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19969_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m19970_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m19971_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m19972_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m19973_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m19974_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m19975_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m19976_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m19977_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m19978_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m19979_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m19980_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m19981_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m19982_gshared ();
extern "C" void Collection_1_Add_m19983_gshared ();
extern "C" void Collection_1_Clear_m19984_gshared ();
extern "C" void Collection_1_ClearItems_m19985_gshared ();
extern "C" void Collection_1_Contains_m19986_gshared ();
extern "C" void Collection_1_CopyTo_m19987_gshared ();
extern "C" void Collection_1_GetEnumerator_m19988_gshared ();
extern "C" void Collection_1_IndexOf_m19989_gshared ();
extern "C" void Collection_1_Insert_m19990_gshared ();
extern "C" void Collection_1_InsertItem_m19991_gshared ();
extern "C" void Collection_1_Remove_m19992_gshared ();
extern "C" void Collection_1_RemoveAt_m19993_gshared ();
extern "C" void Collection_1_RemoveItem_m19994_gshared ();
extern "C" void Collection_1_get_Count_m19995_gshared ();
extern "C" void Collection_1_get_Item_m19996_gshared ();
extern "C" void Collection_1_set_Item_m19997_gshared ();
extern "C" void Collection_1_SetItem_m19998_gshared ();
extern "C" void Collection_1_IsValidItem_m19999_gshared ();
extern "C" void Collection_1_ConvertItem_m20000_gshared ();
extern "C" void Collection_1_CheckWritable_m20001_gshared ();
extern "C" void Collection_1_IsSynchronized_m20002_gshared ();
extern "C" void Collection_1_IsFixedSize_m20003_gshared ();
extern "C" void EqualityComparer_1__ctor_m20004_gshared ();
extern "C" void EqualityComparer_1__cctor_m20005_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20006_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20007_gshared ();
extern "C" void EqualityComparer_1_get_Default_m20008_gshared ();
extern "C" void DefaultComparer__ctor_m20009_gshared ();
extern "C" void DefaultComparer_GetHashCode_m20010_gshared ();
extern "C" void DefaultComparer_Equals_m20011_gshared ();
extern "C" void Predicate_1__ctor_m20012_gshared ();
extern "C" void Predicate_1_Invoke_m20013_gshared ();
extern "C" void Predicate_1_BeginInvoke_m20014_gshared ();
extern "C" void Predicate_1_EndInvoke_m20015_gshared ();
extern "C" void Comparer_1__ctor_m20016_gshared ();
extern "C" void Comparer_1__cctor_m20017_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m20018_gshared ();
extern "C" void Comparer_1_get_Default_m20019_gshared ();
extern "C" void DefaultComparer__ctor_m20020_gshared ();
extern "C" void DefaultComparer_Compare_m20021_gshared ();
extern "C" void InternalEnumerator_1__ctor_m20558_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20559_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20560_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20561_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20562_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20563_gshared ();
extern "C" void Comparison_1_Invoke_m20564_gshared ();
extern "C" void Comparison_1_BeginInvoke_m20565_gshared ();
extern "C" void Comparison_1_EndInvoke_m20566_gshared ();
extern "C" void InternalEnumerator_1__ctor_m20567_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20568_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20569_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20570_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20571_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20572_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m20573_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m20574_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m20575_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m20576_gshared ();
extern "C" void UnityAction_1_Invoke_m20577_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m20578_gshared ();
extern "C" void UnityAction_1_EndInvoke_m20579_gshared ();
extern "C" void InvokableCall_1__ctor_m20580_gshared ();
extern "C" void InvokableCall_1__ctor_m20581_gshared ();
extern "C" void InvokableCall_1_Invoke_m20582_gshared ();
extern "C" void InvokableCall_1_Find_m20583_gshared ();
extern "C" void List_1__ctor_m20972_gshared ();
extern "C" void List_1__cctor_m20973_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20974_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m20975_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m20976_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m20977_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m20978_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m20979_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m20980_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m20981_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20982_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m20983_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m20984_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m20985_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m20986_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m20987_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m20988_gshared ();
extern "C" void List_1_Add_m20989_gshared ();
extern "C" void List_1_GrowIfNeeded_m20990_gshared ();
extern "C" void List_1_AddCollection_m20991_gshared ();
extern "C" void List_1_AddEnumerable_m20992_gshared ();
extern "C" void List_1_AddRange_m20993_gshared ();
extern "C" void List_1_AsReadOnly_m20994_gshared ();
extern "C" void List_1_Clear_m20995_gshared ();
extern "C" void List_1_Contains_m20996_gshared ();
extern "C" void List_1_CopyTo_m20997_gshared ();
extern "C" void List_1_Find_m20998_gshared ();
extern "C" void List_1_CheckMatch_m20999_gshared ();
extern "C" void List_1_GetIndex_m21000_gshared ();
extern "C" void List_1_GetEnumerator_m21001_gshared ();
extern "C" void List_1_IndexOf_m21002_gshared ();
extern "C" void List_1_Shift_m21003_gshared ();
extern "C" void List_1_CheckIndex_m21004_gshared ();
extern "C" void List_1_Insert_m21005_gshared ();
extern "C" void List_1_CheckCollection_m21006_gshared ();
extern "C" void List_1_Remove_m21007_gshared ();
extern "C" void List_1_RemoveAll_m21008_gshared ();
extern "C" void List_1_RemoveAt_m21009_gshared ();
extern "C" void List_1_Reverse_m21010_gshared ();
extern "C" void List_1_Sort_m21011_gshared ();
extern "C" void List_1_Sort_m21012_gshared ();
extern "C" void List_1_TrimExcess_m21013_gshared ();
extern "C" void List_1_get_Count_m21014_gshared ();
extern "C" void List_1_get_Item_m21015_gshared ();
extern "C" void List_1_set_Item_m21016_gshared ();
extern "C" void Enumerator__ctor_m20950_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m20951_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20952_gshared ();
extern "C" void Enumerator_Dispose_m20953_gshared ();
extern "C" void Enumerator_VerifyState_m20954_gshared ();
extern "C" void Enumerator_MoveNext_m20955_gshared ();
extern "C" void Enumerator_get_Current_m20956_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m20916_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20917_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20918_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20919_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20920_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20921_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20922_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20923_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20924_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20925_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20926_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m20927_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m20928_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m20929_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20930_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m20931_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m20932_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20933_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20934_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20935_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20936_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20937_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m20938_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m20939_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m20940_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m20941_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m20942_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m20943_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m20944_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m20945_gshared ();
extern "C" void Collection_1__ctor_m21020_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21021_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m21022_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m21023_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m21024_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m21025_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m21026_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m21027_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m21028_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m21029_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m21030_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m21031_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m21032_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m21033_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m21034_gshared ();
extern "C" void Collection_1_Add_m21035_gshared ();
extern "C" void Collection_1_Clear_m21036_gshared ();
extern "C" void Collection_1_ClearItems_m21037_gshared ();
extern "C" void Collection_1_Contains_m21038_gshared ();
extern "C" void Collection_1_CopyTo_m21039_gshared ();
extern "C" void Collection_1_GetEnumerator_m21040_gshared ();
extern "C" void Collection_1_IndexOf_m21041_gshared ();
extern "C" void Collection_1_Insert_m21042_gshared ();
extern "C" void Collection_1_InsertItem_m21043_gshared ();
extern "C" void Collection_1_Remove_m21044_gshared ();
extern "C" void Collection_1_RemoveAt_m21045_gshared ();
extern "C" void Collection_1_RemoveItem_m21046_gshared ();
extern "C" void Collection_1_get_Count_m21047_gshared ();
extern "C" void Collection_1_get_Item_m21048_gshared ();
extern "C" void Collection_1_set_Item_m21049_gshared ();
extern "C" void Collection_1_SetItem_m21050_gshared ();
extern "C" void Collection_1_IsValidItem_m21051_gshared ();
extern "C" void Collection_1_ConvertItem_m21052_gshared ();
extern "C" void Collection_1_CheckWritable_m21053_gshared ();
extern "C" void Collection_1_IsSynchronized_m21054_gshared ();
extern "C" void Collection_1_IsFixedSize_m21055_gshared ();
extern "C" void EqualityComparer_1__ctor_m21056_gshared ();
extern "C" void EqualityComparer_1__cctor_m21057_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21058_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21059_gshared ();
extern "C" void EqualityComparer_1_get_Default_m21060_gshared ();
extern "C" void DefaultComparer__ctor_m21061_gshared ();
extern "C" void DefaultComparer_GetHashCode_m21062_gshared ();
extern "C" void DefaultComparer_Equals_m21063_gshared ();
extern "C" void Predicate_1__ctor_m20946_gshared ();
extern "C" void Predicate_1_Invoke_m20947_gshared ();
extern "C" void Predicate_1_BeginInvoke_m20948_gshared ();
extern "C" void Predicate_1_EndInvoke_m20949_gshared ();
extern "C" void Comparer_1__ctor_m21064_gshared ();
extern "C" void Comparer_1__cctor_m21065_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m21066_gshared ();
extern "C" void Comparer_1_get_Default_m21067_gshared ();
extern "C" void DefaultComparer__ctor_m21068_gshared ();
extern "C" void DefaultComparer_Compare_m21069_gshared ();
extern "C" void Comparison_1__ctor_m20957_gshared ();
extern "C" void Comparison_1_Invoke_m20958_gshared ();
extern "C" void Comparison_1_BeginInvoke_m20959_gshared ();
extern "C" void Comparison_1_EndInvoke_m20960_gshared ();
extern "C" void TweenRunner_1_Start_m21070_gshared ();
extern "C" void U3CStartU3Ec__Iterator0__ctor_m21071_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m21072_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m21073_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_MoveNext_m21074_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m21075_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Reset_m21076_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21529_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21530_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21531_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21532_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21533_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21534_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21535_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21536_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21537_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21538_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21539_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21540_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21541_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21542_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21543_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21544_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21545_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21546_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m21556_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m21557_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m21558_gshared ();
extern "C" void UnityAction_1_Invoke_m21559_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m21560_gshared ();
extern "C" void UnityAction_1_EndInvoke_m21561_gshared ();
extern "C" void InvokableCall_1__ctor_m21562_gshared ();
extern "C" void InvokableCall_1__ctor_m21563_gshared ();
extern "C" void InvokableCall_1_Invoke_m21564_gshared ();
extern "C" void InvokableCall_1_Find_m21565_gshared ();
extern "C" void UnityEvent_1_AddListener_m21566_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m21567_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m21568_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m21569_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m21570_gshared ();
extern "C" void UnityAction_1__ctor_m21571_gshared ();
extern "C" void UnityAction_1_Invoke_m21572_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m21573_gshared ();
extern "C" void UnityAction_1_EndInvoke_m21574_gshared ();
extern "C" void InvokableCall_1__ctor_m21575_gshared ();
extern "C" void InvokableCall_1__ctor_m21576_gshared ();
extern "C" void InvokableCall_1_Invoke_m21577_gshared ();
extern "C" void InvokableCall_1_Find_m21578_gshared ();
extern "C" void UnityEvent_1_AddListener_m21861_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m21862_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m21863_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m21864_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m21865_gshared ();
extern "C" void UnityAction_1__ctor_m21866_gshared ();
extern "C" void UnityAction_1_Invoke_m21867_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m21868_gshared ();
extern "C" void UnityAction_1_EndInvoke_m21869_gshared ();
extern "C" void InvokableCall_1__ctor_m21870_gshared ();
extern "C" void InvokableCall_1__ctor_m21871_gshared ();
extern "C" void InvokableCall_1_Invoke_m21872_gshared ();
extern "C" void InvokableCall_1_Find_m21873_gshared ();
extern "C" void Func_2_Invoke_m21968_gshared ();
extern "C" void Func_2_BeginInvoke_m21970_gshared ();
extern "C" void Func_2_EndInvoke_m21972_gshared ();
extern "C" void Func_2_BeginInvoke_m22080_gshared ();
extern "C" void Func_2_EndInvoke_m22082_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22119_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22120_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22121_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22122_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22123_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22124_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22404_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22405_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22406_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22407_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22408_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22409_gshared ();
extern "C" void Dictionary_2__ctor_m22691_gshared ();
extern "C" void Dictionary_2__ctor_m22693_gshared ();
extern "C" void Dictionary_2__ctor_m22695_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m22697_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m22699_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22701_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22703_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m22705_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22707_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22709_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22711_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22713_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22715_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22717_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22719_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22721_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22723_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22725_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22727_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22729_gshared ();
extern "C" void Dictionary_2_get_Count_m22731_gshared ();
extern "C" void Dictionary_2_get_Item_m22733_gshared ();
extern "C" void Dictionary_2_set_Item_m22735_gshared ();
extern "C" void Dictionary_2_Init_m22737_gshared ();
extern "C" void Dictionary_2_InitArrays_m22739_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m22741_gshared ();
extern "C" void Dictionary_2_make_pair_m22743_gshared ();
extern "C" void Dictionary_2_pick_key_m22745_gshared ();
extern "C" void Dictionary_2_pick_value_m22747_gshared ();
extern "C" void Dictionary_2_CopyTo_m22749_gshared ();
extern "C" void Dictionary_2_Resize_m22751_gshared ();
extern "C" void Dictionary_2_Add_m22753_gshared ();
extern "C" void Dictionary_2_Clear_m22755_gshared ();
extern "C" void Dictionary_2_ContainsKey_m22757_gshared ();
extern "C" void Dictionary_2_ContainsValue_m22759_gshared ();
extern "C" void Dictionary_2_GetObjectData_m22761_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m22763_gshared ();
extern "C" void Dictionary_2_Remove_m22765_gshared ();
extern "C" void Dictionary_2_TryGetValue_m22767_gshared ();
extern "C" void Dictionary_2_get_Keys_m22769_gshared ();
extern "C" void Dictionary_2_ToTKey_m22772_gshared ();
extern "C" void Dictionary_2_ToTValue_m22774_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m22776_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m22778_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m22780_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22781_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22782_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22783_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22784_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22785_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22786_gshared ();
extern "C" void KeyValuePair_2__ctor_m22787_gshared ();
extern "C" void KeyValuePair_2_get_Key_m22788_gshared ();
extern "C" void KeyValuePair_2_set_Key_m22789_gshared ();
extern "C" void KeyValuePair_2_get_Value_m22790_gshared ();
extern "C" void KeyValuePair_2_set_Value_m22791_gshared ();
extern "C" void KeyValuePair_2_ToString_m22792_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22793_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22794_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22795_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22796_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22797_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22798_gshared ();
extern "C" void KeyCollection__ctor_m22799_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22800_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22801_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22802_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22803_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22804_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m22805_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22806_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22807_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22808_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m22809_gshared ();
extern "C" void KeyCollection_CopyTo_m22810_gshared ();
extern "C" void KeyCollection_GetEnumerator_m22811_gshared ();
extern "C" void KeyCollection_get_Count_m22812_gshared ();
extern "C" void Enumerator__ctor_m22813_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22814_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m22815_gshared ();
extern "C" void Enumerator_Dispose_m22816_gshared ();
extern "C" void Enumerator_MoveNext_m22817_gshared ();
extern "C" void Enumerator_get_Current_m22818_gshared ();
extern "C" void Enumerator__ctor_m22819_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22820_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m22821_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22822_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22823_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22824_gshared ();
extern "C" void Enumerator_MoveNext_m22825_gshared ();
extern "C" void Enumerator_get_Current_m22826_gshared ();
extern "C" void Enumerator_get_CurrentKey_m22827_gshared ();
extern "C" void Enumerator_get_CurrentValue_m22828_gshared ();
extern "C" void Enumerator_Reset_m22829_gshared ();
extern "C" void Enumerator_VerifyState_m22830_gshared ();
extern "C" void Enumerator_VerifyCurrent_m22831_gshared ();
extern "C" void Enumerator_Dispose_m22832_gshared ();
extern "C" void Transform_1__ctor_m22833_gshared ();
extern "C" void Transform_1_Invoke_m22834_gshared ();
extern "C" void Transform_1_BeginInvoke_m22835_gshared ();
extern "C" void Transform_1_EndInvoke_m22836_gshared ();
extern "C" void ValueCollection__ctor_m22837_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22838_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22839_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22840_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22841_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22842_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22843_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22844_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22845_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22846_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m22847_gshared ();
extern "C" void ValueCollection_CopyTo_m22848_gshared ();
extern "C" void ValueCollection_get_Count_m22850_gshared ();
extern "C" void Enumerator__ctor_m22851_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22852_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m22853_gshared ();
extern "C" void Transform_1__ctor_m22857_gshared ();
extern "C" void Transform_1_Invoke_m22858_gshared ();
extern "C" void Transform_1_BeginInvoke_m22859_gshared ();
extern "C" void Transform_1_EndInvoke_m22860_gshared ();
extern "C" void Transform_1__ctor_m22861_gshared ();
extern "C" void Transform_1_Invoke_m22862_gshared ();
extern "C" void Transform_1_BeginInvoke_m22863_gshared ();
extern "C" void Transform_1_EndInvoke_m22864_gshared ();
extern "C" void Transform_1__ctor_m22865_gshared ();
extern "C" void Transform_1_Invoke_m22866_gshared ();
extern "C" void Transform_1_BeginInvoke_m22867_gshared ();
extern "C" void Transform_1_EndInvoke_m22868_gshared ();
extern "C" void ShimEnumerator__ctor_m22869_gshared ();
extern "C" void ShimEnumerator_MoveNext_m22870_gshared ();
extern "C" void ShimEnumerator_get_Entry_m22871_gshared ();
extern "C" void ShimEnumerator_get_Key_m22872_gshared ();
extern "C" void ShimEnumerator_get_Value_m22873_gshared ();
extern "C" void ShimEnumerator_get_Current_m22874_gshared ();
extern "C" void ShimEnumerator_Reset_m22875_gshared ();
extern "C" void EqualityComparer_1__ctor_m22876_gshared ();
extern "C" void EqualityComparer_1__cctor_m22877_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22878_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22879_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22880_gshared ();
extern "C" void DefaultComparer__ctor_m22881_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22882_gshared ();
extern "C" void DefaultComparer_Equals_m22883_gshared ();
extern "C" void List_1__ctor_m22935_gshared ();
extern "C" void List_1__ctor_m22936_gshared ();
extern "C" void List_1__cctor_m22937_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22938_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22939_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m22940_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m22941_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m22942_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m22943_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m22944_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m22945_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22946_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m22947_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m22948_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m22949_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m22950_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m22951_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m22952_gshared ();
extern "C" void List_1_Add_m22953_gshared ();
extern "C" void List_1_GrowIfNeeded_m22954_gshared ();
extern "C" void List_1_AddCollection_m22955_gshared ();
extern "C" void List_1_AddEnumerable_m22956_gshared ();
extern "C" void List_1_AddRange_m22957_gshared ();
extern "C" void List_1_AsReadOnly_m22958_gshared ();
extern "C" void List_1_Clear_m22959_gshared ();
extern "C" void List_1_Contains_m22960_gshared ();
extern "C" void List_1_CopyTo_m22961_gshared ();
extern "C" void List_1_Find_m22962_gshared ();
extern "C" void List_1_CheckMatch_m22963_gshared ();
extern "C" void List_1_GetIndex_m22964_gshared ();
extern "C" void List_1_GetEnumerator_m22965_gshared ();
extern "C" void List_1_IndexOf_m22966_gshared ();
extern "C" void List_1_Shift_m22967_gshared ();
extern "C" void List_1_CheckIndex_m22968_gshared ();
extern "C" void List_1_Insert_m22969_gshared ();
extern "C" void List_1_CheckCollection_m22970_gshared ();
extern "C" void List_1_Remove_m22971_gshared ();
extern "C" void List_1_RemoveAll_m22972_gshared ();
extern "C" void List_1_RemoveAt_m22973_gshared ();
extern "C" void List_1_Reverse_m22974_gshared ();
extern "C" void List_1_Sort_m22975_gshared ();
extern "C" void List_1_Sort_m22976_gshared ();
extern "C" void List_1_ToArray_m22977_gshared ();
extern "C" void List_1_TrimExcess_m22978_gshared ();
extern "C" void List_1_get_Capacity_m22979_gshared ();
extern "C" void List_1_set_Capacity_m22980_gshared ();
extern "C" void List_1_get_Count_m22981_gshared ();
extern "C" void List_1_get_Item_m22982_gshared ();
extern "C" void List_1_set_Item_m22983_gshared ();
extern "C" void Enumerator__ctor_m22984_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m22985_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22986_gshared ();
extern "C" void Enumerator_Dispose_m22987_gshared ();
extern "C" void Enumerator_VerifyState_m22988_gshared ();
extern "C" void Enumerator_MoveNext_m22989_gshared ();
extern "C" void Enumerator_get_Current_m22990_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m22991_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22992_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22993_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22994_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22995_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22996_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22997_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22998_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22999_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23000_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23001_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m23002_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m23003_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m23004_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23005_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m23006_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m23007_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23008_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23009_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23010_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23011_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23012_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m23013_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m23014_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m23015_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m23016_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m23017_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m23018_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m23019_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m23020_gshared ();
extern "C" void Collection_1__ctor_m23021_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23022_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23023_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m23024_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m23025_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m23026_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m23027_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m23028_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m23029_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m23030_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m23031_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m23032_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m23033_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m23034_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m23035_gshared ();
extern "C" void Collection_1_Add_m23036_gshared ();
extern "C" void Collection_1_Clear_m23037_gshared ();
extern "C" void Collection_1_ClearItems_m23038_gshared ();
extern "C" void Collection_1_Contains_m23039_gshared ();
extern "C" void Collection_1_CopyTo_m23040_gshared ();
extern "C" void Collection_1_GetEnumerator_m23041_gshared ();
extern "C" void Collection_1_IndexOf_m23042_gshared ();
extern "C" void Collection_1_Insert_m23043_gshared ();
extern "C" void Collection_1_InsertItem_m23044_gshared ();
extern "C" void Collection_1_Remove_m23045_gshared ();
extern "C" void Collection_1_RemoveAt_m23046_gshared ();
extern "C" void Collection_1_RemoveItem_m23047_gshared ();
extern "C" void Collection_1_get_Count_m23048_gshared ();
extern "C" void Collection_1_get_Item_m23049_gshared ();
extern "C" void Collection_1_set_Item_m23050_gshared ();
extern "C" void Collection_1_SetItem_m23051_gshared ();
extern "C" void Collection_1_IsValidItem_m23052_gshared ();
extern "C" void Collection_1_ConvertItem_m23053_gshared ();
extern "C" void Collection_1_CheckWritable_m23054_gshared ();
extern "C" void Collection_1_IsSynchronized_m23055_gshared ();
extern "C" void Collection_1_IsFixedSize_m23056_gshared ();
extern "C" void Predicate_1__ctor_m23057_gshared ();
extern "C" void Predicate_1_Invoke_m23058_gshared ();
extern "C" void Predicate_1_BeginInvoke_m23059_gshared ();
extern "C" void Predicate_1_EndInvoke_m23060_gshared ();
extern "C" void Comparer_1__ctor_m23061_gshared ();
extern "C" void Comparer_1__cctor_m23062_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m23063_gshared ();
extern "C" void Comparer_1_get_Default_m23064_gshared ();
extern "C" void DefaultComparer__ctor_m23065_gshared ();
extern "C" void DefaultComparer_Compare_m23066_gshared ();
extern "C" void Comparison_1__ctor_m23067_gshared ();
extern "C" void Comparison_1_Invoke_m23068_gshared ();
extern "C" void Comparison_1_BeginInvoke_m23069_gshared ();
extern "C" void Comparison_1_EndInvoke_m23070_gshared ();
extern "C" void InternalEnumerator_1__ctor_m23257_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23258_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23259_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23260_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23261_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23262_gshared ();
extern "C" void InternalEnumerator_1__ctor_m23742_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23743_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23744_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23745_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23746_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23747_gshared ();
extern "C" void InternalEnumerator_1__ctor_m23748_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23749_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23750_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23751_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23752_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23753_gshared ();
extern "C" void InternalEnumerator_1__ctor_m23754_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23755_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23756_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23757_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23758_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23759_gshared ();
extern "C" void LinkedList_1__ctor_m23760_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23761_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m23762_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23763_gshared ();
extern "C" void LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m23764_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23765_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m23766_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_SyncRoot_m23767_gshared ();
extern "C" void LinkedList_1_VerifyReferencedNode_m23768_gshared ();
extern "C" void LinkedList_1_Clear_m23769_gshared ();
extern "C" void LinkedList_1_Contains_m23770_gshared ();
extern "C" void LinkedList_1_CopyTo_m23771_gshared ();
extern "C" void LinkedList_1_Find_m23772_gshared ();
extern "C" void LinkedList_1_GetEnumerator_m23773_gshared ();
extern "C" void LinkedList_1_GetObjectData_m23774_gshared ();
extern "C" void LinkedList_1_OnDeserialization_m23775_gshared ();
extern "C" void LinkedList_1_Remove_m23776_gshared ();
extern "C" void LinkedList_1_get_Count_m23777_gshared ();
extern "C" void LinkedListNode_1__ctor_m23778_gshared ();
extern "C" void LinkedListNode_1__ctor_m23779_gshared ();
extern "C" void LinkedListNode_1_Detach_m23780_gshared ();
extern "C" void LinkedListNode_1_get_List_m23781_gshared ();
extern "C" void Enumerator__ctor_m23782_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m23783_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m23784_gshared ();
extern "C" void Enumerator_get_Current_m23785_gshared ();
extern "C" void Enumerator_MoveNext_m23786_gshared ();
extern "C" void Enumerator_Dispose_m23787_gshared ();
extern "C" void List_1__ctor_m23788_gshared ();
extern "C" void List_1__cctor_m23789_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23790_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m23791_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m23792_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m23793_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m23794_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m23795_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m23796_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m23797_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23798_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m23799_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m23800_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m23801_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m23802_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m23803_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m23804_gshared ();
extern "C" void List_1_Add_m23805_gshared ();
extern "C" void List_1_GrowIfNeeded_m23806_gshared ();
extern "C" void List_1_AddCollection_m23807_gshared ();
extern "C" void List_1_AddEnumerable_m23808_gshared ();
extern "C" void List_1_AddRange_m23809_gshared ();
extern "C" void List_1_AsReadOnly_m23810_gshared ();
extern "C" void List_1_Clear_m23811_gshared ();
extern "C" void List_1_Contains_m23812_gshared ();
extern "C" void List_1_CopyTo_m23813_gshared ();
extern "C" void List_1_Find_m23814_gshared ();
extern "C" void List_1_CheckMatch_m23815_gshared ();
extern "C" void List_1_GetIndex_m23816_gshared ();
extern "C" void List_1_IndexOf_m23817_gshared ();
extern "C" void List_1_Shift_m23818_gshared ();
extern "C" void List_1_CheckIndex_m23819_gshared ();
extern "C" void List_1_Insert_m23820_gshared ();
extern "C" void List_1_CheckCollection_m23821_gshared ();
extern "C" void List_1_Remove_m23822_gshared ();
extern "C" void List_1_RemoveAll_m23823_gshared ();
extern "C" void List_1_RemoveAt_m23824_gshared ();
extern "C" void List_1_Reverse_m23825_gshared ();
extern "C" void List_1_Sort_m23826_gshared ();
extern "C" void List_1_Sort_m23827_gshared ();
extern "C" void List_1_ToArray_m23828_gshared ();
extern "C" void List_1_TrimExcess_m23829_gshared ();
extern "C" void List_1_get_Capacity_m23830_gshared ();
extern "C" void List_1_set_Capacity_m23831_gshared ();
extern "C" void List_1_get_Count_m23832_gshared ();
extern "C" void List_1_get_Item_m23833_gshared ();
extern "C" void List_1_set_Item_m23834_gshared ();
extern "C" void Enumerator__ctor_m23835_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m23836_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m23837_gshared ();
extern "C" void Enumerator_VerifyState_m23838_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m23839_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23840_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23841_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23842_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23843_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23844_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23845_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23846_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23847_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23848_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23849_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m23850_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m23851_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m23852_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23853_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m23854_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m23855_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23856_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23857_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23858_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23859_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23860_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m23861_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m23862_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m23863_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m23864_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m23865_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m23866_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m23867_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m23868_gshared ();
extern "C" void Collection_1__ctor_m23869_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23870_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23871_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m23872_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m23873_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m23874_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m23875_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m23876_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m23877_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m23878_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m23879_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m23880_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m23881_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m23882_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m23883_gshared ();
extern "C" void Collection_1_Add_m23884_gshared ();
extern "C" void Collection_1_Clear_m23885_gshared ();
extern "C" void Collection_1_ClearItems_m23886_gshared ();
extern "C" void Collection_1_Contains_m23887_gshared ();
extern "C" void Collection_1_CopyTo_m23888_gshared ();
extern "C" void Collection_1_GetEnumerator_m23889_gshared ();
extern "C" void Collection_1_IndexOf_m23890_gshared ();
extern "C" void Collection_1_Insert_m23891_gshared ();
extern "C" void Collection_1_InsertItem_m23892_gshared ();
extern "C" void Collection_1_Remove_m23893_gshared ();
extern "C" void Collection_1_RemoveAt_m23894_gshared ();
extern "C" void Collection_1_RemoveItem_m23895_gshared ();
extern "C" void Collection_1_get_Count_m23896_gshared ();
extern "C" void Collection_1_get_Item_m23897_gshared ();
extern "C" void Collection_1_set_Item_m23898_gshared ();
extern "C" void Collection_1_SetItem_m23899_gshared ();
extern "C" void Collection_1_IsValidItem_m23900_gshared ();
extern "C" void Collection_1_ConvertItem_m23901_gshared ();
extern "C" void Collection_1_CheckWritable_m23902_gshared ();
extern "C" void Collection_1_IsSynchronized_m23903_gshared ();
extern "C" void Collection_1_IsFixedSize_m23904_gshared ();
extern "C" void Predicate_1__ctor_m23905_gshared ();
extern "C" void Predicate_1_Invoke_m23906_gshared ();
extern "C" void Predicate_1_BeginInvoke_m23907_gshared ();
extern "C" void Predicate_1_EndInvoke_m23908_gshared ();
extern "C" void Comparer_1__ctor_m23909_gshared ();
extern "C" void Comparer_1__cctor_m23910_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m23911_gshared ();
extern "C" void Comparer_1_get_Default_m23912_gshared ();
extern "C" void GenericComparer_1__ctor_m23913_gshared ();
extern "C" void GenericComparer_1_Compare_m23914_gshared ();
extern "C" void DefaultComparer__ctor_m23915_gshared ();
extern "C" void DefaultComparer_Compare_m23916_gshared ();
extern "C" void Comparison_1__ctor_m23917_gshared ();
extern "C" void Comparison_1_Invoke_m23918_gshared ();
extern "C" void Comparison_1_BeginInvoke_m23919_gshared ();
extern "C" void Comparison_1_EndInvoke_m23920_gshared ();
extern "C" void Predicate_1_Invoke_m23921_gshared ();
extern "C" void Predicate_1_BeginInvoke_m23922_gshared ();
extern "C" void Predicate_1_EndInvoke_m23923_gshared ();
extern "C" void InternalEnumerator_1__ctor_m23924_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23925_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23926_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23927_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23928_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23929_gshared ();
extern "C" void InternalEnumerator_1__ctor_m23930_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23931_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23932_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23933_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23934_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23935_gshared ();
extern "C" void InternalEnumerator_1__ctor_m23936_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23937_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23938_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23939_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23940_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23941_gshared ();
extern "C" void Dictionary_2__ctor_m24142_gshared ();
extern "C" void Dictionary_2__ctor_m24144_gshared ();
extern "C" void Dictionary_2__ctor_m24146_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m24148_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m24150_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m24152_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m24154_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m24156_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m24158_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24160_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24162_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24164_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24166_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24168_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24170_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24172_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m24174_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24176_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24178_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24180_gshared ();
extern "C" void Dictionary_2_get_Count_m24182_gshared ();
extern "C" void Dictionary_2_get_Item_m24184_gshared ();
extern "C" void Dictionary_2_set_Item_m24186_gshared ();
extern "C" void Dictionary_2_Init_m24188_gshared ();
extern "C" void Dictionary_2_InitArrays_m24190_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m24192_gshared ();
extern "C" void Dictionary_2_make_pair_m24194_gshared ();
extern "C" void Dictionary_2_pick_key_m24196_gshared ();
extern "C" void Dictionary_2_pick_value_m24198_gshared ();
extern "C" void Dictionary_2_CopyTo_m24200_gshared ();
extern "C" void Dictionary_2_Resize_m24202_gshared ();
extern "C" void Dictionary_2_Add_m24204_gshared ();
extern "C" void Dictionary_2_Clear_m24206_gshared ();
extern "C" void Dictionary_2_ContainsKey_m24208_gshared ();
extern "C" void Dictionary_2_ContainsValue_m24210_gshared ();
extern "C" void Dictionary_2_GetObjectData_m24212_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m24214_gshared ();
extern "C" void Dictionary_2_Remove_m24216_gshared ();
extern "C" void Dictionary_2_TryGetValue_m24218_gshared ();
extern "C" void Dictionary_2_get_Keys_m24220_gshared ();
extern "C" void Dictionary_2_get_Values_m24222_gshared ();
extern "C" void Dictionary_2_ToTKey_m24224_gshared ();
extern "C" void Dictionary_2_ToTValue_m24226_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m24228_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m24230_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m24232_gshared ();
extern "C" void InternalEnumerator_1__ctor_m24233_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24234_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24235_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24236_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24237_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24238_gshared ();
extern "C" void KeyValuePair_2__ctor_m24239_gshared ();
extern "C" void KeyValuePair_2_get_Key_m24240_gshared ();
extern "C" void KeyValuePair_2_set_Key_m24241_gshared ();
extern "C" void KeyValuePair_2_get_Value_m24242_gshared ();
extern "C" void KeyValuePair_2_set_Value_m24243_gshared ();
extern "C" void KeyValuePair_2_ToString_m24244_gshared ();
extern "C" void InternalEnumerator_1__ctor_m24245_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24246_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24247_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24248_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24249_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24250_gshared ();
extern "C" void KeyCollection__ctor_m24251_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m24252_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m24253_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24254_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m24255_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m24256_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m24257_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m24258_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m24259_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m24260_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m24261_gshared ();
extern "C" void KeyCollection_CopyTo_m24262_gshared ();
extern "C" void KeyCollection_GetEnumerator_m24263_gshared ();
extern "C" void KeyCollection_get_Count_m24264_gshared ();
extern "C" void Enumerator__ctor_m24265_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m24266_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m24267_gshared ();
extern "C" void Enumerator_Dispose_m24268_gshared ();
extern "C" void Enumerator_MoveNext_m24269_gshared ();
extern "C" void Enumerator_get_Current_m24270_gshared ();
extern "C" void Enumerator__ctor_m24271_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m24272_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m24273_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24274_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24275_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24276_gshared ();
extern "C" void Enumerator_MoveNext_m24277_gshared ();
extern "C" void Enumerator_get_Current_m24278_gshared ();
extern "C" void Enumerator_get_CurrentKey_m24279_gshared ();
extern "C" void Enumerator_get_CurrentValue_m24280_gshared ();
extern "C" void Enumerator_Reset_m24281_gshared ();
extern "C" void Enumerator_VerifyState_m24282_gshared ();
extern "C" void Enumerator_VerifyCurrent_m24283_gshared ();
extern "C" void Enumerator_Dispose_m24284_gshared ();
extern "C" void Transform_1__ctor_m24285_gshared ();
extern "C" void Transform_1_Invoke_m24286_gshared ();
extern "C" void Transform_1_BeginInvoke_m24287_gshared ();
extern "C" void Transform_1_EndInvoke_m24288_gshared ();
extern "C" void ValueCollection__ctor_m24289_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m24290_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m24291_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m24292_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m24293_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m24294_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m24295_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m24296_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m24297_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m24298_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m24299_gshared ();
extern "C" void ValueCollection_CopyTo_m24300_gshared ();
extern "C" void ValueCollection_GetEnumerator_m24301_gshared ();
extern "C" void ValueCollection_get_Count_m24302_gshared ();
extern "C" void Enumerator__ctor_m24303_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m24304_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m24305_gshared ();
extern "C" void Enumerator_Dispose_m24306_gshared ();
extern "C" void Enumerator_MoveNext_m24307_gshared ();
extern "C" void Enumerator_get_Current_m24308_gshared ();
extern "C" void Transform_1__ctor_m24309_gshared ();
extern "C" void Transform_1_Invoke_m24310_gshared ();
extern "C" void Transform_1_BeginInvoke_m24311_gshared ();
extern "C" void Transform_1_EndInvoke_m24312_gshared ();
extern "C" void Transform_1__ctor_m24313_gshared ();
extern "C" void Transform_1_Invoke_m24314_gshared ();
extern "C" void Transform_1_BeginInvoke_m24315_gshared ();
extern "C" void Transform_1_EndInvoke_m24316_gshared ();
extern "C" void Transform_1__ctor_m24317_gshared ();
extern "C" void Transform_1_Invoke_m24318_gshared ();
extern "C" void Transform_1_BeginInvoke_m24319_gshared ();
extern "C" void Transform_1_EndInvoke_m24320_gshared ();
extern "C" void ShimEnumerator__ctor_m24321_gshared ();
extern "C" void ShimEnumerator_MoveNext_m24322_gshared ();
extern "C" void ShimEnumerator_get_Entry_m24323_gshared ();
extern "C" void ShimEnumerator_get_Key_m24324_gshared ();
extern "C" void ShimEnumerator_get_Value_m24325_gshared ();
extern "C" void ShimEnumerator_get_Current_m24326_gshared ();
extern "C" void ShimEnumerator_Reset_m24327_gshared ();
extern "C" void EqualityComparer_1__ctor_m24328_gshared ();
extern "C" void EqualityComparer_1__cctor_m24329_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m24330_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m24331_gshared ();
extern "C" void EqualityComparer_1_get_Default_m24332_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m24333_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m24334_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m24335_gshared ();
extern "C" void DefaultComparer__ctor_m24336_gshared ();
extern "C" void DefaultComparer_GetHashCode_m24337_gshared ();
extern "C" void DefaultComparer_Equals_m24338_gshared ();
extern "C" void InternalEnumerator_1__ctor_m24391_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24392_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24393_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24394_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24395_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24396_gshared ();
extern "C" void Action_1__ctor_m25226_gshared ();
extern "C" void Action_1_BeginInvoke_m25227_gshared ();
extern "C" void Action_1_EndInvoke_m25228_gshared ();
extern "C" void Dictionary_2__ctor_m25918_gshared ();
extern "C" void Dictionary_2__ctor_m25919_gshared ();
extern "C" void Dictionary_2__ctor_m25920_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m25921_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m25922_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25923_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25924_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m25925_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25926_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25927_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25928_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25929_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25930_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25931_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25932_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25933_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25934_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25935_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25936_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25937_gshared ();
extern "C" void Dictionary_2_get_Count_m25938_gshared ();
extern "C" void Dictionary_2_get_Item_m25939_gshared ();
extern "C" void Dictionary_2_set_Item_m25940_gshared ();
extern "C" void Dictionary_2_Init_m25941_gshared ();
extern "C" void Dictionary_2_InitArrays_m25942_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m25943_gshared ();
extern "C" void Dictionary_2_make_pair_m25944_gshared ();
extern "C" void Dictionary_2_pick_key_m25945_gshared ();
extern "C" void Dictionary_2_pick_value_m25946_gshared ();
extern "C" void Dictionary_2_CopyTo_m25947_gshared ();
extern "C" void Dictionary_2_Resize_m25948_gshared ();
extern "C" void Dictionary_2_Add_m25949_gshared ();
extern "C" void Dictionary_2_Clear_m25950_gshared ();
extern "C" void Dictionary_2_ContainsKey_m25951_gshared ();
extern "C" void Dictionary_2_ContainsValue_m25952_gshared ();
extern "C" void Dictionary_2_GetObjectData_m25953_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m25954_gshared ();
extern "C" void Dictionary_2_Remove_m25955_gshared ();
extern "C" void Dictionary_2_TryGetValue_m25956_gshared ();
extern "C" void Dictionary_2_get_Keys_m25957_gshared ();
extern "C" void Dictionary_2_get_Values_m25958_gshared ();
extern "C" void Dictionary_2_ToTKey_m25959_gshared ();
extern "C" void Dictionary_2_ToTValue_m25960_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m25961_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m25962_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m25963_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25964_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m25965_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25966_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25967_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25968_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25969_gshared ();
extern "C" void KeyValuePair_2__ctor_m25970_gshared ();
extern "C" void KeyValuePair_2_get_Key_m25971_gshared ();
extern "C" void KeyValuePair_2_set_Key_m25972_gshared ();
extern "C" void KeyValuePair_2_get_Value_m25973_gshared ();
extern "C" void KeyValuePair_2_set_Value_m25974_gshared ();
extern "C" void KeyValuePair_2_ToString_m25975_gshared ();
extern "C" void KeyCollection__ctor_m25976_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25977_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25978_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25979_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25980_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25981_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m25982_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25983_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25984_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25985_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m25986_gshared ();
extern "C" void KeyCollection_CopyTo_m25987_gshared ();
extern "C" void KeyCollection_GetEnumerator_m25988_gshared ();
extern "C" void KeyCollection_get_Count_m25989_gshared ();
extern "C" void Enumerator__ctor_m25990_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25991_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m25992_gshared ();
extern "C" void Enumerator_Dispose_m25993_gshared ();
extern "C" void Enumerator_MoveNext_m25994_gshared ();
extern "C" void Enumerator_get_Current_m25995_gshared ();
extern "C" void Enumerator__ctor_m25996_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25997_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m25998_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25999_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26000_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26001_gshared ();
extern "C" void Enumerator_MoveNext_m26002_gshared ();
extern "C" void Enumerator_get_Current_m26003_gshared ();
extern "C" void Enumerator_get_CurrentKey_m26004_gshared ();
extern "C" void Enumerator_get_CurrentValue_m26005_gshared ();
extern "C" void Enumerator_Reset_m26006_gshared ();
extern "C" void Enumerator_VerifyState_m26007_gshared ();
extern "C" void Enumerator_VerifyCurrent_m26008_gshared ();
extern "C" void Enumerator_Dispose_m26009_gshared ();
extern "C" void Transform_1__ctor_m26010_gshared ();
extern "C" void Transform_1_Invoke_m26011_gshared ();
extern "C" void Transform_1_BeginInvoke_m26012_gshared ();
extern "C" void Transform_1_EndInvoke_m26013_gshared ();
extern "C" void ValueCollection__ctor_m26014_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26015_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26016_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26017_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26018_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26019_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m26020_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26021_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26022_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26023_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m26024_gshared ();
extern "C" void ValueCollection_CopyTo_m26025_gshared ();
extern "C" void ValueCollection_GetEnumerator_m26026_gshared ();
extern "C" void ValueCollection_get_Count_m26027_gshared ();
extern "C" void Enumerator__ctor_m26028_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26029_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26030_gshared ();
extern "C" void Enumerator_Dispose_m26031_gshared ();
extern "C" void Enumerator_MoveNext_m26032_gshared ();
extern "C" void Enumerator_get_Current_m26033_gshared ();
extern "C" void Transform_1__ctor_m26034_gshared ();
extern "C" void Transform_1_Invoke_m26035_gshared ();
extern "C" void Transform_1_BeginInvoke_m26036_gshared ();
extern "C" void Transform_1_EndInvoke_m26037_gshared ();
extern "C" void Transform_1__ctor_m26038_gshared ();
extern "C" void Transform_1_Invoke_m26039_gshared ();
extern "C" void Transform_1_BeginInvoke_m26040_gshared ();
extern "C" void Transform_1_EndInvoke_m26041_gshared ();
extern "C" void Transform_1__ctor_m26042_gshared ();
extern "C" void Transform_1_Invoke_m26043_gshared ();
extern "C" void Transform_1_BeginInvoke_m26044_gshared ();
extern "C" void Transform_1_EndInvoke_m26045_gshared ();
extern "C" void ShimEnumerator__ctor_m26046_gshared ();
extern "C" void ShimEnumerator_MoveNext_m26047_gshared ();
extern "C" void ShimEnumerator_get_Entry_m26048_gshared ();
extern "C" void ShimEnumerator_get_Key_m26049_gshared ();
extern "C" void ShimEnumerator_get_Value_m26050_gshared ();
extern "C" void ShimEnumerator_get_Current_m26051_gshared ();
extern "C" void ShimEnumerator_Reset_m26052_gshared ();
extern "C" void EqualityComparer_1__ctor_m26053_gshared ();
extern "C" void EqualityComparer_1__cctor_m26054_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26055_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26056_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26057_gshared ();
extern "C" void DefaultComparer__ctor_m26058_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26059_gshared ();
extern "C" void DefaultComparer_Equals_m26060_gshared ();
extern "C" void Dictionary_2__ctor_m26061_gshared ();
extern "C" void Dictionary_2__ctor_m26062_gshared ();
extern "C" void Dictionary_2__ctor_m26063_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m26064_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m26065_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m26066_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m26067_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m26068_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m26069_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26070_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26071_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26072_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26073_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26074_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26075_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26076_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m26077_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26078_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26079_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26080_gshared ();
extern "C" void Dictionary_2_get_Count_m26081_gshared ();
extern "C" void Dictionary_2_get_Item_m26082_gshared ();
extern "C" void Dictionary_2_set_Item_m26083_gshared ();
extern "C" void Dictionary_2_Init_m26084_gshared ();
extern "C" void Dictionary_2_InitArrays_m26085_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m26086_gshared ();
extern "C" void Dictionary_2_make_pair_m26087_gshared ();
extern "C" void Dictionary_2_pick_key_m26088_gshared ();
extern "C" void Dictionary_2_pick_value_m26089_gshared ();
extern "C" void Dictionary_2_CopyTo_m26090_gshared ();
extern "C" void Dictionary_2_Resize_m26091_gshared ();
extern "C" void Dictionary_2_Add_m26092_gshared ();
extern "C" void Dictionary_2_Clear_m26093_gshared ();
extern "C" void Dictionary_2_ContainsKey_m26094_gshared ();
extern "C" void Dictionary_2_ContainsValue_m26095_gshared ();
extern "C" void Dictionary_2_GetObjectData_m26096_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m26097_gshared ();
extern "C" void Dictionary_2_Remove_m26098_gshared ();
extern "C" void Dictionary_2_TryGetValue_m26099_gshared ();
extern "C" void Dictionary_2_get_Keys_m26100_gshared ();
extern "C" void Dictionary_2_get_Values_m26101_gshared ();
extern "C" void Dictionary_2_ToTKey_m26102_gshared ();
extern "C" void Dictionary_2_ToTValue_m26103_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m26104_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m26105_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m26106_gshared ();
extern "C" void InternalEnumerator_1__ctor_m26107_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26108_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26109_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26110_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26111_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26112_gshared ();
extern "C" void KeyValuePair_2__ctor_m26113_gshared ();
extern "C" void KeyValuePair_2_get_Key_m26114_gshared ();
extern "C" void KeyValuePair_2_set_Key_m26115_gshared ();
extern "C" void KeyValuePair_2_get_Value_m26116_gshared ();
extern "C" void KeyValuePair_2_set_Value_m26117_gshared ();
extern "C" void KeyValuePair_2_ToString_m26118_gshared ();
extern "C" void InternalEnumerator_1__ctor_m26119_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26120_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26121_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26122_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26123_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26124_gshared ();
extern "C" void KeyCollection__ctor_m26125_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26126_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26127_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26128_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26129_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26130_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m26131_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26132_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26133_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26134_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m26135_gshared ();
extern "C" void KeyCollection_CopyTo_m26136_gshared ();
extern "C" void KeyCollection_GetEnumerator_m26137_gshared ();
extern "C" void KeyCollection_get_Count_m26138_gshared ();
extern "C" void Enumerator__ctor_m26139_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26140_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26141_gshared ();
extern "C" void Enumerator_Dispose_m26142_gshared ();
extern "C" void Enumerator_MoveNext_m26143_gshared ();
extern "C" void Enumerator_get_Current_m26144_gshared ();
extern "C" void Enumerator__ctor_m26145_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26146_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26147_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26148_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26149_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26150_gshared ();
extern "C" void Enumerator_MoveNext_m26151_gshared ();
extern "C" void Enumerator_get_Current_m26152_gshared ();
extern "C" void Enumerator_get_CurrentKey_m26153_gshared ();
extern "C" void Enumerator_get_CurrentValue_m26154_gshared ();
extern "C" void Enumerator_Reset_m26155_gshared ();
extern "C" void Enumerator_VerifyState_m26156_gshared ();
extern "C" void Enumerator_VerifyCurrent_m26157_gshared ();
extern "C" void Enumerator_Dispose_m26158_gshared ();
extern "C" void Transform_1__ctor_m26159_gshared ();
extern "C" void Transform_1_Invoke_m26160_gshared ();
extern "C" void Transform_1_BeginInvoke_m26161_gshared ();
extern "C" void Transform_1_EndInvoke_m26162_gshared ();
extern "C" void ValueCollection__ctor_m26163_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26164_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26165_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26166_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26167_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26168_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m26169_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26170_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26171_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26172_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m26173_gshared ();
extern "C" void ValueCollection_CopyTo_m26174_gshared ();
extern "C" void ValueCollection_GetEnumerator_m26175_gshared ();
extern "C" void ValueCollection_get_Count_m26176_gshared ();
extern "C" void Enumerator__ctor_m26177_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26178_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26179_gshared ();
extern "C" void Enumerator_Dispose_m26180_gshared ();
extern "C" void Enumerator_MoveNext_m26181_gshared ();
extern "C" void Enumerator_get_Current_m26182_gshared ();
extern "C" void Transform_1__ctor_m26183_gshared ();
extern "C" void Transform_1_Invoke_m26184_gshared ();
extern "C" void Transform_1_BeginInvoke_m26185_gshared ();
extern "C" void Transform_1_EndInvoke_m26186_gshared ();
extern "C" void Transform_1__ctor_m26187_gshared ();
extern "C" void Transform_1_Invoke_m26188_gshared ();
extern "C" void Transform_1_BeginInvoke_m26189_gshared ();
extern "C" void Transform_1_EndInvoke_m26190_gshared ();
extern "C" void Transform_1__ctor_m26191_gshared ();
extern "C" void Transform_1_Invoke_m26192_gshared ();
extern "C" void Transform_1_BeginInvoke_m26193_gshared ();
extern "C" void Transform_1_EndInvoke_m26194_gshared ();
extern "C" void ShimEnumerator__ctor_m26195_gshared ();
extern "C" void ShimEnumerator_MoveNext_m26196_gshared ();
extern "C" void ShimEnumerator_get_Entry_m26197_gshared ();
extern "C" void ShimEnumerator_get_Key_m26198_gshared ();
extern "C" void ShimEnumerator_get_Value_m26199_gshared ();
extern "C" void ShimEnumerator_get_Current_m26200_gshared ();
extern "C" void ShimEnumerator_Reset_m26201_gshared ();
extern "C" void EqualityComparer_1__ctor_m26202_gshared ();
extern "C" void EqualityComparer_1__cctor_m26203_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26204_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26205_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26206_gshared ();
extern "C" void DefaultComparer__ctor_m26207_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26208_gshared ();
extern "C" void DefaultComparer_Equals_m26209_gshared ();
extern "C" void List_1__ctor_m26302_gshared ();
extern "C" void List_1__ctor_m26303_gshared ();
extern "C" void List_1__cctor_m26304_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26305_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m26306_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m26307_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m26308_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m26309_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m26310_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m26311_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m26312_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26313_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m26314_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m26315_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m26316_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m26317_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m26318_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m26319_gshared ();
extern "C" void List_1_Add_m26320_gshared ();
extern "C" void List_1_GrowIfNeeded_m26321_gshared ();
extern "C" void List_1_AddCollection_m26322_gshared ();
extern "C" void List_1_AddEnumerable_m26323_gshared ();
extern "C" void List_1_AddRange_m26324_gshared ();
extern "C" void List_1_AsReadOnly_m26325_gshared ();
extern "C" void List_1_Clear_m26326_gshared ();
extern "C" void List_1_Contains_m26327_gshared ();
extern "C" void List_1_CopyTo_m26328_gshared ();
extern "C" void List_1_Find_m26329_gshared ();
extern "C" void List_1_CheckMatch_m26330_gshared ();
extern "C" void List_1_GetIndex_m26331_gshared ();
extern "C" void List_1_GetEnumerator_m26332_gshared ();
extern "C" void List_1_IndexOf_m26333_gshared ();
extern "C" void List_1_Shift_m26334_gshared ();
extern "C" void List_1_CheckIndex_m26335_gshared ();
extern "C" void List_1_Insert_m26336_gshared ();
extern "C" void List_1_CheckCollection_m26337_gshared ();
extern "C" void List_1_Remove_m26338_gshared ();
extern "C" void List_1_RemoveAll_m26339_gshared ();
extern "C" void List_1_RemoveAt_m26340_gshared ();
extern "C" void List_1_Reverse_m26341_gshared ();
extern "C" void List_1_Sort_m26342_gshared ();
extern "C" void List_1_Sort_m26343_gshared ();
extern "C" void List_1_ToArray_m26344_gshared ();
extern "C" void List_1_TrimExcess_m26345_gshared ();
extern "C" void List_1_get_Capacity_m26346_gshared ();
extern "C" void List_1_set_Capacity_m26347_gshared ();
extern "C" void List_1_get_Count_m26348_gshared ();
extern "C" void List_1_get_Item_m26349_gshared ();
extern "C" void List_1_set_Item_m26350_gshared ();
extern "C" void Enumerator__ctor_m26351_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26352_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26353_gshared ();
extern "C" void Enumerator_Dispose_m26354_gshared ();
extern "C" void Enumerator_VerifyState_m26355_gshared ();
extern "C" void Enumerator_MoveNext_m26356_gshared ();
extern "C" void Enumerator_get_Current_m26357_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m26358_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26359_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m26360_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m26361_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m26362_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m26363_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m26364_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m26365_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26366_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m26367_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m26368_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m26369_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m26370_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m26371_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m26372_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m26373_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m26374_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m26375_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m26376_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m26377_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m26378_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m26379_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m26380_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m26381_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m26382_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m26383_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m26384_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m26385_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m26386_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m26387_gshared ();
extern "C" void Collection_1__ctor_m26388_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26389_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m26390_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m26391_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m26392_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m26393_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m26394_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m26395_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m26396_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m26397_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m26398_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m26399_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m26400_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m26401_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m26402_gshared ();
extern "C" void Collection_1_Add_m26403_gshared ();
extern "C" void Collection_1_Clear_m26404_gshared ();
extern "C" void Collection_1_ClearItems_m26405_gshared ();
extern "C" void Collection_1_Contains_m26406_gshared ();
extern "C" void Collection_1_CopyTo_m26407_gshared ();
extern "C" void Collection_1_GetEnumerator_m26408_gshared ();
extern "C" void Collection_1_IndexOf_m26409_gshared ();
extern "C" void Collection_1_Insert_m26410_gshared ();
extern "C" void Collection_1_InsertItem_m26411_gshared ();
extern "C" void Collection_1_Remove_m26412_gshared ();
extern "C" void Collection_1_RemoveAt_m26413_gshared ();
extern "C" void Collection_1_RemoveItem_m26414_gshared ();
extern "C" void Collection_1_get_Count_m26415_gshared ();
extern "C" void Collection_1_get_Item_m26416_gshared ();
extern "C" void Collection_1_set_Item_m26417_gshared ();
extern "C" void Collection_1_SetItem_m26418_gshared ();
extern "C" void Collection_1_IsValidItem_m26419_gshared ();
extern "C" void Collection_1_ConvertItem_m26420_gshared ();
extern "C" void Collection_1_CheckWritable_m26421_gshared ();
extern "C" void Collection_1_IsSynchronized_m26422_gshared ();
extern "C" void Collection_1_IsFixedSize_m26423_gshared ();
extern "C" void EqualityComparer_1__ctor_m26424_gshared ();
extern "C" void EqualityComparer_1__cctor_m26425_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26426_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26427_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26428_gshared ();
extern "C" void DefaultComparer__ctor_m26429_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26430_gshared ();
extern "C" void DefaultComparer_Equals_m26431_gshared ();
extern "C" void Predicate_1__ctor_m26432_gshared ();
extern "C" void Predicate_1_Invoke_m26433_gshared ();
extern "C" void Predicate_1_BeginInvoke_m26434_gshared ();
extern "C" void Predicate_1_EndInvoke_m26435_gshared ();
extern "C" void Comparer_1__ctor_m26436_gshared ();
extern "C" void Comparer_1__cctor_m26437_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m26438_gshared ();
extern "C" void Comparer_1_get_Default_m26439_gshared ();
extern "C" void DefaultComparer__ctor_m26440_gshared ();
extern "C" void DefaultComparer_Compare_m26441_gshared ();
extern "C" void Comparison_1__ctor_m26442_gshared ();
extern "C" void Comparison_1_Invoke_m26443_gshared ();
extern "C" void Comparison_1_BeginInvoke_m26444_gshared ();
extern "C" void Comparison_1_EndInvoke_m26445_gshared ();
extern "C" void Dictionary_2__ctor_m26546_gshared ();
extern "C" void Dictionary_2__ctor_m26548_gshared ();
extern "C" void Dictionary_2__ctor_m26550_gshared ();
extern "C" void Dictionary_2__ctor_m26552_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m26554_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m26556_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m26558_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m26560_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m26562_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m26564_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26566_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26568_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26570_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26572_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26574_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26576_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26578_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m26580_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26582_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26584_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26586_gshared ();
extern "C" void Dictionary_2_get_Count_m26588_gshared ();
extern "C" void Dictionary_2_get_Item_m26590_gshared ();
extern "C" void Dictionary_2_set_Item_m26592_gshared ();
extern "C" void Dictionary_2_Init_m26594_gshared ();
extern "C" void Dictionary_2_InitArrays_m26596_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m26598_gshared ();
extern "C" void Dictionary_2_make_pair_m26600_gshared ();
extern "C" void Dictionary_2_pick_key_m26602_gshared ();
extern "C" void Dictionary_2_pick_value_m26604_gshared ();
extern "C" void Dictionary_2_CopyTo_m26606_gshared ();
extern "C" void Dictionary_2_Resize_m26608_gshared ();
extern "C" void Dictionary_2_Add_m26610_gshared ();
extern "C" void Dictionary_2_Clear_m26612_gshared ();
extern "C" void Dictionary_2_ContainsKey_m26614_gshared ();
extern "C" void Dictionary_2_ContainsValue_m26616_gshared ();
extern "C" void Dictionary_2_GetObjectData_m26618_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m26620_gshared ();
extern "C" void Dictionary_2_Remove_m26622_gshared ();
extern "C" void Dictionary_2_TryGetValue_m26624_gshared ();
extern "C" void Dictionary_2_get_Keys_m26626_gshared ();
extern "C" void Dictionary_2_get_Values_m26628_gshared ();
extern "C" void Dictionary_2_ToTKey_m26630_gshared ();
extern "C" void Dictionary_2_ToTValue_m26632_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m26634_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m26636_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m26638_gshared ();
extern "C" void InternalEnumerator_1__ctor_m26639_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26640_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26641_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26642_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26643_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26644_gshared ();
extern "C" void KeyValuePair_2__ctor_m26645_gshared ();
extern "C" void KeyValuePair_2_get_Key_m26646_gshared ();
extern "C" void KeyValuePair_2_set_Key_m26647_gshared ();
extern "C" void KeyValuePair_2_get_Value_m26648_gshared ();
extern "C" void KeyValuePair_2_set_Value_m26649_gshared ();
extern "C" void KeyValuePair_2_ToString_m26650_gshared ();
extern "C" void InternalEnumerator_1__ctor_m26651_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26652_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26653_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26654_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26655_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26656_gshared ();
extern "C" void KeyCollection__ctor_m26657_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26658_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26659_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26660_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26661_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26662_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m26663_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26664_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26665_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26666_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m26667_gshared ();
extern "C" void KeyCollection_CopyTo_m26668_gshared ();
extern "C" void KeyCollection_GetEnumerator_m26669_gshared ();
extern "C" void KeyCollection_get_Count_m26670_gshared ();
extern "C" void Enumerator__ctor_m26671_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26672_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26673_gshared ();
extern "C" void Enumerator_Dispose_m26674_gshared ();
extern "C" void Enumerator_MoveNext_m26675_gshared ();
extern "C" void Enumerator_get_Current_m26676_gshared ();
extern "C" void Enumerator__ctor_m26677_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26678_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26679_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26680_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26681_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26682_gshared ();
extern "C" void Enumerator_MoveNext_m26683_gshared ();
extern "C" void Enumerator_get_Current_m26684_gshared ();
extern "C" void Enumerator_get_CurrentKey_m26685_gshared ();
extern "C" void Enumerator_get_CurrentValue_m26686_gshared ();
extern "C" void Enumerator_Reset_m26687_gshared ();
extern "C" void Enumerator_VerifyState_m26688_gshared ();
extern "C" void Enumerator_VerifyCurrent_m26689_gshared ();
extern "C" void Enumerator_Dispose_m26690_gshared ();
extern "C" void Transform_1__ctor_m26691_gshared ();
extern "C" void Transform_1_Invoke_m26692_gshared ();
extern "C" void Transform_1_BeginInvoke_m26693_gshared ();
extern "C" void Transform_1_EndInvoke_m26694_gshared ();
extern "C" void ValueCollection__ctor_m26695_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26696_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26697_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26698_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26699_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26700_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m26701_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26702_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26703_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26704_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m26705_gshared ();
extern "C" void ValueCollection_CopyTo_m26706_gshared ();
extern "C" void ValueCollection_GetEnumerator_m26707_gshared ();
extern "C" void ValueCollection_get_Count_m26708_gshared ();
extern "C" void Enumerator__ctor_m26709_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26710_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26711_gshared ();
extern "C" void Enumerator_Dispose_m26712_gshared ();
extern "C" void Enumerator_MoveNext_m26713_gshared ();
extern "C" void Enumerator_get_Current_m26714_gshared ();
extern "C" void Transform_1__ctor_m26715_gshared ();
extern "C" void Transform_1_Invoke_m26716_gshared ();
extern "C" void Transform_1_BeginInvoke_m26717_gshared ();
extern "C" void Transform_1_EndInvoke_m26718_gshared ();
extern "C" void Transform_1__ctor_m26719_gshared ();
extern "C" void Transform_1_Invoke_m26720_gshared ();
extern "C" void Transform_1_BeginInvoke_m26721_gshared ();
extern "C" void Transform_1_EndInvoke_m26722_gshared ();
extern "C" void Transform_1__ctor_m26723_gshared ();
extern "C" void Transform_1_Invoke_m26724_gshared ();
extern "C" void Transform_1_BeginInvoke_m26725_gshared ();
extern "C" void Transform_1_EndInvoke_m26726_gshared ();
extern "C" void ShimEnumerator__ctor_m26727_gshared ();
extern "C" void ShimEnumerator_MoveNext_m26728_gshared ();
extern "C" void ShimEnumerator_get_Entry_m26729_gshared ();
extern "C" void ShimEnumerator_get_Key_m26730_gshared ();
extern "C" void ShimEnumerator_get_Value_m26731_gshared ();
extern "C" void ShimEnumerator_get_Current_m26732_gshared ();
extern "C" void ShimEnumerator_Reset_m26733_gshared ();
extern "C" void EqualityComparer_1__ctor_m26734_gshared ();
extern "C" void EqualityComparer_1__cctor_m26735_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26736_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26737_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26738_gshared ();
extern "C" void DefaultComparer__ctor_m26739_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26740_gshared ();
extern "C" void DefaultComparer_Equals_m26741_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27294_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27295_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27296_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27297_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27298_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27299_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27419_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27420_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27421_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27422_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27423_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27424_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27558_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27559_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27560_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27561_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27562_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27563_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27570_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27571_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27572_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27573_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27574_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27575_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27582_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27583_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27584_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27585_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27586_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27587_gshared ();
extern "C" void Dictionary_2__ctor_m27595_gshared ();
extern "C" void Dictionary_2__ctor_m27597_gshared ();
extern "C" void Dictionary_2__ctor_m27599_gshared ();
extern "C" void Dictionary_2__ctor_m27601_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m27603_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m27605_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27607_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27609_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m27611_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27613_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27615_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27617_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27619_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27621_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27623_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27625_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27627_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27629_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27631_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27633_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27635_gshared ();
extern "C" void Dictionary_2_get_Count_m27637_gshared ();
extern "C" void Dictionary_2_get_Item_m27639_gshared ();
extern "C" void Dictionary_2_set_Item_m27641_gshared ();
extern "C" void Dictionary_2_Init_m27643_gshared ();
extern "C" void Dictionary_2_InitArrays_m27645_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m27647_gshared ();
extern "C" void Dictionary_2_make_pair_m27649_gshared ();
extern "C" void Dictionary_2_pick_key_m27651_gshared ();
extern "C" void Dictionary_2_pick_value_m27653_gshared ();
extern "C" void Dictionary_2_CopyTo_m27655_gshared ();
extern "C" void Dictionary_2_Resize_m27657_gshared ();
extern "C" void Dictionary_2_Add_m27659_gshared ();
extern "C" void Dictionary_2_Clear_m27661_gshared ();
extern "C" void Dictionary_2_ContainsKey_m27663_gshared ();
extern "C" void Dictionary_2_ContainsValue_m27665_gshared ();
extern "C" void Dictionary_2_GetObjectData_m27667_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m27669_gshared ();
extern "C" void Dictionary_2_Remove_m27671_gshared ();
extern "C" void Dictionary_2_TryGetValue_m27673_gshared ();
extern "C" void Dictionary_2_get_Keys_m27675_gshared ();
extern "C" void Dictionary_2_get_Values_m27677_gshared ();
extern "C" void Dictionary_2_ToTKey_m27679_gshared ();
extern "C" void Dictionary_2_ToTValue_m27681_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m27683_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m27685_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m27687_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27688_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27689_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27690_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27691_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27692_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27693_gshared ();
extern "C" void KeyValuePair_2__ctor_m27694_gshared ();
extern "C" void KeyValuePair_2_get_Key_m27695_gshared ();
extern "C" void KeyValuePair_2_set_Key_m27696_gshared ();
extern "C" void KeyValuePair_2_get_Value_m27697_gshared ();
extern "C" void KeyValuePair_2_set_Value_m27698_gshared ();
extern "C" void KeyValuePair_2_ToString_m27699_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27700_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27701_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27702_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27703_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27704_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27705_gshared ();
extern "C" void KeyCollection__ctor_m27706_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27707_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27708_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27709_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27710_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27711_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m27712_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27713_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27714_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27715_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m27716_gshared ();
extern "C" void KeyCollection_CopyTo_m27717_gshared ();
extern "C" void KeyCollection_GetEnumerator_m27718_gshared ();
extern "C" void KeyCollection_get_Count_m27719_gshared ();
extern "C" void Enumerator__ctor_m27720_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27721_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m27722_gshared ();
extern "C" void Enumerator_Dispose_m27723_gshared ();
extern "C" void Enumerator_MoveNext_m27724_gshared ();
extern "C" void Enumerator_get_Current_m27725_gshared ();
extern "C" void Enumerator__ctor_m27726_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27727_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m27728_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27729_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27730_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27731_gshared ();
extern "C" void Enumerator_MoveNext_m27732_gshared ();
extern "C" void Enumerator_get_Current_m27733_gshared ();
extern "C" void Enumerator_get_CurrentKey_m27734_gshared ();
extern "C" void Enumerator_get_CurrentValue_m27735_gshared ();
extern "C" void Enumerator_Reset_m27736_gshared ();
extern "C" void Enumerator_VerifyState_m27737_gshared ();
extern "C" void Enumerator_VerifyCurrent_m27738_gshared ();
extern "C" void Enumerator_Dispose_m27739_gshared ();
extern "C" void Transform_1__ctor_m27740_gshared ();
extern "C" void Transform_1_Invoke_m27741_gshared ();
extern "C" void Transform_1_BeginInvoke_m27742_gshared ();
extern "C" void Transform_1_EndInvoke_m27743_gshared ();
extern "C" void ValueCollection__ctor_m27744_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27745_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27746_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27747_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27748_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27749_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m27750_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27751_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27752_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27753_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m27754_gshared ();
extern "C" void ValueCollection_CopyTo_m27755_gshared ();
extern "C" void ValueCollection_GetEnumerator_m27756_gshared ();
extern "C" void ValueCollection_get_Count_m27757_gshared ();
extern "C" void Enumerator__ctor_m27758_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27759_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m27760_gshared ();
extern "C" void Enumerator_Dispose_m27761_gshared ();
extern "C" void Enumerator_MoveNext_m27762_gshared ();
extern "C" void Enumerator_get_Current_m27763_gshared ();
extern "C" void Transform_1__ctor_m27764_gshared ();
extern "C" void Transform_1_Invoke_m27765_gshared ();
extern "C" void Transform_1_BeginInvoke_m27766_gshared ();
extern "C" void Transform_1_EndInvoke_m27767_gshared ();
extern "C" void Transform_1__ctor_m27768_gshared ();
extern "C" void Transform_1_Invoke_m27769_gshared ();
extern "C" void Transform_1_BeginInvoke_m27770_gshared ();
extern "C" void Transform_1_EndInvoke_m27771_gshared ();
extern "C" void Transform_1__ctor_m27772_gshared ();
extern "C" void Transform_1_Invoke_m27773_gshared ();
extern "C" void Transform_1_BeginInvoke_m27774_gshared ();
extern "C" void Transform_1_EndInvoke_m27775_gshared ();
extern "C" void ShimEnumerator__ctor_m27776_gshared ();
extern "C" void ShimEnumerator_MoveNext_m27777_gshared ();
extern "C" void ShimEnumerator_get_Entry_m27778_gshared ();
extern "C" void ShimEnumerator_get_Key_m27779_gshared ();
extern "C" void ShimEnumerator_get_Value_m27780_gshared ();
extern "C" void ShimEnumerator_get_Current_m27781_gshared ();
extern "C" void ShimEnumerator_Reset_m27782_gshared ();
extern "C" void EqualityComparer_1__ctor_m27783_gshared ();
extern "C" void EqualityComparer_1__cctor_m27784_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27785_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27786_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27787_gshared ();
extern "C" void DefaultComparer__ctor_m27788_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27789_gshared ();
extern "C" void DefaultComparer_Equals_m27790_gshared ();
extern "C" void List_1__ctor_m27843_gshared ();
extern "C" void List_1__ctor_m27844_gshared ();
extern "C" void List_1__cctor_m27845_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27846_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m27847_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m27848_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m27849_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m27850_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m27851_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m27852_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m27853_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27854_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m27855_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m27856_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m27857_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m27858_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m27859_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m27860_gshared ();
extern "C" void List_1_Add_m27861_gshared ();
extern "C" void List_1_GrowIfNeeded_m27862_gshared ();
extern "C" void List_1_AddCollection_m27863_gshared ();
extern "C" void List_1_AddEnumerable_m27864_gshared ();
extern "C" void List_1_AddRange_m27865_gshared ();
extern "C" void List_1_AsReadOnly_m27866_gshared ();
extern "C" void List_1_Clear_m27867_gshared ();
extern "C" void List_1_Contains_m27868_gshared ();
extern "C" void List_1_CopyTo_m27869_gshared ();
extern "C" void List_1_Find_m27870_gshared ();
extern "C" void List_1_CheckMatch_m27871_gshared ();
extern "C" void List_1_GetIndex_m27872_gshared ();
extern "C" void List_1_GetEnumerator_m27873_gshared ();
extern "C" void List_1_IndexOf_m27874_gshared ();
extern "C" void List_1_Shift_m27875_gshared ();
extern "C" void List_1_CheckIndex_m27876_gshared ();
extern "C" void List_1_Insert_m27877_gshared ();
extern "C" void List_1_CheckCollection_m27878_gshared ();
extern "C" void List_1_Remove_m27879_gshared ();
extern "C" void List_1_RemoveAll_m27880_gshared ();
extern "C" void List_1_RemoveAt_m27881_gshared ();
extern "C" void List_1_Reverse_m27882_gshared ();
extern "C" void List_1_Sort_m27883_gshared ();
extern "C" void List_1_Sort_m27884_gshared ();
extern "C" void List_1_ToArray_m27885_gshared ();
extern "C" void List_1_TrimExcess_m27886_gshared ();
extern "C" void List_1_get_Capacity_m27887_gshared ();
extern "C" void List_1_set_Capacity_m27888_gshared ();
extern "C" void List_1_get_Count_m27889_gshared ();
extern "C" void List_1_get_Item_m27890_gshared ();
extern "C" void List_1_set_Item_m27891_gshared ();
extern "C" void Enumerator__ctor_m27892_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m27893_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27894_gshared ();
extern "C" void Enumerator_Dispose_m27895_gshared ();
extern "C" void Enumerator_VerifyState_m27896_gshared ();
extern "C" void Enumerator_MoveNext_m27897_gshared ();
extern "C" void Enumerator_get_Current_m27898_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m27899_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27900_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m27901_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m27902_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m27903_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m27904_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m27905_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m27906_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27907_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m27908_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m27909_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m27910_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m27911_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m27912_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m27913_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m27914_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m27915_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m27916_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m27917_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m27918_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m27919_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m27920_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m27921_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m27922_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m27923_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m27924_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m27925_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m27926_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m27927_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m27928_gshared ();
extern "C" void Collection_1__ctor_m27929_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27930_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m27931_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m27932_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m27933_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m27934_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m27935_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m27936_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m27937_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m27938_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m27939_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m27940_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m27941_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m27942_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m27943_gshared ();
extern "C" void Collection_1_Add_m27944_gshared ();
extern "C" void Collection_1_Clear_m27945_gshared ();
extern "C" void Collection_1_ClearItems_m27946_gshared ();
extern "C" void Collection_1_Contains_m27947_gshared ();
extern "C" void Collection_1_CopyTo_m27948_gshared ();
extern "C" void Collection_1_GetEnumerator_m27949_gshared ();
extern "C" void Collection_1_IndexOf_m27950_gshared ();
extern "C" void Collection_1_Insert_m27951_gshared ();
extern "C" void Collection_1_InsertItem_m27952_gshared ();
extern "C" void Collection_1_Remove_m27953_gshared ();
extern "C" void Collection_1_RemoveAt_m27954_gshared ();
extern "C" void Collection_1_RemoveItem_m27955_gshared ();
extern "C" void Collection_1_get_Count_m27956_gshared ();
extern "C" void Collection_1_get_Item_m27957_gshared ();
extern "C" void Collection_1_set_Item_m27958_gshared ();
extern "C" void Collection_1_SetItem_m27959_gshared ();
extern "C" void Collection_1_IsValidItem_m27960_gshared ();
extern "C" void Collection_1_ConvertItem_m27961_gshared ();
extern "C" void Collection_1_CheckWritable_m27962_gshared ();
extern "C" void Collection_1_IsSynchronized_m27963_gshared ();
extern "C" void Collection_1_IsFixedSize_m27964_gshared ();
extern "C" void EqualityComparer_1__ctor_m27965_gshared ();
extern "C" void EqualityComparer_1__cctor_m27966_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27967_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27968_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27969_gshared ();
extern "C" void DefaultComparer__ctor_m27970_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27971_gshared ();
extern "C" void DefaultComparer_Equals_m27972_gshared ();
extern "C" void Predicate_1__ctor_m27973_gshared ();
extern "C" void Predicate_1_Invoke_m27974_gshared ();
extern "C" void Predicate_1_BeginInvoke_m27975_gshared ();
extern "C" void Predicate_1_EndInvoke_m27976_gshared ();
extern "C" void Comparer_1__ctor_m27977_gshared ();
extern "C" void Comparer_1__cctor_m27978_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27979_gshared ();
extern "C" void Comparer_1_get_Default_m27980_gshared ();
extern "C" void DefaultComparer__ctor_m27981_gshared ();
extern "C" void DefaultComparer_Compare_m27982_gshared ();
extern "C" void Comparison_1__ctor_m27983_gshared ();
extern "C" void Comparison_1_Invoke_m27984_gshared ();
extern "C" void Comparison_1_BeginInvoke_m27985_gshared ();
extern "C" void Comparison_1_EndInvoke_m27986_gshared ();
extern "C" void List_1__ctor_m27987_gshared ();
extern "C" void List_1__ctor_m27988_gshared ();
extern "C" void List_1__cctor_m27989_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27990_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m27991_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m27992_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m27993_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m27994_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m27995_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m27996_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m27997_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27998_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m27999_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m28000_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m28001_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m28002_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m28003_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m28004_gshared ();
extern "C" void List_1_Add_m28005_gshared ();
extern "C" void List_1_GrowIfNeeded_m28006_gshared ();
extern "C" void List_1_AddCollection_m28007_gshared ();
extern "C" void List_1_AddEnumerable_m28008_gshared ();
extern "C" void List_1_AddRange_m28009_gshared ();
extern "C" void List_1_AsReadOnly_m28010_gshared ();
extern "C" void List_1_Clear_m28011_gshared ();
extern "C" void List_1_Contains_m28012_gshared ();
extern "C" void List_1_CopyTo_m28013_gshared ();
extern "C" void List_1_Find_m28014_gshared ();
extern "C" void List_1_CheckMatch_m28015_gshared ();
extern "C" void List_1_GetIndex_m28016_gshared ();
extern "C" void List_1_GetEnumerator_m28017_gshared ();
extern "C" void List_1_IndexOf_m28018_gshared ();
extern "C" void List_1_Shift_m28019_gshared ();
extern "C" void List_1_CheckIndex_m28020_gshared ();
extern "C" void List_1_Insert_m28021_gshared ();
extern "C" void List_1_CheckCollection_m28022_gshared ();
extern "C" void List_1_Remove_m28023_gshared ();
extern "C" void List_1_RemoveAll_m28024_gshared ();
extern "C" void List_1_RemoveAt_m28025_gshared ();
extern "C" void List_1_Reverse_m28026_gshared ();
extern "C" void List_1_Sort_m28027_gshared ();
extern "C" void List_1_Sort_m28028_gshared ();
extern "C" void List_1_ToArray_m28029_gshared ();
extern "C" void List_1_TrimExcess_m28030_gshared ();
extern "C" void List_1_get_Capacity_m28031_gshared ();
extern "C" void List_1_set_Capacity_m28032_gshared ();
extern "C" void List_1_get_Count_m28033_gshared ();
extern "C" void List_1_get_Item_m28034_gshared ();
extern "C" void List_1_set_Item_m28035_gshared ();
extern "C" void Enumerator__ctor_m28036_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m28037_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m28038_gshared ();
extern "C" void Enumerator_Dispose_m28039_gshared ();
extern "C" void Enumerator_VerifyState_m28040_gshared ();
extern "C" void Enumerator_MoveNext_m28041_gshared ();
extern "C" void Enumerator_get_Current_m28042_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m28043_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m28044_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m28045_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m28046_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m28047_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m28048_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m28049_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m28050_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28051_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m28052_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m28053_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m28054_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m28055_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m28056_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m28057_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m28058_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m28059_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m28060_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m28061_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m28062_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m28063_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m28064_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m28065_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m28066_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m28067_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m28068_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m28069_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m28070_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m28071_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m28072_gshared ();
extern "C" void Collection_1__ctor_m28073_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28074_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m28075_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m28076_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m28077_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m28078_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m28079_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m28080_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m28081_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m28082_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m28083_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m28084_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m28085_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m28086_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m28087_gshared ();
extern "C" void Collection_1_Add_m28088_gshared ();
extern "C" void Collection_1_Clear_m28089_gshared ();
extern "C" void Collection_1_ClearItems_m28090_gshared ();
extern "C" void Collection_1_Contains_m28091_gshared ();
extern "C" void Collection_1_CopyTo_m28092_gshared ();
extern "C" void Collection_1_GetEnumerator_m28093_gshared ();
extern "C" void Collection_1_IndexOf_m28094_gshared ();
extern "C" void Collection_1_Insert_m28095_gshared ();
extern "C" void Collection_1_InsertItem_m28096_gshared ();
extern "C" void Collection_1_Remove_m28097_gshared ();
extern "C" void Collection_1_RemoveAt_m28098_gshared ();
extern "C" void Collection_1_RemoveItem_m28099_gshared ();
extern "C" void Collection_1_get_Count_m28100_gshared ();
extern "C" void Collection_1_get_Item_m28101_gshared ();
extern "C" void Collection_1_set_Item_m28102_gshared ();
extern "C" void Collection_1_SetItem_m28103_gshared ();
extern "C" void Collection_1_IsValidItem_m28104_gshared ();
extern "C" void Collection_1_ConvertItem_m28105_gshared ();
extern "C" void Collection_1_CheckWritable_m28106_gshared ();
extern "C" void Collection_1_IsSynchronized_m28107_gshared ();
extern "C" void Collection_1_IsFixedSize_m28108_gshared ();
extern "C" void EqualityComparer_1__ctor_m28109_gshared ();
extern "C" void EqualityComparer_1__cctor_m28110_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m28111_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m28112_gshared ();
extern "C" void EqualityComparer_1_get_Default_m28113_gshared ();
extern "C" void DefaultComparer__ctor_m28114_gshared ();
extern "C" void DefaultComparer_GetHashCode_m28115_gshared ();
extern "C" void DefaultComparer_Equals_m28116_gshared ();
extern "C" void Predicate_1__ctor_m28117_gshared ();
extern "C" void Predicate_1_Invoke_m28118_gshared ();
extern "C" void Predicate_1_BeginInvoke_m28119_gshared ();
extern "C" void Predicate_1_EndInvoke_m28120_gshared ();
extern "C" void Comparer_1__ctor_m28121_gshared ();
extern "C" void Comparer_1__cctor_m28122_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m28123_gshared ();
extern "C" void Comparer_1_get_Default_m28124_gshared ();
extern "C" void DefaultComparer__ctor_m28125_gshared ();
extern "C" void DefaultComparer_Compare_m28126_gshared ();
extern "C" void Comparison_1__ctor_m28127_gshared ();
extern "C" void Comparison_1_Invoke_m28128_gshared ();
extern "C" void Comparison_1_BeginInvoke_m28129_gshared ();
extern "C" void Comparison_1_EndInvoke_m28130_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m28154_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m28155_gshared ();
extern "C" void InvokableCall_1__ctor_m28156_gshared ();
extern "C" void InvokableCall_1__ctor_m28157_gshared ();
extern "C" void InvokableCall_1_Invoke_m28158_gshared ();
extern "C" void InvokableCall_1_Find_m28159_gshared ();
extern "C" void UnityAction_1__ctor_m28160_gshared ();
extern "C" void UnityAction_1_Invoke_m28161_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m28162_gshared ();
extern "C" void UnityAction_1_EndInvoke_m28163_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m28169_gshared ();
extern "C" void InternalEnumerator_1__ctor_m28455_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m28456_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28457_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m28458_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m28459_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m28460_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29046_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29047_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29048_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29049_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29050_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29051_gshared ();
extern "C" void Dictionary_2__ctor_m29087_gshared ();
extern "C" void Dictionary_2__ctor_m29090_gshared ();
extern "C" void Dictionary_2__ctor_m29092_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m29094_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m29096_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m29098_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m29100_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m29102_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m29104_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m29106_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m29108_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m29110_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m29112_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m29114_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m29116_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29118_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m29120_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m29122_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m29124_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m29126_gshared ();
extern "C" void Dictionary_2_get_Count_m29128_gshared ();
extern "C" void Dictionary_2_get_Item_m29130_gshared ();
extern "C" void Dictionary_2_set_Item_m29132_gshared ();
extern "C" void Dictionary_2_Init_m29134_gshared ();
extern "C" void Dictionary_2_InitArrays_m29136_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m29138_gshared ();
extern "C" void Dictionary_2_make_pair_m29140_gshared ();
extern "C" void Dictionary_2_pick_key_m29142_gshared ();
extern "C" void Dictionary_2_pick_value_m29144_gshared ();
extern "C" void Dictionary_2_CopyTo_m29146_gshared ();
extern "C" void Dictionary_2_Resize_m29148_gshared ();
extern "C" void Dictionary_2_Add_m29150_gshared ();
extern "C" void Dictionary_2_Clear_m29152_gshared ();
extern "C" void Dictionary_2_ContainsKey_m29154_gshared ();
extern "C" void Dictionary_2_ContainsValue_m29156_gshared ();
extern "C" void Dictionary_2_GetObjectData_m29158_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m29160_gshared ();
extern "C" void Dictionary_2_Remove_m29162_gshared ();
extern "C" void Dictionary_2_TryGetValue_m29164_gshared ();
extern "C" void Dictionary_2_get_Keys_m29166_gshared ();
extern "C" void Dictionary_2_get_Values_m29168_gshared ();
extern "C" void Dictionary_2_ToTKey_m29170_gshared ();
extern "C" void Dictionary_2_ToTValue_m29172_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m29174_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m29176_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m29178_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29179_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29180_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29181_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29182_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29183_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29184_gshared ();
extern "C" void KeyValuePair_2__ctor_m29185_gshared ();
extern "C" void KeyValuePair_2_get_Key_m29186_gshared ();
extern "C" void KeyValuePair_2_set_Key_m29187_gshared ();
extern "C" void KeyValuePair_2_get_Value_m29188_gshared ();
extern "C" void KeyValuePair_2_set_Value_m29189_gshared ();
extern "C" void KeyValuePair_2_ToString_m29190_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29191_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29192_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29193_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29194_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29195_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29196_gshared ();
extern "C" void KeyCollection__ctor_m29197_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m29198_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m29199_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m29200_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m29201_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m29202_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m29203_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m29204_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m29205_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m29206_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m29207_gshared ();
extern "C" void KeyCollection_CopyTo_m29208_gshared ();
extern "C" void KeyCollection_GetEnumerator_m29209_gshared ();
extern "C" void KeyCollection_get_Count_m29210_gshared ();
extern "C" void Enumerator__ctor_m29211_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m29212_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m29213_gshared ();
extern "C" void Enumerator_Dispose_m29214_gshared ();
extern "C" void Enumerator_MoveNext_m29215_gshared ();
extern "C" void Enumerator_get_Current_m29216_gshared ();
extern "C" void Enumerator__ctor_m29217_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m29218_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m29219_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m29220_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m29221_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29222_gshared ();
extern "C" void Enumerator_MoveNext_m29223_gshared ();
extern "C" void Enumerator_get_Current_m29224_gshared ();
extern "C" void Enumerator_get_CurrentKey_m29225_gshared ();
extern "C" void Enumerator_get_CurrentValue_m29226_gshared ();
extern "C" void Enumerator_Reset_m29227_gshared ();
extern "C" void Enumerator_VerifyState_m29228_gshared ();
extern "C" void Enumerator_VerifyCurrent_m29229_gshared ();
extern "C" void Enumerator_Dispose_m29230_gshared ();
extern "C" void Transform_1__ctor_m29231_gshared ();
extern "C" void Transform_1_Invoke_m29232_gshared ();
extern "C" void Transform_1_BeginInvoke_m29233_gshared ();
extern "C" void Transform_1_EndInvoke_m29234_gshared ();
extern "C" void ValueCollection__ctor_m29235_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m29236_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m29237_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m29238_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m29239_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m29240_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m29241_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m29242_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m29243_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m29244_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m29245_gshared ();
extern "C" void ValueCollection_CopyTo_m29246_gshared ();
extern "C" void ValueCollection_GetEnumerator_m29247_gshared ();
extern "C" void ValueCollection_get_Count_m29248_gshared ();
extern "C" void Enumerator__ctor_m29249_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m29250_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m29251_gshared ();
extern "C" void Enumerator_Dispose_m29252_gshared ();
extern "C" void Enumerator_MoveNext_m29253_gshared ();
extern "C" void Enumerator_get_Current_m29254_gshared ();
extern "C" void Transform_1__ctor_m29255_gshared ();
extern "C" void Transform_1_Invoke_m29256_gshared ();
extern "C" void Transform_1_BeginInvoke_m29257_gshared ();
extern "C" void Transform_1_EndInvoke_m29258_gshared ();
extern "C" void Transform_1__ctor_m29259_gshared ();
extern "C" void Transform_1_Invoke_m29260_gshared ();
extern "C" void Transform_1_BeginInvoke_m29261_gshared ();
extern "C" void Transform_1_EndInvoke_m29262_gshared ();
extern "C" void Transform_1__ctor_m29263_gshared ();
extern "C" void Transform_1_Invoke_m29264_gshared ();
extern "C" void Transform_1_BeginInvoke_m29265_gshared ();
extern "C" void Transform_1_EndInvoke_m29266_gshared ();
extern "C" void ShimEnumerator__ctor_m29267_gshared ();
extern "C" void ShimEnumerator_MoveNext_m29268_gshared ();
extern "C" void ShimEnumerator_get_Entry_m29269_gshared ();
extern "C" void ShimEnumerator_get_Key_m29270_gshared ();
extern "C" void ShimEnumerator_get_Value_m29271_gshared ();
extern "C" void ShimEnumerator_get_Current_m29272_gshared ();
extern "C" void ShimEnumerator_Reset_m29273_gshared ();
extern "C" void EqualityComparer_1__ctor_m29274_gshared ();
extern "C" void EqualityComparer_1__cctor_m29275_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29276_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29277_gshared ();
extern "C" void EqualityComparer_1_get_Default_m29278_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m29279_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m29280_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m29281_gshared ();
extern "C" void DefaultComparer__ctor_m29282_gshared ();
extern "C" void DefaultComparer_GetHashCode_m29283_gshared ();
extern "C" void DefaultComparer_Equals_m29284_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29343_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29344_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29345_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29346_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29347_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29348_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29361_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29362_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29363_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29364_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29365_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29366_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29367_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29368_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29369_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29370_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29371_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29372_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29373_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29374_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29375_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29376_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29377_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29378_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29391_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29392_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29393_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29394_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29395_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29396_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29407_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29408_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29409_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29410_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29411_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29412_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29413_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29414_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29415_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29416_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29417_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29418_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29419_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29420_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29421_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29422_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29423_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29424_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29425_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29426_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29428_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29429_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29430_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29469_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29470_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29471_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29472_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29473_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29474_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29504_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29505_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29506_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29507_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29508_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29509_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29510_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29511_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29512_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29513_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29514_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29515_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29546_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29547_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29548_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29549_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29550_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29551_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29552_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29553_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29554_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29555_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29556_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29557_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29558_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29559_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29560_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29561_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29562_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29563_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29600_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29601_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29602_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29603_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29604_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29605_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29606_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29607_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29608_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29609_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29610_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29611_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m29612_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29613_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m29614_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m29615_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m29616_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m29617_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m29618_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m29619_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29620_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m29621_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m29622_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m29623_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m29624_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m29625_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m29626_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m29627_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m29628_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m29629_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m29630_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m29631_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m29632_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m29633_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m29634_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m29635_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m29636_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m29637_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m29638_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m29639_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m29640_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m29641_gshared ();
extern "C" void Collection_1__ctor_m29642_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29643_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m29644_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m29645_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m29646_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m29647_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m29648_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m29649_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m29650_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m29651_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m29652_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m29653_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m29654_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m29655_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m29656_gshared ();
extern "C" void Collection_1_Add_m29657_gshared ();
extern "C" void Collection_1_Clear_m29658_gshared ();
extern "C" void Collection_1_ClearItems_m29659_gshared ();
extern "C" void Collection_1_Contains_m29660_gshared ();
extern "C" void Collection_1_CopyTo_m29661_gshared ();
extern "C" void Collection_1_GetEnumerator_m29662_gshared ();
extern "C" void Collection_1_IndexOf_m29663_gshared ();
extern "C" void Collection_1_Insert_m29664_gshared ();
extern "C" void Collection_1_InsertItem_m29665_gshared ();
extern "C" void Collection_1_Remove_m29666_gshared ();
extern "C" void Collection_1_RemoveAt_m29667_gshared ();
extern "C" void Collection_1_RemoveItem_m29668_gshared ();
extern "C" void Collection_1_get_Count_m29669_gshared ();
extern "C" void Collection_1_get_Item_m29670_gshared ();
extern "C" void Collection_1_set_Item_m29671_gshared ();
extern "C" void Collection_1_SetItem_m29672_gshared ();
extern "C" void Collection_1_IsValidItem_m29673_gshared ();
extern "C" void Collection_1_ConvertItem_m29674_gshared ();
extern "C" void Collection_1_CheckWritable_m29675_gshared ();
extern "C" void Collection_1_IsSynchronized_m29676_gshared ();
extern "C" void Collection_1_IsFixedSize_m29677_gshared ();
extern "C" void List_1__ctor_m29678_gshared ();
extern "C" void List_1__ctor_m29679_gshared ();
extern "C" void List_1__ctor_m29680_gshared ();
extern "C" void List_1__cctor_m29681_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29682_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m29683_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m29684_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m29685_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m29686_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m29687_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m29688_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m29689_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29690_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m29691_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m29692_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m29693_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m29694_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m29695_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m29696_gshared ();
extern "C" void List_1_Add_m29697_gshared ();
extern "C" void List_1_GrowIfNeeded_m29698_gshared ();
extern "C" void List_1_AddCollection_m29699_gshared ();
extern "C" void List_1_AddEnumerable_m29700_gshared ();
extern "C" void List_1_AddRange_m29701_gshared ();
extern "C" void List_1_AsReadOnly_m29702_gshared ();
extern "C" void List_1_Clear_m29703_gshared ();
extern "C" void List_1_Contains_m29704_gshared ();
extern "C" void List_1_CopyTo_m29705_gshared ();
extern "C" void List_1_Find_m29706_gshared ();
extern "C" void List_1_CheckMatch_m29707_gshared ();
extern "C" void List_1_GetIndex_m29708_gshared ();
extern "C" void List_1_GetEnumerator_m29709_gshared ();
extern "C" void List_1_IndexOf_m29710_gshared ();
extern "C" void List_1_Shift_m29711_gshared ();
extern "C" void List_1_CheckIndex_m29712_gshared ();
extern "C" void List_1_Insert_m29713_gshared ();
extern "C" void List_1_CheckCollection_m29714_gshared ();
extern "C" void List_1_Remove_m29715_gshared ();
extern "C" void List_1_RemoveAll_m29716_gshared ();
extern "C" void List_1_RemoveAt_m29717_gshared ();
extern "C" void List_1_Reverse_m29718_gshared ();
extern "C" void List_1_Sort_m29719_gshared ();
extern "C" void List_1_Sort_m29720_gshared ();
extern "C" void List_1_ToArray_m29721_gshared ();
extern "C" void List_1_TrimExcess_m29722_gshared ();
extern "C" void List_1_get_Capacity_m29723_gshared ();
extern "C" void List_1_set_Capacity_m29724_gshared ();
extern "C" void List_1_get_Count_m29725_gshared ();
extern "C" void List_1_get_Item_m29726_gshared ();
extern "C" void List_1_set_Item_m29727_gshared ();
extern "C" void Enumerator__ctor_m29728_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m29729_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m29730_gshared ();
extern "C" void Enumerator_Dispose_m29731_gshared ();
extern "C" void Enumerator_VerifyState_m29732_gshared ();
extern "C" void Enumerator_MoveNext_m29733_gshared ();
extern "C" void Enumerator_get_Current_m29734_gshared ();
extern "C" void EqualityComparer_1__ctor_m29735_gshared ();
extern "C" void EqualityComparer_1__cctor_m29736_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29737_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29738_gshared ();
extern "C" void EqualityComparer_1_get_Default_m29739_gshared ();
extern "C" void DefaultComparer__ctor_m29740_gshared ();
extern "C" void DefaultComparer_GetHashCode_m29741_gshared ();
extern "C" void DefaultComparer_Equals_m29742_gshared ();
extern "C" void Predicate_1__ctor_m29743_gshared ();
extern "C" void Predicate_1_Invoke_m29744_gshared ();
extern "C" void Predicate_1_BeginInvoke_m29745_gshared ();
extern "C" void Predicate_1_EndInvoke_m29746_gshared ();
extern "C" void Comparer_1__ctor_m29747_gshared ();
extern "C" void Comparer_1__cctor_m29748_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m29749_gshared ();
extern "C" void Comparer_1_get_Default_m29750_gshared ();
extern "C" void DefaultComparer__ctor_m29751_gshared ();
extern "C" void DefaultComparer_Compare_m29752_gshared ();
extern "C" void Comparison_1__ctor_m29753_gshared ();
extern "C" void Comparison_1_Invoke_m29754_gshared ();
extern "C" void Comparison_1_BeginInvoke_m29755_gshared ();
extern "C" void Comparison_1_EndInvoke_m29756_gshared ();
extern "C" void ArrayReadOnlyList_1__ctor_m29757_gshared ();
extern "C" void ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29758_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Item_m29759_gshared ();
extern "C" void ArrayReadOnlyList_1_set_Item_m29760_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Count_m29761_gshared ();
extern "C" void ArrayReadOnlyList_1_get_IsReadOnly_m29762_gshared ();
extern "C" void ArrayReadOnlyList_1_Add_m29763_gshared ();
extern "C" void ArrayReadOnlyList_1_Clear_m29764_gshared ();
extern "C" void ArrayReadOnlyList_1_Contains_m29765_gshared ();
extern "C" void ArrayReadOnlyList_1_CopyTo_m29766_gshared ();
extern "C" void ArrayReadOnlyList_1_GetEnumerator_m29767_gshared ();
extern "C" void ArrayReadOnlyList_1_IndexOf_m29768_gshared ();
extern "C" void ArrayReadOnlyList_1_Insert_m29769_gshared ();
extern "C" void ArrayReadOnlyList_1_Remove_m29770_gshared ();
extern "C" void ArrayReadOnlyList_1_RemoveAt_m29771_gshared ();
extern "C" void ArrayReadOnlyList_1_ReadOnlyError_m29772_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m29773_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29774_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29775_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29776_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29777_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m29778_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m29779_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29780_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m29781_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m29782_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m29783_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m29784_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m29785_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m29786_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29787_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m29788_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m29789_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m29790_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m29791_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m29792_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m29793_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m29794_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m29795_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m29796_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m29797_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m29798_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m29799_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m29800_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m29801_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m29802_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m29803_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m29804_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m29805_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m29806_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m29807_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m29808_gshared ();
extern "C" void Collection_1__ctor_m29809_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29810_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m29811_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m29812_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m29813_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m29814_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m29815_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m29816_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m29817_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m29818_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m29819_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m29820_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m29821_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m29822_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m29823_gshared ();
extern "C" void Collection_1_Add_m29824_gshared ();
extern "C" void Collection_1_Clear_m29825_gshared ();
extern "C" void Collection_1_ClearItems_m29826_gshared ();
extern "C" void Collection_1_Contains_m29827_gshared ();
extern "C" void Collection_1_CopyTo_m29828_gshared ();
extern "C" void Collection_1_GetEnumerator_m29829_gshared ();
extern "C" void Collection_1_IndexOf_m29830_gshared ();
extern "C" void Collection_1_Insert_m29831_gshared ();
extern "C" void Collection_1_InsertItem_m29832_gshared ();
extern "C" void Collection_1_Remove_m29833_gshared ();
extern "C" void Collection_1_RemoveAt_m29834_gshared ();
extern "C" void Collection_1_RemoveItem_m29835_gshared ();
extern "C" void Collection_1_get_Count_m29836_gshared ();
extern "C" void Collection_1_get_Item_m29837_gshared ();
extern "C" void Collection_1_set_Item_m29838_gshared ();
extern "C" void Collection_1_SetItem_m29839_gshared ();
extern "C" void Collection_1_IsValidItem_m29840_gshared ();
extern "C" void Collection_1_ConvertItem_m29841_gshared ();
extern "C" void Collection_1_CheckWritable_m29842_gshared ();
extern "C" void Collection_1_IsSynchronized_m29843_gshared ();
extern "C" void Collection_1_IsFixedSize_m29844_gshared ();
extern "C" void List_1__ctor_m29845_gshared ();
extern "C" void List_1__ctor_m29846_gshared ();
extern "C" void List_1__ctor_m29847_gshared ();
extern "C" void List_1__cctor_m29848_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29849_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m29850_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m29851_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m29852_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m29853_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m29854_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m29855_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m29856_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29857_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m29858_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m29859_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m29860_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m29861_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m29862_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m29863_gshared ();
extern "C" void List_1_Add_m29864_gshared ();
extern "C" void List_1_GrowIfNeeded_m29865_gshared ();
extern "C" void List_1_AddCollection_m29866_gshared ();
extern "C" void List_1_AddEnumerable_m29867_gshared ();
extern "C" void List_1_AddRange_m29868_gshared ();
extern "C" void List_1_AsReadOnly_m29869_gshared ();
extern "C" void List_1_Clear_m29870_gshared ();
extern "C" void List_1_Contains_m29871_gshared ();
extern "C" void List_1_CopyTo_m29872_gshared ();
extern "C" void List_1_Find_m29873_gshared ();
extern "C" void List_1_CheckMatch_m29874_gshared ();
extern "C" void List_1_GetIndex_m29875_gshared ();
extern "C" void List_1_GetEnumerator_m29876_gshared ();
extern "C" void List_1_IndexOf_m29877_gshared ();
extern "C" void List_1_Shift_m29878_gshared ();
extern "C" void List_1_CheckIndex_m29879_gshared ();
extern "C" void List_1_Insert_m29880_gshared ();
extern "C" void List_1_CheckCollection_m29881_gshared ();
extern "C" void List_1_Remove_m29882_gshared ();
extern "C" void List_1_RemoveAll_m29883_gshared ();
extern "C" void List_1_RemoveAt_m29884_gshared ();
extern "C" void List_1_Reverse_m29885_gshared ();
extern "C" void List_1_Sort_m29886_gshared ();
extern "C" void List_1_Sort_m29887_gshared ();
extern "C" void List_1_ToArray_m29888_gshared ();
extern "C" void List_1_TrimExcess_m29889_gshared ();
extern "C" void List_1_get_Capacity_m29890_gshared ();
extern "C" void List_1_set_Capacity_m29891_gshared ();
extern "C" void List_1_get_Count_m29892_gshared ();
extern "C" void List_1_get_Item_m29893_gshared ();
extern "C" void List_1_set_Item_m29894_gshared ();
extern "C" void Enumerator__ctor_m29895_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m29896_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m29897_gshared ();
extern "C" void Enumerator_Dispose_m29898_gshared ();
extern "C" void Enumerator_VerifyState_m29899_gshared ();
extern "C" void Enumerator_MoveNext_m29900_gshared ();
extern "C" void Enumerator_get_Current_m29901_gshared ();
extern "C" void EqualityComparer_1__ctor_m29902_gshared ();
extern "C" void EqualityComparer_1__cctor_m29903_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29904_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29905_gshared ();
extern "C" void EqualityComparer_1_get_Default_m29906_gshared ();
extern "C" void DefaultComparer__ctor_m29907_gshared ();
extern "C" void DefaultComparer_GetHashCode_m29908_gshared ();
extern "C" void DefaultComparer_Equals_m29909_gshared ();
extern "C" void Predicate_1__ctor_m29910_gshared ();
extern "C" void Predicate_1_Invoke_m29911_gshared ();
extern "C" void Predicate_1_BeginInvoke_m29912_gshared ();
extern "C" void Predicate_1_EndInvoke_m29913_gshared ();
extern "C" void Comparer_1__ctor_m29914_gshared ();
extern "C" void Comparer_1__cctor_m29915_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m29916_gshared ();
extern "C" void Comparer_1_get_Default_m29917_gshared ();
extern "C" void DefaultComparer__ctor_m29918_gshared ();
extern "C" void DefaultComparer_Compare_m29919_gshared ();
extern "C" void Comparison_1__ctor_m29920_gshared ();
extern "C" void Comparison_1_Invoke_m29921_gshared ();
extern "C" void Comparison_1_BeginInvoke_m29922_gshared ();
extern "C" void Comparison_1_EndInvoke_m29923_gshared ();
extern "C" void ArrayReadOnlyList_1__ctor_m29924_gshared ();
extern "C" void ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29925_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Item_m29926_gshared ();
extern "C" void ArrayReadOnlyList_1_set_Item_m29927_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Count_m29928_gshared ();
extern "C" void ArrayReadOnlyList_1_get_IsReadOnly_m29929_gshared ();
extern "C" void ArrayReadOnlyList_1_Add_m29930_gshared ();
extern "C" void ArrayReadOnlyList_1_Clear_m29931_gshared ();
extern "C" void ArrayReadOnlyList_1_Contains_m29932_gshared ();
extern "C" void ArrayReadOnlyList_1_CopyTo_m29933_gshared ();
extern "C" void ArrayReadOnlyList_1_GetEnumerator_m29934_gshared ();
extern "C" void ArrayReadOnlyList_1_IndexOf_m29935_gshared ();
extern "C" void ArrayReadOnlyList_1_Insert_m29936_gshared ();
extern "C" void ArrayReadOnlyList_1_Remove_m29937_gshared ();
extern "C" void ArrayReadOnlyList_1_RemoveAt_m29938_gshared ();
extern "C" void ArrayReadOnlyList_1_ReadOnlyError_m29939_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m29940_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29941_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29942_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29943_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29944_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m29945_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29954_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29955_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29956_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29957_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29958_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29959_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29960_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29961_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29962_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29963_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29964_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29965_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29990_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29991_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29992_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29993_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m29994_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m29995_gshared ();
extern "C" void InternalEnumerator_1__ctor_m29996_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29997_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29998_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m29999_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m30000_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m30001_gshared ();
extern "C" void InternalEnumerator_1__ctor_m30002_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m30003_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30004_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m30005_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m30006_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m30007_gshared ();
extern "C" void InternalEnumerator_1__ctor_m30008_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m30009_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30010_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m30011_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m30012_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m30013_gshared ();
extern "C" void GenericComparer_1_Compare_m30114_gshared ();
extern "C" void Comparer_1__ctor_m30115_gshared ();
extern "C" void Comparer_1__cctor_m30116_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m30117_gshared ();
extern "C" void Comparer_1_get_Default_m30118_gshared ();
extern "C" void DefaultComparer__ctor_m30119_gshared ();
extern "C" void DefaultComparer_Compare_m30120_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m30121_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m30122_gshared ();
extern "C" void EqualityComparer_1__ctor_m30123_gshared ();
extern "C" void EqualityComparer_1__cctor_m30124_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m30125_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m30126_gshared ();
extern "C" void EqualityComparer_1_get_Default_m30127_gshared ();
extern "C" void DefaultComparer__ctor_m30128_gshared ();
extern "C" void DefaultComparer_GetHashCode_m30129_gshared ();
extern "C" void DefaultComparer_Equals_m30130_gshared ();
extern "C" void GenericComparer_1_Compare_m30131_gshared ();
extern "C" void Comparer_1__ctor_m30132_gshared ();
extern "C" void Comparer_1__cctor_m30133_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m30134_gshared ();
extern "C" void Comparer_1_get_Default_m30135_gshared ();
extern "C" void DefaultComparer__ctor_m30136_gshared ();
extern "C" void DefaultComparer_Compare_m30137_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m30138_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m30139_gshared ();
extern "C" void EqualityComparer_1__ctor_m30140_gshared ();
extern "C" void EqualityComparer_1__cctor_m30141_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m30142_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m30143_gshared ();
extern "C" void EqualityComparer_1_get_Default_m30144_gshared ();
extern "C" void DefaultComparer__ctor_m30145_gshared ();
extern "C" void DefaultComparer_GetHashCode_m30146_gshared ();
extern "C" void DefaultComparer_Equals_m30147_gshared ();
extern "C" void Nullable_1_Equals_m30148_gshared ();
extern "C" void Nullable_1_Equals_m30149_gshared ();
extern "C" void Nullable_1_GetHashCode_m30150_gshared ();
extern "C" void Nullable_1_GetValueOrDefault_m30151_gshared ();
extern "C" void Nullable_1_ToString_m30152_gshared ();
extern "C" void GenericComparer_1_Compare_m30153_gshared ();
extern "C" void Comparer_1__ctor_m30154_gshared ();
extern "C" void Comparer_1__cctor_m30155_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m30156_gshared ();
extern "C" void Comparer_1_get_Default_m30157_gshared ();
extern "C" void DefaultComparer__ctor_m30158_gshared ();
extern "C" void DefaultComparer_Compare_m30159_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m30160_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m30161_gshared ();
extern "C" void EqualityComparer_1__ctor_m30162_gshared ();
extern "C" void EqualityComparer_1__cctor_m30163_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m30164_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m30165_gshared ();
extern "C" void EqualityComparer_1_get_Default_m30166_gshared ();
extern "C" void DefaultComparer__ctor_m30167_gshared ();
extern "C" void DefaultComparer_GetHashCode_m30168_gshared ();
extern "C" void DefaultComparer_Equals_m30169_gshared ();
extern "C" void GenericComparer_1_Compare_m30206_gshared ();
extern "C" void Comparer_1__ctor_m30207_gshared ();
extern "C" void Comparer_1__cctor_m30208_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m30209_gshared ();
extern "C" void Comparer_1_get_Default_m30210_gshared ();
extern "C" void DefaultComparer__ctor_m30211_gshared ();
extern "C" void DefaultComparer_Compare_m30212_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m30213_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m30214_gshared ();
extern "C" void EqualityComparer_1__ctor_m30215_gshared ();
extern "C" void EqualityComparer_1__cctor_m30216_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m30217_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m30218_gshared ();
extern "C" void EqualityComparer_1_get_Default_m30219_gshared ();
extern "C" void DefaultComparer__ctor_m30220_gshared ();
extern "C" void DefaultComparer_GetHashCode_m30221_gshared ();
extern "C" void DefaultComparer_Equals_m30222_gshared ();
extern const methodPointerType g_Il2CppGenericMethodPointers[5360] = 
{
	NULL/* 0*/,
	(methodPointerType)&ComponentTracker_GetElement_TisObject_t_m2797_gshared/* 1*/,
	(methodPointerType)&ISN_Singleton_1_get_instance_m16860_gshared/* 2*/,
	(methodPointerType)&ISN_Singleton_1_get_HasInstance_m16861_gshared/* 3*/,
	(methodPointerType)&ISN_Singleton_1_get_IsDestroyed_m16862_gshared/* 4*/,
	(methodPointerType)&ISN_Singleton_1__ctor_m16858_gshared/* 5*/,
	(methodPointerType)&ISN_Singleton_1__cctor_m16859_gshared/* 6*/,
	(methodPointerType)&ISN_Singleton_1_OnDestroy_m16863_gshared/* 7*/,
	(methodPointerType)&ISN_Singleton_1_OnApplicationQuit_m16864_gshared/* 8*/,
	(methodPointerType)&SAC_Singleton_1_get_instance_m18047_gshared/* 9*/,
	(methodPointerType)&SAC_Singleton_1_get_IsDestroyed_m18048_gshared/* 10*/,
	(methodPointerType)&SAC_Singleton_1__ctor_m18045_gshared/* 11*/,
	(methodPointerType)&SAC_Singleton_1__cctor_m18046_gshared/* 12*/,
	(methodPointerType)&CallbackManager_AddFacebookDelegate_TisObject_t_m2621_gshared/* 13*/,
	(methodPointerType)&CallbackManager_TryCallCallback_TisObject_t_m2620_gshared/* 14*/,
	(methodPointerType)&CanvasUIMethodCall_1__ctor_m18261_gshared/* 15*/,
	(methodPointerType)&CanvasUIMethodCall_1_Call_m18263_gshared/* 16*/,
	(methodPointerType)&CanvasUIMethodCall_1_UI_m18265_gshared/* 17*/,
	(methodPointerType)&ComponentFactory_GetComponent_TisObject_t_m2623_gshared/* 18*/,
	(methodPointerType)&ComponentFactory_AddComponent_TisObject_t_m2631_gshared/* 19*/,
	(methodPointerType)&MethodArguments_AddList_TisObject_t_m2375_gshared/* 20*/,
	(methodPointerType)&MethodCall_1_get_MethodName_m18267_gshared/* 21*/,
	(methodPointerType)&MethodCall_1_set_MethodName_m18268_gshared/* 22*/,
	(methodPointerType)&MethodCall_1_get_Callback_m18269_gshared/* 23*/,
	(methodPointerType)&MethodCall_1_set_Callback_m18270_gshared/* 24*/,
	(methodPointerType)&MethodCall_1_get_FacebookImpl_m18271_gshared/* 25*/,
	(methodPointerType)&MethodCall_1_set_FacebookImpl_m18272_gshared/* 26*/,
	(methodPointerType)&MethodCall_1_get_Parameters_m18273_gshared/* 27*/,
	(methodPointerType)&MethodCall_1_set_Parameters_m18274_gshared/* 28*/,
	(methodPointerType)&MethodCall_1__ctor_m18266_gshared/* 29*/,
	(methodPointerType)&JavaMethodCall_1__ctor_m18448_gshared/* 30*/,
	(methodPointerType)&JavaMethodCall_1_Call_m18450_gshared/* 31*/,
	(methodPointerType)&FBJavaClass_CallStatic_TisObject_t_m30405_gshared/* 32*/,
	(methodPointerType)&AndroidJavaClass_CallStatic_TisObject_t_m30406_gshared/* 33*/,
	(methodPointerType)&IOSFacebook_AddCallback_TisObject_t_m2625_gshared/* 34*/,
	(methodPointerType)&EditorFacebook_ShowEmptyMockDialog_TisObject_t_m2627_gshared/* 35*/,
	(methodPointerType)&Utilities_TryGetValue_TisObject_t_m2622_gshared/* 36*/,
	(methodPointerType)&Utilities_GetValueOrDefault_TisObject_t_m2628_gshared/* 37*/,
	(methodPointerType)&FacebookDelegate_1__ctor_m18240_gshared/* 38*/,
	(methodPointerType)&FacebookDelegate_1_Invoke_m18241_gshared/* 39*/,
	(methodPointerType)&FacebookDelegate_1_BeginInvoke_m18242_gshared/* 40*/,
	(methodPointerType)&FacebookDelegate_1_EndInvoke_m18243_gshared/* 41*/,
	(methodPointerType)&ExecuteEvents_ValidateEventData_TisObject_t_m4393_gshared/* 42*/,
	(methodPointerType)&ExecuteEvents_Execute_TisObject_t_m4392_gshared/* 43*/,
	(methodPointerType)&ExecuteEvents_ExecuteHierarchy_TisObject_t_m4395_gshared/* 44*/,
	(methodPointerType)&ExecuteEvents_ShouldSendToComponent_TisObject_t_m30457_gshared/* 45*/,
	(methodPointerType)&ExecuteEvents_GetEventList_TisObject_t_m30454_gshared/* 46*/,
	(methodPointerType)&ExecuteEvents_CanHandleEvent_TisObject_t_m30480_gshared/* 47*/,
	(methodPointerType)&ExecuteEvents_GetEventHandler_TisObject_t_m4394_gshared/* 48*/,
	(methodPointerType)&EventFunction_1__ctor_m19631_gshared/* 49*/,
	(methodPointerType)&EventFunction_1_Invoke_m19633_gshared/* 50*/,
	(methodPointerType)&EventFunction_1_BeginInvoke_m19635_gshared/* 51*/,
	(methodPointerType)&EventFunction_1_EndInvoke_m19637_gshared/* 52*/,
	(methodPointerType)&SetPropertyUtility_SetClass_TisObject_t_m4397_gshared/* 53*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisObject_t_m4437_gshared/* 54*/,
	(methodPointerType)&IndexedSet_1_get_Count_m20600_gshared/* 55*/,
	(methodPointerType)&IndexedSet_1_get_IsReadOnly_m20602_gshared/* 56*/,
	(methodPointerType)&IndexedSet_1_get_Item_m20610_gshared/* 57*/,
	(methodPointerType)&IndexedSet_1_set_Item_m20612_gshared/* 58*/,
	(methodPointerType)&IndexedSet_1__ctor_m20584_gshared/* 59*/,
	(methodPointerType)&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20586_gshared/* 60*/,
	(methodPointerType)&IndexedSet_1_Add_m20588_gshared/* 61*/,
	(methodPointerType)&IndexedSet_1_Remove_m20590_gshared/* 62*/,
	(methodPointerType)&IndexedSet_1_GetEnumerator_m20592_gshared/* 63*/,
	(methodPointerType)&IndexedSet_1_Clear_m20594_gshared/* 64*/,
	(methodPointerType)&IndexedSet_1_Contains_m20596_gshared/* 65*/,
	(methodPointerType)&IndexedSet_1_CopyTo_m20598_gshared/* 66*/,
	(methodPointerType)&IndexedSet_1_IndexOf_m20604_gshared/* 67*/,
	(methodPointerType)&IndexedSet_1_Insert_m20606_gshared/* 68*/,
	(methodPointerType)&IndexedSet_1_RemoveAt_m20608_gshared/* 69*/,
	(methodPointerType)&IndexedSet_1_RemoveAll_m20613_gshared/* 70*/,
	(methodPointerType)&IndexedSet_1_Sort_m20614_gshared/* 71*/,
	(methodPointerType)&ObjectPool_1_get_countAll_m19735_gshared/* 72*/,
	(methodPointerType)&ObjectPool_1_set_countAll_m19737_gshared/* 73*/,
	(methodPointerType)&ObjectPool_1_get_countActive_m19739_gshared/* 74*/,
	(methodPointerType)&ObjectPool_1_get_countInactive_m19741_gshared/* 75*/,
	(methodPointerType)&ObjectPool_1__ctor_m19733_gshared/* 76*/,
	(methodPointerType)&ObjectPool_1_Get_m19743_gshared/* 77*/,
	(methodPointerType)&ObjectPool_1_Release_m19745_gshared/* 78*/,
	(methodPointerType)&NullPremiumObjectFactory_CreateReconstruction_TisObject_t_m30617_gshared/* 79*/,
	(methodPointerType)&SmartTerrainBuilderImpl_CreateReconstruction_TisObject_t_m30684_gshared/* 80*/,
	(methodPointerType)&TrackerManagerImpl_GetTracker_TisObject_t_m30780_gshared/* 81*/,
	(methodPointerType)&TrackerManagerImpl_InitTracker_TisObject_t_m30781_gshared/* 82*/,
	(methodPointerType)&TrackerManagerImpl_DeinitTracker_TisObject_t_m30782_gshared/* 83*/,
	(methodPointerType)&VuforiaAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m6594_gshared/* 84*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m30479_gshared/* 85*/,
	(methodPointerType)&InvokableCall_1__ctor_m20223_gshared/* 86*/,
	(methodPointerType)&InvokableCall_1__ctor_m20224_gshared/* 87*/,
	(methodPointerType)&InvokableCall_1_Invoke_m20225_gshared/* 88*/,
	(methodPointerType)&InvokableCall_1_Find_m20226_gshared/* 89*/,
	(methodPointerType)&InvokableCall_2__ctor_m28131_gshared/* 90*/,
	(methodPointerType)&InvokableCall_2_Invoke_m28132_gshared/* 91*/,
	(methodPointerType)&InvokableCall_2_Find_m28133_gshared/* 92*/,
	(methodPointerType)&InvokableCall_3__ctor_m28138_gshared/* 93*/,
	(methodPointerType)&InvokableCall_3_Invoke_m28139_gshared/* 94*/,
	(methodPointerType)&InvokableCall_3_Find_m28140_gshared/* 95*/,
	(methodPointerType)&InvokableCall_4__ctor_m28145_gshared/* 96*/,
	(methodPointerType)&InvokableCall_4_Invoke_m28146_gshared/* 97*/,
	(methodPointerType)&InvokableCall_4_Find_m28147_gshared/* 98*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m28152_gshared/* 99*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m28153_gshared/* 100*/,
	(methodPointerType)&UnityEvent_1__ctor_m20211_gshared/* 101*/,
	(methodPointerType)&UnityEvent_1_AddListener_m20213_gshared/* 102*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m20215_gshared/* 103*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m20217_gshared/* 104*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m20219_gshared/* 105*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m20221_gshared/* 106*/,
	(methodPointerType)&UnityEvent_1_Invoke_m20222_gshared/* 107*/,
	(methodPointerType)&UnityEvent_2__ctor_m28352_gshared/* 108*/,
	(methodPointerType)&UnityEvent_2_FindMethod_Impl_m28353_gshared/* 109*/,
	(methodPointerType)&UnityEvent_2_GetDelegate_m28354_gshared/* 110*/,
	(methodPointerType)&UnityEvent_3__ctor_m28355_gshared/* 111*/,
	(methodPointerType)&UnityEvent_3_FindMethod_Impl_m28356_gshared/* 112*/,
	(methodPointerType)&UnityEvent_3_GetDelegate_m28357_gshared/* 113*/,
	(methodPointerType)&UnityEvent_4__ctor_m28358_gshared/* 114*/,
	(methodPointerType)&UnityEvent_4_FindMethod_Impl_m28359_gshared/* 115*/,
	(methodPointerType)&UnityEvent_4_GetDelegate_m28360_gshared/* 116*/,
	(methodPointerType)&GameObject_GetComponent_TisObject_t_m436_gshared/* 117*/,
	(methodPointerType)&GameObject_GetComponentInChildren_TisObject_t_m2795_gshared/* 118*/,
	(methodPointerType)&GameObject_GetComponentsInParent_TisObject_t_m4396_gshared/* 119*/,
	(methodPointerType)&GameObject_GetComponents_TisObject_t_m439_gshared/* 120*/,
	(methodPointerType)&GameObject_GetComponents_TisObject_t_m30456_gshared/* 121*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m2633_gshared/* 122*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m30547_gshared/* 123*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m2799_gshared/* 124*/,
	(methodPointerType)&GameObject_AddComponent_TisObject_t_m438_gshared/* 125*/,
	(methodPointerType)&ScriptableObject_CreateInstance_TisObject_t_m2339_gshared/* 126*/,
	(methodPointerType)&Resources_ConvertObjects_TisObject_t_m30567_gshared/* 127*/,
	(methodPointerType)&Object_FindObjectsOfType_TisObject_t_m6592_gshared/* 128*/,
	(methodPointerType)&Object_FindObjectOfType_TisObject_t_m2796_gshared/* 129*/,
	(methodPointerType)&Component_GetComponent_TisObject_t_m437_gshared/* 130*/,
	(methodPointerType)&Component_GetComponentInChildren_TisObject_t_m6589_gshared/* 131*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m2798_gshared/* 132*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m30546_gshared/* 133*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m2632_gshared/* 134*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m4438_gshared/* 135*/,
	(methodPointerType)&Component_GetComponents_TisObject_t_m440_gshared/* 136*/,
	(methodPointerType)&Component_GetComponents_TisObject_t_m4391_gshared/* 137*/,
	(methodPointerType)&UnityAction_1__ctor_m19763_gshared/* 138*/,
	(methodPointerType)&UnityAction_1_Invoke_m19764_gshared/* 139*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m19765_gshared/* 140*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m19766_gshared/* 141*/,
	(methodPointerType)&UnityAction_2__ctor_m28134_gshared/* 142*/,
	(methodPointerType)&UnityAction_2_Invoke_m28135_gshared/* 143*/,
	(methodPointerType)&UnityAction_2_BeginInvoke_m28136_gshared/* 144*/,
	(methodPointerType)&UnityAction_2_EndInvoke_m28137_gshared/* 145*/,
	(methodPointerType)&UnityAction_3__ctor_m28141_gshared/* 146*/,
	(methodPointerType)&UnityAction_3_Invoke_m28142_gshared/* 147*/,
	(methodPointerType)&UnityAction_3_BeginInvoke_m28143_gshared/* 148*/,
	(methodPointerType)&UnityAction_3_EndInvoke_m28144_gshared/* 149*/,
	(methodPointerType)&UnityAction_4__ctor_m28148_gshared/* 150*/,
	(methodPointerType)&UnityAction_4_Invoke_m28149_gshared/* 151*/,
	(methodPointerType)&UnityAction_4_BeginInvoke_m28150_gshared/* 152*/,
	(methodPointerType)&UnityAction_4_EndInvoke_m28151_gshared/* 153*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29058_gshared/* 154*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29059_gshared/* 155*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29060_gshared/* 156*/,
	(methodPointerType)&LinkedList_1_get_Count_m29072_gshared/* 157*/,
	(methodPointerType)&LinkedList_1_get_First_m29073_gshared/* 158*/,
	(methodPointerType)&LinkedList_1__ctor_m29052_gshared/* 159*/,
	(methodPointerType)&LinkedList_1__ctor_m29053_gshared/* 160*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29054_gshared/* 161*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_CopyTo_m29055_gshared/* 162*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29056_gshared/* 163*/,
	(methodPointerType)&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29057_gshared/* 164*/,
	(methodPointerType)&LinkedList_1_VerifyReferencedNode_m29061_gshared/* 165*/,
	(methodPointerType)&LinkedList_1_AddLast_m29062_gshared/* 166*/,
	(methodPointerType)&LinkedList_1_Clear_m29063_gshared/* 167*/,
	(methodPointerType)&LinkedList_1_Contains_m29064_gshared/* 168*/,
	(methodPointerType)&LinkedList_1_CopyTo_m29065_gshared/* 169*/,
	(methodPointerType)&LinkedList_1_Find_m29066_gshared/* 170*/,
	(methodPointerType)&LinkedList_1_GetEnumerator_m29067_gshared/* 171*/,
	(methodPointerType)&LinkedList_1_GetObjectData_m29068_gshared/* 172*/,
	(methodPointerType)&LinkedList_1_OnDeserialization_m29069_gshared/* 173*/,
	(methodPointerType)&LinkedList_1_Remove_m29070_gshared/* 174*/,
	(methodPointerType)&LinkedList_1_Remove_m29071_gshared/* 175*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m29081_gshared/* 176*/,
	(methodPointerType)&Enumerator_get_Current_m29083_gshared/* 177*/,
	(methodPointerType)&Enumerator__ctor_m29080_gshared/* 178*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m29082_gshared/* 179*/,
	(methodPointerType)&Enumerator_MoveNext_m29084_gshared/* 180*/,
	(methodPointerType)&Enumerator_Dispose_m29085_gshared/* 181*/,
	(methodPointerType)&LinkedListNode_1_get_List_m29077_gshared/* 182*/,
	(methodPointerType)&LinkedListNode_1_get_Next_m29078_gshared/* 183*/,
	(methodPointerType)&LinkedListNode_1_get_Value_m29079_gshared/* 184*/,
	(methodPointerType)&LinkedListNode_1__ctor_m29074_gshared/* 185*/,
	(methodPointerType)&LinkedListNode_1__ctor_m29075_gshared/* 186*/,
	(methodPointerType)&LinkedListNode_1_Detach_m29076_gshared/* 187*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_IsSynchronized_m19747_gshared/* 188*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_SyncRoot_m19748_gshared/* 189*/,
	(methodPointerType)&Stack_1_get_Count_m19755_gshared/* 190*/,
	(methodPointerType)&Stack_1__ctor_m19746_gshared/* 191*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_CopyTo_m19749_gshared/* 192*/,
	(methodPointerType)&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19750_gshared/* 193*/,
	(methodPointerType)&Stack_1_System_Collections_IEnumerable_GetEnumerator_m19751_gshared/* 194*/,
	(methodPointerType)&Stack_1_Peek_m19752_gshared/* 195*/,
	(methodPointerType)&Stack_1_Pop_m19753_gshared/* 196*/,
	(methodPointerType)&Stack_1_Push_m19754_gshared/* 197*/,
	(methodPointerType)&Stack_1_GetEnumerator_m19756_gshared/* 198*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19759_gshared/* 199*/,
	(methodPointerType)&Enumerator_get_Current_m19762_gshared/* 200*/,
	(methodPointerType)&Enumerator__ctor_m19757_gshared/* 201*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m19758_gshared/* 202*/,
	(methodPointerType)&Enumerator_Dispose_m19760_gshared/* 203*/,
	(methodPointerType)&Enumerator_MoveNext_m19761_gshared/* 204*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27257_gshared/* 205*/,
	(methodPointerType)&HashSet_1_get_Count_m27265_gshared/* 206*/,
	(methodPointerType)&HashSet_1__ctor_m27251_gshared/* 207*/,
	(methodPointerType)&HashSet_1__ctor_m27253_gshared/* 208*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27255_gshared/* 209*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m27259_gshared/* 210*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27261_gshared/* 211*/,
	(methodPointerType)&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m27263_gshared/* 212*/,
	(methodPointerType)&HashSet_1_Init_m27267_gshared/* 213*/,
	(methodPointerType)&HashSet_1_InitArrays_m27269_gshared/* 214*/,
	(methodPointerType)&HashSet_1_SlotsContainsAt_m27271_gshared/* 215*/,
	(methodPointerType)&HashSet_1_CopyTo_m27273_gshared/* 216*/,
	(methodPointerType)&HashSet_1_CopyTo_m27275_gshared/* 217*/,
	(methodPointerType)&HashSet_1_Resize_m27277_gshared/* 218*/,
	(methodPointerType)&HashSet_1_GetLinkHashCode_m27279_gshared/* 219*/,
	(methodPointerType)&HashSet_1_GetItemHashCode_m27281_gshared/* 220*/,
	(methodPointerType)&HashSet_1_Add_m27282_gshared/* 221*/,
	(methodPointerType)&HashSet_1_Clear_m27284_gshared/* 222*/,
	(methodPointerType)&HashSet_1_Contains_m27286_gshared/* 223*/,
	(methodPointerType)&HashSet_1_Remove_m27288_gshared/* 224*/,
	(methodPointerType)&HashSet_1_GetObjectData_m27290_gshared/* 225*/,
	(methodPointerType)&HashSet_1_OnDeserialization_m27292_gshared/* 226*/,
	(methodPointerType)&HashSet_1_GetEnumerator_m27293_gshared/* 227*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27301_gshared/* 228*/,
	(methodPointerType)&Enumerator_get_Current_m27304_gshared/* 229*/,
	(methodPointerType)&Enumerator__ctor_m27300_gshared/* 230*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m27302_gshared/* 231*/,
	(methodPointerType)&Enumerator_MoveNext_m27303_gshared/* 232*/,
	(methodPointerType)&Enumerator_Dispose_m27305_gshared/* 233*/,
	(methodPointerType)&Enumerator_CheckState_m27306_gshared/* 234*/,
	(methodPointerType)&PrimeHelper__cctor_m27307_gshared/* 235*/,
	(methodPointerType)&PrimeHelper_TestPrime_m27308_gshared/* 236*/,
	(methodPointerType)&PrimeHelper_CalcPrime_m27309_gshared/* 237*/,
	(methodPointerType)&PrimeHelper_ToPrime_m27310_gshared/* 238*/,
	(methodPointerType)&Enumerable_Any_TisObject_t_m2427_gshared/* 239*/,
	(methodPointerType)&Enumerable_Cast_TisObject_t_m6596_gshared/* 240*/,
	(methodPointerType)&Enumerable_CreateCastIterator_TisObject_t_m30616_gshared/* 241*/,
	(methodPointerType)&Enumerable_Contains_TisObject_t_m6591_gshared/* 242*/,
	(methodPointerType)&Enumerable_Contains_TisObject_t_m30557_gshared/* 243*/,
	(methodPointerType)&Enumerable_Count_TisObject_t_m2626_gshared/* 244*/,
	(methodPointerType)&Enumerable_First_TisObject_t_m2428_gshared/* 245*/,
	(methodPointerType)&Enumerable_Select_TisObject_t_TisObject_t_m2629_gshared/* 246*/,
	(methodPointerType)&Enumerable_CreateSelectIterator_TisObject_t_TisObject_t_m30409_gshared/* 247*/,
	(methodPointerType)&Enumerable_ToArray_TisObject_t_m2619_gshared/* 248*/,
	(methodPointerType)&Enumerable_ToList_TisObject_t_m2630_gshared/* 249*/,
	(methodPointerType)&Enumerable_Where_TisObject_t_m4398_gshared/* 250*/,
	(methodPointerType)&Enumerable_CreateWhereIterator_TisObject_t_m30545_gshared/* 251*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m23546_gshared/* 252*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m23547_gshared/* 253*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m23545_gshared/* 254*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m23548_gshared/* 255*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m23549_gshared/* 256*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m23550_gshared/* 257*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m23551_gshared/* 258*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m23552_gshared/* 259*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m18505_gshared/* 260*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m18506_gshared/* 261*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m18504_gshared/* 262*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m18507_gshared/* 263*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m18508_gshared/* 264*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m18509_gshared/* 265*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m18510_gshared/* 266*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m18511_gshared/* 267*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m21974_gshared/* 268*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m21975_gshared/* 269*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m21973_gshared/* 270*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m21976_gshared/* 271*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m21977_gshared/* 272*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m21978_gshared/* 273*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m21979_gshared/* 274*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m21980_gshared/* 275*/,
	(methodPointerType)&Action_2__ctor_m29397_gshared/* 276*/,
	(methodPointerType)&Action_2_Invoke_m29398_gshared/* 277*/,
	(methodPointerType)&Action_2_BeginInvoke_m29399_gshared/* 278*/,
	(methodPointerType)&Action_2_EndInvoke_m29400_gshared/* 279*/,
	(methodPointerType)&Func_2__ctor_m18497_gshared/* 280*/,
	(methodPointerType)&Func_2_Invoke_m18499_gshared/* 281*/,
	(methodPointerType)&Func_2_BeginInvoke_m18501_gshared/* 282*/,
	(methodPointerType)&Func_2_EndInvoke_m18503_gshared/* 283*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m30231_gshared/* 284*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisObject_t_m30224_gshared/* 285*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisObject_t_m30227_gshared/* 286*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisObject_t_m30225_gshared/* 287*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisObject_t_m30226_gshared/* 288*/,
	(methodPointerType)&Array_InternalArray__Insert_TisObject_t_m30229_gshared/* 289*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisObject_t_m30228_gshared/* 290*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisObject_t_m30223_gshared/* 291*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisObject_t_m30230_gshared/* 292*/,
	(methodPointerType)&Array_get_swapper_TisObject_t_m30236_gshared/* 293*/,
	(methodPointerType)&Array_Sort_TisObject_t_m31157_gshared/* 294*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m31158_gshared/* 295*/,
	(methodPointerType)&Array_Sort_TisObject_t_m31159_gshared/* 296*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m31160_gshared/* 297*/,
	(methodPointerType)&Array_Sort_TisObject_t_m15427_gshared/* 298*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m31161_gshared/* 299*/,
	(methodPointerType)&Array_Sort_TisObject_t_m30234_gshared/* 300*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m30235_gshared/* 301*/,
	(methodPointerType)&Array_Sort_TisObject_t_m31162_gshared/* 302*/,
	(methodPointerType)&Array_Sort_TisObject_t_m30267_gshared/* 303*/,
	(methodPointerType)&Array_qsort_TisObject_t_TisObject_t_m30264_gshared/* 304*/,
	(methodPointerType)&Array_compare_TisObject_t_m30265_gshared/* 305*/,
	(methodPointerType)&Array_qsort_TisObject_t_m30268_gshared/* 306*/,
	(methodPointerType)&Array_swap_TisObject_t_TisObject_t_m30266_gshared/* 307*/,
	(methodPointerType)&Array_swap_TisObject_t_m30269_gshared/* 308*/,
	(methodPointerType)&Array_Resize_TisObject_t_m30232_gshared/* 309*/,
	(methodPointerType)&Array_Resize_TisObject_t_m30233_gshared/* 310*/,
	(methodPointerType)&Array_TrueForAll_TisObject_t_m31163_gshared/* 311*/,
	(methodPointerType)&Array_ForEach_TisObject_t_m31164_gshared/* 312*/,
	(methodPointerType)&Array_ConvertAll_TisObject_t_TisObject_t_m31165_gshared/* 313*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m31166_gshared/* 314*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m31168_gshared/* 315*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m31167_gshared/* 316*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m31169_gshared/* 317*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m31171_gshared/* 318*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m31170_gshared/* 319*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m31172_gshared/* 320*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m31174_gshared/* 321*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m31175_gshared/* 322*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m31173_gshared/* 323*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m15433_gshared/* 324*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m31176_gshared/* 325*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m15426_gshared/* 326*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m31177_gshared/* 327*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m31178_gshared/* 328*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m31179_gshared/* 329*/,
	(methodPointerType)&Array_FindAll_TisObject_t_m31180_gshared/* 330*/,
	(methodPointerType)&Array_Exists_TisObject_t_m31181_gshared/* 331*/,
	(methodPointerType)&Array_AsReadOnly_TisObject_t_m15447_gshared/* 332*/,
	(methodPointerType)&Array_Find_TisObject_t_m31182_gshared/* 333*/,
	(methodPointerType)&Array_FindLast_TisObject_t_m31183_gshared/* 334*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15546_gshared/* 335*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15549_gshared/* 336*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15544_gshared/* 337*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15545_gshared/* 338*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15547_gshared/* 339*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15548_gshared/* 340*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Item_m29437_gshared/* 341*/,
	(methodPointerType)&ArrayReadOnlyList_1_set_Item_m29438_gshared/* 342*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Count_m29439_gshared/* 343*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_IsReadOnly_m29440_gshared/* 344*/,
	(methodPointerType)&ArrayReadOnlyList_1__ctor_m29435_gshared/* 345*/,
	(methodPointerType)&ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29436_gshared/* 346*/,
	(methodPointerType)&ArrayReadOnlyList_1_Add_m29441_gshared/* 347*/,
	(methodPointerType)&ArrayReadOnlyList_1_Clear_m29442_gshared/* 348*/,
	(methodPointerType)&ArrayReadOnlyList_1_Contains_m29443_gshared/* 349*/,
	(methodPointerType)&ArrayReadOnlyList_1_CopyTo_m29444_gshared/* 350*/,
	(methodPointerType)&ArrayReadOnlyList_1_GetEnumerator_m29445_gshared/* 351*/,
	(methodPointerType)&ArrayReadOnlyList_1_IndexOf_m29446_gshared/* 352*/,
	(methodPointerType)&ArrayReadOnlyList_1_Insert_m29447_gshared/* 353*/,
	(methodPointerType)&ArrayReadOnlyList_1_Remove_m29448_gshared/* 354*/,
	(methodPointerType)&ArrayReadOnlyList_1_RemoveAt_m29449_gshared/* 355*/,
	(methodPointerType)&ArrayReadOnlyList_1_ReadOnlyError_m29450_gshared/* 356*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29452_gshared/* 357*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29453_gshared/* 358*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0__ctor_m29451_gshared/* 359*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29454_gshared/* 360*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29455_gshared/* 361*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Reset_m29456_gshared/* 362*/,
	(methodPointerType)&Comparer_1_get_Default_m15642_gshared/* 363*/,
	(methodPointerType)&Comparer_1__ctor_m15639_gshared/* 364*/,
	(methodPointerType)&Comparer_1__cctor_m15640_gshared/* 365*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15641_gshared/* 366*/,
	(methodPointerType)&DefaultComparer__ctor_m15643_gshared/* 367*/,
	(methodPointerType)&DefaultComparer_Compare_m15644_gshared/* 368*/,
	(methodPointerType)&GenericComparer_1__ctor_m29499_gshared/* 369*/,
	(methodPointerType)&GenericComparer_1_Compare_m29500_gshared/* 370*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m16028_gshared/* 371*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16030_gshared/* 372*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16032_gshared/* 373*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16040_gshared/* 374*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16042_gshared/* 375*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16044_gshared/* 376*/,
	(methodPointerType)&Dictionary_2_get_Count_m16062_gshared/* 377*/,
	(methodPointerType)&Dictionary_2_get_Item_m16064_gshared/* 378*/,
	(methodPointerType)&Dictionary_2_set_Item_m16066_gshared/* 379*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16100_gshared/* 380*/,
	(methodPointerType)&Dictionary_2_get_Values_m16102_gshared/* 381*/,
	(methodPointerType)&Dictionary_2__ctor_m16020_gshared/* 382*/,
	(methodPointerType)&Dictionary_2__ctor_m16022_gshared/* 383*/,
	(methodPointerType)&Dictionary_2__ctor_m16024_gshared/* 384*/,
	(methodPointerType)&Dictionary_2__ctor_m16026_gshared/* 385*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16034_gshared/* 386*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16036_gshared/* 387*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16038_gshared/* 388*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16046_gshared/* 389*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16048_gshared/* 390*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16050_gshared/* 391*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16052_gshared/* 392*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16054_gshared/* 393*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16056_gshared/* 394*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16058_gshared/* 395*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16060_gshared/* 396*/,
	(methodPointerType)&Dictionary_2_Init_m16068_gshared/* 397*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16070_gshared/* 398*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16072_gshared/* 399*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30361_gshared/* 400*/,
	(methodPointerType)&Dictionary_2_make_pair_m16074_gshared/* 401*/,
	(methodPointerType)&Dictionary_2_pick_key_m16076_gshared/* 402*/,
	(methodPointerType)&Dictionary_2_pick_value_m16078_gshared/* 403*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16080_gshared/* 404*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30360_gshared/* 405*/,
	(methodPointerType)&Dictionary_2_Resize_m16082_gshared/* 406*/,
	(methodPointerType)&Dictionary_2_Add_m16084_gshared/* 407*/,
	(methodPointerType)&Dictionary_2_Clear_m16086_gshared/* 408*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16088_gshared/* 409*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16090_gshared/* 410*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16092_gshared/* 411*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16094_gshared/* 412*/,
	(methodPointerType)&Dictionary_2_Remove_m16096_gshared/* 413*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16098_gshared/* 414*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16104_gshared/* 415*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16106_gshared/* 416*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16108_gshared/* 417*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16110_gshared/* 418*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16112_gshared/* 419*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16193_gshared/* 420*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16194_gshared/* 421*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16195_gshared/* 422*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16196_gshared/* 423*/,
	(methodPointerType)&ShimEnumerator__ctor_m16191_gshared/* 424*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16192_gshared/* 425*/,
	(methodPointerType)&ShimEnumerator_Reset_m16197_gshared/* 426*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16146_gshared/* 427*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16148_gshared/* 428*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16149_gshared/* 429*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16150_gshared/* 430*/,
	(methodPointerType)&Enumerator_get_Current_m16152_gshared/* 431*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16153_gshared/* 432*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16154_gshared/* 433*/,
	(methodPointerType)&Enumerator__ctor_m16145_gshared/* 434*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m16147_gshared/* 435*/,
	(methodPointerType)&Enumerator_MoveNext_m16151_gshared/* 436*/,
	(methodPointerType)&Enumerator_Reset_m16155_gshared/* 437*/,
	(methodPointerType)&Enumerator_VerifyState_m16156_gshared/* 438*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16157_gshared/* 439*/,
	(methodPointerType)&Enumerator_Dispose_m16158_gshared/* 440*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16133_gshared/* 441*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16134_gshared/* 442*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16135_gshared/* 443*/,
	(methodPointerType)&KeyCollection_get_Count_m16138_gshared/* 444*/,
	(methodPointerType)&KeyCollection__ctor_m16125_gshared/* 445*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16126_gshared/* 446*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16127_gshared/* 447*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16128_gshared/* 448*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16129_gshared/* 449*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16130_gshared/* 450*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16131_gshared/* 451*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16132_gshared/* 452*/,
	(methodPointerType)&KeyCollection_CopyTo_m16136_gshared/* 453*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16137_gshared/* 454*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16140_gshared/* 455*/,
	(methodPointerType)&Enumerator_get_Current_m16144_gshared/* 456*/,
	(methodPointerType)&Enumerator__ctor_m16139_gshared/* 457*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m16141_gshared/* 458*/,
	(methodPointerType)&Enumerator_Dispose_m16142_gshared/* 459*/,
	(methodPointerType)&Enumerator_MoveNext_m16143_gshared/* 460*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16171_gshared/* 461*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16172_gshared/* 462*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16173_gshared/* 463*/,
	(methodPointerType)&ValueCollection_get_Count_m16176_gshared/* 464*/,
	(methodPointerType)&ValueCollection__ctor_m16163_gshared/* 465*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared/* 466*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16165_gshared/* 467*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16166_gshared/* 468*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16167_gshared/* 469*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16168_gshared/* 470*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16169_gshared/* 471*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16170_gshared/* 472*/,
	(methodPointerType)&ValueCollection_CopyTo_m16174_gshared/* 473*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16175_gshared/* 474*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16178_gshared/* 475*/,
	(methodPointerType)&Enumerator_get_Current_m16182_gshared/* 476*/,
	(methodPointerType)&Enumerator__ctor_m16177_gshared/* 477*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m16179_gshared/* 478*/,
	(methodPointerType)&Enumerator_Dispose_m16180_gshared/* 479*/,
	(methodPointerType)&Enumerator_MoveNext_m16181_gshared/* 480*/,
	(methodPointerType)&Transform_1__ctor_m16159_gshared/* 481*/,
	(methodPointerType)&Transform_1_Invoke_m16160_gshared/* 482*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16161_gshared/* 483*/,
	(methodPointerType)&Transform_1_EndInvoke_m16162_gshared/* 484*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15625_gshared/* 485*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15621_gshared/* 486*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15622_gshared/* 487*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15623_gshared/* 488*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15624_gshared/* 489*/,
	(methodPointerType)&DefaultComparer__ctor_m15632_gshared/* 490*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15633_gshared/* 491*/,
	(methodPointerType)&DefaultComparer_Equals_m15634_gshared/* 492*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m29501_gshared/* 493*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m29502_gshared/* 494*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m29503_gshared/* 495*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16120_gshared/* 496*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16121_gshared/* 497*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16122_gshared/* 498*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16123_gshared/* 499*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16119_gshared/* 500*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16124_gshared/* 501*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15471_gshared/* 502*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m15473_gshared/* 503*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m15475_gshared/* 504*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m15477_gshared/* 505*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m15479_gshared/* 506*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m15481_gshared/* 507*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m15483_gshared/* 508*/,
	(methodPointerType)&List_1_get_Capacity_m15535_gshared/* 509*/,
	(methodPointerType)&List_1_set_Capacity_m15537_gshared/* 510*/,
	(methodPointerType)&List_1_get_Count_m15539_gshared/* 511*/,
	(methodPointerType)&List_1_get_Item_m15541_gshared/* 512*/,
	(methodPointerType)&List_1_set_Item_m15543_gshared/* 513*/,
	(methodPointerType)&List_1__ctor_m2529_gshared/* 514*/,
	(methodPointerType)&List_1__ctor_m15449_gshared/* 515*/,
	(methodPointerType)&List_1__ctor_m15451_gshared/* 516*/,
	(methodPointerType)&List_1__cctor_m15453_gshared/* 517*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15455_gshared/* 518*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m15457_gshared/* 519*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m15459_gshared/* 520*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m15461_gshared/* 521*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m15463_gshared/* 522*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m15465_gshared/* 523*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m15467_gshared/* 524*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m15469_gshared/* 525*/,
	(methodPointerType)&List_1_Add_m15485_gshared/* 526*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15487_gshared/* 527*/,
	(methodPointerType)&List_1_AddCollection_m15489_gshared/* 528*/,
	(methodPointerType)&List_1_AddEnumerable_m15491_gshared/* 529*/,
	(methodPointerType)&List_1_AddRange_m15493_gshared/* 530*/,
	(methodPointerType)&List_1_AsReadOnly_m15495_gshared/* 531*/,
	(methodPointerType)&List_1_Clear_m15497_gshared/* 532*/,
	(methodPointerType)&List_1_Contains_m15499_gshared/* 533*/,
	(methodPointerType)&List_1_CopyTo_m15501_gshared/* 534*/,
	(methodPointerType)&List_1_Find_m15503_gshared/* 535*/,
	(methodPointerType)&List_1_CheckMatch_m15505_gshared/* 536*/,
	(methodPointerType)&List_1_GetIndex_m15507_gshared/* 537*/,
	(methodPointerType)&List_1_GetEnumerator_m2487_gshared/* 538*/,
	(methodPointerType)&List_1_IndexOf_m15509_gshared/* 539*/,
	(methodPointerType)&List_1_Shift_m15511_gshared/* 540*/,
	(methodPointerType)&List_1_CheckIndex_m15513_gshared/* 541*/,
	(methodPointerType)&List_1_Insert_m15515_gshared/* 542*/,
	(methodPointerType)&List_1_CheckCollection_m15517_gshared/* 543*/,
	(methodPointerType)&List_1_Remove_m15519_gshared/* 544*/,
	(methodPointerType)&List_1_RemoveAll_m15521_gshared/* 545*/,
	(methodPointerType)&List_1_RemoveAt_m15523_gshared/* 546*/,
	(methodPointerType)&List_1_Reverse_m15525_gshared/* 547*/,
	(methodPointerType)&List_1_Sort_m15527_gshared/* 548*/,
	(methodPointerType)&List_1_Sort_m15529_gshared/* 549*/,
	(methodPointerType)&List_1_ToArray_m15531_gshared/* 550*/,
	(methodPointerType)&List_1_TrimExcess_m15533_gshared/* 551*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15552_gshared/* 552*/,
	(methodPointerType)&Enumerator_get_Current_m2488_gshared/* 553*/,
	(methodPointerType)&Enumerator__ctor_m15550_gshared/* 554*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m15551_gshared/* 555*/,
	(methodPointerType)&Enumerator_Dispose_m15553_gshared/* 556*/,
	(methodPointerType)&Enumerator_VerifyState_m15554_gshared/* 557*/,
	(methodPointerType)&Enumerator_MoveNext_m2489_gshared/* 558*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15586_gshared/* 559*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15594_gshared/* 560*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15595_gshared/* 561*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15596_gshared/* 562*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15597_gshared/* 563*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15598_gshared/* 564*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15599_gshared/* 565*/,
	(methodPointerType)&Collection_1_get_Count_m15612_gshared/* 566*/,
	(methodPointerType)&Collection_1_get_Item_m15613_gshared/* 567*/,
	(methodPointerType)&Collection_1_set_Item_m15614_gshared/* 568*/,
	(methodPointerType)&Collection_1__ctor_m15585_gshared/* 569*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15587_gshared/* 570*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15588_gshared/* 571*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15589_gshared/* 572*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15590_gshared/* 573*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15591_gshared/* 574*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15592_gshared/* 575*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15593_gshared/* 576*/,
	(methodPointerType)&Collection_1_Add_m15600_gshared/* 577*/,
	(methodPointerType)&Collection_1_Clear_m15601_gshared/* 578*/,
	(methodPointerType)&Collection_1_ClearItems_m15602_gshared/* 579*/,
	(methodPointerType)&Collection_1_Contains_m15603_gshared/* 580*/,
	(methodPointerType)&Collection_1_CopyTo_m15604_gshared/* 581*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15605_gshared/* 582*/,
	(methodPointerType)&Collection_1_IndexOf_m15606_gshared/* 583*/,
	(methodPointerType)&Collection_1_Insert_m15607_gshared/* 584*/,
	(methodPointerType)&Collection_1_InsertItem_m15608_gshared/* 585*/,
	(methodPointerType)&Collection_1_Remove_m15609_gshared/* 586*/,
	(methodPointerType)&Collection_1_RemoveAt_m15610_gshared/* 587*/,
	(methodPointerType)&Collection_1_RemoveItem_m15611_gshared/* 588*/,
	(methodPointerType)&Collection_1_SetItem_m15615_gshared/* 589*/,
	(methodPointerType)&Collection_1_IsValidItem_m15616_gshared/* 590*/,
	(methodPointerType)&Collection_1_ConvertItem_m15617_gshared/* 591*/,
	(methodPointerType)&Collection_1_CheckWritable_m15618_gshared/* 592*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15619_gshared/* 593*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15620_gshared/* 594*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15561_gshared/* 595*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15562_gshared/* 596*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15563_gshared/* 597*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15573_gshared/* 598*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15574_gshared/* 599*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15575_gshared/* 600*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15576_gshared/* 601*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15577_gshared/* 602*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15578_gshared/* 603*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15583_gshared/* 604*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15584_gshared/* 605*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15555_gshared/* 606*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15556_gshared/* 607*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15557_gshared/* 608*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15558_gshared/* 609*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15559_gshared/* 610*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15560_gshared/* 611*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15564_gshared/* 612*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15565_gshared/* 613*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15566_gshared/* 614*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15567_gshared/* 615*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15568_gshared/* 616*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15569_gshared/* 617*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15570_gshared/* 618*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15571_gshared/* 619*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15572_gshared/* 620*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15579_gshared/* 621*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15580_gshared/* 622*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15581_gshared/* 623*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15582_gshared/* 624*/,
	(methodPointerType)&CustomAttributeData_UnboxValues_TisObject_t_m31282_gshared/* 625*/,
	(methodPointerType)&MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m31283_gshared/* 626*/,
	(methodPointerType)&MonoProperty_StaticGetterAdapterFrame_TisObject_t_m31284_gshared/* 627*/,
	(methodPointerType)&Getter_2__ctor_m29946_gshared/* 628*/,
	(methodPointerType)&Getter_2_Invoke_m29947_gshared/* 629*/,
	(methodPointerType)&Getter_2_BeginInvoke_m29948_gshared/* 630*/,
	(methodPointerType)&Getter_2_EndInvoke_m29949_gshared/* 631*/,
	(methodPointerType)&StaticGetter_1__ctor_m29950_gshared/* 632*/,
	(methodPointerType)&StaticGetter_1_Invoke_m29951_gshared/* 633*/,
	(methodPointerType)&StaticGetter_1_BeginInvoke_m29952_gshared/* 634*/,
	(methodPointerType)&StaticGetter_1_EndInvoke_m29953_gshared/* 635*/,
	(methodPointerType)&Activator_CreateInstance_TisObject_t_m30453_gshared/* 636*/,
	(methodPointerType)&Action_1__ctor_m17054_gshared/* 637*/,
	(methodPointerType)&Action_1_Invoke_m17055_gshared/* 638*/,
	(methodPointerType)&Action_1_BeginInvoke_m17057_gshared/* 639*/,
	(methodPointerType)&Action_1_EndInvoke_m17059_gshared/* 640*/,
	(methodPointerType)&Comparison_1__ctor_m15663_gshared/* 641*/,
	(methodPointerType)&Comparison_1_Invoke_m15664_gshared/* 642*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15665_gshared/* 643*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15666_gshared/* 644*/,
	(methodPointerType)&Converter_2__ctor_m29431_gshared/* 645*/,
	(methodPointerType)&Converter_2_Invoke_m29432_gshared/* 646*/,
	(methodPointerType)&Converter_2_BeginInvoke_m29433_gshared/* 647*/,
	(methodPointerType)&Converter_2_EndInvoke_m29434_gshared/* 648*/,
	(methodPointerType)&Predicate_1__ctor_m15635_gshared/* 649*/,
	(methodPointerType)&Predicate_1_Invoke_m15636_gshared/* 650*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15637_gshared/* 651*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15638_gshared/* 652*/,
	(methodPointerType)&Nullable_1_get_HasValue_m328_gshared/* 653*/,
	(methodPointerType)&Nullable_1_get_Value_m329_gshared/* 654*/,
	(methodPointerType)&Dictionary_2__ctor_m15743_gshared/* 655*/,
	(methodPointerType)&Dictionary_2__ctor_m8129_gshared/* 656*/,
	(methodPointerType)&List_1__ctor_m2216_gshared/* 657*/,
	(methodPointerType)&List_1_ToArray_m2218_gshared/* 658*/,
	(methodPointerType)&Dictionary_2__ctor_m15740_gshared/* 659*/,
	(methodPointerType)&Action_1__ctor_m2226_gshared/* 660*/,
	(methodPointerType)&Action_1_Invoke_m2235_gshared/* 661*/,
	(methodPointerType)&Action_1__ctor_m2255_gshared/* 662*/,
	(methodPointerType)&Action_1_Invoke_m2258_gshared/* 663*/,
	(methodPointerType)&Action_1__ctor_m2268_gshared/* 664*/,
	(methodPointerType)&Action_1_Invoke_m2271_gshared/* 665*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16339_gshared/* 666*/,
	(methodPointerType)&Enumerator_get_Current_m16426_gshared/* 667*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16396_gshared/* 668*/,
	(methodPointerType)&Enumerator_MoveNext_m16425_gshared/* 669*/,
	(methodPointerType)&MethodArguments_AddPrimative_TisBoolean_t41_m2368_gshared/* 670*/,
	(methodPointerType)&Nullable_1_get_HasValue_m2373_gshared/* 671*/,
	(methodPointerType)&Nullable_1_ToString_m2374_gshared/* 672*/,
	(methodPointerType)&MethodArguments_AddNullablePrimitive_TisInt32_t59_m2377_gshared/* 673*/,
	(methodPointerType)&MethodArguments_AddPrimative_TisInt32_t59_m2382_gshared/* 674*/,
	(methodPointerType)&Nullable_1__ctor_m2401_gshared/* 675*/,
	(methodPointerType)&Nullable_1_GetValueOrDefault_m2408_gshared/* 676*/,
	(methodPointerType)&MethodArguments_AddNullablePrimitive_TisOGActionType_t265_m2426_gshared/* 677*/,
	(methodPointerType)&MethodArguments_AddNullablePrimitive_TisSingle_t36_m2437_gshared/* 678*/,
	(methodPointerType)&MethodArguments_AddPrimative_TisSingle_t36_m2438_gshared/* 679*/,
	(methodPointerType)&Nullable_1_get_HasValue_m2443_gshared/* 680*/,
	(methodPointerType)&Nullable_1_get_Value_m2444_gshared/* 681*/,
	(methodPointerType)&Nullable_1_get_HasValue_m2449_gshared/* 682*/,
	(methodPointerType)&Nullable_1_get_Value_m2450_gshared/* 683*/,
	(methodPointerType)&Utilities_GetValueOrDefault_TisInt64_t507_m2504_gshared/* 684*/,
	(methodPointerType)&Nullable_1_get_HasValue_m2512_gshared/* 685*/,
	(methodPointerType)&Nullable_1_get_Value_m2513_gshared/* 686*/,
	(methodPointerType)&Dictionary_2__ctor_m18518_gshared/* 687*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m18607_gshared/* 688*/,
	(methodPointerType)&Enumerator_get_Current_m18649_gshared/* 689*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m18617_gshared/* 690*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m18619_gshared/* 691*/,
	(methodPointerType)&Enumerator_MoveNext_m18648_gshared/* 692*/,
	(methodPointerType)&Dictionary_2__ctor_m2587_gshared/* 693*/,
	(methodPointerType)&Action_2__ctor_m19399_gshared/* 694*/,
	(methodPointerType)&Action_2_Invoke_m19400_gshared/* 695*/,
	(methodPointerType)&Action_1__ctor_m2681_gshared/* 696*/,
	(methodPointerType)&Action_1_Invoke_m2682_gshared/* 697*/,
	(methodPointerType)&Action_1__ctor_m2699_gshared/* 698*/,
	(methodPointerType)&Comparison_1__ctor_m3994_gshared/* 699*/,
	(methodPointerType)&List_1_Sort_m4000_gshared/* 700*/,
	(methodPointerType)&List_1__ctor_m4033_gshared/* 701*/,
	(methodPointerType)&Dictionary_2_get_Values_m16331_gshared/* 702*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16449_gshared/* 703*/,
	(methodPointerType)&Enumerator_get_Current_m16456_gshared/* 704*/,
	(methodPointerType)&Enumerator_MoveNext_m16455_gshared/* 705*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16394_gshared/* 706*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16398_gshared/* 707*/,
	(methodPointerType)&Comparison_1__ctor_m4099_gshared/* 708*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t566_m4100_gshared/* 709*/,
	(methodPointerType)&UnityEvent_1__ctor_m4104_gshared/* 710*/,
	(methodPointerType)&UnityEvent_1_Invoke_m4105_gshared/* 711*/,
	(methodPointerType)&UnityEvent_1_AddListener_m4106_gshared/* 712*/,
	(methodPointerType)&TweenRunner_1__ctor_m4125_gshared/* 713*/,
	(methodPointerType)&TweenRunner_1_Init_m4126_gshared/* 714*/,
	(methodPointerType)&UnityAction_1__ctor_m4154_gshared/* 715*/,
	(methodPointerType)&TweenRunner_1_StartTween_m4155_gshared/* 716*/,
	(methodPointerType)&List_1_get_Capacity_m4156_gshared/* 717*/,
	(methodPointerType)&List_1_set_Capacity_m4157_gshared/* 718*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisType_t701_m4178_gshared/* 719*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisBoolean_t41_m4179_gshared/* 720*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisFillMethod_t702_m4180_gshared/* 721*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSingle_t36_m4181_gshared/* 722*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisInt32_t59_m4182_gshared/* 723*/,
	(methodPointerType)&List_1__ctor_m4218_gshared/* 724*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisContentType_t710_m4229_gshared/* 725*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisLineType_t713_m4230_gshared/* 726*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisInputType_t711_m4231_gshared/* 727*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisTouchScreenKeyboardType_t854_m4232_gshared/* 728*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisCharacterValidation_t712_m4233_gshared/* 729*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisChar_t556_m4234_gshared/* 730*/,
	(methodPointerType)&List_1_ToArray_m4278_gshared/* 731*/,
	(methodPointerType)&UnityEvent_1__ctor_m4305_gshared/* 732*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisDirection_t732_m4308_gshared/* 733*/,
	(methodPointerType)&UnityEvent_1_Invoke_m4310_gshared/* 734*/,
	(methodPointerType)&UnityEvent_1__ctor_m4315_gshared/* 735*/,
	(methodPointerType)&UnityAction_1__ctor_m4316_gshared/* 736*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m4317_gshared/* 737*/,
	(methodPointerType)&UnityEvent_1_AddListener_m4318_gshared/* 738*/,
	(methodPointerType)&UnityEvent_1_Invoke_m4323_gshared/* 739*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisNavigation_t730_m4341_gshared/* 740*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisTransition_t743_m4342_gshared/* 741*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisColorBlock_t682_m4343_gshared/* 742*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSpriteState_t745_m4344_gshared/* 743*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisDirection_t749_m4359_gshared/* 744*/,
	(methodPointerType)&UnityEvent_1__ctor_m4376_gshared/* 745*/,
	(methodPointerType)&UnityEvent_1_Invoke_m4377_gshared/* 746*/,
	(methodPointerType)&Func_2__ctor_m21966_gshared/* 747*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisAspectMode_t764_m4383_gshared/* 748*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisFitMode_t770_m4390_gshared/* 749*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisCorner_t772_m4399_gshared/* 750*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisAxis_t773_m4400_gshared/* 751*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisVector2_t29_m4401_gshared/* 752*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisConstraint_t774_m4402_gshared/* 753*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisInt32_t59_m4403_gshared/* 754*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisSingle_t36_m4408_gshared/* 755*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisBoolean_t41_m4409_gshared/* 756*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisTextAnchor_t869_m4415_gshared/* 757*/,
	(methodPointerType)&Func_2__ctor_m22077_gshared/* 758*/,
	(methodPointerType)&Func_2_Invoke_m22078_gshared/* 759*/,
	(methodPointerType)&Dictionary_2__ctor_m22689_gshared/* 760*/,
	(methodPointerType)&List_1__ctor_m6516_gshared/* 761*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16319_gshared/* 762*/,
	(methodPointerType)&Enumerator_Dispose_m16454_gshared/* 763*/,
	(methodPointerType)&LinkedList_1__ctor_m6567_gshared/* 764*/,
	(methodPointerType)&LinkedList_1_AddLast_m6572_gshared/* 765*/,
	(methodPointerType)&List_1__ctor_m6573_gshared/* 766*/,
	(methodPointerType)&List_1_GetEnumerator_m6574_gshared/* 767*/,
	(methodPointerType)&Enumerator_get_Current_m6575_gshared/* 768*/,
	(methodPointerType)&Predicate_1__ctor_m6576_gshared/* 769*/,
	(methodPointerType)&Array_Exists_TisTrackableResultData_t969_m6577_gshared/* 770*/,
	(methodPointerType)&Enumerator_MoveNext_m6578_gshared/* 771*/,
	(methodPointerType)&Enumerator_Dispose_m6579_gshared/* 772*/,
	(methodPointerType)&LinkedList_1_get_First_m6580_gshared/* 773*/,
	(methodPointerType)&LinkedListNode_1_get_Value_m6581_gshared/* 774*/,
	(methodPointerType)&Dictionary_2_get_Values_m22770_gshared/* 775*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m22849_gshared/* 776*/,
	(methodPointerType)&Enumerator_get_Current_m22856_gshared/* 777*/,
	(methodPointerType)&Enumerator_MoveNext_m22855_gshared/* 778*/,
	(methodPointerType)&Enumerator_Dispose_m22854_gshared/* 779*/,
	(methodPointerType)&Dictionary_2__ctor_m24140_gshared/* 780*/,
	(methodPointerType)&List_1__ctor_m6641_gshared/* 781*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16329_gshared/* 782*/,
	(methodPointerType)&Enumerable_ToList_TisInt32_t59_m6643_gshared/* 783*/,
	(methodPointerType)&Enumerator_Dispose_m16432_gshared/* 784*/,
	(methodPointerType)&Action_1_Invoke_m6677_gshared/* 785*/,
	(methodPointerType)&KeyCollection_CopyTo_m16410_gshared/* 786*/,
	(methodPointerType)&Enumerable_ToArray_TisInt32_t59_m6738_gshared/* 787*/,
	(methodPointerType)&LinkedListNode_1_get_Next_m6740_gshared/* 788*/,
	(methodPointerType)&LinkedList_1_Remove_m6741_gshared/* 789*/,
	(methodPointerType)&Dictionary_2__ctor_m6742_gshared/* 790*/,
	(methodPointerType)&Dictionary_2__ctor_m6744_gshared/* 791*/,
	(methodPointerType)&List_1__ctor_m6759_gshared/* 792*/,
	(methodPointerType)&Action_1_Invoke_m6793_gshared/* 793*/,
	(methodPointerType)&List_1__ctor_m8141_gshared/* 794*/,
	(methodPointerType)&List_1__ctor_m8142_gshared/* 795*/,
	(methodPointerType)&List_1__ctor_m8143_gshared/* 796*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m8146_gshared/* 797*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m8147_gshared/* 798*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m8149_gshared/* 799*/,
	(methodPointerType)&Dictionary_2__ctor_m29088_gshared/* 800*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t59_m9267_gshared/* 801*/,
	(methodPointerType)&CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t2101_m15429_gshared/* 802*/,
	(methodPointerType)&Array_AsReadOnly_TisCustomAttributeTypedArgument_t2101_m15430_gshared/* 803*/,
	(methodPointerType)&CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t2100_m15431_gshared/* 804*/,
	(methodPointerType)&Array_AsReadOnly_TisCustomAttributeNamedArgument_t2100_m15432_gshared/* 805*/,
	(methodPointerType)&GenericComparer_1__ctor_m15435_gshared/* 806*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m15436_gshared/* 807*/,
	(methodPointerType)&GenericComparer_1__ctor_m15437_gshared/* 808*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m15438_gshared/* 809*/,
	(methodPointerType)&Nullable_1__ctor_m15439_gshared/* 810*/,
	(methodPointerType)&Nullable_1_get_HasValue_m15440_gshared/* 811*/,
	(methodPointerType)&Nullable_1_get_Value_m15441_gshared/* 812*/,
	(methodPointerType)&GenericComparer_1__ctor_m15442_gshared/* 813*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m15443_gshared/* 814*/,
	(methodPointerType)&GenericComparer_1__ctor_m15445_gshared/* 815*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m15446_gshared/* 816*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt32_t59_m30237_gshared/* 817*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt32_t59_m30238_gshared/* 818*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt32_t59_m30239_gshared/* 819*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt32_t59_m30240_gshared/* 820*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt32_t59_m30241_gshared/* 821*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt32_t59_m30242_gshared/* 822*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt32_t59_m30243_gshared/* 823*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt32_t59_m30244_gshared/* 824*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t59_m30245_gshared/* 825*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDouble_t60_m30246_gshared/* 826*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDouble_t60_m30247_gshared/* 827*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDouble_t60_m30248_gshared/* 828*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDouble_t60_m30249_gshared/* 829*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDouble_t60_m30250_gshared/* 830*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDouble_t60_m30251_gshared/* 831*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDouble_t60_m30252_gshared/* 832*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDouble_t60_m30253_gshared/* 833*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t60_m30254_gshared/* 834*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisChar_t556_m30255_gshared/* 835*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisChar_t556_m30256_gshared/* 836*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisChar_t556_m30257_gshared/* 837*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisChar_t556_m30258_gshared/* 838*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisChar_t556_m30259_gshared/* 839*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisChar_t556_m30260_gshared/* 840*/,
	(methodPointerType)&Array_InternalArray__Insert_TisChar_t556_m30261_gshared/* 841*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisChar_t556_m30262_gshared/* 842*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisChar_t556_m30263_gshared/* 843*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector3_t6_m30270_gshared/* 844*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector3_t6_m30271_gshared/* 845*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector3_t6_m30272_gshared/* 846*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector3_t6_m30273_gshared/* 847*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector3_t6_m30274_gshared/* 848*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector3_t6_m30275_gshared/* 849*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector3_t6_m30276_gshared/* 850*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector3_t6_m30277_gshared/* 851*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t6_m30278_gshared/* 852*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector2_t29_m30279_gshared/* 853*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector2_t29_m30280_gshared/* 854*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector2_t29_m30281_gshared/* 855*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector2_t29_m30282_gshared/* 856*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector2_t29_m30283_gshared/* 857*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector2_t29_m30284_gshared/* 858*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector2_t29_m30285_gshared/* 859*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector2_t29_m30286_gshared/* 860*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t29_m30287_gshared/* 861*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisColor_t5_m30288_gshared/* 862*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisColor_t5_m30289_gshared/* 863*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisColor_t5_m30290_gshared/* 864*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisColor_t5_m30291_gshared/* 865*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisColor_t5_m30292_gshared/* 866*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisColor_t5_m30293_gshared/* 867*/,
	(methodPointerType)&Array_InternalArray__Insert_TisColor_t5_m30294_gshared/* 868*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisColor_t5_m30295_gshared/* 869*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisColor_t5_m30296_gshared/* 870*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSingle_t36_m30297_gshared/* 871*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSingle_t36_m30298_gshared/* 872*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSingle_t36_m30299_gshared/* 873*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSingle_t36_m30300_gshared/* 874*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSingle_t36_m30301_gshared/* 875*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSingle_t36_m30302_gshared/* 876*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSingle_t36_m30303_gshared/* 877*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSingle_t36_m30304_gshared/* 878*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t36_m30305_gshared/* 879*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRect_t30_m30306_gshared/* 880*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRect_t30_m30307_gshared/* 881*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRect_t30_m30308_gshared/* 882*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRect_t30_m30309_gshared/* 883*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRect_t30_m30310_gshared/* 884*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRect_t30_m30311_gshared/* 885*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRect_t30_m30312_gshared/* 886*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRect_t30_m30313_gshared/* 887*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRect_t30_m30314_gshared/* 888*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2607_m30315_gshared/* 889*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2607_m30316_gshared/* 890*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2607_m30317_gshared/* 891*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2607_m30318_gshared/* 892*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2607_m30319_gshared/* 893*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2607_m30320_gshared/* 894*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2607_m30321_gshared/* 895*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2607_m30322_gshared/* 896*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2607_m30323_gshared/* 897*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t1956_m30324_gshared/* 898*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t1956_m30325_gshared/* 899*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t1956_m30326_gshared/* 900*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t1956_m30327_gshared/* 901*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t1956_m30328_gshared/* 902*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t1956_m30329_gshared/* 903*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t1956_m30330_gshared/* 904*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t1956_m30331_gshared/* 905*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t1956_m30332_gshared/* 906*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30333_gshared/* 907*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30334_gshared/* 908*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30335_gshared/* 909*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30336_gshared/* 910*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30337_gshared/* 911*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDictionaryEntry_t58_m30338_gshared/* 912*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDictionaryEntry_t58_m30339_gshared/* 913*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t58_m30340_gshared/* 914*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t58_m30341_gshared/* 915*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t58_m30342_gshared/* 916*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDictionaryEntry_t58_m30343_gshared/* 917*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDictionaryEntry_t58_m30344_gshared/* 918*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDictionaryEntry_t58_m30345_gshared/* 919*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t58_m30346_gshared/* 920*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30347_gshared/* 921*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2607_m30348_gshared/* 922*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2607_TisObject_t_m30349_gshared/* 923*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2607_TisKeyValuePair_2_t2607_m30350_gshared/* 924*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2634_m30351_gshared/* 925*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2634_m30352_gshared/* 926*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2634_m30353_gshared/* 927*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2634_m30354_gshared/* 928*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2634_m30355_gshared/* 929*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2634_m30356_gshared/* 930*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2634_m30357_gshared/* 931*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2634_m30358_gshared/* 932*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2634_m30359_gshared/* 933*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30362_gshared/* 934*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2634_m30363_gshared/* 935*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2634_TisObject_t_m30364_gshared/* 936*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2634_TisKeyValuePair_2_t2634_m30365_gshared/* 937*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2658_m30366_gshared/* 938*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2658_m30367_gshared/* 939*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2658_m30368_gshared/* 940*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2658_m30369_gshared/* 941*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2658_m30370_gshared/* 942*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2658_m30371_gshared/* 943*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2658_m30372_gshared/* 944*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2658_m30373_gshared/* 945*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2658_m30374_gshared/* 946*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30375_gshared/* 947*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30376_gshared/* 948*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30377_gshared/* 949*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30378_gshared/* 950*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30379_gshared/* 951*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30380_gshared/* 952*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2658_m30381_gshared/* 953*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2658_TisObject_t_m30382_gshared/* 954*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2658_TisKeyValuePair_2_t2658_m30383_gshared/* 955*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisByte_t560_m30384_gshared/* 956*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisByte_t560_m30385_gshared/* 957*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisByte_t560_m30386_gshared/* 958*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisByte_t560_m30387_gshared/* 959*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisByte_t560_m30388_gshared/* 960*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisByte_t560_m30389_gshared/* 961*/,
	(methodPointerType)&Array_InternalArray__Insert_TisByte_t560_m30390_gshared/* 962*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisByte_t560_m30391_gshared/* 963*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t560_m30392_gshared/* 964*/,
	(methodPointerType)&Array_Resize_TisByte_t560_m30393_gshared/* 965*/,
	(methodPointerType)&Array_Resize_TisByte_t560_m30394_gshared/* 966*/,
	(methodPointerType)&Array_IndexOf_TisByte_t560_m30395_gshared/* 967*/,
	(methodPointerType)&Array_Sort_TisByte_t560_m30396_gshared/* 968*/,
	(methodPointerType)&Array_Sort_TisByte_t560_TisByte_t560_m30397_gshared/* 969*/,
	(methodPointerType)&Array_get_swapper_TisByte_t560_m30398_gshared/* 970*/,
	(methodPointerType)&Array_qsort_TisByte_t560_TisByte_t560_m30399_gshared/* 971*/,
	(methodPointerType)&Array_compare_TisByte_t560_m30400_gshared/* 972*/,
	(methodPointerType)&Array_swap_TisByte_t560_TisByte_t560_m30401_gshared/* 973*/,
	(methodPointerType)&Array_Sort_TisByte_t560_m30402_gshared/* 974*/,
	(methodPointerType)&Array_qsort_TisByte_t560_m30403_gshared/* 975*/,
	(methodPointerType)&Array_swap_TisByte_t560_m30404_gshared/* 976*/,
	(methodPointerType)&Utilities_TryGetValue_TisInt64_t507_m30408_gshared/* 977*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2787_m30410_gshared/* 978*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2787_m30411_gshared/* 979*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2787_m30412_gshared/* 980*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2787_m30413_gshared/* 981*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2787_m30414_gshared/* 982*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2787_m30415_gshared/* 983*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2787_m30416_gshared/* 984*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2787_m30417_gshared/* 985*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2787_m30418_gshared/* 986*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30419_gshared/* 987*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30420_gshared/* 988*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisSingle_t36_m30421_gshared/* 989*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisSingle_t36_TisObject_t_m30422_gshared/* 990*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisSingle_t36_TisSingle_t36_m30423_gshared/* 991*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30424_gshared/* 992*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2787_m30425_gshared/* 993*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2787_TisObject_t_m30426_gshared/* 994*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2787_TisKeyValuePair_2_t2787_m30427_gshared/* 995*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2811_m30428_gshared/* 996*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2811_m30429_gshared/* 997*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2811_m30430_gshared/* 998*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2811_m30431_gshared/* 999*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2811_m30432_gshared/* 1000*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2811_m30433_gshared/* 1001*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2811_m30434_gshared/* 1002*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2811_m30435_gshared/* 1003*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2811_m30436_gshared/* 1004*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30437_gshared/* 1005*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30438_gshared/* 1006*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30439_gshared/* 1007*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30440_gshared/* 1008*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2811_m30441_gshared/* 1009*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2811_TisObject_t_m30442_gshared/* 1010*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2811_TisKeyValuePair_2_t2811_m30443_gshared/* 1011*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWebCamDevice_t1219_m30444_gshared/* 1012*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWebCamDevice_t1219_m30445_gshared/* 1013*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWebCamDevice_t1219_m30446_gshared/* 1014*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWebCamDevice_t1219_m30447_gshared/* 1015*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWebCamDevice_t1219_m30448_gshared/* 1016*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWebCamDevice_t1219_m30449_gshared/* 1017*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWebCamDevice_t1219_m30450_gshared/* 1018*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWebCamDevice_t1219_m30451_gshared/* 1019*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamDevice_t1219_m30452_gshared/* 1020*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastResult_t647_m30458_gshared/* 1021*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastResult_t647_m30459_gshared/* 1022*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastResult_t647_m30460_gshared/* 1023*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t647_m30461_gshared/* 1024*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastResult_t647_m30462_gshared/* 1025*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastResult_t647_m30463_gshared/* 1026*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastResult_t647_m30464_gshared/* 1027*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastResult_t647_m30465_gshared/* 1028*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t647_m30466_gshared/* 1029*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t647_m30467_gshared/* 1030*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t647_m30468_gshared/* 1031*/,
	(methodPointerType)&Array_IndexOf_TisRaycastResult_t647_m30469_gshared/* 1032*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t647_m30470_gshared/* 1033*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t647_TisRaycastResult_t647_m30471_gshared/* 1034*/,
	(methodPointerType)&Array_get_swapper_TisRaycastResult_t647_m30472_gshared/* 1035*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t647_TisRaycastResult_t647_m30473_gshared/* 1036*/,
	(methodPointerType)&Array_compare_TisRaycastResult_t647_m30474_gshared/* 1037*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t647_TisRaycastResult_t647_m30475_gshared/* 1038*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t647_m30476_gshared/* 1039*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t647_m30477_gshared/* 1040*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t647_m30478_gshared/* 1041*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit2D_t838_m30481_gshared/* 1042*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit2D_t838_m30482_gshared/* 1043*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t838_m30483_gshared/* 1044*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t838_m30484_gshared/* 1045*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t838_m30485_gshared/* 1046*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit2D_t838_m30486_gshared/* 1047*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit2D_t838_m30487_gshared/* 1048*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit2D_t838_m30488_gshared/* 1049*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t838_m30489_gshared/* 1050*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit_t566_m30490_gshared/* 1051*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit_t566_m30491_gshared/* 1052*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit_t566_m30492_gshared/* 1053*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t566_m30493_gshared/* 1054*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit_t566_m30494_gshared/* 1055*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit_t566_m30495_gshared/* 1056*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit_t566_m30496_gshared/* 1057*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit_t566_m30497_gshared/* 1058*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t566_m30498_gshared/* 1059*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t566_m30499_gshared/* 1060*/,
	(methodPointerType)&Array_qsort_TisRaycastHit_t566_m30500_gshared/* 1061*/,
	(methodPointerType)&Array_swap_TisRaycastHit_t566_m30501_gshared/* 1062*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisColor_t5_m30502_gshared/* 1063*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t727_m30503_gshared/* 1064*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t727_m30504_gshared/* 1065*/,
	(methodPointerType)&Array_IndexOf_TisUIVertex_t727_m30505_gshared/* 1066*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t727_m30506_gshared/* 1067*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t727_TisUIVertex_t727_m30507_gshared/* 1068*/,
	(methodPointerType)&Array_get_swapper_TisUIVertex_t727_m30508_gshared/* 1069*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t727_TisUIVertex_t727_m30509_gshared/* 1070*/,
	(methodPointerType)&Array_compare_TisUIVertex_t727_m30510_gshared/* 1071*/,
	(methodPointerType)&Array_swap_TisUIVertex_t727_TisUIVertex_t727_m30511_gshared/* 1072*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t727_m30512_gshared/* 1073*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t727_m30513_gshared/* 1074*/,
	(methodPointerType)&Array_swap_TisUIVertex_t727_m30514_gshared/* 1075*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisContentType_t710_m30515_gshared/* 1076*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisContentType_t710_m30516_gshared/* 1077*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisContentType_t710_m30517_gshared/* 1078*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisContentType_t710_m30518_gshared/* 1079*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisContentType_t710_m30519_gshared/* 1080*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisContentType_t710_m30520_gshared/* 1081*/,
	(methodPointerType)&Array_InternalArray__Insert_TisContentType_t710_m30521_gshared/* 1082*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisContentType_t710_m30522_gshared/* 1083*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisContentType_t710_m30523_gshared/* 1084*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUILineInfo_t857_m30524_gshared/* 1085*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUILineInfo_t857_m30525_gshared/* 1086*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUILineInfo_t857_m30526_gshared/* 1087*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t857_m30527_gshared/* 1088*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUILineInfo_t857_m30528_gshared/* 1089*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUILineInfo_t857_m30529_gshared/* 1090*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUILineInfo_t857_m30530_gshared/* 1091*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUILineInfo_t857_m30531_gshared/* 1092*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t857_m30532_gshared/* 1093*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUICharInfo_t859_m30533_gshared/* 1094*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUICharInfo_t859_m30534_gshared/* 1095*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUICharInfo_t859_m30535_gshared/* 1096*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t859_m30536_gshared/* 1097*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUICharInfo_t859_m30537_gshared/* 1098*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUICharInfo_t859_m30538_gshared/* 1099*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUICharInfo_t859_m30539_gshared/* 1100*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUICharInfo_t859_m30540_gshared/* 1101*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t859_m30541_gshared/* 1102*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t36_m30542_gshared/* 1103*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t29_m30543_gshared/* 1104*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t41_m30544_gshared/* 1105*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisEyewearCalibrationReading_t913_m30548_gshared/* 1106*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisEyewearCalibrationReading_t913_m30549_gshared/* 1107*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisEyewearCalibrationReading_t913_m30550_gshared/* 1108*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisEyewearCalibrationReading_t913_m30551_gshared/* 1109*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisEyewearCalibrationReading_t913_m30552_gshared/* 1110*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisEyewearCalibrationReading_t913_m30553_gshared/* 1111*/,
	(methodPointerType)&Array_InternalArray__Insert_TisEyewearCalibrationReading_t913_m30554_gshared/* 1112*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisEyewearCalibrationReading_t913_m30555_gshared/* 1113*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisEyewearCalibrationReading_t913_m30556_gshared/* 1114*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTargetSearchResult_t1050_m30558_gshared/* 1115*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTargetSearchResult_t1050_m30559_gshared/* 1116*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTargetSearchResult_t1050_m30560_gshared/* 1117*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTargetSearchResult_t1050_m30561_gshared/* 1118*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTargetSearchResult_t1050_m30562_gshared/* 1119*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTargetSearchResult_t1050_m30563_gshared/* 1120*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTargetSearchResult_t1050_m30564_gshared/* 1121*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTargetSearchResult_t1050_m30565_gshared/* 1122*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTargetSearchResult_t1050_m30566_gshared/* 1123*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3060_m30568_gshared/* 1124*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3060_m30569_gshared/* 1125*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3060_m30570_gshared/* 1126*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3060_m30571_gshared/* 1127*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3060_m30572_gshared/* 1128*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3060_m30573_gshared/* 1129*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3060_m30574_gshared/* 1130*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3060_m30575_gshared/* 1131*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3060_m30576_gshared/* 1132*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisPIXEL_FORMAT_t942_m30577_gshared/* 1133*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisPIXEL_FORMAT_t942_m30578_gshared/* 1134*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisPIXEL_FORMAT_t942_m30579_gshared/* 1135*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisPIXEL_FORMAT_t942_m30580_gshared/* 1136*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisPIXEL_FORMAT_t942_m30581_gshared/* 1137*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisPIXEL_FORMAT_t942_m30582_gshared/* 1138*/,
	(methodPointerType)&Array_InternalArray__Insert_TisPIXEL_FORMAT_t942_m30583_gshared/* 1139*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisPIXEL_FORMAT_t942_m30584_gshared/* 1140*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisPIXEL_FORMAT_t942_m30585_gshared/* 1141*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisPIXEL_FORMAT_t942_m30586_gshared/* 1142*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisPIXEL_FORMAT_t942_TisObject_t_m30587_gshared/* 1143*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisPIXEL_FORMAT_t942_TisPIXEL_FORMAT_t942_m30588_gshared/* 1144*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30589_gshared/* 1145*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30590_gshared/* 1146*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30591_gshared/* 1147*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3060_m30592_gshared/* 1148*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3060_TisObject_t_m30593_gshared/* 1149*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3060_TisKeyValuePair_2_t3060_m30594_gshared/* 1150*/,
	(methodPointerType)&Array_Resize_TisPIXEL_FORMAT_t942_m30595_gshared/* 1151*/,
	(methodPointerType)&Array_Resize_TisPIXEL_FORMAT_t942_m30596_gshared/* 1152*/,
	(methodPointerType)&Array_IndexOf_TisPIXEL_FORMAT_t942_m30597_gshared/* 1153*/,
	(methodPointerType)&Array_Sort_TisPIXEL_FORMAT_t942_m30598_gshared/* 1154*/,
	(methodPointerType)&Array_Sort_TisPIXEL_FORMAT_t942_TisPIXEL_FORMAT_t942_m30599_gshared/* 1155*/,
	(methodPointerType)&Array_get_swapper_TisPIXEL_FORMAT_t942_m30600_gshared/* 1156*/,
	(methodPointerType)&Array_qsort_TisPIXEL_FORMAT_t942_TisPIXEL_FORMAT_t942_m30601_gshared/* 1157*/,
	(methodPointerType)&Array_compare_TisPIXEL_FORMAT_t942_m30602_gshared/* 1158*/,
	(methodPointerType)&Array_swap_TisPIXEL_FORMAT_t942_TisPIXEL_FORMAT_t942_m30603_gshared/* 1159*/,
	(methodPointerType)&Array_Sort_TisPIXEL_FORMAT_t942_m30604_gshared/* 1160*/,
	(methodPointerType)&Array_qsort_TisPIXEL_FORMAT_t942_m30605_gshared/* 1161*/,
	(methodPointerType)&Array_swap_TisPIXEL_FORMAT_t942_m30606_gshared/* 1162*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisColor32_t829_m30607_gshared/* 1163*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisColor32_t829_m30608_gshared/* 1164*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisColor32_t829_m30609_gshared/* 1165*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisColor32_t829_m30610_gshared/* 1166*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisColor32_t829_m30611_gshared/* 1167*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisColor32_t829_m30612_gshared/* 1168*/,
	(methodPointerType)&Array_InternalArray__Insert_TisColor32_t829_m30613_gshared/* 1169*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisColor32_t829_m30614_gshared/* 1170*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisColor32_t829_m30615_gshared/* 1171*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTrackableResultData_t969_m30618_gshared/* 1172*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTrackableResultData_t969_m30619_gshared/* 1173*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTrackableResultData_t969_m30620_gshared/* 1174*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTrackableResultData_t969_m30621_gshared/* 1175*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTrackableResultData_t969_m30622_gshared/* 1176*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTrackableResultData_t969_m30623_gshared/* 1177*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTrackableResultData_t969_m30624_gshared/* 1178*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTrackableResultData_t969_m30625_gshared/* 1179*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackableResultData_t969_m30626_gshared/* 1180*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWordData_t974_m30627_gshared/* 1181*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWordData_t974_m30628_gshared/* 1182*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWordData_t974_m30629_gshared/* 1183*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWordData_t974_m30630_gshared/* 1184*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWordData_t974_m30631_gshared/* 1185*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWordData_t974_m30632_gshared/* 1186*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWordData_t974_m30633_gshared/* 1187*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWordData_t974_m30634_gshared/* 1188*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWordData_t974_m30635_gshared/* 1189*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWordResultData_t973_m30636_gshared/* 1190*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWordResultData_t973_m30637_gshared/* 1191*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWordResultData_t973_m30638_gshared/* 1192*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWordResultData_t973_m30639_gshared/* 1193*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWordResultData_t973_m30640_gshared/* 1194*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWordResultData_t973_m30641_gshared/* 1195*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWordResultData_t973_m30642_gshared/* 1196*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWordResultData_t973_m30643_gshared/* 1197*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWordResultData_t973_m30644_gshared/* 1198*/,
	(methodPointerType)&Array_Resize_TisInt32_t59_m30645_gshared/* 1199*/,
	(methodPointerType)&Array_Resize_TisInt32_t59_m30646_gshared/* 1200*/,
	(methodPointerType)&Array_IndexOf_TisInt32_t59_m30647_gshared/* 1201*/,
	(methodPointerType)&Array_Sort_TisInt32_t59_m30648_gshared/* 1202*/,
	(methodPointerType)&Array_Sort_TisInt32_t59_TisInt32_t59_m30649_gshared/* 1203*/,
	(methodPointerType)&Array_get_swapper_TisInt32_t59_m30650_gshared/* 1204*/,
	(methodPointerType)&Array_qsort_TisInt32_t59_TisInt32_t59_m30651_gshared/* 1205*/,
	(methodPointerType)&Array_compare_TisInt32_t59_m30652_gshared/* 1206*/,
	(methodPointerType)&Array_swap_TisInt32_t59_TisInt32_t59_m30653_gshared/* 1207*/,
	(methodPointerType)&Array_Sort_TisInt32_t59_m30654_gshared/* 1208*/,
	(methodPointerType)&Array_qsort_TisInt32_t59_m30655_gshared/* 1209*/,
	(methodPointerType)&Array_swap_TisInt32_t59_m30656_gshared/* 1210*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t977_m30657_gshared/* 1211*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSmartTerrainRevisionData_t977_m30658_gshared/* 1212*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSmartTerrainRevisionData_t977_m30659_gshared/* 1213*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSmartTerrainRevisionData_t977_m30660_gshared/* 1214*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSmartTerrainRevisionData_t977_m30661_gshared/* 1215*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSmartTerrainRevisionData_t977_m30662_gshared/* 1216*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSmartTerrainRevisionData_t977_m30663_gshared/* 1217*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSmartTerrainRevisionData_t977_m30664_gshared/* 1218*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainRevisionData_t977_m30665_gshared/* 1219*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSurfaceData_t978_m30666_gshared/* 1220*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSurfaceData_t978_m30667_gshared/* 1221*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSurfaceData_t978_m30668_gshared/* 1222*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSurfaceData_t978_m30669_gshared/* 1223*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSurfaceData_t978_m30670_gshared/* 1224*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSurfaceData_t978_m30671_gshared/* 1225*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSurfaceData_t978_m30672_gshared/* 1226*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSurfaceData_t978_m30673_gshared/* 1227*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSurfaceData_t978_m30674_gshared/* 1228*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisPropData_t979_m30675_gshared/* 1229*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisPropData_t979_m30676_gshared/* 1230*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisPropData_t979_m30677_gshared/* 1231*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisPropData_t979_m30678_gshared/* 1232*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisPropData_t979_m30679_gshared/* 1233*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisPropData_t979_m30680_gshared/* 1234*/,
	(methodPointerType)&Array_InternalArray__Insert_TisPropData_t979_m30681_gshared/* 1235*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisPropData_t979_m30682_gshared/* 1236*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisPropData_t979_m30683_gshared/* 1237*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3151_m30685_gshared/* 1238*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3151_m30686_gshared/* 1239*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3151_m30687_gshared/* 1240*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3151_m30688_gshared/* 1241*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3151_m30689_gshared/* 1242*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3151_m30690_gshared/* 1243*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3151_m30691_gshared/* 1244*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3151_m30692_gshared/* 1245*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3151_m30693_gshared/* 1246*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt16_t562_m30694_gshared/* 1247*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt16_t562_m30695_gshared/* 1248*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt16_t562_m30696_gshared/* 1249*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt16_t562_m30697_gshared/* 1250*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt16_t562_m30698_gshared/* 1251*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt16_t562_m30699_gshared/* 1252*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt16_t562_m30700_gshared/* 1253*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt16_t562_m30701_gshared/* 1254*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t562_m30702_gshared/* 1255*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30703_gshared/* 1256*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30704_gshared/* 1257*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t562_m30705_gshared/* 1258*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt16_t562_TisObject_t_m30706_gshared/* 1259*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt16_t562_TisUInt16_t562_m30707_gshared/* 1260*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30708_gshared/* 1261*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3151_m30709_gshared/* 1262*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3151_TisObject_t_m30710_gshared/* 1263*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3151_TisKeyValuePair_2_t3151_m30711_gshared/* 1264*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRectangleData_t923_m30712_gshared/* 1265*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRectangleData_t923_m30713_gshared/* 1266*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRectangleData_t923_m30714_gshared/* 1267*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRectangleData_t923_m30715_gshared/* 1268*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRectangleData_t923_m30716_gshared/* 1269*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRectangleData_t923_m30717_gshared/* 1270*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRectangleData_t923_m30718_gshared/* 1271*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRectangleData_t923_m30719_gshared/* 1272*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRectangleData_t923_m30720_gshared/* 1273*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3240_m30721_gshared/* 1274*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3240_m30722_gshared/* 1275*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3240_m30723_gshared/* 1276*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3240_m30724_gshared/* 1277*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3240_m30725_gshared/* 1278*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3240_m30726_gshared/* 1279*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3240_m30727_gshared/* 1280*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3240_m30728_gshared/* 1281*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3240_m30729_gshared/* 1282*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30730_gshared/* 1283*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30731_gshared/* 1284*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30732_gshared/* 1285*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisTrackableResultData_t969_m30733_gshared/* 1286*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisTrackableResultData_t969_TisObject_t_m30734_gshared/* 1287*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisTrackableResultData_t969_TisTrackableResultData_t969_m30735_gshared/* 1288*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30736_gshared/* 1289*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3240_m30737_gshared/* 1290*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3240_TisObject_t_m30738_gshared/* 1291*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3240_TisKeyValuePair_2_t3240_m30739_gshared/* 1292*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3255_m30740_gshared/* 1293*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3255_m30741_gshared/* 1294*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3255_m30742_gshared/* 1295*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3255_m30743_gshared/* 1296*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3255_m30744_gshared/* 1297*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3255_m30745_gshared/* 1298*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3255_m30746_gshared/* 1299*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3255_m30747_gshared/* 1300*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3255_m30748_gshared/* 1301*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVirtualButtonData_t970_m30749_gshared/* 1302*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVirtualButtonData_t970_m30750_gshared/* 1303*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVirtualButtonData_t970_m30751_gshared/* 1304*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVirtualButtonData_t970_m30752_gshared/* 1305*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVirtualButtonData_t970_m30753_gshared/* 1306*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVirtualButtonData_t970_m30754_gshared/* 1307*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVirtualButtonData_t970_m30755_gshared/* 1308*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVirtualButtonData_t970_m30756_gshared/* 1309*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVirtualButtonData_t970_m30757_gshared/* 1310*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t59_m30758_gshared/* 1311*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisObject_t_m30759_gshared/* 1312*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t59_TisInt32_t59_m30760_gshared/* 1313*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisVirtualButtonData_t970_m30761_gshared/* 1314*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisVirtualButtonData_t970_TisObject_t_m30762_gshared/* 1315*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisVirtualButtonData_t970_TisVirtualButtonData_t970_m30763_gshared/* 1316*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30764_gshared/* 1317*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3255_m30765_gshared/* 1318*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3255_TisObject_t_m30766_gshared/* 1319*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3255_TisKeyValuePair_2_t3255_m30767_gshared/* 1320*/,
	(methodPointerType)&Array_Resize_TisTargetSearchResult_t1050_m30768_gshared/* 1321*/,
	(methodPointerType)&Array_Resize_TisTargetSearchResult_t1050_m30769_gshared/* 1322*/,
	(methodPointerType)&Array_IndexOf_TisTargetSearchResult_t1050_m30770_gshared/* 1323*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t1050_m30771_gshared/* 1324*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t1050_TisTargetSearchResult_t1050_m30772_gshared/* 1325*/,
	(methodPointerType)&Array_get_swapper_TisTargetSearchResult_t1050_m30773_gshared/* 1326*/,
	(methodPointerType)&Array_qsort_TisTargetSearchResult_t1050_TisTargetSearchResult_t1050_m30774_gshared/* 1327*/,
	(methodPointerType)&Array_compare_TisTargetSearchResult_t1050_m30775_gshared/* 1328*/,
	(methodPointerType)&Array_swap_TisTargetSearchResult_t1050_TisTargetSearchResult_t1050_m30776_gshared/* 1329*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t1050_m30777_gshared/* 1330*/,
	(methodPointerType)&Array_qsort_TisTargetSearchResult_t1050_m30778_gshared/* 1331*/,
	(methodPointerType)&Array_swap_TisTargetSearchResult_t1050_m30779_gshared/* 1332*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3292_m30783_gshared/* 1333*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3292_m30784_gshared/* 1334*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3292_m30785_gshared/* 1335*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3292_m30786_gshared/* 1336*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3292_m30787_gshared/* 1337*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3292_m30788_gshared/* 1338*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3292_m30789_gshared/* 1339*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3292_m30790_gshared/* 1340*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3292_m30791_gshared/* 1341*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisProfileData_t1064_m30792_gshared/* 1342*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisProfileData_t1064_m30793_gshared/* 1343*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisProfileData_t1064_m30794_gshared/* 1344*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisProfileData_t1064_m30795_gshared/* 1345*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisProfileData_t1064_m30796_gshared/* 1346*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisProfileData_t1064_m30797_gshared/* 1347*/,
	(methodPointerType)&Array_InternalArray__Insert_TisProfileData_t1064_m30798_gshared/* 1348*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisProfileData_t1064_m30799_gshared/* 1349*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisProfileData_t1064_m30800_gshared/* 1350*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30801_gshared/* 1351*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30802_gshared/* 1352*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t1064_m30803_gshared/* 1353*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisProfileData_t1064_TisObject_t_m30804_gshared/* 1354*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisProfileData_t1064_TisProfileData_t1064_m30805_gshared/* 1355*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30806_gshared/* 1356*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3292_m30807_gshared/* 1357*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3292_TisObject_t_m30808_gshared/* 1358*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3292_TisKeyValuePair_2_t3292_m30809_gshared/* 1359*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t3340_m30810_gshared/* 1360*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t3340_m30811_gshared/* 1361*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t3340_m30812_gshared/* 1362*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t3340_m30813_gshared/* 1363*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t3340_m30814_gshared/* 1364*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t3340_m30815_gshared/* 1365*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t3340_m30816_gshared/* 1366*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t3340_m30817_gshared/* 1367*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3340_m30818_gshared/* 1368*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisParameterModifier_t2119_m30819_gshared/* 1369*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisParameterModifier_t2119_m30820_gshared/* 1370*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisParameterModifier_t2119_m30821_gshared/* 1371*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t2119_m30822_gshared/* 1372*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisParameterModifier_t2119_m30823_gshared/* 1373*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisParameterModifier_t2119_m30824_gshared/* 1374*/,
	(methodPointerType)&Array_InternalArray__Insert_TisParameterModifier_t2119_m30825_gshared/* 1375*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisParameterModifier_t2119_m30826_gshared/* 1376*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t2119_m30827_gshared/* 1377*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcAchievementData_t1244_m30828_gshared/* 1378*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcAchievementData_t1244_m30829_gshared/* 1379*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcAchievementData_t1244_m30830_gshared/* 1380*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t1244_m30831_gshared/* 1381*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcAchievementData_t1244_m30832_gshared/* 1382*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcAchievementData_t1244_m30833_gshared/* 1383*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcAchievementData_t1244_m30834_gshared/* 1384*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcAchievementData_t1244_m30835_gshared/* 1385*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t1244_m30836_gshared/* 1386*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcScoreData_t1245_m30837_gshared/* 1387*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcScoreData_t1245_m30838_gshared/* 1388*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcScoreData_t1245_m30839_gshared/* 1389*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t1245_m30840_gshared/* 1390*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcScoreData_t1245_m30841_gshared/* 1391*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcScoreData_t1245_m30842_gshared/* 1392*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcScoreData_t1245_m30843_gshared/* 1393*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcScoreData_t1245_m30844_gshared/* 1394*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t1245_m30845_gshared/* 1395*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisHitInfo_t1271_m30846_gshared/* 1396*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisHitInfo_t1271_m30847_gshared/* 1397*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisHitInfo_t1271_m30848_gshared/* 1398*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisHitInfo_t1271_m30849_gshared/* 1399*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisHitInfo_t1271_m30850_gshared/* 1400*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisHitInfo_t1271_m30851_gshared/* 1401*/,
	(methodPointerType)&Array_InternalArray__Insert_TisHitInfo_t1271_m30852_gshared/* 1402*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisHitInfo_t1271_m30853_gshared/* 1403*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t1271_m30854_gshared/* 1404*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3373_m30855_gshared/* 1405*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3373_m30856_gshared/* 1406*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3373_m30857_gshared/* 1407*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3373_m30858_gshared/* 1408*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3373_m30859_gshared/* 1409*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3373_m30860_gshared/* 1410*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3373_m30861_gshared/* 1411*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3373_m30862_gshared/* 1412*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3373_m30863_gshared/* 1413*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTextEditOp_t1286_m30864_gshared/* 1414*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTextEditOp_t1286_m30865_gshared/* 1415*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTextEditOp_t1286_m30866_gshared/* 1416*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTextEditOp_t1286_m30867_gshared/* 1417*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTextEditOp_t1286_m30868_gshared/* 1418*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTextEditOp_t1286_m30869_gshared/* 1419*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTextEditOp_t1286_m30870_gshared/* 1420*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTextEditOp_t1286_m30871_gshared/* 1421*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTextEditOp_t1286_m30872_gshared/* 1422*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30873_gshared/* 1423*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30874_gshared/* 1424*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisTextEditOp_t1286_m30875_gshared/* 1425*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisTextEditOp_t1286_TisObject_t_m30876_gshared/* 1426*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisTextEditOp_t1286_TisTextEditOp_t1286_m30877_gshared/* 1427*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30878_gshared/* 1428*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3373_m30879_gshared/* 1429*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3373_TisObject_t_m30880_gshared/* 1430*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3373_TisKeyValuePair_2_t3373_m30881_gshared/* 1431*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t859_m30882_gshared/* 1432*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t859_m30883_gshared/* 1433*/,
	(methodPointerType)&Array_IndexOf_TisUICharInfo_t859_m30884_gshared/* 1434*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t859_m30885_gshared/* 1435*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t859_TisUICharInfo_t859_m30886_gshared/* 1436*/,
	(methodPointerType)&Array_get_swapper_TisUICharInfo_t859_m30887_gshared/* 1437*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t859_TisUICharInfo_t859_m30888_gshared/* 1438*/,
	(methodPointerType)&Array_compare_TisUICharInfo_t859_m30889_gshared/* 1439*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t859_TisUICharInfo_t859_m30890_gshared/* 1440*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t859_m30891_gshared/* 1441*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t859_m30892_gshared/* 1442*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t859_m30893_gshared/* 1443*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t857_m30894_gshared/* 1444*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t857_m30895_gshared/* 1445*/,
	(methodPointerType)&Array_IndexOf_TisUILineInfo_t857_m30896_gshared/* 1446*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t857_m30897_gshared/* 1447*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t857_TisUILineInfo_t857_m30898_gshared/* 1448*/,
	(methodPointerType)&Array_get_swapper_TisUILineInfo_t857_m30899_gshared/* 1449*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t857_TisUILineInfo_t857_m30900_gshared/* 1450*/,
	(methodPointerType)&Array_compare_TisUILineInfo_t857_m30901_gshared/* 1451*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t857_TisUILineInfo_t857_m30902_gshared/* 1452*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t857_m30903_gshared/* 1453*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t857_m30904_gshared/* 1454*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t857_m30905_gshared/* 1455*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t59_m30906_gshared/* 1456*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyframe_t1321_m30907_gshared/* 1457*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyframe_t1321_m30908_gshared/* 1458*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyframe_t1321_m30909_gshared/* 1459*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyframe_t1321_m30910_gshared/* 1460*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyframe_t1321_m30911_gshared/* 1461*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyframe_t1321_m30912_gshared/* 1462*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyframe_t1321_m30913_gshared/* 1463*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyframe_t1321_m30914_gshared/* 1464*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t1321_m30915_gshared/* 1465*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisIntPtr_t_m30916_gshared/* 1466*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisIntPtr_t_m30917_gshared/* 1467*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisIntPtr_t_m30918_gshared/* 1468*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m30919_gshared/* 1469*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisIntPtr_t_m30920_gshared/* 1470*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisIntPtr_t_m30921_gshared/* 1471*/,
	(methodPointerType)&Array_InternalArray__Insert_TisIntPtr_t_m30922_gshared/* 1472*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisIntPtr_t_m30923_gshared/* 1473*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m30924_gshared/* 1474*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3478_m30925_gshared/* 1475*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3478_m30926_gshared/* 1476*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3478_m30927_gshared/* 1477*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3478_m30928_gshared/* 1478*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3478_m30929_gshared/* 1479*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3478_m30930_gshared/* 1480*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3478_m30931_gshared/* 1481*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3478_m30932_gshared/* 1482*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3478_m30933_gshared/* 1483*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisBoolean_t41_m30934_gshared/* 1484*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisBoolean_t41_m30935_gshared/* 1485*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisBoolean_t41_m30936_gshared/* 1486*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisBoolean_t41_m30937_gshared/* 1487*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisBoolean_t41_m30938_gshared/* 1488*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisBoolean_t41_m30939_gshared/* 1489*/,
	(methodPointerType)&Array_InternalArray__Insert_TisBoolean_t41_m30940_gshared/* 1490*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisBoolean_t41_m30941_gshared/* 1491*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisBoolean_t41_m30942_gshared/* 1492*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30943_gshared/* 1493*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30944_gshared/* 1494*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisBoolean_t41_m30945_gshared/* 1495*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisBoolean_t41_TisObject_t_m30946_gshared/* 1496*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisBoolean_t41_TisBoolean_t41_m30947_gshared/* 1497*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t58_TisDictionaryEntry_t58_m30948_gshared/* 1498*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3478_m30949_gshared/* 1499*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3478_TisObject_t_m30950_gshared/* 1500*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3478_TisKeyValuePair_2_t3478_m30951_gshared/* 1501*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisX509ChainStatus_t1558_m30952_gshared/* 1502*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1558_m30953_gshared/* 1503*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1558_m30954_gshared/* 1504*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1558_m30955_gshared/* 1505*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1558_m30956_gshared/* 1506*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisX509ChainStatus_t1558_m30957_gshared/* 1507*/,
	(methodPointerType)&Array_InternalArray__Insert_TisX509ChainStatus_t1558_m30958_gshared/* 1508*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisX509ChainStatus_t1558_m30959_gshared/* 1509*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1558_m30960_gshared/* 1510*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t59_m30961_gshared/* 1511*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisMark_t1605_m30962_gshared/* 1512*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisMark_t1605_m30963_gshared/* 1513*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisMark_t1605_m30964_gshared/* 1514*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisMark_t1605_m30965_gshared/* 1515*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisMark_t1605_m30966_gshared/* 1516*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisMark_t1605_m30967_gshared/* 1517*/,
	(methodPointerType)&Array_InternalArray__Insert_TisMark_t1605_m30968_gshared/* 1518*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisMark_t1605_m30969_gshared/* 1519*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1605_m30970_gshared/* 1520*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUriScheme_t1641_m30971_gshared/* 1521*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUriScheme_t1641_m30972_gshared/* 1522*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUriScheme_t1641_m30973_gshared/* 1523*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1641_m30974_gshared/* 1524*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUriScheme_t1641_m30975_gshared/* 1525*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUriScheme_t1641_m30976_gshared/* 1526*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUriScheme_t1641_m30977_gshared/* 1527*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUriScheme_t1641_m30978_gshared/* 1528*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1641_m30979_gshared/* 1529*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt32_t558_m30980_gshared/* 1530*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt32_t558_m30981_gshared/* 1531*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt32_t558_m30982_gshared/* 1532*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt32_t558_m30983_gshared/* 1533*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt32_t558_m30984_gshared/* 1534*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt32_t558_m30985_gshared/* 1535*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt32_t558_m30986_gshared/* 1536*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt32_t558_m30987_gshared/* 1537*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t558_m30988_gshared/* 1538*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisClientCertificateType_t1784_m30989_gshared/* 1539*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisClientCertificateType_t1784_m30990_gshared/* 1540*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisClientCertificateType_t1784_m30991_gshared/* 1541*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisClientCertificateType_t1784_m30992_gshared/* 1542*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisClientCertificateType_t1784_m30993_gshared/* 1543*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisClientCertificateType_t1784_m30994_gshared/* 1544*/,
	(methodPointerType)&Array_InternalArray__Insert_TisClientCertificateType_t1784_m30995_gshared/* 1545*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisClientCertificateType_t1784_m30996_gshared/* 1546*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisClientCertificateType_t1784_m30997_gshared/* 1547*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt64_t563_m30998_gshared/* 1548*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt64_t563_m30999_gshared/* 1549*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt64_t563_m31000_gshared/* 1550*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt64_t563_m31001_gshared/* 1551*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt64_t563_m31002_gshared/* 1552*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt64_t563_m31003_gshared/* 1553*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt64_t563_m31004_gshared/* 1554*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt64_t563_m31005_gshared/* 1555*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t563_m31006_gshared/* 1556*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt16_t561_m31007_gshared/* 1557*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt16_t561_m31008_gshared/* 1558*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt16_t561_m31009_gshared/* 1559*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt16_t561_m31010_gshared/* 1560*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt16_t561_m31011_gshared/* 1561*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt16_t561_m31012_gshared/* 1562*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt16_t561_m31013_gshared/* 1563*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt16_t561_m31014_gshared/* 1564*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t561_m31138_gshared/* 1565*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSByte_t559_m31139_gshared/* 1566*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSByte_t559_m31140_gshared/* 1567*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSByte_t559_m31141_gshared/* 1568*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSByte_t559_m31142_gshared/* 1569*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSByte_t559_m31143_gshared/* 1570*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSByte_t559_m31144_gshared/* 1571*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSByte_t559_m31145_gshared/* 1572*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSByte_t559_m31146_gshared/* 1573*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t559_m31147_gshared/* 1574*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt64_t507_m31148_gshared/* 1575*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt64_t507_m31149_gshared/* 1576*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt64_t507_m31150_gshared/* 1577*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt64_t507_m31151_gshared/* 1578*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt64_t507_m31152_gshared/* 1579*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt64_t507_m31153_gshared/* 1580*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt64_t507_m31154_gshared/* 1581*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt64_t507_m31155_gshared/* 1582*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t507_m31156_gshared/* 1583*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTableRange_t1889_m31184_gshared/* 1584*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTableRange_t1889_m31185_gshared/* 1585*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTableRange_t1889_m31186_gshared/* 1586*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTableRange_t1889_m31187_gshared/* 1587*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTableRange_t1889_m31188_gshared/* 1588*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTableRange_t1889_m31189_gshared/* 1589*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTableRange_t1889_m31190_gshared/* 1590*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTableRange_t1889_m31191_gshared/* 1591*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1889_m31192_gshared/* 1592*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t1966_m31193_gshared/* 1593*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t1966_m31194_gshared/* 1594*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t1966_m31195_gshared/* 1595*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t1966_m31196_gshared/* 1596*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t1966_m31197_gshared/* 1597*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t1966_m31198_gshared/* 1598*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t1966_m31199_gshared/* 1599*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t1966_m31200_gshared/* 1600*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1966_m31201_gshared/* 1601*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t1974_m31202_gshared/* 1602*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t1974_m31203_gshared/* 1603*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t1974_m31204_gshared/* 1604*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t1974_m31205_gshared/* 1605*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t1974_m31206_gshared/* 1606*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t1974_m31207_gshared/* 1607*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t1974_m31208_gshared/* 1608*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t1974_m31209_gshared/* 1609*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1974_m31210_gshared/* 1610*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisILTokenInfo_t2053_m31211_gshared/* 1611*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisILTokenInfo_t2053_m31212_gshared/* 1612*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisILTokenInfo_t2053_m31213_gshared/* 1613*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t2053_m31214_gshared/* 1614*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisILTokenInfo_t2053_m31215_gshared/* 1615*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisILTokenInfo_t2053_m31216_gshared/* 1616*/,
	(methodPointerType)&Array_InternalArray__Insert_TisILTokenInfo_t2053_m31217_gshared/* 1617*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisILTokenInfo_t2053_m31218_gshared/* 1618*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t2053_m31219_gshared/* 1619*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLabelData_t2055_m31220_gshared/* 1620*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLabelData_t2055_m31221_gshared/* 1621*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLabelData_t2055_m31222_gshared/* 1622*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLabelData_t2055_m31223_gshared/* 1623*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLabelData_t2055_m31224_gshared/* 1624*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLabelData_t2055_m31225_gshared/* 1625*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLabelData_t2055_m31226_gshared/* 1626*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLabelData_t2055_m31227_gshared/* 1627*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t2055_m31228_gshared/* 1628*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLabelFixup_t2054_m31229_gshared/* 1629*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLabelFixup_t2054_m31230_gshared/* 1630*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLabelFixup_t2054_m31231_gshared/* 1631*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t2054_m31232_gshared/* 1632*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLabelFixup_t2054_m31233_gshared/* 1633*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLabelFixup_t2054_m31234_gshared/* 1634*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLabelFixup_t2054_m31235_gshared/* 1635*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLabelFixup_t2054_m31236_gshared/* 1636*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t2054_m31237_gshared/* 1637*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2101_m31238_gshared/* 1638*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t2101_m31239_gshared/* 1639*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t2101_m31240_gshared/* 1640*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t2101_m31241_gshared/* 1641*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t2101_m31242_gshared/* 1642*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t2101_m31243_gshared/* 1643*/,
	(methodPointerType)&Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t2101_m31244_gshared/* 1644*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t2101_m31245_gshared/* 1645*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t2101_m31246_gshared/* 1646*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t2100_m31247_gshared/* 1647*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t2100_m31248_gshared/* 1648*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t2100_m31249_gshared/* 1649*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t2100_m31250_gshared/* 1650*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t2100_m31251_gshared/* 1651*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t2100_m31252_gshared/* 1652*/,
	(methodPointerType)&Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t2100_m31253_gshared/* 1653*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t2100_m31254_gshared/* 1654*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t2100_m31255_gshared/* 1655*/,
	(methodPointerType)&Array_Resize_TisCustomAttributeTypedArgument_t2101_m31256_gshared/* 1656*/,
	(methodPointerType)&Array_Resize_TisCustomAttributeTypedArgument_t2101_m31257_gshared/* 1657*/,
	(methodPointerType)&Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31258_gshared/* 1658*/,
	(methodPointerType)&Array_Sort_TisCustomAttributeTypedArgument_t2101_m31259_gshared/* 1659*/,
	(methodPointerType)&Array_Sort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31260_gshared/* 1660*/,
	(methodPointerType)&Array_get_swapper_TisCustomAttributeTypedArgument_t2101_m31261_gshared/* 1661*/,
	(methodPointerType)&Array_qsort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31262_gshared/* 1662*/,
	(methodPointerType)&Array_compare_TisCustomAttributeTypedArgument_t2101_m31263_gshared/* 1663*/,
	(methodPointerType)&Array_swap_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31264_gshared/* 1664*/,
	(methodPointerType)&Array_Sort_TisCustomAttributeTypedArgument_t2101_m31265_gshared/* 1665*/,
	(methodPointerType)&Array_qsort_TisCustomAttributeTypedArgument_t2101_m31266_gshared/* 1666*/,
	(methodPointerType)&Array_swap_TisCustomAttributeTypedArgument_t2101_m31267_gshared/* 1667*/,
	(methodPointerType)&Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31268_gshared/* 1668*/,
	(methodPointerType)&Array_Resize_TisCustomAttributeNamedArgument_t2100_m31269_gshared/* 1669*/,
	(methodPointerType)&Array_Resize_TisCustomAttributeNamedArgument_t2100_m31270_gshared/* 1670*/,
	(methodPointerType)&Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31271_gshared/* 1671*/,
	(methodPointerType)&Array_Sort_TisCustomAttributeNamedArgument_t2100_m31272_gshared/* 1672*/,
	(methodPointerType)&Array_Sort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31273_gshared/* 1673*/,
	(methodPointerType)&Array_get_swapper_TisCustomAttributeNamedArgument_t2100_m31274_gshared/* 1674*/,
	(methodPointerType)&Array_qsort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31275_gshared/* 1675*/,
	(methodPointerType)&Array_compare_TisCustomAttributeNamedArgument_t2100_m31276_gshared/* 1676*/,
	(methodPointerType)&Array_swap_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31277_gshared/* 1677*/,
	(methodPointerType)&Array_Sort_TisCustomAttributeNamedArgument_t2100_m31278_gshared/* 1678*/,
	(methodPointerType)&Array_qsort_TisCustomAttributeNamedArgument_t2100_m31279_gshared/* 1679*/,
	(methodPointerType)&Array_swap_TisCustomAttributeNamedArgument_t2100_m31280_gshared/* 1680*/,
	(methodPointerType)&Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31281_gshared/* 1681*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisResourceInfo_t2130_m31285_gshared/* 1682*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisResourceInfo_t2130_m31286_gshared/* 1683*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisResourceInfo_t2130_m31287_gshared/* 1684*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t2130_m31288_gshared/* 1685*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisResourceInfo_t2130_m31289_gshared/* 1686*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisResourceInfo_t2130_m31290_gshared/* 1687*/,
	(methodPointerType)&Array_InternalArray__Insert_TisResourceInfo_t2130_m31291_gshared/* 1688*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisResourceInfo_t2130_m31292_gshared/* 1689*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t2130_m31293_gshared/* 1690*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisResourceCacheItem_t2131_m31294_gshared/* 1691*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisResourceCacheItem_t2131_m31295_gshared/* 1692*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t2131_m31296_gshared/* 1693*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t2131_m31297_gshared/* 1694*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t2131_m31298_gshared/* 1695*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisResourceCacheItem_t2131_m31299_gshared/* 1696*/,
	(methodPointerType)&Array_InternalArray__Insert_TisResourceCacheItem_t2131_m31300_gshared/* 1697*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisResourceCacheItem_t2131_m31301_gshared/* 1698*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t2131_m31302_gshared/* 1699*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDateTime_t220_m31303_gshared/* 1700*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDateTime_t220_m31304_gshared/* 1701*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDateTime_t220_m31305_gshared/* 1702*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDateTime_t220_m31306_gshared/* 1703*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDateTime_t220_m31307_gshared/* 1704*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDateTime_t220_m31308_gshared/* 1705*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDateTime_t220_m31309_gshared/* 1706*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDateTime_t220_m31310_gshared/* 1707*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t220_m31311_gshared/* 1708*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDecimal_t564_m31312_gshared/* 1709*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDecimal_t564_m31313_gshared/* 1710*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDecimal_t564_m31314_gshared/* 1711*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDecimal_t564_m31315_gshared/* 1712*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDecimal_t564_m31316_gshared/* 1713*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDecimal_t564_m31317_gshared/* 1714*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDecimal_t564_m31318_gshared/* 1715*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDecimal_t564_m31319_gshared/* 1716*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t564_m31320_gshared/* 1717*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTimeSpan_t565_m31321_gshared/* 1718*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTimeSpan_t565_m31322_gshared/* 1719*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTimeSpan_t565_m31323_gshared/* 1720*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t565_m31324_gshared/* 1721*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTimeSpan_t565_m31325_gshared/* 1722*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTimeSpan_t565_m31326_gshared/* 1723*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTimeSpan_t565_m31327_gshared/* 1724*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTimeSpan_t565_m31328_gshared/* 1725*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t565_m31329_gshared/* 1726*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTypeTag_t2267_m31330_gshared/* 1727*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTypeTag_t2267_m31331_gshared/* 1728*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTypeTag_t2267_m31332_gshared/* 1729*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTypeTag_t2267_m31333_gshared/* 1730*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTypeTag_t2267_m31334_gshared/* 1731*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTypeTag_t2267_m31335_gshared/* 1732*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTypeTag_t2267_m31336_gshared/* 1733*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTypeTag_t2267_m31337_gshared/* 1734*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t2267_m31338_gshared/* 1735*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15645_gshared/* 1736*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15646_gshared/* 1737*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15647_gshared/* 1738*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15648_gshared/* 1739*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15649_gshared/* 1740*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15650_gshared/* 1741*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15651_gshared/* 1742*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15652_gshared/* 1743*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15653_gshared/* 1744*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15654_gshared/* 1745*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15655_gshared/* 1746*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15656_gshared/* 1747*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15657_gshared/* 1748*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15658_gshared/* 1749*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15659_gshared/* 1750*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15660_gshared/* 1751*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15661_gshared/* 1752*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15662_gshared/* 1753*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15710_gshared/* 1754*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15711_gshared/* 1755*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15712_gshared/* 1756*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15713_gshared/* 1757*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15714_gshared/* 1758*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15715_gshared/* 1759*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15716_gshared/* 1760*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15717_gshared/* 1761*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15718_gshared/* 1762*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15719_gshared/* 1763*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15720_gshared/* 1764*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15721_gshared/* 1765*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15722_gshared/* 1766*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15723_gshared/* 1767*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15724_gshared/* 1768*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15725_gshared/* 1769*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15726_gshared/* 1770*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15727_gshared/* 1771*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15728_gshared/* 1772*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15729_gshared/* 1773*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15730_gshared/* 1774*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15731_gshared/* 1775*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15732_gshared/* 1776*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15733_gshared/* 1777*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15734_gshared/* 1778*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15735_gshared/* 1779*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15736_gshared/* 1780*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15737_gshared/* 1781*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15738_gshared/* 1782*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15739_gshared/* 1783*/,
	(methodPointerType)&Dictionary_2__ctor_m15742_gshared/* 1784*/,
	(methodPointerType)&Dictionary_2__ctor_m15745_gshared/* 1785*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m15747_gshared/* 1786*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m15749_gshared/* 1787*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m15751_gshared/* 1788*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m15753_gshared/* 1789*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m15755_gshared/* 1790*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m15757_gshared/* 1791*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15759_gshared/* 1792*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15761_gshared/* 1793*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15763_gshared/* 1794*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15765_gshared/* 1795*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15767_gshared/* 1796*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15769_gshared/* 1797*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15771_gshared/* 1798*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m15773_gshared/* 1799*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15775_gshared/* 1800*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15777_gshared/* 1801*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15779_gshared/* 1802*/,
	(methodPointerType)&Dictionary_2_get_Count_m15781_gshared/* 1803*/,
	(methodPointerType)&Dictionary_2_get_Item_m15783_gshared/* 1804*/,
	(methodPointerType)&Dictionary_2_set_Item_m15785_gshared/* 1805*/,
	(methodPointerType)&Dictionary_2_Init_m15787_gshared/* 1806*/,
	(methodPointerType)&Dictionary_2_InitArrays_m15789_gshared/* 1807*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m15791_gshared/* 1808*/,
	(methodPointerType)&Dictionary_2_make_pair_m15793_gshared/* 1809*/,
	(methodPointerType)&Dictionary_2_pick_key_m15795_gshared/* 1810*/,
	(methodPointerType)&Dictionary_2_pick_value_m15797_gshared/* 1811*/,
	(methodPointerType)&Dictionary_2_CopyTo_m15799_gshared/* 1812*/,
	(methodPointerType)&Dictionary_2_Resize_m15801_gshared/* 1813*/,
	(methodPointerType)&Dictionary_2_Add_m15803_gshared/* 1814*/,
	(methodPointerType)&Dictionary_2_Clear_m15805_gshared/* 1815*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m15807_gshared/* 1816*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m15809_gshared/* 1817*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m15811_gshared/* 1818*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m15813_gshared/* 1819*/,
	(methodPointerType)&Dictionary_2_Remove_m15815_gshared/* 1820*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m15817_gshared/* 1821*/,
	(methodPointerType)&Dictionary_2_get_Keys_m15819_gshared/* 1822*/,
	(methodPointerType)&Dictionary_2_get_Values_m15821_gshared/* 1823*/,
	(methodPointerType)&Dictionary_2_ToTKey_m15823_gshared/* 1824*/,
	(methodPointerType)&Dictionary_2_ToTValue_m15825_gshared/* 1825*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m15827_gshared/* 1826*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m15829_gshared/* 1827*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m15831_gshared/* 1828*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15832_gshared/* 1829*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15833_gshared/* 1830*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15834_gshared/* 1831*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15835_gshared/* 1832*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15836_gshared/* 1833*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15837_gshared/* 1834*/,
	(methodPointerType)&KeyValuePair_2__ctor_m15838_gshared/* 1835*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m15839_gshared/* 1836*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m15840_gshared/* 1837*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m15841_gshared/* 1838*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m15842_gshared/* 1839*/,
	(methodPointerType)&KeyValuePair_2_ToString_m15843_gshared/* 1840*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15850_gshared/* 1841*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15851_gshared/* 1842*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15852_gshared/* 1843*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15853_gshared/* 1844*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15854_gshared/* 1845*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15855_gshared/* 1846*/,
	(methodPointerType)&KeyCollection__ctor_m15856_gshared/* 1847*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15857_gshared/* 1848*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15858_gshared/* 1849*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15859_gshared/* 1850*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15860_gshared/* 1851*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15861_gshared/* 1852*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m15862_gshared/* 1853*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15863_gshared/* 1854*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15864_gshared/* 1855*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15865_gshared/* 1856*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m15866_gshared/* 1857*/,
	(methodPointerType)&KeyCollection_CopyTo_m15867_gshared/* 1858*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m15868_gshared/* 1859*/,
	(methodPointerType)&KeyCollection_get_Count_m15869_gshared/* 1860*/,
	(methodPointerType)&Enumerator__ctor_m15870_gshared/* 1861*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15871_gshared/* 1862*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m15872_gshared/* 1863*/,
	(methodPointerType)&Enumerator_Dispose_m15873_gshared/* 1864*/,
	(methodPointerType)&Enumerator_MoveNext_m15874_gshared/* 1865*/,
	(methodPointerType)&Enumerator_get_Current_m15875_gshared/* 1866*/,
	(methodPointerType)&Enumerator__ctor_m15876_gshared/* 1867*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15877_gshared/* 1868*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m15878_gshared/* 1869*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15879_gshared/* 1870*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15880_gshared/* 1871*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15881_gshared/* 1872*/,
	(methodPointerType)&Enumerator_MoveNext_m15882_gshared/* 1873*/,
	(methodPointerType)&Enumerator_get_Current_m15883_gshared/* 1874*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m15884_gshared/* 1875*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m15885_gshared/* 1876*/,
	(methodPointerType)&Enumerator_Reset_m15886_gshared/* 1877*/,
	(methodPointerType)&Enumerator_VerifyState_m15887_gshared/* 1878*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m15888_gshared/* 1879*/,
	(methodPointerType)&Enumerator_Dispose_m15889_gshared/* 1880*/,
	(methodPointerType)&Transform_1__ctor_m15890_gshared/* 1881*/,
	(methodPointerType)&Transform_1_Invoke_m15891_gshared/* 1882*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15892_gshared/* 1883*/,
	(methodPointerType)&Transform_1_EndInvoke_m15893_gshared/* 1884*/,
	(methodPointerType)&ValueCollection__ctor_m15894_gshared/* 1885*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15895_gshared/* 1886*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15896_gshared/* 1887*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15897_gshared/* 1888*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15898_gshared/* 1889*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15899_gshared/* 1890*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m15900_gshared/* 1891*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15901_gshared/* 1892*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15902_gshared/* 1893*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15903_gshared/* 1894*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m15904_gshared/* 1895*/,
	(methodPointerType)&ValueCollection_CopyTo_m15905_gshared/* 1896*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m15906_gshared/* 1897*/,
	(methodPointerType)&ValueCollection_get_Count_m15907_gshared/* 1898*/,
	(methodPointerType)&Enumerator__ctor_m15908_gshared/* 1899*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15909_gshared/* 1900*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m15910_gshared/* 1901*/,
	(methodPointerType)&Enumerator_Dispose_m15911_gshared/* 1902*/,
	(methodPointerType)&Enumerator_MoveNext_m15912_gshared/* 1903*/,
	(methodPointerType)&Enumerator_get_Current_m15913_gshared/* 1904*/,
	(methodPointerType)&Transform_1__ctor_m15914_gshared/* 1905*/,
	(methodPointerType)&Transform_1_Invoke_m15915_gshared/* 1906*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15916_gshared/* 1907*/,
	(methodPointerType)&Transform_1_EndInvoke_m15917_gshared/* 1908*/,
	(methodPointerType)&Transform_1__ctor_m15918_gshared/* 1909*/,
	(methodPointerType)&Transform_1_Invoke_m15919_gshared/* 1910*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15920_gshared/* 1911*/,
	(methodPointerType)&Transform_1_EndInvoke_m15921_gshared/* 1912*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15922_gshared/* 1913*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15923_gshared/* 1914*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_gshared/* 1915*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15925_gshared/* 1916*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15926_gshared/* 1917*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15927_gshared/* 1918*/,
	(methodPointerType)&Transform_1__ctor_m15928_gshared/* 1919*/,
	(methodPointerType)&Transform_1_Invoke_m15929_gshared/* 1920*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15930_gshared/* 1921*/,
	(methodPointerType)&Transform_1_EndInvoke_m15931_gshared/* 1922*/,
	(methodPointerType)&ShimEnumerator__ctor_m15932_gshared/* 1923*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m15933_gshared/* 1924*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m15934_gshared/* 1925*/,
	(methodPointerType)&ShimEnumerator_get_Key_m15935_gshared/* 1926*/,
	(methodPointerType)&ShimEnumerator_get_Value_m15936_gshared/* 1927*/,
	(methodPointerType)&ShimEnumerator_get_Current_m15937_gshared/* 1928*/,
	(methodPointerType)&ShimEnumerator_Reset_m15938_gshared/* 1929*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15939_gshared/* 1930*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15940_gshared/* 1931*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15941_gshared/* 1932*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15942_gshared/* 1933*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15943_gshared/* 1934*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m15944_gshared/* 1935*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m15945_gshared/* 1936*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m15946_gshared/* 1937*/,
	(methodPointerType)&DefaultComparer__ctor_m15947_gshared/* 1938*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15948_gshared/* 1939*/,
	(methodPointerType)&DefaultComparer_Equals_m15949_gshared/* 1940*/,
	(methodPointerType)&Nullable_1__ctor_m16002_gshared/* 1941*/,
	(methodPointerType)&Nullable_1_Equals_m16003_gshared/* 1942*/,
	(methodPointerType)&Nullable_1_Equals_m16004_gshared/* 1943*/,
	(methodPointerType)&Nullable_1_GetHashCode_m16005_gshared/* 1944*/,
	(methodPointerType)&Nullable_1_GetValueOrDefault_m16006_gshared/* 1945*/,
	(methodPointerType)&Nullable_1_ToString_m16007_gshared/* 1946*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16113_gshared/* 1947*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16114_gshared/* 1948*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16115_gshared/* 1949*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16116_gshared/* 1950*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16117_gshared/* 1951*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16118_gshared/* 1952*/,
	(methodPointerType)&Transform_1__ctor_m16183_gshared/* 1953*/,
	(methodPointerType)&Transform_1_Invoke_m16184_gshared/* 1954*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16185_gshared/* 1955*/,
	(methodPointerType)&Transform_1_EndInvoke_m16186_gshared/* 1956*/,
	(methodPointerType)&Transform_1__ctor_m16187_gshared/* 1957*/,
	(methodPointerType)&Transform_1_Invoke_m16188_gshared/* 1958*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16189_gshared/* 1959*/,
	(methodPointerType)&Transform_1_EndInvoke_m16190_gshared/* 1960*/,
	(methodPointerType)&Dictionary_2__ctor_m16251_gshared/* 1961*/,
	(methodPointerType)&Dictionary_2__ctor_m16253_gshared/* 1962*/,
	(methodPointerType)&Dictionary_2__ctor_m16255_gshared/* 1963*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m16257_gshared/* 1964*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16259_gshared/* 1965*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16261_gshared/* 1966*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16263_gshared/* 1967*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16265_gshared/* 1968*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16267_gshared/* 1969*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16269_gshared/* 1970*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16271_gshared/* 1971*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16273_gshared/* 1972*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16275_gshared/* 1973*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16277_gshared/* 1974*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16279_gshared/* 1975*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16281_gshared/* 1976*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16283_gshared/* 1977*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16285_gshared/* 1978*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16287_gshared/* 1979*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16289_gshared/* 1980*/,
	(methodPointerType)&Dictionary_2_get_Count_m16291_gshared/* 1981*/,
	(methodPointerType)&Dictionary_2_get_Item_m16293_gshared/* 1982*/,
	(methodPointerType)&Dictionary_2_set_Item_m16295_gshared/* 1983*/,
	(methodPointerType)&Dictionary_2_Init_m16297_gshared/* 1984*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16299_gshared/* 1985*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16301_gshared/* 1986*/,
	(methodPointerType)&Dictionary_2_make_pair_m16303_gshared/* 1987*/,
	(methodPointerType)&Dictionary_2_pick_key_m16305_gshared/* 1988*/,
	(methodPointerType)&Dictionary_2_pick_value_m16307_gshared/* 1989*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16309_gshared/* 1990*/,
	(methodPointerType)&Dictionary_2_Resize_m16311_gshared/* 1991*/,
	(methodPointerType)&Dictionary_2_Add_m16313_gshared/* 1992*/,
	(methodPointerType)&Dictionary_2_Clear_m16315_gshared/* 1993*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16317_gshared/* 1994*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16321_gshared/* 1995*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16323_gshared/* 1996*/,
	(methodPointerType)&Dictionary_2_Remove_m16325_gshared/* 1997*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16327_gshared/* 1998*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16333_gshared/* 1999*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16335_gshared/* 2000*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16337_gshared/* 2001*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16341_gshared/* 2002*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16387_gshared/* 2003*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16388_gshared/* 2004*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16389_gshared/* 2005*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16390_gshared/* 2006*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16391_gshared/* 2007*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16392_gshared/* 2008*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16393_gshared/* 2009*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16395_gshared/* 2010*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16397_gshared/* 2011*/,
	(methodPointerType)&KeyCollection__ctor_m16399_gshared/* 2012*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16400_gshared/* 2013*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16401_gshared/* 2014*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16402_gshared/* 2015*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16403_gshared/* 2016*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16404_gshared/* 2017*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16405_gshared/* 2018*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16406_gshared/* 2019*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16407_gshared/* 2020*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16408_gshared/* 2021*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16409_gshared/* 2022*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16411_gshared/* 2023*/,
	(methodPointerType)&KeyCollection_get_Count_m16412_gshared/* 2024*/,
	(methodPointerType)&Enumerator__ctor_m16413_gshared/* 2025*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16414_gshared/* 2026*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m16415_gshared/* 2027*/,
	(methodPointerType)&Enumerator_Dispose_m16416_gshared/* 2028*/,
	(methodPointerType)&Enumerator_MoveNext_m16417_gshared/* 2029*/,
	(methodPointerType)&Enumerator_get_Current_m16418_gshared/* 2030*/,
	(methodPointerType)&Enumerator__ctor_m16419_gshared/* 2031*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16420_gshared/* 2032*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m16421_gshared/* 2033*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16422_gshared/* 2034*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16423_gshared/* 2035*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16424_gshared/* 2036*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16427_gshared/* 2037*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16428_gshared/* 2038*/,
	(methodPointerType)&Enumerator_Reset_m16429_gshared/* 2039*/,
	(methodPointerType)&Enumerator_VerifyState_m16430_gshared/* 2040*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16431_gshared/* 2041*/,
	(methodPointerType)&Transform_1__ctor_m16433_gshared/* 2042*/,
	(methodPointerType)&Transform_1_Invoke_m16434_gshared/* 2043*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16435_gshared/* 2044*/,
	(methodPointerType)&Transform_1_EndInvoke_m16436_gshared/* 2045*/,
	(methodPointerType)&ValueCollection__ctor_m16437_gshared/* 2046*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16438_gshared/* 2047*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16439_gshared/* 2048*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16440_gshared/* 2049*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16441_gshared/* 2050*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16442_gshared/* 2051*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16443_gshared/* 2052*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16444_gshared/* 2053*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16445_gshared/* 2054*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16446_gshared/* 2055*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16447_gshared/* 2056*/,
	(methodPointerType)&ValueCollection_CopyTo_m16448_gshared/* 2057*/,
	(methodPointerType)&ValueCollection_get_Count_m16450_gshared/* 2058*/,
	(methodPointerType)&Enumerator__ctor_m16451_gshared/* 2059*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16452_gshared/* 2060*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m16453_gshared/* 2061*/,
	(methodPointerType)&Transform_1__ctor_m16457_gshared/* 2062*/,
	(methodPointerType)&Transform_1_Invoke_m16458_gshared/* 2063*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16459_gshared/* 2064*/,
	(methodPointerType)&Transform_1_EndInvoke_m16460_gshared/* 2065*/,
	(methodPointerType)&Transform_1__ctor_m16461_gshared/* 2066*/,
	(methodPointerType)&Transform_1_Invoke_m16462_gshared/* 2067*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16463_gshared/* 2068*/,
	(methodPointerType)&Transform_1_EndInvoke_m16464_gshared/* 2069*/,
	(methodPointerType)&Transform_1__ctor_m16465_gshared/* 2070*/,
	(methodPointerType)&Transform_1_Invoke_m16466_gshared/* 2071*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16467_gshared/* 2072*/,
	(methodPointerType)&Transform_1_EndInvoke_m16468_gshared/* 2073*/,
	(methodPointerType)&ShimEnumerator__ctor_m16469_gshared/* 2074*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16470_gshared/* 2075*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16471_gshared/* 2076*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16472_gshared/* 2077*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16473_gshared/* 2078*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16474_gshared/* 2079*/,
	(methodPointerType)&ShimEnumerator_Reset_m16475_gshared/* 2080*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17172_gshared/* 2081*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17173_gshared/* 2082*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17174_gshared/* 2083*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17175_gshared/* 2084*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17176_gshared/* 2085*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17177_gshared/* 2086*/,
	(methodPointerType)&List_1__ctor_m17178_gshared/* 2087*/,
	(methodPointerType)&List_1__ctor_m17179_gshared/* 2088*/,
	(methodPointerType)&List_1__cctor_m17180_gshared/* 2089*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17181_gshared/* 2090*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m17182_gshared/* 2091*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m17183_gshared/* 2092*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m17184_gshared/* 2093*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m17185_gshared/* 2094*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m17186_gshared/* 2095*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m17187_gshared/* 2096*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m17188_gshared/* 2097*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17189_gshared/* 2098*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m17190_gshared/* 2099*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m17191_gshared/* 2100*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m17192_gshared/* 2101*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m17193_gshared/* 2102*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m17194_gshared/* 2103*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m17195_gshared/* 2104*/,
	(methodPointerType)&List_1_Add_m17196_gshared/* 2105*/,
	(methodPointerType)&List_1_GrowIfNeeded_m17197_gshared/* 2106*/,
	(methodPointerType)&List_1_AddCollection_m17198_gshared/* 2107*/,
	(methodPointerType)&List_1_AddEnumerable_m17199_gshared/* 2108*/,
	(methodPointerType)&List_1_AddRange_m17200_gshared/* 2109*/,
	(methodPointerType)&List_1_AsReadOnly_m17201_gshared/* 2110*/,
	(methodPointerType)&List_1_Clear_m17202_gshared/* 2111*/,
	(methodPointerType)&List_1_Contains_m17203_gshared/* 2112*/,
	(methodPointerType)&List_1_CopyTo_m17204_gshared/* 2113*/,
	(methodPointerType)&List_1_Find_m17205_gshared/* 2114*/,
	(methodPointerType)&List_1_CheckMatch_m17206_gshared/* 2115*/,
	(methodPointerType)&List_1_GetIndex_m17207_gshared/* 2116*/,
	(methodPointerType)&List_1_GetEnumerator_m17208_gshared/* 2117*/,
	(methodPointerType)&List_1_IndexOf_m17209_gshared/* 2118*/,
	(methodPointerType)&List_1_Shift_m17210_gshared/* 2119*/,
	(methodPointerType)&List_1_CheckIndex_m17211_gshared/* 2120*/,
	(methodPointerType)&List_1_Insert_m17212_gshared/* 2121*/,
	(methodPointerType)&List_1_CheckCollection_m17213_gshared/* 2122*/,
	(methodPointerType)&List_1_Remove_m17214_gshared/* 2123*/,
	(methodPointerType)&List_1_RemoveAll_m17215_gshared/* 2124*/,
	(methodPointerType)&List_1_RemoveAt_m17216_gshared/* 2125*/,
	(methodPointerType)&List_1_Reverse_m17217_gshared/* 2126*/,
	(methodPointerType)&List_1_Sort_m17218_gshared/* 2127*/,
	(methodPointerType)&List_1_Sort_m17219_gshared/* 2128*/,
	(methodPointerType)&List_1_TrimExcess_m17220_gshared/* 2129*/,
	(methodPointerType)&List_1_get_Capacity_m17221_gshared/* 2130*/,
	(methodPointerType)&List_1_set_Capacity_m17222_gshared/* 2131*/,
	(methodPointerType)&List_1_get_Count_m17223_gshared/* 2132*/,
	(methodPointerType)&List_1_get_Item_m17224_gshared/* 2133*/,
	(methodPointerType)&List_1_set_Item_m17225_gshared/* 2134*/,
	(methodPointerType)&Enumerator__ctor_m17226_gshared/* 2135*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m17227_gshared/* 2136*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17228_gshared/* 2137*/,
	(methodPointerType)&Enumerator_Dispose_m17229_gshared/* 2138*/,
	(methodPointerType)&Enumerator_VerifyState_m17230_gshared/* 2139*/,
	(methodPointerType)&Enumerator_MoveNext_m17231_gshared/* 2140*/,
	(methodPointerType)&Enumerator_get_Current_m17232_gshared/* 2141*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m17233_gshared/* 2142*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17234_gshared/* 2143*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17235_gshared/* 2144*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17236_gshared/* 2145*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17237_gshared/* 2146*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17238_gshared/* 2147*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17239_gshared/* 2148*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17240_gshared/* 2149*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17241_gshared/* 2150*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17242_gshared/* 2151*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17243_gshared/* 2152*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m17244_gshared/* 2153*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m17245_gshared/* 2154*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m17246_gshared/* 2155*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17247_gshared/* 2156*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m17248_gshared/* 2157*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m17249_gshared/* 2158*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17250_gshared/* 2159*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17251_gshared/* 2160*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17252_gshared/* 2161*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17253_gshared/* 2162*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17254_gshared/* 2163*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m17255_gshared/* 2164*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m17256_gshared/* 2165*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m17257_gshared/* 2166*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m17258_gshared/* 2167*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m17259_gshared/* 2168*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m17260_gshared/* 2169*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m17261_gshared/* 2170*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m17262_gshared/* 2171*/,
	(methodPointerType)&Collection_1__ctor_m17263_gshared/* 2172*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17264_gshared/* 2173*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m17265_gshared/* 2174*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m17266_gshared/* 2175*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m17267_gshared/* 2176*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m17268_gshared/* 2177*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m17269_gshared/* 2178*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m17270_gshared/* 2179*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m17271_gshared/* 2180*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m17272_gshared/* 2181*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m17273_gshared/* 2182*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m17274_gshared/* 2183*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m17275_gshared/* 2184*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m17276_gshared/* 2185*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m17277_gshared/* 2186*/,
	(methodPointerType)&Collection_1_Add_m17278_gshared/* 2187*/,
	(methodPointerType)&Collection_1_Clear_m17279_gshared/* 2188*/,
	(methodPointerType)&Collection_1_ClearItems_m17280_gshared/* 2189*/,
	(methodPointerType)&Collection_1_Contains_m17281_gshared/* 2190*/,
	(methodPointerType)&Collection_1_CopyTo_m17282_gshared/* 2191*/,
	(methodPointerType)&Collection_1_GetEnumerator_m17283_gshared/* 2192*/,
	(methodPointerType)&Collection_1_IndexOf_m17284_gshared/* 2193*/,
	(methodPointerType)&Collection_1_Insert_m17285_gshared/* 2194*/,
	(methodPointerType)&Collection_1_InsertItem_m17286_gshared/* 2195*/,
	(methodPointerType)&Collection_1_Remove_m17287_gshared/* 2196*/,
	(methodPointerType)&Collection_1_RemoveAt_m17288_gshared/* 2197*/,
	(methodPointerType)&Collection_1_RemoveItem_m17289_gshared/* 2198*/,
	(methodPointerType)&Collection_1_get_Count_m17290_gshared/* 2199*/,
	(methodPointerType)&Collection_1_get_Item_m17291_gshared/* 2200*/,
	(methodPointerType)&Collection_1_set_Item_m17292_gshared/* 2201*/,
	(methodPointerType)&Collection_1_SetItem_m17293_gshared/* 2202*/,
	(methodPointerType)&Collection_1_IsValidItem_m17294_gshared/* 2203*/,
	(methodPointerType)&Collection_1_ConvertItem_m17295_gshared/* 2204*/,
	(methodPointerType)&Collection_1_CheckWritable_m17296_gshared/* 2205*/,
	(methodPointerType)&Collection_1_IsSynchronized_m17297_gshared/* 2206*/,
	(methodPointerType)&Collection_1_IsFixedSize_m17298_gshared/* 2207*/,
	(methodPointerType)&EqualityComparer_1__ctor_m17299_gshared/* 2208*/,
	(methodPointerType)&EqualityComparer_1__cctor_m17300_gshared/* 2209*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17301_gshared/* 2210*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17302_gshared/* 2211*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m17303_gshared/* 2212*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m17304_gshared/* 2213*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m17305_gshared/* 2214*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m17306_gshared/* 2215*/,
	(methodPointerType)&DefaultComparer__ctor_m17307_gshared/* 2216*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m17308_gshared/* 2217*/,
	(methodPointerType)&DefaultComparer_Equals_m17309_gshared/* 2218*/,
	(methodPointerType)&Predicate_1__ctor_m17310_gshared/* 2219*/,
	(methodPointerType)&Predicate_1_Invoke_m17311_gshared/* 2220*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m17312_gshared/* 2221*/,
	(methodPointerType)&Predicate_1_EndInvoke_m17313_gshared/* 2222*/,
	(methodPointerType)&Comparer_1__ctor_m17314_gshared/* 2223*/,
	(methodPointerType)&Comparer_1__cctor_m17315_gshared/* 2224*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m17316_gshared/* 2225*/,
	(methodPointerType)&Comparer_1_get_Default_m17317_gshared/* 2226*/,
	(methodPointerType)&GenericComparer_1__ctor_m17318_gshared/* 2227*/,
	(methodPointerType)&GenericComparer_1_Compare_m17319_gshared/* 2228*/,
	(methodPointerType)&DefaultComparer__ctor_m17320_gshared/* 2229*/,
	(methodPointerType)&DefaultComparer_Compare_m17321_gshared/* 2230*/,
	(methodPointerType)&Comparison_1__ctor_m17322_gshared/* 2231*/,
	(methodPointerType)&Comparison_1_Invoke_m17323_gshared/* 2232*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m17324_gshared/* 2233*/,
	(methodPointerType)&Comparison_1_EndInvoke_m17325_gshared/* 2234*/,
	(methodPointerType)&Action_1_BeginInvoke_m17517_gshared/* 2235*/,
	(methodPointerType)&Action_1_EndInvoke_m17518_gshared/* 2236*/,
	(methodPointerType)&Action_1_BeginInvoke_m17638_gshared/* 2237*/,
	(methodPointerType)&Action_1_EndInvoke_m17639_gshared/* 2238*/,
	(methodPointerType)&Action_1_BeginInvoke_m17661_gshared/* 2239*/,
	(methodPointerType)&Action_1_EndInvoke_m17662_gshared/* 2240*/,
	(methodPointerType)&Nullable_1_get_Value_m18247_gshared/* 2241*/,
	(methodPointerType)&Nullable_1_Equals_m18248_gshared/* 2242*/,
	(methodPointerType)&Nullable_1_Equals_m18249_gshared/* 2243*/,
	(methodPointerType)&Nullable_1_GetHashCode_m18250_gshared/* 2244*/,
	(methodPointerType)&Nullable_1__ctor_m18251_gshared/* 2245*/,
	(methodPointerType)&Nullable_1_Equals_m18252_gshared/* 2246*/,
	(methodPointerType)&Nullable_1_Equals_m18253_gshared/* 2247*/,
	(methodPointerType)&Nullable_1_GetHashCode_m18254_gshared/* 2248*/,
	(methodPointerType)&Nullable_1_GetValueOrDefault_m18255_gshared/* 2249*/,
	(methodPointerType)&Nullable_1_ToString_m18256_gshared/* 2250*/,
	(methodPointerType)&Nullable_1__ctor_m18342_gshared/* 2251*/,
	(methodPointerType)&Nullable_1_Equals_m18343_gshared/* 2252*/,
	(methodPointerType)&Nullable_1_Equals_m18344_gshared/* 2253*/,
	(methodPointerType)&Nullable_1_GetHashCode_m18345_gshared/* 2254*/,
	(methodPointerType)&Nullable_1_GetValueOrDefault_m18346_gshared/* 2255*/,
	(methodPointerType)&Nullable_1_ToString_m18347_gshared/* 2256*/,
	(methodPointerType)&Nullable_1__ctor_m18512_gshared/* 2257*/,
	(methodPointerType)&Nullable_1_Equals_m18513_gshared/* 2258*/,
	(methodPointerType)&Nullable_1_Equals_m18514_gshared/* 2259*/,
	(methodPointerType)&Nullable_1_GetHashCode_m18515_gshared/* 2260*/,
	(methodPointerType)&Nullable_1_GetValueOrDefault_m18516_gshared/* 2261*/,
	(methodPointerType)&Nullable_1_ToString_m18517_gshared/* 2262*/,
	(methodPointerType)&Dictionary_2__ctor_m18520_gshared/* 2263*/,
	(methodPointerType)&Dictionary_2__ctor_m18522_gshared/* 2264*/,
	(methodPointerType)&Dictionary_2__ctor_m18524_gshared/* 2265*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m18526_gshared/* 2266*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m18528_gshared/* 2267*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m18530_gshared/* 2268*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m18532_gshared/* 2269*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m18534_gshared/* 2270*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m18536_gshared/* 2271*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18538_gshared/* 2272*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18540_gshared/* 2273*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18542_gshared/* 2274*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18544_gshared/* 2275*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18546_gshared/* 2276*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18548_gshared/* 2277*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18550_gshared/* 2278*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m18552_gshared/* 2279*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18554_gshared/* 2280*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18556_gshared/* 2281*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18558_gshared/* 2282*/,
	(methodPointerType)&Dictionary_2_get_Count_m18560_gshared/* 2283*/,
	(methodPointerType)&Dictionary_2_get_Item_m18562_gshared/* 2284*/,
	(methodPointerType)&Dictionary_2_set_Item_m18564_gshared/* 2285*/,
	(methodPointerType)&Dictionary_2_Init_m18566_gshared/* 2286*/,
	(methodPointerType)&Dictionary_2_InitArrays_m18568_gshared/* 2287*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m18570_gshared/* 2288*/,
	(methodPointerType)&Dictionary_2_make_pair_m18572_gshared/* 2289*/,
	(methodPointerType)&Dictionary_2_pick_key_m18574_gshared/* 2290*/,
	(methodPointerType)&Dictionary_2_pick_value_m18576_gshared/* 2291*/,
	(methodPointerType)&Dictionary_2_CopyTo_m18578_gshared/* 2292*/,
	(methodPointerType)&Dictionary_2_Resize_m18580_gshared/* 2293*/,
	(methodPointerType)&Dictionary_2_Add_m18582_gshared/* 2294*/,
	(methodPointerType)&Dictionary_2_Clear_m18584_gshared/* 2295*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m18586_gshared/* 2296*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m18588_gshared/* 2297*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m18590_gshared/* 2298*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m18592_gshared/* 2299*/,
	(methodPointerType)&Dictionary_2_Remove_m18594_gshared/* 2300*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m18596_gshared/* 2301*/,
	(methodPointerType)&Dictionary_2_get_Keys_m18598_gshared/* 2302*/,
	(methodPointerType)&Dictionary_2_get_Values_m18600_gshared/* 2303*/,
	(methodPointerType)&Dictionary_2_ToTKey_m18602_gshared/* 2304*/,
	(methodPointerType)&Dictionary_2_ToTValue_m18604_gshared/* 2305*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m18606_gshared/* 2306*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m18609_gshared/* 2307*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18610_gshared/* 2308*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18611_gshared/* 2309*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18612_gshared/* 2310*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18613_gshared/* 2311*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18614_gshared/* 2312*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18615_gshared/* 2313*/,
	(methodPointerType)&KeyValuePair_2__ctor_m18616_gshared/* 2314*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m18618_gshared/* 2315*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m18620_gshared/* 2316*/,
	(methodPointerType)&KeyValuePair_2_ToString_m18621_gshared/* 2317*/,
	(methodPointerType)&KeyCollection__ctor_m18622_gshared/* 2318*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18623_gshared/* 2319*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18624_gshared/* 2320*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18625_gshared/* 2321*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18626_gshared/* 2322*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18627_gshared/* 2323*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m18628_gshared/* 2324*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18629_gshared/* 2325*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18630_gshared/* 2326*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18631_gshared/* 2327*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m18632_gshared/* 2328*/,
	(methodPointerType)&KeyCollection_CopyTo_m18633_gshared/* 2329*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m18634_gshared/* 2330*/,
	(methodPointerType)&KeyCollection_get_Count_m18635_gshared/* 2331*/,
	(methodPointerType)&Enumerator__ctor_m18636_gshared/* 2332*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18637_gshared/* 2333*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m18638_gshared/* 2334*/,
	(methodPointerType)&Enumerator_Dispose_m18639_gshared/* 2335*/,
	(methodPointerType)&Enumerator_MoveNext_m18640_gshared/* 2336*/,
	(methodPointerType)&Enumerator_get_Current_m18641_gshared/* 2337*/,
	(methodPointerType)&Enumerator__ctor_m18642_gshared/* 2338*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18643_gshared/* 2339*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m18644_gshared/* 2340*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18645_gshared/* 2341*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18646_gshared/* 2342*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18647_gshared/* 2343*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m18650_gshared/* 2344*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m18651_gshared/* 2345*/,
	(methodPointerType)&Enumerator_Reset_m18652_gshared/* 2346*/,
	(methodPointerType)&Enumerator_VerifyState_m18653_gshared/* 2347*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m18654_gshared/* 2348*/,
	(methodPointerType)&Enumerator_Dispose_m18655_gshared/* 2349*/,
	(methodPointerType)&Transform_1__ctor_m18656_gshared/* 2350*/,
	(methodPointerType)&Transform_1_Invoke_m18657_gshared/* 2351*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18658_gshared/* 2352*/,
	(methodPointerType)&Transform_1_EndInvoke_m18659_gshared/* 2353*/,
	(methodPointerType)&ValueCollection__ctor_m18660_gshared/* 2354*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18661_gshared/* 2355*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18662_gshared/* 2356*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18663_gshared/* 2357*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18664_gshared/* 2358*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18665_gshared/* 2359*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m18666_gshared/* 2360*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18667_gshared/* 2361*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18668_gshared/* 2362*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18669_gshared/* 2363*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m18670_gshared/* 2364*/,
	(methodPointerType)&ValueCollection_CopyTo_m18671_gshared/* 2365*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m18672_gshared/* 2366*/,
	(methodPointerType)&ValueCollection_get_Count_m18673_gshared/* 2367*/,
	(methodPointerType)&Enumerator__ctor_m18674_gshared/* 2368*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18675_gshared/* 2369*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m18676_gshared/* 2370*/,
	(methodPointerType)&Enumerator_Dispose_m18677_gshared/* 2371*/,
	(methodPointerType)&Enumerator_MoveNext_m18678_gshared/* 2372*/,
	(methodPointerType)&Enumerator_get_Current_m18679_gshared/* 2373*/,
	(methodPointerType)&Transform_1__ctor_m18680_gshared/* 2374*/,
	(methodPointerType)&Transform_1_Invoke_m18681_gshared/* 2375*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18682_gshared/* 2376*/,
	(methodPointerType)&Transform_1_EndInvoke_m18683_gshared/* 2377*/,
	(methodPointerType)&Transform_1__ctor_m18684_gshared/* 2378*/,
	(methodPointerType)&Transform_1_Invoke_m18685_gshared/* 2379*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18686_gshared/* 2380*/,
	(methodPointerType)&Transform_1_EndInvoke_m18687_gshared/* 2381*/,
	(methodPointerType)&Transform_1__ctor_m18688_gshared/* 2382*/,
	(methodPointerType)&Transform_1_Invoke_m18689_gshared/* 2383*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18690_gshared/* 2384*/,
	(methodPointerType)&Transform_1_EndInvoke_m18691_gshared/* 2385*/,
	(methodPointerType)&ShimEnumerator__ctor_m18692_gshared/* 2386*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m18693_gshared/* 2387*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m18694_gshared/* 2388*/,
	(methodPointerType)&ShimEnumerator_get_Key_m18695_gshared/* 2389*/,
	(methodPointerType)&ShimEnumerator_get_Value_m18696_gshared/* 2390*/,
	(methodPointerType)&ShimEnumerator_get_Current_m18697_gshared/* 2391*/,
	(methodPointerType)&ShimEnumerator_Reset_m18698_gshared/* 2392*/,
	(methodPointerType)&EqualityComparer_1__ctor_m18699_gshared/* 2393*/,
	(methodPointerType)&EqualityComparer_1__cctor_m18700_gshared/* 2394*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18701_gshared/* 2395*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18702_gshared/* 2396*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m18703_gshared/* 2397*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m18704_gshared/* 2398*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m18705_gshared/* 2399*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m18706_gshared/* 2400*/,
	(methodPointerType)&DefaultComparer__ctor_m18707_gshared/* 2401*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m18708_gshared/* 2402*/,
	(methodPointerType)&DefaultComparer_Equals_m18709_gshared/* 2403*/,
	(methodPointerType)&Dictionary_2__ctor_m18862_gshared/* 2404*/,
	(methodPointerType)&Dictionary_2__ctor_m18863_gshared/* 2405*/,
	(methodPointerType)&Dictionary_2__ctor_m18864_gshared/* 2406*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m18865_gshared/* 2407*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m18866_gshared/* 2408*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m18867_gshared/* 2409*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m18868_gshared/* 2410*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m18869_gshared/* 2411*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m18870_gshared/* 2412*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18871_gshared/* 2413*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18872_gshared/* 2414*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18873_gshared/* 2415*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18874_gshared/* 2416*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18875_gshared/* 2417*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18876_gshared/* 2418*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18877_gshared/* 2419*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m18878_gshared/* 2420*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18879_gshared/* 2421*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18880_gshared/* 2422*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18881_gshared/* 2423*/,
	(methodPointerType)&Dictionary_2_get_Count_m18882_gshared/* 2424*/,
	(methodPointerType)&Dictionary_2_get_Item_m18883_gshared/* 2425*/,
	(methodPointerType)&Dictionary_2_set_Item_m18884_gshared/* 2426*/,
	(methodPointerType)&Dictionary_2_Init_m18885_gshared/* 2427*/,
	(methodPointerType)&Dictionary_2_InitArrays_m18886_gshared/* 2428*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m18887_gshared/* 2429*/,
	(methodPointerType)&Dictionary_2_make_pair_m18888_gshared/* 2430*/,
	(methodPointerType)&Dictionary_2_pick_key_m18889_gshared/* 2431*/,
	(methodPointerType)&Dictionary_2_pick_value_m18890_gshared/* 2432*/,
	(methodPointerType)&Dictionary_2_CopyTo_m18891_gshared/* 2433*/,
	(methodPointerType)&Dictionary_2_Resize_m18892_gshared/* 2434*/,
	(methodPointerType)&Dictionary_2_Add_m18893_gshared/* 2435*/,
	(methodPointerType)&Dictionary_2_Clear_m18894_gshared/* 2436*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m18895_gshared/* 2437*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m18896_gshared/* 2438*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m18897_gshared/* 2439*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m18898_gshared/* 2440*/,
	(methodPointerType)&Dictionary_2_Remove_m18899_gshared/* 2441*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m18900_gshared/* 2442*/,
	(methodPointerType)&Dictionary_2_get_Keys_m18901_gshared/* 2443*/,
	(methodPointerType)&Dictionary_2_get_Values_m18902_gshared/* 2444*/,
	(methodPointerType)&Dictionary_2_ToTKey_m18903_gshared/* 2445*/,
	(methodPointerType)&Dictionary_2_ToTValue_m18904_gshared/* 2446*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m18905_gshared/* 2447*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m18906_gshared/* 2448*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m18907_gshared/* 2449*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18908_gshared/* 2450*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18909_gshared/* 2451*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18910_gshared/* 2452*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18911_gshared/* 2453*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18912_gshared/* 2454*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18913_gshared/* 2455*/,
	(methodPointerType)&KeyValuePair_2__ctor_m18914_gshared/* 2456*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m18915_gshared/* 2457*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m18916_gshared/* 2458*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m18917_gshared/* 2459*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m18918_gshared/* 2460*/,
	(methodPointerType)&KeyValuePair_2_ToString_m18919_gshared/* 2461*/,
	(methodPointerType)&KeyCollection__ctor_m18920_gshared/* 2462*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18921_gshared/* 2463*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18922_gshared/* 2464*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18923_gshared/* 2465*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18924_gshared/* 2466*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18925_gshared/* 2467*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m18926_gshared/* 2468*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18927_gshared/* 2469*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18928_gshared/* 2470*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18929_gshared/* 2471*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m18930_gshared/* 2472*/,
	(methodPointerType)&KeyCollection_CopyTo_m18931_gshared/* 2473*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m18932_gshared/* 2474*/,
	(methodPointerType)&KeyCollection_get_Count_m18933_gshared/* 2475*/,
	(methodPointerType)&Enumerator__ctor_m18934_gshared/* 2476*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18935_gshared/* 2477*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m18936_gshared/* 2478*/,
	(methodPointerType)&Enumerator_Dispose_m18937_gshared/* 2479*/,
	(methodPointerType)&Enumerator_MoveNext_m18938_gshared/* 2480*/,
	(methodPointerType)&Enumerator_get_Current_m18939_gshared/* 2481*/,
	(methodPointerType)&Enumerator__ctor_m18940_gshared/* 2482*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18941_gshared/* 2483*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m18942_gshared/* 2484*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18943_gshared/* 2485*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18944_gshared/* 2486*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18945_gshared/* 2487*/,
	(methodPointerType)&Enumerator_MoveNext_m18946_gshared/* 2488*/,
	(methodPointerType)&Enumerator_get_Current_m18947_gshared/* 2489*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m18948_gshared/* 2490*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m18949_gshared/* 2491*/,
	(methodPointerType)&Enumerator_Reset_m18950_gshared/* 2492*/,
	(methodPointerType)&Enumerator_VerifyState_m18951_gshared/* 2493*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m18952_gshared/* 2494*/,
	(methodPointerType)&Enumerator_Dispose_m18953_gshared/* 2495*/,
	(methodPointerType)&Transform_1__ctor_m18954_gshared/* 2496*/,
	(methodPointerType)&Transform_1_Invoke_m18955_gshared/* 2497*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18956_gshared/* 2498*/,
	(methodPointerType)&Transform_1_EndInvoke_m18957_gshared/* 2499*/,
	(methodPointerType)&ValueCollection__ctor_m18958_gshared/* 2500*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18959_gshared/* 2501*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18960_gshared/* 2502*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18961_gshared/* 2503*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18962_gshared/* 2504*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18963_gshared/* 2505*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m18964_gshared/* 2506*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18965_gshared/* 2507*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18966_gshared/* 2508*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18967_gshared/* 2509*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m18968_gshared/* 2510*/,
	(methodPointerType)&ValueCollection_CopyTo_m18969_gshared/* 2511*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m18970_gshared/* 2512*/,
	(methodPointerType)&ValueCollection_get_Count_m18971_gshared/* 2513*/,
	(methodPointerType)&Enumerator__ctor_m18972_gshared/* 2514*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18973_gshared/* 2515*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m18974_gshared/* 2516*/,
	(methodPointerType)&Enumerator_Dispose_m18975_gshared/* 2517*/,
	(methodPointerType)&Enumerator_MoveNext_m18976_gshared/* 2518*/,
	(methodPointerType)&Enumerator_get_Current_m18977_gshared/* 2519*/,
	(methodPointerType)&Transform_1__ctor_m18978_gshared/* 2520*/,
	(methodPointerType)&Transform_1_Invoke_m18979_gshared/* 2521*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18980_gshared/* 2522*/,
	(methodPointerType)&Transform_1_EndInvoke_m18981_gshared/* 2523*/,
	(methodPointerType)&Transform_1__ctor_m18982_gshared/* 2524*/,
	(methodPointerType)&Transform_1_Invoke_m18983_gshared/* 2525*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18984_gshared/* 2526*/,
	(methodPointerType)&Transform_1_EndInvoke_m18985_gshared/* 2527*/,
	(methodPointerType)&ShimEnumerator__ctor_m18986_gshared/* 2528*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m18987_gshared/* 2529*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m18988_gshared/* 2530*/,
	(methodPointerType)&ShimEnumerator_get_Key_m18989_gshared/* 2531*/,
	(methodPointerType)&ShimEnumerator_get_Value_m18990_gshared/* 2532*/,
	(methodPointerType)&ShimEnumerator_get_Current_m18991_gshared/* 2533*/,
	(methodPointerType)&ShimEnumerator_Reset_m18992_gshared/* 2534*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19297_gshared/* 2535*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19298_gshared/* 2536*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19299_gshared/* 2537*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19300_gshared/* 2538*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19301_gshared/* 2539*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19302_gshared/* 2540*/,
	(methodPointerType)&Action_2_BeginInvoke_m19402_gshared/* 2541*/,
	(methodPointerType)&Action_2_EndInvoke_m19404_gshared/* 2542*/,
	(methodPointerType)&Action_1_BeginInvoke_m19405_gshared/* 2543*/,
	(methodPointerType)&Action_1_EndInvoke_m19406_gshared/* 2544*/,
	(methodPointerType)&Action_1_BeginInvoke_m19413_gshared/* 2545*/,
	(methodPointerType)&Action_1_EndInvoke_m19414_gshared/* 2546*/,
	(methodPointerType)&Comparison_1_Invoke_m19628_gshared/* 2547*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m19629_gshared/* 2548*/,
	(methodPointerType)&Comparison_1_EndInvoke_m19630_gshared/* 2549*/,
	(methodPointerType)&List_1__ctor_m19877_gshared/* 2550*/,
	(methodPointerType)&List_1__ctor_m19878_gshared/* 2551*/,
	(methodPointerType)&List_1__cctor_m19879_gshared/* 2552*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19880_gshared/* 2553*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m19881_gshared/* 2554*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m19882_gshared/* 2555*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m19883_gshared/* 2556*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m19884_gshared/* 2557*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m19885_gshared/* 2558*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m19886_gshared/* 2559*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m19887_gshared/* 2560*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19888_gshared/* 2561*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m19889_gshared/* 2562*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m19890_gshared/* 2563*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m19891_gshared/* 2564*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m19892_gshared/* 2565*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m19893_gshared/* 2566*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m19894_gshared/* 2567*/,
	(methodPointerType)&List_1_Add_m19895_gshared/* 2568*/,
	(methodPointerType)&List_1_GrowIfNeeded_m19896_gshared/* 2569*/,
	(methodPointerType)&List_1_AddCollection_m19897_gshared/* 2570*/,
	(methodPointerType)&List_1_AddEnumerable_m19898_gshared/* 2571*/,
	(methodPointerType)&List_1_AddRange_m19899_gshared/* 2572*/,
	(methodPointerType)&List_1_AsReadOnly_m19900_gshared/* 2573*/,
	(methodPointerType)&List_1_Clear_m19901_gshared/* 2574*/,
	(methodPointerType)&List_1_Contains_m19902_gshared/* 2575*/,
	(methodPointerType)&List_1_CopyTo_m19903_gshared/* 2576*/,
	(methodPointerType)&List_1_Find_m19904_gshared/* 2577*/,
	(methodPointerType)&List_1_CheckMatch_m19905_gshared/* 2578*/,
	(methodPointerType)&List_1_GetIndex_m19906_gshared/* 2579*/,
	(methodPointerType)&List_1_GetEnumerator_m19907_gshared/* 2580*/,
	(methodPointerType)&List_1_IndexOf_m19908_gshared/* 2581*/,
	(methodPointerType)&List_1_Shift_m19909_gshared/* 2582*/,
	(methodPointerType)&List_1_CheckIndex_m19910_gshared/* 2583*/,
	(methodPointerType)&List_1_Insert_m19911_gshared/* 2584*/,
	(methodPointerType)&List_1_CheckCollection_m19912_gshared/* 2585*/,
	(methodPointerType)&List_1_Remove_m19913_gshared/* 2586*/,
	(methodPointerType)&List_1_RemoveAll_m19914_gshared/* 2587*/,
	(methodPointerType)&List_1_RemoveAt_m19915_gshared/* 2588*/,
	(methodPointerType)&List_1_Reverse_m19916_gshared/* 2589*/,
	(methodPointerType)&List_1_Sort_m19917_gshared/* 2590*/,
	(methodPointerType)&List_1_ToArray_m19918_gshared/* 2591*/,
	(methodPointerType)&List_1_TrimExcess_m19919_gshared/* 2592*/,
	(methodPointerType)&List_1_get_Capacity_m19920_gshared/* 2593*/,
	(methodPointerType)&List_1_set_Capacity_m19921_gshared/* 2594*/,
	(methodPointerType)&List_1_get_Count_m19922_gshared/* 2595*/,
	(methodPointerType)&List_1_get_Item_m19923_gshared/* 2596*/,
	(methodPointerType)&List_1_set_Item_m19924_gshared/* 2597*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19925_gshared/* 2598*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19926_gshared/* 2599*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19927_gshared/* 2600*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19928_gshared/* 2601*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19929_gshared/* 2602*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19930_gshared/* 2603*/,
	(methodPointerType)&Enumerator__ctor_m19931_gshared/* 2604*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m19932_gshared/* 2605*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19933_gshared/* 2606*/,
	(methodPointerType)&Enumerator_Dispose_m19934_gshared/* 2607*/,
	(methodPointerType)&Enumerator_VerifyState_m19935_gshared/* 2608*/,
	(methodPointerType)&Enumerator_MoveNext_m19936_gshared/* 2609*/,
	(methodPointerType)&Enumerator_get_Current_m19937_gshared/* 2610*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m19938_gshared/* 2611*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19939_gshared/* 2612*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19940_gshared/* 2613*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19941_gshared/* 2614*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19942_gshared/* 2615*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19943_gshared/* 2616*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19944_gshared/* 2617*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19945_gshared/* 2618*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19946_gshared/* 2619*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19947_gshared/* 2620*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19948_gshared/* 2621*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m19949_gshared/* 2622*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m19950_gshared/* 2623*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m19951_gshared/* 2624*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19952_gshared/* 2625*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m19953_gshared/* 2626*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m19954_gshared/* 2627*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19955_gshared/* 2628*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19956_gshared/* 2629*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19957_gshared/* 2630*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19958_gshared/* 2631*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19959_gshared/* 2632*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m19960_gshared/* 2633*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m19961_gshared/* 2634*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m19962_gshared/* 2635*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m19963_gshared/* 2636*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m19964_gshared/* 2637*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m19965_gshared/* 2638*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m19966_gshared/* 2639*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m19967_gshared/* 2640*/,
	(methodPointerType)&Collection_1__ctor_m19968_gshared/* 2641*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19969_gshared/* 2642*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m19970_gshared/* 2643*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m19971_gshared/* 2644*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m19972_gshared/* 2645*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m19973_gshared/* 2646*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m19974_gshared/* 2647*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m19975_gshared/* 2648*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m19976_gshared/* 2649*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m19977_gshared/* 2650*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m19978_gshared/* 2651*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m19979_gshared/* 2652*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m19980_gshared/* 2653*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m19981_gshared/* 2654*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m19982_gshared/* 2655*/,
	(methodPointerType)&Collection_1_Add_m19983_gshared/* 2656*/,
	(methodPointerType)&Collection_1_Clear_m19984_gshared/* 2657*/,
	(methodPointerType)&Collection_1_ClearItems_m19985_gshared/* 2658*/,
	(methodPointerType)&Collection_1_Contains_m19986_gshared/* 2659*/,
	(methodPointerType)&Collection_1_CopyTo_m19987_gshared/* 2660*/,
	(methodPointerType)&Collection_1_GetEnumerator_m19988_gshared/* 2661*/,
	(methodPointerType)&Collection_1_IndexOf_m19989_gshared/* 2662*/,
	(methodPointerType)&Collection_1_Insert_m19990_gshared/* 2663*/,
	(methodPointerType)&Collection_1_InsertItem_m19991_gshared/* 2664*/,
	(methodPointerType)&Collection_1_Remove_m19992_gshared/* 2665*/,
	(methodPointerType)&Collection_1_RemoveAt_m19993_gshared/* 2666*/,
	(methodPointerType)&Collection_1_RemoveItem_m19994_gshared/* 2667*/,
	(methodPointerType)&Collection_1_get_Count_m19995_gshared/* 2668*/,
	(methodPointerType)&Collection_1_get_Item_m19996_gshared/* 2669*/,
	(methodPointerType)&Collection_1_set_Item_m19997_gshared/* 2670*/,
	(methodPointerType)&Collection_1_SetItem_m19998_gshared/* 2671*/,
	(methodPointerType)&Collection_1_IsValidItem_m19999_gshared/* 2672*/,
	(methodPointerType)&Collection_1_ConvertItem_m20000_gshared/* 2673*/,
	(methodPointerType)&Collection_1_CheckWritable_m20001_gshared/* 2674*/,
	(methodPointerType)&Collection_1_IsSynchronized_m20002_gshared/* 2675*/,
	(methodPointerType)&Collection_1_IsFixedSize_m20003_gshared/* 2676*/,
	(methodPointerType)&EqualityComparer_1__ctor_m20004_gshared/* 2677*/,
	(methodPointerType)&EqualityComparer_1__cctor_m20005_gshared/* 2678*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20006_gshared/* 2679*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20007_gshared/* 2680*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m20008_gshared/* 2681*/,
	(methodPointerType)&DefaultComparer__ctor_m20009_gshared/* 2682*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m20010_gshared/* 2683*/,
	(methodPointerType)&DefaultComparer_Equals_m20011_gshared/* 2684*/,
	(methodPointerType)&Predicate_1__ctor_m20012_gshared/* 2685*/,
	(methodPointerType)&Predicate_1_Invoke_m20013_gshared/* 2686*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m20014_gshared/* 2687*/,
	(methodPointerType)&Predicate_1_EndInvoke_m20015_gshared/* 2688*/,
	(methodPointerType)&Comparer_1__ctor_m20016_gshared/* 2689*/,
	(methodPointerType)&Comparer_1__cctor_m20017_gshared/* 2690*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m20018_gshared/* 2691*/,
	(methodPointerType)&Comparer_1_get_Default_m20019_gshared/* 2692*/,
	(methodPointerType)&DefaultComparer__ctor_m20020_gshared/* 2693*/,
	(methodPointerType)&DefaultComparer_Compare_m20021_gshared/* 2694*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20558_gshared/* 2695*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20559_gshared/* 2696*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20560_gshared/* 2697*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20561_gshared/* 2698*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20562_gshared/* 2699*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20563_gshared/* 2700*/,
	(methodPointerType)&Comparison_1_Invoke_m20564_gshared/* 2701*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m20565_gshared/* 2702*/,
	(methodPointerType)&Comparison_1_EndInvoke_m20566_gshared/* 2703*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20567_gshared/* 2704*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20568_gshared/* 2705*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20569_gshared/* 2706*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20570_gshared/* 2707*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20571_gshared/* 2708*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20572_gshared/* 2709*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m20573_gshared/* 2710*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m20574_gshared/* 2711*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m20575_gshared/* 2712*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m20576_gshared/* 2713*/,
	(methodPointerType)&UnityAction_1_Invoke_m20577_gshared/* 2714*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m20578_gshared/* 2715*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m20579_gshared/* 2716*/,
	(methodPointerType)&InvokableCall_1__ctor_m20580_gshared/* 2717*/,
	(methodPointerType)&InvokableCall_1__ctor_m20581_gshared/* 2718*/,
	(methodPointerType)&InvokableCall_1_Invoke_m20582_gshared/* 2719*/,
	(methodPointerType)&InvokableCall_1_Find_m20583_gshared/* 2720*/,
	(methodPointerType)&List_1__ctor_m20972_gshared/* 2721*/,
	(methodPointerType)&List_1__cctor_m20973_gshared/* 2722*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20974_gshared/* 2723*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m20975_gshared/* 2724*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m20976_gshared/* 2725*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m20977_gshared/* 2726*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m20978_gshared/* 2727*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m20979_gshared/* 2728*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m20980_gshared/* 2729*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m20981_gshared/* 2730*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20982_gshared/* 2731*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m20983_gshared/* 2732*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m20984_gshared/* 2733*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m20985_gshared/* 2734*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m20986_gshared/* 2735*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m20987_gshared/* 2736*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m20988_gshared/* 2737*/,
	(methodPointerType)&List_1_Add_m20989_gshared/* 2738*/,
	(methodPointerType)&List_1_GrowIfNeeded_m20990_gshared/* 2739*/,
	(methodPointerType)&List_1_AddCollection_m20991_gshared/* 2740*/,
	(methodPointerType)&List_1_AddEnumerable_m20992_gshared/* 2741*/,
	(methodPointerType)&List_1_AddRange_m20993_gshared/* 2742*/,
	(methodPointerType)&List_1_AsReadOnly_m20994_gshared/* 2743*/,
	(methodPointerType)&List_1_Clear_m20995_gshared/* 2744*/,
	(methodPointerType)&List_1_Contains_m20996_gshared/* 2745*/,
	(methodPointerType)&List_1_CopyTo_m20997_gshared/* 2746*/,
	(methodPointerType)&List_1_Find_m20998_gshared/* 2747*/,
	(methodPointerType)&List_1_CheckMatch_m20999_gshared/* 2748*/,
	(methodPointerType)&List_1_GetIndex_m21000_gshared/* 2749*/,
	(methodPointerType)&List_1_GetEnumerator_m21001_gshared/* 2750*/,
	(methodPointerType)&List_1_IndexOf_m21002_gshared/* 2751*/,
	(methodPointerType)&List_1_Shift_m21003_gshared/* 2752*/,
	(methodPointerType)&List_1_CheckIndex_m21004_gshared/* 2753*/,
	(methodPointerType)&List_1_Insert_m21005_gshared/* 2754*/,
	(methodPointerType)&List_1_CheckCollection_m21006_gshared/* 2755*/,
	(methodPointerType)&List_1_Remove_m21007_gshared/* 2756*/,
	(methodPointerType)&List_1_RemoveAll_m21008_gshared/* 2757*/,
	(methodPointerType)&List_1_RemoveAt_m21009_gshared/* 2758*/,
	(methodPointerType)&List_1_Reverse_m21010_gshared/* 2759*/,
	(methodPointerType)&List_1_Sort_m21011_gshared/* 2760*/,
	(methodPointerType)&List_1_Sort_m21012_gshared/* 2761*/,
	(methodPointerType)&List_1_TrimExcess_m21013_gshared/* 2762*/,
	(methodPointerType)&List_1_get_Count_m21014_gshared/* 2763*/,
	(methodPointerType)&List_1_get_Item_m21015_gshared/* 2764*/,
	(methodPointerType)&List_1_set_Item_m21016_gshared/* 2765*/,
	(methodPointerType)&Enumerator__ctor_m20950_gshared/* 2766*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m20951_gshared/* 2767*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20952_gshared/* 2768*/,
	(methodPointerType)&Enumerator_Dispose_m20953_gshared/* 2769*/,
	(methodPointerType)&Enumerator_VerifyState_m20954_gshared/* 2770*/,
	(methodPointerType)&Enumerator_MoveNext_m20955_gshared/* 2771*/,
	(methodPointerType)&Enumerator_get_Current_m20956_gshared/* 2772*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m20916_gshared/* 2773*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20917_gshared/* 2774*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20918_gshared/* 2775*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20919_gshared/* 2776*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20920_gshared/* 2777*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20921_gshared/* 2778*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20922_gshared/* 2779*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20923_gshared/* 2780*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20924_gshared/* 2781*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20925_gshared/* 2782*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20926_gshared/* 2783*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m20927_gshared/* 2784*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m20928_gshared/* 2785*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m20929_gshared/* 2786*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20930_gshared/* 2787*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m20931_gshared/* 2788*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m20932_gshared/* 2789*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20933_gshared/* 2790*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20934_gshared/* 2791*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20935_gshared/* 2792*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20936_gshared/* 2793*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20937_gshared/* 2794*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m20938_gshared/* 2795*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m20939_gshared/* 2796*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m20940_gshared/* 2797*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m20941_gshared/* 2798*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m20942_gshared/* 2799*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m20943_gshared/* 2800*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m20944_gshared/* 2801*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m20945_gshared/* 2802*/,
	(methodPointerType)&Collection_1__ctor_m21020_gshared/* 2803*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21021_gshared/* 2804*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m21022_gshared/* 2805*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m21023_gshared/* 2806*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m21024_gshared/* 2807*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m21025_gshared/* 2808*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m21026_gshared/* 2809*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m21027_gshared/* 2810*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m21028_gshared/* 2811*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m21029_gshared/* 2812*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m21030_gshared/* 2813*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m21031_gshared/* 2814*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m21032_gshared/* 2815*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m21033_gshared/* 2816*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m21034_gshared/* 2817*/,
	(methodPointerType)&Collection_1_Add_m21035_gshared/* 2818*/,
	(methodPointerType)&Collection_1_Clear_m21036_gshared/* 2819*/,
	(methodPointerType)&Collection_1_ClearItems_m21037_gshared/* 2820*/,
	(methodPointerType)&Collection_1_Contains_m21038_gshared/* 2821*/,
	(methodPointerType)&Collection_1_CopyTo_m21039_gshared/* 2822*/,
	(methodPointerType)&Collection_1_GetEnumerator_m21040_gshared/* 2823*/,
	(methodPointerType)&Collection_1_IndexOf_m21041_gshared/* 2824*/,
	(methodPointerType)&Collection_1_Insert_m21042_gshared/* 2825*/,
	(methodPointerType)&Collection_1_InsertItem_m21043_gshared/* 2826*/,
	(methodPointerType)&Collection_1_Remove_m21044_gshared/* 2827*/,
	(methodPointerType)&Collection_1_RemoveAt_m21045_gshared/* 2828*/,
	(methodPointerType)&Collection_1_RemoveItem_m21046_gshared/* 2829*/,
	(methodPointerType)&Collection_1_get_Count_m21047_gshared/* 2830*/,
	(methodPointerType)&Collection_1_get_Item_m21048_gshared/* 2831*/,
	(methodPointerType)&Collection_1_set_Item_m21049_gshared/* 2832*/,
	(methodPointerType)&Collection_1_SetItem_m21050_gshared/* 2833*/,
	(methodPointerType)&Collection_1_IsValidItem_m21051_gshared/* 2834*/,
	(methodPointerType)&Collection_1_ConvertItem_m21052_gshared/* 2835*/,
	(methodPointerType)&Collection_1_CheckWritable_m21053_gshared/* 2836*/,
	(methodPointerType)&Collection_1_IsSynchronized_m21054_gshared/* 2837*/,
	(methodPointerType)&Collection_1_IsFixedSize_m21055_gshared/* 2838*/,
	(methodPointerType)&EqualityComparer_1__ctor_m21056_gshared/* 2839*/,
	(methodPointerType)&EqualityComparer_1__cctor_m21057_gshared/* 2840*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21058_gshared/* 2841*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21059_gshared/* 2842*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m21060_gshared/* 2843*/,
	(methodPointerType)&DefaultComparer__ctor_m21061_gshared/* 2844*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m21062_gshared/* 2845*/,
	(methodPointerType)&DefaultComparer_Equals_m21063_gshared/* 2846*/,
	(methodPointerType)&Predicate_1__ctor_m20946_gshared/* 2847*/,
	(methodPointerType)&Predicate_1_Invoke_m20947_gshared/* 2848*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m20948_gshared/* 2849*/,
	(methodPointerType)&Predicate_1_EndInvoke_m20949_gshared/* 2850*/,
	(methodPointerType)&Comparer_1__ctor_m21064_gshared/* 2851*/,
	(methodPointerType)&Comparer_1__cctor_m21065_gshared/* 2852*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m21066_gshared/* 2853*/,
	(methodPointerType)&Comparer_1_get_Default_m21067_gshared/* 2854*/,
	(methodPointerType)&DefaultComparer__ctor_m21068_gshared/* 2855*/,
	(methodPointerType)&DefaultComparer_Compare_m21069_gshared/* 2856*/,
	(methodPointerType)&Comparison_1__ctor_m20957_gshared/* 2857*/,
	(methodPointerType)&Comparison_1_Invoke_m20958_gshared/* 2858*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m20959_gshared/* 2859*/,
	(methodPointerType)&Comparison_1_EndInvoke_m20960_gshared/* 2860*/,
	(methodPointerType)&TweenRunner_1_Start_m21070_gshared/* 2861*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0__ctor_m21071_gshared/* 2862*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m21072_gshared/* 2863*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m21073_gshared/* 2864*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_MoveNext_m21074_gshared/* 2865*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Dispose_m21075_gshared/* 2866*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Reset_m21076_gshared/* 2867*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21529_gshared/* 2868*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21530_gshared/* 2869*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21531_gshared/* 2870*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21532_gshared/* 2871*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21533_gshared/* 2872*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21534_gshared/* 2873*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21535_gshared/* 2874*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21536_gshared/* 2875*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21537_gshared/* 2876*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21538_gshared/* 2877*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21539_gshared/* 2878*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21540_gshared/* 2879*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21541_gshared/* 2880*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m21542_gshared/* 2881*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21543_gshared/* 2882*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21544_gshared/* 2883*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21545_gshared/* 2884*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21546_gshared/* 2885*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m21556_gshared/* 2886*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m21557_gshared/* 2887*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m21558_gshared/* 2888*/,
	(methodPointerType)&UnityAction_1_Invoke_m21559_gshared/* 2889*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m21560_gshared/* 2890*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m21561_gshared/* 2891*/,
	(methodPointerType)&InvokableCall_1__ctor_m21562_gshared/* 2892*/,
	(methodPointerType)&InvokableCall_1__ctor_m21563_gshared/* 2893*/,
	(methodPointerType)&InvokableCall_1_Invoke_m21564_gshared/* 2894*/,
	(methodPointerType)&InvokableCall_1_Find_m21565_gshared/* 2895*/,
	(methodPointerType)&UnityEvent_1_AddListener_m21566_gshared/* 2896*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m21567_gshared/* 2897*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m21568_gshared/* 2898*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m21569_gshared/* 2899*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m21570_gshared/* 2900*/,
	(methodPointerType)&UnityAction_1__ctor_m21571_gshared/* 2901*/,
	(methodPointerType)&UnityAction_1_Invoke_m21572_gshared/* 2902*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m21573_gshared/* 2903*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m21574_gshared/* 2904*/,
	(methodPointerType)&InvokableCall_1__ctor_m21575_gshared/* 2905*/,
	(methodPointerType)&InvokableCall_1__ctor_m21576_gshared/* 2906*/,
	(methodPointerType)&InvokableCall_1_Invoke_m21577_gshared/* 2907*/,
	(methodPointerType)&InvokableCall_1_Find_m21578_gshared/* 2908*/,
	(methodPointerType)&UnityEvent_1_AddListener_m21861_gshared/* 2909*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m21862_gshared/* 2910*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m21863_gshared/* 2911*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m21864_gshared/* 2912*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m21865_gshared/* 2913*/,
	(methodPointerType)&UnityAction_1__ctor_m21866_gshared/* 2914*/,
	(methodPointerType)&UnityAction_1_Invoke_m21867_gshared/* 2915*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m21868_gshared/* 2916*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m21869_gshared/* 2917*/,
	(methodPointerType)&InvokableCall_1__ctor_m21870_gshared/* 2918*/,
	(methodPointerType)&InvokableCall_1__ctor_m21871_gshared/* 2919*/,
	(methodPointerType)&InvokableCall_1_Invoke_m21872_gshared/* 2920*/,
	(methodPointerType)&InvokableCall_1_Find_m21873_gshared/* 2921*/,
	(methodPointerType)&Func_2_Invoke_m21968_gshared/* 2922*/,
	(methodPointerType)&Func_2_BeginInvoke_m21970_gshared/* 2923*/,
	(methodPointerType)&Func_2_EndInvoke_m21972_gshared/* 2924*/,
	(methodPointerType)&Func_2_BeginInvoke_m22080_gshared/* 2925*/,
	(methodPointerType)&Func_2_EndInvoke_m22082_gshared/* 2926*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22119_gshared/* 2927*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22120_gshared/* 2928*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22121_gshared/* 2929*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22122_gshared/* 2930*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22123_gshared/* 2931*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22124_gshared/* 2932*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22404_gshared/* 2933*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22405_gshared/* 2934*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22406_gshared/* 2935*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22407_gshared/* 2936*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22408_gshared/* 2937*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22409_gshared/* 2938*/,
	(methodPointerType)&Dictionary_2__ctor_m22691_gshared/* 2939*/,
	(methodPointerType)&Dictionary_2__ctor_m22693_gshared/* 2940*/,
	(methodPointerType)&Dictionary_2__ctor_m22695_gshared/* 2941*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m22697_gshared/* 2942*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m22699_gshared/* 2943*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m22701_gshared/* 2944*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m22703_gshared/* 2945*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m22705_gshared/* 2946*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m22707_gshared/* 2947*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22709_gshared/* 2948*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22711_gshared/* 2949*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22713_gshared/* 2950*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22715_gshared/* 2951*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22717_gshared/* 2952*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22719_gshared/* 2953*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22721_gshared/* 2954*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m22723_gshared/* 2955*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22725_gshared/* 2956*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22727_gshared/* 2957*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22729_gshared/* 2958*/,
	(methodPointerType)&Dictionary_2_get_Count_m22731_gshared/* 2959*/,
	(methodPointerType)&Dictionary_2_get_Item_m22733_gshared/* 2960*/,
	(methodPointerType)&Dictionary_2_set_Item_m22735_gshared/* 2961*/,
	(methodPointerType)&Dictionary_2_Init_m22737_gshared/* 2962*/,
	(methodPointerType)&Dictionary_2_InitArrays_m22739_gshared/* 2963*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m22741_gshared/* 2964*/,
	(methodPointerType)&Dictionary_2_make_pair_m22743_gshared/* 2965*/,
	(methodPointerType)&Dictionary_2_pick_key_m22745_gshared/* 2966*/,
	(methodPointerType)&Dictionary_2_pick_value_m22747_gshared/* 2967*/,
	(methodPointerType)&Dictionary_2_CopyTo_m22749_gshared/* 2968*/,
	(methodPointerType)&Dictionary_2_Resize_m22751_gshared/* 2969*/,
	(methodPointerType)&Dictionary_2_Add_m22753_gshared/* 2970*/,
	(methodPointerType)&Dictionary_2_Clear_m22755_gshared/* 2971*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m22757_gshared/* 2972*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m22759_gshared/* 2973*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m22761_gshared/* 2974*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m22763_gshared/* 2975*/,
	(methodPointerType)&Dictionary_2_Remove_m22765_gshared/* 2976*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m22767_gshared/* 2977*/,
	(methodPointerType)&Dictionary_2_get_Keys_m22769_gshared/* 2978*/,
	(methodPointerType)&Dictionary_2_ToTKey_m22772_gshared/* 2979*/,
	(methodPointerType)&Dictionary_2_ToTValue_m22774_gshared/* 2980*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m22776_gshared/* 2981*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m22778_gshared/* 2982*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m22780_gshared/* 2983*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22781_gshared/* 2984*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22782_gshared/* 2985*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22783_gshared/* 2986*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22784_gshared/* 2987*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22785_gshared/* 2988*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22786_gshared/* 2989*/,
	(methodPointerType)&KeyValuePair_2__ctor_m22787_gshared/* 2990*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m22788_gshared/* 2991*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m22789_gshared/* 2992*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m22790_gshared/* 2993*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m22791_gshared/* 2994*/,
	(methodPointerType)&KeyValuePair_2_ToString_m22792_gshared/* 2995*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22793_gshared/* 2996*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m22794_gshared/* 2997*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22795_gshared/* 2998*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22796_gshared/* 2999*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22797_gshared/* 3000*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22798_gshared/* 3001*/,
	(methodPointerType)&KeyCollection__ctor_m22799_gshared/* 3002*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22800_gshared/* 3003*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22801_gshared/* 3004*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22802_gshared/* 3005*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22803_gshared/* 3006*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22804_gshared/* 3007*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m22805_gshared/* 3008*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22806_gshared/* 3009*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22807_gshared/* 3010*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22808_gshared/* 3011*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m22809_gshared/* 3012*/,
	(methodPointerType)&KeyCollection_CopyTo_m22810_gshared/* 3013*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m22811_gshared/* 3014*/,
	(methodPointerType)&KeyCollection_get_Count_m22812_gshared/* 3015*/,
	(methodPointerType)&Enumerator__ctor_m22813_gshared/* 3016*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22814_gshared/* 3017*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m22815_gshared/* 3018*/,
	(methodPointerType)&Enumerator_Dispose_m22816_gshared/* 3019*/,
	(methodPointerType)&Enumerator_MoveNext_m22817_gshared/* 3020*/,
	(methodPointerType)&Enumerator_get_Current_m22818_gshared/* 3021*/,
	(methodPointerType)&Enumerator__ctor_m22819_gshared/* 3022*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22820_gshared/* 3023*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m22821_gshared/* 3024*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22822_gshared/* 3025*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22823_gshared/* 3026*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22824_gshared/* 3027*/,
	(methodPointerType)&Enumerator_MoveNext_m22825_gshared/* 3028*/,
	(methodPointerType)&Enumerator_get_Current_m22826_gshared/* 3029*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m22827_gshared/* 3030*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m22828_gshared/* 3031*/,
	(methodPointerType)&Enumerator_Reset_m22829_gshared/* 3032*/,
	(methodPointerType)&Enumerator_VerifyState_m22830_gshared/* 3033*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m22831_gshared/* 3034*/,
	(methodPointerType)&Enumerator_Dispose_m22832_gshared/* 3035*/,
	(methodPointerType)&Transform_1__ctor_m22833_gshared/* 3036*/,
	(methodPointerType)&Transform_1_Invoke_m22834_gshared/* 3037*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22835_gshared/* 3038*/,
	(methodPointerType)&Transform_1_EndInvoke_m22836_gshared/* 3039*/,
	(methodPointerType)&ValueCollection__ctor_m22837_gshared/* 3040*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22838_gshared/* 3041*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22839_gshared/* 3042*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22840_gshared/* 3043*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22841_gshared/* 3044*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22842_gshared/* 3045*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m22843_gshared/* 3046*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22844_gshared/* 3047*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22845_gshared/* 3048*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22846_gshared/* 3049*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m22847_gshared/* 3050*/,
	(methodPointerType)&ValueCollection_CopyTo_m22848_gshared/* 3051*/,
	(methodPointerType)&ValueCollection_get_Count_m22850_gshared/* 3052*/,
	(methodPointerType)&Enumerator__ctor_m22851_gshared/* 3053*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22852_gshared/* 3054*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m22853_gshared/* 3055*/,
	(methodPointerType)&Transform_1__ctor_m22857_gshared/* 3056*/,
	(methodPointerType)&Transform_1_Invoke_m22858_gshared/* 3057*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22859_gshared/* 3058*/,
	(methodPointerType)&Transform_1_EndInvoke_m22860_gshared/* 3059*/,
	(methodPointerType)&Transform_1__ctor_m22861_gshared/* 3060*/,
	(methodPointerType)&Transform_1_Invoke_m22862_gshared/* 3061*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22863_gshared/* 3062*/,
	(methodPointerType)&Transform_1_EndInvoke_m22864_gshared/* 3063*/,
	(methodPointerType)&Transform_1__ctor_m22865_gshared/* 3064*/,
	(methodPointerType)&Transform_1_Invoke_m22866_gshared/* 3065*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22867_gshared/* 3066*/,
	(methodPointerType)&Transform_1_EndInvoke_m22868_gshared/* 3067*/,
	(methodPointerType)&ShimEnumerator__ctor_m22869_gshared/* 3068*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m22870_gshared/* 3069*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m22871_gshared/* 3070*/,
	(methodPointerType)&ShimEnumerator_get_Key_m22872_gshared/* 3071*/,
	(methodPointerType)&ShimEnumerator_get_Value_m22873_gshared/* 3072*/,
	(methodPointerType)&ShimEnumerator_get_Current_m22874_gshared/* 3073*/,
	(methodPointerType)&ShimEnumerator_Reset_m22875_gshared/* 3074*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22876_gshared/* 3075*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22877_gshared/* 3076*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22878_gshared/* 3077*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22879_gshared/* 3078*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22880_gshared/* 3079*/,
	(methodPointerType)&DefaultComparer__ctor_m22881_gshared/* 3080*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22882_gshared/* 3081*/,
	(methodPointerType)&DefaultComparer_Equals_m22883_gshared/* 3082*/,
	(methodPointerType)&List_1__ctor_m22935_gshared/* 3083*/,
	(methodPointerType)&List_1__ctor_m22936_gshared/* 3084*/,
	(methodPointerType)&List_1__cctor_m22937_gshared/* 3085*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22938_gshared/* 3086*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m22939_gshared/* 3087*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m22940_gshared/* 3088*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m22941_gshared/* 3089*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m22942_gshared/* 3090*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m22943_gshared/* 3091*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m22944_gshared/* 3092*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m22945_gshared/* 3093*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22946_gshared/* 3094*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m22947_gshared/* 3095*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m22948_gshared/* 3096*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m22949_gshared/* 3097*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m22950_gshared/* 3098*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m22951_gshared/* 3099*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m22952_gshared/* 3100*/,
	(methodPointerType)&List_1_Add_m22953_gshared/* 3101*/,
	(methodPointerType)&List_1_GrowIfNeeded_m22954_gshared/* 3102*/,
	(methodPointerType)&List_1_AddCollection_m22955_gshared/* 3103*/,
	(methodPointerType)&List_1_AddEnumerable_m22956_gshared/* 3104*/,
	(methodPointerType)&List_1_AddRange_m22957_gshared/* 3105*/,
	(methodPointerType)&List_1_AsReadOnly_m22958_gshared/* 3106*/,
	(methodPointerType)&List_1_Clear_m22959_gshared/* 3107*/,
	(methodPointerType)&List_1_Contains_m22960_gshared/* 3108*/,
	(methodPointerType)&List_1_CopyTo_m22961_gshared/* 3109*/,
	(methodPointerType)&List_1_Find_m22962_gshared/* 3110*/,
	(methodPointerType)&List_1_CheckMatch_m22963_gshared/* 3111*/,
	(methodPointerType)&List_1_GetIndex_m22964_gshared/* 3112*/,
	(methodPointerType)&List_1_GetEnumerator_m22965_gshared/* 3113*/,
	(methodPointerType)&List_1_IndexOf_m22966_gshared/* 3114*/,
	(methodPointerType)&List_1_Shift_m22967_gshared/* 3115*/,
	(methodPointerType)&List_1_CheckIndex_m22968_gshared/* 3116*/,
	(methodPointerType)&List_1_Insert_m22969_gshared/* 3117*/,
	(methodPointerType)&List_1_CheckCollection_m22970_gshared/* 3118*/,
	(methodPointerType)&List_1_Remove_m22971_gshared/* 3119*/,
	(methodPointerType)&List_1_RemoveAll_m22972_gshared/* 3120*/,
	(methodPointerType)&List_1_RemoveAt_m22973_gshared/* 3121*/,
	(methodPointerType)&List_1_Reverse_m22974_gshared/* 3122*/,
	(methodPointerType)&List_1_Sort_m22975_gshared/* 3123*/,
	(methodPointerType)&List_1_Sort_m22976_gshared/* 3124*/,
	(methodPointerType)&List_1_ToArray_m22977_gshared/* 3125*/,
	(methodPointerType)&List_1_TrimExcess_m22978_gshared/* 3126*/,
	(methodPointerType)&List_1_get_Capacity_m22979_gshared/* 3127*/,
	(methodPointerType)&List_1_set_Capacity_m22980_gshared/* 3128*/,
	(methodPointerType)&List_1_get_Count_m22981_gshared/* 3129*/,
	(methodPointerType)&List_1_get_Item_m22982_gshared/* 3130*/,
	(methodPointerType)&List_1_set_Item_m22983_gshared/* 3131*/,
	(methodPointerType)&Enumerator__ctor_m22984_gshared/* 3132*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m22985_gshared/* 3133*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22986_gshared/* 3134*/,
	(methodPointerType)&Enumerator_Dispose_m22987_gshared/* 3135*/,
	(methodPointerType)&Enumerator_VerifyState_m22988_gshared/* 3136*/,
	(methodPointerType)&Enumerator_MoveNext_m22989_gshared/* 3137*/,
	(methodPointerType)&Enumerator_get_Current_m22990_gshared/* 3138*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m22991_gshared/* 3139*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22992_gshared/* 3140*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22993_gshared/* 3141*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22994_gshared/* 3142*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22995_gshared/* 3143*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22996_gshared/* 3144*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22997_gshared/* 3145*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22998_gshared/* 3146*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22999_gshared/* 3147*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23000_gshared/* 3148*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23001_gshared/* 3149*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m23002_gshared/* 3150*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m23003_gshared/* 3151*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m23004_gshared/* 3152*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23005_gshared/* 3153*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m23006_gshared/* 3154*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m23007_gshared/* 3155*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23008_gshared/* 3156*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23009_gshared/* 3157*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23010_gshared/* 3158*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23011_gshared/* 3159*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23012_gshared/* 3160*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m23013_gshared/* 3161*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m23014_gshared/* 3162*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m23015_gshared/* 3163*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m23016_gshared/* 3164*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m23017_gshared/* 3165*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m23018_gshared/* 3166*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m23019_gshared/* 3167*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m23020_gshared/* 3168*/,
	(methodPointerType)&Collection_1__ctor_m23021_gshared/* 3169*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23022_gshared/* 3170*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m23023_gshared/* 3171*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m23024_gshared/* 3172*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m23025_gshared/* 3173*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m23026_gshared/* 3174*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m23027_gshared/* 3175*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m23028_gshared/* 3176*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m23029_gshared/* 3177*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m23030_gshared/* 3178*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m23031_gshared/* 3179*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m23032_gshared/* 3180*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m23033_gshared/* 3181*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m23034_gshared/* 3182*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m23035_gshared/* 3183*/,
	(methodPointerType)&Collection_1_Add_m23036_gshared/* 3184*/,
	(methodPointerType)&Collection_1_Clear_m23037_gshared/* 3185*/,
	(methodPointerType)&Collection_1_ClearItems_m23038_gshared/* 3186*/,
	(methodPointerType)&Collection_1_Contains_m23039_gshared/* 3187*/,
	(methodPointerType)&Collection_1_CopyTo_m23040_gshared/* 3188*/,
	(methodPointerType)&Collection_1_GetEnumerator_m23041_gshared/* 3189*/,
	(methodPointerType)&Collection_1_IndexOf_m23042_gshared/* 3190*/,
	(methodPointerType)&Collection_1_Insert_m23043_gshared/* 3191*/,
	(methodPointerType)&Collection_1_InsertItem_m23044_gshared/* 3192*/,
	(methodPointerType)&Collection_1_Remove_m23045_gshared/* 3193*/,
	(methodPointerType)&Collection_1_RemoveAt_m23046_gshared/* 3194*/,
	(methodPointerType)&Collection_1_RemoveItem_m23047_gshared/* 3195*/,
	(methodPointerType)&Collection_1_get_Count_m23048_gshared/* 3196*/,
	(methodPointerType)&Collection_1_get_Item_m23049_gshared/* 3197*/,
	(methodPointerType)&Collection_1_set_Item_m23050_gshared/* 3198*/,
	(methodPointerType)&Collection_1_SetItem_m23051_gshared/* 3199*/,
	(methodPointerType)&Collection_1_IsValidItem_m23052_gshared/* 3200*/,
	(methodPointerType)&Collection_1_ConvertItem_m23053_gshared/* 3201*/,
	(methodPointerType)&Collection_1_CheckWritable_m23054_gshared/* 3202*/,
	(methodPointerType)&Collection_1_IsSynchronized_m23055_gshared/* 3203*/,
	(methodPointerType)&Collection_1_IsFixedSize_m23056_gshared/* 3204*/,
	(methodPointerType)&Predicate_1__ctor_m23057_gshared/* 3205*/,
	(methodPointerType)&Predicate_1_Invoke_m23058_gshared/* 3206*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m23059_gshared/* 3207*/,
	(methodPointerType)&Predicate_1_EndInvoke_m23060_gshared/* 3208*/,
	(methodPointerType)&Comparer_1__ctor_m23061_gshared/* 3209*/,
	(methodPointerType)&Comparer_1__cctor_m23062_gshared/* 3210*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m23063_gshared/* 3211*/,
	(methodPointerType)&Comparer_1_get_Default_m23064_gshared/* 3212*/,
	(methodPointerType)&DefaultComparer__ctor_m23065_gshared/* 3213*/,
	(methodPointerType)&DefaultComparer_Compare_m23066_gshared/* 3214*/,
	(methodPointerType)&Comparison_1__ctor_m23067_gshared/* 3215*/,
	(methodPointerType)&Comparison_1_Invoke_m23068_gshared/* 3216*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m23069_gshared/* 3217*/,
	(methodPointerType)&Comparison_1_EndInvoke_m23070_gshared/* 3218*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23257_gshared/* 3219*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23258_gshared/* 3220*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23259_gshared/* 3221*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23260_gshared/* 3222*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23261_gshared/* 3223*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23262_gshared/* 3224*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23742_gshared/* 3225*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23743_gshared/* 3226*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23744_gshared/* 3227*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23745_gshared/* 3228*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23746_gshared/* 3229*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23747_gshared/* 3230*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23748_gshared/* 3231*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23749_gshared/* 3232*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23750_gshared/* 3233*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23751_gshared/* 3234*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23752_gshared/* 3235*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23753_gshared/* 3236*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23754_gshared/* 3237*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23755_gshared/* 3238*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23756_gshared/* 3239*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23757_gshared/* 3240*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23758_gshared/* 3241*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23759_gshared/* 3242*/,
	(methodPointerType)&LinkedList_1__ctor_m23760_gshared/* 3243*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23761_gshared/* 3244*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_CopyTo_m23762_gshared/* 3245*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23763_gshared/* 3246*/,
	(methodPointerType)&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m23764_gshared/* 3247*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23765_gshared/* 3248*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m23766_gshared/* 3249*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m23767_gshared/* 3250*/,
	(methodPointerType)&LinkedList_1_VerifyReferencedNode_m23768_gshared/* 3251*/,
	(methodPointerType)&LinkedList_1_Clear_m23769_gshared/* 3252*/,
	(methodPointerType)&LinkedList_1_Contains_m23770_gshared/* 3253*/,
	(methodPointerType)&LinkedList_1_CopyTo_m23771_gshared/* 3254*/,
	(methodPointerType)&LinkedList_1_Find_m23772_gshared/* 3255*/,
	(methodPointerType)&LinkedList_1_GetEnumerator_m23773_gshared/* 3256*/,
	(methodPointerType)&LinkedList_1_GetObjectData_m23774_gshared/* 3257*/,
	(methodPointerType)&LinkedList_1_OnDeserialization_m23775_gshared/* 3258*/,
	(methodPointerType)&LinkedList_1_Remove_m23776_gshared/* 3259*/,
	(methodPointerType)&LinkedList_1_get_Count_m23777_gshared/* 3260*/,
	(methodPointerType)&LinkedListNode_1__ctor_m23778_gshared/* 3261*/,
	(methodPointerType)&LinkedListNode_1__ctor_m23779_gshared/* 3262*/,
	(methodPointerType)&LinkedListNode_1_Detach_m23780_gshared/* 3263*/,
	(methodPointerType)&LinkedListNode_1_get_List_m23781_gshared/* 3264*/,
	(methodPointerType)&Enumerator__ctor_m23782_gshared/* 3265*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m23783_gshared/* 3266*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m23784_gshared/* 3267*/,
	(methodPointerType)&Enumerator_get_Current_m23785_gshared/* 3268*/,
	(methodPointerType)&Enumerator_MoveNext_m23786_gshared/* 3269*/,
	(methodPointerType)&Enumerator_Dispose_m23787_gshared/* 3270*/,
	(methodPointerType)&List_1__ctor_m23788_gshared/* 3271*/,
	(methodPointerType)&List_1__cctor_m23789_gshared/* 3272*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23790_gshared/* 3273*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m23791_gshared/* 3274*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m23792_gshared/* 3275*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m23793_gshared/* 3276*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m23794_gshared/* 3277*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m23795_gshared/* 3278*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m23796_gshared/* 3279*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m23797_gshared/* 3280*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23798_gshared/* 3281*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m23799_gshared/* 3282*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m23800_gshared/* 3283*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m23801_gshared/* 3284*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m23802_gshared/* 3285*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m23803_gshared/* 3286*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m23804_gshared/* 3287*/,
	(methodPointerType)&List_1_Add_m23805_gshared/* 3288*/,
	(methodPointerType)&List_1_GrowIfNeeded_m23806_gshared/* 3289*/,
	(methodPointerType)&List_1_AddCollection_m23807_gshared/* 3290*/,
	(methodPointerType)&List_1_AddEnumerable_m23808_gshared/* 3291*/,
	(methodPointerType)&List_1_AddRange_m23809_gshared/* 3292*/,
	(methodPointerType)&List_1_AsReadOnly_m23810_gshared/* 3293*/,
	(methodPointerType)&List_1_Clear_m23811_gshared/* 3294*/,
	(methodPointerType)&List_1_Contains_m23812_gshared/* 3295*/,
	(methodPointerType)&List_1_CopyTo_m23813_gshared/* 3296*/,
	(methodPointerType)&List_1_Find_m23814_gshared/* 3297*/,
	(methodPointerType)&List_1_CheckMatch_m23815_gshared/* 3298*/,
	(methodPointerType)&List_1_GetIndex_m23816_gshared/* 3299*/,
	(methodPointerType)&List_1_IndexOf_m23817_gshared/* 3300*/,
	(methodPointerType)&List_1_Shift_m23818_gshared/* 3301*/,
	(methodPointerType)&List_1_CheckIndex_m23819_gshared/* 3302*/,
	(methodPointerType)&List_1_Insert_m23820_gshared/* 3303*/,
	(methodPointerType)&List_1_CheckCollection_m23821_gshared/* 3304*/,
	(methodPointerType)&List_1_Remove_m23822_gshared/* 3305*/,
	(methodPointerType)&List_1_RemoveAll_m23823_gshared/* 3306*/,
	(methodPointerType)&List_1_RemoveAt_m23824_gshared/* 3307*/,
	(methodPointerType)&List_1_Reverse_m23825_gshared/* 3308*/,
	(methodPointerType)&List_1_Sort_m23826_gshared/* 3309*/,
	(methodPointerType)&List_1_Sort_m23827_gshared/* 3310*/,
	(methodPointerType)&List_1_ToArray_m23828_gshared/* 3311*/,
	(methodPointerType)&List_1_TrimExcess_m23829_gshared/* 3312*/,
	(methodPointerType)&List_1_get_Capacity_m23830_gshared/* 3313*/,
	(methodPointerType)&List_1_set_Capacity_m23831_gshared/* 3314*/,
	(methodPointerType)&List_1_get_Count_m23832_gshared/* 3315*/,
	(methodPointerType)&List_1_get_Item_m23833_gshared/* 3316*/,
	(methodPointerType)&List_1_set_Item_m23834_gshared/* 3317*/,
	(methodPointerType)&Enumerator__ctor_m23835_gshared/* 3318*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m23836_gshared/* 3319*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m23837_gshared/* 3320*/,
	(methodPointerType)&Enumerator_VerifyState_m23838_gshared/* 3321*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m23839_gshared/* 3322*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23840_gshared/* 3323*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23841_gshared/* 3324*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23842_gshared/* 3325*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23843_gshared/* 3326*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23844_gshared/* 3327*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23845_gshared/* 3328*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23846_gshared/* 3329*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23847_gshared/* 3330*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23848_gshared/* 3331*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23849_gshared/* 3332*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m23850_gshared/* 3333*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m23851_gshared/* 3334*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m23852_gshared/* 3335*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23853_gshared/* 3336*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m23854_gshared/* 3337*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m23855_gshared/* 3338*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23856_gshared/* 3339*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23857_gshared/* 3340*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23858_gshared/* 3341*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23859_gshared/* 3342*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23860_gshared/* 3343*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m23861_gshared/* 3344*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m23862_gshared/* 3345*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m23863_gshared/* 3346*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m23864_gshared/* 3347*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m23865_gshared/* 3348*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m23866_gshared/* 3349*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m23867_gshared/* 3350*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m23868_gshared/* 3351*/,
	(methodPointerType)&Collection_1__ctor_m23869_gshared/* 3352*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23870_gshared/* 3353*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m23871_gshared/* 3354*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m23872_gshared/* 3355*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m23873_gshared/* 3356*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m23874_gshared/* 3357*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m23875_gshared/* 3358*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m23876_gshared/* 3359*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m23877_gshared/* 3360*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m23878_gshared/* 3361*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m23879_gshared/* 3362*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m23880_gshared/* 3363*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m23881_gshared/* 3364*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m23882_gshared/* 3365*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m23883_gshared/* 3366*/,
	(methodPointerType)&Collection_1_Add_m23884_gshared/* 3367*/,
	(methodPointerType)&Collection_1_Clear_m23885_gshared/* 3368*/,
	(methodPointerType)&Collection_1_ClearItems_m23886_gshared/* 3369*/,
	(methodPointerType)&Collection_1_Contains_m23887_gshared/* 3370*/,
	(methodPointerType)&Collection_1_CopyTo_m23888_gshared/* 3371*/,
	(methodPointerType)&Collection_1_GetEnumerator_m23889_gshared/* 3372*/,
	(methodPointerType)&Collection_1_IndexOf_m23890_gshared/* 3373*/,
	(methodPointerType)&Collection_1_Insert_m23891_gshared/* 3374*/,
	(methodPointerType)&Collection_1_InsertItem_m23892_gshared/* 3375*/,
	(methodPointerType)&Collection_1_Remove_m23893_gshared/* 3376*/,
	(methodPointerType)&Collection_1_RemoveAt_m23894_gshared/* 3377*/,
	(methodPointerType)&Collection_1_RemoveItem_m23895_gshared/* 3378*/,
	(methodPointerType)&Collection_1_get_Count_m23896_gshared/* 3379*/,
	(methodPointerType)&Collection_1_get_Item_m23897_gshared/* 3380*/,
	(methodPointerType)&Collection_1_set_Item_m23898_gshared/* 3381*/,
	(methodPointerType)&Collection_1_SetItem_m23899_gshared/* 3382*/,
	(methodPointerType)&Collection_1_IsValidItem_m23900_gshared/* 3383*/,
	(methodPointerType)&Collection_1_ConvertItem_m23901_gshared/* 3384*/,
	(methodPointerType)&Collection_1_CheckWritable_m23902_gshared/* 3385*/,
	(methodPointerType)&Collection_1_IsSynchronized_m23903_gshared/* 3386*/,
	(methodPointerType)&Collection_1_IsFixedSize_m23904_gshared/* 3387*/,
	(methodPointerType)&Predicate_1__ctor_m23905_gshared/* 3388*/,
	(methodPointerType)&Predicate_1_Invoke_m23906_gshared/* 3389*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m23907_gshared/* 3390*/,
	(methodPointerType)&Predicate_1_EndInvoke_m23908_gshared/* 3391*/,
	(methodPointerType)&Comparer_1__ctor_m23909_gshared/* 3392*/,
	(methodPointerType)&Comparer_1__cctor_m23910_gshared/* 3393*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m23911_gshared/* 3394*/,
	(methodPointerType)&Comparer_1_get_Default_m23912_gshared/* 3395*/,
	(methodPointerType)&GenericComparer_1__ctor_m23913_gshared/* 3396*/,
	(methodPointerType)&GenericComparer_1_Compare_m23914_gshared/* 3397*/,
	(methodPointerType)&DefaultComparer__ctor_m23915_gshared/* 3398*/,
	(methodPointerType)&DefaultComparer_Compare_m23916_gshared/* 3399*/,
	(methodPointerType)&Comparison_1__ctor_m23917_gshared/* 3400*/,
	(methodPointerType)&Comparison_1_Invoke_m23918_gshared/* 3401*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m23919_gshared/* 3402*/,
	(methodPointerType)&Comparison_1_EndInvoke_m23920_gshared/* 3403*/,
	(methodPointerType)&Predicate_1_Invoke_m23921_gshared/* 3404*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m23922_gshared/* 3405*/,
	(methodPointerType)&Predicate_1_EndInvoke_m23923_gshared/* 3406*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23924_gshared/* 3407*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23925_gshared/* 3408*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23926_gshared/* 3409*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23927_gshared/* 3410*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23928_gshared/* 3411*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23929_gshared/* 3412*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23930_gshared/* 3413*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23931_gshared/* 3414*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23932_gshared/* 3415*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23933_gshared/* 3416*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23934_gshared/* 3417*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23935_gshared/* 3418*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23936_gshared/* 3419*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23937_gshared/* 3420*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23938_gshared/* 3421*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23939_gshared/* 3422*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23940_gshared/* 3423*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23941_gshared/* 3424*/,
	(methodPointerType)&Dictionary_2__ctor_m24142_gshared/* 3425*/,
	(methodPointerType)&Dictionary_2__ctor_m24144_gshared/* 3426*/,
	(methodPointerType)&Dictionary_2__ctor_m24146_gshared/* 3427*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m24148_gshared/* 3428*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m24150_gshared/* 3429*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m24152_gshared/* 3430*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m24154_gshared/* 3431*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m24156_gshared/* 3432*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m24158_gshared/* 3433*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24160_gshared/* 3434*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24162_gshared/* 3435*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24164_gshared/* 3436*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24166_gshared/* 3437*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24168_gshared/* 3438*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24170_gshared/* 3439*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24172_gshared/* 3440*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m24174_gshared/* 3441*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24176_gshared/* 3442*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24178_gshared/* 3443*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24180_gshared/* 3444*/,
	(methodPointerType)&Dictionary_2_get_Count_m24182_gshared/* 3445*/,
	(methodPointerType)&Dictionary_2_get_Item_m24184_gshared/* 3446*/,
	(methodPointerType)&Dictionary_2_set_Item_m24186_gshared/* 3447*/,
	(methodPointerType)&Dictionary_2_Init_m24188_gshared/* 3448*/,
	(methodPointerType)&Dictionary_2_InitArrays_m24190_gshared/* 3449*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m24192_gshared/* 3450*/,
	(methodPointerType)&Dictionary_2_make_pair_m24194_gshared/* 3451*/,
	(methodPointerType)&Dictionary_2_pick_key_m24196_gshared/* 3452*/,
	(methodPointerType)&Dictionary_2_pick_value_m24198_gshared/* 3453*/,
	(methodPointerType)&Dictionary_2_CopyTo_m24200_gshared/* 3454*/,
	(methodPointerType)&Dictionary_2_Resize_m24202_gshared/* 3455*/,
	(methodPointerType)&Dictionary_2_Add_m24204_gshared/* 3456*/,
	(methodPointerType)&Dictionary_2_Clear_m24206_gshared/* 3457*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m24208_gshared/* 3458*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m24210_gshared/* 3459*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m24212_gshared/* 3460*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m24214_gshared/* 3461*/,
	(methodPointerType)&Dictionary_2_Remove_m24216_gshared/* 3462*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m24218_gshared/* 3463*/,
	(methodPointerType)&Dictionary_2_get_Keys_m24220_gshared/* 3464*/,
	(methodPointerType)&Dictionary_2_get_Values_m24222_gshared/* 3465*/,
	(methodPointerType)&Dictionary_2_ToTKey_m24224_gshared/* 3466*/,
	(methodPointerType)&Dictionary_2_ToTValue_m24226_gshared/* 3467*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m24228_gshared/* 3468*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m24230_gshared/* 3469*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m24232_gshared/* 3470*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24233_gshared/* 3471*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24234_gshared/* 3472*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24235_gshared/* 3473*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24236_gshared/* 3474*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24237_gshared/* 3475*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24238_gshared/* 3476*/,
	(methodPointerType)&KeyValuePair_2__ctor_m24239_gshared/* 3477*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m24240_gshared/* 3478*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m24241_gshared/* 3479*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m24242_gshared/* 3480*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m24243_gshared/* 3481*/,
	(methodPointerType)&KeyValuePair_2_ToString_m24244_gshared/* 3482*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24245_gshared/* 3483*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24246_gshared/* 3484*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24247_gshared/* 3485*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24248_gshared/* 3486*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24249_gshared/* 3487*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24250_gshared/* 3488*/,
	(methodPointerType)&KeyCollection__ctor_m24251_gshared/* 3489*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m24252_gshared/* 3490*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m24253_gshared/* 3491*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24254_gshared/* 3492*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m24255_gshared/* 3493*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m24256_gshared/* 3494*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m24257_gshared/* 3495*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m24258_gshared/* 3496*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m24259_gshared/* 3497*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m24260_gshared/* 3498*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m24261_gshared/* 3499*/,
	(methodPointerType)&KeyCollection_CopyTo_m24262_gshared/* 3500*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m24263_gshared/* 3501*/,
	(methodPointerType)&KeyCollection_get_Count_m24264_gshared/* 3502*/,
	(methodPointerType)&Enumerator__ctor_m24265_gshared/* 3503*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m24266_gshared/* 3504*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m24267_gshared/* 3505*/,
	(methodPointerType)&Enumerator_Dispose_m24268_gshared/* 3506*/,
	(methodPointerType)&Enumerator_MoveNext_m24269_gshared/* 3507*/,
	(methodPointerType)&Enumerator_get_Current_m24270_gshared/* 3508*/,
	(methodPointerType)&Enumerator__ctor_m24271_gshared/* 3509*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m24272_gshared/* 3510*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m24273_gshared/* 3511*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24274_gshared/* 3512*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24275_gshared/* 3513*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24276_gshared/* 3514*/,
	(methodPointerType)&Enumerator_MoveNext_m24277_gshared/* 3515*/,
	(methodPointerType)&Enumerator_get_Current_m24278_gshared/* 3516*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m24279_gshared/* 3517*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m24280_gshared/* 3518*/,
	(methodPointerType)&Enumerator_Reset_m24281_gshared/* 3519*/,
	(methodPointerType)&Enumerator_VerifyState_m24282_gshared/* 3520*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m24283_gshared/* 3521*/,
	(methodPointerType)&Enumerator_Dispose_m24284_gshared/* 3522*/,
	(methodPointerType)&Transform_1__ctor_m24285_gshared/* 3523*/,
	(methodPointerType)&Transform_1_Invoke_m24286_gshared/* 3524*/,
	(methodPointerType)&Transform_1_BeginInvoke_m24287_gshared/* 3525*/,
	(methodPointerType)&Transform_1_EndInvoke_m24288_gshared/* 3526*/,
	(methodPointerType)&ValueCollection__ctor_m24289_gshared/* 3527*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m24290_gshared/* 3528*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m24291_gshared/* 3529*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m24292_gshared/* 3530*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m24293_gshared/* 3531*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m24294_gshared/* 3532*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m24295_gshared/* 3533*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m24296_gshared/* 3534*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m24297_gshared/* 3535*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m24298_gshared/* 3536*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m24299_gshared/* 3537*/,
	(methodPointerType)&ValueCollection_CopyTo_m24300_gshared/* 3538*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m24301_gshared/* 3539*/,
	(methodPointerType)&ValueCollection_get_Count_m24302_gshared/* 3540*/,
	(methodPointerType)&Enumerator__ctor_m24303_gshared/* 3541*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m24304_gshared/* 3542*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m24305_gshared/* 3543*/,
	(methodPointerType)&Enumerator_Dispose_m24306_gshared/* 3544*/,
	(methodPointerType)&Enumerator_MoveNext_m24307_gshared/* 3545*/,
	(methodPointerType)&Enumerator_get_Current_m24308_gshared/* 3546*/,
	(methodPointerType)&Transform_1__ctor_m24309_gshared/* 3547*/,
	(methodPointerType)&Transform_1_Invoke_m24310_gshared/* 3548*/,
	(methodPointerType)&Transform_1_BeginInvoke_m24311_gshared/* 3549*/,
	(methodPointerType)&Transform_1_EndInvoke_m24312_gshared/* 3550*/,
	(methodPointerType)&Transform_1__ctor_m24313_gshared/* 3551*/,
	(methodPointerType)&Transform_1_Invoke_m24314_gshared/* 3552*/,
	(methodPointerType)&Transform_1_BeginInvoke_m24315_gshared/* 3553*/,
	(methodPointerType)&Transform_1_EndInvoke_m24316_gshared/* 3554*/,
	(methodPointerType)&Transform_1__ctor_m24317_gshared/* 3555*/,
	(methodPointerType)&Transform_1_Invoke_m24318_gshared/* 3556*/,
	(methodPointerType)&Transform_1_BeginInvoke_m24319_gshared/* 3557*/,
	(methodPointerType)&Transform_1_EndInvoke_m24320_gshared/* 3558*/,
	(methodPointerType)&ShimEnumerator__ctor_m24321_gshared/* 3559*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m24322_gshared/* 3560*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m24323_gshared/* 3561*/,
	(methodPointerType)&ShimEnumerator_get_Key_m24324_gshared/* 3562*/,
	(methodPointerType)&ShimEnumerator_get_Value_m24325_gshared/* 3563*/,
	(methodPointerType)&ShimEnumerator_get_Current_m24326_gshared/* 3564*/,
	(methodPointerType)&ShimEnumerator_Reset_m24327_gshared/* 3565*/,
	(methodPointerType)&EqualityComparer_1__ctor_m24328_gshared/* 3566*/,
	(methodPointerType)&EqualityComparer_1__cctor_m24329_gshared/* 3567*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m24330_gshared/* 3568*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m24331_gshared/* 3569*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m24332_gshared/* 3570*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m24333_gshared/* 3571*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m24334_gshared/* 3572*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m24335_gshared/* 3573*/,
	(methodPointerType)&DefaultComparer__ctor_m24336_gshared/* 3574*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m24337_gshared/* 3575*/,
	(methodPointerType)&DefaultComparer_Equals_m24338_gshared/* 3576*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24391_gshared/* 3577*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24392_gshared/* 3578*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24393_gshared/* 3579*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24394_gshared/* 3580*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24395_gshared/* 3581*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24396_gshared/* 3582*/,
	(methodPointerType)&Action_1__ctor_m25226_gshared/* 3583*/,
	(methodPointerType)&Action_1_BeginInvoke_m25227_gshared/* 3584*/,
	(methodPointerType)&Action_1_EndInvoke_m25228_gshared/* 3585*/,
	(methodPointerType)&Dictionary_2__ctor_m25918_gshared/* 3586*/,
	(methodPointerType)&Dictionary_2__ctor_m25919_gshared/* 3587*/,
	(methodPointerType)&Dictionary_2__ctor_m25920_gshared/* 3588*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m25921_gshared/* 3589*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m25922_gshared/* 3590*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m25923_gshared/* 3591*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m25924_gshared/* 3592*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m25925_gshared/* 3593*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m25926_gshared/* 3594*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25927_gshared/* 3595*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25928_gshared/* 3596*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25929_gshared/* 3597*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25930_gshared/* 3598*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25931_gshared/* 3599*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25932_gshared/* 3600*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25933_gshared/* 3601*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m25934_gshared/* 3602*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25935_gshared/* 3603*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25936_gshared/* 3604*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25937_gshared/* 3605*/,
	(methodPointerType)&Dictionary_2_get_Count_m25938_gshared/* 3606*/,
	(methodPointerType)&Dictionary_2_get_Item_m25939_gshared/* 3607*/,
	(methodPointerType)&Dictionary_2_set_Item_m25940_gshared/* 3608*/,
	(methodPointerType)&Dictionary_2_Init_m25941_gshared/* 3609*/,
	(methodPointerType)&Dictionary_2_InitArrays_m25942_gshared/* 3610*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m25943_gshared/* 3611*/,
	(methodPointerType)&Dictionary_2_make_pair_m25944_gshared/* 3612*/,
	(methodPointerType)&Dictionary_2_pick_key_m25945_gshared/* 3613*/,
	(methodPointerType)&Dictionary_2_pick_value_m25946_gshared/* 3614*/,
	(methodPointerType)&Dictionary_2_CopyTo_m25947_gshared/* 3615*/,
	(methodPointerType)&Dictionary_2_Resize_m25948_gshared/* 3616*/,
	(methodPointerType)&Dictionary_2_Add_m25949_gshared/* 3617*/,
	(methodPointerType)&Dictionary_2_Clear_m25950_gshared/* 3618*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m25951_gshared/* 3619*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m25952_gshared/* 3620*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m25953_gshared/* 3621*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m25954_gshared/* 3622*/,
	(methodPointerType)&Dictionary_2_Remove_m25955_gshared/* 3623*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m25956_gshared/* 3624*/,
	(methodPointerType)&Dictionary_2_get_Keys_m25957_gshared/* 3625*/,
	(methodPointerType)&Dictionary_2_get_Values_m25958_gshared/* 3626*/,
	(methodPointerType)&Dictionary_2_ToTKey_m25959_gshared/* 3627*/,
	(methodPointerType)&Dictionary_2_ToTValue_m25960_gshared/* 3628*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m25961_gshared/* 3629*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m25962_gshared/* 3630*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m25963_gshared/* 3631*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25964_gshared/* 3632*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m25965_gshared/* 3633*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25966_gshared/* 3634*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25967_gshared/* 3635*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25968_gshared/* 3636*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25969_gshared/* 3637*/,
	(methodPointerType)&KeyValuePair_2__ctor_m25970_gshared/* 3638*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m25971_gshared/* 3639*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m25972_gshared/* 3640*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m25973_gshared/* 3641*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m25974_gshared/* 3642*/,
	(methodPointerType)&KeyValuePair_2_ToString_m25975_gshared/* 3643*/,
	(methodPointerType)&KeyCollection__ctor_m25976_gshared/* 3644*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25977_gshared/* 3645*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25978_gshared/* 3646*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25979_gshared/* 3647*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25980_gshared/* 3648*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25981_gshared/* 3649*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m25982_gshared/* 3650*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25983_gshared/* 3651*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25984_gshared/* 3652*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25985_gshared/* 3653*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m25986_gshared/* 3654*/,
	(methodPointerType)&KeyCollection_CopyTo_m25987_gshared/* 3655*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m25988_gshared/* 3656*/,
	(methodPointerType)&KeyCollection_get_Count_m25989_gshared/* 3657*/,
	(methodPointerType)&Enumerator__ctor_m25990_gshared/* 3658*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25991_gshared/* 3659*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m25992_gshared/* 3660*/,
	(methodPointerType)&Enumerator_Dispose_m25993_gshared/* 3661*/,
	(methodPointerType)&Enumerator_MoveNext_m25994_gshared/* 3662*/,
	(methodPointerType)&Enumerator_get_Current_m25995_gshared/* 3663*/,
	(methodPointerType)&Enumerator__ctor_m25996_gshared/* 3664*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25997_gshared/* 3665*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m25998_gshared/* 3666*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25999_gshared/* 3667*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26000_gshared/* 3668*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26001_gshared/* 3669*/,
	(methodPointerType)&Enumerator_MoveNext_m26002_gshared/* 3670*/,
	(methodPointerType)&Enumerator_get_Current_m26003_gshared/* 3671*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m26004_gshared/* 3672*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m26005_gshared/* 3673*/,
	(methodPointerType)&Enumerator_Reset_m26006_gshared/* 3674*/,
	(methodPointerType)&Enumerator_VerifyState_m26007_gshared/* 3675*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m26008_gshared/* 3676*/,
	(methodPointerType)&Enumerator_Dispose_m26009_gshared/* 3677*/,
	(methodPointerType)&Transform_1__ctor_m26010_gshared/* 3678*/,
	(methodPointerType)&Transform_1_Invoke_m26011_gshared/* 3679*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26012_gshared/* 3680*/,
	(methodPointerType)&Transform_1_EndInvoke_m26013_gshared/* 3681*/,
	(methodPointerType)&ValueCollection__ctor_m26014_gshared/* 3682*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26015_gshared/* 3683*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26016_gshared/* 3684*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26017_gshared/* 3685*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26018_gshared/* 3686*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26019_gshared/* 3687*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m26020_gshared/* 3688*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26021_gshared/* 3689*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26022_gshared/* 3690*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26023_gshared/* 3691*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m26024_gshared/* 3692*/,
	(methodPointerType)&ValueCollection_CopyTo_m26025_gshared/* 3693*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m26026_gshared/* 3694*/,
	(methodPointerType)&ValueCollection_get_Count_m26027_gshared/* 3695*/,
	(methodPointerType)&Enumerator__ctor_m26028_gshared/* 3696*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26029_gshared/* 3697*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m26030_gshared/* 3698*/,
	(methodPointerType)&Enumerator_Dispose_m26031_gshared/* 3699*/,
	(methodPointerType)&Enumerator_MoveNext_m26032_gshared/* 3700*/,
	(methodPointerType)&Enumerator_get_Current_m26033_gshared/* 3701*/,
	(methodPointerType)&Transform_1__ctor_m26034_gshared/* 3702*/,
	(methodPointerType)&Transform_1_Invoke_m26035_gshared/* 3703*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26036_gshared/* 3704*/,
	(methodPointerType)&Transform_1_EndInvoke_m26037_gshared/* 3705*/,
	(methodPointerType)&Transform_1__ctor_m26038_gshared/* 3706*/,
	(methodPointerType)&Transform_1_Invoke_m26039_gshared/* 3707*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26040_gshared/* 3708*/,
	(methodPointerType)&Transform_1_EndInvoke_m26041_gshared/* 3709*/,
	(methodPointerType)&Transform_1__ctor_m26042_gshared/* 3710*/,
	(methodPointerType)&Transform_1_Invoke_m26043_gshared/* 3711*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26044_gshared/* 3712*/,
	(methodPointerType)&Transform_1_EndInvoke_m26045_gshared/* 3713*/,
	(methodPointerType)&ShimEnumerator__ctor_m26046_gshared/* 3714*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m26047_gshared/* 3715*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m26048_gshared/* 3716*/,
	(methodPointerType)&ShimEnumerator_get_Key_m26049_gshared/* 3717*/,
	(methodPointerType)&ShimEnumerator_get_Value_m26050_gshared/* 3718*/,
	(methodPointerType)&ShimEnumerator_get_Current_m26051_gshared/* 3719*/,
	(methodPointerType)&ShimEnumerator_Reset_m26052_gshared/* 3720*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26053_gshared/* 3721*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26054_gshared/* 3722*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26055_gshared/* 3723*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26056_gshared/* 3724*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26057_gshared/* 3725*/,
	(methodPointerType)&DefaultComparer__ctor_m26058_gshared/* 3726*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26059_gshared/* 3727*/,
	(methodPointerType)&DefaultComparer_Equals_m26060_gshared/* 3728*/,
	(methodPointerType)&Dictionary_2__ctor_m26061_gshared/* 3729*/,
	(methodPointerType)&Dictionary_2__ctor_m26062_gshared/* 3730*/,
	(methodPointerType)&Dictionary_2__ctor_m26063_gshared/* 3731*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m26064_gshared/* 3732*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m26065_gshared/* 3733*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m26066_gshared/* 3734*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m26067_gshared/* 3735*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m26068_gshared/* 3736*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m26069_gshared/* 3737*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26070_gshared/* 3738*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26071_gshared/* 3739*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26072_gshared/* 3740*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26073_gshared/* 3741*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26074_gshared/* 3742*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26075_gshared/* 3743*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26076_gshared/* 3744*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m26077_gshared/* 3745*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26078_gshared/* 3746*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26079_gshared/* 3747*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26080_gshared/* 3748*/,
	(methodPointerType)&Dictionary_2_get_Count_m26081_gshared/* 3749*/,
	(methodPointerType)&Dictionary_2_get_Item_m26082_gshared/* 3750*/,
	(methodPointerType)&Dictionary_2_set_Item_m26083_gshared/* 3751*/,
	(methodPointerType)&Dictionary_2_Init_m26084_gshared/* 3752*/,
	(methodPointerType)&Dictionary_2_InitArrays_m26085_gshared/* 3753*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m26086_gshared/* 3754*/,
	(methodPointerType)&Dictionary_2_make_pair_m26087_gshared/* 3755*/,
	(methodPointerType)&Dictionary_2_pick_key_m26088_gshared/* 3756*/,
	(methodPointerType)&Dictionary_2_pick_value_m26089_gshared/* 3757*/,
	(methodPointerType)&Dictionary_2_CopyTo_m26090_gshared/* 3758*/,
	(methodPointerType)&Dictionary_2_Resize_m26091_gshared/* 3759*/,
	(methodPointerType)&Dictionary_2_Add_m26092_gshared/* 3760*/,
	(methodPointerType)&Dictionary_2_Clear_m26093_gshared/* 3761*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m26094_gshared/* 3762*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m26095_gshared/* 3763*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m26096_gshared/* 3764*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m26097_gshared/* 3765*/,
	(methodPointerType)&Dictionary_2_Remove_m26098_gshared/* 3766*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m26099_gshared/* 3767*/,
	(methodPointerType)&Dictionary_2_get_Keys_m26100_gshared/* 3768*/,
	(methodPointerType)&Dictionary_2_get_Values_m26101_gshared/* 3769*/,
	(methodPointerType)&Dictionary_2_ToTKey_m26102_gshared/* 3770*/,
	(methodPointerType)&Dictionary_2_ToTValue_m26103_gshared/* 3771*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m26104_gshared/* 3772*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m26105_gshared/* 3773*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m26106_gshared/* 3774*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26107_gshared/* 3775*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26108_gshared/* 3776*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26109_gshared/* 3777*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26110_gshared/* 3778*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26111_gshared/* 3779*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26112_gshared/* 3780*/,
	(methodPointerType)&KeyValuePair_2__ctor_m26113_gshared/* 3781*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m26114_gshared/* 3782*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m26115_gshared/* 3783*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m26116_gshared/* 3784*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m26117_gshared/* 3785*/,
	(methodPointerType)&KeyValuePair_2_ToString_m26118_gshared/* 3786*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26119_gshared/* 3787*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26120_gshared/* 3788*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26121_gshared/* 3789*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26122_gshared/* 3790*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26123_gshared/* 3791*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26124_gshared/* 3792*/,
	(methodPointerType)&KeyCollection__ctor_m26125_gshared/* 3793*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26126_gshared/* 3794*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26127_gshared/* 3795*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26128_gshared/* 3796*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26129_gshared/* 3797*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26130_gshared/* 3798*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m26131_gshared/* 3799*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26132_gshared/* 3800*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26133_gshared/* 3801*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26134_gshared/* 3802*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m26135_gshared/* 3803*/,
	(methodPointerType)&KeyCollection_CopyTo_m26136_gshared/* 3804*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m26137_gshared/* 3805*/,
	(methodPointerType)&KeyCollection_get_Count_m26138_gshared/* 3806*/,
	(methodPointerType)&Enumerator__ctor_m26139_gshared/* 3807*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26140_gshared/* 3808*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m26141_gshared/* 3809*/,
	(methodPointerType)&Enumerator_Dispose_m26142_gshared/* 3810*/,
	(methodPointerType)&Enumerator_MoveNext_m26143_gshared/* 3811*/,
	(methodPointerType)&Enumerator_get_Current_m26144_gshared/* 3812*/,
	(methodPointerType)&Enumerator__ctor_m26145_gshared/* 3813*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26146_gshared/* 3814*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m26147_gshared/* 3815*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26148_gshared/* 3816*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26149_gshared/* 3817*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26150_gshared/* 3818*/,
	(methodPointerType)&Enumerator_MoveNext_m26151_gshared/* 3819*/,
	(methodPointerType)&Enumerator_get_Current_m26152_gshared/* 3820*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m26153_gshared/* 3821*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m26154_gshared/* 3822*/,
	(methodPointerType)&Enumerator_Reset_m26155_gshared/* 3823*/,
	(methodPointerType)&Enumerator_VerifyState_m26156_gshared/* 3824*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m26157_gshared/* 3825*/,
	(methodPointerType)&Enumerator_Dispose_m26158_gshared/* 3826*/,
	(methodPointerType)&Transform_1__ctor_m26159_gshared/* 3827*/,
	(methodPointerType)&Transform_1_Invoke_m26160_gshared/* 3828*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26161_gshared/* 3829*/,
	(methodPointerType)&Transform_1_EndInvoke_m26162_gshared/* 3830*/,
	(methodPointerType)&ValueCollection__ctor_m26163_gshared/* 3831*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26164_gshared/* 3832*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26165_gshared/* 3833*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26166_gshared/* 3834*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26167_gshared/* 3835*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26168_gshared/* 3836*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m26169_gshared/* 3837*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26170_gshared/* 3838*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26171_gshared/* 3839*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26172_gshared/* 3840*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m26173_gshared/* 3841*/,
	(methodPointerType)&ValueCollection_CopyTo_m26174_gshared/* 3842*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m26175_gshared/* 3843*/,
	(methodPointerType)&ValueCollection_get_Count_m26176_gshared/* 3844*/,
	(methodPointerType)&Enumerator__ctor_m26177_gshared/* 3845*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26178_gshared/* 3846*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m26179_gshared/* 3847*/,
	(methodPointerType)&Enumerator_Dispose_m26180_gshared/* 3848*/,
	(methodPointerType)&Enumerator_MoveNext_m26181_gshared/* 3849*/,
	(methodPointerType)&Enumerator_get_Current_m26182_gshared/* 3850*/,
	(methodPointerType)&Transform_1__ctor_m26183_gshared/* 3851*/,
	(methodPointerType)&Transform_1_Invoke_m26184_gshared/* 3852*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26185_gshared/* 3853*/,
	(methodPointerType)&Transform_1_EndInvoke_m26186_gshared/* 3854*/,
	(methodPointerType)&Transform_1__ctor_m26187_gshared/* 3855*/,
	(methodPointerType)&Transform_1_Invoke_m26188_gshared/* 3856*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26189_gshared/* 3857*/,
	(methodPointerType)&Transform_1_EndInvoke_m26190_gshared/* 3858*/,
	(methodPointerType)&Transform_1__ctor_m26191_gshared/* 3859*/,
	(methodPointerType)&Transform_1_Invoke_m26192_gshared/* 3860*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26193_gshared/* 3861*/,
	(methodPointerType)&Transform_1_EndInvoke_m26194_gshared/* 3862*/,
	(methodPointerType)&ShimEnumerator__ctor_m26195_gshared/* 3863*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m26196_gshared/* 3864*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m26197_gshared/* 3865*/,
	(methodPointerType)&ShimEnumerator_get_Key_m26198_gshared/* 3866*/,
	(methodPointerType)&ShimEnumerator_get_Value_m26199_gshared/* 3867*/,
	(methodPointerType)&ShimEnumerator_get_Current_m26200_gshared/* 3868*/,
	(methodPointerType)&ShimEnumerator_Reset_m26201_gshared/* 3869*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26202_gshared/* 3870*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26203_gshared/* 3871*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26204_gshared/* 3872*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26205_gshared/* 3873*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26206_gshared/* 3874*/,
	(methodPointerType)&DefaultComparer__ctor_m26207_gshared/* 3875*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26208_gshared/* 3876*/,
	(methodPointerType)&DefaultComparer_Equals_m26209_gshared/* 3877*/,
	(methodPointerType)&List_1__ctor_m26302_gshared/* 3878*/,
	(methodPointerType)&List_1__ctor_m26303_gshared/* 3879*/,
	(methodPointerType)&List_1__cctor_m26304_gshared/* 3880*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26305_gshared/* 3881*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m26306_gshared/* 3882*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m26307_gshared/* 3883*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m26308_gshared/* 3884*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m26309_gshared/* 3885*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m26310_gshared/* 3886*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m26311_gshared/* 3887*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m26312_gshared/* 3888*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26313_gshared/* 3889*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m26314_gshared/* 3890*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m26315_gshared/* 3891*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m26316_gshared/* 3892*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m26317_gshared/* 3893*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m26318_gshared/* 3894*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m26319_gshared/* 3895*/,
	(methodPointerType)&List_1_Add_m26320_gshared/* 3896*/,
	(methodPointerType)&List_1_GrowIfNeeded_m26321_gshared/* 3897*/,
	(methodPointerType)&List_1_AddCollection_m26322_gshared/* 3898*/,
	(methodPointerType)&List_1_AddEnumerable_m26323_gshared/* 3899*/,
	(methodPointerType)&List_1_AddRange_m26324_gshared/* 3900*/,
	(methodPointerType)&List_1_AsReadOnly_m26325_gshared/* 3901*/,
	(methodPointerType)&List_1_Clear_m26326_gshared/* 3902*/,
	(methodPointerType)&List_1_Contains_m26327_gshared/* 3903*/,
	(methodPointerType)&List_1_CopyTo_m26328_gshared/* 3904*/,
	(methodPointerType)&List_1_Find_m26329_gshared/* 3905*/,
	(methodPointerType)&List_1_CheckMatch_m26330_gshared/* 3906*/,
	(methodPointerType)&List_1_GetIndex_m26331_gshared/* 3907*/,
	(methodPointerType)&List_1_GetEnumerator_m26332_gshared/* 3908*/,
	(methodPointerType)&List_1_IndexOf_m26333_gshared/* 3909*/,
	(methodPointerType)&List_1_Shift_m26334_gshared/* 3910*/,
	(methodPointerType)&List_1_CheckIndex_m26335_gshared/* 3911*/,
	(methodPointerType)&List_1_Insert_m26336_gshared/* 3912*/,
	(methodPointerType)&List_1_CheckCollection_m26337_gshared/* 3913*/,
	(methodPointerType)&List_1_Remove_m26338_gshared/* 3914*/,
	(methodPointerType)&List_1_RemoveAll_m26339_gshared/* 3915*/,
	(methodPointerType)&List_1_RemoveAt_m26340_gshared/* 3916*/,
	(methodPointerType)&List_1_Reverse_m26341_gshared/* 3917*/,
	(methodPointerType)&List_1_Sort_m26342_gshared/* 3918*/,
	(methodPointerType)&List_1_Sort_m26343_gshared/* 3919*/,
	(methodPointerType)&List_1_ToArray_m26344_gshared/* 3920*/,
	(methodPointerType)&List_1_TrimExcess_m26345_gshared/* 3921*/,
	(methodPointerType)&List_1_get_Capacity_m26346_gshared/* 3922*/,
	(methodPointerType)&List_1_set_Capacity_m26347_gshared/* 3923*/,
	(methodPointerType)&List_1_get_Count_m26348_gshared/* 3924*/,
	(methodPointerType)&List_1_get_Item_m26349_gshared/* 3925*/,
	(methodPointerType)&List_1_set_Item_m26350_gshared/* 3926*/,
	(methodPointerType)&Enumerator__ctor_m26351_gshared/* 3927*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m26352_gshared/* 3928*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26353_gshared/* 3929*/,
	(methodPointerType)&Enumerator_Dispose_m26354_gshared/* 3930*/,
	(methodPointerType)&Enumerator_VerifyState_m26355_gshared/* 3931*/,
	(methodPointerType)&Enumerator_MoveNext_m26356_gshared/* 3932*/,
	(methodPointerType)&Enumerator_get_Current_m26357_gshared/* 3933*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m26358_gshared/* 3934*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26359_gshared/* 3935*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m26360_gshared/* 3936*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m26361_gshared/* 3937*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m26362_gshared/* 3938*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m26363_gshared/* 3939*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m26364_gshared/* 3940*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m26365_gshared/* 3941*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26366_gshared/* 3942*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m26367_gshared/* 3943*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m26368_gshared/* 3944*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m26369_gshared/* 3945*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m26370_gshared/* 3946*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m26371_gshared/* 3947*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m26372_gshared/* 3948*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m26373_gshared/* 3949*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m26374_gshared/* 3950*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m26375_gshared/* 3951*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m26376_gshared/* 3952*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m26377_gshared/* 3953*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m26378_gshared/* 3954*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m26379_gshared/* 3955*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m26380_gshared/* 3956*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m26381_gshared/* 3957*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m26382_gshared/* 3958*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m26383_gshared/* 3959*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m26384_gshared/* 3960*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m26385_gshared/* 3961*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m26386_gshared/* 3962*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m26387_gshared/* 3963*/,
	(methodPointerType)&Collection_1__ctor_m26388_gshared/* 3964*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26389_gshared/* 3965*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m26390_gshared/* 3966*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m26391_gshared/* 3967*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m26392_gshared/* 3968*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m26393_gshared/* 3969*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m26394_gshared/* 3970*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m26395_gshared/* 3971*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m26396_gshared/* 3972*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m26397_gshared/* 3973*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m26398_gshared/* 3974*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m26399_gshared/* 3975*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m26400_gshared/* 3976*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m26401_gshared/* 3977*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m26402_gshared/* 3978*/,
	(methodPointerType)&Collection_1_Add_m26403_gshared/* 3979*/,
	(methodPointerType)&Collection_1_Clear_m26404_gshared/* 3980*/,
	(methodPointerType)&Collection_1_ClearItems_m26405_gshared/* 3981*/,
	(methodPointerType)&Collection_1_Contains_m26406_gshared/* 3982*/,
	(methodPointerType)&Collection_1_CopyTo_m26407_gshared/* 3983*/,
	(methodPointerType)&Collection_1_GetEnumerator_m26408_gshared/* 3984*/,
	(methodPointerType)&Collection_1_IndexOf_m26409_gshared/* 3985*/,
	(methodPointerType)&Collection_1_Insert_m26410_gshared/* 3986*/,
	(methodPointerType)&Collection_1_InsertItem_m26411_gshared/* 3987*/,
	(methodPointerType)&Collection_1_Remove_m26412_gshared/* 3988*/,
	(methodPointerType)&Collection_1_RemoveAt_m26413_gshared/* 3989*/,
	(methodPointerType)&Collection_1_RemoveItem_m26414_gshared/* 3990*/,
	(methodPointerType)&Collection_1_get_Count_m26415_gshared/* 3991*/,
	(methodPointerType)&Collection_1_get_Item_m26416_gshared/* 3992*/,
	(methodPointerType)&Collection_1_set_Item_m26417_gshared/* 3993*/,
	(methodPointerType)&Collection_1_SetItem_m26418_gshared/* 3994*/,
	(methodPointerType)&Collection_1_IsValidItem_m26419_gshared/* 3995*/,
	(methodPointerType)&Collection_1_ConvertItem_m26420_gshared/* 3996*/,
	(methodPointerType)&Collection_1_CheckWritable_m26421_gshared/* 3997*/,
	(methodPointerType)&Collection_1_IsSynchronized_m26422_gshared/* 3998*/,
	(methodPointerType)&Collection_1_IsFixedSize_m26423_gshared/* 3999*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26424_gshared/* 4000*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26425_gshared/* 4001*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26426_gshared/* 4002*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26427_gshared/* 4003*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26428_gshared/* 4004*/,
	(methodPointerType)&DefaultComparer__ctor_m26429_gshared/* 4005*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26430_gshared/* 4006*/,
	(methodPointerType)&DefaultComparer_Equals_m26431_gshared/* 4007*/,
	(methodPointerType)&Predicate_1__ctor_m26432_gshared/* 4008*/,
	(methodPointerType)&Predicate_1_Invoke_m26433_gshared/* 4009*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m26434_gshared/* 4010*/,
	(methodPointerType)&Predicate_1_EndInvoke_m26435_gshared/* 4011*/,
	(methodPointerType)&Comparer_1__ctor_m26436_gshared/* 4012*/,
	(methodPointerType)&Comparer_1__cctor_m26437_gshared/* 4013*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m26438_gshared/* 4014*/,
	(methodPointerType)&Comparer_1_get_Default_m26439_gshared/* 4015*/,
	(methodPointerType)&DefaultComparer__ctor_m26440_gshared/* 4016*/,
	(methodPointerType)&DefaultComparer_Compare_m26441_gshared/* 4017*/,
	(methodPointerType)&Comparison_1__ctor_m26442_gshared/* 4018*/,
	(methodPointerType)&Comparison_1_Invoke_m26443_gshared/* 4019*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m26444_gshared/* 4020*/,
	(methodPointerType)&Comparison_1_EndInvoke_m26445_gshared/* 4021*/,
	(methodPointerType)&Dictionary_2__ctor_m26546_gshared/* 4022*/,
	(methodPointerType)&Dictionary_2__ctor_m26548_gshared/* 4023*/,
	(methodPointerType)&Dictionary_2__ctor_m26550_gshared/* 4024*/,
	(methodPointerType)&Dictionary_2__ctor_m26552_gshared/* 4025*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m26554_gshared/* 4026*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m26556_gshared/* 4027*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m26558_gshared/* 4028*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m26560_gshared/* 4029*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m26562_gshared/* 4030*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m26564_gshared/* 4031*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26566_gshared/* 4032*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26568_gshared/* 4033*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26570_gshared/* 4034*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26572_gshared/* 4035*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26574_gshared/* 4036*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26576_gshared/* 4037*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26578_gshared/* 4038*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m26580_gshared/* 4039*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26582_gshared/* 4040*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26584_gshared/* 4041*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26586_gshared/* 4042*/,
	(methodPointerType)&Dictionary_2_get_Count_m26588_gshared/* 4043*/,
	(methodPointerType)&Dictionary_2_get_Item_m26590_gshared/* 4044*/,
	(methodPointerType)&Dictionary_2_set_Item_m26592_gshared/* 4045*/,
	(methodPointerType)&Dictionary_2_Init_m26594_gshared/* 4046*/,
	(methodPointerType)&Dictionary_2_InitArrays_m26596_gshared/* 4047*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m26598_gshared/* 4048*/,
	(methodPointerType)&Dictionary_2_make_pair_m26600_gshared/* 4049*/,
	(methodPointerType)&Dictionary_2_pick_key_m26602_gshared/* 4050*/,
	(methodPointerType)&Dictionary_2_pick_value_m26604_gshared/* 4051*/,
	(methodPointerType)&Dictionary_2_CopyTo_m26606_gshared/* 4052*/,
	(methodPointerType)&Dictionary_2_Resize_m26608_gshared/* 4053*/,
	(methodPointerType)&Dictionary_2_Add_m26610_gshared/* 4054*/,
	(methodPointerType)&Dictionary_2_Clear_m26612_gshared/* 4055*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m26614_gshared/* 4056*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m26616_gshared/* 4057*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m26618_gshared/* 4058*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m26620_gshared/* 4059*/,
	(methodPointerType)&Dictionary_2_Remove_m26622_gshared/* 4060*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m26624_gshared/* 4061*/,
	(methodPointerType)&Dictionary_2_get_Keys_m26626_gshared/* 4062*/,
	(methodPointerType)&Dictionary_2_get_Values_m26628_gshared/* 4063*/,
	(methodPointerType)&Dictionary_2_ToTKey_m26630_gshared/* 4064*/,
	(methodPointerType)&Dictionary_2_ToTValue_m26632_gshared/* 4065*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m26634_gshared/* 4066*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m26636_gshared/* 4067*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m26638_gshared/* 4068*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26639_gshared/* 4069*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26640_gshared/* 4070*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26641_gshared/* 4071*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26642_gshared/* 4072*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26643_gshared/* 4073*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26644_gshared/* 4074*/,
	(methodPointerType)&KeyValuePair_2__ctor_m26645_gshared/* 4075*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m26646_gshared/* 4076*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m26647_gshared/* 4077*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m26648_gshared/* 4078*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m26649_gshared/* 4079*/,
	(methodPointerType)&KeyValuePair_2_ToString_m26650_gshared/* 4080*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26651_gshared/* 4081*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26652_gshared/* 4082*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26653_gshared/* 4083*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26654_gshared/* 4084*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26655_gshared/* 4085*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26656_gshared/* 4086*/,
	(methodPointerType)&KeyCollection__ctor_m26657_gshared/* 4087*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26658_gshared/* 4088*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26659_gshared/* 4089*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26660_gshared/* 4090*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26661_gshared/* 4091*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26662_gshared/* 4092*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m26663_gshared/* 4093*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26664_gshared/* 4094*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26665_gshared/* 4095*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26666_gshared/* 4096*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m26667_gshared/* 4097*/,
	(methodPointerType)&KeyCollection_CopyTo_m26668_gshared/* 4098*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m26669_gshared/* 4099*/,
	(methodPointerType)&KeyCollection_get_Count_m26670_gshared/* 4100*/,
	(methodPointerType)&Enumerator__ctor_m26671_gshared/* 4101*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26672_gshared/* 4102*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m26673_gshared/* 4103*/,
	(methodPointerType)&Enumerator_Dispose_m26674_gshared/* 4104*/,
	(methodPointerType)&Enumerator_MoveNext_m26675_gshared/* 4105*/,
	(methodPointerType)&Enumerator_get_Current_m26676_gshared/* 4106*/,
	(methodPointerType)&Enumerator__ctor_m26677_gshared/* 4107*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26678_gshared/* 4108*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m26679_gshared/* 4109*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26680_gshared/* 4110*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26681_gshared/* 4111*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26682_gshared/* 4112*/,
	(methodPointerType)&Enumerator_MoveNext_m26683_gshared/* 4113*/,
	(methodPointerType)&Enumerator_get_Current_m26684_gshared/* 4114*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m26685_gshared/* 4115*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m26686_gshared/* 4116*/,
	(methodPointerType)&Enumerator_Reset_m26687_gshared/* 4117*/,
	(methodPointerType)&Enumerator_VerifyState_m26688_gshared/* 4118*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m26689_gshared/* 4119*/,
	(methodPointerType)&Enumerator_Dispose_m26690_gshared/* 4120*/,
	(methodPointerType)&Transform_1__ctor_m26691_gshared/* 4121*/,
	(methodPointerType)&Transform_1_Invoke_m26692_gshared/* 4122*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26693_gshared/* 4123*/,
	(methodPointerType)&Transform_1_EndInvoke_m26694_gshared/* 4124*/,
	(methodPointerType)&ValueCollection__ctor_m26695_gshared/* 4125*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26696_gshared/* 4126*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26697_gshared/* 4127*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26698_gshared/* 4128*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26699_gshared/* 4129*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26700_gshared/* 4130*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m26701_gshared/* 4131*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26702_gshared/* 4132*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26703_gshared/* 4133*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26704_gshared/* 4134*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m26705_gshared/* 4135*/,
	(methodPointerType)&ValueCollection_CopyTo_m26706_gshared/* 4136*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m26707_gshared/* 4137*/,
	(methodPointerType)&ValueCollection_get_Count_m26708_gshared/* 4138*/,
	(methodPointerType)&Enumerator__ctor_m26709_gshared/* 4139*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26710_gshared/* 4140*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m26711_gshared/* 4141*/,
	(methodPointerType)&Enumerator_Dispose_m26712_gshared/* 4142*/,
	(methodPointerType)&Enumerator_MoveNext_m26713_gshared/* 4143*/,
	(methodPointerType)&Enumerator_get_Current_m26714_gshared/* 4144*/,
	(methodPointerType)&Transform_1__ctor_m26715_gshared/* 4145*/,
	(methodPointerType)&Transform_1_Invoke_m26716_gshared/* 4146*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26717_gshared/* 4147*/,
	(methodPointerType)&Transform_1_EndInvoke_m26718_gshared/* 4148*/,
	(methodPointerType)&Transform_1__ctor_m26719_gshared/* 4149*/,
	(methodPointerType)&Transform_1_Invoke_m26720_gshared/* 4150*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26721_gshared/* 4151*/,
	(methodPointerType)&Transform_1_EndInvoke_m26722_gshared/* 4152*/,
	(methodPointerType)&Transform_1__ctor_m26723_gshared/* 4153*/,
	(methodPointerType)&Transform_1_Invoke_m26724_gshared/* 4154*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26725_gshared/* 4155*/,
	(methodPointerType)&Transform_1_EndInvoke_m26726_gshared/* 4156*/,
	(methodPointerType)&ShimEnumerator__ctor_m26727_gshared/* 4157*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m26728_gshared/* 4158*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m26729_gshared/* 4159*/,
	(methodPointerType)&ShimEnumerator_get_Key_m26730_gshared/* 4160*/,
	(methodPointerType)&ShimEnumerator_get_Value_m26731_gshared/* 4161*/,
	(methodPointerType)&ShimEnumerator_get_Current_m26732_gshared/* 4162*/,
	(methodPointerType)&ShimEnumerator_Reset_m26733_gshared/* 4163*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26734_gshared/* 4164*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26735_gshared/* 4165*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26736_gshared/* 4166*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26737_gshared/* 4167*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26738_gshared/* 4168*/,
	(methodPointerType)&DefaultComparer__ctor_m26739_gshared/* 4169*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26740_gshared/* 4170*/,
	(methodPointerType)&DefaultComparer_Equals_m26741_gshared/* 4171*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27294_gshared/* 4172*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27295_gshared/* 4173*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27296_gshared/* 4174*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27297_gshared/* 4175*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27298_gshared/* 4176*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27299_gshared/* 4177*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27419_gshared/* 4178*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27420_gshared/* 4179*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27421_gshared/* 4180*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27422_gshared/* 4181*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27423_gshared/* 4182*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27424_gshared/* 4183*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27558_gshared/* 4184*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27559_gshared/* 4185*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27560_gshared/* 4186*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27561_gshared/* 4187*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27562_gshared/* 4188*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27563_gshared/* 4189*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27570_gshared/* 4190*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27571_gshared/* 4191*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27572_gshared/* 4192*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27573_gshared/* 4193*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27574_gshared/* 4194*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27575_gshared/* 4195*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27582_gshared/* 4196*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27583_gshared/* 4197*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27584_gshared/* 4198*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27585_gshared/* 4199*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27586_gshared/* 4200*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27587_gshared/* 4201*/,
	(methodPointerType)&Dictionary_2__ctor_m27595_gshared/* 4202*/,
	(methodPointerType)&Dictionary_2__ctor_m27597_gshared/* 4203*/,
	(methodPointerType)&Dictionary_2__ctor_m27599_gshared/* 4204*/,
	(methodPointerType)&Dictionary_2__ctor_m27601_gshared/* 4205*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m27603_gshared/* 4206*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m27605_gshared/* 4207*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m27607_gshared/* 4208*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m27609_gshared/* 4209*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m27611_gshared/* 4210*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m27613_gshared/* 4211*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27615_gshared/* 4212*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27617_gshared/* 4213*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27619_gshared/* 4214*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27621_gshared/* 4215*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27623_gshared/* 4216*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27625_gshared/* 4217*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27627_gshared/* 4218*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m27629_gshared/* 4219*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27631_gshared/* 4220*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27633_gshared/* 4221*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27635_gshared/* 4222*/,
	(methodPointerType)&Dictionary_2_get_Count_m27637_gshared/* 4223*/,
	(methodPointerType)&Dictionary_2_get_Item_m27639_gshared/* 4224*/,
	(methodPointerType)&Dictionary_2_set_Item_m27641_gshared/* 4225*/,
	(methodPointerType)&Dictionary_2_Init_m27643_gshared/* 4226*/,
	(methodPointerType)&Dictionary_2_InitArrays_m27645_gshared/* 4227*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m27647_gshared/* 4228*/,
	(methodPointerType)&Dictionary_2_make_pair_m27649_gshared/* 4229*/,
	(methodPointerType)&Dictionary_2_pick_key_m27651_gshared/* 4230*/,
	(methodPointerType)&Dictionary_2_pick_value_m27653_gshared/* 4231*/,
	(methodPointerType)&Dictionary_2_CopyTo_m27655_gshared/* 4232*/,
	(methodPointerType)&Dictionary_2_Resize_m27657_gshared/* 4233*/,
	(methodPointerType)&Dictionary_2_Add_m27659_gshared/* 4234*/,
	(methodPointerType)&Dictionary_2_Clear_m27661_gshared/* 4235*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m27663_gshared/* 4236*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m27665_gshared/* 4237*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m27667_gshared/* 4238*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m27669_gshared/* 4239*/,
	(methodPointerType)&Dictionary_2_Remove_m27671_gshared/* 4240*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m27673_gshared/* 4241*/,
	(methodPointerType)&Dictionary_2_get_Keys_m27675_gshared/* 4242*/,
	(methodPointerType)&Dictionary_2_get_Values_m27677_gshared/* 4243*/,
	(methodPointerType)&Dictionary_2_ToTKey_m27679_gshared/* 4244*/,
	(methodPointerType)&Dictionary_2_ToTValue_m27681_gshared/* 4245*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m27683_gshared/* 4246*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m27685_gshared/* 4247*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m27687_gshared/* 4248*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27688_gshared/* 4249*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27689_gshared/* 4250*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27690_gshared/* 4251*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27691_gshared/* 4252*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27692_gshared/* 4253*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27693_gshared/* 4254*/,
	(methodPointerType)&KeyValuePair_2__ctor_m27694_gshared/* 4255*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m27695_gshared/* 4256*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m27696_gshared/* 4257*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m27697_gshared/* 4258*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m27698_gshared/* 4259*/,
	(methodPointerType)&KeyValuePair_2_ToString_m27699_gshared/* 4260*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27700_gshared/* 4261*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27701_gshared/* 4262*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27702_gshared/* 4263*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27703_gshared/* 4264*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27704_gshared/* 4265*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27705_gshared/* 4266*/,
	(methodPointerType)&KeyCollection__ctor_m27706_gshared/* 4267*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27707_gshared/* 4268*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27708_gshared/* 4269*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27709_gshared/* 4270*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27710_gshared/* 4271*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27711_gshared/* 4272*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m27712_gshared/* 4273*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27713_gshared/* 4274*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27714_gshared/* 4275*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27715_gshared/* 4276*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m27716_gshared/* 4277*/,
	(methodPointerType)&KeyCollection_CopyTo_m27717_gshared/* 4278*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m27718_gshared/* 4279*/,
	(methodPointerType)&KeyCollection_get_Count_m27719_gshared/* 4280*/,
	(methodPointerType)&Enumerator__ctor_m27720_gshared/* 4281*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27721_gshared/* 4282*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m27722_gshared/* 4283*/,
	(methodPointerType)&Enumerator_Dispose_m27723_gshared/* 4284*/,
	(methodPointerType)&Enumerator_MoveNext_m27724_gshared/* 4285*/,
	(methodPointerType)&Enumerator_get_Current_m27725_gshared/* 4286*/,
	(methodPointerType)&Enumerator__ctor_m27726_gshared/* 4287*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27727_gshared/* 4288*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m27728_gshared/* 4289*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27729_gshared/* 4290*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27730_gshared/* 4291*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27731_gshared/* 4292*/,
	(methodPointerType)&Enumerator_MoveNext_m27732_gshared/* 4293*/,
	(methodPointerType)&Enumerator_get_Current_m27733_gshared/* 4294*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m27734_gshared/* 4295*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m27735_gshared/* 4296*/,
	(methodPointerType)&Enumerator_Reset_m27736_gshared/* 4297*/,
	(methodPointerType)&Enumerator_VerifyState_m27737_gshared/* 4298*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m27738_gshared/* 4299*/,
	(methodPointerType)&Enumerator_Dispose_m27739_gshared/* 4300*/,
	(methodPointerType)&Transform_1__ctor_m27740_gshared/* 4301*/,
	(methodPointerType)&Transform_1_Invoke_m27741_gshared/* 4302*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27742_gshared/* 4303*/,
	(methodPointerType)&Transform_1_EndInvoke_m27743_gshared/* 4304*/,
	(methodPointerType)&ValueCollection__ctor_m27744_gshared/* 4305*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27745_gshared/* 4306*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27746_gshared/* 4307*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27747_gshared/* 4308*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27748_gshared/* 4309*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27749_gshared/* 4310*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m27750_gshared/* 4311*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27751_gshared/* 4312*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27752_gshared/* 4313*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27753_gshared/* 4314*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m27754_gshared/* 4315*/,
	(methodPointerType)&ValueCollection_CopyTo_m27755_gshared/* 4316*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m27756_gshared/* 4317*/,
	(methodPointerType)&ValueCollection_get_Count_m27757_gshared/* 4318*/,
	(methodPointerType)&Enumerator__ctor_m27758_gshared/* 4319*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27759_gshared/* 4320*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m27760_gshared/* 4321*/,
	(methodPointerType)&Enumerator_Dispose_m27761_gshared/* 4322*/,
	(methodPointerType)&Enumerator_MoveNext_m27762_gshared/* 4323*/,
	(methodPointerType)&Enumerator_get_Current_m27763_gshared/* 4324*/,
	(methodPointerType)&Transform_1__ctor_m27764_gshared/* 4325*/,
	(methodPointerType)&Transform_1_Invoke_m27765_gshared/* 4326*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27766_gshared/* 4327*/,
	(methodPointerType)&Transform_1_EndInvoke_m27767_gshared/* 4328*/,
	(methodPointerType)&Transform_1__ctor_m27768_gshared/* 4329*/,
	(methodPointerType)&Transform_1_Invoke_m27769_gshared/* 4330*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27770_gshared/* 4331*/,
	(methodPointerType)&Transform_1_EndInvoke_m27771_gshared/* 4332*/,
	(methodPointerType)&Transform_1__ctor_m27772_gshared/* 4333*/,
	(methodPointerType)&Transform_1_Invoke_m27773_gshared/* 4334*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27774_gshared/* 4335*/,
	(methodPointerType)&Transform_1_EndInvoke_m27775_gshared/* 4336*/,
	(methodPointerType)&ShimEnumerator__ctor_m27776_gshared/* 4337*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m27777_gshared/* 4338*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m27778_gshared/* 4339*/,
	(methodPointerType)&ShimEnumerator_get_Key_m27779_gshared/* 4340*/,
	(methodPointerType)&ShimEnumerator_get_Value_m27780_gshared/* 4341*/,
	(methodPointerType)&ShimEnumerator_get_Current_m27781_gshared/* 4342*/,
	(methodPointerType)&ShimEnumerator_Reset_m27782_gshared/* 4343*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27783_gshared/* 4344*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27784_gshared/* 4345*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27785_gshared/* 4346*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27786_gshared/* 4347*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27787_gshared/* 4348*/,
	(methodPointerType)&DefaultComparer__ctor_m27788_gshared/* 4349*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27789_gshared/* 4350*/,
	(methodPointerType)&DefaultComparer_Equals_m27790_gshared/* 4351*/,
	(methodPointerType)&List_1__ctor_m27843_gshared/* 4352*/,
	(methodPointerType)&List_1__ctor_m27844_gshared/* 4353*/,
	(methodPointerType)&List_1__cctor_m27845_gshared/* 4354*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27846_gshared/* 4355*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m27847_gshared/* 4356*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m27848_gshared/* 4357*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m27849_gshared/* 4358*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m27850_gshared/* 4359*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m27851_gshared/* 4360*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m27852_gshared/* 4361*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m27853_gshared/* 4362*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27854_gshared/* 4363*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m27855_gshared/* 4364*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m27856_gshared/* 4365*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m27857_gshared/* 4366*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m27858_gshared/* 4367*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m27859_gshared/* 4368*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m27860_gshared/* 4369*/,
	(methodPointerType)&List_1_Add_m27861_gshared/* 4370*/,
	(methodPointerType)&List_1_GrowIfNeeded_m27862_gshared/* 4371*/,
	(methodPointerType)&List_1_AddCollection_m27863_gshared/* 4372*/,
	(methodPointerType)&List_1_AddEnumerable_m27864_gshared/* 4373*/,
	(methodPointerType)&List_1_AddRange_m27865_gshared/* 4374*/,
	(methodPointerType)&List_1_AsReadOnly_m27866_gshared/* 4375*/,
	(methodPointerType)&List_1_Clear_m27867_gshared/* 4376*/,
	(methodPointerType)&List_1_Contains_m27868_gshared/* 4377*/,
	(methodPointerType)&List_1_CopyTo_m27869_gshared/* 4378*/,
	(methodPointerType)&List_1_Find_m27870_gshared/* 4379*/,
	(methodPointerType)&List_1_CheckMatch_m27871_gshared/* 4380*/,
	(methodPointerType)&List_1_GetIndex_m27872_gshared/* 4381*/,
	(methodPointerType)&List_1_GetEnumerator_m27873_gshared/* 4382*/,
	(methodPointerType)&List_1_IndexOf_m27874_gshared/* 4383*/,
	(methodPointerType)&List_1_Shift_m27875_gshared/* 4384*/,
	(methodPointerType)&List_1_CheckIndex_m27876_gshared/* 4385*/,
	(methodPointerType)&List_1_Insert_m27877_gshared/* 4386*/,
	(methodPointerType)&List_1_CheckCollection_m27878_gshared/* 4387*/,
	(methodPointerType)&List_1_Remove_m27879_gshared/* 4388*/,
	(methodPointerType)&List_1_RemoveAll_m27880_gshared/* 4389*/,
	(methodPointerType)&List_1_RemoveAt_m27881_gshared/* 4390*/,
	(methodPointerType)&List_1_Reverse_m27882_gshared/* 4391*/,
	(methodPointerType)&List_1_Sort_m27883_gshared/* 4392*/,
	(methodPointerType)&List_1_Sort_m27884_gshared/* 4393*/,
	(methodPointerType)&List_1_ToArray_m27885_gshared/* 4394*/,
	(methodPointerType)&List_1_TrimExcess_m27886_gshared/* 4395*/,
	(methodPointerType)&List_1_get_Capacity_m27887_gshared/* 4396*/,
	(methodPointerType)&List_1_set_Capacity_m27888_gshared/* 4397*/,
	(methodPointerType)&List_1_get_Count_m27889_gshared/* 4398*/,
	(methodPointerType)&List_1_get_Item_m27890_gshared/* 4399*/,
	(methodPointerType)&List_1_set_Item_m27891_gshared/* 4400*/,
	(methodPointerType)&Enumerator__ctor_m27892_gshared/* 4401*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m27893_gshared/* 4402*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27894_gshared/* 4403*/,
	(methodPointerType)&Enumerator_Dispose_m27895_gshared/* 4404*/,
	(methodPointerType)&Enumerator_VerifyState_m27896_gshared/* 4405*/,
	(methodPointerType)&Enumerator_MoveNext_m27897_gshared/* 4406*/,
	(methodPointerType)&Enumerator_get_Current_m27898_gshared/* 4407*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m27899_gshared/* 4408*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27900_gshared/* 4409*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m27901_gshared/* 4410*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m27902_gshared/* 4411*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m27903_gshared/* 4412*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m27904_gshared/* 4413*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m27905_gshared/* 4414*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m27906_gshared/* 4415*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27907_gshared/* 4416*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m27908_gshared/* 4417*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m27909_gshared/* 4418*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m27910_gshared/* 4419*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m27911_gshared/* 4420*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m27912_gshared/* 4421*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m27913_gshared/* 4422*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m27914_gshared/* 4423*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m27915_gshared/* 4424*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m27916_gshared/* 4425*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m27917_gshared/* 4426*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m27918_gshared/* 4427*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m27919_gshared/* 4428*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m27920_gshared/* 4429*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m27921_gshared/* 4430*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m27922_gshared/* 4431*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m27923_gshared/* 4432*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m27924_gshared/* 4433*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m27925_gshared/* 4434*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m27926_gshared/* 4435*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m27927_gshared/* 4436*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m27928_gshared/* 4437*/,
	(methodPointerType)&Collection_1__ctor_m27929_gshared/* 4438*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27930_gshared/* 4439*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m27931_gshared/* 4440*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m27932_gshared/* 4441*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m27933_gshared/* 4442*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m27934_gshared/* 4443*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m27935_gshared/* 4444*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m27936_gshared/* 4445*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m27937_gshared/* 4446*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m27938_gshared/* 4447*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m27939_gshared/* 4448*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m27940_gshared/* 4449*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m27941_gshared/* 4450*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m27942_gshared/* 4451*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m27943_gshared/* 4452*/,
	(methodPointerType)&Collection_1_Add_m27944_gshared/* 4453*/,
	(methodPointerType)&Collection_1_Clear_m27945_gshared/* 4454*/,
	(methodPointerType)&Collection_1_ClearItems_m27946_gshared/* 4455*/,
	(methodPointerType)&Collection_1_Contains_m27947_gshared/* 4456*/,
	(methodPointerType)&Collection_1_CopyTo_m27948_gshared/* 4457*/,
	(methodPointerType)&Collection_1_GetEnumerator_m27949_gshared/* 4458*/,
	(methodPointerType)&Collection_1_IndexOf_m27950_gshared/* 4459*/,
	(methodPointerType)&Collection_1_Insert_m27951_gshared/* 4460*/,
	(methodPointerType)&Collection_1_InsertItem_m27952_gshared/* 4461*/,
	(methodPointerType)&Collection_1_Remove_m27953_gshared/* 4462*/,
	(methodPointerType)&Collection_1_RemoveAt_m27954_gshared/* 4463*/,
	(methodPointerType)&Collection_1_RemoveItem_m27955_gshared/* 4464*/,
	(methodPointerType)&Collection_1_get_Count_m27956_gshared/* 4465*/,
	(methodPointerType)&Collection_1_get_Item_m27957_gshared/* 4466*/,
	(methodPointerType)&Collection_1_set_Item_m27958_gshared/* 4467*/,
	(methodPointerType)&Collection_1_SetItem_m27959_gshared/* 4468*/,
	(methodPointerType)&Collection_1_IsValidItem_m27960_gshared/* 4469*/,
	(methodPointerType)&Collection_1_ConvertItem_m27961_gshared/* 4470*/,
	(methodPointerType)&Collection_1_CheckWritable_m27962_gshared/* 4471*/,
	(methodPointerType)&Collection_1_IsSynchronized_m27963_gshared/* 4472*/,
	(methodPointerType)&Collection_1_IsFixedSize_m27964_gshared/* 4473*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27965_gshared/* 4474*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27966_gshared/* 4475*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27967_gshared/* 4476*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27968_gshared/* 4477*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27969_gshared/* 4478*/,
	(methodPointerType)&DefaultComparer__ctor_m27970_gshared/* 4479*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27971_gshared/* 4480*/,
	(methodPointerType)&DefaultComparer_Equals_m27972_gshared/* 4481*/,
	(methodPointerType)&Predicate_1__ctor_m27973_gshared/* 4482*/,
	(methodPointerType)&Predicate_1_Invoke_m27974_gshared/* 4483*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m27975_gshared/* 4484*/,
	(methodPointerType)&Predicate_1_EndInvoke_m27976_gshared/* 4485*/,
	(methodPointerType)&Comparer_1__ctor_m27977_gshared/* 4486*/,
	(methodPointerType)&Comparer_1__cctor_m27978_gshared/* 4487*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27979_gshared/* 4488*/,
	(methodPointerType)&Comparer_1_get_Default_m27980_gshared/* 4489*/,
	(methodPointerType)&DefaultComparer__ctor_m27981_gshared/* 4490*/,
	(methodPointerType)&DefaultComparer_Compare_m27982_gshared/* 4491*/,
	(methodPointerType)&Comparison_1__ctor_m27983_gshared/* 4492*/,
	(methodPointerType)&Comparison_1_Invoke_m27984_gshared/* 4493*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m27985_gshared/* 4494*/,
	(methodPointerType)&Comparison_1_EndInvoke_m27986_gshared/* 4495*/,
	(methodPointerType)&List_1__ctor_m27987_gshared/* 4496*/,
	(methodPointerType)&List_1__ctor_m27988_gshared/* 4497*/,
	(methodPointerType)&List_1__cctor_m27989_gshared/* 4498*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27990_gshared/* 4499*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m27991_gshared/* 4500*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m27992_gshared/* 4501*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m27993_gshared/* 4502*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m27994_gshared/* 4503*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m27995_gshared/* 4504*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m27996_gshared/* 4505*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m27997_gshared/* 4506*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27998_gshared/* 4507*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m27999_gshared/* 4508*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m28000_gshared/* 4509*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m28001_gshared/* 4510*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m28002_gshared/* 4511*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m28003_gshared/* 4512*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m28004_gshared/* 4513*/,
	(methodPointerType)&List_1_Add_m28005_gshared/* 4514*/,
	(methodPointerType)&List_1_GrowIfNeeded_m28006_gshared/* 4515*/,
	(methodPointerType)&List_1_AddCollection_m28007_gshared/* 4516*/,
	(methodPointerType)&List_1_AddEnumerable_m28008_gshared/* 4517*/,
	(methodPointerType)&List_1_AddRange_m28009_gshared/* 4518*/,
	(methodPointerType)&List_1_AsReadOnly_m28010_gshared/* 4519*/,
	(methodPointerType)&List_1_Clear_m28011_gshared/* 4520*/,
	(methodPointerType)&List_1_Contains_m28012_gshared/* 4521*/,
	(methodPointerType)&List_1_CopyTo_m28013_gshared/* 4522*/,
	(methodPointerType)&List_1_Find_m28014_gshared/* 4523*/,
	(methodPointerType)&List_1_CheckMatch_m28015_gshared/* 4524*/,
	(methodPointerType)&List_1_GetIndex_m28016_gshared/* 4525*/,
	(methodPointerType)&List_1_GetEnumerator_m28017_gshared/* 4526*/,
	(methodPointerType)&List_1_IndexOf_m28018_gshared/* 4527*/,
	(methodPointerType)&List_1_Shift_m28019_gshared/* 4528*/,
	(methodPointerType)&List_1_CheckIndex_m28020_gshared/* 4529*/,
	(methodPointerType)&List_1_Insert_m28021_gshared/* 4530*/,
	(methodPointerType)&List_1_CheckCollection_m28022_gshared/* 4531*/,
	(methodPointerType)&List_1_Remove_m28023_gshared/* 4532*/,
	(methodPointerType)&List_1_RemoveAll_m28024_gshared/* 4533*/,
	(methodPointerType)&List_1_RemoveAt_m28025_gshared/* 4534*/,
	(methodPointerType)&List_1_Reverse_m28026_gshared/* 4535*/,
	(methodPointerType)&List_1_Sort_m28027_gshared/* 4536*/,
	(methodPointerType)&List_1_Sort_m28028_gshared/* 4537*/,
	(methodPointerType)&List_1_ToArray_m28029_gshared/* 4538*/,
	(methodPointerType)&List_1_TrimExcess_m28030_gshared/* 4539*/,
	(methodPointerType)&List_1_get_Capacity_m28031_gshared/* 4540*/,
	(methodPointerType)&List_1_set_Capacity_m28032_gshared/* 4541*/,
	(methodPointerType)&List_1_get_Count_m28033_gshared/* 4542*/,
	(methodPointerType)&List_1_get_Item_m28034_gshared/* 4543*/,
	(methodPointerType)&List_1_set_Item_m28035_gshared/* 4544*/,
	(methodPointerType)&Enumerator__ctor_m28036_gshared/* 4545*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m28037_gshared/* 4546*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m28038_gshared/* 4547*/,
	(methodPointerType)&Enumerator_Dispose_m28039_gshared/* 4548*/,
	(methodPointerType)&Enumerator_VerifyState_m28040_gshared/* 4549*/,
	(methodPointerType)&Enumerator_MoveNext_m28041_gshared/* 4550*/,
	(methodPointerType)&Enumerator_get_Current_m28042_gshared/* 4551*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m28043_gshared/* 4552*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m28044_gshared/* 4553*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m28045_gshared/* 4554*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m28046_gshared/* 4555*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m28047_gshared/* 4556*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m28048_gshared/* 4557*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m28049_gshared/* 4558*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m28050_gshared/* 4559*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28051_gshared/* 4560*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m28052_gshared/* 4561*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m28053_gshared/* 4562*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m28054_gshared/* 4563*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m28055_gshared/* 4564*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m28056_gshared/* 4565*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m28057_gshared/* 4566*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m28058_gshared/* 4567*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m28059_gshared/* 4568*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m28060_gshared/* 4569*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m28061_gshared/* 4570*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m28062_gshared/* 4571*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m28063_gshared/* 4572*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m28064_gshared/* 4573*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m28065_gshared/* 4574*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m28066_gshared/* 4575*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m28067_gshared/* 4576*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m28068_gshared/* 4577*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m28069_gshared/* 4578*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m28070_gshared/* 4579*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m28071_gshared/* 4580*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m28072_gshared/* 4581*/,
	(methodPointerType)&Collection_1__ctor_m28073_gshared/* 4582*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28074_gshared/* 4583*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m28075_gshared/* 4584*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m28076_gshared/* 4585*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m28077_gshared/* 4586*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m28078_gshared/* 4587*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m28079_gshared/* 4588*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m28080_gshared/* 4589*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m28081_gshared/* 4590*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m28082_gshared/* 4591*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m28083_gshared/* 4592*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m28084_gshared/* 4593*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m28085_gshared/* 4594*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m28086_gshared/* 4595*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m28087_gshared/* 4596*/,
	(methodPointerType)&Collection_1_Add_m28088_gshared/* 4597*/,
	(methodPointerType)&Collection_1_Clear_m28089_gshared/* 4598*/,
	(methodPointerType)&Collection_1_ClearItems_m28090_gshared/* 4599*/,
	(methodPointerType)&Collection_1_Contains_m28091_gshared/* 4600*/,
	(methodPointerType)&Collection_1_CopyTo_m28092_gshared/* 4601*/,
	(methodPointerType)&Collection_1_GetEnumerator_m28093_gshared/* 4602*/,
	(methodPointerType)&Collection_1_IndexOf_m28094_gshared/* 4603*/,
	(methodPointerType)&Collection_1_Insert_m28095_gshared/* 4604*/,
	(methodPointerType)&Collection_1_InsertItem_m28096_gshared/* 4605*/,
	(methodPointerType)&Collection_1_Remove_m28097_gshared/* 4606*/,
	(methodPointerType)&Collection_1_RemoveAt_m28098_gshared/* 4607*/,
	(methodPointerType)&Collection_1_RemoveItem_m28099_gshared/* 4608*/,
	(methodPointerType)&Collection_1_get_Count_m28100_gshared/* 4609*/,
	(methodPointerType)&Collection_1_get_Item_m28101_gshared/* 4610*/,
	(methodPointerType)&Collection_1_set_Item_m28102_gshared/* 4611*/,
	(methodPointerType)&Collection_1_SetItem_m28103_gshared/* 4612*/,
	(methodPointerType)&Collection_1_IsValidItem_m28104_gshared/* 4613*/,
	(methodPointerType)&Collection_1_ConvertItem_m28105_gshared/* 4614*/,
	(methodPointerType)&Collection_1_CheckWritable_m28106_gshared/* 4615*/,
	(methodPointerType)&Collection_1_IsSynchronized_m28107_gshared/* 4616*/,
	(methodPointerType)&Collection_1_IsFixedSize_m28108_gshared/* 4617*/,
	(methodPointerType)&EqualityComparer_1__ctor_m28109_gshared/* 4618*/,
	(methodPointerType)&EqualityComparer_1__cctor_m28110_gshared/* 4619*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m28111_gshared/* 4620*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m28112_gshared/* 4621*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m28113_gshared/* 4622*/,
	(methodPointerType)&DefaultComparer__ctor_m28114_gshared/* 4623*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m28115_gshared/* 4624*/,
	(methodPointerType)&DefaultComparer_Equals_m28116_gshared/* 4625*/,
	(methodPointerType)&Predicate_1__ctor_m28117_gshared/* 4626*/,
	(methodPointerType)&Predicate_1_Invoke_m28118_gshared/* 4627*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m28119_gshared/* 4628*/,
	(methodPointerType)&Predicate_1_EndInvoke_m28120_gshared/* 4629*/,
	(methodPointerType)&Comparer_1__ctor_m28121_gshared/* 4630*/,
	(methodPointerType)&Comparer_1__cctor_m28122_gshared/* 4631*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m28123_gshared/* 4632*/,
	(methodPointerType)&Comparer_1_get_Default_m28124_gshared/* 4633*/,
	(methodPointerType)&DefaultComparer__ctor_m28125_gshared/* 4634*/,
	(methodPointerType)&DefaultComparer_Compare_m28126_gshared/* 4635*/,
	(methodPointerType)&Comparison_1__ctor_m28127_gshared/* 4636*/,
	(methodPointerType)&Comparison_1_Invoke_m28128_gshared/* 4637*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m28129_gshared/* 4638*/,
	(methodPointerType)&Comparison_1_EndInvoke_m28130_gshared/* 4639*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m28154_gshared/* 4640*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m28155_gshared/* 4641*/,
	(methodPointerType)&InvokableCall_1__ctor_m28156_gshared/* 4642*/,
	(methodPointerType)&InvokableCall_1__ctor_m28157_gshared/* 4643*/,
	(methodPointerType)&InvokableCall_1_Invoke_m28158_gshared/* 4644*/,
	(methodPointerType)&InvokableCall_1_Find_m28159_gshared/* 4645*/,
	(methodPointerType)&UnityAction_1__ctor_m28160_gshared/* 4646*/,
	(methodPointerType)&UnityAction_1_Invoke_m28161_gshared/* 4647*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m28162_gshared/* 4648*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m28163_gshared/* 4649*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m28169_gshared/* 4650*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m28455_gshared/* 4651*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m28456_gshared/* 4652*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28457_gshared/* 4653*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m28458_gshared/* 4654*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m28459_gshared/* 4655*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m28460_gshared/* 4656*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29046_gshared/* 4657*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29047_gshared/* 4658*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29048_gshared/* 4659*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29049_gshared/* 4660*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29050_gshared/* 4661*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29051_gshared/* 4662*/,
	(methodPointerType)&Dictionary_2__ctor_m29087_gshared/* 4663*/,
	(methodPointerType)&Dictionary_2__ctor_m29090_gshared/* 4664*/,
	(methodPointerType)&Dictionary_2__ctor_m29092_gshared/* 4665*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m29094_gshared/* 4666*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m29096_gshared/* 4667*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m29098_gshared/* 4668*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m29100_gshared/* 4669*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m29102_gshared/* 4670*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m29104_gshared/* 4671*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m29106_gshared/* 4672*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m29108_gshared/* 4673*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m29110_gshared/* 4674*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m29112_gshared/* 4675*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m29114_gshared/* 4676*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m29116_gshared/* 4677*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29118_gshared/* 4678*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m29120_gshared/* 4679*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m29122_gshared/* 4680*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m29124_gshared/* 4681*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m29126_gshared/* 4682*/,
	(methodPointerType)&Dictionary_2_get_Count_m29128_gshared/* 4683*/,
	(methodPointerType)&Dictionary_2_get_Item_m29130_gshared/* 4684*/,
	(methodPointerType)&Dictionary_2_set_Item_m29132_gshared/* 4685*/,
	(methodPointerType)&Dictionary_2_Init_m29134_gshared/* 4686*/,
	(methodPointerType)&Dictionary_2_InitArrays_m29136_gshared/* 4687*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m29138_gshared/* 4688*/,
	(methodPointerType)&Dictionary_2_make_pair_m29140_gshared/* 4689*/,
	(methodPointerType)&Dictionary_2_pick_key_m29142_gshared/* 4690*/,
	(methodPointerType)&Dictionary_2_pick_value_m29144_gshared/* 4691*/,
	(methodPointerType)&Dictionary_2_CopyTo_m29146_gshared/* 4692*/,
	(methodPointerType)&Dictionary_2_Resize_m29148_gshared/* 4693*/,
	(methodPointerType)&Dictionary_2_Add_m29150_gshared/* 4694*/,
	(methodPointerType)&Dictionary_2_Clear_m29152_gshared/* 4695*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m29154_gshared/* 4696*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m29156_gshared/* 4697*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m29158_gshared/* 4698*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m29160_gshared/* 4699*/,
	(methodPointerType)&Dictionary_2_Remove_m29162_gshared/* 4700*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m29164_gshared/* 4701*/,
	(methodPointerType)&Dictionary_2_get_Keys_m29166_gshared/* 4702*/,
	(methodPointerType)&Dictionary_2_get_Values_m29168_gshared/* 4703*/,
	(methodPointerType)&Dictionary_2_ToTKey_m29170_gshared/* 4704*/,
	(methodPointerType)&Dictionary_2_ToTValue_m29172_gshared/* 4705*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m29174_gshared/* 4706*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m29176_gshared/* 4707*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m29178_gshared/* 4708*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29179_gshared/* 4709*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29180_gshared/* 4710*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29181_gshared/* 4711*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29182_gshared/* 4712*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29183_gshared/* 4713*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29184_gshared/* 4714*/,
	(methodPointerType)&KeyValuePair_2__ctor_m29185_gshared/* 4715*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m29186_gshared/* 4716*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m29187_gshared/* 4717*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m29188_gshared/* 4718*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m29189_gshared/* 4719*/,
	(methodPointerType)&KeyValuePair_2_ToString_m29190_gshared/* 4720*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29191_gshared/* 4721*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29192_gshared/* 4722*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29193_gshared/* 4723*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29194_gshared/* 4724*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29195_gshared/* 4725*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29196_gshared/* 4726*/,
	(methodPointerType)&KeyCollection__ctor_m29197_gshared/* 4727*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m29198_gshared/* 4728*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m29199_gshared/* 4729*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m29200_gshared/* 4730*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m29201_gshared/* 4731*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m29202_gshared/* 4732*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m29203_gshared/* 4733*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m29204_gshared/* 4734*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m29205_gshared/* 4735*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m29206_gshared/* 4736*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m29207_gshared/* 4737*/,
	(methodPointerType)&KeyCollection_CopyTo_m29208_gshared/* 4738*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m29209_gshared/* 4739*/,
	(methodPointerType)&KeyCollection_get_Count_m29210_gshared/* 4740*/,
	(methodPointerType)&Enumerator__ctor_m29211_gshared/* 4741*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m29212_gshared/* 4742*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m29213_gshared/* 4743*/,
	(methodPointerType)&Enumerator_Dispose_m29214_gshared/* 4744*/,
	(methodPointerType)&Enumerator_MoveNext_m29215_gshared/* 4745*/,
	(methodPointerType)&Enumerator_get_Current_m29216_gshared/* 4746*/,
	(methodPointerType)&Enumerator__ctor_m29217_gshared/* 4747*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m29218_gshared/* 4748*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m29219_gshared/* 4749*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m29220_gshared/* 4750*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m29221_gshared/* 4751*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29222_gshared/* 4752*/,
	(methodPointerType)&Enumerator_MoveNext_m29223_gshared/* 4753*/,
	(methodPointerType)&Enumerator_get_Current_m29224_gshared/* 4754*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m29225_gshared/* 4755*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m29226_gshared/* 4756*/,
	(methodPointerType)&Enumerator_Reset_m29227_gshared/* 4757*/,
	(methodPointerType)&Enumerator_VerifyState_m29228_gshared/* 4758*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m29229_gshared/* 4759*/,
	(methodPointerType)&Enumerator_Dispose_m29230_gshared/* 4760*/,
	(methodPointerType)&Transform_1__ctor_m29231_gshared/* 4761*/,
	(methodPointerType)&Transform_1_Invoke_m29232_gshared/* 4762*/,
	(methodPointerType)&Transform_1_BeginInvoke_m29233_gshared/* 4763*/,
	(methodPointerType)&Transform_1_EndInvoke_m29234_gshared/* 4764*/,
	(methodPointerType)&ValueCollection__ctor_m29235_gshared/* 4765*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m29236_gshared/* 4766*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m29237_gshared/* 4767*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m29238_gshared/* 4768*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m29239_gshared/* 4769*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m29240_gshared/* 4770*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m29241_gshared/* 4771*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m29242_gshared/* 4772*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m29243_gshared/* 4773*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m29244_gshared/* 4774*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m29245_gshared/* 4775*/,
	(methodPointerType)&ValueCollection_CopyTo_m29246_gshared/* 4776*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m29247_gshared/* 4777*/,
	(methodPointerType)&ValueCollection_get_Count_m29248_gshared/* 4778*/,
	(methodPointerType)&Enumerator__ctor_m29249_gshared/* 4779*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m29250_gshared/* 4780*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m29251_gshared/* 4781*/,
	(methodPointerType)&Enumerator_Dispose_m29252_gshared/* 4782*/,
	(methodPointerType)&Enumerator_MoveNext_m29253_gshared/* 4783*/,
	(methodPointerType)&Enumerator_get_Current_m29254_gshared/* 4784*/,
	(methodPointerType)&Transform_1__ctor_m29255_gshared/* 4785*/,
	(methodPointerType)&Transform_1_Invoke_m29256_gshared/* 4786*/,
	(methodPointerType)&Transform_1_BeginInvoke_m29257_gshared/* 4787*/,
	(methodPointerType)&Transform_1_EndInvoke_m29258_gshared/* 4788*/,
	(methodPointerType)&Transform_1__ctor_m29259_gshared/* 4789*/,
	(methodPointerType)&Transform_1_Invoke_m29260_gshared/* 4790*/,
	(methodPointerType)&Transform_1_BeginInvoke_m29261_gshared/* 4791*/,
	(methodPointerType)&Transform_1_EndInvoke_m29262_gshared/* 4792*/,
	(methodPointerType)&Transform_1__ctor_m29263_gshared/* 4793*/,
	(methodPointerType)&Transform_1_Invoke_m29264_gshared/* 4794*/,
	(methodPointerType)&Transform_1_BeginInvoke_m29265_gshared/* 4795*/,
	(methodPointerType)&Transform_1_EndInvoke_m29266_gshared/* 4796*/,
	(methodPointerType)&ShimEnumerator__ctor_m29267_gshared/* 4797*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m29268_gshared/* 4798*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m29269_gshared/* 4799*/,
	(methodPointerType)&ShimEnumerator_get_Key_m29270_gshared/* 4800*/,
	(methodPointerType)&ShimEnumerator_get_Value_m29271_gshared/* 4801*/,
	(methodPointerType)&ShimEnumerator_get_Current_m29272_gshared/* 4802*/,
	(methodPointerType)&ShimEnumerator_Reset_m29273_gshared/* 4803*/,
	(methodPointerType)&EqualityComparer_1__ctor_m29274_gshared/* 4804*/,
	(methodPointerType)&EqualityComparer_1__cctor_m29275_gshared/* 4805*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29276_gshared/* 4806*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29277_gshared/* 4807*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m29278_gshared/* 4808*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m29279_gshared/* 4809*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m29280_gshared/* 4810*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m29281_gshared/* 4811*/,
	(methodPointerType)&DefaultComparer__ctor_m29282_gshared/* 4812*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m29283_gshared/* 4813*/,
	(methodPointerType)&DefaultComparer_Equals_m29284_gshared/* 4814*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29343_gshared/* 4815*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29344_gshared/* 4816*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29345_gshared/* 4817*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29346_gshared/* 4818*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29347_gshared/* 4819*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29348_gshared/* 4820*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29361_gshared/* 4821*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29362_gshared/* 4822*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29363_gshared/* 4823*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29364_gshared/* 4824*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29365_gshared/* 4825*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29366_gshared/* 4826*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29367_gshared/* 4827*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29368_gshared/* 4828*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29369_gshared/* 4829*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29370_gshared/* 4830*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29371_gshared/* 4831*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29372_gshared/* 4832*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29373_gshared/* 4833*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29374_gshared/* 4834*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29375_gshared/* 4835*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29376_gshared/* 4836*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29377_gshared/* 4837*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29378_gshared/* 4838*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29391_gshared/* 4839*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29392_gshared/* 4840*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29393_gshared/* 4841*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29394_gshared/* 4842*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29395_gshared/* 4843*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29396_gshared/* 4844*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29407_gshared/* 4845*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29408_gshared/* 4846*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29409_gshared/* 4847*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29410_gshared/* 4848*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29411_gshared/* 4849*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29412_gshared/* 4850*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29413_gshared/* 4851*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29414_gshared/* 4852*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29415_gshared/* 4853*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29416_gshared/* 4854*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29417_gshared/* 4855*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29418_gshared/* 4856*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29419_gshared/* 4857*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29420_gshared/* 4858*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29421_gshared/* 4859*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29422_gshared/* 4860*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29423_gshared/* 4861*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29424_gshared/* 4862*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29425_gshared/* 4863*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29426_gshared/* 4864*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_gshared/* 4865*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29428_gshared/* 4866*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29429_gshared/* 4867*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29430_gshared/* 4868*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29469_gshared/* 4869*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29470_gshared/* 4870*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29471_gshared/* 4871*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29472_gshared/* 4872*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29473_gshared/* 4873*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29474_gshared/* 4874*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29504_gshared/* 4875*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29505_gshared/* 4876*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29506_gshared/* 4877*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29507_gshared/* 4878*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29508_gshared/* 4879*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29509_gshared/* 4880*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29510_gshared/* 4881*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29511_gshared/* 4882*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29512_gshared/* 4883*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29513_gshared/* 4884*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29514_gshared/* 4885*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29515_gshared/* 4886*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29546_gshared/* 4887*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29547_gshared/* 4888*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29548_gshared/* 4889*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29549_gshared/* 4890*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29550_gshared/* 4891*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29551_gshared/* 4892*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29552_gshared/* 4893*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29553_gshared/* 4894*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29554_gshared/* 4895*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29555_gshared/* 4896*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29556_gshared/* 4897*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29557_gshared/* 4898*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29558_gshared/* 4899*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29559_gshared/* 4900*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29560_gshared/* 4901*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29561_gshared/* 4902*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29562_gshared/* 4903*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29563_gshared/* 4904*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29600_gshared/* 4905*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29601_gshared/* 4906*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29602_gshared/* 4907*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29603_gshared/* 4908*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29604_gshared/* 4909*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29605_gshared/* 4910*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29606_gshared/* 4911*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29607_gshared/* 4912*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29608_gshared/* 4913*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29609_gshared/* 4914*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29610_gshared/* 4915*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29611_gshared/* 4916*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m29612_gshared/* 4917*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29613_gshared/* 4918*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m29614_gshared/* 4919*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m29615_gshared/* 4920*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m29616_gshared/* 4921*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m29617_gshared/* 4922*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m29618_gshared/* 4923*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m29619_gshared/* 4924*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29620_gshared/* 4925*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m29621_gshared/* 4926*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m29622_gshared/* 4927*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m29623_gshared/* 4928*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m29624_gshared/* 4929*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m29625_gshared/* 4930*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m29626_gshared/* 4931*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m29627_gshared/* 4932*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m29628_gshared/* 4933*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m29629_gshared/* 4934*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m29630_gshared/* 4935*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m29631_gshared/* 4936*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m29632_gshared/* 4937*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m29633_gshared/* 4938*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m29634_gshared/* 4939*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m29635_gshared/* 4940*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m29636_gshared/* 4941*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m29637_gshared/* 4942*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m29638_gshared/* 4943*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m29639_gshared/* 4944*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m29640_gshared/* 4945*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m29641_gshared/* 4946*/,
	(methodPointerType)&Collection_1__ctor_m29642_gshared/* 4947*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29643_gshared/* 4948*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m29644_gshared/* 4949*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m29645_gshared/* 4950*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m29646_gshared/* 4951*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m29647_gshared/* 4952*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m29648_gshared/* 4953*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m29649_gshared/* 4954*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m29650_gshared/* 4955*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m29651_gshared/* 4956*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m29652_gshared/* 4957*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m29653_gshared/* 4958*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m29654_gshared/* 4959*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m29655_gshared/* 4960*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m29656_gshared/* 4961*/,
	(methodPointerType)&Collection_1_Add_m29657_gshared/* 4962*/,
	(methodPointerType)&Collection_1_Clear_m29658_gshared/* 4963*/,
	(methodPointerType)&Collection_1_ClearItems_m29659_gshared/* 4964*/,
	(methodPointerType)&Collection_1_Contains_m29660_gshared/* 4965*/,
	(methodPointerType)&Collection_1_CopyTo_m29661_gshared/* 4966*/,
	(methodPointerType)&Collection_1_GetEnumerator_m29662_gshared/* 4967*/,
	(methodPointerType)&Collection_1_IndexOf_m29663_gshared/* 4968*/,
	(methodPointerType)&Collection_1_Insert_m29664_gshared/* 4969*/,
	(methodPointerType)&Collection_1_InsertItem_m29665_gshared/* 4970*/,
	(methodPointerType)&Collection_1_Remove_m29666_gshared/* 4971*/,
	(methodPointerType)&Collection_1_RemoveAt_m29667_gshared/* 4972*/,
	(methodPointerType)&Collection_1_RemoveItem_m29668_gshared/* 4973*/,
	(methodPointerType)&Collection_1_get_Count_m29669_gshared/* 4974*/,
	(methodPointerType)&Collection_1_get_Item_m29670_gshared/* 4975*/,
	(methodPointerType)&Collection_1_set_Item_m29671_gshared/* 4976*/,
	(methodPointerType)&Collection_1_SetItem_m29672_gshared/* 4977*/,
	(methodPointerType)&Collection_1_IsValidItem_m29673_gshared/* 4978*/,
	(methodPointerType)&Collection_1_ConvertItem_m29674_gshared/* 4979*/,
	(methodPointerType)&Collection_1_CheckWritable_m29675_gshared/* 4980*/,
	(methodPointerType)&Collection_1_IsSynchronized_m29676_gshared/* 4981*/,
	(methodPointerType)&Collection_1_IsFixedSize_m29677_gshared/* 4982*/,
	(methodPointerType)&List_1__ctor_m29678_gshared/* 4983*/,
	(methodPointerType)&List_1__ctor_m29679_gshared/* 4984*/,
	(methodPointerType)&List_1__ctor_m29680_gshared/* 4985*/,
	(methodPointerType)&List_1__cctor_m29681_gshared/* 4986*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29682_gshared/* 4987*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m29683_gshared/* 4988*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m29684_gshared/* 4989*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m29685_gshared/* 4990*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m29686_gshared/* 4991*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m29687_gshared/* 4992*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m29688_gshared/* 4993*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m29689_gshared/* 4994*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29690_gshared/* 4995*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m29691_gshared/* 4996*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m29692_gshared/* 4997*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m29693_gshared/* 4998*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m29694_gshared/* 4999*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m29695_gshared/* 5000*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m29696_gshared/* 5001*/,
	(methodPointerType)&List_1_Add_m29697_gshared/* 5002*/,
	(methodPointerType)&List_1_GrowIfNeeded_m29698_gshared/* 5003*/,
	(methodPointerType)&List_1_AddCollection_m29699_gshared/* 5004*/,
	(methodPointerType)&List_1_AddEnumerable_m29700_gshared/* 5005*/,
	(methodPointerType)&List_1_AddRange_m29701_gshared/* 5006*/,
	(methodPointerType)&List_1_AsReadOnly_m29702_gshared/* 5007*/,
	(methodPointerType)&List_1_Clear_m29703_gshared/* 5008*/,
	(methodPointerType)&List_1_Contains_m29704_gshared/* 5009*/,
	(methodPointerType)&List_1_CopyTo_m29705_gshared/* 5010*/,
	(methodPointerType)&List_1_Find_m29706_gshared/* 5011*/,
	(methodPointerType)&List_1_CheckMatch_m29707_gshared/* 5012*/,
	(methodPointerType)&List_1_GetIndex_m29708_gshared/* 5013*/,
	(methodPointerType)&List_1_GetEnumerator_m29709_gshared/* 5014*/,
	(methodPointerType)&List_1_IndexOf_m29710_gshared/* 5015*/,
	(methodPointerType)&List_1_Shift_m29711_gshared/* 5016*/,
	(methodPointerType)&List_1_CheckIndex_m29712_gshared/* 5017*/,
	(methodPointerType)&List_1_Insert_m29713_gshared/* 5018*/,
	(methodPointerType)&List_1_CheckCollection_m29714_gshared/* 5019*/,
	(methodPointerType)&List_1_Remove_m29715_gshared/* 5020*/,
	(methodPointerType)&List_1_RemoveAll_m29716_gshared/* 5021*/,
	(methodPointerType)&List_1_RemoveAt_m29717_gshared/* 5022*/,
	(methodPointerType)&List_1_Reverse_m29718_gshared/* 5023*/,
	(methodPointerType)&List_1_Sort_m29719_gshared/* 5024*/,
	(methodPointerType)&List_1_Sort_m29720_gshared/* 5025*/,
	(methodPointerType)&List_1_ToArray_m29721_gshared/* 5026*/,
	(methodPointerType)&List_1_TrimExcess_m29722_gshared/* 5027*/,
	(methodPointerType)&List_1_get_Capacity_m29723_gshared/* 5028*/,
	(methodPointerType)&List_1_set_Capacity_m29724_gshared/* 5029*/,
	(methodPointerType)&List_1_get_Count_m29725_gshared/* 5030*/,
	(methodPointerType)&List_1_get_Item_m29726_gshared/* 5031*/,
	(methodPointerType)&List_1_set_Item_m29727_gshared/* 5032*/,
	(methodPointerType)&Enumerator__ctor_m29728_gshared/* 5033*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m29729_gshared/* 5034*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m29730_gshared/* 5035*/,
	(methodPointerType)&Enumerator_Dispose_m29731_gshared/* 5036*/,
	(methodPointerType)&Enumerator_VerifyState_m29732_gshared/* 5037*/,
	(methodPointerType)&Enumerator_MoveNext_m29733_gshared/* 5038*/,
	(methodPointerType)&Enumerator_get_Current_m29734_gshared/* 5039*/,
	(methodPointerType)&EqualityComparer_1__ctor_m29735_gshared/* 5040*/,
	(methodPointerType)&EqualityComparer_1__cctor_m29736_gshared/* 5041*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29737_gshared/* 5042*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29738_gshared/* 5043*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m29739_gshared/* 5044*/,
	(methodPointerType)&DefaultComparer__ctor_m29740_gshared/* 5045*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m29741_gshared/* 5046*/,
	(methodPointerType)&DefaultComparer_Equals_m29742_gshared/* 5047*/,
	(methodPointerType)&Predicate_1__ctor_m29743_gshared/* 5048*/,
	(methodPointerType)&Predicate_1_Invoke_m29744_gshared/* 5049*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m29745_gshared/* 5050*/,
	(methodPointerType)&Predicate_1_EndInvoke_m29746_gshared/* 5051*/,
	(methodPointerType)&Comparer_1__ctor_m29747_gshared/* 5052*/,
	(methodPointerType)&Comparer_1__cctor_m29748_gshared/* 5053*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m29749_gshared/* 5054*/,
	(methodPointerType)&Comparer_1_get_Default_m29750_gshared/* 5055*/,
	(methodPointerType)&DefaultComparer__ctor_m29751_gshared/* 5056*/,
	(methodPointerType)&DefaultComparer_Compare_m29752_gshared/* 5057*/,
	(methodPointerType)&Comparison_1__ctor_m29753_gshared/* 5058*/,
	(methodPointerType)&Comparison_1_Invoke_m29754_gshared/* 5059*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m29755_gshared/* 5060*/,
	(methodPointerType)&Comparison_1_EndInvoke_m29756_gshared/* 5061*/,
	(methodPointerType)&ArrayReadOnlyList_1__ctor_m29757_gshared/* 5062*/,
	(methodPointerType)&ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29758_gshared/* 5063*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Item_m29759_gshared/* 5064*/,
	(methodPointerType)&ArrayReadOnlyList_1_set_Item_m29760_gshared/* 5065*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Count_m29761_gshared/* 5066*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_IsReadOnly_m29762_gshared/* 5067*/,
	(methodPointerType)&ArrayReadOnlyList_1_Add_m29763_gshared/* 5068*/,
	(methodPointerType)&ArrayReadOnlyList_1_Clear_m29764_gshared/* 5069*/,
	(methodPointerType)&ArrayReadOnlyList_1_Contains_m29765_gshared/* 5070*/,
	(methodPointerType)&ArrayReadOnlyList_1_CopyTo_m29766_gshared/* 5071*/,
	(methodPointerType)&ArrayReadOnlyList_1_GetEnumerator_m29767_gshared/* 5072*/,
	(methodPointerType)&ArrayReadOnlyList_1_IndexOf_m29768_gshared/* 5073*/,
	(methodPointerType)&ArrayReadOnlyList_1_Insert_m29769_gshared/* 5074*/,
	(methodPointerType)&ArrayReadOnlyList_1_Remove_m29770_gshared/* 5075*/,
	(methodPointerType)&ArrayReadOnlyList_1_RemoveAt_m29771_gshared/* 5076*/,
	(methodPointerType)&ArrayReadOnlyList_1_ReadOnlyError_m29772_gshared/* 5077*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0__ctor_m29773_gshared/* 5078*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29774_gshared/* 5079*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29775_gshared/* 5080*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29776_gshared/* 5081*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29777_gshared/* 5082*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Reset_m29778_gshared/* 5083*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m29779_gshared/* 5084*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29780_gshared/* 5085*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m29781_gshared/* 5086*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m29782_gshared/* 5087*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m29783_gshared/* 5088*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m29784_gshared/* 5089*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m29785_gshared/* 5090*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m29786_gshared/* 5091*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29787_gshared/* 5092*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m29788_gshared/* 5093*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m29789_gshared/* 5094*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m29790_gshared/* 5095*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m29791_gshared/* 5096*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m29792_gshared/* 5097*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m29793_gshared/* 5098*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m29794_gshared/* 5099*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m29795_gshared/* 5100*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m29796_gshared/* 5101*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m29797_gshared/* 5102*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m29798_gshared/* 5103*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m29799_gshared/* 5104*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m29800_gshared/* 5105*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m29801_gshared/* 5106*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m29802_gshared/* 5107*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m29803_gshared/* 5108*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m29804_gshared/* 5109*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m29805_gshared/* 5110*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m29806_gshared/* 5111*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m29807_gshared/* 5112*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m29808_gshared/* 5113*/,
	(methodPointerType)&Collection_1__ctor_m29809_gshared/* 5114*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29810_gshared/* 5115*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m29811_gshared/* 5116*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m29812_gshared/* 5117*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m29813_gshared/* 5118*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m29814_gshared/* 5119*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m29815_gshared/* 5120*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m29816_gshared/* 5121*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m29817_gshared/* 5122*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m29818_gshared/* 5123*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m29819_gshared/* 5124*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m29820_gshared/* 5125*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m29821_gshared/* 5126*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m29822_gshared/* 5127*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m29823_gshared/* 5128*/,
	(methodPointerType)&Collection_1_Add_m29824_gshared/* 5129*/,
	(methodPointerType)&Collection_1_Clear_m29825_gshared/* 5130*/,
	(methodPointerType)&Collection_1_ClearItems_m29826_gshared/* 5131*/,
	(methodPointerType)&Collection_1_Contains_m29827_gshared/* 5132*/,
	(methodPointerType)&Collection_1_CopyTo_m29828_gshared/* 5133*/,
	(methodPointerType)&Collection_1_GetEnumerator_m29829_gshared/* 5134*/,
	(methodPointerType)&Collection_1_IndexOf_m29830_gshared/* 5135*/,
	(methodPointerType)&Collection_1_Insert_m29831_gshared/* 5136*/,
	(methodPointerType)&Collection_1_InsertItem_m29832_gshared/* 5137*/,
	(methodPointerType)&Collection_1_Remove_m29833_gshared/* 5138*/,
	(methodPointerType)&Collection_1_RemoveAt_m29834_gshared/* 5139*/,
	(methodPointerType)&Collection_1_RemoveItem_m29835_gshared/* 5140*/,
	(methodPointerType)&Collection_1_get_Count_m29836_gshared/* 5141*/,
	(methodPointerType)&Collection_1_get_Item_m29837_gshared/* 5142*/,
	(methodPointerType)&Collection_1_set_Item_m29838_gshared/* 5143*/,
	(methodPointerType)&Collection_1_SetItem_m29839_gshared/* 5144*/,
	(methodPointerType)&Collection_1_IsValidItem_m29840_gshared/* 5145*/,
	(methodPointerType)&Collection_1_ConvertItem_m29841_gshared/* 5146*/,
	(methodPointerType)&Collection_1_CheckWritable_m29842_gshared/* 5147*/,
	(methodPointerType)&Collection_1_IsSynchronized_m29843_gshared/* 5148*/,
	(methodPointerType)&Collection_1_IsFixedSize_m29844_gshared/* 5149*/,
	(methodPointerType)&List_1__ctor_m29845_gshared/* 5150*/,
	(methodPointerType)&List_1__ctor_m29846_gshared/* 5151*/,
	(methodPointerType)&List_1__ctor_m29847_gshared/* 5152*/,
	(methodPointerType)&List_1__cctor_m29848_gshared/* 5153*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29849_gshared/* 5154*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m29850_gshared/* 5155*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m29851_gshared/* 5156*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m29852_gshared/* 5157*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m29853_gshared/* 5158*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m29854_gshared/* 5159*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m29855_gshared/* 5160*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m29856_gshared/* 5161*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29857_gshared/* 5162*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m29858_gshared/* 5163*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m29859_gshared/* 5164*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m29860_gshared/* 5165*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m29861_gshared/* 5166*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m29862_gshared/* 5167*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m29863_gshared/* 5168*/,
	(methodPointerType)&List_1_Add_m29864_gshared/* 5169*/,
	(methodPointerType)&List_1_GrowIfNeeded_m29865_gshared/* 5170*/,
	(methodPointerType)&List_1_AddCollection_m29866_gshared/* 5171*/,
	(methodPointerType)&List_1_AddEnumerable_m29867_gshared/* 5172*/,
	(methodPointerType)&List_1_AddRange_m29868_gshared/* 5173*/,
	(methodPointerType)&List_1_AsReadOnly_m29869_gshared/* 5174*/,
	(methodPointerType)&List_1_Clear_m29870_gshared/* 5175*/,
	(methodPointerType)&List_1_Contains_m29871_gshared/* 5176*/,
	(methodPointerType)&List_1_CopyTo_m29872_gshared/* 5177*/,
	(methodPointerType)&List_1_Find_m29873_gshared/* 5178*/,
	(methodPointerType)&List_1_CheckMatch_m29874_gshared/* 5179*/,
	(methodPointerType)&List_1_GetIndex_m29875_gshared/* 5180*/,
	(methodPointerType)&List_1_GetEnumerator_m29876_gshared/* 5181*/,
	(methodPointerType)&List_1_IndexOf_m29877_gshared/* 5182*/,
	(methodPointerType)&List_1_Shift_m29878_gshared/* 5183*/,
	(methodPointerType)&List_1_CheckIndex_m29879_gshared/* 5184*/,
	(methodPointerType)&List_1_Insert_m29880_gshared/* 5185*/,
	(methodPointerType)&List_1_CheckCollection_m29881_gshared/* 5186*/,
	(methodPointerType)&List_1_Remove_m29882_gshared/* 5187*/,
	(methodPointerType)&List_1_RemoveAll_m29883_gshared/* 5188*/,
	(methodPointerType)&List_1_RemoveAt_m29884_gshared/* 5189*/,
	(methodPointerType)&List_1_Reverse_m29885_gshared/* 5190*/,
	(methodPointerType)&List_1_Sort_m29886_gshared/* 5191*/,
	(methodPointerType)&List_1_Sort_m29887_gshared/* 5192*/,
	(methodPointerType)&List_1_ToArray_m29888_gshared/* 5193*/,
	(methodPointerType)&List_1_TrimExcess_m29889_gshared/* 5194*/,
	(methodPointerType)&List_1_get_Capacity_m29890_gshared/* 5195*/,
	(methodPointerType)&List_1_set_Capacity_m29891_gshared/* 5196*/,
	(methodPointerType)&List_1_get_Count_m29892_gshared/* 5197*/,
	(methodPointerType)&List_1_get_Item_m29893_gshared/* 5198*/,
	(methodPointerType)&List_1_set_Item_m29894_gshared/* 5199*/,
	(methodPointerType)&Enumerator__ctor_m29895_gshared/* 5200*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_Reset_m29896_gshared/* 5201*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m29897_gshared/* 5202*/,
	(methodPointerType)&Enumerator_Dispose_m29898_gshared/* 5203*/,
	(methodPointerType)&Enumerator_VerifyState_m29899_gshared/* 5204*/,
	(methodPointerType)&Enumerator_MoveNext_m29900_gshared/* 5205*/,
	(methodPointerType)&Enumerator_get_Current_m29901_gshared/* 5206*/,
	(methodPointerType)&EqualityComparer_1__ctor_m29902_gshared/* 5207*/,
	(methodPointerType)&EqualityComparer_1__cctor_m29903_gshared/* 5208*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m29904_gshared/* 5209*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m29905_gshared/* 5210*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m29906_gshared/* 5211*/,
	(methodPointerType)&DefaultComparer__ctor_m29907_gshared/* 5212*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m29908_gshared/* 5213*/,
	(methodPointerType)&DefaultComparer_Equals_m29909_gshared/* 5214*/,
	(methodPointerType)&Predicate_1__ctor_m29910_gshared/* 5215*/,
	(methodPointerType)&Predicate_1_Invoke_m29911_gshared/* 5216*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m29912_gshared/* 5217*/,
	(methodPointerType)&Predicate_1_EndInvoke_m29913_gshared/* 5218*/,
	(methodPointerType)&Comparer_1__ctor_m29914_gshared/* 5219*/,
	(methodPointerType)&Comparer_1__cctor_m29915_gshared/* 5220*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m29916_gshared/* 5221*/,
	(methodPointerType)&Comparer_1_get_Default_m29917_gshared/* 5222*/,
	(methodPointerType)&DefaultComparer__ctor_m29918_gshared/* 5223*/,
	(methodPointerType)&DefaultComparer_Compare_m29919_gshared/* 5224*/,
	(methodPointerType)&Comparison_1__ctor_m29920_gshared/* 5225*/,
	(methodPointerType)&Comparison_1_Invoke_m29921_gshared/* 5226*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m29922_gshared/* 5227*/,
	(methodPointerType)&Comparison_1_EndInvoke_m29923_gshared/* 5228*/,
	(methodPointerType)&ArrayReadOnlyList_1__ctor_m29924_gshared/* 5229*/,
	(methodPointerType)&ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29925_gshared/* 5230*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Item_m29926_gshared/* 5231*/,
	(methodPointerType)&ArrayReadOnlyList_1_set_Item_m29927_gshared/* 5232*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Count_m29928_gshared/* 5233*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_IsReadOnly_m29929_gshared/* 5234*/,
	(methodPointerType)&ArrayReadOnlyList_1_Add_m29930_gshared/* 5235*/,
	(methodPointerType)&ArrayReadOnlyList_1_Clear_m29931_gshared/* 5236*/,
	(methodPointerType)&ArrayReadOnlyList_1_Contains_m29932_gshared/* 5237*/,
	(methodPointerType)&ArrayReadOnlyList_1_CopyTo_m29933_gshared/* 5238*/,
	(methodPointerType)&ArrayReadOnlyList_1_GetEnumerator_m29934_gshared/* 5239*/,
	(methodPointerType)&ArrayReadOnlyList_1_IndexOf_m29935_gshared/* 5240*/,
	(methodPointerType)&ArrayReadOnlyList_1_Insert_m29936_gshared/* 5241*/,
	(methodPointerType)&ArrayReadOnlyList_1_Remove_m29937_gshared/* 5242*/,
	(methodPointerType)&ArrayReadOnlyList_1_RemoveAt_m29938_gshared/* 5243*/,
	(methodPointerType)&ArrayReadOnlyList_1_ReadOnlyError_m29939_gshared/* 5244*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0__ctor_m29940_gshared/* 5245*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29941_gshared/* 5246*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29942_gshared/* 5247*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29943_gshared/* 5248*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29944_gshared/* 5249*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Reset_m29945_gshared/* 5250*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29954_gshared/* 5251*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29955_gshared/* 5252*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29956_gshared/* 5253*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29957_gshared/* 5254*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29958_gshared/* 5255*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29959_gshared/* 5256*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29960_gshared/* 5257*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29961_gshared/* 5258*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29962_gshared/* 5259*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29963_gshared/* 5260*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29964_gshared/* 5261*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29965_gshared/* 5262*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29990_gshared/* 5263*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29991_gshared/* 5264*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29992_gshared/* 5265*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29993_gshared/* 5266*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m29994_gshared/* 5267*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m29995_gshared/* 5268*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m29996_gshared/* 5269*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29997_gshared/* 5270*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29998_gshared/* 5271*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m29999_gshared/* 5272*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m30000_gshared/* 5273*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m30001_gshared/* 5274*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m30002_gshared/* 5275*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m30003_gshared/* 5276*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30004_gshared/* 5277*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m30005_gshared/* 5278*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m30006_gshared/* 5279*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m30007_gshared/* 5280*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m30008_gshared/* 5281*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_Reset_m30009_gshared/* 5282*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30010_gshared/* 5283*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m30011_gshared/* 5284*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m30012_gshared/* 5285*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m30013_gshared/* 5286*/,
	(methodPointerType)&GenericComparer_1_Compare_m30114_gshared/* 5287*/,
	(methodPointerType)&Comparer_1__ctor_m30115_gshared/* 5288*/,
	(methodPointerType)&Comparer_1__cctor_m30116_gshared/* 5289*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m30117_gshared/* 5290*/,
	(methodPointerType)&Comparer_1_get_Default_m30118_gshared/* 5291*/,
	(methodPointerType)&DefaultComparer__ctor_m30119_gshared/* 5292*/,
	(methodPointerType)&DefaultComparer_Compare_m30120_gshared/* 5293*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m30121_gshared/* 5294*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m30122_gshared/* 5295*/,
	(methodPointerType)&EqualityComparer_1__ctor_m30123_gshared/* 5296*/,
	(methodPointerType)&EqualityComparer_1__cctor_m30124_gshared/* 5297*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m30125_gshared/* 5298*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m30126_gshared/* 5299*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m30127_gshared/* 5300*/,
	(methodPointerType)&DefaultComparer__ctor_m30128_gshared/* 5301*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m30129_gshared/* 5302*/,
	(methodPointerType)&DefaultComparer_Equals_m30130_gshared/* 5303*/,
	(methodPointerType)&GenericComparer_1_Compare_m30131_gshared/* 5304*/,
	(methodPointerType)&Comparer_1__ctor_m30132_gshared/* 5305*/,
	(methodPointerType)&Comparer_1__cctor_m30133_gshared/* 5306*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m30134_gshared/* 5307*/,
	(methodPointerType)&Comparer_1_get_Default_m30135_gshared/* 5308*/,
	(methodPointerType)&DefaultComparer__ctor_m30136_gshared/* 5309*/,
	(methodPointerType)&DefaultComparer_Compare_m30137_gshared/* 5310*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m30138_gshared/* 5311*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m30139_gshared/* 5312*/,
	(methodPointerType)&EqualityComparer_1__ctor_m30140_gshared/* 5313*/,
	(methodPointerType)&EqualityComparer_1__cctor_m30141_gshared/* 5314*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m30142_gshared/* 5315*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m30143_gshared/* 5316*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m30144_gshared/* 5317*/,
	(methodPointerType)&DefaultComparer__ctor_m30145_gshared/* 5318*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m30146_gshared/* 5319*/,
	(methodPointerType)&DefaultComparer_Equals_m30147_gshared/* 5320*/,
	(methodPointerType)&Nullable_1_Equals_m30148_gshared/* 5321*/,
	(methodPointerType)&Nullable_1_Equals_m30149_gshared/* 5322*/,
	(methodPointerType)&Nullable_1_GetHashCode_m30150_gshared/* 5323*/,
	(methodPointerType)&Nullable_1_GetValueOrDefault_m30151_gshared/* 5324*/,
	(methodPointerType)&Nullable_1_ToString_m30152_gshared/* 5325*/,
	(methodPointerType)&GenericComparer_1_Compare_m30153_gshared/* 5326*/,
	(methodPointerType)&Comparer_1__ctor_m30154_gshared/* 5327*/,
	(methodPointerType)&Comparer_1__cctor_m30155_gshared/* 5328*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m30156_gshared/* 5329*/,
	(methodPointerType)&Comparer_1_get_Default_m30157_gshared/* 5330*/,
	(methodPointerType)&DefaultComparer__ctor_m30158_gshared/* 5331*/,
	(methodPointerType)&DefaultComparer_Compare_m30159_gshared/* 5332*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m30160_gshared/* 5333*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m30161_gshared/* 5334*/,
	(methodPointerType)&EqualityComparer_1__ctor_m30162_gshared/* 5335*/,
	(methodPointerType)&EqualityComparer_1__cctor_m30163_gshared/* 5336*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m30164_gshared/* 5337*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m30165_gshared/* 5338*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m30166_gshared/* 5339*/,
	(methodPointerType)&DefaultComparer__ctor_m30167_gshared/* 5340*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m30168_gshared/* 5341*/,
	(methodPointerType)&DefaultComparer_Equals_m30169_gshared/* 5342*/,
	(methodPointerType)&GenericComparer_1_Compare_m30206_gshared/* 5343*/,
	(methodPointerType)&Comparer_1__ctor_m30207_gshared/* 5344*/,
	(methodPointerType)&Comparer_1__cctor_m30208_gshared/* 5345*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m30209_gshared/* 5346*/,
	(methodPointerType)&Comparer_1_get_Default_m30210_gshared/* 5347*/,
	(methodPointerType)&DefaultComparer__ctor_m30211_gshared/* 5348*/,
	(methodPointerType)&DefaultComparer_Compare_m30212_gshared/* 5349*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m30213_gshared/* 5350*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m30214_gshared/* 5351*/,
	(methodPointerType)&EqualityComparer_1__ctor_m30215_gshared/* 5352*/,
	(methodPointerType)&EqualityComparer_1__cctor_m30216_gshared/* 5353*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m30217_gshared/* 5354*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m30218_gshared/* 5355*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m30219_gshared/* 5356*/,
	(methodPointerType)&DefaultComparer__ctor_m30220_gshared/* 5357*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m30221_gshared/* 5358*/,
	(methodPointerType)&DefaultComparer_Equals_m30222_gshared/* 5359*/,
};
