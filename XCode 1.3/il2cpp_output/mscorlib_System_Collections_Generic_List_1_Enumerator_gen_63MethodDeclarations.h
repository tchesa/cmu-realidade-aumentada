﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t1054;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_63.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m26351_gshared (Enumerator_t3274 * __this, List_1_t1054 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m26351(__this, ___l, method) (( void (*) (Enumerator_t3274 *, List_1_t1054 *, const MethodInfo*))Enumerator__ctor_m26351_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26352_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m26352(__this, method) (( void (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m26352_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26353_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26353(__this, method) (( Object_t * (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26353_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void Enumerator_Dispose_m26354_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26354(__this, method) (( void (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_Dispose_m26354_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m26355_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m26355(__this, method) (( void (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_VerifyState_m26355_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26356_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26356(__this, method) (( bool (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_MoveNext_m26356_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t1050  Enumerator_get_Current_m26357_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26357(__this, method) (( TargetSearchResult_t1050  (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_get_Current_m26357_gshared)(__this, method)
