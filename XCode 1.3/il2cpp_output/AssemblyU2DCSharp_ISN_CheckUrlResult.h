﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ISN_Result.h"

// ISN_CheckUrlResult
struct  ISN_CheckUrlResult_t156  : public ISN_Result_t114
{
	// System.String ISN_CheckUrlResult::_url
	String_t* ____url_2;
};
