﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// IOSStoreKitVerificationResponse
struct  IOSStoreKitVerificationResponse_t138  : public Object_t
{
	// System.Int32 IOSStoreKitVerificationResponse::status
	int32_t ___status_0;
	// System.String IOSStoreKitVerificationResponse::receipt
	String_t* ___receipt_1;
	// System.String IOSStoreKitVerificationResponse::productIdentifier
	String_t* ___productIdentifier_2;
	// System.String IOSStoreKitVerificationResponse::originalJSON
	String_t* ___originalJSON_3;
};
