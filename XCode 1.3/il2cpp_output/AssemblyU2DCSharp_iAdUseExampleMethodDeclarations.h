﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iAdUseExample
struct iAdUseExample_t214;

#include "codegen/il2cpp-codegen.h"

// System.Void iAdUseExample::.ctor()
extern "C" void iAdUseExample__ctor_m1195 (iAdUseExample_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdUseExample::Start()
extern "C" void iAdUseExample_Start_m1196 (iAdUseExample_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdUseExample::InitStyles()
extern "C" void iAdUseExample_InitStyles_m1197 (iAdUseExample_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdUseExample::Update()
extern "C" void iAdUseExample_Update_m1198 (iAdUseExample_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdUseExample::OnGUI()
extern "C" void iAdUseExample_OnGUI_m1199 (iAdUseExample_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdUseExample::OnInterstitialLoaded()
extern "C" void iAdUseExample_OnInterstitialLoaded_m1200 (iAdUseExample_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdUseExample::OnInterstitialFinish()
extern "C" void iAdUseExample_OnInterstitialFinish_m1201 (iAdUseExample_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdUseExample::InterstitialAdDidFinishAction()
extern "C" void iAdUseExample_InterstitialAdDidFinishAction_m1202 (iAdUseExample_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
