﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<IOSDialogResult>
struct Action_1_t165;

#include "AssemblyU2DCSharp_BaseIOSPopup.h"

// IOSDialog
struct  IOSDialog_t163  : public BaseIOSPopup_t164
{
	// System.String IOSDialog::yes
	String_t* ___yes_5;
	// System.String IOSDialog::no
	String_t* ___no_6;
	// System.Action`1<IOSDialogResult> IOSDialog::OnComplete
	Action_1_t165 * ___OnComplete_7;
};
struct IOSDialog_t163_StaticFields{
	// System.Action`1<IOSDialogResult> IOSDialog::<>f__am$cache3
	Action_1_t165 * ___U3CU3Ef__amU24cache3_8;
};
