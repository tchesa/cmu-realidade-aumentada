﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t3398;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m27981_gshared (DefaultComparer_t3398 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27981(__this, method) (( void (*) (DefaultComparer_t3398 *, const MethodInfo*))DefaultComparer__ctor_m27981_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m27982_gshared (DefaultComparer_t3398 * __this, UICharInfo_t859  ___x, UICharInfo_t859  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m27982(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3398 *, UICharInfo_t859 , UICharInfo_t859 , const MethodInfo*))DefaultComparer_Compare_m27982_gshared)(__this, ___x, ___y, method)
