﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetTrackingTrigger
struct TargetTrackingTrigger_t391;

#include "codegen/il2cpp-codegen.h"

// System.Void TargetTrackingTrigger::.ctor()
extern "C" void TargetTrackingTrigger__ctor_m1999 (TargetTrackingTrigger_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetTrackingTrigger::Update()
extern "C" void TargetTrackingTrigger_Update_m2000 (TargetTrackingTrigger_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetTrackingTrigger::OnEnabled()
extern "C" void TargetTrackingTrigger_OnEnabled_m2001 (TargetTrackingTrigger_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TargetTrackingTrigger::OnDisabled()
extern "C" void TargetTrackingTrigger_OnDisabled_m2002 (TargetTrackingTrigger_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
