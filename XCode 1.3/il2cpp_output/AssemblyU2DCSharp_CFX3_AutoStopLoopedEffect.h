﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// CFX3_AutoStopLoopedEffect
struct  CFX3_AutoStopLoopedEffect_t312  : public MonoBehaviour_t18
{
	// System.Single CFX3_AutoStopLoopedEffect::effectDuration
	float ___effectDuration_1;
	// System.Single CFX3_AutoStopLoopedEffect::d
	float ___d_2;
};
