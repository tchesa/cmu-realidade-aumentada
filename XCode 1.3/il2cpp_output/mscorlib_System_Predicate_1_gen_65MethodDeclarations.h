﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t3405;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m28117_gshared (Predicate_1_t3405 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m28117(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3405 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m28117_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m28118_gshared (Predicate_1_t3405 * __this, UILineInfo_t857  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m28118(__this, ___obj, method) (( bool (*) (Predicate_1_t3405 *, UILineInfo_t857 , const MethodInfo*))Predicate_1_Invoke_m28118_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m28119_gshared (Predicate_1_t3405 * __this, UILineInfo_t857  ___obj, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m28119(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3405 *, UILineInfo_t857 , AsyncCallback_t12 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m28119_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m28120_gshared (Predicate_1_t3405 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m28120(__this, ___result, method) (( bool (*) (Predicate_1_t3405 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m28120_gshared)(__this, ___result, method)
