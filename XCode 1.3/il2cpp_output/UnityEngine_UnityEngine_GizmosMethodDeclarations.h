﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawLine_m419 (Object_t * __this /* static, unused */, Vector3_t6  ___from, Vector3_t6  ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawLine_m7824 (Object_t * __this /* static, unused */, Vector3_t6 * ___from, Vector3_t6 * ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawSphere(UnityEngine.Vector3,System.Single)
extern "C" void Gizmos_DrawSphere_m2666 (Object_t * __this /* static, unused */, Vector3_t6  ___center, float ___radius, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)
extern "C" void Gizmos_INTERNAL_CALL_DrawSphere_m7825 (Object_t * __this /* static, unused */, Vector3_t6 * ___center, float ___radius, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawWireCube_m6475 (Object_t * __this /* static, unused */, Vector3_t6  ___center, Vector3_t6  ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawWireCube_m7826 (Object_t * __this /* static, unused */, Vector3_t6 * ___center, Vector3_t6 * ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawCube_m6480 (Object_t * __this /* static, unused */, Vector3_t6  ___center, Vector3_t6  ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawCube_m7827 (Object_t * __this /* static, unused */, Vector3_t6 * ___center, Vector3_t6 * ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C" void Gizmos_set_color_m418 (Object_t * __this /* static, unused */, Color_t5  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Gizmos_INTERNAL_set_color_m7828 (Object_t * __this /* static, unused */, Color_t5 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_matrix(UnityEngine.Matrix4x4)
extern "C" void Gizmos_set_matrix_m2792 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_set_matrix(UnityEngine.Matrix4x4&)
extern "C" void Gizmos_INTERNAL_set_matrix_m7829 (Object_t * __this /* static, unused */, Matrix4x4_t603 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
