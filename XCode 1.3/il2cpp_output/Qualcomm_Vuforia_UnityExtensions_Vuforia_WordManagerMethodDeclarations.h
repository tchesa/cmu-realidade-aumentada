﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WordManager
struct WordManager_t1016;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.WordManager::.ctor()
extern "C" void WordManager__ctor_m5105 (WordManager_t1016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
