﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t119;

#include "mscorlib_System_Object.h"

// ByteBuffer
struct  ByteBuffer_t194  : public Object_t
{
	// System.Byte[] ByteBuffer::buffer
	ByteU5BU5D_t119* ___buffer_0;
	// System.Int32 ByteBuffer::pointer
	int32_t ___pointer_1;
};
