﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3476;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__49.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_49.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m29217_gshared (Enumerator_t3483 * __this, Dictionary_2_t3476 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m29217(__this, ___dictionary, method) (( void (*) (Enumerator_t3483 *, Dictionary_2_t3476 *, const MethodInfo*))Enumerator__ctor_m29217_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m29218_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m29218(__this, method) (( Object_t * (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m29218_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m29219_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m29219(__this, method) (( void (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m29219_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t58  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m29220_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m29220(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m29220_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m29221_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m29221(__this, method) (( Object_t * (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m29221_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29222_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29222(__this, method) (( Object_t * (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m29223_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m29223(__this, method) (( bool (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_MoveNext_m29223_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t3478  Enumerator_get_Current_m29224_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m29224(__this, method) (( KeyValuePair_2_t3478  (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_get_Current_m29224_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m29225_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m29225(__this, method) (( Object_t * (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_get_CurrentKey_m29225_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m29226_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m29226(__this, method) (( bool (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_get_CurrentValue_m29226_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C" void Enumerator_Reset_m29227_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_Reset_m29227(__this, method) (( void (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_Reset_m29227_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m29228_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m29228(__this, method) (( void (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_VerifyState_m29228_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m29229_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m29229(__this, method) (( void (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_VerifyCurrent_m29229_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m29230_gshared (Enumerator_t3483 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m29230(__this, method) (( void (*) (Enumerator_t3483 *, const MethodInfo*))Enumerator_Dispose_m29230_gshared)(__this, method)
