﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_58MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m24363(__this, ___dictionary, method) (( void (*) (ValueCollection_t3168 *, Dictionary_2_t1010 *, const MethodInfo*))ValueCollection__ctor_m24289_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m24364(__this, ___item, method) (( void (*) (ValueCollection_t3168 *, uint16_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m24290_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m24365(__this, method) (( void (*) (ValueCollection_t3168 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m24291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m24366(__this, ___item, method) (( bool (*) (ValueCollection_t3168 *, uint16_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m24292_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m24367(__this, ___item, method) (( bool (*) (ValueCollection_t3168 *, uint16_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m24293_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m24368(__this, method) (( Object_t* (*) (ValueCollection_t3168 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m24294_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m24369(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3168 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m24295_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m24370(__this, method) (( Object_t * (*) (ValueCollection_t3168 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m24296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m24371(__this, method) (( bool (*) (ValueCollection_t3168 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m24297_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m24372(__this, method) (( bool (*) (ValueCollection_t3168 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m24298_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m24373(__this, method) (( Object_t * (*) (ValueCollection_t3168 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m24299_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m24374(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3168 *, UInt16U5BU5D_t1523*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m24300_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::GetEnumerator()
#define ValueCollection_GetEnumerator_m24375(__this, method) (( Enumerator_t3707  (*) (ValueCollection_t3168 *, const MethodInfo*))ValueCollection_GetEnumerator_m24301_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::get_Count()
#define ValueCollection_get_Count_m24376(__this, method) (( int32_t (*) (ValueCollection_t3168 *, const MethodInfo*))ValueCollection_get_Count_m24302_gshared)(__this, method)
