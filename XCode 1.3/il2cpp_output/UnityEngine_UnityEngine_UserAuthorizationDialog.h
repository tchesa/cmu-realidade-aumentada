﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t731;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"

// UnityEngine.UserAuthorizationDialog
struct  UserAuthorizationDialog_t1302  : public MonoBehaviour_t18
{
	// UnityEngine.Rect UnityEngine.UserAuthorizationDialog::windowRect
	Rect_t30  ___windowRect_3;
	// UnityEngine.Texture UnityEngine.UserAuthorizationDialog::warningIcon
	Texture_t731 * ___warningIcon_4;
};
