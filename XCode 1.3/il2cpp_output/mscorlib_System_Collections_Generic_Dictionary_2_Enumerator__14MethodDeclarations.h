﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m17040(__this, ___dictionary, method) (( void (*) (Enumerator_t2700 *, Dictionary_2_t106 *, const MethodInfo*))Enumerator__ctor_m16145_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17041(__this, method) (( Object_t * (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16146_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m17042(__this, method) (( void (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16147_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17043(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16148_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17044(__this, method) (( Object_t * (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16149_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17045(__this, method) (( Object_t * (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::MoveNext()
#define Enumerator_MoveNext_m17046(__this, method) (( bool (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_MoveNext_m16151_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::get_Current()
#define Enumerator_get_Current_m17047(__this, method) (( KeyValuePair_2_t2697  (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_get_Current_m16152_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17048(__this, method) (( String_t* (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_get_CurrentKey_m16153_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17049(__this, method) (( GameCenterPlayerTemplate_t107 * (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_get_CurrentValue_m16154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::Reset()
#define Enumerator_Reset_m17050(__this, method) (( void (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_Reset_m16155_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::VerifyState()
#define Enumerator_VerifyState_m17051(__this, method) (( void (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_VerifyState_m16156_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17052(__this, method) (( void (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_VerifyCurrent_m16157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::Dispose()
#define Enumerator_Dispose_m17053(__this, method) (( void (*) (Enumerator_t2700 *, const MethodInfo*))Enumerator_Dispose_m16158_gshared)(__this, method)
