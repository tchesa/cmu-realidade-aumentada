﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1209;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_49.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25990_gshared (Enumerator_t3243 * __this, Dictionary_2_t1209 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m25990(__this, ___host, method) (( void (*) (Enumerator_t3243 *, Dictionary_2_t1209 *, const MethodInfo*))Enumerator__ctor_m25990_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25991_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25991(__this, method) (( Object_t * (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m25992_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m25992(__this, method) (( void (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m25992_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m25993_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25993(__this, method) (( void (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_Dispose_m25993_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25994_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25994(__this, method) (( bool (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_MoveNext_m25994_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" int32_t Enumerator_get_Current_m25995_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25995(__this, method) (( int32_t (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_get_Current_m25995_gshared)(__this, method)
