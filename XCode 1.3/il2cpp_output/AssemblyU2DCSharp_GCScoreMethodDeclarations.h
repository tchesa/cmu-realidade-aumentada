﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GCScore
struct GCScore_t117;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GCBoardTimeSpan.h"
#include "AssemblyU2DCSharp_GCCollectionType.h"

// System.Void GCScore::.ctor(System.String,System.Int32,GCBoardTimeSpan,GCCollectionType,System.String,System.String)
extern "C" void GCScore__ctor_m691 (GCScore_t117 * __this, String_t* ___vScore, int32_t ___vRank, int32_t ___vTimeSpan, int32_t ___sCollection, String_t* ___lid, String_t* ___pid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GCScore::GetDoubleScore()
extern "C" double GCScore_GetDoubleScore_m692 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GCScore::GetLongScore()
extern "C" int64_t GCScore_GetLongScore_m693 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GCScore::GetRank()
extern "C" int32_t GCScore_GetRank_m694 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GCScore::get_rank()
extern "C" int32_t GCScore_get_rank_m695 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GCScore::get_score()
extern "C" String_t* GCScore_get_score_m696 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GCScore::get_playerId()
extern "C" String_t* GCScore_get_playerId_m697 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GCScore::get_leaderboardId()
extern "C" String_t* GCScore_get_leaderboardId_m698 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GCCollectionType GCScore::get_collection()
extern "C" int32_t GCScore_get_collection_m699 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GCBoardTimeSpan GCScore::get_timeSpan()
extern "C" int32_t GCScore_get_timeSpan_m700 (GCScore_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
