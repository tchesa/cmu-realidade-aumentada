﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iAdIOSInterstitial
struct iAdIOSInterstitial_t178;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void iAdIOSInterstitial::.ctor()
extern "C" void iAdIOSInterstitial__ctor_m1005 (iAdIOSInterstitial_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdIOSInterstitial::Start()
extern "C" void iAdIOSInterstitial_Start_m1006 (iAdIOSInterstitial_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdIOSInterstitial::ShowBanner()
extern "C" void iAdIOSInterstitial_ShowBanner_m1007 (iAdIOSInterstitial_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String iAdIOSInterstitial::get_sceneBannerId()
extern "C" String_t* iAdIOSInterstitial_get_sceneBannerId_m1008 (iAdIOSInterstitial_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
