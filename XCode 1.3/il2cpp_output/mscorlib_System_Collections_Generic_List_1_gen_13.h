﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnimationControllerScript/TimeEvent[]
struct TimeEventU5BU5D_t577;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>
struct  List_1_t371  : public Object_t
{
	// T[] System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::_items
	TimeEventU5BU5D_t577* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::_version
	int32_t ____version_3;
};
struct List_1_t371_StaticFields{
	// T[] System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::EmptyArray
	TimeEventU5BU5D_t577* ___EmptyArray_4;
};
