﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GCScore
struct GCScore_t117;

#include "AssemblyU2DCSharp_ISN_Result.h"

// ISN_PlayerScoreLoadedResult
struct  ISN_PlayerScoreLoadedResult_t116  : public ISN_Result_t114
{
	// GCScore ISN_PlayerScoreLoadedResult::_score
	GCScore_t117 * ____score_2;
};
