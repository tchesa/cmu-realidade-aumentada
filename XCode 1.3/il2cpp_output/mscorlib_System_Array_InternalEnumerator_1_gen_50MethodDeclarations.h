﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_50.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__9.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23930_gshared (InternalEnumerator_1_t3133 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23930(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3133 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23930_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23931_gshared (InternalEnumerator_1_t3133 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23931(__this, method) (( void (*) (InternalEnumerator_1_t3133 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23931_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23932_gshared (InternalEnumerator_1_t3133 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23932(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3133 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23932_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23933_gshared (InternalEnumerator_1_t3133 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23933(__this, method) (( void (*) (InternalEnumerator_1_t3133 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23933_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23934_gshared (InternalEnumerator_1_t3133 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23934(__this, method) (( bool (*) (InternalEnumerator_1_t3133 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23934_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::get_Current()
extern "C" SurfaceData_t978  InternalEnumerator_1_get_Current_m23935_gshared (InternalEnumerator_1_t3133 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23935(__this, method) (( SurfaceData_t978  (*) (InternalEnumerator_1_t3133 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23935_gshared)(__this, method)
