﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t110;
// System.Action`1<GameCenterMatchData>
struct Action_1_t111;
// System.Action`1<GameCenterDataPackage>
struct Action_1_t112;
// GameCenterMultiplayer
struct GameCenterMultiplayer_t108;
// GameCenterMatchData
struct GameCenterMatchData_t109;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// GameCenterMultiplayer
struct  GameCenterMultiplayer_t108  : public EventDispatcher_t69
{
	// GameCenterMatchData GameCenterMultiplayer::_match
	GameCenterMatchData_t109 * ____match_12;
};
struct GameCenterMultiplayer_t108_StaticFields{
	// System.Action`1<System.String> GameCenterMultiplayer::OnPlayerConnected
	Action_1_t110 * ___OnPlayerConnected_7;
	// System.Action`1<System.String> GameCenterMultiplayer::OnPlayerDisconnected
	Action_1_t110 * ___OnPlayerDisconnected_8;
	// System.Action`1<GameCenterMatchData> GameCenterMultiplayer::OnMatchStarted
	Action_1_t111 * ___OnMatchStarted_9;
	// System.Action`1<GameCenterDataPackage> GameCenterMultiplayer::OnDataReceived
	Action_1_t112 * ___OnDataReceived_10;
	// GameCenterMultiplayer GameCenterMultiplayer::_instance
	GameCenterMultiplayer_t108 * ____instance_11;
	// System.Action`1<System.String> GameCenterMultiplayer::<>f__am$cache6
	Action_1_t110 * ___U3CU3Ef__amU24cache6_13;
	// System.Action`1<System.String> GameCenterMultiplayer::<>f__am$cache7
	Action_1_t110 * ___U3CU3Ef__amU24cache7_14;
	// System.Action`1<GameCenterMatchData> GameCenterMultiplayer::<>f__am$cache8
	Action_1_t111 * ___U3CU3Ef__amU24cache8_15;
	// System.Action`1<GameCenterDataPackage> GameCenterMultiplayer::<>f__am$cache9
	Action_1_t112 * ___U3CU3Ef__amU24cache9_16;
};
