﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.Prop
struct Prop_t491;
// UnityEngine.BoxCollider
struct BoxCollider_t1041;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"

// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t430  : public SmartTerrainTrackableBehaviour_t917
{
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	Object_t * ___mProp_12;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t1041 * ___mBoxColliderToUpdate_13;
};
