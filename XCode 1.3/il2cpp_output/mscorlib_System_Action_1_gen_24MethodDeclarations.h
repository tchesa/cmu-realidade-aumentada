﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t1035;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"

// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m25226_gshared (Action_1_t1035 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m25226(__this, ___object, ___method, method) (( void (*) (Action_1_t1035 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m25226_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::Invoke(T)
extern "C" void Action_1_Invoke_m6677_gshared (Action_1_t1035 * __this, SmartTerrainInitializationInfo_t916  ___obj, const MethodInfo* method);
#define Action_1_Invoke_m6677(__this, ___obj, method) (( void (*) (Action_1_t1035 *, SmartTerrainInitializationInfo_t916 , const MethodInfo*))Action_1_Invoke_m6677_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<Vuforia.SmartTerrainInitializationInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m25227_gshared (Action_1_t1035 * __this, SmartTerrainInitializationInfo_t916  ___obj, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m25227(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t1035 *, SmartTerrainInitializationInfo_t916 , AsyncCallback_t12 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m25227_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m25228_gshared (Action_1_t1035 * __this, Object_t * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m25228(__this, ___result, method) (( void (*) (Action_1_t1035 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m25228_gshared)(__this, ___result, method)
