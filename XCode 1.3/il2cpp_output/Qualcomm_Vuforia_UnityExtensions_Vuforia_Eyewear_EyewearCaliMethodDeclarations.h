﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void EyewearCalibrationReading_t913_marshal(const EyewearCalibrationReading_t913& unmarshaled, EyewearCalibrationReading_t913_marshaled& marshaled);
extern "C" void EyewearCalibrationReading_t913_marshal_back(const EyewearCalibrationReading_t913_marshaled& marshaled, EyewearCalibrationReading_t913& unmarshaled);
extern "C" void EyewearCalibrationReading_t913_marshal_cleanup(EyewearCalibrationReading_t913_marshaled& marshaled);
