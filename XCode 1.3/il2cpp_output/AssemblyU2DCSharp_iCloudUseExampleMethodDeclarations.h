﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iCloudUseExample
struct iCloudUseExample_t211;
// ISN_Result
struct ISN_Result_t114;
// iCloudData
struct iCloudData_t179;

#include "codegen/il2cpp-codegen.h"

// System.Void iCloudUseExample::.ctor()
extern "C" void iCloudUseExample__ctor_m1180 (iCloudUseExample_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::Awake()
extern "C" void iCloudUseExample_Awake_m1181 (iCloudUseExample_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnGUI()
extern "C" void iCloudUseExample_OnGUI_m1182 (iCloudUseExample_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnCloundInitAction(ISN_Result)
extern "C" void iCloudUseExample_OnCloundInitAction_m1183 (iCloudUseExample_t211 * __this, ISN_Result_t114 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnCloudDataReceivedAction(iCloudData)
extern "C" void iCloudUseExample_OnCloudDataReceivedAction_m1184 (iCloudUseExample_t211 * __this, iCloudData_t179 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnCloundDataChangedAction()
extern "C" void iCloudUseExample_OnCloundDataChangedAction_m1185 (iCloudUseExample_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnDestroy()
extern "C" void iCloudUseExample_OnDestroy_m1186 (iCloudUseExample_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
