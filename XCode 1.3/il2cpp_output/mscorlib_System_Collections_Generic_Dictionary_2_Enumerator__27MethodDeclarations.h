﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t3058;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22819_gshared (Enumerator_t3065 * __this, Dictionary_2_t3058 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22819(__this, ___dictionary, method) (( void (*) (Enumerator_t3065 *, Dictionary_2_t3058 *, const MethodInfo*))Enumerator__ctor_m22819_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22820_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22820(__this, method) (( Object_t * (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m22821_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m22821(__this, method) (( void (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m22821_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t58  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22822_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22822(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22822_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22823_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22823(__this, method) (( Object_t * (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22823_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22824_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22824(__this, method) (( Object_t * (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22824_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22825_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22825(__this, method) (( bool (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_MoveNext_m22825_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3060  Enumerator_get_Current_m22826_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22826(__this, method) (( KeyValuePair_2_t3060  (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_get_Current_m22826_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m22827_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22827(__this, method) (( int32_t (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_get_CurrentKey_m22827_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m22828_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22828(__this, method) (( Object_t * (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_get_CurrentValue_m22828_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Reset()
extern "C" void Enumerator_Reset_m22829_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_Reset_m22829(__this, method) (( void (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_Reset_m22829_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m22830_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22830(__this, method) (( void (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_VerifyState_m22830_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22831_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22831(__this, method) (( void (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_VerifyCurrent_m22831_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m22832_gshared (Enumerator_t3065 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22832(__this, method) (( void (*) (Enumerator_t3065 *, const MethodInfo*))Enumerator_Dispose_m22832_gshared)(__this, method)
