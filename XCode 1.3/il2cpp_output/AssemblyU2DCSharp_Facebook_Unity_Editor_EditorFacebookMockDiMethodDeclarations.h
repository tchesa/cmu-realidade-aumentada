﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete
struct OnComplete_t269;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete::.ctor(System.Object,System.IntPtr)
extern "C" void OnComplete__ctor_m1551 (OnComplete_t269 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete::Invoke(System.String)
extern "C" void OnComplete_Invoke_m1552 (OnComplete_t269 * __this, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnComplete_t269(Il2CppObject* delegate, String_t* ___result);
// System.IAsyncResult Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * OnComplete_BeginInvoke_m1553 (OnComplete_t269 * __this, String_t* ___result, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete::EndInvoke(System.IAsyncResult)
extern "C" void OnComplete_EndInvoke_m1554 (OnComplete_t269 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
