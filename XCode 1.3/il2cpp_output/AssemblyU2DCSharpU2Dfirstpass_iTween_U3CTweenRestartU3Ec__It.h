﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// iTween
struct iTween_t15;

#include "mscorlib_System_Object.h"

// iTween/<TweenRestart>c__Iterator1
struct  U3CTweenRestartU3Ec__Iterator1_t16  : public Object_t
{
	// System.Int32 iTween/<TweenRestart>c__Iterator1::$PC
	int32_t ___U24PC_0;
	// System.Object iTween/<TweenRestart>c__Iterator1::$current
	Object_t * ___U24current_1;
	// iTween iTween/<TweenRestart>c__Iterator1::<>f__this
	iTween_t15 * ___U3CU3Ef__this_2;
};
