﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.PlayModeUnityPlayer
struct PlayModeUnityPlayer_t599;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"

// System.Void Vuforia.PlayModeUnityPlayer::LoadNativeLibraries()
extern "C" void PlayModeUnityPlayer_LoadNativeLibraries_m4680 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializePlatform()
extern "C" void PlayModeUnityPlayer_InitializePlatform_m4681 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializeSurface()
extern "C" void PlayModeUnityPlayer_InitializeSurface_m4682 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaUnity/InitError Vuforia.PlayModeUnityPlayer::Start(System.String)
extern "C" int32_t PlayModeUnityPlayer_Start_m4683 (PlayModeUnityPlayer_t599 * __this, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Update()
extern "C" void PlayModeUnityPlayer_Update_m4684 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Dispose()
extern "C" void PlayModeUnityPlayer_Dispose_m4685 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnPause()
extern "C" void PlayModeUnityPlayer_OnPause_m4686 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnResume()
extern "C" void PlayModeUnityPlayer_OnResume_m4687 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnDestroy()
extern "C" void PlayModeUnityPlayer_OnDestroy_m4688 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C" void PlayModeUnityPlayer__ctor_m2769 (PlayModeUnityPlayer_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
