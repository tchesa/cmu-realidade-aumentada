﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_34.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19925_gshared (InternalEnumerator_1_t2878 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19925(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2878 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19925_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19926_gshared (InternalEnumerator_1_t2878 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19926(__this, method) (( void (*) (InternalEnumerator_1_t2878 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19926_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19927_gshared (InternalEnumerator_1_t2878 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19927(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2878 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19927_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19928_gshared (InternalEnumerator_1_t2878 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19928(__this, method) (( void (*) (InternalEnumerator_1_t2878 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19928_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19929_gshared (InternalEnumerator_1_t2878 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19929(__this, method) (( bool (*) (InternalEnumerator_1_t2878 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19929_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t647  InternalEnumerator_1_get_Current_m19930_gshared (InternalEnumerator_1_t2878 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19930(__this, method) (( RaycastResult_t647  (*) (InternalEnumerator_1_t2878 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19930_gshared)(__this, method)
