﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.String>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m15844(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2609 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15544_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.String>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15845(__this, method) (( void (*) (InternalEnumerator_1_t2609 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15545_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.String>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15846(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2609 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15546_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.String>::Dispose()
#define InternalEnumerator_1_Dispose_m15847(__this, method) (( void (*) (InternalEnumerator_1_t2609 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15547_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.String>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15848(__this, method) (( bool (*) (InternalEnumerator_1_t2609 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15548_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.String>::get_Current()
#define InternalEnumerator_1_get_Current_m15849(__this, method) (( String_t* (*) (InternalEnumerator_1_t2609 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15549_gshared)(__this, method)
