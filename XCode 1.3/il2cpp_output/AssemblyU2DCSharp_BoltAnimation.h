﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem
struct ParticleSystem_t332;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// BoltAnimation
struct  BoltAnimation_t378  : public MonoBehaviour_t18
{
	// System.Single BoltAnimation::delay
	float ___delay_1;
	// System.Single BoltAnimation::_time
	float ____time_2;
	// System.Boolean BoltAnimation::play
	bool ___play_3;
	// System.Boolean BoltAnimation::_play
	bool ____play_4;
	// UnityEngine.ParticleSystem BoltAnimation::particle
	ParticleSystem_t332 * ___particle_5;
};
