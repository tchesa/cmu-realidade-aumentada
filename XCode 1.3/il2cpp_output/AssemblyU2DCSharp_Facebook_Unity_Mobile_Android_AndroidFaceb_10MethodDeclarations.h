﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>
struct JavaMethodCall_1_t2777;
// Facebook.Unity.Mobile.Android.AndroidFacebook
struct AndroidFacebook_t249;
// System.String
struct String_t;
// Facebook.Unity.MethodArguments
struct MethodArguments_t248;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
extern "C" void JavaMethodCall_1__ctor_m18448_gshared (JavaMethodCall_1_t2777 * __this, AndroidFacebook_t249 * ___androidImpl, String_t* ___methodName, const MethodInfo* method);
#define JavaMethodCall_1__ctor_m18448(__this, ___androidImpl, ___methodName, method) (( void (*) (JavaMethodCall_1_t2777 *, AndroidFacebook_t249 *, String_t*, const MethodInfo*))JavaMethodCall_1__ctor_m18448_gshared)(__this, ___androidImpl, ___methodName, method)
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::Call(Facebook.Unity.MethodArguments)
extern "C" void JavaMethodCall_1_Call_m18450_gshared (JavaMethodCall_1_t2777 * __this, MethodArguments_t248 * ___args, const MethodInfo* method);
#define JavaMethodCall_1_Call_m18450(__this, ___args, method) (( void (*) (JavaMethodCall_1_t2777 *, MethodArguments_t248 *, const MethodInfo*))JavaMethodCall_1_Call_m18450_gshared)(__this, ___args, method)
