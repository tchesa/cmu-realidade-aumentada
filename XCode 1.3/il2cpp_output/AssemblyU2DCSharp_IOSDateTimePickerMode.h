﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_IOSDateTimePickerMode.h"

// IOSDateTimePickerMode
struct  IOSDateTimePickerMode_t89 
{
	// System.Int32 IOSDateTimePickerMode::value__
	int32_t ___value___1;
};
