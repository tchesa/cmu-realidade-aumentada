﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ArUiController
struct ArUiController_t374;
// ComponentTracker
struct ComponentTracker_t67;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// ArUiController
struct  ArUiController_t374  : public MonoBehaviour_t18
{
	// System.Single ArUiController::sharedDelay
	float ___sharedDelay_2;
	// ComponentTracker ArUiController::tracker
	ComponentTracker_t67 * ___tracker_3;
};
struct ArUiController_t374_StaticFields{
	// ArUiController ArUiController::instance
	ArUiController_t374 * ___instance_1;
};
