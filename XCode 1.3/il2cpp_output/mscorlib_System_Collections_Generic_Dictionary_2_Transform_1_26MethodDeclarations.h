﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_24MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Single,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m18710(__this, ___object, ___method, method) (( void (*) (Transform_1_t2784 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m18684_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Single,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m18711(__this, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Transform_1_t2784 *, String_t*, float, const MethodInfo*))Transform_1_Invoke_m18685_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Single,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m18712(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2784 *, String_t*, float, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m18686_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Single,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m18713(__this, ___result, method) (( DictionaryEntry_t58  (*) (Transform_1_t2784 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m18687_gshared)(__this, ___result, method)
