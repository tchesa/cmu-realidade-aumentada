﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t910;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t431;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct  ReconstructionFromTargetAbstractBehaviour_t433  : public MonoBehaviour_t18
{
	// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionFromTarget
	Object_t * ___mReconstructionFromTarget_1;
	// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionBehaviour
	ReconstructionAbstractBehaviour_t431 * ___mReconstructionBehaviour_2;
};
