﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<System.String>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m21552(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t2980 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m19763_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.String>::Invoke(T0)
#define UnityAction_1_Invoke_m21553(__this, ___arg0, method) (( void (*) (UnityAction_1_t2980 *, String_t*, const MethodInfo*))UnityAction_1_Invoke_m19764_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.String>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m21554(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t2980 *, String_t*, AsyncCallback_t12 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m19765_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.String>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m21555(__this, ___result, method) (( void (*) (UnityAction_1_t2980 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m19766_gshared)(__this, ___result, method)
