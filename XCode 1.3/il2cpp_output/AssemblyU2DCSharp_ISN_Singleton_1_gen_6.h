﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// ISN_Singleton`1<System.Object>
struct  ISN_Singleton_1_t2689  : public EventDispatcher_t69
{
};
struct ISN_Singleton_1_t2689_StaticFields{
	// T ISN_Singleton`1<System.Object>::_instance
	Object_t * ____instance_3;
	// System.Boolean ISN_Singleton`1<System.Object>::applicationIsQuitting
	bool ___applicationIsQuitting_4;
};
