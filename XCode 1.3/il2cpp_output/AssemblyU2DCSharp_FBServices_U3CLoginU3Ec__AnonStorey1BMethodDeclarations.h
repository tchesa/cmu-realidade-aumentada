﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBServices/<Login>c__AnonStorey1B
struct U3CLoginU3Ec__AnonStorey1B_t379;
// Facebook.Unity.ILoginResult
struct ILoginResult_t489;

#include "codegen/il2cpp-codegen.h"

// System.Void FBServices/<Login>c__AnonStorey1B::.ctor()
extern "C" void U3CLoginU3Ec__AnonStorey1B__ctor_m1946 (U3CLoginU3Ec__AnonStorey1B_t379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBServices/<Login>c__AnonStorey1B::<>m__3B(Facebook.Unity.ILoginResult)
extern "C" void U3CLoginU3Ec__AnonStorey1B_U3CU3Em__3B_m1947 (U3CLoginU3Ec__AnonStorey1B_t379 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
