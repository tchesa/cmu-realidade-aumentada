﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RotateScale
struct RotateScale_t385;

#include "codegen/il2cpp-codegen.h"

// System.Void RotateScale::.ctor()
extern "C" void RotateScale__ctor_m1970 (RotateScale_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateScale::Awake()
extern "C" void RotateScale_Awake_m1971 (RotateScale_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateScale::Update()
extern "C" void RotateScale_Update_m1972 (RotateScale_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateScale::Play()
extern "C" void RotateScale_Play_m1973 (RotateScale_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateScale::Stop()
extern "C" void RotateScale_Stop_m1974 (RotateScale_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
