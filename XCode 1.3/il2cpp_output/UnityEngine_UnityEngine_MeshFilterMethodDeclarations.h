﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MeshFilter
struct MeshFilter_t597;
// UnityEngine.Mesh
struct Mesh_t601;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C" Mesh_t601 * MeshFilter_get_mesh_m6434 (MeshFilter_t597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_mesh_m6779 (MeshFilter_t597 * __this, Mesh_t601 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C" Mesh_t601 * MeshFilter_get_sharedMesh_m2778 (MeshFilter_t597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_sharedMesh_m6506 (MeshFilter_t597 * __this, Mesh_t601 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
