﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SinoidalAnimation
struct SinoidalAnimation_t388;

#include "codegen/il2cpp-codegen.h"

// System.Void SinoidalAnimation::.ctor()
extern "C" void SinoidalAnimation__ctor_m1980 (SinoidalAnimation_t388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SinoidalAnimation::Start()
extern "C" void SinoidalAnimation_Start_m1981 (SinoidalAnimation_t388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SinoidalAnimation::Update()
extern "C" void SinoidalAnimation_Update_m1982 (SinoidalAnimation_t388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
