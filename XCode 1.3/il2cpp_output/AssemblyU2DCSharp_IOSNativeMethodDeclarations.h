﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNative
struct IOSNative_t79;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNative::.ctor()
extern "C" void IOSNative__ctor_m515 (IOSNative_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
