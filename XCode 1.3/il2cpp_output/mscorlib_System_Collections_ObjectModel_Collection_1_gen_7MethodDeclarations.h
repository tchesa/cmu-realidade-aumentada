﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t3402;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1441;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3753;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t858;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m28073_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1__ctor_m28073(__this, method) (( void (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1__ctor_m28073_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28074_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28074(__this, method) (( bool (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28074_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m28075_gshared (Collection_1_t3402 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m28075(__this, ___array, ___index, method) (( void (*) (Collection_1_t3402 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m28075_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m28076_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m28076(__this, method) (( Object_t * (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m28076_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m28077_gshared (Collection_1_t3402 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m28077(__this, ___value, method) (( int32_t (*) (Collection_1_t3402 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m28077_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m28078_gshared (Collection_1_t3402 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m28078(__this, ___value, method) (( bool (*) (Collection_1_t3402 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m28078_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m28079_gshared (Collection_1_t3402 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m28079(__this, ___value, method) (( int32_t (*) (Collection_1_t3402 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m28079_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m28080_gshared (Collection_1_t3402 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m28080(__this, ___index, ___value, method) (( void (*) (Collection_1_t3402 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m28080_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m28081_gshared (Collection_1_t3402 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m28081(__this, ___value, method) (( void (*) (Collection_1_t3402 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m28081_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m28082_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m28082(__this, method) (( bool (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m28082_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m28083_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m28083(__this, method) (( Object_t * (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m28083_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m28084_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m28084(__this, method) (( bool (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m28084_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m28085_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m28085(__this, method) (( bool (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m28085_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m28086_gshared (Collection_1_t3402 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m28086(__this, ___index, method) (( Object_t * (*) (Collection_1_t3402 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m28086_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m28087_gshared (Collection_1_t3402 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m28087(__this, ___index, ___value, method) (( void (*) (Collection_1_t3402 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m28087_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m28088_gshared (Collection_1_t3402 * __this, UILineInfo_t857  ___item, const MethodInfo* method);
#define Collection_1_Add_m28088(__this, ___item, method) (( void (*) (Collection_1_t3402 *, UILineInfo_t857 , const MethodInfo*))Collection_1_Add_m28088_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m28089_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_Clear_m28089(__this, method) (( void (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_Clear_m28089_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m28090_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m28090(__this, method) (( void (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_ClearItems_m28090_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m28091_gshared (Collection_1_t3402 * __this, UILineInfo_t857  ___item, const MethodInfo* method);
#define Collection_1_Contains_m28091(__this, ___item, method) (( bool (*) (Collection_1_t3402 *, UILineInfo_t857 , const MethodInfo*))Collection_1_Contains_m28091_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m28092_gshared (Collection_1_t3402 * __this, UILineInfoU5BU5D_t1441* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m28092(__this, ___array, ___index, method) (( void (*) (Collection_1_t3402 *, UILineInfoU5BU5D_t1441*, int32_t, const MethodInfo*))Collection_1_CopyTo_m28092_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m28093_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m28093(__this, method) (( Object_t* (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_GetEnumerator_m28093_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m28094_gshared (Collection_1_t3402 * __this, UILineInfo_t857  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m28094(__this, ___item, method) (( int32_t (*) (Collection_1_t3402 *, UILineInfo_t857 , const MethodInfo*))Collection_1_IndexOf_m28094_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m28095_gshared (Collection_1_t3402 * __this, int32_t ___index, UILineInfo_t857  ___item, const MethodInfo* method);
#define Collection_1_Insert_m28095(__this, ___index, ___item, method) (( void (*) (Collection_1_t3402 *, int32_t, UILineInfo_t857 , const MethodInfo*))Collection_1_Insert_m28095_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m28096_gshared (Collection_1_t3402 * __this, int32_t ___index, UILineInfo_t857  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m28096(__this, ___index, ___item, method) (( void (*) (Collection_1_t3402 *, int32_t, UILineInfo_t857 , const MethodInfo*))Collection_1_InsertItem_m28096_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m28097_gshared (Collection_1_t3402 * __this, UILineInfo_t857  ___item, const MethodInfo* method);
#define Collection_1_Remove_m28097(__this, ___item, method) (( bool (*) (Collection_1_t3402 *, UILineInfo_t857 , const MethodInfo*))Collection_1_Remove_m28097_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m28098_gshared (Collection_1_t3402 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m28098(__this, ___index, method) (( void (*) (Collection_1_t3402 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m28098_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m28099_gshared (Collection_1_t3402 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m28099(__this, ___index, method) (( void (*) (Collection_1_t3402 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m28099_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m28100_gshared (Collection_1_t3402 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m28100(__this, method) (( int32_t (*) (Collection_1_t3402 *, const MethodInfo*))Collection_1_get_Count_m28100_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t857  Collection_1_get_Item_m28101_gshared (Collection_1_t3402 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m28101(__this, ___index, method) (( UILineInfo_t857  (*) (Collection_1_t3402 *, int32_t, const MethodInfo*))Collection_1_get_Item_m28101_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m28102_gshared (Collection_1_t3402 * __this, int32_t ___index, UILineInfo_t857  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m28102(__this, ___index, ___value, method) (( void (*) (Collection_1_t3402 *, int32_t, UILineInfo_t857 , const MethodInfo*))Collection_1_set_Item_m28102_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m28103_gshared (Collection_1_t3402 * __this, int32_t ___index, UILineInfo_t857  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m28103(__this, ___index, ___item, method) (( void (*) (Collection_1_t3402 *, int32_t, UILineInfo_t857 , const MethodInfo*))Collection_1_SetItem_m28103_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m28104_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m28104(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m28104_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t857  Collection_1_ConvertItem_m28105_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m28105(__this /* static, unused */, ___item, method) (( UILineInfo_t857  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m28105_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m28106_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m28106(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m28106_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m28107_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m28107(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m28107_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m28108_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m28108(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m28108_gshared)(__this /* static, unused */, ___list, method)
