﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t724;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m20950_gshared (Enumerator_t2941 * __this, List_1_t724 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m20950(__this, ___l, method) (( void (*) (Enumerator_t2941 *, List_1_t724 *, const MethodInfo*))Enumerator__ctor_m20950_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m20951_gshared (Enumerator_t2941 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m20951(__this, method) (( void (*) (Enumerator_t2941 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m20951_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20952_gshared (Enumerator_t2941 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20952(__this, method) (( Object_t * (*) (Enumerator_t2941 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20952_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C" void Enumerator_Dispose_m20953_gshared (Enumerator_t2941 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20953(__this, method) (( void (*) (Enumerator_t2941 *, const MethodInfo*))Enumerator_Dispose_m20953_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C" void Enumerator_VerifyState_m20954_gshared (Enumerator_t2941 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m20954(__this, method) (( void (*) (Enumerator_t2941 *, const MethodInfo*))Enumerator_VerifyState_m20954_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20955_gshared (Enumerator_t2941 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20955(__this, method) (( bool (*) (Enumerator_t2941 *, const MethodInfo*))Enumerator_MoveNext_m20955_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t727  Enumerator_get_Current_m20956_gshared (Enumerator_t2941 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20956(__this, method) (( UIVertex_t727  (*) (Enumerator_t2941 *, const MethodInfo*))Enumerator_get_Current_m20956_gshared)(__this, method)
