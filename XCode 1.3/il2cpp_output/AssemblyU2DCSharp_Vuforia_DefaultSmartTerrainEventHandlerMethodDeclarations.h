﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t405;
// Vuforia.Prop
struct Prop_t491;
// Vuforia.Surface
struct Surface_t492;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C" void DefaultSmartTerrainEventHandler__ctor_m2038 (DefaultSmartTerrainEventHandler_t405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern "C" void DefaultSmartTerrainEventHandler_Start_m2039 (DefaultSmartTerrainEventHandler_t405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern "C" void DefaultSmartTerrainEventHandler_OnDestroy_m2040 (DefaultSmartTerrainEventHandler_t405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern "C" void DefaultSmartTerrainEventHandler_OnPropCreated_m2041 (DefaultSmartTerrainEventHandler_t405 * __this, Object_t * ___prop, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern "C" void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2042 (DefaultSmartTerrainEventHandler_t405 * __this, Object_t * ___surface, const MethodInfo* method) IL2CPP_METHOD_ATTR;
