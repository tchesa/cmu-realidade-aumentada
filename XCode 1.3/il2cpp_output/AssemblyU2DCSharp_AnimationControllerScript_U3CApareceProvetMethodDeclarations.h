﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript/<ApareceProveta>c__Iterator10
struct U3CApareceProvetaU3Ec__Iterator10_t365;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript/<ApareceProveta>c__Iterator10::.ctor()
extern "C" void U3CApareceProvetaU3Ec__Iterator10__ctor_m1846 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<ApareceProveta>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CApareceProvetaU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1847 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<ApareceProveta>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CApareceProvetaU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m1848 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationControllerScript/<ApareceProveta>c__Iterator10::MoveNext()
extern "C" bool U3CApareceProvetaU3Ec__Iterator10_MoveNext_m1849 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<ApareceProveta>c__Iterator10::Dispose()
extern "C" void U3CApareceProvetaU3Ec__Iterator10_Dispose_m1850 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<ApareceProveta>c__Iterator10::Reset()
extern "C" void U3CApareceProvetaU3Ec__Iterator10_Reset_m1851 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
