﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t3371;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2606;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3744;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t3745;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t3376;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t3380;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__45.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C" void Dictionary_2__ctor_m27595_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m27595(__this, method) (( void (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2__ctor_m27595_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27597_gshared (Dictionary_2_t3371 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27597(__this, ___comparer, method) (( void (*) (Dictionary_2_t3371 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27597_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m27599_gshared (Dictionary_2_t3371 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m27599(__this, ___capacity, method) (( void (*) (Dictionary_2_t3371 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m27599_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m27601_gshared (Dictionary_2_t3371 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m27601(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3371 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m27601_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m27603_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m27603(__this, method) (( Object_t * (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m27603_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m27605_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m27605(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m27605_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27607_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m27607(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3371 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m27607_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27609_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m27609(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3371 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m27609_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m27611_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m27611(__this, ___key, method) (( bool (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m27611_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27613_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m27613(__this, ___key, method) (( void (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m27613_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27615_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27615(__this, method) (( bool (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27615_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27617_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27617(__this, method) (( Object_t * (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27617_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27619_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27619(__this, method) (( bool (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27621_gshared (Dictionary_2_t3371 * __this, KeyValuePair_2_t3373  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27621(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3371 *, KeyValuePair_2_t3373 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27621_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27623_gshared (Dictionary_2_t3371 * __this, KeyValuePair_2_t3373  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27623(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3371 *, KeyValuePair_2_t3373 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27623_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27625_gshared (Dictionary_2_t3371 * __this, KeyValuePair_2U5BU5D_t3744* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27625(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3371 *, KeyValuePair_2U5BU5D_t3744*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27625_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27627_gshared (Dictionary_2_t3371 * __this, KeyValuePair_2_t3373  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27627(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3371 *, KeyValuePair_2_t3373 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27627_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27629_gshared (Dictionary_2_t3371 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m27629(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3371 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m27629_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27631_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27631(__this, method) (( Object_t * (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27631_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27633_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27633(__this, method) (( Object_t* (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27633_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27635_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27635(__this, method) (( Object_t * (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27635_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m27637_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m27637(__this, method) (( int32_t (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_get_Count_m27637_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m27639_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m27639(__this, ___key, method) (( int32_t (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m27639_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m27641_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m27641(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3371 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m27641_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m27643_gshared (Dictionary_2_t3371 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m27643(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3371 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m27643_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m27645_gshared (Dictionary_2_t3371 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m27645(__this, ___size, method) (( void (*) (Dictionary_2_t3371 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m27645_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m27647_gshared (Dictionary_2_t3371 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m27647(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3371 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m27647_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3373  Dictionary_2_make_pair_m27649_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m27649(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3373  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m27649_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m27651_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m27651(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m27651_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m27653_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m27653(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m27653_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m27655_gshared (Dictionary_2_t3371 * __this, KeyValuePair_2U5BU5D_t3744* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m27655(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3371 *, KeyValuePair_2U5BU5D_t3744*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m27655_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Resize()
extern "C" void Dictionary_2_Resize_m27657_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m27657(__this, method) (( void (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_Resize_m27657_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m27659_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m27659(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3371 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m27659_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Clear()
extern "C" void Dictionary_2_Clear_m27661_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m27661(__this, method) (( void (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_Clear_m27661_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m27663_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m27663(__this, ___key, method) (( bool (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m27663_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m27665_gshared (Dictionary_2_t3371 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m27665(__this, ___value, method) (( bool (*) (Dictionary_2_t3371 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m27665_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m27667_gshared (Dictionary_2_t3371 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m27667(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3371 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m27667_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m27669_gshared (Dictionary_2_t3371 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m27669(__this, ___sender, method) (( void (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m27669_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m27671_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m27671(__this, ___key, method) (( bool (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m27671_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m27673_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m27673(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3371 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m27673_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Keys()
extern "C" KeyCollection_t3376 * Dictionary_2_get_Keys_m27675_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m27675(__this, method) (( KeyCollection_t3376 * (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_get_Keys_m27675_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Values()
extern "C" ValueCollection_t3380 * Dictionary_2_get_Values_m27677_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m27677(__this, method) (( ValueCollection_t3380 * (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_get_Values_m27677_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m27679_gshared (Dictionary_2_t3371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m27679(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m27679_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m27681_gshared (Dictionary_2_t3371 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m27681(__this, ___value, method) (( int32_t (*) (Dictionary_2_t3371 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m27681_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m27683_gshared (Dictionary_2_t3371 * __this, KeyValuePair_2_t3373  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m27683(__this, ___pair, method) (( bool (*) (Dictionary_2_t3371 *, KeyValuePair_2_t3373 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m27683_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
extern "C" Enumerator_t3378  Dictionary_2_GetEnumerator_m27685_gshared (Dictionary_2_t3371 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m27685(__this, method) (( Enumerator_t3378  (*) (Dictionary_2_t3371 *, const MethodInfo*))Dictionary_2_GetEnumerator_m27685_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m27687_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m27687(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m27687_gshared)(__this /* static, unused */, ___key, ___value, method)
