﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.YieldInstruction
struct YieldInstruction_t1349;
struct YieldInstruction_t1349_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m8107 (YieldInstruction_t1349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void YieldInstruction_t1349_marshal(const YieldInstruction_t1349& unmarshaled, YieldInstruction_t1349_marshaled& marshaled);
extern "C" void YieldInstruction_t1349_marshal_back(const YieldInstruction_t1349_marshaled& marshaled, YieldInstruction_t1349& unmarshaled);
extern "C" void YieldInstruction_t1349_marshal_cleanup(YieldInstruction_t1349_marshaled& marshaled);
