﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// iAdBannerController
struct iAdBannerController_t173;
// System.Collections.Generic.Dictionary`2<System.Int32,iAdBanner>
struct Dictionary_2_t174;
// System.Action
struct Action_t81;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// iAdBannerController
struct  iAdBannerController_t173  : public EventDispatcher_t69
{
	// System.Collections.Generic.Dictionary`2<System.Int32,iAdBanner> iAdBannerController::_banners
	Dictionary_2_t174 * ____banners_5;
	// System.Action iAdBannerController::InterstitialDidFailWithErrorAction
	Action_t81 * ___InterstitialDidFailWithErrorAction_6;
	// System.Action iAdBannerController::InterstitialAdWillLoadAction
	Action_t81 * ___InterstitialAdWillLoadAction_7;
	// System.Action iAdBannerController::InterstitialAdDidLoadAction
	Action_t81 * ___InterstitialAdDidLoadAction_8;
	// System.Action iAdBannerController::InterstitialAdDidFinishAction
	Action_t81 * ___InterstitialAdDidFinishAction_9;
};
struct iAdBannerController_t173_StaticFields{
	// System.Int32 iAdBannerController::_nextId
	int32_t ____nextId_3;
	// iAdBannerController iAdBannerController::_instance
	iAdBannerController_t173 * ____instance_4;
	// System.Action iAdBannerController::<>f__am$cache7
	Action_t81 * ___U3CU3Ef__amU24cache7_10;
	// System.Action iAdBannerController::<>f__am$cache8
	Action_t81 * ___U3CU3Ef__amU24cache8_11;
	// System.Action iAdBannerController::<>f__am$cache9
	Action_t81 * ___U3CU3Ef__amU24cache9_12;
	// System.Action iAdBannerController::<>f__am$cacheA
	Action_t81 * ___U3CU3Ef__amU24cacheA_13;
};
