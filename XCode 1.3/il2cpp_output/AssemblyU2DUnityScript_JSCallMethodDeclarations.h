﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCall
struct JSCall_t607;

#include "codegen/il2cpp-codegen.h"

// System.Void JSCall::.ctor()
extern "C" void JSCall__ctor_m2800 (JSCall_t607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCall::Start()
extern "C" void JSCall_Start_m2801 (JSCall_t607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCall::OnGUI()
extern "C" void JSCall_OnGUI_m2802 (JSCall_t607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCall::Main()
extern "C" void JSCall_Main_m2803 (JSCall_t607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
