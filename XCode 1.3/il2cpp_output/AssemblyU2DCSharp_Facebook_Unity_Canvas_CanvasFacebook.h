﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_FacebookBase.h"

// Facebook.Unity.Canvas.CanvasFacebook
struct  CanvasFacebook_t226  : public FacebookBase_t227
{
	// System.String Facebook.Unity.Canvas.CanvasFacebook::appId
	String_t* ___appId_14;
	// System.Boolean Facebook.Unity.Canvas.CanvasFacebook::sdkDebug
	bool ___sdkDebug_15;
	// System.String Facebook.Unity.Canvas.CanvasFacebook::appLinkUrl
	String_t* ___appLinkUrl_16;
	// System.Boolean Facebook.Unity.Canvas.CanvasFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_17;
};
