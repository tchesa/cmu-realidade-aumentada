﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARAnimation/TrailTrigger
struct TrailTrigger_t341;

#include "codegen/il2cpp-codegen.h"

// System.Void ARAnimation/TrailTrigger::.ctor()
extern "C" void TrailTrigger__ctor_m1808 (TrailTrigger_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
