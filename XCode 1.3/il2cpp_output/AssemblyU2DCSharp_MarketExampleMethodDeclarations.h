﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketExample
struct MarketExample_t185;
// ISN_DeviceGUID
struct ISN_DeviceGUID_t170;
// ISN_LocalReceiptResult
struct ISN_LocalReceiptResult_t171;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_IOSDialogResult.h"

// System.Void MarketExample::.ctor()
extern "C" void MarketExample__ctor_m1039 (MarketExample_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::Awake()
extern "C" void MarketExample_Awake_m1040 (MarketExample_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnGUI()
extern "C" void MarketExample_OnGUI_m1041 (MarketExample_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnGUIDLoaded(ISN_DeviceGUID)
extern "C" void MarketExample_OnGUIDLoaded_m1042 (MarketExample_t185 * __this, ISN_DeviceGUID_t170 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnPurchasesStateSettingsLoaded(System.Boolean)
extern "C" void MarketExample_OnPurchasesStateSettingsLoaded_m1043 (MarketExample_t185 * __this, bool ___IsInAppPurchasesEnabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnReceiptLoaded(ISN_LocalReceiptResult)
extern "C" void MarketExample_OnReceiptLoaded_m1044 (MarketExample_t185 * __this, ISN_LocalReceiptResult_t171 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnComplete(IOSDialogResult)
extern "C" void MarketExample_OnComplete_m1045 (MarketExample_t185 * __this, int32_t ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnReceiptRefreshComplete(ISN_Result)
extern "C" void MarketExample_OnReceiptRefreshComplete_m1046 (MarketExample_t185 * __this, ISN_Result_t114 * ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::Dialog_RetrieveLocalReceipt(IOSDialogResult)
extern "C" void MarketExample_Dialog_RetrieveLocalReceipt_m1047 (MarketExample_t185 * __this, int32_t ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
