﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iAdEvent
struct iAdEvent_t175;

#include "codegen/il2cpp-codegen.h"

// System.Void iAdEvent::.ctor()
extern "C" void iAdEvent__ctor_m996 (iAdEvent_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
