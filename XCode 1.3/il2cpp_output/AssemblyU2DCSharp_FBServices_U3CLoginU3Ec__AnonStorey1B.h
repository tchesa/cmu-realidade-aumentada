﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>
struct Action_2_t380;

#include "mscorlib_System_Object.h"

// FBServices/<Login>c__AnonStorey1B
struct  U3CLoginU3Ec__AnonStorey1B_t379  : public Object_t
{
	// System.Action`2<System.Boolean,Facebook.Unity.ILoginResult> FBServices/<Login>c__AnonStorey1B::callback
	Action_2_t380 * ___callback_0;
};
