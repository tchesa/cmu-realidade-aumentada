﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Byte>
struct List_1_t497;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_34.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m17226_gshared (Enumerator_t2709 * __this, List_1_t497 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m17226(__this, ___l, method) (( void (*) (Enumerator_t2709 *, List_1_t497 *, const MethodInfo*))Enumerator__ctor_m17226_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17227_gshared (Enumerator_t2709 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17227(__this, method) (( void (*) (Enumerator_t2709 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17227_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17228_gshared (Enumerator_t2709 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17228(__this, method) (( Object_t * (*) (Enumerator_t2709 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17228_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m17229_gshared (Enumerator_t2709 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17229(__this, method) (( void (*) (Enumerator_t2709 *, const MethodInfo*))Enumerator_Dispose_m17229_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::VerifyState()
extern "C" void Enumerator_VerifyState_m17230_gshared (Enumerator_t2709 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17230(__this, method) (( void (*) (Enumerator_t2709 *, const MethodInfo*))Enumerator_VerifyState_m17230_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17231_gshared (Enumerator_t2709 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17231(__this, method) (( bool (*) (Enumerator_t2709 *, const MethodInfo*))Enumerator_MoveNext_m17231_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Byte>::get_Current()
extern "C" uint8_t Enumerator_get_Current_m17232_gshared (Enumerator_t2709 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17232(__this, method) (( uint8_t (*) (Enumerator_t2709 *, const MethodInfo*))Enumerator_get_Current_m17232_gshared)(__this, method)
