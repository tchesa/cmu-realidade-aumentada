﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t218;

#include "AssemblyU2DCSharp_SAC_Singleton_1_gen.h"

// SA_ScreenShotMaker
struct  SA_ScreenShotMaker_t216  : public SAC_Singleton_1_t217
{
	// System.Action`1<UnityEngine.Texture2D> SA_ScreenShotMaker::OnScreenshotReady
	Action_1_t218 * ___OnScreenshotReady_4;
};
