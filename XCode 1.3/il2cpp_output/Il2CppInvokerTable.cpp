﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t652;
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t827;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t449;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t457;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t430;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t436;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t987;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t1252;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t1486;
// System.Net.IPAddress
struct IPAddress_t1522;
// System.Net.IPv6Address
struct IPv6Address_t1524;
// System.UriFormatException
struct UriFormatException_t1643;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.Object[]
struct ObjectU5BU5D_t34;
// System.Exception
struct Exception_t40;
// System.MulticastDelegate
struct MulticastDelegate_t10;
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1906;
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1907;
// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1890;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t1893;
// System.Reflection.MethodBase
struct MethodBase_t1457;
// System.Reflection.Module
struct Module_t2059;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t2221;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2483;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Text.StringBuilder
struct StringBuilder_t305;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2376;
// System.Char[]
struct CharU5BU5D_t493;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2367;
// System.Int64[]
struct Int64U5BU5D_t2511;
// System.String[]
struct StringU5BU5D_t260;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2877;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t722;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3054;
// System.Int32[]
struct Int32U5BU5D_t335;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3273;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1440;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1441;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2537;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2538;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_SByte.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_Double.h"
#include "AssemblyU2DCSharp_GCCollectionType.h"
#include "AssemblyU2DCSharp_GCBoardTimeSpan.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "mscorlib_System_Nullable_1_gen_0.h"
#include "mscorlib_System_Nullable_1_gen_1.h"
#include "mscorlib_System_Nullable_1_gen_2.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Char.h"
#include "AssemblyU2DCSharp_Facebook_MiniJSON_Json_Parser_TOKEN.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
#include "UnityEngine_UI_UnityEngine_UI_FontData.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
#include "mscorlib_System_Int16.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid_0.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Storag.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__11.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid_1.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntData.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD.h"
#include "mscorlib_System_UInt16.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__6.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__7.h"
#include "System_System_Collections_Generic_LinkedList_1_gen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#include "UnityEngine_UnityEngine_iPhoneGeneration.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_TerrainRenderFlags.h"
#include "UnityEngine_UnityEngine_TreeInstance.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_ColorSpace.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"
#include "UnityEngine_UnityEngine_ImagePosition.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_NetworkReachability.h"
#include "UnityEngine_UnityEngine_UserAuthorization.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "System_System_ComponentModel_EditorBrowsableState.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_Sockets_AddressFamily.h"
#include "System_System_Net_IPv6Address.h"
#include "System_System_Net_SecurityProtocolType.h"
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
#include "mscorlib_System_Byte.h"
#include "System_System_Text_RegularExpressions_RegexOptions.h"
#include "System_System_Text_RegularExpressions_Category.h"
#include "System_System_Text_RegularExpressions_OpFlags.h"
#include "System_System_Text_RegularExpressions_Interval.h"
#include "System_System_Text_RegularExpressions_Position.h"
#include "System_System_UriHostNameType.h"
#include "System_System_UriFormatException.h"
#include "mscorlib_System_UInt32.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
#include "mscorlib_System_UInt64.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_Globalization_UnicodeCategory.h"
#include "mscorlib_System_UIntPtr.h"
#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_IO_MonoIOError.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_IO_MonoFileType.h"
#include "mscorlib_System_IO_MonoIOStat.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Reflection_MonoEventInfo.h"
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
#include "mscorlib_System_DateTimeKind.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_Nullable_1_gen_4.h"
#include "mscorlib_System_MonoEnumInfo.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_Guid.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_8.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
#include "AssemblyU2DCSharp_Facebook_Unity_OGActionType.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_35.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_57.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_EyewearCali.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__5.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__4.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__8.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__9.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__10.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_40.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_42.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_49.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
#include "System_System_Text_RegularExpressions_Mark.h"
#include "System_System_Uri_UriScheme.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_29.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_11.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_34.h"
#include "mscorlib_System_Nullable_1_gen_3.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_47.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_27.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_51.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_44.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_35.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_59.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_41.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_59.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__39.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_49.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_62.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__40.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_51.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_64.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_63.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__42.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_54.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_66.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__45.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_58.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_69.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_65.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__49.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_63.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_73.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_70.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_71.h"

void* RuntimeInvoker_Void_t1856_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Color_t5_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t5  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t5 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Vector3_t6_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t6  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

void* RuntimeInvoker_Rect_t30_Rect_t30_Rect_t30_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, Rect_t30  p1, Rect_t30  p2, float p3, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), *((Rect_t30 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Vector3_t6_Vector3_t6_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, float p3, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t29_Vector2_t29_Vector2_t29_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, float p3, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t5  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Vector3_t6_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t5  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t5 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Color_t5_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t5  p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t5 *)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Single_t36_Single_t36_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Single_t36_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Double_t60_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Single_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GCCollectionType_t87 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GCBoardTimeSpan_t86 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_SByte_t559_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Object_t_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Single_t36_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t220  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t220 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TextAnchor_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_DateTime_t220_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, DateTime_t220  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((DateTime_t220 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t_Object_t_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, Object_t * p7, int8_t p8, Object_t * p9, Object_t * p10, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int8_t*)args[7]), (Object_t *)args[8], (Object_t *)args[9], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t466_Object_t_Object_t_Object_t_Object_t_Nullable_1_t467_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Nullable_1_t466  p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Nullable_1_t467  p7, Object_t * p8, Object_t * p9, Object_t * p10, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Nullable_1_t466 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((Nullable_1_t467 *)args[6]), (Object_t *)args[7], (Object_t *)args[8], (Object_t *)args[9], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Nullable_1_t467_Nullable_1_t467_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Nullable_1_t467  p4, Nullable_1_t467  p5, Object_t * p6, Object_t * p7, Object_t * p8, Object_t * p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((Nullable_1_t467 *)args[3]), *((Nullable_1_t467 *)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], (Object_t *)args[8], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t474_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Nullable_1_t474  p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Nullable_1_t474 *)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, Object_t * p8, Object_t * p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], (Object_t *)args[7], (Object_t *)args[8], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Object_t_Object_t_Nullable_1_t467_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Nullable_1_t467  p6, Object_t * p7, Object_t * p8, Object_t * p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((Nullable_1_t467 *)args[5]), (Object_t *)args[6], (Object_t *)args[7], (Object_t *)args[8], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Nullable_1_t467_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Nullable_1_t467  p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((Nullable_1_t467 *)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_ShareDialogMode_t264 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t_Object_t_Int32_t59_Object_t_Object_t_Int32_t59_SByte_t559_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int32_t p6, Object_t * p7, Object_t * p8, int32_t p9, int8_t p10, int32_t p11, Object_t * p12, Object_t * p13, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int32_t*)args[5]), (Object_t *)args[6], (Object_t *)args[7], *((int32_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), (Object_t *)args[11], (Object_t *)args[12], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Double_t60_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Double_t60_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t_StringU26_t4016 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Char_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TOKEN_t301 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, DateTime_t220  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3_t6_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6  p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6 *)args[1]), *((float*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_InitError_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_RaycastResult_t647_RaycastResult_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t647  p1, RaycastResult_t647  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t647 *)args[0]), *((RaycastResult_t647 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t29 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_MoveDirection_t644 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t647  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t647  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_RaycastResult_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t647  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t647 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_InputButton_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t647_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t647  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t647  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t644_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t644_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_PointerEventDataU26_t4017_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t652 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t652 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Touch_t818_BooleanU26_t4018_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t818  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t818 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

void* RuntimeInvoker_FramePressState_t651_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector2_t29_Vector2_t29_Single_t36_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_InputMode_t660 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_SByte_t559_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_LayerMask_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t665  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t665  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_LayerMask_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t665  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t665 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_RaycastHit_t566_RaycastHit_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t566  p1, RaycastHit_t566  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t566 *)args[0]), *((RaycastHit_t566 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t5  (*Func)(void* obj, const MethodInfo* method);
	Color_t5  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t5  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t5 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_ColorTweenMode_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ColorBlock_t682 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t682  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t682  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FontData_t683 (const MethodInfo* method, void* obj, void** args)
{
	typedef FontData_t683  (*Func)(void* obj, const MethodInfo* method);
	FontData_t683  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FontStyle_t1398 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HorizontalWrapMode_t1371 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VerticalWrapMode_t1372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector2_t29_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t29  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t29_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Vector2_t29  p1, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Color_t5_Single_t36_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t5  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t5 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Color_t5_Single_t36_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t5  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t5 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t5_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t5  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t5  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Single_t36_Single_t36_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_BlockingObjects_t695 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Vector2_t29_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t29  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t29 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Type_t701 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FillMethod_t702 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t822_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_UIVertex_t727_Vector2_t29_Vector2_t29_Vector2_t29_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UIVertex_t727  p2, Vector2_t29  p3, Vector2_t29  p4, Vector2_t29  p5, Vector2_t29  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t727 *)args[1]), *((Vector2_t29 *)args[2]), *((Vector2_t29 *)args[3]), *((Vector2_t29 *)args[4]), *((Vector2_t29 *)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t822_Vector4_t822_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, Vector4_t822  p1, Rect_t30  p2, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, *((Vector4_t822 *)args[0]), *((Rect_t30 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Single_t36_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Single_t36_Single_t36_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t29_Vector2_t29_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Vector2_t29  p1, Rect_t30  p2, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Rect_t30 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContentType_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LineType_t713 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_InputType_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TouchScreenKeyboardType_t854 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterValidation_t712 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Vector2_t29_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t29  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t29  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditState_t717_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32U26_t4019_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t29  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t29 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t36_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t556_Object_t_Int32_t59_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Int16_t561_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Char_t556_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mode_t729 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Navigation_t730 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t730  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t730  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Direction_t732 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Single_t36_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Axis_t735 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MovementType_t739 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Bounds_t742 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t742  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t742  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Navigation_t730 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t730  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t730 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Transition_t743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ColorBlock_t682 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t682  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t682 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SpriteState_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t745  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t745  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_SpriteState_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t745  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t745 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SelectionState_t744 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Vector3_t6_Object_t_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Object_t * p1, Vector2_t29  p2, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t29 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Color_t5_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t5  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t5 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_ColorU26_t4020_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t5 * p1, Color_t5  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t5 *)args[0], *((Color_t5 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Direction_t749 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t751 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextGenerationSettings_t824_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t824  (*Func)(void* obj, Vector2_t29  p1, const MethodInfo* method);
	TextGenerationSettings_t824  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t29_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AspectMode_t764 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScaleMode_t766 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScreenMatchMode_t767 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Unit_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FitMode_t770 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Corner_t772 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t773 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Constraint_t774 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Int32_t59_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_LayoutRebuilder_t782 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LayoutRebuilder_t782  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LayoutRebuilder_t782 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Object_t_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Object_t_Object_t_Single_t36_ILayoutElementU26_t4021 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Color32_t829_Int32_t59_Int32_t59_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t829  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t829 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_CameraDirection_t896 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoBackgroundReflection_t990 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, Matrix4x4_t603  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((Matrix4x4_t603 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t51  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Vector3U26_t4022_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6 * p1, Vector3_t6 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t6 *)args[0], (Vector3_t6 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector3_t6_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Matrix4x4_t603  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t603 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScreenOrientation_t1126 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Matrix4x4_t603_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t603  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Matrix4x4_t603  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t603 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_Single_t36_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, float p4, int32_t p5, Object_t * p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((float*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Matrix4x4_t603_SingleU26_t4023_SingleU26_t4023 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t603  p1, float* p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), (float*)args[1], (float*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Single_t36_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Matrix4x4_t603  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Vector4_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Vector4_t822  p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Vector4_t822 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Status_t889 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoModeData_t897 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoModeData_t897  (*Func)(void* obj, const MethodInfo* method);
	VideoModeData_t897  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoModeData_t897_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoModeData_t897  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	VideoModeData_t897  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_CameraDeviceModeU26_t4024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t30  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_RectU26_t4025 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t30 * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t30 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t6_Quaternion_t51_Vector3_t6_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector3_t6  p3, Quaternion_t51  p4, Vector3_t6  p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector3_t6 *)args[2]), *((Quaternion_t51 *)args[3]), *((Vector3_t6 *)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_Object_t_Vector3_t6_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector3_t6  p2, Vector3_t6  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6 *)args[1]), *((Vector3_t6 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Vector3_t6_Vector3_t6_Vector3_t6_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector3_t6  p2, Vector3_t6  p3, Vector3_t6  p4, Quaternion_t51  p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6 *)args[1]), *((Vector3_t6 *)args[2]), *((Vector3_t6 *)args[3]), *((Quaternion_t51 *)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3U26_t4022_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t6 * p1, Vector3_t6 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t6 *)args[0], (Vector3_t6 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3U26_t4022_Vector3U26_t4022_Vector3U26_t4022_QuaternionU26_t4026 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t6 * p1, Vector3_t6 * p2, Vector3_t6 * p3, Quaternion_t51 * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t6 *)args[0], (Vector3_t6 *)args[1], (Vector3_t6 *)args[2], (Quaternion_t51 *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_IntPtr_t_Object_t_Vector3_t6_Vector3_t6_Vector3_t6_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector3_t6  p3, Vector3_t6  p4, Vector3_t6  p5, Quaternion_t51  p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((Vector3_t6 *)args[2]), *((Vector3_t6 *)args[3]), *((Vector3_t6 *)args[4]), *((Quaternion_t51 *)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StorageType_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector2_t29_Vector2_t29_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Vector3_t6_Vector3_t6_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_TargetSearchResult_t1050 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResult_t1050  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_ImageTargetType_t932 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_RectangleData_t923 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, RectangleData_t923  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RectangleData_t923 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Vector3_t6  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((Vector3_t6 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FrameQuality_t934 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_VirtualButtonAbstractBehaviourU26_t4027 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, VirtualButtonAbstractBehaviour_t449 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (VirtualButtonAbstractBehaviour_t449 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordFilterMode_t1087 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordPrefabCreationMode_t1015 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sensitivity_t1060 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t603  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector2_t29_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordTemplateMode_t941 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PIXEL_FORMAT_t942 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextureFormat_t1257_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Vec2I_t992 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vec2I_t992  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vec2I_t992 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ProfileCollection_t1065_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileCollection_t1065  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	ProfileCollection_t1065  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WorldCenterMode_t1075 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_FrameState_t980 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FrameState_t980  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((FrameState_t980 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_FrameState_t980_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FrameState_t980  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((FrameState_t980 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TrackableResultData_t969  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TrackableResultData_t969 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoBGCfgData_t991 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoBGCfgData_t991  (*Func)(void* obj, const MethodInfo* method);
	VideoBGCfgData_t991  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_VideoBGCfgData_t991 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, VideoBGCfgData_t991  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((VideoBGCfgData_t991 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_VideoTextureInfo_t880 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoTextureInfo_t880  (*Func)(void* obj, const MethodInfo* method);
	VideoTextureInfo_t880  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603_Single_t36_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, float p1, float p2, int32_t p3, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_PoseData_t968 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PoseData_t968  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((PoseData_t968 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_OrientedBoundingBox3D_t926 (const MethodInfo* method, void* obj, void** args)
{
	typedef OrientedBoundingBox3D_t926  (*Func)(void* obj, const MethodInfo* method);
	OrientedBoundingBox3D_t926  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_OrientedBoundingBox3D_t926 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OrientedBoundingBox3D_t926  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OrientedBoundingBox3D_t926 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Rect_t30_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t30  p1, Rect_t30  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), *((Rect_t30 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_RectU26_t4025_RectU26_t4025 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t30 * p1, Rect_t30 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t30 *)args[0], (Rect_t30 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_RectangleIntData_t924_Rect_t30_SByte_t559_VideoModeData_t897 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, RectangleIntData_t924  p1, Rect_t30  p2, int8_t p3, VideoModeData_t897  p4, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((RectangleIntData_t924 *)args[0]), *((Rect_t30 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t897 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UpDirection_t1006 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Vec2I_t992 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Vec2I_t992  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((Vec2I_t992 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Vector2_t29  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((Vector2_t29 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_ImageHeaderData_t975_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ImageHeaderData_t975  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((ImageHeaderData_t975 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t_WordAbstractBehaviourU26_t4028 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, WordAbstractBehaviour_t457 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (WordAbstractBehaviour_t457 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OrientedBoundingBox_t925 (const MethodInfo* method, void* obj, void** args)
{
	typedef OrientedBoundingBox_t925  (*Func)(void* obj, const MethodInfo* method);
	OrientedBoundingBox_t925  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector3_t6_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6  p1, Quaternion_t51  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Quaternion_t51 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_OrientedBoundingBox_t925 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OrientedBoundingBox_t925  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OrientedBoundingBox_t925 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_IntPtr_t_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_IntPtr_t_Object_t_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, Object_t * p3, int32_t p4, IntPtr_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((IntPtr_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, IntPtr_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((IntPtr_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_IntPtr_t_Int32_t59_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Int32_t59_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Single_t36_Single_t36_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_IntPtr_t_IntPtr_t_Int32_t59_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, IntPtr_t p5, IntPtr_t p6, IntPtr_t p7, float p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), *((IntPtr_t*)args[4]), *((IntPtr_t*)args[5]), *((IntPtr_t*)args[6]), *((float*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_IntPtr_t_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_SmartTerrainInitializationInfo_t916 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SmartTerrainInitializationInfo_t916  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SmartTerrainInitializationInfo_t916 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t_PropAbstractBehaviourU26_t4029 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, PropAbstractBehaviour_t430 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (PropAbstractBehaviour_t430 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_SurfaceAbstractBehaviourU26_t4030 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, SurfaceAbstractBehaviour_t436 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (SurfaceAbstractBehaviour_t436 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_MeshData_t976_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, MeshData_t976  p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((MeshData_t976 *)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t59_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_LinkedList_1U26_t4031 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LinkedList_1_t987 ** p1, const MethodInfo* method);
	((Func)method->method)(obj, (LinkedList_1_t987 **)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_PoseData_t968 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, PoseData_t968  p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((PoseData_t968 *)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t6_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Matrix4x4_t603  p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Matrix4x4_t603  p1, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_PoseData_t968 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, PoseData_t968  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((PoseData_t968 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_InitState_t1047 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UpdateState_t1048 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UpdateState_t1048_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_TargetSearchResult_t1050_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TargetSearchResult_t1050  p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_RectangleData_t923 (const MethodInfo* method, void* obj, void** args)
{
	typedef RectangleData_t923  (*Func)(void* obj, const MethodInfo* method);
	RectangleData_t923  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_RectangleData_t923 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RectangleData_t923  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RectangleData_t923 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_RectangleData_t923_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, RectangleData_t923  p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((RectangleData_t923 *)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Vec2I_t992 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vec2I_t992  (*Func)(void* obj, const MethodInfo* method);
	Vec2I_t992  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t1064  (*Func)(void* obj, const MethodInfo* method);
	ProfileData_t1064  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ProfileData_t1064_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t1064  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	ProfileData_t1064  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ProfileData_t1064_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ProfileData_t1064  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((ProfileData_t1064 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Vector2_t29_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t29  p2, Vector2_t29  p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t29 *)args[1]), *((Vector2_t29 *)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector2_t29_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t29  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t29 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_CameraDeviceMode_t894 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vec2I_t992_Vector2_t29_Rect_t30_SByte_t559_VideoModeData_t897 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vec2I_t992  (*Func)(void* obj, Vector2_t29  p1, Rect_t30  p2, int8_t p3, VideoModeData_t897  p4, const MethodInfo* method);
	Vec2I_t992  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Rect_t30 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t897 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t29_Vector2_t29_Rect_t30_SByte_t559_VideoModeData_t897 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Vector2_t29  p1, Rect_t30  p2, int8_t p3, VideoModeData_t897  p4, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Rect_t30 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t897 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OrientedBoundingBox_t925_OrientedBoundingBox_t925_Rect_t30_SByte_t559_VideoModeData_t897 (const MethodInfo* method, void* obj, void** args)
{
	typedef OrientedBoundingBox_t925  (*Func)(void* obj, OrientedBoundingBox_t925  p1, Rect_t30  p2, int8_t p3, VideoModeData_t897  p4, const MethodInfo* method);
	OrientedBoundingBox_t925  ret = ((Func)method->method)(obj, *((OrientedBoundingBox_t925 *)args[0]), *((Rect_t30 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t897 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Rect_t30_SByte_t559_Vector2U26_t4032_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, int8_t p2, Vector2_t29 * p3, Vector2_t29 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), *((int8_t*)args[1]), (Vector2_t29 *)args[2], (Vector2_t29 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t30_Vector2_t29_Vector2_t29_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, int8_t p3, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SingleU26_t4023_SingleU26_t4023_SingleU26_t4023_SingleU26_t4023_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, float* p2, float* p3, float* p4, float* p5, bool* p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (float*)args[1], (float*)args[2], (float*)args[3], (float*)args[4], (bool*)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Vector2U26_t4032_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t29 * p1, Vector2_t29 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector2_t29 *)args[0], (Vector2_t29 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector2_t29_Vector2_t29_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_UserProfileU5BU5DU26_t4033_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t1252** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t1252**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_GcAchievementDescriptionData_t1243_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t1243  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t1243 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_GcUserProfileData_t1242_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t1242  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t1242 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Double_t60_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_UserProfileU5BU5DU26_t4033_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t1252** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t1252**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_UserState_t1274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Double_t60_SByte_t559_SByte_t559_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t220  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t220 *)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Object_t_DateTime_t220_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t220  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t220 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_UserScope_t1275 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Range_t1269 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t1269  (*Func)(void* obj, const MethodInfo* method);
	Range_t1269  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Range_t1269 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t1269  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t1269 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_TimeScope_t1276 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_HitInfo_t1271 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t1271  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t1271 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_HitInfo_t1271_HitInfo_t1271 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t1271  p1, HitInfo_t1271  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t1271 *)args[0]), *((HitInfo_t1271 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_HitInfo_t1271 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t1271  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t1271 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_StringU26_t4016_StringU26_t4016 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_StreamingContext_t1439 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t1439  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1439 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Color_t5_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t5  p1, Color_t5  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), *((Color_t5 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_TextGenerationSettings_t824 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t824  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t824 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextGenerationSettings_t824_TextGenerationSettings_t824 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t824  (*Func)(void* obj, TextGenerationSettings_t824  p1, const MethodInfo* method);
	TextGenerationSettings_t824  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t824 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Object_t_TextGenerationSettings_t824 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t824  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t824 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_TextGenerationSettings_t824 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t824  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t824 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Color_t5_Int32_t59_Single_t36_Single_t36_Int32_t59_SByte_t559_SByte_t559_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_Int32_t59_Vector2_t29_Vector2_t29_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t5  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t29  p16, Vector2_t29  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t5 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t29 *)args[15]), *((Vector2_t29 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Color_t5_Int32_t59_Single_t36_Single_t36_Int32_t59_SByte_t559_SByte_t559_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_Int32_t59_Single_t36_Single_t36_Single_t36_Single_t36_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t5  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t5 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_ColorU26_t4020_Int32_t59_Single_t36_Single_t36_Int32_t59_SByte_t559_SByte_t559_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_Int32_t59_Single_t36_Single_t36_Single_t36_Single_t36_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t5 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t5 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_RectU26_t4025 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t30 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_PersistentListenerMode_t1291 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_iPhoneGeneration_t501 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t6 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Vector3_t6_Vector3_t6_RaycastHitU26_t4034_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, RaycastHit_t566 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), (RaycastHit_t566 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector3U26_t4022_Vector3U26_t4022_RaycastHitU26_t4034_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6 * p1, Vector3_t6 * p2, RaycastHit_t566 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t6 *)args[0], (Vector3_t6 *)args[1], (RaycastHit_t566 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Ray_t567_RaycastHitU26_t4034_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t567  p1, RaycastHit_t566 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t567 *)args[0]), (RaycastHit_t566 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t567_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t567  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t567 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3_t6_Vector3_t6_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3U26_t4022_Vector3U26_t4022_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t6 * p1, Vector3_t6 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t6 *)args[0], (Vector3_t6 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t6 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_QuaternionU26_t4026 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Quaternion_t51 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Quaternion_t51 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t_Ray_t567_RaycastHitU26_t4034_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t567  p2, RaycastHit_t566 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Ray_t567 *)args[1]), (RaycastHit_t566 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_RayU26_t4035_RaycastHitU26_t4034_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t567 * p2, RaycastHit_t566 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t567 *)args[1], (RaycastHit_t566 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Ray_t567_RaycastHitU26_t4034_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t567  p1, RaycastHit_t566 * p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t567 *)args[0]), (RaycastHit_t566 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector2_t29_Vector2_t29_Single_t36_Int32_t59_Single_t36_Single_t36_RaycastHit2DU26_t4036 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t838 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t838 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Vector2U26_t4032_Vector2U26_t4032_Single_t36_Int32_t59_Single_t36_Single_t36_RaycastHit2DU26_t4036 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29 * p1, Vector2_t29 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t838 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t29 *)args[0], (Vector2_t29 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t838 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t838_Vector2_t29_Vector2_t29_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t838  (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t838  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t838_Vector2_t29_Vector2_t29_Single_t36_Int32_t59_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t838  (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t838  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t29_Vector2_t29_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector2U26_t4032_Vector2U26_t4032_Single_t36_Int32_t59_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t29 * p1, Vector2_t29 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t29 *)args[0], (Vector2_t29 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_Ray_t567_Object_t_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Ray_t567  p1, Object_t * p2, float p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Ray_t567 *)args[0]), (Object_t *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_RayU26_t4035_Object_t_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Ray_t567 * p1, Object_t * p2, float p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Ray_t567 *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t6  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_SendMessageOptions_t1345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_SByte_t559_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_TerrainRenderFlags_t1333 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Single_t36_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_IntPtr_t_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Vector3_t6  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Object_t_IntPtr_t_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Vector3_t6 * p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Vector3_t6 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_TreeInstance_t1330 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TreeInstance_t1330  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TreeInstance_t1330 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_TreeInstance_t1330 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, TreeInstance_t1330  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((TreeInstance_t1330 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_IntPtr_t_TreeInstanceU26_t4037 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, TreeInstance_t1330 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (TreeInstance_t1330 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, IntPtr_t p3, IntPtr_t p4, IntPtr_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), *((IntPtr_t*)args[3]), *((IntPtr_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t6_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector2_t29_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29  p1, float p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Vector2_t29_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Vector2_t29  p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Vector2_t29 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_IntPtr_t_Vector2U26_t4032_Single_t36_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Vector2_t29 * p3, float p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Vector2_t29 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t29 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t_Vector2_t29_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t29  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t29 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Vector2U26_t4032_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t29 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t29 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t29_Vector2_t29_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Vector2_t29  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector2_t29_Object_t_Object_t_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29  p1, Object_t * p2, Object_t * p3, Vector2_t29 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t29 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t29 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Vector2U26_t4032_Object_t_Object_t_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29 * p1, Object_t * p2, Object_t * p3, Vector2_t29 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t29 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t29 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t30_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Vector2_t29_Object_t_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t29  p2, Object_t * p3, Vector3_t6 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t29 *)args[1]), (Object_t *)args[2], (Vector3_t6 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Vector2_t29_Object_t_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t29  p2, Object_t * p3, Vector2_t29 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t29 *)args[1]), (Object_t *)args[2], (Vector2_t29 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t567_Object_t_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t567  (*Func)(void* obj, Object_t * p1, Vector2_t29  p2, const MethodInfo* method);
	Ray_t567  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t29 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_RenderMode_t1338 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_ColorU26_t4020 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t5 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t5 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_LayerMask_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t665  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t665 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LayerMask_t665_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t665  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t665  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_GcScoreData_t1245 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t1245  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t1245 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_ColorSpace_t1367 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_BoundsU26_t4038 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t742 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Bounds_t742 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_BoneWeight_t1361_BoneWeight_t1361 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t1361  p1, BoneWeight_t1361  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t1361 *)args[0]), *((BoneWeight_t1361 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_InternalDrawTextureArgumentsU26_t4039 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, InternalDrawTextureArguments_t1364 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (InternalDrawTextureArguments_t1364 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t603  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Matrix4x4U26_t4040 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t603 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t603 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t5  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t5 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_Color_t5_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t5  p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t5 *)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_ColorU26_t4020_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t5 * p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Color_t5 *)args[2], *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_TextureFormat_t1257 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Color_t5  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((Color_t5 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_ColorU26_t4020 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Color_t5 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Color_t5 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Color_t5_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t5  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t5  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Int32_t59_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_RectU26_t4025_Int32_t59_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t30 * p2, int32_t p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Rect_t30 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_ColorU26_t4020 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t5 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t5 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Color_t5_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t5  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t5 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_RectU26_t4025_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t30 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Object_t_Int32_t59_SByte_t559_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, Object_t * p2, int32_t p3, int8_t p4, float p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), *((float*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Rect_t30_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t30  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Rect_t30_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t30  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Rect_t30_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t30  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_RectU26_t4025_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t30 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t30 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Int32_t59_Object_t_SByte_t559_Int32_t59_Object_t_Object_t_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, int32_t p2, Object_t * p3, int8_t p4, int32_t p5, Object_t * p6, Object_t * p7, int16_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int16_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Int32_t59_Object_t_SByte_t559_Int32_t59_Object_t_Object_t_Int16_t561_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, int32_t p2, Object_t * p3, int8_t p4, int32_t p5, Object_t * p6, Object_t * p7, int16_t p8, Object_t * p9, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int16_t*)args[7]), (Object_t *)args[8], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t30_Int32_t59_Rect_t30_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, int32_t p1, Rect_t30  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t30 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_Int32_t59_Rect_t30_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, int32_t p1, Rect_t30  p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t30 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_Int32_t59_Rect_t30_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, int32_t p1, Rect_t30  p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t30 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_Int32_t59_RectU26_t4025_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, int32_t p1, Rect_t30 * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t30 *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Int32_t59_Single_t36_Single_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t30_Int32_t59_Rect_t30_Object_t_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, int32_t p1, Rect_t30  p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t30 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_Int32_t59_RectU26_t4025_Object_t_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, int32_t p1, Rect_t30 * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t30 *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Rect_t30_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t30  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t30 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_RectU26_t4025 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t30 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t30 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t30_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_Single_t36_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, float p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_Single_t36_Single_t36_Single_t36_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36_Single_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Vector2_t29_Vector2_t29_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, Vector2_t29  p2, Vector2_t29  p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), *((Vector2_t29 *)args[1]), *((Vector2_t29 *)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_RectU26_t4025_Vector2U26_t4032_Vector2U26_t4032_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30 * p1, Vector2_t29 * p2, Vector2_t29 * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t30 *)args[0], (Vector2_t29 *)args[1], (Vector2_t29 *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Rect_t30_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, Rect_t30  p1, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_Object_t_RectU26_t4025 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, Object_t * p1, Rect_t30 * p2, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Rect_t30 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ImagePosition_t1399 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Rect_t30_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t30  p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Rect_t30_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t30  p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t30 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_RectU26_t4025_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t30 * p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t30 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t29_Rect_t30_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Rect_t30  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Rect_t30_Object_t_Int32_t59_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t30  p2, Object_t * p3, int32_t p4, Vector2_t29 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t30 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t29 *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_RectU26_t4025_Object_t_Int32_t59_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t30 * p2, Object_t * p3, int32_t p4, Vector2_t29 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t30 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t29 *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t29_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Object_t_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector2_t29 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Vector2_t29 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Single_t36_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_IntPtr_t_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_SingleU26_t4023_SingleU26_t4023 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float* p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (float*)args[1], (float*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Object_t_SingleU26_t4023_SingleU26_t4023 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float* p3, float* p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (float*)args[2], (float*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t4041_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t1400 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t1400 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

void* RuntimeInvoker_EventType_t1402 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EventType_t1402_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Vector2U26_t4032 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t29 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t29 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Ray_t567 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t567  (*Func)(void* obj, const MethodInfo* method);
	Ray_t567  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Ray_t567 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Ray_t567  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Ray_t567 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_EventModifiers_t1403 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyCode_t1401 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector3_t6_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Vector3U26_t4022_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6 * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t6 *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t29_Vector2_t29_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Vector2_t29  p1, Vector2_t29  p2, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((Vector2_t29 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t29  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t29_Vector2_t29_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Vector2_t29  p1, float p2, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t29_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t29  (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	Vector2_t29  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Vector2_t29  p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Vector3_t6_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Vector3_t6_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Vector3_t6_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Vector3_t6  p1, float p2, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Single_t36_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, float p1, Vector3_t6  p2, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector3_t6_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t5_Color_t5_Color_t5_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t5  (*Func)(void* obj, Color_t5  p1, Color_t5  p2, float p3, const MethodInfo* method);
	Color_t5  ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), *((Color_t5 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t5_Color_t5_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t5  (*Func)(void* obj, Color_t5  p1, float p2, const MethodInfo* method);
	Color_t5  ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t822_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, Color_t5  p1, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t829_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t829  (*Func)(void* obj, Color_t5  p1, const MethodInfo* method);
	Color32_t829  ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t5_Color32_t829 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t5  (*Func)(void* obj, Color32_t829  p1, const MethodInfo* method);
	Color_t5  ret = ((Func)method->method)(obj, *((Color32_t829 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Quaternion_t51_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t51  p1, Quaternion_t51  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), *((Quaternion_t51 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Single_t36_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, float p1, Vector3_t6  p2, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Single_t36_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, float p1, Vector3_t6 * p2, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((float*)args[0]), (Vector3_t6 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_SingleU26_t4023_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, Vector3_t6 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], (Vector3_t6 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Quaternion_t51_Vector3_t6_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Vector3_t6  p1, Vector3_t6  p2, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Vector3U26_t4022_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Vector3_t6 * p1, Vector3_t6 * p2, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, (Vector3_t6 *)args[0], (Vector3_t6 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Quaternion_t51_Quaternion_t51_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Quaternion_t51  p1, Quaternion_t51  p2, float p3, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), *((Quaternion_t51 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_QuaternionU26_t4026_QuaternionU26_t4026_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Quaternion_t51 * p1, Quaternion_t51 * p2, float p3, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, (Quaternion_t51 *)args[0], (Quaternion_t51 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Quaternion_t51  p1, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_QuaternionU26_t4026 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Quaternion_t51 * p1, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, (Quaternion_t51 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Quaternion_t51  p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_QuaternionU26_t4026 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Quaternion_t51 * p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, (Quaternion_t51 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t51_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Vector3_t6 * p1, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, (Vector3_t6 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Quaternion_t51_Vector3U26_t4022_SingleU26_t4023 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t51  p1, Vector3_t6 * p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), (Vector3_t6 *)args[1], (float*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_QuaternionU26_t4026_Vector3U26_t4022_SingleU26_t4023 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t51 * p1, Vector3_t6 * p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t51 *)args[0], (Vector3_t6 *)args[1], (float*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Quaternion_t51_Quaternion_t51_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t51  (*Func)(void* obj, Quaternion_t51  p1, Quaternion_t51  p2, const MethodInfo* method);
	Quaternion_t51  ret = ((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), *((Quaternion_t51 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Quaternion_t51_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Quaternion_t51  p1, Vector3_t6  p2, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Quaternion_t51_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t51  p1, Quaternion_t51  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t51 *)args[0]), *((Quaternion_t51 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t30_Single_t36_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t30  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Rect_t30  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t29  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t603_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, Matrix4x4_t603  p1, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603_Matrix4x4U26_t4040 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, Matrix4x4_t603 * p1, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, (Matrix4x4_t603 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Matrix4x4_t603_Matrix4x4U26_t4040 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t603  p1, Matrix4x4_t603 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), (Matrix4x4_t603 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Matrix4x4U26_t4040_Matrix4x4U26_t4040 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t603 * p1, Matrix4x4_t603 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t603 *)args[0], (Matrix4x4_t603 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t822_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Vector4_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t822  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t822 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t603_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector3_t6_Quaternion_t51_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6  p1, Quaternion_t51  p2, Vector3_t6  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Quaternion_t51 *)args[1]), *((Vector3_t6 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t603_Vector3_t6_Quaternion_t51_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, Vector3_t6  p1, Quaternion_t51  p2, Vector3_t6  p3, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((Quaternion_t51 *)args[1]), *((Vector3_t6 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603_Vector3U26_t4022_QuaternionU26_t4026_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, Vector3_t6 * p1, Quaternion_t51 * p2, Vector3_t6 * p3, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, (Vector3_t6 *)args[0], (Quaternion_t51 *)args[1], (Vector3_t6 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603_Single_t36_Single_t36_Single_t36_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603_Single_t36_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t603_Matrix4x4_t603_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t603  (*Func)(void* obj, Matrix4x4_t603  p1, Matrix4x4_t603  p2, const MethodInfo* method);
	Matrix4x4_t603  ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), *((Matrix4x4_t603 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t822_Matrix4x4_t603_Vector4_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, Matrix4x4_t603  p1, Vector4_t822  p2, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), *((Vector4_t822 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Matrix4x4_t603_Matrix4x4_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t603  p1, Matrix4x4_t603  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t603 *)args[0]), *((Matrix4x4_t603 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Bounds_t742 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t742  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t742 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Bounds_t742 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t742  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t742 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Bounds_t742_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t742  p1, Vector3_t6  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t742 *)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_BoundsU26_t4038_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t742 * p1, Vector3_t6 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t742 *)args[0], (Vector3_t6 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Bounds_t742_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t742  p1, Vector3_t6  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t742 *)args[0]), *((Vector3_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_BoundsU26_t4038_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t742 * p1, Vector3_t6 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t742 *)args[0], (Vector3_t6 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_RayU26_t4035_BoundsU26_t4038_SingleU26_t4023 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t567 * p1, Bounds_t742 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t567 *)args[0], (Bounds_t742 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Ray_t567 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t567  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t567 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Ray_t567_SingleU26_t4023 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t567  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t567 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Bounds_t742_Bounds_t742 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t742  p1, Bounds_t742  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t742 *)args[0]), *((Bounds_t742 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Vector4_t822_Vector4_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t822  p1, Vector4_t822  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t822 *)args[0]), *((Vector4_t822 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Vector4_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t822  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t822 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t822_Vector4_t822_Vector4_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, Vector4_t822  p1, Vector4_t822  p2, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, *((Vector4_t822 *)args[0]), *((Vector4_t822 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t822_Vector4_t822_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, Vector4_t822  p1, float p2, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, *((Vector4_t822 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Vector4_t822_Vector4_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t822  p1, Vector4_t822  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t822 *)args[0]), *((Vector4_t822 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Single_t36_Single_t36_SingleU26_t4023_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Single_t36_Single_t36_SingleU26_t4023_Single_t36_Single_t36_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t5  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color_t5 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_ColorU26_t4020 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Color_t5 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Color_t5 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Color_t5_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t5  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Color_t5  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t5_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t5  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color_t5  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Vector4U26_t4042 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t822 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4_t822 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t822_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t822  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t822  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559_SByte_t559_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_RuntimePlatform_t1347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_NetworkReachability_t1415 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UserAuthorization_t1418 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_CameraClearFlags_t1360 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_Object_t_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, Object_t * p1, Vector3_t6 * p2, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t567_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t567  (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	Ray_t567  ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t567_Object_t_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t567  (*Func)(void* obj, Object_t * p1, Vector3_t6 * p2, const MethodInfo* method);
	Ray_t567  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RenderBuffer_t1363 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t1363  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t1363  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Int32U26_t4019_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_RenderBufferU26_t4043_RenderBufferU26_t4043 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t1363 * p2, RenderBuffer_t1363 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t1363 *)args[1], (RenderBuffer_t1363 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TouchPhase_t1425 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Touch_t818_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t818  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t818  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Vector3_t6_Quaternion_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6  p2, Quaternion_t51  p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6 *)args[1]), *((Quaternion_t51 *)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t4022_QuaternionU26_t4026 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6 * p2, Quaternion_t51 * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6 *)args[1], (Quaternion_t51 *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_QuaternionU26_t4026 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t51 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t51 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Vector3_t6_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Vector3U26_t4022_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t6 * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6 *)args[1], *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Vector3U26_t4022_Vector3U26_t4022 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t6 * p2, Vector3_t6 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6 *)args[1], (Vector3_t6 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t4044 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t1486 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t1486 **)args[1], method);
	return ret;
}

void* RuntimeInvoker_DictionaryEntry_t58 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditorBrowsableState_t1500 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_IPAddressU26_t4045 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t1522 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t1522 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AddressFamily_t1505 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_IPv6AddressU26_t4046 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t1524 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t1524 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t1525 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_AsnDecodeStatus_t1574_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_X509ChainStatusFlags_t1561_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1561_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1561_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32U26_t4019_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_X509RevocationFlag_t1568 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509RevocationMode_t1569 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509VerificationFlags_t1573 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t1566 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t1566_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t560_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Int16_t561_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_RegexOptions_t1590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Category_t1597_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_UInt16_t562_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int16_t561_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_UInt16_t562_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int16_t561_Int16_t561_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int16_t561_Object_t_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_UInt16_t562 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_UInt16_t562_UInt16_t562_UInt16_t562 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OpFlags_t1592_SByte_t559_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_UInt16_t562_UInt16_t562 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32U26_t4019_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32U26_t4019_Int32U26_t4019_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32U26_t4019_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_UInt16_t562_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32U26_t4019_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t1612 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1612  (*Func)(void* obj, const MethodInfo* method);
	Interval_t1612  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Interval_t1612 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t1612  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t1612 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Interval_t1612 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t1612  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t1612 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t1612_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1612  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t1612  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Double_t60_Interval_t1612 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t1612  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t1612 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Interval_t1612_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t1612  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t1612 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Double_t60_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32U26_t4019_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RegexOptionsU26_t4047 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_RegexOptionsU26_t4047_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Int32U26_t4019_Int32U26_t4019_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t1597 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int16_t561_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t556_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32U26_t4019_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32U26_t4019_Int32U26_t4019_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_UInt16_t562_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int16_t561_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_UInt16_t562 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Position_t1593 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriHostNameType_t1645_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_StringU26_t4016 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Char_t556_Object_t_Int32U26_t4019_CharU26_t4048 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_UriFormatExceptionU26_t4049 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t1643 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t1643 **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_UInt32_t558_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t1690_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

void* RuntimeInvoker_ConfidenceFactor_t1696 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32U26_t4019_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32U26_t4019_ByteU26_t4050_Int32U26_t4019_ByteU5BU5DU26_t4051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t119** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t119**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t1674 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t1674  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t1674 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_RSAParameters_t1672_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1672  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t1672  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_RSAParameters_t1672 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t1672  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t1672 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_DSAParameters_t1674_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1674  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t1674  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t220  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Byte_t560_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_AlertLevel_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AlertDescription_t1737 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Int16_t561_Object_t_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Int16_t561_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_CipherAlgorithmType_t1739 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HashAlgorithmType_t1758 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExchangeAlgorithmType_t1756 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CipherMode_t1845 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_ByteU5BU5DU26_t4051_ByteU5BU5DU26_t4051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t119** p2, ByteU5BU5D_t119** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t119**)args[1], (ByteU5BU5D_t119**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t560_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t561_Object_t_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Int16_t561_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

void* RuntimeInvoker_SecurityProtocolType_t1772 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityCompressionType_t1771 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeType_t1785 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeState_t1757 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t1772_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t560_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t560_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Byte_t560_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t560_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Int64_t507_Int64_t507_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Byte_t560_Byte_t560_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_RSAParameters_t1672 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1672  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t1672  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Byte_t560_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Byte_t560_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t1751 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t558_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_ObjectU5BU5DU26_t4052 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t34** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t34**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_ObjectU5BU5DU26_t4052 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t34** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t34**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_SByte_t559_Object_t_Int32_t59_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t40 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t40 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_Int32U26_t4019_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t40 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t40 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_SByte_t559_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t40 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t40 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32U26_t4019_Object_t_SByte_t559_SByte_t559_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t40 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t40 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32U26_t4019_Object_t_Object_t_BooleanU26_t4018_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32U26_t4019_Object_t_Object_t_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Int32U26_t4019_Object_t_Int32U26_t4019_SByte_t559_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t40 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t40 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32U26_t4019_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int16_t561_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_Int32U26_t4019_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t40 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t40 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeCode_t2475 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_Int64U26_t4054_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t40 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t40 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_Int64U26_t4054_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t40 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t40 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int64U26_t4054 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_Int64U26_t4054 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_UInt32U26_t4055_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t40 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t40 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_UInt32U26_t4055_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t40 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t40 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_UInt32U26_t4055 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_UInt32U26_t4055 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_UInt64U26_t4056_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t40 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t40 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_UInt64U26_t4056 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_ByteU26_t4050 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_ByteU26_t4050 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_SByteU26_t4057_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t40 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t40 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_SByteU26_t4057 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_Int16U26_t4058_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t40 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t40 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int16U26_t4058 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_UInt16U26_t4059 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_UInt16U26_t4059 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ByteU2AU26_t4060_ByteU2AU26_t4060_DoubleU2AU26_t4061_UInt16U2AU26_t4062_UInt16U2AU26_t4062_UInt16U2AU26_t4062_UInt16U2AU26_t4062 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

void* RuntimeInvoker_UnicodeCategory_t2002_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t556_Int16_t561_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int16_t561_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Char_t556_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59_Int32_t59_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int16_t561_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t561_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32U26_t4019_Int32U26_t4019_Int32U26_t4019_BooleanU26_t4018_StringU26_t4016 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t561_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_DoubleU26_t4063_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t40 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t40 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_DoubleU26_t4063 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t564_Decimal_t564_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, Decimal_t564  p1, Decimal_t564  p2, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), *((Decimal_t564 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Decimal_t564_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t564  p1, Decimal_t564  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), *((Decimal_t564 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Decimal_t564_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t564  p1, Decimal_t564  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), *((Decimal_t564 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Int32U26_t4019_BooleanU26_t4018_BooleanU26_t4018_Int32U26_t4019_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t564_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_DecimalU26_t4064_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t564 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t564 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_DecimalU26_t4064_UInt64U26_t4056 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t564 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t564 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_DecimalU26_t4064_Int64U26_t4054 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t564 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t564 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_DecimalU26_t4064_DecimalU26_t4064 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t564 * p1, Decimal_t564 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t564 *)args[0], (Decimal_t564 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_DecimalU26_t4064_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t564 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t564 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_DecimalU26_t4064_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t564 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t564 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_DecimalU26_t4064 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t564 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t564 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_DecimalU26_t4064_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t564 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t564 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_DecimalU26_t4064_DecimalU26_t4064_DecimalU26_t4064 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t564 * p1, Decimal_t564 * p2, Decimal_t564 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t564 *)args[0], (Decimal_t564 *)args[1], (Decimal_t564 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t4065 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t10 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t10 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t507_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t507_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Object_t_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_TypeAttributes_t2126 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MemberTypes_t2106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeTypeHandle_t1857 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1857  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1857  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t2475_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1857 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1857  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1857 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_RuntimeTypeHandle_t1857_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1857  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1857  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_RuntimeFieldHandle_t1859 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1859  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1859 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_ContractionU5BU5DU26_t4066_Level2MapU5BU5DU26_t4067 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1906** p3, Level2MapU5BU5D_t1907** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1906**)args[2], (Level2MapU5BU5D_t1907**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_CodePointIndexerU26_t4068_ByteU2AU26_t4060_ByteU2AU26_t4060_CodePointIndexerU26_t4068_ByteU2AU26_t4060 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1890 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1890 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1890 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1890 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t560_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExtenderType_t1903_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_BooleanU26_t4018_BooleanU26_t4018_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_BooleanU26_t4018_BooleanU26_t4018_SByte_t559_SByte_t559_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1900 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1900 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59_Int32_t59_SByte_t559_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1900 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1900 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int16_t561_Int32_t59_SByte_t559_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1900 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1900 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59_Object_t_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1900 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1900 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_Object_t_Int32_t59_SByte_t559_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1900 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1900 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Object_t_SByte_t559_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1900 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1900 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Object_t_SByte_t559_Int32_t59_ContractionU26_t4070_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1893 ** p8, Context_t1900 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1893 **)args[7], (Context_t1900 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Int32_t59_Object_t_SByte_t559_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1900 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1900 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Int32_t59_Object_t_SByte_t559_Int32_t59_ContractionU26_t4070_ContextU26_t4069 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1893 ** p9, Context_t1900 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1893 **)args[8], (Context_t1900 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_ByteU5BU5DU26_t4051_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t119** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t119**)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t1912 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t1914_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DSAParameters_t1674_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1674  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t1674  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_DSAParameters_t1674 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t1674  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t1674 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int16_t561_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t561_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Single_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_SByte_t559_MethodBaseU26_t4071_Int32U26_t4019_Int32U26_t4019_StringU26_t4016_Int32U26_t4019_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t1457 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t1457 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t220  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t2425_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t220  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32U26_t4019_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t2425_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32U26_t4019_Int32U26_t4019_Int32U26_t4019_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_DateTime_t220_DateTime_t220_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t220  p1, DateTime_t220  p2, TimeSpan_t565  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((DateTime_t220 *)args[1]), *((TimeSpan_t565 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t564  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t564  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_SByte_t559_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Object_t_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_FileAttributes_t2012_Object_t_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MonoFileType_t2021_IntPtr_t_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_MonoIOStatU26_t4073_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t2020 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t2020 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_IntPtr_t_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Int32_t59_Int32_t59_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_IntPtr_t_Int64_t507_Int32_t59_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_IntPtr_t_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_IntPtr_t_Int64_t507_MonoIOErrorU26_t4072 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_CallingConventions_t2096 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeMethodHandle_t2467 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t2467  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t2467  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t2107 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodToken_t2062 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t2062  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t2062  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FieldAttributes_t2104 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeFieldHandle_t1859 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1859  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1859  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_OpCode_t2066 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t2066  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t2066 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_OpCode_t2066_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t2066  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t2066 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_StackBehaviour_t2070 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t4019_ModuleU26_t4074 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t2059 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t2059 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_AssemblyNameFlags_t2090 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_ObjectU5BU5DU26_t4052_Object_t_Object_t_Object_t_ObjectU26_t4075 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t34** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t34**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_ObjectU5BU5DU26_t4052_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t34** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t34**)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_ObjectU5BU5DU26_t4052_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t34** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t34**)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_EventAttributes_t2102 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1859 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1859  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1859 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t1439 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1439  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1439 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t2467 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t2467  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t2467 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_MonoEventInfoU26_t4076 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t2111 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t2111 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoEventInfo_t2111_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t2111  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t2111  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_MonoMethodInfoU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t2114 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t2114 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoMethodInfo_t2114_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t2114  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t2114  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t2107_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CallingConventions_t2096_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t40 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t40 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_MonoPropertyInfoU26_t4078_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t2115 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t2115 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_PropertyAttributes_t2122 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_ParameterAttributes_t2118 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int64_t507_ResourceInfoU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, ResourceInfo_t2130 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (ResourceInfo_t2130 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_GCHandle_t1139_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1139  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1139  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Int32_t59_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Object_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t560_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_IntPtr_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t4016 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t4016 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_Object_t_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t565  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t565 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1439_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1439  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1439 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1439_ISurrogateSelectorU26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t1439  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1439 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t565_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_StringU26_t4016 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t4075 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_Object_t_StringU26_t4016_StringU26_t4016 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WellKnownObjectMode_t2263 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContext_t1439 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t1439  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t1439  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeFilterLevel_t2279 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t560_Object_t_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t560_Object_t_SByte_t559_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559_ObjectU26_t4075_HeaderU5BU5DU26_t4081 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t2483** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t2483**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Byte_t560_Object_t_SByte_t559_ObjectU26_t4075_HeaderU5BU5DU26_t4081 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t2483** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t2483**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Byte_t560_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Byte_t560_Object_t_Int64U26_t4054_ObjectU26_t4075_SerializationInfoU26_t4082 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t1438 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t1438 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559_Int64U26_t4054_ObjectU26_t4075_SerializationInfoU26_t4082 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t1438 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t1438 **)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64U26_t4054_ObjectU26_t4075_SerializationInfoU26_t4082 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t1438 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t1438 **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int64_t507_ObjectU26_t4075_SerializationInfoU26_t4082 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t1438 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t1438 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Object_t_Object_t_Int64_t507_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64U26_t4054_ObjectU26_t4075 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int64U26_t4054_ObjectU26_t4075 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int64_t507_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Int64_t507_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int64_t507_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Int32_t59_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Object_t_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Object_t_Int64_t507_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_SByte_t559_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_StreamingContext_t1439 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1439  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1439 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_StreamingContext_t1439 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1439  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1439 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_StreamingContext_t1439 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t1439  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t1439 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t1439_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1439  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1439 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t220  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t220 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SerializationEntry_t2296 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t2296  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t2296  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContextStates_t2299 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CspProviderFlags_t2303 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t558_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_UInt32U26_t4055_Int32_t59_UInt32U26_t4055_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UInt64_t563_Int64_t507_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Int64_t507_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PaddingMode_t1846 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_StringBuilderU26_t4083_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t305 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t305 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_EncoderFallbackBufferU26_t4084_CharU5BU5DU26_t4085 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t2376 ** p6, CharU5BU5D_t493** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t2376 **)args[5], (CharU5BU5D_t493**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_DecoderFallbackBufferU26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t2367 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t2367 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int16_t561_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int16_t561_Int16_t561_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int16_t561_Int16_t561_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t59_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_SByte_t559_Int32_t59_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_SByte_t559_Int32U26_t4019_BooleanU26_t4018_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_CharU26_t4048_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_CharU26_t4048_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_CharU26_t4048_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59_CharU26_t4048_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t2367 ** p7, ByteU5BU5D_t119** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t2367 **)args[6], (ByteU5BU5D_t119**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t2367 ** p6, ByteU5BU5D_t119** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t2367 **)args[5], (ByteU5BU5D_t119**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_Object_t_Int64_t507_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2367 ** p2, ByteU5BU5D_t119** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2367 **)args[1], (ByteU5BU5D_t119**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_Object_t_Int64_t507_Int32_t59_Object_t_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2367 ** p2, ByteU5BU5D_t119** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2367 **)args[1], (ByteU5BU5D_t119**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_UInt32U26_t4055_UInt32U26_t4055_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t2367 ** p9, ByteU5BU5D_t119** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t2367 **)args[8], (ByteU5BU5D_t119**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59_UInt32U26_t4055_UInt32U26_t4055_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t2367 ** p8, ByteU5BU5D_t119** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t2367 **)args[7], (ByteU5BU5D_t119**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_SByte_t559_Object_t_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_SByte_t559_SByte_t559_Object_t_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_TimeSpan_t565_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t565  p1, TimeSpan_t565  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t565 *)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int64_t507_Int64_t507_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_IntPtr_t_Int32_t59_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t507_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Byte_t560_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t560_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t556_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t556_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t556_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t556_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t60_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t561_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t559_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t36_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t558_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t563_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ByteU5BU5DU26_t4051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ByteU5BU5D_t119** p1, const MethodInfo* method);
	((Func)method->method)(obj, (ByteU5BU5D_t119**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SByte_t559_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t565  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int64_t507_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DayOfWeek_t2425 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTimeKind_t2422 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, TimeSpan_t565  p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((TimeSpan_t565 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_DateTime_t220_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t220  p1, DateTime_t220  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((DateTime_t220 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_DateTime_t220_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, DateTime_t220  p1, int32_t p2, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59_DateTimeU26_t4087_DateTimeOffsetU26_t4088_SByte_t559_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t220 * p4, DateTimeOffset_t2423 * p5, int8_t p6, Exception_t40 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t220 *)args[3], (DateTimeOffset_t2423 *)args[4], *((int8_t*)args[5]), (Exception_t40 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t40 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t40 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Object_t_SByte_t559_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Int32_t59_Object_t_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Int32_t59_Object_t_SByte_t559_Int32U26_t4019_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_SByte_t559_DateTimeU26_t4087_DateTimeOffsetU26_t4088_Object_t_Int32_t59_SByte_t559_BooleanU26_t4018_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t220 * p5, DateTimeOffset_t2423 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t220 *)args[4], (DateTimeOffset_t2423 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_Object_t_Object_t_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_Int32_t59_DateTimeU26_t4087_SByte_t559_BooleanU26_t4018_SByte_t559_ExceptionU26_t4053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t220 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t40 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t220 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t40 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_DateTime_t220_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, DateTime_t220  p1, TimeSpan_t565  p2, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_DateTime_t220_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t220  p1, DateTime_t220  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((DateTime_t220 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t565_DateTime_t220_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, DateTime_t220  p1, DateTime_t220  p2, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((DateTime_t220 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_DateTime_t220_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t220  p1, TimeSpan_t565  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int64_t507_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t565  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_DateTimeOffset_t2423 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2423  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t2423 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_DateTimeOffset_t2423 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2423  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t2423 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t561_Object_t_BooleanU26_t4018_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t561_Object_t_BooleanU26_t4018_BooleanU26_t4018_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t220_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t220  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t220_Nullable_1_t2528_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t220  p1, Nullable_1_t2528  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((Nullable_1_t2528 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_MonoEnumInfo_t2436 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t2436  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t2436 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_MonoEnumInfoU26_t4089 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t2436 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t2436 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Int16_t561_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int64_t507_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PlatformID_t2464 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int16_t561_Int16_t561_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Guid_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t61  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t61 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Guid_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t61  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t61 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Guid_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t61  (*Func)(void* obj, const MethodInfo* method);
	Guid_t61  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t559_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Double_t60_Double_t60_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeAttributes_t2126_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t559_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_UInt64U2AU26_t4090_Int32U2AU26_t4091_CharU2AU26_t4092_CharU2AU26_t4092_Int64U2AU26_t4093_Int32U2AU26_t4091 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Double_t60_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t564  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t564 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t561_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t507_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Single_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Double_t60_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Decimal_t564_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t564  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t564 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Double_t60_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_BooleanU26_t4018_SByte_t559_Int32U26_t4019_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59_Object_t_SByte_t559_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t507_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t565_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, TimeSpan_t565  p1, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, *((TimeSpan_t565 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_TimeSpan_t565_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t565  p1, TimeSpan_t565  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t565 *)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t565  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t565 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t565  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t565 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t565_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t565_Double_t60_Int64_t507 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t565_TimeSpan_t565_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, TimeSpan_t565  p1, TimeSpan_t565  p2, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, *((TimeSpan_t565 *)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t565_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, DateTime_t220  p1, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_DateTime_t220_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t220  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t220_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t220  (*Func)(void* obj, DateTime_t220  p1, const MethodInfo* method);
	DateTime_t220  ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t565_DateTime_t220_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, DateTime_t220  p1, TimeSpan_t565  p2, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, *((DateTime_t220 *)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int64U5BU5DU26_t4094_StringU5BU5DU26_t4095 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t2511** p2, StringU5BU5D_t260** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t2511**)args[1], (StringU5BU5D_t260**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_ObjectU26_t4075 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_ObjectU26_t4075_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ObjectU26_t4075_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Enumerator_t3474 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3474  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3474  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2871 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2871  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2871  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3342 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3342  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3342  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_ObjectU26_t4075 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_ObjectU5BU5DU26_t4052_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t34** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t34**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_ObjectU5BU5DU26_t4052_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t34** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t34**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t2634 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2634  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2634 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2634 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2634  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2634 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2634_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2634  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2634  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_ObjectU26_t4075 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2638 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2638  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2638  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2634 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2634  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2634  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2637 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2637  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2637  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2641 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2641  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2641  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t552 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t552  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t552  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2662 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2662  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2662  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2658 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2658  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2658  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t467 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Nullable_1_t467  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Nullable_1_t467 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_OGActionType_t265 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Nullable_1_t466  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Nullable_1_t466 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t474 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Nullable_1_t474  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Nullable_1_t474 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t507_Object_t_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2791  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2791  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2787 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2787  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2787  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2665 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2665  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2665  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ColorTween_t670 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t670  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t670 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_TypeU26_t4096_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_BooleanU26_t4018_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_FillMethodU26_t4097_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_SingleU26_t4023_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_ContentTypeU26_t4098_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_LineTypeU26_t4099_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_InputTypeU26_t4100_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_TouchScreenKeyboardTypeU26_t4101_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_CharacterValidationU26_t4102_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_CharU26_t4048_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_DirectionU26_t4103_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_NavigationU26_t4104_Navigation_t730 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t730 * p1, Navigation_t730  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t730 *)args[0], *((Navigation_t730 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_TransitionU26_t4105_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_ColorBlockU26_t4106_ColorBlock_t682 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t682 * p1, ColorBlock_t682  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t682 *)args[0], *((ColorBlock_t682 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_SpriteStateU26_t4107_SpriteState_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t745 * p1, SpriteState_t745  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t745 *)args[0], *((SpriteState_t745 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_DirectionU26_t4108_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_AspectModeU26_t4109_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_FitModeU26_t4110_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_CornerU26_t4111_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_AxisU26_t4112_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Vector2U26_t4032_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t29 * p1, Vector2_t29  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t29 *)args[0], *((Vector2_t29 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_ConstraintU26_t4113_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32U26_t4019_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_SingleU26_t4023_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_BooleanU26_t4018_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_TextAnchorU26_t4114_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Enumerator_t1150 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1150  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1150  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3068 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3068  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3068  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Double_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t6_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t6  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t6  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Vector3_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t6  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t6 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Vector2_t29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t29  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t29 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t5  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Color_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color_t5  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Rect_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Rect_t30  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Rect_t30 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2607_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2607  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2607  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t2607 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2607  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2607 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2607 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2607  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2607 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t2607 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2607  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2607 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2607 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2607  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2607 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t1956_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1956  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1956  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Link_t1956 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1956  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1956 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Link_t1956 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1956  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1956 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Link_t1956 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1956  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1956 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Link_t1956 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1956  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1956 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t58_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_DictionaryEntry_t58 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t58  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t58 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_DictionaryEntry_t58 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t58  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t58 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_DictionaryEntry_t58 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t58  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t58 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_DictionaryEntry_t58 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t58  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t58 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2634_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2634  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2634  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t2634 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2634  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2634 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2634 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2634  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2634 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2658_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2658  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2658  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t2658 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2658  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2658 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2658 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2658  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2658 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t2658 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2658  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2658 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2658 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2658  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2658 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_ByteU5BU5DU26_t4051_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ByteU5BU5D_t119** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ByteU5BU5D_t119**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_ByteU5BU5DU26_t4051_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ByteU5BU5D_t119** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ByteU5BU5D_t119**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_SByte_t559_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_SByte_t559_SByte_t559_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int64U26_t4054 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2787_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2787  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2787  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t2787 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2787  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2787 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2787 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2787  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2787 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t2787 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2787  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2787 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2787 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2787  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2787 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2811_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2811  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2811  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t2811 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2811  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2811 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2811 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2811  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2811 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t2811 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2811  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2811 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2811 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2811  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2811 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_WebCamDevice_t1219_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef WebCamDevice_t1219  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WebCamDevice_t1219  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_WebCamDevice_t1219 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WebCamDevice_t1219  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WebCamDevice_t1219 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_WebCamDevice_t1219 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WebCamDevice_t1219  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WebCamDevice_t1219 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_WebCamDevice_t1219 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WebCamDevice_t1219  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WebCamDevice_t1219 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_WebCamDevice_t1219 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WebCamDevice_t1219  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WebCamDevice_t1219 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastResult_t647_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t647  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t647  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_RaycastResult_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t647  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t647 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_RaycastResult_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t647  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t647 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_RaycastResult_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t647  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t647 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_RaycastResultU5BU5DU26_t4115_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2877** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2877**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_RaycastResultU5BU5DU26_t4115_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2877** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2877**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_RaycastResult_t647_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t647  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t647 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_RaycastResult_t647_RaycastResult_t647_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t647  p1, RaycastResult_t647  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t647 *)args[0]), *((RaycastResult_t647 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t838_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t838  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t838  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_RaycastHit2D_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t838  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t838 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_RaycastHit2D_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t838  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t838 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_RaycastHit2D_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t838  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t838 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_RaycastHit2D_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t838  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t838 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit_t566_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t566  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t566  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_RaycastHit_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t566  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t566 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_RaycastHit_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t566  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t566 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_RaycastHit_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t566  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t566 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_RaycastHit_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t566  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t566 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_UIVertexU5BU5DU26_t4116_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t722** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t722**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_UIVertexU5BU5DU26_t4116_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t722** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t722**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_UIVertex_t727_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t727  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t727 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_UIVertex_t727_UIVertex_t727_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t727  p1, UIVertex_t727  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t727 *)args[0]), *((UIVertex_t727 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContentType_t710_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UILineInfo_t857_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t857  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t857  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_UILineInfo_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t857  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t857 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_UILineInfo_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t857  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t857 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_UILineInfo_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t857  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t857 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_UILineInfo_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t857  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t857 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UICharInfo_t859_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t859  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t859  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_UICharInfo_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t859  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t859 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_UICharInfo_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t859  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t859 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_UICharInfo_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t859  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t859 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_UICharInfo_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t859  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t859 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_EyewearCalibrationReading_t913_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef EyewearCalibrationReading_t913  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	EyewearCalibrationReading_t913  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_EyewearCalibrationReading_t913 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, EyewearCalibrationReading_t913  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((EyewearCalibrationReading_t913 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_EyewearCalibrationReading_t913 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, EyewearCalibrationReading_t913  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((EyewearCalibrationReading_t913 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_EyewearCalibrationReading_t913 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, EyewearCalibrationReading_t913  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((EyewearCalibrationReading_t913 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_EyewearCalibrationReading_t913 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, EyewearCalibrationReading_t913  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((EyewearCalibrationReading_t913 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TargetSearchResult_t1050_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t1050  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TargetSearchResult_t1050  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_TargetSearchResult_t1050 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TargetSearchResult_t1050  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_TargetSearchResult_t1050 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t1050  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_TargetSearchResult_t1050 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TargetSearchResult_t1050  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TargetSearchResult_t1050 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t3060_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3060  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3060  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t3060 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3060  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3060 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3060 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3060  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3060 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t3060 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3060  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3060 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3060 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3060  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3060 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_PIXEL_FORMAT_t942_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_PIXEL_FORMATU5BU5DU26_t4117_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PIXEL_FORMATU5BU5D_t3054** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (PIXEL_FORMATU5BU5D_t3054**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_PIXEL_FORMATU5BU5DU26_t4117_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PIXEL_FORMATU5BU5D_t3054** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (PIXEL_FORMATU5BU5D_t3054**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t829_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t829  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color32_t829  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Color32_t829 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32_t829  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color32_t829 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Color32_t829 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t829  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t829 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Color32_t829 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t829  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t829 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Color32_t829 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color32_t829  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color32_t829 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TrackableResultData_t969_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t969  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TrackableResultData_t969  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TrackableResultData_t969  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TrackableResultData_t969 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TrackableResultData_t969  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TrackableResultData_t969 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TrackableResultData_t969  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t969 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_WordData_t974_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordData_t974  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WordData_t974  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_WordData_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WordData_t974  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WordData_t974 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_WordData_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WordData_t974  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WordData_t974 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_WordData_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WordData_t974  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WordData_t974 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_WordData_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WordData_t974  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WordData_t974 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_WordResultData_t973_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordResultData_t973  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WordResultData_t973  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_WordResultData_t973 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WordResultData_t973  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WordResultData_t973 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_WordResultData_t973 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WordResultData_t973  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WordResultData_t973 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_WordResultData_t973 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WordResultData_t973  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WordResultData_t973 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_WordResultData_t973 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WordResultData_t973  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WordResultData_t973 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32U5BU5DU26_t4118_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t335** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t335**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32U5BU5DU26_t4118_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t335** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t335**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_SmartTerrainRevisionData_t977_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef SmartTerrainRevisionData_t977  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	SmartTerrainRevisionData_t977  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_SmartTerrainRevisionData_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SmartTerrainRevisionData_t977  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SmartTerrainRevisionData_t977 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_SmartTerrainRevisionData_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SmartTerrainRevisionData_t977  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SmartTerrainRevisionData_t977 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_SmartTerrainRevisionData_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, SmartTerrainRevisionData_t977  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((SmartTerrainRevisionData_t977 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_SmartTerrainRevisionData_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, SmartTerrainRevisionData_t977  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((SmartTerrainRevisionData_t977 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SurfaceData_t978_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef SurfaceData_t978  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	SurfaceData_t978  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_SurfaceData_t978 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SurfaceData_t978  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SurfaceData_t978 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_SurfaceData_t978 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SurfaceData_t978  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SurfaceData_t978 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_SurfaceData_t978 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, SurfaceData_t978  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((SurfaceData_t978 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_SurfaceData_t978 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, SurfaceData_t978  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((SurfaceData_t978 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_PropData_t979_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef PropData_t979  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	PropData_t979  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_PropData_t979 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PropData_t979  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((PropData_t979 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_PropData_t979 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, PropData_t979  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((PropData_t979 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_PropData_t979 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, PropData_t979  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((PropData_t979 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_PropData_t979 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, PropData_t979  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((PropData_t979 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t3151_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3151  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3151  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t3151 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3151  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3151 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3151 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3151  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3151 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t3151 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3151  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3151 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3151 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3151  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3151 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RectangleData_t923_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef RectangleData_t923  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RectangleData_t923  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_RectangleData_t923 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RectangleData_t923  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RectangleData_t923 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_RectangleData_t923 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RectangleData_t923  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RectangleData_t923 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_RectangleData_t923 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RectangleData_t923  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RectangleData_t923 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t3240_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3240  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3240  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t3240 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3240  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3240 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3240 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3240  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t3240 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3240  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3240 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3240  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3240 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t3255_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3255  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3255  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3255  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3255 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3255  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3255 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3255  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3255 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3255  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3255 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_VirtualButtonData_t970_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t970  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	VirtualButtonData_t970  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, VirtualButtonData_t970  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((VirtualButtonData_t970 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, VirtualButtonData_t970  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((VirtualButtonData_t970 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, VirtualButtonData_t970  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((VirtualButtonData_t970 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, VirtualButtonData_t970  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t970 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_TargetSearchResultU5BU5DU26_t4119_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResultU5BU5D_t3273** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (TargetSearchResultU5BU5D_t3273**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_TargetSearchResultU5BU5DU26_t4119_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResultU5BU5D_t3273** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (TargetSearchResultU5BU5D_t3273**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_TargetSearchResult_t1050_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, TargetSearchResult_t1050  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TargetSearchResult_t1050 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_TargetSearchResult_t1050_TargetSearchResult_t1050_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t1050  p1, TargetSearchResult_t1050  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), *((TargetSearchResult_t1050 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3292_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3292  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3292  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t3292 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3292  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3292 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3292 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3292  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3292 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t3292 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3292  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3292 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3292 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3292  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3292 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ProfileData_t1064_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t1064  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ProfileData_t1064  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ProfileData_t1064  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ProfileData_t1064 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ProfileData_t1064  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ProfileData_t1064 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ProfileData_t1064  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ProfileData_t1064 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ProfileData_t1064  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ProfileData_t1064 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t3340_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t3340  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t3340  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Link_t3340 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t3340  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t3340 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Link_t3340 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t3340  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t3340 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Link_t3340 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t3340  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t3340 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Link_t3340 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t3340  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t3340 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ParameterModifier_t2119_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t2119  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t2119  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ParameterModifier_t2119 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t2119  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t2119 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_ParameterModifier_t2119 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t2119  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t2119 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_ParameterModifier_t2119 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t2119  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t2119 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_ParameterModifier_t2119 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t2119  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t2119 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcAchievementData_t1244_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t1244  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t1244  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_GcAchievementData_t1244 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t1244  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t1244 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_GcAchievementData_t1244 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t1244  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t1244 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_GcAchievementData_t1244 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t1244  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t1244 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_GcAchievementData_t1244 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t1244  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t1244 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcScoreData_t1245_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t1245  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t1245  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_GcScoreData_t1245 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t1245  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t1245 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_GcScoreData_t1245 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t1245  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t1245 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_GcScoreData_t1245 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t1245  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t1245 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_HitInfo_t1271_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t1271  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t1271  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_HitInfo_t1271 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t1271  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t1271 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_HitInfo_t1271 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t1271  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t1271 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3373_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3373  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3373  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t3373 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3373  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3373 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3373 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3373  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3373 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t3373 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3373  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3373 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3373 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3373  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3373 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TextEditOp_t1286_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_UICharInfoU5BU5DU26_t4120_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1440** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1440**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_UICharInfoU5BU5DU26_t4120_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1440** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1440**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_UICharInfo_t859_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t859  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t859 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_UICharInfo_t859_UICharInfo_t859_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t859  p1, UICharInfo_t859  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t859 *)args[0]), *((UICharInfo_t859 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_UILineInfoU5BU5DU26_t4121_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1441** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1441**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_UILineInfoU5BU5DU26_t4121_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1441** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1441**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_UILineInfo_t857_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t857  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t857 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_UILineInfo_t857_UILineInfo_t857_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t857  p1, UILineInfo_t857  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t857 *)args[0]), *((UILineInfo_t857 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Keyframe_t1321_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t1321  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t1321  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Keyframe_t1321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t1321  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t1321 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Keyframe_t1321 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t1321  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t1321 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Keyframe_t1321 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t1321  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t1321 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Keyframe_t1321 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t1321  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t1321 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t3478_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3478  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3478  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3478  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3478 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3478  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3478 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3478  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3478 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3478  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3478 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_X509ChainStatus_t1558_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1558  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t1558  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_X509ChainStatus_t1558 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t1558  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t1558 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_X509ChainStatus_t1558 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t1558  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t1558 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_X509ChainStatus_t1558 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t1558  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t1558 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_X509ChainStatus_t1558 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t1558  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t1558 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t1605_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1605  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t1605  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Mark_t1605 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t1605  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t1605 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Mark_t1605 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t1605  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t1605 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Mark_t1605 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t1605  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t1605 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Mark_t1605 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t1605  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t1605 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UriScheme_t1641_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1641  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t1641  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_UriScheme_t1641 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t1641  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t1641 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_UriScheme_t1641 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t1641  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t1641 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_UriScheme_t1641 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t1641  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t1641 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_UriScheme_t1641 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t1641  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t1641 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ClientCertificateType_t1784_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t1889_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1889  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1889  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_TableRange_t1889 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1889  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1889 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_TableRange_t1889 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1889  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1889 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_TableRange_t1889 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1889  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1889 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_TableRange_t1889 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1889  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1889 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1966_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1966  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1966  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Slot_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1966  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1966 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Slot_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1966  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1966 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Slot_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1966  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1966 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Slot_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1966  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1966 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1974_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1974  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1974  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Slot_t1974 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1974  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1974 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_Slot_t1974 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1974  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1974 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Slot_t1974 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1974  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1974 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Slot_t1974 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1974  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1974 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ILTokenInfo_t2053_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t2053  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t2053  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ILTokenInfo_t2053 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t2053  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t2053 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_ILTokenInfo_t2053 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t2053  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t2053 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_ILTokenInfo_t2053 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t2053  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t2053 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_ILTokenInfo_t2053 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t2053  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t2053 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelData_t2055_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t2055  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t2055  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_LabelData_t2055 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t2055  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t2055 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_LabelData_t2055 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t2055  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t2055 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_LabelData_t2055 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t2055  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t2055 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_LabelData_t2055 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t2055  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t2055 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelFixup_t2054_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t2054  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t2054  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_LabelFixup_t2054 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t2054  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t2054 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_LabelFixup_t2054 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t2054  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t2054 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_LabelFixup_t2054 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t2054  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t2054 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_LabelFixup_t2054 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t2054  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t2054 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t2101_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t2101  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t2101  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_CustomAttributeTypedArgument_t2101 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgument_t2101  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeTypedArgument_t2101 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_CustomAttributeTypedArgument_t2101 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t2101  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t2101 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_CustomAttributeTypedArgument_t2101 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t2101  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t2101 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_CustomAttributeTypedArgument_t2101 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeTypedArgument_t2101  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeTypedArgument_t2101 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t2100_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t2100  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t2100  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_CustomAttributeNamedArgument_t2100 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgument_t2100  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeNamedArgument_t2100 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_CustomAttributeNamedArgument_t2100 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t2100  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t2100 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_CustomAttributeNamedArgument_t2100 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t2100  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t2100 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_CustomAttributeNamedArgument_t2100 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeNamedArgument_t2100  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeNamedArgument_t2100 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_CustomAttributeTypedArgumentU5BU5DU26_t4122_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t2537** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t2537**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_CustomAttributeTypedArgumentU5BU5DU26_t4122_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t2537** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t2537**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_CustomAttributeTypedArgument_t2101_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t2101  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t2101 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_CustomAttributeTypedArgument_t2101_CustomAttributeTypedArgument_t2101_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t2101  p1, CustomAttributeTypedArgument_t2101  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t2101 *)args[0]), *((CustomAttributeTypedArgument_t2101 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_CustomAttributeTypedArgument_t2101 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t2101  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t2101 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_CustomAttributeNamedArgumentU5BU5DU26_t4123_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t2538** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t2538**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_CustomAttributeNamedArgumentU5BU5DU26_t4123_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t2538** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t2538**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t59_Object_t_CustomAttributeNamedArgument_t2100_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t2100  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t2100 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_CustomAttributeNamedArgument_t2100_CustomAttributeNamedArgument_t2100_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t2100  p1, CustomAttributeNamedArgument_t2100  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t2100 *)args[0]), *((CustomAttributeNamedArgument_t2100 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Object_t_CustomAttributeNamedArgument_t2100 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t2100  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t2100 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceInfo_t2130_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t2130  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceInfo_t2130  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ResourceInfo_t2130 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceInfo_t2130  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceInfo_t2130 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_ResourceInfo_t2130 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceInfo_t2130  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceInfo_t2130 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_ResourceInfo_t2130 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceInfo_t2130  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceInfo_t2130 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_ResourceInfo_t2130 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceInfo_t2130  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceInfo_t2130 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ResourceCacheItem_t2131_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t2131  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceCacheItem_t2131  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_ResourceCacheItem_t2131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceCacheItem_t2131  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceCacheItem_t2131 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_ResourceCacheItem_t2131 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceCacheItem_t2131  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceCacheItem_t2131 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_ResourceCacheItem_t2131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceCacheItem_t2131  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceCacheItem_t2131 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_ResourceCacheItem_t2131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceCacheItem_t2131  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceCacheItem_t2131 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_DateTime_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t220  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t220 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t564  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t564 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Decimal_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t564  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t564 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t565_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t565  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t565  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_TimeSpan_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t565  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t565 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TypeTag_t2267_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_Byte_t560 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2607_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2607  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2607  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2613 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2613  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2613  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2607 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2607  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2607  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t1956 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1956  (*Func)(void* obj, const MethodInfo* method);
	Link_t1956  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2612 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2612  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2612  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2616 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2616  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2616  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2607_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2607  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2607  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Nullable_1_t48 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t48  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2634_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2634  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2634  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2658_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2658  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2658  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_ObjectU26_t4075 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2661 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2661  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2661  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2658_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2658  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2658  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2709 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2709  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2709  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t559_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_Nullable_1_t466 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t466  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t466 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Nullable_1_t467 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t467  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t467 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Nullable_1_t474 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t474  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t474 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Nullable_1_t555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t555  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t555 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2787_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2787  (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	KeyValuePair_2_t2787  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t41_Object_t_SingleU26_t4023 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Object_t_Single_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2790 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2790  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2790  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2794  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2794  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2787_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2787  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2787  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2811_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2811  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2811  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_Int32U26_t4019 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2815  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2815  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Int32_t59_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2811 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2811  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2811  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2814 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2814  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2814  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2818 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2818  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2818  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2811_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2811  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2811  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WebCamDevice_t1219 (const MethodInfo* method, void* obj, void** args)
{
	typedef WebCamDevice_t1219  (*Func)(void* obj, const MethodInfo* method);
	WebCamDevice_t1219  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t559_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RaycastResult_t647_RaycastResult_t647_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t647  p1, RaycastResult_t647  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t647 *)args[0]), *((RaycastResult_t647 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2879 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2879  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2879  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_RaycastResult_t647_RaycastResult_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t647  p1, RaycastResult_t647  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t647 *)args[0]), *((RaycastResult_t647 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastResult_t647_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t647  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t647 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_RaycastHit2D_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t838  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t838  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastHit_t566_RaycastHit_t566_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t566  p1, RaycastHit_t566  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t566 *)args[0]), *((RaycastHit_t566 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_RaycastHit_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t566  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t566  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color_t5_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t5  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t5 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_UIVertex_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t727  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t727 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t41_UIVertex_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t727  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t727 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t727_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t727  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t727  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2941 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2941  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2941  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_UIVertex_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t727  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t727 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1856_Int32_t59_UIVertex_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t727  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t727 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UIVertex_t727_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t727  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t727  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t727  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t727  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_UIVertex_t727_UIVertex_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t727  p1, UIVertex_t727  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t727 *)args[0]), *((UIVertex_t727 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t727_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t727  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t727 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_UIVertex_t727_UIVertex_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t727  p1, UIVertex_t727  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t727 *)args[0]), *((UIVertex_t727 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t727_UIVertex_t727_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t727  p1, UIVertex_t727  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t727 *)args[0]), *((UIVertex_t727 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_ColorTween_t670 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t670  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t670 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t857  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t857  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t859  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t859  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Single_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector2_t29_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t29  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t29 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_EyewearCalibrationReading_t913 (const MethodInfo* method, void* obj, void** args)
{
	typedef EyewearCalibrationReading_t913  (*Func)(void* obj, const MethodInfo* method);
	EyewearCalibrationReading_t913  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TargetSearchResult_t1050 (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t1050  (*Func)(void* obj, const MethodInfo* method);
	TargetSearchResult_t1050  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3060_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3060  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t3060  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PIXEL_FORMAT_t942_Int32_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PIXEL_FORMAT_t942_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3065 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3065  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3065  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3060 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3060  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3060  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3064 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3064  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3064  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3060_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3060  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3060  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3077  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3077  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color32_t829 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t829  (*Func)(void* obj, const MethodInfo* method);
	Color32_t829  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t969  (*Func)(void* obj, const MethodInfo* method);
	TrackableResultData_t969  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordData_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordData_t974  (*Func)(void* obj, const MethodInfo* method);
	WordData_t974  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordResultData_t973 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordResultData_t973  (*Func)(void* obj, const MethodInfo* method);
	WordResultData_t973  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3123 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3123  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3123  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_TrackableResultData_t969_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TrackableResultData_t969  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TrackableResultData_t969 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_SmartTerrainRevisionData_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef SmartTerrainRevisionData_t977  (*Func)(void* obj, const MethodInfo* method);
	SmartTerrainRevisionData_t977  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SurfaceData_t978 (const MethodInfo* method, void* obj, void** args)
{
	typedef SurfaceData_t978  (*Func)(void* obj, const MethodInfo* method);
	SurfaceData_t978  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PropData_t979 (const MethodInfo* method, void* obj, void** args)
{
	typedef PropData_t979  (*Func)(void* obj, const MethodInfo* method);
	PropData_t979  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3151_Object_t_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3151  (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	KeyValuePair_2_t3151  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t562_Object_t_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3156 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3156  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3156  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Object_t_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3151 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3151  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3151  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3155 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3155  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3155  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t561_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t3159 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3159  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3159  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3151_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3151  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3151  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int16_t561_Int16_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t916_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, SmartTerrainInitializationInfo_t916  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((SmartTerrainInitializationInfo_t916 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t3240_Int32_t59_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3240  (*Func)(void* obj, int32_t p1, TrackableResultData_t969  p2, const MethodInfo* method);
	KeyValuePair_2_t3240  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, TrackableResultData_t969  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TrackableResultData_t969_Int32_t59_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t969  (*Func)(void* obj, int32_t p1, TrackableResultData_t969  p2, const MethodInfo* method);
	TrackableResultData_t969  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_TrackableResultDataU26_t4124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, TrackableResultData_t969 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (TrackableResultData_t969 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TrackableResultData_t969_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t969  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TrackableResultData_t969  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3244 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3244  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3244  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Int32_t59_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, int32_t p1, TrackableResultData_t969  p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3240 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3240  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3240  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3243 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3243  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3243  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59_TrackableResultData_t969_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, TrackableResultData_t969  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t969 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t3247 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3247  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3247  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3240_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3240  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3240  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_TrackableResultData_t969_TrackableResultData_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TrackableResultData_t969  p1, TrackableResultData_t969  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TrackableResultData_t969 *)args[0]), *((TrackableResultData_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3255_Int32_t59_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3255  (*Func)(void* obj, int32_t p1, VirtualButtonData_t970  p2, const MethodInfo* method);
	KeyValuePair_2_t3255  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t970 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Int32_t59_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, VirtualButtonData_t970  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t970 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VirtualButtonData_t970_Int32_t59_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t970  (*Func)(void* obj, int32_t p1, VirtualButtonData_t970  p2, const MethodInfo* method);
	VirtualButtonData_t970  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t970 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Int32_t59_VirtualButtonDataU26_t4125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, VirtualButtonData_t970 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (VirtualButtonData_t970 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VirtualButtonData_t970_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t970  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	VirtualButtonData_t970  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3260 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3260  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3260  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Int32_t59_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, int32_t p1, VirtualButtonData_t970  p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t970 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3255  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3255  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t970  (*Func)(void* obj, const MethodInfo* method);
	VirtualButtonData_t970  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3259 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3259  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3259  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t59_VirtualButtonData_t970_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, VirtualButtonData_t970  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t970 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t3263 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3263  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3263  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3255_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3255  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3255  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_VirtualButtonData_t970_VirtualButtonData_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, VirtualButtonData_t970  p1, VirtualButtonData_t970  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((VirtualButtonData_t970 *)args[0]), *((VirtualButtonData_t970 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TargetSearchResult_t1050_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t1050  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TargetSearchResult_t1050  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3274  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3274  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_TargetSearchResult_t1050_TargetSearchResult_t1050 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TargetSearchResult_t1050  p1, TargetSearchResult_t1050  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), *((TargetSearchResult_t1050 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_TargetSearchResult_t1050_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TargetSearchResult_t1050  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_TargetSearchResult_t1050_TargetSearchResult_t1050 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t1050  p1, TargetSearchResult_t1050  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), *((TargetSearchResult_t1050 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_TargetSearchResult_t1050_TargetSearchResult_t1050_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TargetSearchResult_t1050  p1, TargetSearchResult_t1050  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TargetSearchResult_t1050 *)args[0]), *((TargetSearchResult_t1050 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1856_Object_t_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ProfileData_t1064  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1064 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t3292_Object_t_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3292  (*Func)(void* obj, Object_t * p1, ProfileData_t1064  p2, const MethodInfo* method);
	KeyValuePair_2_t3292  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1064 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, ProfileData_t1064  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1064 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_ProfileData_t1064_Object_t_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t1064  (*Func)(void* obj, Object_t * p1, ProfileData_t1064  p2, const MethodInfo* method);
	ProfileData_t1064  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1064 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_ProfileDataU26_t4126 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, ProfileData_t1064 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (ProfileData_t1064 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3297 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3297  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3297  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Object_t_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, Object_t * p1, ProfileData_t1064  p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1064 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3292 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3292  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3292  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3296 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3296  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3296  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_ProfileData_t1064_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, ProfileData_t1064  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1064 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t3300 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3300  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3300  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3292_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3292  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3292  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_ProfileData_t1064_ProfileData_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ProfileData_t1064  p1, ProfileData_t1064  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ProfileData_t1064 *)args[0]), *((ProfileData_t1064 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t3340 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t3340  (*Func)(void* obj, const MethodInfo* method);
	Link_t3340  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ParameterModifier_t2119 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t2119  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t2119  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcAchievementData_t1244 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t1244  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t1244  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcScoreData_t1245 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t1245  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t1245  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HitInfo_t1271 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t1271  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t1271  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t1286_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3373_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3373  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t3373  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t1286_Object_t_Int32_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_TextEditOpU26_t4127 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3378 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3378  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3378  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3373 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3373  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3373  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t1286 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3377 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3377  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3377  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3381 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3381  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3381  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3373_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3373  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3373  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t859_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t859  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t859  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3391 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3391  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3391  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_UICharInfo_t859_UICharInfo_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t859  p1, UICharInfo_t859  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t859 *)args[0]), *((UICharInfo_t859 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t859_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t859  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t859 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_UICharInfo_t859_UICharInfo_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t859  p1, UICharInfo_t859  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t859 *)args[0]), *((UICharInfo_t859 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t859_UICharInfo_t859_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t859  p1, UICharInfo_t859  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t859 *)args[0]), *((UICharInfo_t859 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t857_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t857  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t857  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3400 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3400  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3400  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_UILineInfo_t857_UILineInfo_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t857  p1, UILineInfo_t857  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t857 *)args[0]), *((UILineInfo_t857 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t857_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t857  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t857 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_UILineInfo_t857_UILineInfo_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t857  p1, UILineInfo_t857  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t857 *)args[0]), *((UILineInfo_t857 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t857_UILineInfo_t857_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t857  p1, UILineInfo_t857  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t857 *)args[0]), *((UILineInfo_t857 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Keyframe_t1321 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t1321  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t1321  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3478_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3478  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t3478  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Object_t_BooleanU26_t4018 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3483 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3483  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3483  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t58_Object_t_SByte_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t58  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t58  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3478  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3478  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3482 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3482  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3482  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t559_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t3486 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3486  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3486  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3478  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3478  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t1558 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1558  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t1558  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t1605 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1605  (*Func)(void* obj, const MethodInfo* method);
	Mark_t1605  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriScheme_t1641 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1641  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t1641  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t1784 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t1889 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1889  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1889  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1966  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1966  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1974 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1974  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1974  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t2053 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t2053  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t2053  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelData_t2055 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t2055  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t2055  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelFixup_t2054 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t2054  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t2054  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t2101 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t2101  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeTypedArgument_t2101  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t2100 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t2100  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeNamedArgument_t2100  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t2101_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t2101  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t2101  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3547 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3547  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3547  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_CustomAttributeTypedArgument_t2101_CustomAttributeTypedArgument_t2101 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t2101  p1, CustomAttributeTypedArgument_t2101  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t2101 *)args[0]), *((CustomAttributeTypedArgument_t2101 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t2101_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t2101  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t2101 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_CustomAttributeTypedArgument_t2101_CustomAttributeTypedArgument_t2101 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t2101  p1, CustomAttributeTypedArgument_t2101  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t2101 *)args[0]), *((CustomAttributeTypedArgument_t2101 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t2101_CustomAttributeTypedArgument_t2101_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t2101  p1, CustomAttributeTypedArgument_t2101  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t2101 *)args[0]), *((CustomAttributeTypedArgument_t2101 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t2100_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t2100  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t2100  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3558  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3558  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_CustomAttributeNamedArgument_t2100_CustomAttributeNamedArgument_t2100 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t2100  p1, CustomAttributeNamedArgument_t2100  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t2100 *)args[0]), *((CustomAttributeNamedArgument_t2100 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t2100_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t2100  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t2100 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t59_CustomAttributeNamedArgument_t2100_CustomAttributeNamedArgument_t2100 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t2100  p1, CustomAttributeNamedArgument_t2100  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t2100 *)args[0]), *((CustomAttributeNamedArgument_t2100 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t2100_CustomAttributeNamedArgument_t2100_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t2100  p1, CustomAttributeNamedArgument_t2100  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t2100 *)args[0]), *((CustomAttributeNamedArgument_t2100 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_ResourceInfo_t2130 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t2130  (*Func)(void* obj, const MethodInfo* method);
	ResourceInfo_t2130  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceCacheItem_t2131 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t2131  (*Func)(void* obj, const MethodInfo* method);
	ResourceCacheItem_t2131  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeTag_t2267 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_DateTimeOffset_t2423_DateTimeOffset_t2423 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2423  p1, DateTimeOffset_t2423  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t2423 *)args[0]), *((DateTimeOffset_t2423 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_DateTimeOffset_t2423_DateTimeOffset_t2423 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2423  p1, DateTimeOffset_t2423  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t2423 *)args[0]), *((DateTimeOffset_t2423 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Nullable_1_t2528 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t2528  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t2528 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t59_Guid_t61_Guid_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t61  p1, Guid_t61  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t61 *)args[0]), *((Guid_t61 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t41_Guid_t61_Guid_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t61  p1, Guid_t61  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t61 *)args[0]), *((Guid_t61 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1922] = 
{
	RuntimeInvoker_Void_t1856_Object_t,
	RuntimeInvoker_Void_t1856,
	RuntimeInvoker_Void_t1856_Single_t36_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Single_t36_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_Color_t5_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_Vector3_t6_Single_t36,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Rect_t30_Rect_t30_Rect_t30_Single_t36,
	RuntimeInvoker_Vector3_t6_Vector3_t6_Vector3_t6_Single_t36,
	RuntimeInvoker_Vector2_t29_Vector2_t29_Vector2_t29_Single_t36,
	RuntimeInvoker_Single_t36_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Single_t36_Object_t,
	RuntimeInvoker_Object_t_Color_t5,
	RuntimeInvoker_Vector3_t6_Object_t_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_Color_t5,
	RuntimeInvoker_Void_t1856_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Color_t5_Object_t,
	RuntimeInvoker_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Vector3_t6_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Single_t36_Single_t36_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Single_t36_Object_t_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int64_t507_Object_t,
	RuntimeInvoker_Void_t1856_Double_t60_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Single_t36_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int64_t507,
	RuntimeInvoker_Single_t36,
	RuntimeInvoker_Void_t1856_Single_t36,
	RuntimeInvoker_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Double_t60,
	RuntimeInvoker_GCCollectionType_t87,
	RuntimeInvoker_GCBoardTimeSpan_t86,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Void_t1856_SByte_t559,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_SByte_t559_Object_t_Int32_t59,
	RuntimeInvoker_Int32_t59_Int32_t59_Object_t_SByte_t559_Int32_t59,
	RuntimeInvoker_Void_t1856_Single_t36_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_DateTime_t220,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59,
	RuntimeInvoker_TextAnchor_t869,
	RuntimeInvoker_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Vector3_t6,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_DateTime_t220_Object_t,
	RuntimeInvoker_DateTime_t220,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t_Object_t_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t466_Object_t_Object_t_Object_t_Object_t_Nullable_1_t467_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Nullable_1_t467_Nullable_1_t467_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t474_Object_t,
	RuntimeInvoker_Void_t1856_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Object_t_Object_t_Nullable_1_t467_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Nullable_1_t467_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_ShareDialogMode_t264,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t_Object_t_Int32_t59_Object_t_Object_t_Int32_t59_SByte_t559_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Double_t60_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Double_t60_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_StringU26_t4016,
	RuntimeInvoker_DateTime_t220_Object_t,
	RuntimeInvoker_DateTime_t220_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_Object_t,
	RuntimeInvoker_Char_t556,
	RuntimeInvoker_TOKEN_t301,
	RuntimeInvoker_Int64_t507_DateTime_t220,
	RuntimeInvoker_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Object_t_Object_t_Vector3_t6_Single_t36,
	RuntimeInvoker_Void_t1856_SByte_t559_Object_t,
	RuntimeInvoker_InitError_t1071_Object_t,
	RuntimeInvoker_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Object_t_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_RaycastResult_t647_RaycastResult_t647,
	RuntimeInvoker_Boolean_t41_Int32_t59,
	RuntimeInvoker_Vector2_t29,
	RuntimeInvoker_Void_t1856_Vector2_t29,
	RuntimeInvoker_MoveDirection_t644,
	RuntimeInvoker_RaycastResult_t647,
	RuntimeInvoker_Void_t1856_RaycastResult_t647,
	RuntimeInvoker_Vector3_t6,
	RuntimeInvoker_InputButton_t650,
	RuntimeInvoker_RaycastResult_t647_Object_t,
	RuntimeInvoker_MoveDirection_t644_Single_t36_Single_t36,
	RuntimeInvoker_MoveDirection_t644_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Object_t_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Boolean_t41_Int32_t59_PointerEventDataU26_t4017_SByte_t559,
	RuntimeInvoker_Object_t_Touch_t818_BooleanU26_t4018_BooleanU26_t4018,
	RuntimeInvoker_FramePressState_t651_Int32_t59,
	RuntimeInvoker_Boolean_t41_Vector2_t29_Vector2_t29_Single_t36_SByte_t559,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_InputMode_t660,
	RuntimeInvoker_Boolean_t41_Single_t36,
	RuntimeInvoker_Boolean_t41_SByte_t559_SByte_t559_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559,
	RuntimeInvoker_LayerMask_t665,
	RuntimeInvoker_Void_t1856_LayerMask_t665,
	RuntimeInvoker_Int32_t59_RaycastHit_t566_RaycastHit_t566,
	RuntimeInvoker_Color_t5,
	RuntimeInvoker_Void_t1856_Color_t5,
	RuntimeInvoker_ColorTweenMode_t667,
	RuntimeInvoker_ColorBlock_t682,
	RuntimeInvoker_FontData_t683,
	RuntimeInvoker_FontStyle_t1398,
	RuntimeInvoker_HorizontalWrapMode_t1371,
	RuntimeInvoker_VerticalWrapMode_t1372,
	RuntimeInvoker_Boolean_t41_Vector2_t29_Object_t,
	RuntimeInvoker_Vector2_t29_Vector2_t29,
	RuntimeInvoker_Rect_t30,
	RuntimeInvoker_Void_t1856_Color_t5_Single_t36_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_Color_t5_Single_t36_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Color_t5_Single_t36,
	RuntimeInvoker_Void_t1856_Single_t36_Single_t36_SByte_t559,
	RuntimeInvoker_BlockingObjects_t695,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Vector2_t29_Object_t,
	RuntimeInvoker_Type_t701,
	RuntimeInvoker_FillMethod_t702,
	RuntimeInvoker_Vector4_t822_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_UIVertex_t727_Vector2_t29_Vector2_t29_Vector2_t29_Vector2_t29,
	RuntimeInvoker_Vector4_t822_Vector4_t822_Rect_t30,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Single_t36_SByte_t559_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Single_t36_Single_t36_SByte_t559_Int32_t59,
	RuntimeInvoker_Vector2_t29_Vector2_t29_Rect_t30,
	RuntimeInvoker_ContentType_t710,
	RuntimeInvoker_LineType_t713,
	RuntimeInvoker_InputType_t711,
	RuntimeInvoker_TouchScreenKeyboardType_t854,
	RuntimeInvoker_CharacterValidation_t712,
	RuntimeInvoker_Void_t1856_Int16_t561,
	RuntimeInvoker_Void_t1856_Int32U26_t4019,
	RuntimeInvoker_Int32_t59_Vector2_t29_Object_t,
	RuntimeInvoker_Int32_t59_Vector2_t29,
	RuntimeInvoker_EditState_t717_Object_t,
	RuntimeInvoker_Boolean_t41_Int16_t561,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559,
	RuntimeInvoker_Int32_t59_Int32_t59_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32U26_t4019_Int32U26_t4019,
	RuntimeInvoker_Void_t1856_Object_t_Vector2_t29,
	RuntimeInvoker_Single_t36_Int32_t59_Object_t,
	RuntimeInvoker_Char_t556_Object_t_Int32_t59_Int16_t561,
	RuntimeInvoker_Void_t1856_Int32_t59_SByte_t559,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Int16_t561_Object_t_Object_t,
	RuntimeInvoker_Char_t556_Object_t,
	RuntimeInvoker_Mode_t729,
	RuntimeInvoker_Navigation_t730,
	RuntimeInvoker_Void_t1856_Rect_t30,
	RuntimeInvoker_Direction_t732,
	RuntimeInvoker_Void_t1856_Single_t36_SByte_t559,
	RuntimeInvoker_Axis_t735,
	RuntimeInvoker_MovementType_t739,
	RuntimeInvoker_Void_t1856_Single_t36_Int32_t59,
	RuntimeInvoker_Bounds_t742,
	RuntimeInvoker_Void_t1856_Navigation_t730,
	RuntimeInvoker_Transition_t743,
	RuntimeInvoker_Void_t1856_ColorBlock_t682,
	RuntimeInvoker_SpriteState_t745,
	RuntimeInvoker_Void_t1856_SpriteState_t745,
	RuntimeInvoker_SelectionState_t744,
	RuntimeInvoker_Object_t_Vector3_t6,
	RuntimeInvoker_Vector3_t6_Object_t_Vector2_t29,
	RuntimeInvoker_Void_t1856_Color_t5_SByte_t559,
	RuntimeInvoker_Boolean_t41_ColorU26_t4020_Color_t5,
	RuntimeInvoker_Direction_t749,
	RuntimeInvoker_Axis_t751,
	RuntimeInvoker_TextGenerationSettings_t824_Vector2_t29,
	RuntimeInvoker_Vector2_t29_Int32_t59,
	RuntimeInvoker_AspectMode_t764,
	RuntimeInvoker_Single_t36_Single_t36_Int32_t59,
	RuntimeInvoker_ScaleMode_t766,
	RuntimeInvoker_ScreenMatchMode_t767,
	RuntimeInvoker_Unit_t768,
	RuntimeInvoker_FitMode_t770,
	RuntimeInvoker_Corner_t772,
	RuntimeInvoker_Axis_t773,
	RuntimeInvoker_Constraint_t774,
	RuntimeInvoker_Single_t36_Int32_t59,
	RuntimeInvoker_Single_t36_Int32_t59_Single_t36,
	RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Single_t36_Single_t36,
	RuntimeInvoker_Boolean_t41_LayoutRebuilder_t782,
	RuntimeInvoker_Single_t36_Object_t_Int32_t59,
	RuntimeInvoker_Single_t36_Object_t_Object_t_Single_t36,
	RuntimeInvoker_Single_t36_Object_t_Object_t_Single_t36_ILayoutElementU26_t4021,
	RuntimeInvoker_Void_t1856_Object_t_Color32_t829_Int32_t59_Int32_t59_Single_t36_Single_t36,
	RuntimeInvoker_CameraDirection_t896,
	RuntimeInvoker_VideoBackgroundReflection_t990,
	RuntimeInvoker_Matrix4x4_t603_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_Matrix4x4_t603,
	RuntimeInvoker_Boolean_t41_Int32_t59_Object_t,
	RuntimeInvoker_Quaternion_t51,
	RuntimeInvoker_Boolean_t41_Vector3_t6,
	RuntimeInvoker_Boolean_t41_SByte_t559,
	RuntimeInvoker_Void_t1856_Quaternion_t51,
	RuntimeInvoker_Void_t1856_Vector3U26_t4022_Vector3U26_t4022,
	RuntimeInvoker_Boolean_t41_Single_t36_Single_t36,
	RuntimeInvoker_Void_t1856_Vector3_t6_Vector3_t6,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Matrix4x4_t603,
	RuntimeInvoker_ScreenOrientation_t1126,
	RuntimeInvoker_Void_t1856_Matrix4x4_t603_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Matrix4x4_t603,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_Single_t36_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Matrix4x4_t603_SingleU26_t4023_SingleU26_t4023,
	RuntimeInvoker_Single_t36_Matrix4x4_t603,
	RuntimeInvoker_Vector3_t6_Vector4_t822,
	RuntimeInvoker_Status_t889,
	RuntimeInvoker_VideoModeData_t897,
	RuntimeInvoker_VideoModeData_t897_Int32_t59,
	RuntimeInvoker_Boolean_t41_CameraDeviceModeU26_t4024,
	RuntimeInvoker_Boolean_t41_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_IntPtr_t,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Boolean_t41_Rect_t30,
	RuntimeInvoker_Boolean_t41_RectU26_t4025,
	RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t6_Quaternion_t51_Vector3_t6_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Vector3_t6_Vector3_t6,
	RuntimeInvoker_Boolean_t41_Object_t_Vector3_t6_Vector3_t6_Vector3_t6_Quaternion_t51,
	RuntimeInvoker_Object_t_Vector3U26_t4022_Vector3U26_t4022,
	RuntimeInvoker_Object_t_Vector3U26_t4022_Vector3U26_t4022_Vector3U26_t4022_QuaternionU26_t4026,
	RuntimeInvoker_Boolean_t41_IntPtr_t_Object_t_Vector3_t6_Vector3_t6_Vector3_t6_Quaternion_t51,
	RuntimeInvoker_Matrix4x4_t603_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_StorageType_t1073,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_SByte_t559,
	RuntimeInvoker_Void_t1856_Vector2_t29_Vector2_t29_Single_t36,
	RuntimeInvoker_Void_t1856_Vector3_t6_Vector3_t6_Single_t36,
	RuntimeInvoker_Void_t1856_TargetSearchResult_t1050,
	RuntimeInvoker_ImageTargetType_t932,
	RuntimeInvoker_Object_t_Object_t_RectangleData_t923,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Vector3_t6,
	RuntimeInvoker_Boolean_t41_Object_t_Single_t36,
	RuntimeInvoker_FrameQuality_t934,
	RuntimeInvoker_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_Int32_t59_VirtualButtonAbstractBehaviourU26_t4027,
	RuntimeInvoker_WordFilterMode_t1087,
	RuntimeInvoker_WordPrefabCreationMode_t1015,
	RuntimeInvoker_Sensitivity_t1060,
	RuntimeInvoker_Matrix4x4_t603,
	RuntimeInvoker_Boolean_t41_Matrix4x4_t603,
	RuntimeInvoker_Boolean_t41_Vector2_t29_Vector2_t29,
	RuntimeInvoker_WordTemplateMode_t941,
	RuntimeInvoker_PIXEL_FORMAT_t942,
	RuntimeInvoker_TextureFormat_t1257_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Single_t36_Int32_t59,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_Single_t36,
	RuntimeInvoker_Int32_t59_Int32_t59_Object_t_Single_t36,
	RuntimeInvoker_Void_t1856_Int32_t59_Vec2I_t992,
	RuntimeInvoker_ProfileCollection_t1065_Object_t,
	RuntimeInvoker_WorldCenterMode_t1075,
	RuntimeInvoker_Void_t1856_FrameState_t980,
	RuntimeInvoker_Void_t1856_FrameState_t980_Object_t,
	RuntimeInvoker_Boolean_t41_TrackableResultData_t969,
	RuntimeInvoker_VideoBGCfgData_t991,
	RuntimeInvoker_Void_t1856_VideoBGCfgData_t991,
	RuntimeInvoker_VideoTextureInfo_t880,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59,
	RuntimeInvoker_Matrix4x4_t603_Single_t36_Single_t36_Int32_t59,
	RuntimeInvoker_Void_t1856_PoseData_t968,
	RuntimeInvoker_OrientedBoundingBox3D_t926,
	RuntimeInvoker_Void_t1856_OrientedBoundingBox3D_t926,
	RuntimeInvoker_Boolean_t41_Rect_t30_Rect_t30,
	RuntimeInvoker_Boolean_t41_RectU26_t4025_RectU26_t4025,
	RuntimeInvoker_Rect_t30_RectangleIntData_t924_Rect_t30_SByte_t559_VideoModeData_t897,
	RuntimeInvoker_UpDirection_t1006,
	RuntimeInvoker_UInt16_t562_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Vec2I_t992,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Vector2_t29,
	RuntimeInvoker_Void_t1856_ImageHeaderData_t975_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_WordAbstractBehaviourU26_t4028,
	RuntimeInvoker_OrientedBoundingBox_t925,
	RuntimeInvoker_Void_t1856_Vector3_t6_Quaternion_t51,
	RuntimeInvoker_Void_t1856_OrientedBoundingBox_t925,
	RuntimeInvoker_Void_t1856_Int32_t59_IntPtr_t,
	RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_IntPtr_t,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_IntPtr_t,
	RuntimeInvoker_Int32_t59_Int32_t59_IntPtr_t,
	RuntimeInvoker_Int32_t59_Int32_t59_IntPtr_t_Int32_t59_IntPtr_t,
	RuntimeInvoker_Int32_t59_IntPtr_t_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Int32_t59_IntPtr_t_IntPtr_t_Object_t_Int32_t59_IntPtr_t,
	RuntimeInvoker_Int32_t59_IntPtr_t_Int32_t59,
	RuntimeInvoker_Int32_t59_IntPtr_t,
	RuntimeInvoker_Int32_t59_Object_t_Single_t36,
	RuntimeInvoker_Void_t1856_IntPtr_t_SByte_t559,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Object_t_IntPtr_t,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t59_IntPtr_t_IntPtr_t_Int32_t59_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_IntPtr_t,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Single_t36,
	RuntimeInvoker_Int32_t59_Int32_t59_Single_t36,
	RuntimeInvoker_Int32_t59_IntPtr_t_Int32_t59_IntPtr_t_Int32_t59,
	RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Single_t36_Single_t36_IntPtr_t_Int32_t59,
	RuntimeInvoker_Boolean_t41_IntPtr_t_IntPtr_t_Int32_t59_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_Single_t36,
	RuntimeInvoker_Boolean_t41_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Void_t1856_IntPtr_t_Single_t36,
	RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59,
	RuntimeInvoker_Int32_t59_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int32_t59,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_IntPtr_t_Int32_t59,
	RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_IntPtr_t,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_IntPtr_t,
	RuntimeInvoker_Boolean_t41_Int32_t59_IntPtr_t,
	RuntimeInvoker_Boolean_t41_IntPtr_t_Int32_t59_IntPtr_t,
	RuntimeInvoker_Int32_t59_SByte_t559,
	RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_IntPtr_t_Int32_t59_IntPtr_t,
	RuntimeInvoker_Void_t1856_SmartTerrainInitializationInfo_t916,
	RuntimeInvoker_Boolean_t41_Object_t_PropAbstractBehaviourU26_t4029,
	RuntimeInvoker_Boolean_t41_Object_t_SurfaceAbstractBehaviourU26_t4030,
	RuntimeInvoker_Object_t_MeshData_t976_Object_t_SByte_t559,
	RuntimeInvoker_Object_t_Int32_t59_IntPtr_t,
	RuntimeInvoker_Void_t1856_LinkedList_1U26_t4031,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_PoseData_t968,
	RuntimeInvoker_Vector3_t6_Matrix4x4_t603,
	RuntimeInvoker_Quaternion_t51_Matrix4x4_t603,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_PoseData_t968,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t,
	RuntimeInvoker_InitState_t1047,
	RuntimeInvoker_UpdateState_t1048,
	RuntimeInvoker_UpdateState_t1048_Int32_t59,
	RuntimeInvoker_Object_t_TargetSearchResult_t1050_Object_t,
	RuntimeInvoker_RectangleData_t923,
	RuntimeInvoker_Boolean_t41_RectangleData_t923,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_RectangleData_t923_Object_t_Object_t,
	RuntimeInvoker_Vec2I_t992,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_SByte_t559,
	RuntimeInvoker_ProfileData_t1064,
	RuntimeInvoker_ProfileData_t1064_Object_t,
	RuntimeInvoker_Void_t1856_ProfileData_t1064_Object_t,
	RuntimeInvoker_Object_t_Object_t_Vector2_t29_Vector2_t29,
	RuntimeInvoker_Object_t_Object_t_Vector2_t29_Object_t,
	RuntimeInvoker_CameraDeviceMode_t894,
	RuntimeInvoker_Boolean_t41_SByte_t559_SByte_t559,
	RuntimeInvoker_Vec2I_t992_Vector2_t29_Rect_t30_SByte_t559_VideoModeData_t897,
	RuntimeInvoker_Vector2_t29_Vector2_t29_Rect_t30_SByte_t559_VideoModeData_t897,
	RuntimeInvoker_OrientedBoundingBox_t925_OrientedBoundingBox_t925_Rect_t30_SByte_t559_VideoModeData_t897,
	RuntimeInvoker_Void_t1856_Rect_t30_SByte_t559_Vector2U26_t4032_Vector2U26_t4032,
	RuntimeInvoker_Rect_t30_Vector2_t29_Vector2_t29_SByte_t559,
	RuntimeInvoker_Void_t1856_SByte_t559_SingleU26_t4023_SingleU26_t4023_SingleU26_t4023_SingleU26_t4023_BooleanU26_t4018,
	RuntimeInvoker_Boolean_t41_Vector2U26_t4032_Vector2U26_t4032,
	RuntimeInvoker_Boolean_t41_Vector2_t29_Vector2_t29_Single_t36,
	RuntimeInvoker_Void_t1856_UserProfileU5BU5DU26_t4033_Int32_t59,
	RuntimeInvoker_Void_t1856_GcAchievementDescriptionData_t1243_Int32_t59,
	RuntimeInvoker_Void_t1856_GcUserProfileData_t1242_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Double_t60_Object_t,
	RuntimeInvoker_Void_t1856_Int64_t507_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_UserProfileU5BU5DU26_t4033_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Double_t60,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_Int32_t59_Object_t,
	RuntimeInvoker_UserState_t1274,
	RuntimeInvoker_Void_t1856_Object_t_Double_t60_SByte_t559_SByte_t559_DateTime_t220,
	RuntimeInvoker_Void_t1856_Double_t60,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t559_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int64_t507,
	RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Object_t_DateTime_t220_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Int64_t507,
	RuntimeInvoker_UserScope_t1275,
	RuntimeInvoker_Range_t1269,
	RuntimeInvoker_Void_t1856_Range_t1269,
	RuntimeInvoker_TimeScope_t1276,
	RuntimeInvoker_Void_t1856_Int32_t59_HitInfo_t1271,
	RuntimeInvoker_Boolean_t41_HitInfo_t1271_HitInfo_t1271,
	RuntimeInvoker_Boolean_t41_HitInfo_t1271,
	RuntimeInvoker_Void_t1856_Object_t_StringU26_t4016_StringU26_t4016,
	RuntimeInvoker_Void_t1856_Object_t_StreamingContext_t1439,
	RuntimeInvoker_Boolean_t41_Color_t5_Color_t5,
	RuntimeInvoker_Boolean_t41_TextGenerationSettings_t824,
	RuntimeInvoker_TextGenerationSettings_t824_TextGenerationSettings_t824,
	RuntimeInvoker_Single_t36_Object_t_TextGenerationSettings_t824,
	RuntimeInvoker_Boolean_t41_Object_t_TextGenerationSettings_t824,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Color_t5_Int32_t59_Single_t36_Single_t36_Int32_t59_SByte_t559_SByte_t559_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_Int32_t59_Vector2_t29_Vector2_t29_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Color_t5_Int32_t59_Single_t36_Single_t36_Int32_t59_SByte_t559_SByte_t559_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_Int32_t59_Single_t36_Single_t36_Single_t36_Single_t36_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_ColorU26_t4020_Int32_t59_Single_t36_Single_t36_Int32_t59_SByte_t559_SByte_t559_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_Int32_t59_Single_t36_Single_t36_Single_t36_Single_t36_SByte_t559,
	RuntimeInvoker_Void_t1856_RectU26_t4025,
	RuntimeInvoker_PersistentListenerMode_t1291,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Object_t_SByte_t559,
	RuntimeInvoker_iPhoneGeneration_t501,
	RuntimeInvoker_Void_t1856_Vector3U26_t4022,
	RuntimeInvoker_Boolean_t41_Vector3_t6_Vector3_t6_RaycastHitU26_t4034_Single_t36_Int32_t59,
	RuntimeInvoker_Boolean_t41_Vector3U26_t4022_Vector3U26_t4022_RaycastHitU26_t4034_Single_t36_Int32_t59,
	RuntimeInvoker_Boolean_t41_Ray_t567_RaycastHitU26_t4034_Single_t36_Int32_t59,
	RuntimeInvoker_Object_t_Ray_t567_Single_t36_Int32_t59,
	RuntimeInvoker_Object_t_Vector3_t6_Vector3_t6_Single_t36_Int32_t59,
	RuntimeInvoker_Object_t_Vector3U26_t4022_Vector3U26_t4022_Single_t36_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Vector3U26_t4022,
	RuntimeInvoker_Void_t1856_Object_t_QuaternionU26_t4026,
	RuntimeInvoker_Boolean_t41_Object_t_Ray_t567_RaycastHitU26_t4034_Single_t36,
	RuntimeInvoker_Boolean_t41_Object_t_RayU26_t4035_RaycastHitU26_t4034_Single_t36,
	RuntimeInvoker_Boolean_t41_Ray_t567_RaycastHitU26_t4034_Single_t36,
	RuntimeInvoker_Void_t1856_Vector2_t29_Vector2_t29_Single_t36_Int32_t59_Single_t36_Single_t36_RaycastHit2DU26_t4036,
	RuntimeInvoker_Void_t1856_Vector2U26_t4032_Vector2U26_t4032_Single_t36_Int32_t59_Single_t36_Single_t36_RaycastHit2DU26_t4036,
	RuntimeInvoker_RaycastHit2D_t838_Vector2_t29_Vector2_t29_Single_t36_Int32_t59,
	RuntimeInvoker_RaycastHit2D_t838_Vector2_t29_Vector2_t29_Single_t36_Int32_t59_Single_t36_Single_t36,
	RuntimeInvoker_Object_t_Vector2_t29_Vector2_t29_Single_t36_Int32_t59,
	RuntimeInvoker_Object_t_Vector2U26_t4032_Vector2U26_t4032_Single_t36_Int32_t59_Single_t36_Single_t36,
	RuntimeInvoker_Int32_t59_Ray_t567_Object_t_Single_t36_Int32_t59,
	RuntimeInvoker_Int32_t59_RayU26_t4035_Object_t_Single_t36_Int32_t59,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Vector3_t6,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_SendMessageOptions_t1345,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_SByte_t559_SByte_t559_Object_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t559_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_TerrainRenderFlags_t1333,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1856_IntPtr_t_Object_t,
	RuntimeInvoker_Single_t36_IntPtr_t,
	RuntimeInvoker_Single_t36_Vector3_t6,
	RuntimeInvoker_Single_t36_IntPtr_t_Vector3_t6,
	RuntimeInvoker_Single_t36_Object_t_IntPtr_t_Vector3U26_t4022,
	RuntimeInvoker_Void_t1856_TreeInstance_t1330,
	RuntimeInvoker_Void_t1856_IntPtr_t_TreeInstance_t1330,
	RuntimeInvoker_Void_t1856_Object_t_IntPtr_t_TreeInstanceU26_t4037,
	RuntimeInvoker_Void_t1856_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Vector3_t6_IntPtr_t,
	RuntimeInvoker_Void_t1856_Vector2_t29_Single_t36_Int32_t59,
	RuntimeInvoker_Void_t1856_IntPtr_t_Vector2_t29_Single_t36_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_IntPtr_t_Vector2U26_t4032_Single_t36_Int32_t59,
	RuntimeInvoker_Void_t1856_Vector2U26_t4032,
	RuntimeInvoker_Void_t1856_Int32_t59_Single_t36_Single_t36,
	RuntimeInvoker_Void_t1856_Int32_t59_Single_t36,
	RuntimeInvoker_Boolean_t41_Object_t_Vector2_t29_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Vector2U26_t4032_Object_t,
	RuntimeInvoker_Vector2_t29_Vector2_t29_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Vector2_t29_Object_t_Object_t_Vector2U26_t4032,
	RuntimeInvoker_Void_t1856_Vector2U26_t4032_Object_t_Object_t_Vector2U26_t4032,
	RuntimeInvoker_Rect_t30_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Vector2_t29_Object_t_Vector3U26_t4022,
	RuntimeInvoker_Boolean_t41_Object_t_Vector2_t29_Object_t_Vector2U26_t4032,
	RuntimeInvoker_Ray_t567_Object_t_Vector2_t29,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_RenderMode_t1338,
	RuntimeInvoker_Void_t1856_Object_t_ColorU26_t4020,
	RuntimeInvoker_Int32_t59_LayerMask_t665,
	RuntimeInvoker_LayerMask_t665_Int32_t59,
	RuntimeInvoker_Void_t1856_GcScoreData_t1245,
	RuntimeInvoker_ColorSpace_t1367,
	RuntimeInvoker_Void_t1856_BoundsU26_t4038,
	RuntimeInvoker_Boolean_t41_BoneWeight_t1361_BoneWeight_t1361,
	RuntimeInvoker_Void_t1856_InternalDrawTextureArgumentsU26_t4039,
	RuntimeInvoker_Void_t1856_Matrix4x4_t603,
	RuntimeInvoker_Void_t1856_Matrix4x4U26_t4040,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_Color_t5,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_Color_t5_Single_t36,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_ColorU26_t4020_Single_t36,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559_IntPtr_t,
	RuntimeInvoker_TextureFormat_t1257,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Color_t5,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_ColorU26_t4020,
	RuntimeInvoker_Color_t5_Single_t36_Single_t36,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_Rect_t30_Int32_t59_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_Rect_t30_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_RectU26_t4025_Int32_t59_Int32_t59_SByte_t559,
	RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_ColorU26_t4020,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t4022,
	RuntimeInvoker_Void_t1856_Color_t5_Single_t36,
	RuntimeInvoker_Void_t1856_Rect_t30_Object_t,
	RuntimeInvoker_Void_t1856_Rect_t30_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Rect_t30_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1856_RectU26_t4025_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1856_Rect_t30_Object_t_Int32_t59_SByte_t559_Single_t36,
	RuntimeInvoker_Boolean_t41_Rect_t30_Object_t,
	RuntimeInvoker_Boolean_t41_Rect_t30_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Rect_t30_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t41_RectU26_t4025_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t561,
	RuntimeInvoker_Void_t1856_Rect_t30_Int32_t59_Object_t_SByte_t559_Int32_t59_Object_t_Object_t_Int16_t561,
	RuntimeInvoker_Void_t1856_Rect_t30_Int32_t59_Object_t_SByte_t559_Int32_t59_Object_t_Object_t_Int16_t561_Object_t,
	RuntimeInvoker_Rect_t30_Int32_t59_Rect_t30_Object_t_Object_t,
	RuntimeInvoker_Rect_t30_Int32_t59_Rect_t30_Object_t_Object_t_Object_t,
	RuntimeInvoker_Rect_t30_Int32_t59_Rect_t30_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Rect_t30_Int32_t59_RectU26_t4025_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Int32_t59_Single_t36_Single_t36_Object_t,
	RuntimeInvoker_Rect_t30_Int32_t59_Rect_t30_Object_t_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Rect_t30_Int32_t59_RectU26_t4025_Object_t_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_Object_t_Single_t36,
	RuntimeInvoker_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Rect_t30_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Rect_t30,
	RuntimeInvoker_Void_t1856_Int32_t59_RectU26_t4025,
	RuntimeInvoker_Rect_t30_Object_t_Object_t_Object_t,
	RuntimeInvoker_Rect_t30_Single_t36_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Rect_t30_Single_t36_Single_t36_Single_t36_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36_Single_t36_Object_t,
	RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Rect_t30_Vector2_t29_Vector2_t29_SByte_t559,
	RuntimeInvoker_Void_t1856_RectU26_t4025_Vector2U26_t4032_Vector2U26_t4032_SByte_t559,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Rect_t30_Rect_t30,
	RuntimeInvoker_Rect_t30_Object_t_RectU26_t4025,
	RuntimeInvoker_ImagePosition_t1399,
	RuntimeInvoker_Void_t1856_Rect_t30_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Rect_t30_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_IntPtr_t_Rect_t30_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_IntPtr_t_RectU26_t4025_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Vector2_t29_Rect_t30_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_IntPtr_t_Rect_t30_Object_t_Int32_t59_Vector2U26_t4032,
	RuntimeInvoker_Void_t1856_IntPtr_t_RectU26_t4025_Object_t_Int32_t59_Vector2U26_t4032,
	RuntimeInvoker_Vector2_t29_Object_t,
	RuntimeInvoker_Void_t1856_IntPtr_t_Object_t_Vector2U26_t4032,
	RuntimeInvoker_Single_t36_Object_t_Single_t36,
	RuntimeInvoker_Single_t36_IntPtr_t_Object_t_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_SingleU26_t4023_SingleU26_t4023,
	RuntimeInvoker_Void_t1856_IntPtr_t_Object_t_SingleU26_t4023_SingleU26_t4023,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t,
	RuntimeInvoker_Void_t1856_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t4041_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Object_t,
	RuntimeInvoker_EventType_t1402,
	RuntimeInvoker_EventType_t1402_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Vector2U26_t4032,
	RuntimeInvoker_Ray_t567,
	RuntimeInvoker_Void_t1856_Ray_t567,
	RuntimeInvoker_EventModifiers_t1403,
	RuntimeInvoker_KeyCode_t1401,
	RuntimeInvoker_Void_t1856_Vector3_t6_Single_t36,
	RuntimeInvoker_Void_t1856_Vector3U26_t4022_Single_t36,
	RuntimeInvoker_Vector2_t29_Vector2_t29_Vector2_t29,
	RuntimeInvoker_Single_t36_Vector2_t29,
	RuntimeInvoker_Vector2_t29_Vector2_t29_Single_t36,
	RuntimeInvoker_Vector2_t29_Vector3_t6,
	RuntimeInvoker_Vector3_t6_Vector2_t29,
	RuntimeInvoker_Vector3_t6_Vector3_t6_Vector3_t6,
	RuntimeInvoker_Vector3_t6_Vector3_t6,
	RuntimeInvoker_Single_t36_Vector3_t6_Vector3_t6,
	RuntimeInvoker_Vector3_t6_Vector3_t6_Single_t36,
	RuntimeInvoker_Vector3_t6_Single_t36_Vector3_t6,
	RuntimeInvoker_Boolean_t41_Vector3_t6_Vector3_t6,
	RuntimeInvoker_Void_t1856_Single_t36_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Color_t5_Color_t5_Color_t5_Single_t36,
	RuntimeInvoker_Color_t5_Color_t5_Single_t36,
	RuntimeInvoker_Vector4_t822_Color_t5,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Color32_t829_Color_t5,
	RuntimeInvoker_Color_t5_Color32_t829,
	RuntimeInvoker_Single_t36_Quaternion_t51_Quaternion_t51,
	RuntimeInvoker_Quaternion_t51_Single_t36_Vector3_t6,
	RuntimeInvoker_Quaternion_t51_Single_t36_Vector3U26_t4022,
	RuntimeInvoker_Void_t1856_SingleU26_t4023_Vector3U26_t4022,
	RuntimeInvoker_Quaternion_t51_Vector3_t6_Vector3_t6,
	RuntimeInvoker_Quaternion_t51_Vector3U26_t4022_Vector3U26_t4022,
	RuntimeInvoker_Quaternion_t51_Quaternion_t51_Quaternion_t51_Single_t36,
	RuntimeInvoker_Quaternion_t51_QuaternionU26_t4026_QuaternionU26_t4026_Single_t36,
	RuntimeInvoker_Quaternion_t51_Quaternion_t51,
	RuntimeInvoker_Quaternion_t51_QuaternionU26_t4026,
	RuntimeInvoker_Quaternion_t51_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Quaternion_t51_Vector3_t6,
	RuntimeInvoker_Vector3_t6_Quaternion_t51,
	RuntimeInvoker_Vector3_t6_QuaternionU26_t4026,
	RuntimeInvoker_Quaternion_t51_Vector3U26_t4022,
	RuntimeInvoker_Void_t1856_Quaternion_t51_Vector3U26_t4022_SingleU26_t4023,
	RuntimeInvoker_Void_t1856_QuaternionU26_t4026_Vector3U26_t4022_SingleU26_t4023,
	RuntimeInvoker_Quaternion_t51_Quaternion_t51_Quaternion_t51,
	RuntimeInvoker_Vector3_t6_Quaternion_t51_Vector3_t6,
	RuntimeInvoker_Boolean_t41_Quaternion_t51_Quaternion_t51,
	RuntimeInvoker_Rect_t30_Single_t36_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Boolean_t41_Vector2_t29,
	RuntimeInvoker_Single_t36_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Single_t36,
	RuntimeInvoker_Matrix4x4_t603_Matrix4x4_t603,
	RuntimeInvoker_Matrix4x4_t603_Matrix4x4U26_t4040,
	RuntimeInvoker_Boolean_t41_Matrix4x4_t603_Matrix4x4U26_t4040,
	RuntimeInvoker_Boolean_t41_Matrix4x4U26_t4040_Matrix4x4U26_t4040,
	RuntimeInvoker_Vector4_t822_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Vector4_t822,
	RuntimeInvoker_Matrix4x4_t603_Vector3_t6,
	RuntimeInvoker_Void_t1856_Vector3_t6_Quaternion_t51_Vector3_t6,
	RuntimeInvoker_Matrix4x4_t603_Vector3_t6_Quaternion_t51_Vector3_t6,
	RuntimeInvoker_Matrix4x4_t603_Vector3U26_t4022_QuaternionU26_t4026_Vector3U26_t4022,
	RuntimeInvoker_Matrix4x4_t603_Single_t36_Single_t36_Single_t36_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Matrix4x4_t603_Single_t36_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Matrix4x4_t603_Matrix4x4_t603_Matrix4x4_t603,
	RuntimeInvoker_Vector4_t822_Matrix4x4_t603_Vector4_t822,
	RuntimeInvoker_Boolean_t41_Matrix4x4_t603_Matrix4x4_t603,
	RuntimeInvoker_Void_t1856_Bounds_t742,
	RuntimeInvoker_Boolean_t41_Bounds_t742,
	RuntimeInvoker_Boolean_t41_Bounds_t742_Vector3_t6,
	RuntimeInvoker_Boolean_t41_BoundsU26_t4038_Vector3U26_t4022,
	RuntimeInvoker_Single_t36_Bounds_t742_Vector3_t6,
	RuntimeInvoker_Single_t36_BoundsU26_t4038_Vector3U26_t4022,
	RuntimeInvoker_Boolean_t41_RayU26_t4035_BoundsU26_t4038_SingleU26_t4023,
	RuntimeInvoker_Boolean_t41_Ray_t567,
	RuntimeInvoker_Boolean_t41_Ray_t567_SingleU26_t4023,
	RuntimeInvoker_Boolean_t41_Bounds_t742_Bounds_t742,
	RuntimeInvoker_Single_t36_Vector4_t822_Vector4_t822,
	RuntimeInvoker_Single_t36_Vector4_t822,
	RuntimeInvoker_Vector4_t822,
	RuntimeInvoker_Vector4_t822_Vector4_t822_Vector4_t822,
	RuntimeInvoker_Vector4_t822_Vector4_t822_Single_t36,
	RuntimeInvoker_Boolean_t41_Vector4_t822_Vector4_t822,
	RuntimeInvoker_Single_t36_Single_t36,
	RuntimeInvoker_Int32_t59_Single_t36,
	RuntimeInvoker_Single_t36_Single_t36_Single_t36_SingleU26_t4023_Single_t36,
	RuntimeInvoker_Single_t36_Single_t36_Single_t36_SingleU26_t4023_Single_t36_Single_t36_Single_t36,
	RuntimeInvoker_Void_t1856_Int32_t59_Color_t5,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_ColorU26_t4020,
	RuntimeInvoker_Color_t5_Object_t,
	RuntimeInvoker_Color_t5_Int32_t59,
	RuntimeInvoker_Void_t1856_Vector4U26_t4042,
	RuntimeInvoker_Vector4_t822_Object_t,
	RuntimeInvoker_Object_t_SByte_t559_Object_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t559_SByte_t559_Object_t_SByte_t559,
	RuntimeInvoker_RuntimePlatform_t1347,
	RuntimeInvoker_NetworkReachability_t1415,
	RuntimeInvoker_UserAuthorization_t1418,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_CameraClearFlags_t1360,
	RuntimeInvoker_Vector3_t6_Object_t_Vector3U26_t4022,
	RuntimeInvoker_Ray_t567_Vector3_t6,
	RuntimeInvoker_Ray_t567_Object_t_Vector3U26_t4022,
	RuntimeInvoker_RenderBuffer_t1363,
	RuntimeInvoker_Void_t1856_IntPtr_t_Int32U26_t4019_Int32U26_t4019,
	RuntimeInvoker_Void_t1856_IntPtr_t_RenderBufferU26_t4043_RenderBufferU26_t4043,
	RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_Int32_t59,
	RuntimeInvoker_TouchPhase_t1425,
	RuntimeInvoker_Touch_t818_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Vector3_t6_Quaternion_t51,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t4022_QuaternionU26_t4026,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_SByte_t559_Object_t,
	RuntimeInvoker_Void_t1856_QuaternionU26_t4026,
	RuntimeInvoker_Void_t1856_Vector3_t6_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Vector3U26_t4022_Single_t36,
	RuntimeInvoker_Void_t1856_Object_t_Vector3U26_t4022_Vector3U26_t4022,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t4044,
	RuntimeInvoker_DictionaryEntry_t58,
	RuntimeInvoker_EditorBrowsableState_t1500,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Int16_t561_Int16_t561,
	RuntimeInvoker_Boolean_t41_Object_t_IPAddressU26_t4045,
	RuntimeInvoker_AddressFamily_t1505,
	RuntimeInvoker_Object_t_Int64_t507,
	RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019,
	RuntimeInvoker_Boolean_t41_Object_t_IPv6AddressU26_t4046,
	RuntimeInvoker_UInt16_t562_Int16_t561,
	RuntimeInvoker_SecurityProtocolType_t1525,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_Int32_t59_SByte_t559,
	RuntimeInvoker_AsnDecodeStatus_t1574_Object_t,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_SByte_t559,
	RuntimeInvoker_X509ChainStatusFlags_t1561_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t1561_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_X509ChainStatusFlags_t1561_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_X509ChainStatusFlags_t1561,
	RuntimeInvoker_Void_t1856_Object_t_Int32U26_t4019_Int32_t59_Int32_t59,
	RuntimeInvoker_X509RevocationFlag_t1568,
	RuntimeInvoker_X509RevocationMode_t1569,
	RuntimeInvoker_X509VerificationFlags_t1573,
	RuntimeInvoker_X509KeyUsageFlags_t1566,
	RuntimeInvoker_X509KeyUsageFlags_t1566_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Byte_t560_Int16_t561,
	RuntimeInvoker_Byte_t560_Int16_t561_Int16_t561,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_RegexOptions_t1590,
	RuntimeInvoker_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Category_t1597_Object_t,
	RuntimeInvoker_Boolean_t41_UInt16_t562_Int16_t561,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int16_t561,
	RuntimeInvoker_Void_t1856_Int16_t561_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_UInt16_t562_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_Int16_t561_Int16_t561_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_Int16_t561_Object_t_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_UInt16_t562,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_SByte_t559_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_SByte_t559_Int32_t59_Object_t,
	RuntimeInvoker_UInt16_t562_UInt16_t562_UInt16_t562,
	RuntimeInvoker_OpFlags_t1592_SByte_t559_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_UInt16_t562_UInt16_t562,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32U26_t4019_Int32_t59,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32U26_t4019_Int32U26_t4019_SByte_t559,
	RuntimeInvoker_Boolean_t41_Int32U26_t4019_Int32_t59,
	RuntimeInvoker_Boolean_t41_UInt16_t562_Int32_t59,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_SByte_t559_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32U26_t4019_Int32U26_t4019,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_SByte_t559_Int32_t59,
	RuntimeInvoker_Interval_t1612,
	RuntimeInvoker_Boolean_t41_Interval_t1612,
	RuntimeInvoker_Void_t1856_Interval_t1612,
	RuntimeInvoker_Interval_t1612_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Double_t60_Interval_t1612,
	RuntimeInvoker_Object_t_Interval_t1612_Object_t_Object_t,
	RuntimeInvoker_Double_t60_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_Int32U26_t4019,
	RuntimeInvoker_Int32_t59_Object_t_Int32U26_t4019_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t4019,
	RuntimeInvoker_Object_t_RegexOptionsU26_t4047,
	RuntimeInvoker_Void_t1856_RegexOptionsU26_t4047_SByte_t559,
	RuntimeInvoker_Boolean_t41_Int32U26_t4019_Int32U26_t4019_Int32_t59,
	RuntimeInvoker_Category_t1597,
	RuntimeInvoker_Int32_t59_Int16_t561_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Int16_t561,
	RuntimeInvoker_Char_t556_Int16_t561,
	RuntimeInvoker_Int32_t59_Int32U26_t4019,
	RuntimeInvoker_Void_t1856_Int32U26_t4019_Int32U26_t4019,
	RuntimeInvoker_Void_t1856_Int32U26_t4019_Int32U26_t4019_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_UInt16_t562_SByte_t559,
	RuntimeInvoker_Void_t1856_Int16_t561_Int16_t561,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_UInt16_t562,
	RuntimeInvoker_Position_t1593,
	RuntimeInvoker_UriHostNameType_t1645_Object_t,
	RuntimeInvoker_Object_t_Int16_t561,
	RuntimeInvoker_Void_t1856_StringU26_t4016,
	RuntimeInvoker_Object_t_Object_t_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Char_t556_Object_t_Int32U26_t4019_CharU26_t4048,
	RuntimeInvoker_Void_t1856_Object_t_UriFormatExceptionU26_t4049,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_UInt32_t558_Object_t_Int32_t59,
	RuntimeInvoker_Sign_t1690_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_ConfidenceFactor_t1696,
	RuntimeInvoker_Byte_t560,
	RuntimeInvoker_Void_t1856_Object_t_Int32U26_t4019_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32U26_t4019_ByteU26_t4050_Int32U26_t4019_ByteU5BU5DU26_t4051,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t1674,
	RuntimeInvoker_RSAParameters_t1672_SByte_t559,
	RuntimeInvoker_Void_t1856_RSAParameters_t1672,
	RuntimeInvoker_DSAParameters_t1674_BooleanU26_t4018,
	RuntimeInvoker_Object_t_Object_t_SByte_t559_Object_t_SByte_t559,
	RuntimeInvoker_Boolean_t41_DateTime_t220,
	RuntimeInvoker_X509ChainStatusFlags_t1723,
	RuntimeInvoker_Void_t1856_Byte_t560,
	RuntimeInvoker_Void_t1856_Byte_t560_Byte_t560,
	RuntimeInvoker_AlertLevel_t1736,
	RuntimeInvoker_AlertDescription_t1737,
	RuntimeInvoker_Object_t_Byte_t560,
	RuntimeInvoker_Void_t1856_Int16_t561_Object_t_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Int16_t561_SByte_t559_SByte_t559,
	RuntimeInvoker_CipherAlgorithmType_t1739,
	RuntimeInvoker_HashAlgorithmType_t1758,
	RuntimeInvoker_ExchangeAlgorithmType_t1756,
	RuntimeInvoker_CipherMode_t1845,
	RuntimeInvoker_Int16_t561,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int16_t561,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int64_t507,
	RuntimeInvoker_Void_t1856_Object_t_ByteU5BU5DU26_t4051_ByteU5BU5DU26_t4051,
	RuntimeInvoker_Object_t_Byte_t560_Object_t,
	RuntimeInvoker_Object_t_Int16_t561_Object_t_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559_SByte_t559_SByte_t559_Int16_t561_SByte_t559_SByte_t559,
	RuntimeInvoker_SecurityProtocolType_t1772,
	RuntimeInvoker_SecurityCompressionType_t1771,
	RuntimeInvoker_HandshakeType_t1785,
	RuntimeInvoker_HandshakeState_t1757,
	RuntimeInvoker_UInt64_t563,
	RuntimeInvoker_SecurityProtocolType_t1772_Int16_t561,
	RuntimeInvoker_Object_t_Byte_t560_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t560_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Byte_t560_Object_t,
	RuntimeInvoker_Object_t_Byte_t560_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_SByte_t559_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Int64_t507_Int64_t507_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_Byte_t560_Byte_t560_Object_t,
	RuntimeInvoker_RSAParameters_t1672,
	RuntimeInvoker_Void_t1856_Object_t_Byte_t560,
	RuntimeInvoker_Void_t1856_Object_t_Byte_t560_Byte_t560,
	RuntimeInvoker_Void_t1856_Object_t_Byte_t560_Object_t,
	RuntimeInvoker_ContentType_t1751,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559_Object_t,
	RuntimeInvoker_UInt32_t558_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_ObjectU5BU5DU26_t4052,
	RuntimeInvoker_Int32_t59_Object_t_ObjectU5BU5DU26_t4052,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Byte_t560_Object_t,
	RuntimeInvoker_Decimal_t564_Object_t,
	RuntimeInvoker_Int16_t561_Object_t,
	RuntimeInvoker_Int64_t507_Object_t,
	RuntimeInvoker_SByte_t559_Object_t,
	RuntimeInvoker_UInt32_t558_Object_t,
	RuntimeInvoker_UInt64_t563_Object_t,
	RuntimeInvoker_Boolean_t41_SByte_t559_Object_t_Int32_t59_ExceptionU26_t4053,
	RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_Int32U26_t4019_ExceptionU26_t4053,
	RuntimeInvoker_Boolean_t41_Int32_t59_SByte_t559_ExceptionU26_t4053,
	RuntimeInvoker_Boolean_t41_Int32U26_t4019_Object_t_SByte_t559_SByte_t559_ExceptionU26_t4053,
	RuntimeInvoker_Void_t1856_Int32U26_t4019_Object_t_Object_t_BooleanU26_t4018_BooleanU26_t4018,
	RuntimeInvoker_Void_t1856_Int32U26_t4019_Object_t_Object_t_BooleanU26_t4018,
	RuntimeInvoker_Boolean_t41_Int32U26_t4019_Object_t_Int32U26_t4019_SByte_t559_ExceptionU26_t4053,
	RuntimeInvoker_Boolean_t41_Int32U26_t4019_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Int16_t561_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_Int32U26_t4019_ExceptionU26_t4053,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_Int32U26_t4019,
	RuntimeInvoker_TypeCode_t2475,
	RuntimeInvoker_Int32_t59_Int64_t507,
	RuntimeInvoker_Boolean_t41_Int64_t507,
	RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_Int64U26_t4054_ExceptionU26_t4053,
	RuntimeInvoker_Int64_t507_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_Int64U26_t4054_ExceptionU26_t4053,
	RuntimeInvoker_Int64_t507_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Int64U26_t4054,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_Int64U26_t4054,
	RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_UInt32U26_t4055_ExceptionU26_t4053,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_UInt32U26_t4055_ExceptionU26_t4053,
	RuntimeInvoker_UInt32_t558_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_UInt32_t558_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_UInt32U26_t4055,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_UInt32U26_t4055,
	RuntimeInvoker_UInt64_t563_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_UInt64U26_t4056_ExceptionU26_t4053,
	RuntimeInvoker_UInt64_t563_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_UInt64U26_t4056,
	RuntimeInvoker_Byte_t560_Object_t_Object_t,
	RuntimeInvoker_Byte_t560_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_ByteU26_t4050,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_ByteU26_t4050,
	RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_SByteU26_t4057_ExceptionU26_t4053,
	RuntimeInvoker_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_SByte_t559_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_SByteU26_t4057,
	RuntimeInvoker_Boolean_t41_Object_t_SByte_t559_Int16U26_t4058_ExceptionU26_t4053,
	RuntimeInvoker_Int16_t561_Object_t_Object_t,
	RuntimeInvoker_Int16_t561_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Int16U26_t4058,
	RuntimeInvoker_UInt16_t562_Object_t_Object_t,
	RuntimeInvoker_UInt16_t562_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_UInt16U26_t4059,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_UInt16U26_t4059,
	RuntimeInvoker_Void_t1856_ByteU2AU26_t4060_ByteU2AU26_t4060_DoubleU2AU26_t4061_UInt16U2AU26_t4062_UInt16U2AU26_t4062_UInt16U2AU26_t4062_UInt16U2AU26_t4062,
	RuntimeInvoker_UnicodeCategory_t2002_Int16_t561,
	RuntimeInvoker_Char_t556_Int16_t561_Object_t,
	RuntimeInvoker_Void_t1856_Int16_t561_Int32_t59,
	RuntimeInvoker_Char_t556_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_SByte_t559_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59_Int32_t59_SByte_t559_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Int16_t561_Int32_t59,
	RuntimeInvoker_Object_t_Int32_t59_Int16_t561,
	RuntimeInvoker_Object_t_Int16_t561_Int16_t561,
	RuntimeInvoker_Void_t1856_Object_t_Int32U26_t4019_Int32U26_t4019_Int32U26_t4019_BooleanU26_t4018_StringU26_t4016,
	RuntimeInvoker_Void_t1856_Int32_t59_Int16_t561,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Object_t_Int16_t561_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_Double_t60,
	RuntimeInvoker_Boolean_t41_Double_t60,
	RuntimeInvoker_Double_t60_Object_t_Object_t,
	RuntimeInvoker_Double_t60_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_DoubleU26_t4063_ExceptionU26_t4053,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_DoubleU26_t4063,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Object_t_Decimal_t564,
	RuntimeInvoker_Decimal_t564_Decimal_t564_Decimal_t564,
	RuntimeInvoker_UInt64_t563_Decimal_t564,
	RuntimeInvoker_Int64_t507_Decimal_t564,
	RuntimeInvoker_Boolean_t41_Decimal_t564_Decimal_t564,
	RuntimeInvoker_Decimal_t564_Decimal_t564,
	RuntimeInvoker_Int32_t59_Decimal_t564_Decimal_t564,
	RuntimeInvoker_Int32_t59_Decimal_t564,
	RuntimeInvoker_Boolean_t41_Decimal_t564,
	RuntimeInvoker_Decimal_t564_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Int32U26_t4019_BooleanU26_t4018_BooleanU26_t4018_Int32U26_t4019_SByte_t559,
	RuntimeInvoker_Decimal_t564_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_DecimalU26_t4064_SByte_t559,
	RuntimeInvoker_Int32_t59_DecimalU26_t4064_UInt64U26_t4056,
	RuntimeInvoker_Int32_t59_DecimalU26_t4064_Int64U26_t4054,
	RuntimeInvoker_Int32_t59_DecimalU26_t4064_DecimalU26_t4064,
	RuntimeInvoker_Int32_t59_DecimalU26_t4064_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_DecimalU26_t4064_Int32_t59,
	RuntimeInvoker_Double_t60_DecimalU26_t4064,
	RuntimeInvoker_Void_t1856_DecimalU26_t4064_Int32_t59,
	RuntimeInvoker_Int32_t59_DecimalU26_t4064_DecimalU26_t4064_DecimalU26_t4064,
	RuntimeInvoker_Byte_t560_Decimal_t564,
	RuntimeInvoker_SByte_t559_Decimal_t564,
	RuntimeInvoker_Int16_t561_Decimal_t564,
	RuntimeInvoker_UInt16_t562_Decimal_t564,
	RuntimeInvoker_UInt32_t558_Decimal_t564,
	RuntimeInvoker_Decimal_t564_SByte_t559,
	RuntimeInvoker_Decimal_t564_Int16_t561,
	RuntimeInvoker_Decimal_t564_Int32_t59,
	RuntimeInvoker_Decimal_t564_Int64_t507,
	RuntimeInvoker_Decimal_t564_Single_t36,
	RuntimeInvoker_Decimal_t564_Double_t60,
	RuntimeInvoker_Single_t36_Decimal_t564,
	RuntimeInvoker_Double_t60_Decimal_t564,
	RuntimeInvoker_IntPtr_t_Int64_t507,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_UInt32_t558,
	RuntimeInvoker_UInt64_t563_IntPtr_t,
	RuntimeInvoker_UInt32_t558_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t507,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t4065,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t559_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_UInt64_t563_Object_t_Int32_t59,
	RuntimeInvoker_Object_t_Object_t_Int64_t507,
	RuntimeInvoker_Int64_t507_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Object_t_Int64_t507_Int64_t507,
	RuntimeInvoker_Object_t_Int64_t507_Int64_t507_Int64_t507,
	RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Int64_t507,
	RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Int64_t507_Int64_t507,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Object_t_Int64_t507_Int64_t507,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int64_t507,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_TypeAttributes_t2126,
	RuntimeInvoker_MemberTypes_t2106,
	RuntimeInvoker_RuntimeTypeHandle_t1857,
	RuntimeInvoker_Object_t_Object_t_SByte_t559_SByte_t559,
	RuntimeInvoker_TypeCode_t2475_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t1857,
	RuntimeInvoker_RuntimeTypeHandle_t1857_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_RuntimeFieldHandle_t1859,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_ContractionU5BU5DU26_t4066_Level2MapU5BU5DU26_t4067,
	RuntimeInvoker_Void_t1856_Object_t_CodePointIndexerU26_t4068_ByteU2AU26_t4060_ByteU2AU26_t4060_CodePointIndexerU26_t4068_ByteU2AU26_t4060,
	RuntimeInvoker_Byte_t560_Int32_t59,
	RuntimeInvoker_Byte_t560_Int32_t59_Int32_t59,
	RuntimeInvoker_ExtenderType_t1903_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_BooleanU26_t4018_BooleanU26_t4018_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32_t59_BooleanU26_t4018_BooleanU26_t4018_SByte_t559_SByte_t559_ContextU26_t4069,
	RuntimeInvoker_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59_Int32_t59_SByte_t559_ContextU26_t4069,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59_BooleanU26_t4018,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int16_t561_Int32_t59_SByte_t559_ContextU26_t4069,
	RuntimeInvoker_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59_Object_t_ContextU26_t4069,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_Object_t_Int32_t59_SByte_t559_ContextU26_t4069,
	RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Object_t_SByte_t559_ContextU26_t4069,
	RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Object_t_SByte_t559_Int32_t59_ContractionU26_t4070_ContextU26_t4069,
	RuntimeInvoker_Boolean_t41_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Int32_t59_Object_t_SByte_t559_ContextU26_t4069,
	RuntimeInvoker_Boolean_t41_Object_t_Int32U26_t4019_Int32_t59_Int32_t59_Int32_t59_Object_t_SByte_t559_Int32_t59_ContractionU26_t4070_ContextU26_t4069,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Object_t_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Object_t_SByte_t559,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_SByte_t559_ByteU5BU5DU26_t4051_Int32U26_t4019,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_ConfidenceFactor_t1912,
	RuntimeInvoker_Sign_t1914_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t1674_SByte_t559,
	RuntimeInvoker_Void_t1856_DSAParameters_t1674,
	RuntimeInvoker_Int16_t561_Object_t_Int32_t59,
	RuntimeInvoker_Double_t60_Object_t_Int32_t59,
	RuntimeInvoker_Object_t_Int16_t561_SByte_t559,
	RuntimeInvoker_Void_t1856_Int32_t59_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Single_t36_Object_t,
	RuntimeInvoker_Boolean_t41_Int32_t59_SByte_t559_MethodBaseU26_t4071_Int32U26_t4019_Int32U26_t4019_StringU26_t4016_Int32U26_t4019_Int32U26_t4019,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Int32_t59_DateTime_t220,
	RuntimeInvoker_DayOfWeek_t2425_DateTime_t220,
	RuntimeInvoker_Int32_t59_Int32U26_t4019_Int32_t59_Int32_t59,
	RuntimeInvoker_DayOfWeek_t2425_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32U26_t4019_Int32U26_t4019_Int32U26_t4019_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_DateTime_t220_DateTime_t220_TimeSpan_t565,
	RuntimeInvoker_TimeSpan_t565,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32U26_t4019,
	RuntimeInvoker_Decimal_t564,
	RuntimeInvoker_SByte_t559,
	RuntimeInvoker_UInt16_t562,
	RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_SByte_t559_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_SByte_t559_Int32_t59,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_MonoIOErrorU26_t4072,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t59_Int32_t59_MonoIOErrorU26_t4072,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t4072,
	RuntimeInvoker_FileAttributes_t2012_Object_t_MonoIOErrorU26_t4072,
	RuntimeInvoker_MonoFileType_t2021_IntPtr_t_MonoIOErrorU26_t4072,
	RuntimeInvoker_Boolean_t41_Object_t_MonoIOStatU26_t4073_MonoIOErrorU26_t4072,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_MonoIOErrorU26_t4072,
	RuntimeInvoker_Boolean_t41_IntPtr_t_MonoIOErrorU26_t4072,
	RuntimeInvoker_Int32_t59_IntPtr_t_Object_t_Int32_t59_Int32_t59_MonoIOErrorU26_t4072,
	RuntimeInvoker_Int64_t507_IntPtr_t_Int64_t507_Int32_t59_MonoIOErrorU26_t4072,
	RuntimeInvoker_Int64_t507_IntPtr_t_MonoIOErrorU26_t4072,
	RuntimeInvoker_Boolean_t41_IntPtr_t_Int64_t507_MonoIOErrorU26_t4072,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_Int32_t59_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t2096,
	RuntimeInvoker_RuntimeMethodHandle_t2467,
	RuntimeInvoker_MethodAttributes_t2107,
	RuntimeInvoker_MethodToken_t2062,
	RuntimeInvoker_FieldAttributes_t2104,
	RuntimeInvoker_RuntimeFieldHandle_t1859,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_OpCode_t2066,
	RuntimeInvoker_Void_t1856_OpCode_t2066_Object_t,
	RuntimeInvoker_StackBehaviour_t2070,
	RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Object_t_Int32_t59_Int32_t59_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_SByte_t559_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t4019_ModuleU26_t4074,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t559_SByte_t559,
	RuntimeInvoker_AssemblyNameFlags_t2090,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_ObjectU5BU5DU26_t4052_Object_t_Object_t_Object_t_ObjectU26_t4075,
	RuntimeInvoker_Void_t1856_ObjectU5BU5DU26_t4052_Object_t,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_ObjectU5BU5DU26_t4052_Object_t,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_EventAttributes_t2102,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t1859,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Int32_t59_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t1439,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t2467,
	RuntimeInvoker_Void_t1856_Object_t_MonoEventInfoU26_t4076,
	RuntimeInvoker_MonoEventInfo_t2111_Object_t,
	RuntimeInvoker_Void_t1856_IntPtr_t_MonoMethodInfoU26_t4077,
	RuntimeInvoker_MonoMethodInfo_t2114_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t2107_IntPtr_t,
	RuntimeInvoker_CallingConventions_t2096_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t4053,
	RuntimeInvoker_Void_t1856_Object_t_MonoPropertyInfoU26_t4078_Int32_t59,
	RuntimeInvoker_PropertyAttributes_t2122,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_Object_t_Object_t_Object_t,
	RuntimeInvoker_ParameterAttributes_t2118,
	RuntimeInvoker_Void_t1856_Int64_t507_ResourceInfoU26_t4079,
	RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Int32_t59,
	RuntimeInvoker_GCHandle_t1139_Object_t_Int32_t59,
	RuntimeInvoker_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Void_t1856_Object_t_Int32_t59_IntPtr_t_Int32_t59,
	RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_IntPtr_t_Object_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Byte_t560_IntPtr_t_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_IntPtr_t_SByte_t559,
	RuntimeInvoker_Void_t1856_IntPtr_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_BooleanU26_t4018,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t4016,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t4016,
	RuntimeInvoker_Void_t1856_SByte_t559_Object_t_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_TimeSpan_t565,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1439_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t1439_ISurrogateSelectorU26_t4080,
	RuntimeInvoker_Void_t1856_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_TimeSpan_t565_Object_t,
	RuntimeInvoker_Object_t_StringU26_t4016,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t4075,
	RuntimeInvoker_Boolean_t41_Object_t_StringU26_t4016_StringU26_t4016,
	RuntimeInvoker_WellKnownObjectMode_t2263,
	RuntimeInvoker_StreamingContext_t1439,
	RuntimeInvoker_TypeFilterLevel_t2279,
	RuntimeInvoker_Void_t1856_Object_t_BooleanU26_t4018,
	RuntimeInvoker_Object_t_Byte_t560_Object_t_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t560_Object_t_SByte_t559_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559_ObjectU26_t4075_HeaderU5BU5DU26_t4081,
	RuntimeInvoker_Void_t1856_Byte_t560_Object_t_SByte_t559_ObjectU26_t4075_HeaderU5BU5DU26_t4081,
	RuntimeInvoker_Boolean_t41_Byte_t560_Object_t,
	RuntimeInvoker_Void_t1856_Byte_t560_Object_t_Int64U26_t4054_ObjectU26_t4075_SerializationInfoU26_t4082,
	RuntimeInvoker_Void_t1856_Object_t_SByte_t559_SByte_t559_Int64U26_t4054_ObjectU26_t4075_SerializationInfoU26_t4082,
	RuntimeInvoker_Void_t1856_Object_t_Int64U26_t4054_ObjectU26_t4075_SerializationInfoU26_t4082,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int64_t507_ObjectU26_t4075_SerializationInfoU26_t4082,
	RuntimeInvoker_Void_t1856_Int64_t507_Object_t_Object_t_Int64_t507_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int64U26_t4054_ObjectU26_t4075,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int64U26_t4054_ObjectU26_t4075,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int64_t507_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int64_t507_Int64_t507_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t507_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t560,
	RuntimeInvoker_Void_t1856_Int64_t507_Int32_t59_Int64_t507,
	RuntimeInvoker_Void_t1856_Int64_t507_Object_t_Int64_t507,
	RuntimeInvoker_Void_t1856_Object_t_Int64_t507_Object_t_Int64_t507_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_SByte_t559_Object_t_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_StreamingContext_t1439,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_StreamingContext_t1439,
	RuntimeInvoker_Void_t1856_StreamingContext_t1439,
	RuntimeInvoker_Object_t_StreamingContext_t1439_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_Int16_t561,
	RuntimeInvoker_Void_t1856_Object_t_DateTime_t220,
	RuntimeInvoker_SerializationEntry_t2296,
	RuntimeInvoker_StreamingContextStates_t2299,
	RuntimeInvoker_CspProviderFlags_t2303,
	RuntimeInvoker_UInt32_t558_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_Int64_t507_Object_t_Int32_t59,
	RuntimeInvoker_UInt32_t558_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_UInt32U26_t4055_Int32_t59_UInt32U26_t4055_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t1856_Int64_t507_Int64_t507,
	RuntimeInvoker_UInt64_t563_Int64_t507_Int32_t59,
	RuntimeInvoker_UInt64_t563_Int64_t507_Int64_t507_Int64_t507,
	RuntimeInvoker_UInt64_t563_Int64_t507,
	RuntimeInvoker_PaddingMode_t1846,
	RuntimeInvoker_Void_t1856_StringBuilderU26_t4083_Int32_t59,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_EncoderFallbackBufferU26_t4084_CharU5BU5DU26_t4085,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_DecoderFallbackBufferU26_t4086,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Boolean_t41_Int16_t561_Int32_t59,
	RuntimeInvoker_Boolean_t41_Int16_t561_Int16_t561_Int32_t59,
	RuntimeInvoker_Void_t1856_Int16_t561_Int16_t561_Int32_t59,
	RuntimeInvoker_Object_t_Int32U26_t4019,
	RuntimeInvoker_Object_t_Int32_t59_Object_t_Int32_t59,
	RuntimeInvoker_Void_t1856_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_SByte_t559_Int32_t59_SByte_t559_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_SByte_t559_Int32U26_t4019_BooleanU26_t4018_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_Int32U26_t4019,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_CharU26_t4048_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_CharU26_t4048_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_CharU26_t4048_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59_CharU26_t4048_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_Object_t_Int64_t507_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_Object_t_Int64_t507_Int32_t59_Object_t_Int32U26_t4019,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Object_t_Int32_t59_UInt32U26_t4055_UInt32U26_t4055_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_SByte_t559,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Int32_t59_UInt32U26_t4055_UInt32U26_t4055_Object_t_DecoderFallbackBufferU26_t4086_ByteU5BU5DU26_t4051_SByte_t559,
	RuntimeInvoker_Void_t1856_Object_t_Object_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Void_t1856_SByte_t559_Int32_t59,
	RuntimeInvoker_IntPtr_t_SByte_t559_Object_t_BooleanU26_t4018,
	RuntimeInvoker_IntPtr_t_SByte_t559_SByte_t559_Object_t_BooleanU26_t4018,
	RuntimeInvoker_Boolean_t41_TimeSpan_t565_TimeSpan_t565,
	RuntimeInvoker_Boolean_t41_Int64_t507_Int64_t507_SByte_t559,
	RuntimeInvoker_Boolean_t41_IntPtr_t_Int32_t59_SByte_t559,
	RuntimeInvoker_Int64_t507_Double_t60,
	RuntimeInvoker_Object_t_Double_t60,
	RuntimeInvoker_Int64_t507_Object_t_Int32_t59,
	RuntimeInvoker_UInt16_t562_Object_t_Int32_t59,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t59_Int32_t59,
	RuntimeInvoker_Byte_t560_SByte_t559,
	RuntimeInvoker_Byte_t560_Double_t60,
	RuntimeInvoker_Byte_t560_Single_t36,
	RuntimeInvoker_Byte_t560_Int64_t507,
	RuntimeInvoker_Char_t556_SByte_t559,
	RuntimeInvoker_Char_t556_Int64_t507,
	RuntimeInvoker_Char_t556_Single_t36,
	RuntimeInvoker_Char_t556_Object_t_Object_t,
	RuntimeInvoker_DateTime_t220_Object_t_Object_t,
	RuntimeInvoker_DateTime_t220_Int16_t561,
	RuntimeInvoker_DateTime_t220_Int64_t507,
	RuntimeInvoker_DateTime_t220_Single_t36,
	RuntimeInvoker_DateTime_t220_SByte_t559,
	RuntimeInvoker_Double_t60_SByte_t559,
	RuntimeInvoker_Double_t60_Double_t60,
	RuntimeInvoker_Double_t60_Single_t36,
	RuntimeInvoker_Double_t60_Int32_t59,
	RuntimeInvoker_Double_t60_Int64_t507,
	RuntimeInvoker_Double_t60_Int16_t561,
	RuntimeInvoker_Int16_t561_SByte_t559,
	RuntimeInvoker_Int16_t561_Double_t60,
	RuntimeInvoker_Int16_t561_Single_t36,
	RuntimeInvoker_Int16_t561_Int32_t59,
	RuntimeInvoker_Int16_t561_Int64_t507,
	RuntimeInvoker_Int64_t507_SByte_t559,
	RuntimeInvoker_Int64_t507_Int16_t561,
	RuntimeInvoker_Int64_t507_Single_t36,
	RuntimeInvoker_Int64_t507_Int64_t507,
	RuntimeInvoker_SByte_t559_SByte_t559,
	RuntimeInvoker_SByte_t559_Int16_t561,
	RuntimeInvoker_SByte_t559_Double_t60,
	RuntimeInvoker_SByte_t559_Single_t36,
	RuntimeInvoker_SByte_t559_Int32_t59,
	RuntimeInvoker_SByte_t559_Int64_t507,
	RuntimeInvoker_Single_t36_SByte_t559,
	RuntimeInvoker_Single_t36_Double_t60,
	RuntimeInvoker_Single_t36_Int64_t507,
	RuntimeInvoker_Single_t36_Int16_t561,
	RuntimeInvoker_UInt16_t562_SByte_t559,
	RuntimeInvoker_UInt16_t562_Double_t60,
	RuntimeInvoker_UInt16_t562_Single_t36,
	RuntimeInvoker_UInt16_t562_Int32_t59,
	RuntimeInvoker_UInt16_t562_Int64_t507,
	RuntimeInvoker_UInt32_t558_SByte_t559,
	RuntimeInvoker_UInt32_t558_Int16_t561,
	RuntimeInvoker_UInt32_t558_Double_t60,
	RuntimeInvoker_UInt32_t558_Single_t36,
	RuntimeInvoker_UInt32_t558_Int64_t507,
	RuntimeInvoker_UInt64_t563_SByte_t559,
	RuntimeInvoker_UInt64_t563_Int16_t561,
	RuntimeInvoker_UInt64_t563_Double_t60,
	RuntimeInvoker_UInt64_t563_Single_t36,
	RuntimeInvoker_UInt64_t563_Int32_t59,
	RuntimeInvoker_Void_t1856_ByteU5BU5DU26_t4051,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_SByte_t559_TimeSpan_t565,
	RuntimeInvoker_Void_t1856_Int64_t507_Int32_t59,
	RuntimeInvoker_DayOfWeek_t2425,
	RuntimeInvoker_DateTimeKind_t2422,
	RuntimeInvoker_DateTime_t220_TimeSpan_t565,
	RuntimeInvoker_DateTime_t220_Double_t60,
	RuntimeInvoker_Int32_t59_DateTime_t220_DateTime_t220,
	RuntimeInvoker_DateTime_t220_DateTime_t220_Int32_t59,
	RuntimeInvoker_DateTime_t220_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int32_t59_DateTimeU26_t4087_DateTimeOffsetU26_t4088_SByte_t559_ExceptionU26_t4053,
	RuntimeInvoker_Object_t_Object_t_SByte_t559_ExceptionU26_t4053,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_SByte_t559_SByte_t559_Int32U26_t4019,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Object_t_Object_t_SByte_t559_Int32U26_t4019,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Int32_t59_Object_t_Int32U26_t4019,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Int32_t59_Object_t_SByte_t559_Int32U26_t4019_Int32U26_t4019,
	RuntimeInvoker_Boolean_t41_Object_t_Int32_t59_Object_t_SByte_t559_Int32U26_t4019,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_SByte_t559_DateTimeU26_t4087_DateTimeOffsetU26_t4088_Object_t_Int32_t59_SByte_t559_BooleanU26_t4018_BooleanU26_t4018,
	RuntimeInvoker_DateTime_t220_Object_t_Object_t_Object_t_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Object_t_Int32_t59_DateTimeU26_t4087_SByte_t559_BooleanU26_t4018_SByte_t559_ExceptionU26_t4053,
	RuntimeInvoker_DateTime_t220_DateTime_t220_TimeSpan_t565,
	RuntimeInvoker_Boolean_t41_DateTime_t220_DateTime_t220,
	RuntimeInvoker_TimeSpan_t565_DateTime_t220_DateTime_t220,
	RuntimeInvoker_Void_t1856_DateTime_t220_TimeSpan_t565,
	RuntimeInvoker_Void_t1856_Int64_t507_TimeSpan_t565,
	RuntimeInvoker_Int32_t59_DateTimeOffset_t2423,
	RuntimeInvoker_Boolean_t41_DateTimeOffset_t2423,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int16_t561,
	RuntimeInvoker_Object_t_Int16_t561_Object_t_BooleanU26_t4018_BooleanU26_t4018,
	RuntimeInvoker_Object_t_Int16_t561_Object_t_BooleanU26_t4018_BooleanU26_t4018_SByte_t559,
	RuntimeInvoker_Object_t_DateTime_t220_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t220_Nullable_1_t2528_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_MonoEnumInfo_t2436,
	RuntimeInvoker_Void_t1856_Object_t_MonoEnumInfoU26_t4089,
	RuntimeInvoker_Int32_t59_Int16_t561_Int16_t561,
	RuntimeInvoker_Int32_t59_Int64_t507_Int64_t507,
	RuntimeInvoker_PlatformID_t2464,
	RuntimeInvoker_Void_t1856_Int32_t59_Int16_t561_Int16_t561_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Int32_t59_Guid_t61,
	RuntimeInvoker_Boolean_t41_Guid_t61,
	RuntimeInvoker_Guid_t61,
	RuntimeInvoker_Object_t_SByte_t559_SByte_t559_SByte_t559,
	RuntimeInvoker_Double_t60_Double_t60_Double_t60,
	RuntimeInvoker_TypeAttributes_t2126_Object_t,
	RuntimeInvoker_Object_t_SByte_t559_SByte_t559,
	RuntimeInvoker_Void_t1856_UInt64U2AU26_t4090_Int32U2AU26_t4091_CharU2AU26_t4092_CharU2AU26_t4092_Int64U2AU26_t4093_Int32U2AU26_t4091,
	RuntimeInvoker_Void_t1856_Int32_t59_Int64_t507,
	RuntimeInvoker_Void_t1856_Object_t_Double_t60_Int32_t59,
	RuntimeInvoker_Void_t1856_Object_t_Decimal_t564,
	RuntimeInvoker_Object_t_Object_t_SByte_t559_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t561_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t507_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t36_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t60_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t564_Object_t,
	RuntimeInvoker_Object_t_Single_t36_Object_t,
	RuntimeInvoker_Object_t_Double_t60_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_BooleanU26_t4018_SByte_t559_Int32U26_t4019_Int32U26_t4019,
	RuntimeInvoker_Object_t_Object_t_Int32_t59_Int32_t59_Object_t_SByte_t559_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_Int64_t507_Int32_t59_Int32_t59_Int32_t59_Int32_t59_Int32_t59,
	RuntimeInvoker_TimeSpan_t565_TimeSpan_t565,
	RuntimeInvoker_Int32_t59_TimeSpan_t565_TimeSpan_t565,
	RuntimeInvoker_Int32_t59_TimeSpan_t565,
	RuntimeInvoker_Boolean_t41_TimeSpan_t565,
	RuntimeInvoker_TimeSpan_t565_Double_t60,
	RuntimeInvoker_TimeSpan_t565_Double_t60_Int64_t507,
	RuntimeInvoker_TimeSpan_t565_TimeSpan_t565_TimeSpan_t565,
	RuntimeInvoker_TimeSpan_t565_DateTime_t220,
	RuntimeInvoker_Boolean_t41_DateTime_t220_Object_t,
	RuntimeInvoker_DateTime_t220_DateTime_t220,
	RuntimeInvoker_TimeSpan_t565_DateTime_t220_TimeSpan_t565,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int64U5BU5DU26_t4094_StringU5BU5DU26_t4095,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_ObjectU26_t4075,
	RuntimeInvoker_Boolean_t41_ObjectU26_t4075_Object_t,
	RuntimeInvoker_Void_t1856_ObjectU26_t4075_Object_t,
	RuntimeInvoker_Enumerator_t3474,
	RuntimeInvoker_Enumerator_t2871,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Enumerator_t3342,
	RuntimeInvoker_Void_t1856_Int32_t59_ObjectU26_t4075,
	RuntimeInvoker_Void_t1856_ObjectU5BU5DU26_t4052_Int32_t59,
	RuntimeInvoker_Void_t1856_ObjectU5BU5DU26_t4052_Int32_t59_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t2634,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2634,
	RuntimeInvoker_KeyValuePair_2_t2634_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_ObjectU26_t4075,
	RuntimeInvoker_Enumerator_t2638,
	RuntimeInvoker_DictionaryEntry_t58_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2634,
	RuntimeInvoker_Enumerator_t2637,
	RuntimeInvoker_Enumerator_t2641,
	RuntimeInvoker_Int32_t59_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Enumerator_t552,
	RuntimeInvoker_Enumerator_t2662,
	RuntimeInvoker_KeyValuePair_2_t2658,
	RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t467,
	RuntimeInvoker_OGActionType_t265,
	RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t466,
	RuntimeInvoker_Void_t1856_Object_t_Nullable_1_t474,
	RuntimeInvoker_Int64_t507_Object_t_Object_t_SByte_t559,
	RuntimeInvoker_Enumerator_t2791,
	RuntimeInvoker_KeyValuePair_2_t2787,
	RuntimeInvoker_Enumerator_t2665,
	RuntimeInvoker_Void_t1856_ColorTween_t670,
	RuntimeInvoker_Boolean_t41_TypeU26_t4096_Int32_t59,
	RuntimeInvoker_Boolean_t41_BooleanU26_t4018_SByte_t559,
	RuntimeInvoker_Boolean_t41_FillMethodU26_t4097_Int32_t59,
	RuntimeInvoker_Boolean_t41_SingleU26_t4023_Single_t36,
	RuntimeInvoker_Boolean_t41_ContentTypeU26_t4098_Int32_t59,
	RuntimeInvoker_Boolean_t41_LineTypeU26_t4099_Int32_t59,
	RuntimeInvoker_Boolean_t41_InputTypeU26_t4100_Int32_t59,
	RuntimeInvoker_Boolean_t41_TouchScreenKeyboardTypeU26_t4101_Int32_t59,
	RuntimeInvoker_Boolean_t41_CharacterValidationU26_t4102_Int32_t59,
	RuntimeInvoker_Boolean_t41_CharU26_t4048_Int16_t561,
	RuntimeInvoker_Boolean_t41_DirectionU26_t4103_Int32_t59,
	RuntimeInvoker_Boolean_t41_NavigationU26_t4104_Navigation_t730,
	RuntimeInvoker_Boolean_t41_TransitionU26_t4105_Int32_t59,
	RuntimeInvoker_Boolean_t41_ColorBlockU26_t4106_ColorBlock_t682,
	RuntimeInvoker_Boolean_t41_SpriteStateU26_t4107_SpriteState_t745,
	RuntimeInvoker_Boolean_t41_DirectionU26_t4108_Int32_t59,
	RuntimeInvoker_Boolean_t41_AspectModeU26_t4109_Int32_t59,
	RuntimeInvoker_Boolean_t41_FitModeU26_t4110_Int32_t59,
	RuntimeInvoker_Void_t1856_CornerU26_t4111_Int32_t59,
	RuntimeInvoker_Void_t1856_AxisU26_t4112_Int32_t59,
	RuntimeInvoker_Void_t1856_Vector2U26_t4032_Vector2_t29,
	RuntimeInvoker_Void_t1856_ConstraintU26_t4113_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32U26_t4019_Int32_t59,
	RuntimeInvoker_Void_t1856_SingleU26_t4023_Single_t36,
	RuntimeInvoker_Void_t1856_BooleanU26_t4018_SByte_t559,
	RuntimeInvoker_Void_t1856_TextAnchorU26_t4114_Int32_t59,
	RuntimeInvoker_Enumerator_t1150,
	RuntimeInvoker_Enumerator_t3068,
	RuntimeInvoker_Void_t1856_Int32_t59_Double_t60,
	RuntimeInvoker_Vector3_t6_Int32_t59,
	RuntimeInvoker_Int32_t59_Vector3_t6,
	RuntimeInvoker_Void_t1856_Int32_t59_Vector3_t6,
	RuntimeInvoker_Void_t1856_Int32_t59_Vector2_t29,
	RuntimeInvoker_Boolean_t41_Color_t5,
	RuntimeInvoker_Int32_t59_Color_t5,
	RuntimeInvoker_Int32_t59_Rect_t30,
	RuntimeInvoker_KeyValuePair_2_t2607_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t2607,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2607,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t2607,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2607,
	RuntimeInvoker_Link_t1956_Int32_t59,
	RuntimeInvoker_Void_t1856_Link_t1956,
	RuntimeInvoker_Boolean_t41_Link_t1956,
	RuntimeInvoker_Int32_t59_Link_t1956,
	RuntimeInvoker_Void_t1856_Int32_t59_Link_t1956,
	RuntimeInvoker_DictionaryEntry_t58_Int32_t59,
	RuntimeInvoker_Void_t1856_DictionaryEntry_t58,
	RuntimeInvoker_Boolean_t41_DictionaryEntry_t58,
	RuntimeInvoker_Int32_t59_DictionaryEntry_t58,
	RuntimeInvoker_Void_t1856_Int32_t59_DictionaryEntry_t58,
	RuntimeInvoker_KeyValuePair_2_t2634_Int32_t59,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t2634,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2634,
	RuntimeInvoker_KeyValuePair_2_t2658_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t2658,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2658,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t2658,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2658,
	RuntimeInvoker_Void_t1856_ByteU5BU5DU26_t4051_Int32_t59,
	RuntimeInvoker_Void_t1856_ByteU5BU5DU26_t4051_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_SByte_t559_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_SByte_t559_SByte_t559_Object_t,
	RuntimeInvoker_Boolean_t41_Object_t_Object_t_Int64U26_t4054,
	RuntimeInvoker_KeyValuePair_2_t2787_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t2787,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2787,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t2787,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2787,
	RuntimeInvoker_KeyValuePair_2_t2811_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t2811,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t2811,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t2811,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t2811,
	RuntimeInvoker_WebCamDevice_t1219_Int32_t59,
	RuntimeInvoker_Void_t1856_WebCamDevice_t1219,
	RuntimeInvoker_Boolean_t41_WebCamDevice_t1219,
	RuntimeInvoker_Int32_t59_WebCamDevice_t1219,
	RuntimeInvoker_Void_t1856_Int32_t59_WebCamDevice_t1219,
	RuntimeInvoker_RaycastResult_t647_Int32_t59,
	RuntimeInvoker_Boolean_t41_RaycastResult_t647,
	RuntimeInvoker_Int32_t59_RaycastResult_t647,
	RuntimeInvoker_Void_t1856_Int32_t59_RaycastResult_t647,
	RuntimeInvoker_Void_t1856_RaycastResultU5BU5DU26_t4115_Int32_t59,
	RuntimeInvoker_Void_t1856_RaycastResultU5BU5DU26_t4115_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_RaycastResult_t647_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_RaycastResult_t647_RaycastResult_t647_Object_t,
	RuntimeInvoker_RaycastHit2D_t838_Int32_t59,
	RuntimeInvoker_Void_t1856_RaycastHit2D_t838,
	RuntimeInvoker_Boolean_t41_RaycastHit2D_t838,
	RuntimeInvoker_Int32_t59_RaycastHit2D_t838,
	RuntimeInvoker_Void_t1856_Int32_t59_RaycastHit2D_t838,
	RuntimeInvoker_RaycastHit_t566_Int32_t59,
	RuntimeInvoker_Void_t1856_RaycastHit_t566,
	RuntimeInvoker_Boolean_t41_RaycastHit_t566,
	RuntimeInvoker_Int32_t59_RaycastHit_t566,
	RuntimeInvoker_Void_t1856_Int32_t59_RaycastHit_t566,
	RuntimeInvoker_Void_t1856_UIVertexU5BU5DU26_t4116_Int32_t59,
	RuntimeInvoker_Void_t1856_UIVertexU5BU5DU26_t4116_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_UIVertex_t727_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_UIVertex_t727_UIVertex_t727_Object_t,
	RuntimeInvoker_ContentType_t710_Int32_t59,
	RuntimeInvoker_UILineInfo_t857_Int32_t59,
	RuntimeInvoker_Void_t1856_UILineInfo_t857,
	RuntimeInvoker_Boolean_t41_UILineInfo_t857,
	RuntimeInvoker_Int32_t59_UILineInfo_t857,
	RuntimeInvoker_Void_t1856_Int32_t59_UILineInfo_t857,
	RuntimeInvoker_UICharInfo_t859_Int32_t59,
	RuntimeInvoker_Void_t1856_UICharInfo_t859,
	RuntimeInvoker_Boolean_t41_UICharInfo_t859,
	RuntimeInvoker_Int32_t59_UICharInfo_t859,
	RuntimeInvoker_Void_t1856_Int32_t59_UICharInfo_t859,
	RuntimeInvoker_EyewearCalibrationReading_t913_Int32_t59,
	RuntimeInvoker_Void_t1856_EyewearCalibrationReading_t913,
	RuntimeInvoker_Boolean_t41_EyewearCalibrationReading_t913,
	RuntimeInvoker_Int32_t59_EyewearCalibrationReading_t913,
	RuntimeInvoker_Void_t1856_Int32_t59_EyewearCalibrationReading_t913,
	RuntimeInvoker_TargetSearchResult_t1050_Int32_t59,
	RuntimeInvoker_Boolean_t41_TargetSearchResult_t1050,
	RuntimeInvoker_Int32_t59_TargetSearchResult_t1050,
	RuntimeInvoker_Void_t1856_Int32_t59_TargetSearchResult_t1050,
	RuntimeInvoker_KeyValuePair_2_t3060_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t3060,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3060,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t3060,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3060,
	RuntimeInvoker_PIXEL_FORMAT_t942_Int32_t59,
	RuntimeInvoker_Void_t1856_PIXEL_FORMATU5BU5DU26_t4117_Int32_t59,
	RuntimeInvoker_Void_t1856_PIXEL_FORMATU5BU5DU26_t4117_Int32_t59_Int32_t59,
	RuntimeInvoker_Color32_t829_Int32_t59,
	RuntimeInvoker_Void_t1856_Color32_t829,
	RuntimeInvoker_Boolean_t41_Color32_t829,
	RuntimeInvoker_Int32_t59_Color32_t829,
	RuntimeInvoker_Void_t1856_Int32_t59_Color32_t829,
	RuntimeInvoker_TrackableResultData_t969_Int32_t59,
	RuntimeInvoker_Void_t1856_TrackableResultData_t969,
	RuntimeInvoker_Int32_t59_TrackableResultData_t969,
	RuntimeInvoker_Void_t1856_Int32_t59_TrackableResultData_t969,
	RuntimeInvoker_WordData_t974_Int32_t59,
	RuntimeInvoker_Void_t1856_WordData_t974,
	RuntimeInvoker_Boolean_t41_WordData_t974,
	RuntimeInvoker_Int32_t59_WordData_t974,
	RuntimeInvoker_Void_t1856_Int32_t59_WordData_t974,
	RuntimeInvoker_WordResultData_t973_Int32_t59,
	RuntimeInvoker_Void_t1856_WordResultData_t973,
	RuntimeInvoker_Boolean_t41_WordResultData_t973,
	RuntimeInvoker_Int32_t59_WordResultData_t973,
	RuntimeInvoker_Void_t1856_Int32_t59_WordResultData_t973,
	RuntimeInvoker_Void_t1856_Int32U5BU5DU26_t4118_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32U5BU5DU26_t4118_Int32_t59_Int32_t59,
	RuntimeInvoker_SmartTerrainRevisionData_t977_Int32_t59,
	RuntimeInvoker_Void_t1856_SmartTerrainRevisionData_t977,
	RuntimeInvoker_Boolean_t41_SmartTerrainRevisionData_t977,
	RuntimeInvoker_Int32_t59_SmartTerrainRevisionData_t977,
	RuntimeInvoker_Void_t1856_Int32_t59_SmartTerrainRevisionData_t977,
	RuntimeInvoker_SurfaceData_t978_Int32_t59,
	RuntimeInvoker_Void_t1856_SurfaceData_t978,
	RuntimeInvoker_Boolean_t41_SurfaceData_t978,
	RuntimeInvoker_Int32_t59_SurfaceData_t978,
	RuntimeInvoker_Void_t1856_Int32_t59_SurfaceData_t978,
	RuntimeInvoker_PropData_t979_Int32_t59,
	RuntimeInvoker_Void_t1856_PropData_t979,
	RuntimeInvoker_Boolean_t41_PropData_t979,
	RuntimeInvoker_Int32_t59_PropData_t979,
	RuntimeInvoker_Void_t1856_Int32_t59_PropData_t979,
	RuntimeInvoker_KeyValuePair_2_t3151_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t3151,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3151,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t3151,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3151,
	RuntimeInvoker_RectangleData_t923_Int32_t59,
	RuntimeInvoker_Void_t1856_RectangleData_t923,
	RuntimeInvoker_Int32_t59_RectangleData_t923,
	RuntimeInvoker_Void_t1856_Int32_t59_RectangleData_t923,
	RuntimeInvoker_KeyValuePair_2_t3240_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t3240,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3240,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t3240,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3240,
	RuntimeInvoker_KeyValuePair_2_t3255_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t3255,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3255,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t3255,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3255,
	RuntimeInvoker_VirtualButtonData_t970_Int32_t59,
	RuntimeInvoker_Void_t1856_VirtualButtonData_t970,
	RuntimeInvoker_Boolean_t41_VirtualButtonData_t970,
	RuntimeInvoker_Int32_t59_VirtualButtonData_t970,
	RuntimeInvoker_Void_t1856_Int32_t59_VirtualButtonData_t970,
	RuntimeInvoker_Void_t1856_TargetSearchResultU5BU5DU26_t4119_Int32_t59,
	RuntimeInvoker_Void_t1856_TargetSearchResultU5BU5DU26_t4119_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_TargetSearchResult_t1050_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_TargetSearchResult_t1050_TargetSearchResult_t1050_Object_t,
	RuntimeInvoker_KeyValuePair_2_t3292_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t3292,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3292,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t3292,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3292,
	RuntimeInvoker_ProfileData_t1064_Int32_t59,
	RuntimeInvoker_Void_t1856_ProfileData_t1064,
	RuntimeInvoker_Boolean_t41_ProfileData_t1064,
	RuntimeInvoker_Int32_t59_ProfileData_t1064,
	RuntimeInvoker_Void_t1856_Int32_t59_ProfileData_t1064,
	RuntimeInvoker_Link_t3340_Int32_t59,
	RuntimeInvoker_Void_t1856_Link_t3340,
	RuntimeInvoker_Boolean_t41_Link_t3340,
	RuntimeInvoker_Int32_t59_Link_t3340,
	RuntimeInvoker_Void_t1856_Int32_t59_Link_t3340,
	RuntimeInvoker_ParameterModifier_t2119_Int32_t59,
	RuntimeInvoker_Void_t1856_ParameterModifier_t2119,
	RuntimeInvoker_Boolean_t41_ParameterModifier_t2119,
	RuntimeInvoker_Int32_t59_ParameterModifier_t2119,
	RuntimeInvoker_Void_t1856_Int32_t59_ParameterModifier_t2119,
	RuntimeInvoker_GcAchievementData_t1244_Int32_t59,
	RuntimeInvoker_Void_t1856_GcAchievementData_t1244,
	RuntimeInvoker_Boolean_t41_GcAchievementData_t1244,
	RuntimeInvoker_Int32_t59_GcAchievementData_t1244,
	RuntimeInvoker_Void_t1856_Int32_t59_GcAchievementData_t1244,
	RuntimeInvoker_GcScoreData_t1245_Int32_t59,
	RuntimeInvoker_Boolean_t41_GcScoreData_t1245,
	RuntimeInvoker_Int32_t59_GcScoreData_t1245,
	RuntimeInvoker_Void_t1856_Int32_t59_GcScoreData_t1245,
	RuntimeInvoker_HitInfo_t1271_Int32_t59,
	RuntimeInvoker_Void_t1856_HitInfo_t1271,
	RuntimeInvoker_Int32_t59_HitInfo_t1271,
	RuntimeInvoker_KeyValuePair_2_t3373_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t3373,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3373,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t3373,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3373,
	RuntimeInvoker_TextEditOp_t1286_Int32_t59,
	RuntimeInvoker_Void_t1856_UICharInfoU5BU5DU26_t4120_Int32_t59,
	RuntimeInvoker_Void_t1856_UICharInfoU5BU5DU26_t4120_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_UICharInfo_t859_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_UICharInfo_t859_UICharInfo_t859_Object_t,
	RuntimeInvoker_Void_t1856_UILineInfoU5BU5DU26_t4121_Int32_t59,
	RuntimeInvoker_Void_t1856_UILineInfoU5BU5DU26_t4121_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_UILineInfo_t857_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_UILineInfo_t857_UILineInfo_t857_Object_t,
	RuntimeInvoker_Keyframe_t1321_Int32_t59,
	RuntimeInvoker_Void_t1856_Keyframe_t1321,
	RuntimeInvoker_Boolean_t41_Keyframe_t1321,
	RuntimeInvoker_Int32_t59_Keyframe_t1321,
	RuntimeInvoker_Void_t1856_Int32_t59_Keyframe_t1321,
	RuntimeInvoker_KeyValuePair_2_t3478_Int32_t59,
	RuntimeInvoker_Void_t1856_KeyValuePair_2_t3478,
	RuntimeInvoker_Boolean_t41_KeyValuePair_2_t3478,
	RuntimeInvoker_Int32_t59_KeyValuePair_2_t3478,
	RuntimeInvoker_Void_t1856_Int32_t59_KeyValuePair_2_t3478,
	RuntimeInvoker_X509ChainStatus_t1558_Int32_t59,
	RuntimeInvoker_Void_t1856_X509ChainStatus_t1558,
	RuntimeInvoker_Boolean_t41_X509ChainStatus_t1558,
	RuntimeInvoker_Int32_t59_X509ChainStatus_t1558,
	RuntimeInvoker_Void_t1856_Int32_t59_X509ChainStatus_t1558,
	RuntimeInvoker_Int32_t59_Object_t_Int32_t59_Int32_t59_Int32_t59_Object_t,
	RuntimeInvoker_Mark_t1605_Int32_t59,
	RuntimeInvoker_Void_t1856_Mark_t1605,
	RuntimeInvoker_Boolean_t41_Mark_t1605,
	RuntimeInvoker_Int32_t59_Mark_t1605,
	RuntimeInvoker_Void_t1856_Int32_t59_Mark_t1605,
	RuntimeInvoker_UriScheme_t1641_Int32_t59,
	RuntimeInvoker_Void_t1856_UriScheme_t1641,
	RuntimeInvoker_Boolean_t41_UriScheme_t1641,
	RuntimeInvoker_Int32_t59_UriScheme_t1641,
	RuntimeInvoker_Void_t1856_Int32_t59_UriScheme_t1641,
	RuntimeInvoker_ClientCertificateType_t1784_Int32_t59,
	RuntimeInvoker_TableRange_t1889_Int32_t59,
	RuntimeInvoker_Void_t1856_TableRange_t1889,
	RuntimeInvoker_Boolean_t41_TableRange_t1889,
	RuntimeInvoker_Int32_t59_TableRange_t1889,
	RuntimeInvoker_Void_t1856_Int32_t59_TableRange_t1889,
	RuntimeInvoker_Slot_t1966_Int32_t59,
	RuntimeInvoker_Void_t1856_Slot_t1966,
	RuntimeInvoker_Boolean_t41_Slot_t1966,
	RuntimeInvoker_Int32_t59_Slot_t1966,
	RuntimeInvoker_Void_t1856_Int32_t59_Slot_t1966,
	RuntimeInvoker_Slot_t1974_Int32_t59,
	RuntimeInvoker_Void_t1856_Slot_t1974,
	RuntimeInvoker_Boolean_t41_Slot_t1974,
	RuntimeInvoker_Int32_t59_Slot_t1974,
	RuntimeInvoker_Void_t1856_Int32_t59_Slot_t1974,
	RuntimeInvoker_ILTokenInfo_t2053_Int32_t59,
	RuntimeInvoker_Void_t1856_ILTokenInfo_t2053,
	RuntimeInvoker_Boolean_t41_ILTokenInfo_t2053,
	RuntimeInvoker_Int32_t59_ILTokenInfo_t2053,
	RuntimeInvoker_Void_t1856_Int32_t59_ILTokenInfo_t2053,
	RuntimeInvoker_LabelData_t2055_Int32_t59,
	RuntimeInvoker_Void_t1856_LabelData_t2055,
	RuntimeInvoker_Boolean_t41_LabelData_t2055,
	RuntimeInvoker_Int32_t59_LabelData_t2055,
	RuntimeInvoker_Void_t1856_Int32_t59_LabelData_t2055,
	RuntimeInvoker_LabelFixup_t2054_Int32_t59,
	RuntimeInvoker_Void_t1856_LabelFixup_t2054,
	RuntimeInvoker_Boolean_t41_LabelFixup_t2054,
	RuntimeInvoker_Int32_t59_LabelFixup_t2054,
	RuntimeInvoker_Void_t1856_Int32_t59_LabelFixup_t2054,
	RuntimeInvoker_CustomAttributeTypedArgument_t2101_Int32_t59,
	RuntimeInvoker_Void_t1856_CustomAttributeTypedArgument_t2101,
	RuntimeInvoker_Boolean_t41_CustomAttributeTypedArgument_t2101,
	RuntimeInvoker_Int32_t59_CustomAttributeTypedArgument_t2101,
	RuntimeInvoker_Void_t1856_Int32_t59_CustomAttributeTypedArgument_t2101,
	RuntimeInvoker_CustomAttributeNamedArgument_t2100_Int32_t59,
	RuntimeInvoker_Void_t1856_CustomAttributeNamedArgument_t2100,
	RuntimeInvoker_Boolean_t41_CustomAttributeNamedArgument_t2100,
	RuntimeInvoker_Int32_t59_CustomAttributeNamedArgument_t2100,
	RuntimeInvoker_Void_t1856_Int32_t59_CustomAttributeNamedArgument_t2100,
	RuntimeInvoker_Void_t1856_CustomAttributeTypedArgumentU5BU5DU26_t4122_Int32_t59,
	RuntimeInvoker_Void_t1856_CustomAttributeTypedArgumentU5BU5DU26_t4122_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_CustomAttributeTypedArgument_t2101_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_CustomAttributeTypedArgument_t2101_CustomAttributeTypedArgument_t2101_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_CustomAttributeTypedArgument_t2101,
	RuntimeInvoker_Void_t1856_CustomAttributeNamedArgumentU5BU5DU26_t4123_Int32_t59,
	RuntimeInvoker_Void_t1856_CustomAttributeNamedArgumentU5BU5DU26_t4123_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_Object_t_CustomAttributeNamedArgument_t2100_Int32_t59_Int32_t59,
	RuntimeInvoker_Int32_t59_CustomAttributeNamedArgument_t2100_CustomAttributeNamedArgument_t2100_Object_t,
	RuntimeInvoker_Int32_t59_Object_t_CustomAttributeNamedArgument_t2100,
	RuntimeInvoker_ResourceInfo_t2130_Int32_t59,
	RuntimeInvoker_Void_t1856_ResourceInfo_t2130,
	RuntimeInvoker_Boolean_t41_ResourceInfo_t2130,
	RuntimeInvoker_Int32_t59_ResourceInfo_t2130,
	RuntimeInvoker_Void_t1856_Int32_t59_ResourceInfo_t2130,
	RuntimeInvoker_ResourceCacheItem_t2131_Int32_t59,
	RuntimeInvoker_Void_t1856_ResourceCacheItem_t2131,
	RuntimeInvoker_Boolean_t41_ResourceCacheItem_t2131,
	RuntimeInvoker_Int32_t59_ResourceCacheItem_t2131,
	RuntimeInvoker_Void_t1856_Int32_t59_ResourceCacheItem_t2131,
	RuntimeInvoker_Void_t1856_Int32_t59_DateTime_t220,
	RuntimeInvoker_Void_t1856_Decimal_t564,
	RuntimeInvoker_Void_t1856_Int32_t59_Decimal_t564,
	RuntimeInvoker_TimeSpan_t565_Int32_t59,
	RuntimeInvoker_Void_t1856_Int32_t59_TimeSpan_t565,
	RuntimeInvoker_TypeTag_t2267_Int32_t59,
	RuntimeInvoker_Boolean_t41_Byte_t560,
	RuntimeInvoker_Int32_t59_Byte_t560,
	RuntimeInvoker_Void_t1856_Int32_t59_Byte_t560,
	RuntimeInvoker_KeyValuePair_2_t2607_Object_t_Int32_t59,
	RuntimeInvoker_Enumerator_t2613,
	RuntimeInvoker_DictionaryEntry_t58_Object_t_Int32_t59,
	RuntimeInvoker_KeyValuePair_2_t2607,
	RuntimeInvoker_Link_t1956,
	RuntimeInvoker_Enumerator_t2612,
	RuntimeInvoker_Enumerator_t2616,
	RuntimeInvoker_DictionaryEntry_t58_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2607_Object_t,
	RuntimeInvoker_Boolean_t41_Nullable_1_t48,
	RuntimeInvoker_KeyValuePair_2_t2634_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2658_Int32_t59_Object_t,
	RuntimeInvoker_Boolean_t41_Int32_t59_ObjectU26_t4075,
	RuntimeInvoker_DictionaryEntry_t58_Int32_t59_Object_t,
	RuntimeInvoker_Enumerator_t2661,
	RuntimeInvoker_KeyValuePair_2_t2658_Object_t,
	RuntimeInvoker_Enumerator_t2709,
	RuntimeInvoker_Object_t_SByte_t559_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_Boolean_t41_Nullable_1_t466,
	RuntimeInvoker_Boolean_t41_Nullable_1_t467,
	RuntimeInvoker_Boolean_t41_Nullable_1_t474,
	RuntimeInvoker_Boolean_t41_Nullable_1_t555,
	RuntimeInvoker_KeyValuePair_2_t2787_Object_t_Single_t36,
	RuntimeInvoker_Object_t_Object_t_Single_t36,
	RuntimeInvoker_Boolean_t41_Object_t_SingleU26_t4023,
	RuntimeInvoker_DictionaryEntry_t58_Object_t_Single_t36,
	RuntimeInvoker_Enumerator_t2790,
	RuntimeInvoker_Object_t_Object_t_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2794,
	RuntimeInvoker_KeyValuePair_2_t2787_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2811_Int32_t59_Int32_t59,
	RuntimeInvoker_Boolean_t41_Int32_t59_Int32U26_t4019,
	RuntimeInvoker_Enumerator_t2815,
	RuntimeInvoker_DictionaryEntry_t58_Int32_t59_Int32_t59,
	RuntimeInvoker_KeyValuePair_2_t2811,
	RuntimeInvoker_Enumerator_t2814,
	RuntimeInvoker_Enumerator_t2818,
	RuntimeInvoker_KeyValuePair_2_t2811_Object_t,
	RuntimeInvoker_WebCamDevice_t1219,
	RuntimeInvoker_Object_t_SByte_t559_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_RaycastResult_t647_RaycastResult_t647_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2879,
	RuntimeInvoker_Boolean_t41_RaycastResult_t647_RaycastResult_t647,
	RuntimeInvoker_Object_t_RaycastResult_t647_Object_t_Object_t,
	RuntimeInvoker_RaycastHit2D_t838,
	RuntimeInvoker_Object_t_RaycastHit_t566_RaycastHit_t566_Object_t_Object_t,
	RuntimeInvoker_RaycastHit_t566,
	RuntimeInvoker_Object_t_Color_t5_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_UIVertex_t727,
	RuntimeInvoker_Boolean_t41_UIVertex_t727,
	RuntimeInvoker_UIVertex_t727_Object_t,
	RuntimeInvoker_Enumerator_t2941,
	RuntimeInvoker_Int32_t59_UIVertex_t727,
	RuntimeInvoker_Void_t1856_Int32_t59_UIVertex_t727,
	RuntimeInvoker_UIVertex_t727_Int32_t59,
	RuntimeInvoker_UIVertex_t727,
	RuntimeInvoker_Boolean_t41_UIVertex_t727_UIVertex_t727,
	RuntimeInvoker_Object_t_UIVertex_t727_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_UIVertex_t727_UIVertex_t727,
	RuntimeInvoker_Object_t_UIVertex_t727_UIVertex_t727_Object_t_Object_t,
	RuntimeInvoker_Object_t_ColorTween_t670,
	RuntimeInvoker_UILineInfo_t857,
	RuntimeInvoker_UICharInfo_t859,
	RuntimeInvoker_Object_t_Single_t36_Object_t_Object_t,
	RuntimeInvoker_Object_t_Vector2_t29_Object_t_Object_t,
	RuntimeInvoker_EyewearCalibrationReading_t913,
	RuntimeInvoker_TargetSearchResult_t1050,
	RuntimeInvoker_KeyValuePair_2_t3060_Int32_t59_Object_t,
	RuntimeInvoker_PIXEL_FORMAT_t942_Int32_t59_Object_t,
	RuntimeInvoker_PIXEL_FORMAT_t942_Object_t,
	RuntimeInvoker_Enumerator_t3065,
	RuntimeInvoker_KeyValuePair_2_t3060,
	RuntimeInvoker_Enumerator_t3064,
	RuntimeInvoker_KeyValuePair_2_t3060_Object_t,
	RuntimeInvoker_Enumerator_t3077,
	RuntimeInvoker_Color32_t829,
	RuntimeInvoker_TrackableResultData_t969,
	RuntimeInvoker_WordData_t974,
	RuntimeInvoker_WordResultData_t973,
	RuntimeInvoker_Enumerator_t3123,
	RuntimeInvoker_Object_t_TrackableResultData_t969_Object_t_Object_t,
	RuntimeInvoker_SmartTerrainRevisionData_t977,
	RuntimeInvoker_SurfaceData_t978,
	RuntimeInvoker_PropData_t979,
	RuntimeInvoker_KeyValuePair_2_t3151_Object_t_Int16_t561,
	RuntimeInvoker_UInt16_t562_Object_t_Int16_t561,
	RuntimeInvoker_Enumerator_t3156,
	RuntimeInvoker_DictionaryEntry_t58_Object_t_Int16_t561,
	RuntimeInvoker_KeyValuePair_2_t3151,
	RuntimeInvoker_Enumerator_t3155,
	RuntimeInvoker_Object_t_Object_t_Int16_t561_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3159,
	RuntimeInvoker_KeyValuePair_2_t3151_Object_t,
	RuntimeInvoker_Boolean_t41_Int16_t561_Int16_t561,
	RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t916_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t3240_Int32_t59_TrackableResultData_t969,
	RuntimeInvoker_Int32_t59_Int32_t59_TrackableResultData_t969,
	RuntimeInvoker_TrackableResultData_t969_Int32_t59_TrackableResultData_t969,
	RuntimeInvoker_Boolean_t41_Int32_t59_TrackableResultDataU26_t4124,
	RuntimeInvoker_TrackableResultData_t969_Object_t,
	RuntimeInvoker_Enumerator_t3244,
	RuntimeInvoker_DictionaryEntry_t58_Int32_t59_TrackableResultData_t969,
	RuntimeInvoker_KeyValuePair_2_t3240,
	RuntimeInvoker_Enumerator_t3243,
	RuntimeInvoker_Object_t_Int32_t59_TrackableResultData_t969_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3247,
	RuntimeInvoker_KeyValuePair_2_t3240_Object_t,
	RuntimeInvoker_Boolean_t41_TrackableResultData_t969_TrackableResultData_t969,
	RuntimeInvoker_KeyValuePair_2_t3255_Int32_t59_VirtualButtonData_t970,
	RuntimeInvoker_Int32_t59_Int32_t59_VirtualButtonData_t970,
	RuntimeInvoker_VirtualButtonData_t970_Int32_t59_VirtualButtonData_t970,
	RuntimeInvoker_Boolean_t41_Int32_t59_VirtualButtonDataU26_t4125,
	RuntimeInvoker_VirtualButtonData_t970_Object_t,
	RuntimeInvoker_Enumerator_t3260,
	RuntimeInvoker_DictionaryEntry_t58_Int32_t59_VirtualButtonData_t970,
	RuntimeInvoker_KeyValuePair_2_t3255,
	RuntimeInvoker_VirtualButtonData_t970,
	RuntimeInvoker_Enumerator_t3259,
	RuntimeInvoker_Object_t_Int32_t59_VirtualButtonData_t970_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3263,
	RuntimeInvoker_KeyValuePair_2_t3255_Object_t,
	RuntimeInvoker_Boolean_t41_VirtualButtonData_t970_VirtualButtonData_t970,
	RuntimeInvoker_TargetSearchResult_t1050_Object_t,
	RuntimeInvoker_Enumerator_t3274,
	RuntimeInvoker_Boolean_t41_TargetSearchResult_t1050_TargetSearchResult_t1050,
	RuntimeInvoker_Object_t_TargetSearchResult_t1050_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_TargetSearchResult_t1050_TargetSearchResult_t1050,
	RuntimeInvoker_Object_t_TargetSearchResult_t1050_TargetSearchResult_t1050_Object_t_Object_t,
	RuntimeInvoker_Void_t1856_Object_t_ProfileData_t1064,
	RuntimeInvoker_KeyValuePair_2_t3292_Object_t_ProfileData_t1064,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t1064,
	RuntimeInvoker_ProfileData_t1064_Object_t_ProfileData_t1064,
	RuntimeInvoker_Boolean_t41_Object_t_ProfileDataU26_t4126,
	RuntimeInvoker_Enumerator_t3297,
	RuntimeInvoker_DictionaryEntry_t58_Object_t_ProfileData_t1064,
	RuntimeInvoker_KeyValuePair_2_t3292,
	RuntimeInvoker_Enumerator_t3296,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t1064_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3300,
	RuntimeInvoker_KeyValuePair_2_t3292_Object_t,
	RuntimeInvoker_Boolean_t41_ProfileData_t1064_ProfileData_t1064,
	RuntimeInvoker_Link_t3340,
	RuntimeInvoker_ParameterModifier_t2119,
	RuntimeInvoker_GcAchievementData_t1244,
	RuntimeInvoker_GcScoreData_t1245,
	RuntimeInvoker_HitInfo_t1271,
	RuntimeInvoker_TextEditOp_t1286_Object_t,
	RuntimeInvoker_KeyValuePair_2_t3373_Object_t_Int32_t59,
	RuntimeInvoker_TextEditOp_t1286_Object_t_Int32_t59,
	RuntimeInvoker_Boolean_t41_Object_t_TextEditOpU26_t4127,
	RuntimeInvoker_Enumerator_t3378,
	RuntimeInvoker_KeyValuePair_2_t3373,
	RuntimeInvoker_TextEditOp_t1286,
	RuntimeInvoker_Enumerator_t3377,
	RuntimeInvoker_Enumerator_t3381,
	RuntimeInvoker_KeyValuePair_2_t3373_Object_t,
	RuntimeInvoker_UICharInfo_t859_Object_t,
	RuntimeInvoker_Enumerator_t3391,
	RuntimeInvoker_Boolean_t41_UICharInfo_t859_UICharInfo_t859,
	RuntimeInvoker_Object_t_UICharInfo_t859_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_UICharInfo_t859_UICharInfo_t859,
	RuntimeInvoker_Object_t_UICharInfo_t859_UICharInfo_t859_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t857_Object_t,
	RuntimeInvoker_Enumerator_t3400,
	RuntimeInvoker_Boolean_t41_UILineInfo_t857_UILineInfo_t857,
	RuntimeInvoker_Object_t_UILineInfo_t857_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_UILineInfo_t857_UILineInfo_t857,
	RuntimeInvoker_Object_t_UILineInfo_t857_UILineInfo_t857_Object_t_Object_t,
	RuntimeInvoker_Keyframe_t1321,
	RuntimeInvoker_KeyValuePair_2_t3478_Object_t_SByte_t559,
	RuntimeInvoker_Boolean_t41_Object_t_BooleanU26_t4018,
	RuntimeInvoker_Enumerator_t3483,
	RuntimeInvoker_DictionaryEntry_t58_Object_t_SByte_t559,
	RuntimeInvoker_KeyValuePair_2_t3478,
	RuntimeInvoker_Enumerator_t3482,
	RuntimeInvoker_Object_t_Object_t_SByte_t559_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3486,
	RuntimeInvoker_KeyValuePair_2_t3478_Object_t,
	RuntimeInvoker_X509ChainStatus_t1558,
	RuntimeInvoker_Mark_t1605,
	RuntimeInvoker_UriScheme_t1641,
	RuntimeInvoker_ClientCertificateType_t1784,
	RuntimeInvoker_TableRange_t1889,
	RuntimeInvoker_Slot_t1966,
	RuntimeInvoker_Slot_t1974,
	RuntimeInvoker_ILTokenInfo_t2053,
	RuntimeInvoker_LabelData_t2055,
	RuntimeInvoker_LabelFixup_t2054,
	RuntimeInvoker_CustomAttributeTypedArgument_t2101,
	RuntimeInvoker_CustomAttributeNamedArgument_t2100,
	RuntimeInvoker_CustomAttributeTypedArgument_t2101_Object_t,
	RuntimeInvoker_Enumerator_t3547,
	RuntimeInvoker_Boolean_t41_CustomAttributeTypedArgument_t2101_CustomAttributeTypedArgument_t2101,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t2101_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_CustomAttributeTypedArgument_t2101_CustomAttributeTypedArgument_t2101,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t2101_CustomAttributeTypedArgument_t2101_Object_t_Object_t,
	RuntimeInvoker_CustomAttributeNamedArgument_t2100_Object_t,
	RuntimeInvoker_Enumerator_t3558,
	RuntimeInvoker_Boolean_t41_CustomAttributeNamedArgument_t2100_CustomAttributeNamedArgument_t2100,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t2100_Object_t_Object_t,
	RuntimeInvoker_Int32_t59_CustomAttributeNamedArgument_t2100_CustomAttributeNamedArgument_t2100,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t2100_CustomAttributeNamedArgument_t2100_Object_t_Object_t,
	RuntimeInvoker_ResourceInfo_t2130,
	RuntimeInvoker_ResourceCacheItem_t2131,
	RuntimeInvoker_TypeTag_t2267,
	RuntimeInvoker_Int32_t59_DateTimeOffset_t2423_DateTimeOffset_t2423,
	RuntimeInvoker_Boolean_t41_DateTimeOffset_t2423_DateTimeOffset_t2423,
	RuntimeInvoker_Boolean_t41_Nullable_1_t2528,
	RuntimeInvoker_Int32_t59_Guid_t61_Guid_t61,
	RuntimeInvoker_Boolean_t41_Guid_t61_Guid_t61,
};
