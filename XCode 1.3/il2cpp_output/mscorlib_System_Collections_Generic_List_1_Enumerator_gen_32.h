﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>
struct List_1_t460;
// UnionAssets.FLE.EventHandlerFunction
struct EventHandlerFunction_t458;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.EventHandlerFunction>
struct  Enumerator_t2655 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.EventHandlerFunction>::l
	List_1_t460 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.EventHandlerFunction>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.EventHandlerFunction>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnionAssets.FLE.EventHandlerFunction>::current
	EventHandlerFunction_t458 * ___current_3;
};
