﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t987;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C" void Enumerator__ctor_m23782_gshared (Enumerator_t3123 * __this, LinkedList_1_t987 * ___parent, const MethodInfo* method);
#define Enumerator__ctor_m23782(__this, ___parent, method) (( void (*) (Enumerator_t3123 *, LinkedList_1_t987 *, const MethodInfo*))Enumerator__ctor_m23782_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23783_gshared (Enumerator_t3123 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23783(__this, method) (( Object_t * (*) (Enumerator_t3123 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23783_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m23784_gshared (Enumerator_t3123 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m23784(__this, method) (( void (*) (Enumerator_t3123 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m23784_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m23785_gshared (Enumerator_t3123 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23785(__this, method) (( int32_t (*) (Enumerator_t3123 *, const MethodInfo*))Enumerator_get_Current_m23785_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23786_gshared (Enumerator_t3123 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23786(__this, method) (( bool (*) (Enumerator_t3123 *, const MethodInfo*))Enumerator_MoveNext_m23786_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m23787_gshared (Enumerator_t3123 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23787(__this, method) (( void (*) (Enumerator_t3123 *, const MethodInfo*))Enumerator_Dispose_m23787_gshared)(__this, method)
