﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete
struct OnComplete_t269;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Facebook.Unity.Editor.EditorFacebookMockDialog
struct  EditorFacebookMockDialog_t270  : public MonoBehaviour_t18
{
	// Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete Facebook.Unity.Editor.EditorFacebookMockDialog::<Callback>k__BackingField
	OnComplete_t269 * ___U3CCallbackU3Ek__BackingField_1;
	// System.String Facebook.Unity.Editor.EditorFacebookMockDialog::<CallbackID>k__BackingField
	String_t* ___U3CCallbackIDU3Ek__BackingField_2;
};
