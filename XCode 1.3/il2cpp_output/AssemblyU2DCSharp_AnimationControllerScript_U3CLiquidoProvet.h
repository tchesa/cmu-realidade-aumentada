﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem
struct ParticleSystem_t332;
// UnityEngine.GameObject
struct GameObject_t27;
// System.Object
struct Object_t;
// AnimationControllerScript
struct AnimationControllerScript_t364;

#include "mscorlib_System_Object.h"

// AnimationControllerScript/<LiquidoProveta>c__Iterator13
struct  U3CLiquidoProvetaU3Ec__Iterator13_t368  : public Object_t
{
	// UnityEngine.ParticleSystem AnimationControllerScript/<LiquidoProveta>c__Iterator13::<liquid>__0
	ParticleSystem_t332 * ___U3CliquidU3E__0_0;
	// UnityEngine.GameObject AnimationControllerScript/<LiquidoProveta>c__Iterator13::<f>__1
	GameObject_t27 * ___U3CfU3E__1_1;
	// System.Int32 AnimationControllerScript/<LiquidoProveta>c__Iterator13::$PC
	int32_t ___U24PC_2;
	// System.Object AnimationControllerScript/<LiquidoProveta>c__Iterator13::$current
	Object_t * ___U24current_3;
	// AnimationControllerScript AnimationControllerScript/<LiquidoProveta>c__Iterator13::<>f__this
	AnimationControllerScript_t364 * ___U3CU3Ef__this_4;
};
