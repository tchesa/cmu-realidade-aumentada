﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TesteTakeScreenshot
struct TesteTakeScreenshot_t394;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void TesteTakeScreenshot::.ctor()
extern "C" void TesteTakeScreenshot__ctor_m2013 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteTakeScreenshot::Start()
extern "C" void TesteTakeScreenshot_Start_m2014 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TesteTakeScreenshot::ScreenShotName(System.Int32,System.Int32)
extern "C" String_t* TesteTakeScreenshot_ScreenShotName_m2015 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteTakeScreenshot::TakeHiResShot()
extern "C" void TesteTakeScreenshot_TakeHiResShot_m2016 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteTakeScreenshot::LateUpdate()
extern "C" void TesteTakeScreenshot_LateUpdate_m2017 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TesteTakeScreenshot::ITakeScreenshot()
extern "C" Object_t * TesteTakeScreenshot_ITakeScreenshot_m2018 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
