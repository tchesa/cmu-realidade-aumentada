﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// System.Object[]
// System.Object[]
struct ObjectU5BU5D_t34  : public Array_t { };
// System.Type[]
// System.Type[]
struct TypeU5BU5D_t1218  : public Array_t { };
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct IReflectU5BU5D_t3769  : public Array_t { };
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct _TypeU5BU5D_t3770  : public Array_t { };
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t2273  : public Array_t { };
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct ICustomAttributeProviderU5BU5D_t3771  : public Array_t { };
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct _MemberInfoU5BU5D_t3772  : public Array_t { };
// System.Int32[]
// System.Int32[]
struct Int32U5BU5D_t335  : public Array_t { };
// System.IFormattable[]
// System.IFormattable[]
struct IFormattableU5BU5D_t3773  : public Array_t { };
// System.IConvertible[]
// System.IConvertible[]
struct IConvertibleU5BU5D_t3774  : public Array_t { };
// System.IComparable[]
// System.IComparable[]
struct IComparableU5BU5D_t3775  : public Array_t { };
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct IComparable_1U5BU5D_t3776  : public Array_t { };
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct IEquatable_1U5BU5D_t3777  : public Array_t { };
// System.ValueType[]
// System.ValueType[]
struct ValueTypeU5BU5D_t3778  : public Array_t { };
// System.Double[]
// System.Double[]
struct DoubleU5BU5D_t2512  : public Array_t { };
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct IComparable_1U5BU5D_t3779  : public Array_t { };
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct IEquatable_1U5BU5D_t3780  : public Array_t { };
// System.Char[]
// System.Char[]
struct CharU5BU5D_t493  : public Array_t { };
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct IComparable_1U5BU5D_t3781  : public Array_t { };
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct IEquatable_1U5BU5D_t3782  : public Array_t { };
// System.Collections.Hashtable[]
// System.Collections.Hashtable[]
struct HashtableU5BU5D_t2579  : public Array_t { };
// System.Single[]
// System.Single[]
struct SingleU5BU5D_t23  : public Array_t { };
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct IComparable_1U5BU5D_t3783  : public Array_t { };
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct IEquatable_1U5BU5D_t3784  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3604  : public Array_t { };
// System.String[]
// System.String[]
struct StringU5BU5D_t260  : public Array_t { };
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t3785  : public Array_t { };
// System.ICloneable[]
// System.ICloneable[]
struct ICloneableU5BU5D_t3786  : public Array_t { };
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct IComparable_1U5BU5D_t3787  : public Array_t { };
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct IEquatable_1U5BU5D_t3788  : public Array_t { };
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2601  : public Array_t { };
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t3789  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3603  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3610  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,ComponentTrack>[]
// System.Collections.Generic.KeyValuePair`2<System.String,ComponentTrack>[]
struct KeyValuePair_2U5BU5D_t3609  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3615  : public Array_t { };
// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>[]
// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>[]
struct List_1U5BU5D_t2648  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>>[]
struct KeyValuePair_2U5BU5D_t3614  : public Array_t { };
// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>[]
// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>[]
struct List_1U5BU5D_t2673  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>[]
struct KeyValuePair_2U5BU5D_t3619  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,GameCenterPlayerTemplate>[]
// System.Collections.Generic.KeyValuePair`2<System.String,GameCenterPlayerTemplate>[]
struct KeyValuePair_2U5BU5D_t3622  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>[]
// System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>[]
struct KeyValuePair_2U5BU5D_t3625  : public Array_t { };
// System.Byte[]
// System.Byte[]
struct ByteU5BU5D_t119  : public Array_t { };
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct IComparable_1U5BU5D_t3790  : public Array_t { };
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct IEquatable_1U5BU5D_t3791  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,GCScore>[]
struct KeyValuePair_2U5BU5D_t3631  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,IOSStoreProductView>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,IOSStoreProductView>[]
struct KeyValuePair_2U5BU5D_t3634  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,iAdBanner>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,iAdBanner>[]
struct KeyValuePair_2U5BU5D_t3637  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,iAdBanner>[]
// System.Collections.Generic.KeyValuePair`2<System.String,iAdBanner>[]
struct KeyValuePair_2U5BU5D_t3640  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t3643  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t3646  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>[]
struct KeyValuePair_2U5BU5D_t3650  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Single>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Single>[]
struct KeyValuePair_2U5BU5D_t3649  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.GameObject>[]
// System.Collections.Generic.List`1<UnityEngine.GameObject>[]
struct List_1U5BU5D_t2804  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>[]
struct KeyValuePair_2U5BU5D_t3655  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3658  : public Array_t { };
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t593  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct List_1U5BU5D_t2872  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct KeyValuePair_2U5BU5D_t3664  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct List_1U5BU5D_t2925  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct KeyValuePair_2U5BU5D_t3669  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct List_1U5BU5D_t2943  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct KeyValuePair_2U5BU5D_t3675  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3676  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3668  : public Array_t { };
// System.Enum[]
// System.Enum[]
struct EnumU5BU5D_t3792  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct List_1U5BU5D_t3018  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct List_1U5BU5D_t3020  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3685  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
struct KeyValuePair_2U5BU5D_t3688  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
struct KeyValuePair_2U5BU5D_t3687  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
struct KeyValuePair_2U5BU5D_t3694  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
struct KeyValuePair_2U5BU5D_t3697  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
struct KeyValuePair_2U5BU5D_t3699  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t3703  : public Array_t { };
// System.UInt16[]
// System.UInt16[]
struct UInt16U5BU5D_t1523  : public Array_t { };
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct IComparable_1U5BU5D_t3793  : public Array_t { };
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct IEquatable_1U5BU5D_t3794  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t3702  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
struct KeyValuePair_2U5BU5D_t3708  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3711  : public Array_t { };
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
struct List_1U5BU5D_t3190  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct KeyValuePair_2U5BU5D_t3713  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
struct KeyValuePair_2U5BU5D_t3715  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3717  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
struct KeyValuePair_2U5BU5D_t3719  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3721  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
struct KeyValuePair_2U5BU5D_t3723  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t3725  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t3728  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
struct KeyValuePair_2U5BU5D_t3732  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t3735  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t3734  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3740  : public Array_t { };
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1450  : public Array_t { };
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1458  : public Array_t { };
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct _ParameterInfoU5BU5D_t3795  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3744  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3743  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct KeyValuePair_2U5BU5D_t3754  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct KeyValuePair_2U5BU5D_t3757  : public Array_t { };
// System.Byte[][]
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1821  : public Array_t { };
// System.IntPtr[]
// System.IntPtr[]
struct IntPtrU5BU5D_t1447  : public Array_t { };
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct ISerializableU5BU5D_t3796  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3760  : public Array_t { };
// System.Boolean[]
// System.Boolean[]
struct BooleanU5BU5D_t1530  : public Array_t { };
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct IComparable_1U5BU5D_t3797  : public Array_t { };
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct IEquatable_1U5BU5D_t3798  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3759  : public Array_t { };
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct X509CertificateU5BU5D_t1657  : public Array_t { };
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct IDeserializationCallbackU5BU5D_t3799  : public Array_t { };
// System.UInt32[]
// System.UInt32[]
struct UInt32U5BU5D_t1694  : public Array_t { };
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct IComparable_1U5BU5D_t3800  : public Array_t { };
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct IEquatable_1U5BU5D_t3801  : public Array_t { };
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1717  : public Array_t { };
// System.Single[,]
// System.Single[,]
struct SingleU5BU2CU5D_t3802  : public Array_t { };
// System.Delegate[]
// System.Delegate[]
struct DelegateU5BU5D_t2510  : public Array_t { };
// System.UInt64[]
// System.UInt64[]
struct UInt64U5BU5D_t2330  : public Array_t { };
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct IComparable_1U5BU5D_t3803  : public Array_t { };
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct IEquatable_1U5BU5D_t3804  : public Array_t { };
// System.Int16[]
// System.Int16[]
struct Int16U5BU5D_t2533  : public Array_t { };
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct IComparable_1U5BU5D_t3805  : public Array_t { };
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct IEquatable_1U5BU5D_t3806  : public Array_t { };
// System.SByte[]
// System.SByte[]
struct SByteU5BU5D_t2386  : public Array_t { };
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct IComparable_1U5BU5D_t3807  : public Array_t { };
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct IEquatable_1U5BU5D_t3808  : public Array_t { };
// System.Int64[]
// System.Int64[]
struct Int64U5BU5D_t2511  : public Array_t { };
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct IComparable_1U5BU5D_t3809  : public Array_t { };
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct IEquatable_1U5BU5D_t3810  : public Array_t { };
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2516  : public Array_t { };
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct _FieldInfoU5BU5D_t3811  : public Array_t { };
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t2515  : public Array_t { };
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct _ConstructorInfoU5BU5D_t3812  : public Array_t { };
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t2517  : public Array_t { };
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct _MethodBaseU5BU5D_t3813  : public Array_t { };
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1891  : public Array_t { };
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct TailoringInfoU5BU5D_t1898  : public Array_t { };
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1906  : public Array_t { };
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1907  : public Array_t { };
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t2513  : public Array_t { };
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t1973  : public Array_t { };
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t1978  : public Array_t { };
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t1987  : public Array_t { };
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1994  : public Array_t { };
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t2041  : public Array_t { };
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct _ModuleBuilderU5BU5D_t3814  : public Array_t { };
// System.Reflection.Module[]
// System.Reflection.Module[]
struct ModuleU5BU5D_t2042  : public Array_t { };
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct _ModuleU5BU5D_t3815  : public Array_t { };
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t2046  : public Array_t { };
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct _ParameterBuilderU5BU5D_t3816  : public Array_t { };
// System.Type[][]
// System.Type[][]
struct TypeU5BU5DU5BU5D_t2047  : public Array_t { };
// System.Array[]
// System.Array[]
struct ArrayU5BU5D_t3817  : public Array_t { };
// System.Collections.ICollection[]
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t3818  : public Array_t { };
// System.Collections.IList[]
// System.Collections.IList[]
struct IListU5BU5D_t3819  : public Array_t { };
// System.Reflection.Emit.ILTokenInfo[]
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t2056  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelData[]
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t2057  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelFixup[]
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t2058  : public Array_t { };
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t2061  : public Array_t { };
// System.Reflection.Emit.TypeBuilder[]
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t2064  : public Array_t { };
// System.Runtime.InteropServices._TypeBuilder[]
// System.Runtime.InteropServices._TypeBuilder[]
struct _TypeBuilderU5BU5D_t3820  : public Array_t { };
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t2071  : public Array_t { };
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct _MethodBuilderU5BU5D_t3821  : public Array_t { };
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct _MethodInfoU5BU5D_t3822  : public Array_t { };
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t2072  : public Array_t { };
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct _ConstructorBuilderU5BU5D_t3823  : public Array_t { };
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t2073  : public Array_t { };
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct _FieldBuilderU5BU5D_t3824  : public Array_t { };
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t2518  : public Array_t { };
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct _PropertyInfoU5BU5D_t3825  : public Array_t { };
// System.Reflection.CustomAttributeTypedArgument[]
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2537  : public Array_t { };
// System.Reflection.CustomAttributeNamedArgument[]
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2538  : public Array_t { };
// System.Resources.ResourceReader/ResourceInfo[]
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t2134  : public Array_t { };
// System.Resources.ResourceReader/ResourceCacheItem[]
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t2135  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextProperty[]
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t2523  : public Array_t { };
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2483  : public Array_t { };
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t2558  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t2527  : public Array_t { };
// System.DateTime[]
// System.DateTime[]
struct DateTimeU5BU5D_t2560  : public Array_t { };
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct IComparable_1U5BU5D_t3826  : public Array_t { };
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct IEquatable_1U5BU5D_t3827  : public Array_t { };
// System.Decimal[]
// System.Decimal[]
struct DecimalU5BU5D_t2561  : public Array_t { };
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct IComparable_1U5BU5D_t3828  : public Array_t { };
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct IEquatable_1U5BU5D_t3829  : public Array_t { };
// System.TimeSpan[]
// System.TimeSpan[]
struct TimeSpanU5BU5D_t2562  : public Array_t { };
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct IComparable_1U5BU5D_t3830  : public Array_t { };
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct IEquatable_1U5BU5D_t3831  : public Array_t { };
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t2563  : public Array_t { };
// System.MonoType[]
// System.MonoType[]
struct MonoTypeU5BU5D_t2566  : public Array_t { };
// System.Byte[,]
// System.Byte[,]
struct ByteU5BU2CU5D_t2304  : public Array_t { };
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t3580  : public Array_t { };
// System.Reflection.CustomAttributeData[]
// System.Reflection.CustomAttributeData[]
struct CustomAttributeDataU5BU5D_t2529  : public Array_t { };
