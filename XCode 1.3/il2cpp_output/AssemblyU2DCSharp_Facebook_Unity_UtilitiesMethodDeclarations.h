﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t221;
// System.Uri
struct Uri_t292;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Int64 Facebook.Unity.Utilities::TotalSeconds(System.DateTime)
extern "C" int64_t Utilities_TotalSeconds_m1696 (Object_t * __this /* static, unused */, DateTime_t220  ___dateTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Utilities::ToCommaSeparateList(System.Collections.Generic.IEnumerable`1<System.String>)
extern "C" String_t* Utilities_ToCommaSeparateList_m1697 (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Utilities::AbsoluteUrlOrEmptyString(System.Uri)
extern "C" String_t* Utilities_AbsoluteUrlOrEmptyString_m1698 (Object_t * __this /* static, unused */, Uri_t292 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Utilities::GetUserAgent(System.String,System.String)
extern "C" String_t* Utilities_GetUserAgent_m1699 (Object_t * __this /* static, unused */, String_t* ___productName, String_t* ___productVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
