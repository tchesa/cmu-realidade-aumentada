﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.TrackerManager
struct TrackerManager_t1058;

#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManager Vuforia.TrackerManager::get_Instance()
extern "C" TrackerManager_t1058 * TrackerManager_get_Instance_m6108 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManager::.ctor()
extern "C" void TrackerManager__ctor_m6109 (TrackerManager_t1058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManager::.cctor()
extern "C" void TrackerManager__cctor_m6110 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
