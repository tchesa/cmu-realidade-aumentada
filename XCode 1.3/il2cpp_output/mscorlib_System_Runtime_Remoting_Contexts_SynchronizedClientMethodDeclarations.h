﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Contexts.SynchronizedClientContextSink
struct SynchronizedClientContextSink_t2192;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t2184;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
struct SynchronizationAttribute_t2189;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Contexts.SynchronizedClientContextSink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink,System.Runtime.Remoting.Contexts.SynchronizationAttribute)
extern "C" void SynchronizedClientContextSink__ctor_m13282 (SynchronizedClientContextSink_t2192 * __this, Object_t * ___next, SynchronizationAttribute_t2189 * ___att, const MethodInfo* method) IL2CPP_METHOD_ATTR;
