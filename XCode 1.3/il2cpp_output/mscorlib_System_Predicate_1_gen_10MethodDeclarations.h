﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<System.Byte>
struct Predicate_1_t2716;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m17310_gshared (Predicate_1_t2716 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m17310(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2716 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m17310_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<System.Byte>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m17311_gshared (Predicate_1_t2716 * __this, uint8_t ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m17311(__this, ___obj, method) (( bool (*) (Predicate_1_t2716 *, uint8_t, const MethodInfo*))Predicate_1_Invoke_m17311_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<System.Byte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m17312_gshared (Predicate_1_t2716 * __this, uint8_t ___obj, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m17312(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2716 *, uint8_t, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m17312_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m17313_gshared (Predicate_1_t2716 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m17313(__this, ___result, method) (( bool (*) (Predicate_1_t2716 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m17313_gshared)(__this, ___result, method)
