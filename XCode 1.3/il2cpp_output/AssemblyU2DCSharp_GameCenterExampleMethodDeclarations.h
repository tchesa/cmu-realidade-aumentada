﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterExample
struct GameCenterExample_t187;
// ISN_Result
struct ISN_Result_t114;
// UnionAssets.FLE.CEvent
struct CEvent_t74;
// ISN_PlayerScoreLoadedResult
struct ISN_PlayerScoreLoadedResult_t116;
// ISN_PlayerSignatureResult
struct ISN_PlayerSignatureResult_t118;

#include "codegen/il2cpp-codegen.h"

// System.Void GameCenterExample::.ctor()
extern "C" void GameCenterExample__ctor_m1057 (GameCenterExample_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::.cctor()
extern "C" void GameCenterExample__cctor_m1058 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::Awake()
extern "C" void GameCenterExample_Awake_m1059 (GameCenterExample_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnGUI()
extern "C" void GameCenterExample_OnGUI_m1060 (GameCenterExample_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnAchievementsLoaded(ISN_Result)
extern "C" void GameCenterExample_OnAchievementsLoaded_m1061 (GameCenterExample_t187 * __this, ISN_Result_t114 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnAchievementsReset()
extern "C" void GameCenterExample_OnAchievementsReset_m1062 (GameCenterExample_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnAchievementProgress(UnionAssets.FLE.CEvent)
extern "C" void GameCenterExample_OnAchievementProgress_m1063 (GameCenterExample_t187 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnLeaderboardScoreLoaded(UnionAssets.FLE.CEvent)
extern "C" void GameCenterExample_OnLeaderboardScoreLoaded_m1064 (GameCenterExample_t187 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnPlayerScoreLoaded(ISN_PlayerScoreLoadedResult)
extern "C" void GameCenterExample_OnPlayerScoreLoaded_m1065 (GameCenterExample_t187 * __this, ISN_PlayerScoreLoadedResult_t116 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnScoreSubmitted(ISN_Result)
extern "C" void GameCenterExample_OnScoreSubmitted_m1066 (GameCenterExample_t187 * __this, ISN_Result_t114 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnAuthFinished(ISN_Result)
extern "C" void GameCenterExample_OnAuthFinished_m1067 (GameCenterExample_t187 * __this, ISN_Result_t114 * ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnPlayerSignatureRetrieveResult(ISN_PlayerSignatureResult)
extern "C" void GameCenterExample_OnPlayerSignatureRetrieveResult_m1068 (GameCenterExample_t187 * __this, ISN_PlayerSignatureResult_t118 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
