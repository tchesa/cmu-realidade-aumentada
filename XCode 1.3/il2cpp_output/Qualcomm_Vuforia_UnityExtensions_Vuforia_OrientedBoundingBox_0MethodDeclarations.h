﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.OrientedBoundingBox3D::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" void OrientedBoundingBox3D__ctor_m4798 (OrientedBoundingBox3D_t926 * __this, Vector3_t6  ___center, Vector3_t6  ___halfExtents, float ___rotationY, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_Center()
extern "C" Vector3_t6  OrientedBoundingBox3D_get_Center_m4799 (OrientedBoundingBox3D_t926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_Center(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_Center_m4800 (OrientedBoundingBox3D_t926 * __this, Vector3_t6  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_HalfExtents()
extern "C" Vector3_t6  OrientedBoundingBox3D_get_HalfExtents_m4801 (OrientedBoundingBox3D_t926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_HalfExtents(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_HalfExtents_m4802 (OrientedBoundingBox3D_t926 * __this, Vector3_t6  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.OrientedBoundingBox3D::get_RotationY()
extern "C" float OrientedBoundingBox3D_get_RotationY_m4803 (OrientedBoundingBox3D_t926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_RotationY(System.Single)
extern "C" void OrientedBoundingBox3D_set_RotationY_m4804 (OrientedBoundingBox3D_t926 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
