﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_RotateCamera
struct CFX_Demo_RotateCamera_t323;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_RotateCamera::.ctor()
extern "C" void CFX_Demo_RotateCamera__ctor_m1761 (CFX_Demo_RotateCamera_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RotateCamera::.cctor()
extern "C" void CFX_Demo_RotateCamera__cctor_m1762 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RotateCamera::Update()
extern "C" void CFX_Demo_RotateCamera_Update_m1763 (CFX_Demo_RotateCamera_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
