﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct ShimEnumerator_t3266;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1210;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m26195_gshared (ShimEnumerator_t3266 * __this, Dictionary_2_t1210 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m26195(__this, ___host, method) (( void (*) (ShimEnumerator_t3266 *, Dictionary_2_t1210 *, const MethodInfo*))ShimEnumerator__ctor_m26195_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m26196_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m26196(__this, method) (( bool (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_MoveNext_m26196_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Entry()
extern "C" DictionaryEntry_t58  ShimEnumerator_get_Entry_m26197_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m26197(__this, method) (( DictionaryEntry_t58  (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_get_Entry_m26197_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m26198_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m26198(__this, method) (( Object_t * (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_get_Key_m26198_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m26199_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m26199(__this, method) (( Object_t * (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_get_Value_m26199_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m26200_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m26200(__this, method) (( Object_t * (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_get_Current_m26200_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Reset()
extern "C" void ShimEnumerator_Reset_m26201_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m26201(__this, method) (( void (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_Reset_m26201_gshared)(__this, method)
