﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Int32>
struct Collection_1_t3126;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Int32[]
struct Int32U5BU5D_t335;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3606;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t3125;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor()
extern "C" void Collection_1__ctor_m23869_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1__ctor_m23869(__this, method) (( void (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1__ctor_m23869_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23870_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23870(__this, method) (( bool (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23870_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23871_gshared (Collection_1_t3126 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m23871(__this, ___array, ___index, method) (( void (*) (Collection_1_t3126 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m23871_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m23872_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m23872(__this, method) (( Object_t * (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m23872_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m23873_gshared (Collection_1_t3126 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m23873(__this, ___value, method) (( int32_t (*) (Collection_1_t3126 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m23873_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m23874_gshared (Collection_1_t3126 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m23874(__this, ___value, method) (( bool (*) (Collection_1_t3126 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m23874_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m23875_gshared (Collection_1_t3126 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m23875(__this, ___value, method) (( int32_t (*) (Collection_1_t3126 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m23875_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m23876_gshared (Collection_1_t3126 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m23876(__this, ___index, ___value, method) (( void (*) (Collection_1_t3126 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m23876_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m23877_gshared (Collection_1_t3126 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m23877(__this, ___value, method) (( void (*) (Collection_1_t3126 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m23877_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m23878_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m23878(__this, method) (( bool (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m23878_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m23879_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m23879(__this, method) (( Object_t * (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m23879_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m23880_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m23880(__this, method) (( bool (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m23880_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m23881_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m23881(__this, method) (( bool (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m23881_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m23882_gshared (Collection_1_t3126 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m23882(__this, ___index, method) (( Object_t * (*) (Collection_1_t3126 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m23882_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m23883_gshared (Collection_1_t3126 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m23883(__this, ___index, ___value, method) (( void (*) (Collection_1_t3126 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m23883_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Add(T)
extern "C" void Collection_1_Add_m23884_gshared (Collection_1_t3126 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m23884(__this, ___item, method) (( void (*) (Collection_1_t3126 *, int32_t, const MethodInfo*))Collection_1_Add_m23884_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Clear()
extern "C" void Collection_1_Clear_m23885_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_Clear_m23885(__this, method) (( void (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_Clear_m23885_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::ClearItems()
extern "C" void Collection_1_ClearItems_m23886_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m23886(__this, method) (( void (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_ClearItems_m23886_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Contains(T)
extern "C" bool Collection_1_Contains_m23887_gshared (Collection_1_t3126 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m23887(__this, ___item, method) (( bool (*) (Collection_1_t3126 *, int32_t, const MethodInfo*))Collection_1_Contains_m23887_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m23888_gshared (Collection_1_t3126 * __this, Int32U5BU5D_t335* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m23888(__this, ___array, ___index, method) (( void (*) (Collection_1_t3126 *, Int32U5BU5D_t335*, int32_t, const MethodInfo*))Collection_1_CopyTo_m23888_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m23889_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m23889(__this, method) (( Object_t* (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_GetEnumerator_m23889_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m23890_gshared (Collection_1_t3126 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m23890(__this, ___item, method) (( int32_t (*) (Collection_1_t3126 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m23890_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m23891_gshared (Collection_1_t3126 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m23891(__this, ___index, ___item, method) (( void (*) (Collection_1_t3126 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m23891_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m23892_gshared (Collection_1_t3126 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m23892(__this, ___index, ___item, method) (( void (*) (Collection_1_t3126 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m23892_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Remove(T)
extern "C" bool Collection_1_Remove_m23893_gshared (Collection_1_t3126 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m23893(__this, ___item, method) (( bool (*) (Collection_1_t3126 *, int32_t, const MethodInfo*))Collection_1_Remove_m23893_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m23894_gshared (Collection_1_t3126 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m23894(__this, ___index, method) (( void (*) (Collection_1_t3126 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m23894_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m23895_gshared (Collection_1_t3126 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m23895(__this, ___index, method) (( void (*) (Collection_1_t3126 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m23895_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m23896_gshared (Collection_1_t3126 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m23896(__this, method) (( int32_t (*) (Collection_1_t3126 *, const MethodInfo*))Collection_1_get_Count_m23896_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m23897_gshared (Collection_1_t3126 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m23897(__this, ___index, method) (( int32_t (*) (Collection_1_t3126 *, int32_t, const MethodInfo*))Collection_1_get_Item_m23897_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m23898_gshared (Collection_1_t3126 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m23898(__this, ___index, ___value, method) (( void (*) (Collection_1_t3126 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m23898_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m23899_gshared (Collection_1_t3126 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m23899(__this, ___index, ___item, method) (( void (*) (Collection_1_t3126 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m23899_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m23900_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m23900(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m23900_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m23901_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m23901(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m23901_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m23902_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m23902(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m23902_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m23903_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m23903(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m23903_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m23904_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m23904(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m23904_gshared)(__this /* static, unused */, ___list, method)
