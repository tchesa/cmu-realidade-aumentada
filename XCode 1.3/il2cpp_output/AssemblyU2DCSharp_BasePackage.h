﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.MemoryStream
struct MemoryStream_t200;
// System.IO.BinaryWriter
struct BinaryWriter_t201;

#include "mscorlib_System_Object.h"

// BasePackage
struct  BasePackage_t199  : public Object_t
{
	// System.IO.MemoryStream BasePackage::buffer
	MemoryStream_t200 * ___buffer_0;
	// System.IO.BinaryWriter BasePackage::writer
	BinaryWriter_t201 * ___writer_1;
};
