﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJNIHelper
struct AndroidJNIHelper_t255;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJNIHelper::.ctor()
extern "C" void AndroidJNIHelper__ctor_m1436 (AndroidJNIHelper_t255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJNIHelper::get_Debug()
extern "C" bool AndroidJNIHelper_get_Debug_m1437 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJNIHelper::set_Debug(System.Boolean)
extern "C" void AndroidJNIHelper_set_Debug_m1438 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
