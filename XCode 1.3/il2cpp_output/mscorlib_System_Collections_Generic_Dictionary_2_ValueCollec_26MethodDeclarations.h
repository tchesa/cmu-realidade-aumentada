﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m28849(__this, ___host, method) (( void (*) (Enumerator_t1471 *, Dictionary_2_t1397 *, const MethodInfo*))Enumerator__ctor_m16177_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m28850(__this, method) (( Object_t * (*) (Enumerator_t1471 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16178_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m28851(__this, method) (( void (*) (Enumerator_t1471 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m28852(__this, method) (( void (*) (Enumerator_t1471 *, const MethodInfo*))Enumerator_Dispose_m16180_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m8188(__this, method) (( bool (*) (Enumerator_t1471 *, const MethodInfo*))Enumerator_MoveNext_m16181_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m8187(__this, method) (( GUIStyle_t184 * (*) (Enumerator_t1471 *, const MethodInfo*))Enumerator_get_Current_m16182_gshared)(__this, method)
