﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t45;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t311;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// CFX2_Demo
struct  CFX2_Demo_t310  : public MonoBehaviour_t18
{
	// System.Boolean CFX2_Demo::orderedSpawns
	bool ___orderedSpawns_1;
	// System.Single CFX2_Demo::step
	float ___step_2;
	// System.Single CFX2_Demo::range
	float ___range_3;
	// System.Single CFX2_Demo::order
	float ___order_4;
	// UnityEngine.Material CFX2_Demo::groundMat
	Material_t45 * ___groundMat_5;
	// UnityEngine.Material CFX2_Demo::waterMat
	Material_t45 * ___waterMat_6;
	// UnityEngine.GameObject[] CFX2_Demo::ParticleExamples
	GameObjectU5BU5D_t311* ___ParticleExamples_7;
	// System.Int32 CFX2_Demo::exampleIndex
	int32_t ___exampleIndex_8;
	// System.String CFX2_Demo::randomSpawnsDelay
	String_t* ___randomSpawnsDelay_9;
	// System.Boolean CFX2_Demo::randomSpawns
	bool ___randomSpawns_10;
	// System.Boolean CFX2_Demo::slowMo
	bool ___slowMo_11;
};
