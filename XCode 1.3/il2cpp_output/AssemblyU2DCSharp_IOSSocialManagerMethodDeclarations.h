﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialManager
struct IOSSocialManager_t169;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t33;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSSocialManager::.ctor()
extern "C" void IOSSocialManager__ctor_m909 (IOSSocialManager_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::.cctor()
extern "C" void IOSSocialManager__cctor_m910 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::_ISN_TwPost(System.String)
extern "C" void IOSSocialManager__ISN_TwPost_m911 (Object_t * __this /* static, unused */, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::_ISN_TwPostWithMedia(System.String,System.String)
extern "C" void IOSSocialManager__ISN_TwPostWithMedia_m912 (Object_t * __this /* static, unused */, String_t* ___text, String_t* ___encodedMedia, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::_ISN_FbPost(System.String)
extern "C" void IOSSocialManager__ISN_FbPost_m913 (Object_t * __this /* static, unused */, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::_ISN_FbPostWithMedia(System.String,System.String)
extern "C" void IOSSocialManager__ISN_FbPostWithMedia_m914 (Object_t * __this /* static, unused */, String_t* ___text, String_t* ___encodedMedia, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::_ISN_MediaShare(System.String,System.String)
extern "C" void IOSSocialManager__ISN_MediaShare_m915 (Object_t * __this /* static, unused */, String_t* ___text, String_t* ___encodedMedia, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::_ISN_SendMail(System.String,System.String,System.String,System.String)
extern "C" void IOSSocialManager__ISN_SendMail_m916 (Object_t * __this /* static, unused */, String_t* ___subject, String_t* ___body, String_t* ___recipients, String_t* ___encodedMedia, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::Awake()
extern "C" void IOSSocialManager_Awake_m917 (IOSSocialManager_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::ShareMedia(System.String)
extern "C" void IOSSocialManager_ShareMedia_m918 (IOSSocialManager_t169 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::ShareMedia(System.String,UnityEngine.Texture2D)
extern "C" void IOSSocialManager_ShareMedia_m919 (IOSSocialManager_t169 * __this, String_t* ___text, Texture2D_t33 * ___texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::TwitterPost(System.String)
extern "C" void IOSSocialManager_TwitterPost_m920 (IOSSocialManager_t169 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::TwitterPost(System.String,UnityEngine.Texture2D)
extern "C" void IOSSocialManager_TwitterPost_m921 (IOSSocialManager_t169 * __this, String_t* ___text, Texture2D_t33 * ___texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::FacebookPost(System.String)
extern "C" void IOSSocialManager_FacebookPost_m922 (IOSSocialManager_t169 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::FacebookPost(System.String,UnityEngine.Texture2D)
extern "C" void IOSSocialManager_FacebookPost_m923 (IOSSocialManager_t169 * __this, String_t* ___text, Texture2D_t33 * ___texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::SendMail(System.String,System.String,System.String)
extern "C" void IOSSocialManager_SendMail_m924 (IOSSocialManager_t169 * __this, String_t* ___subject, String_t* ___body, String_t* ___recipients, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::SendMail(System.String,System.String,System.String,UnityEngine.Texture2D)
extern "C" void IOSSocialManager_SendMail_m925 (IOSSocialManager_t169 * __this, String_t* ___subject, String_t* ___body, String_t* ___recipients, Texture2D_t33 * ___texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSSocialManager IOSSocialManager::get_instance()
extern "C" IOSSocialManager_t169 * IOSSocialManager_get_instance_m926 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnTwitterPostFailed()
extern "C" void IOSSocialManager_OnTwitterPostFailed_m927 (IOSSocialManager_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnTwitterPostSuccess()
extern "C" void IOSSocialManager_OnTwitterPostSuccess_m928 (IOSSocialManager_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnFacebookPostFailed()
extern "C" void IOSSocialManager_OnFacebookPostFailed_m929 (IOSSocialManager_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnFacebookPostSuccess()
extern "C" void IOSSocialManager_OnFacebookPostSuccess_m930 (IOSSocialManager_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnMailFailed()
extern "C" void IOSSocialManager_OnMailFailed_m931 (IOSSocialManager_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnMailSuccess()
extern "C" void IOSSocialManager_OnMailSuccess_m932 (IOSSocialManager_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
