﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Single>
struct GenericEqualityComparer_1_t2799;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Single>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m18704_gshared (GenericEqualityComparer_1_t2799 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m18704(__this, method) (( void (*) (GenericEqualityComparer_1_t2799 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m18704_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Single>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m18705_gshared (GenericEqualityComparer_1_t2799 * __this, float ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m18705(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2799 *, float, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m18705_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Single>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m18706_gshared (GenericEqualityComparer_1_t2799 * __this, float ___x, float ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m18706(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2799 *, float, float, const MethodInfo*))GenericEqualityComparer_1_Equals_m18706_gshared)(__this, ___x, ___y, method)
