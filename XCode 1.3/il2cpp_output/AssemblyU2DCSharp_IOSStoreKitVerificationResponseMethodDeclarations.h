﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSStoreKitVerificationResponse
struct IOSStoreKitVerificationResponse_t138;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSStoreKitVerificationResponse::.ctor()
extern "C" void IOSStoreKitVerificationResponse__ctor_m783 (IOSStoreKitVerificationResponse_t138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
