﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen_3MethodDeclarations.h"

// System.Void System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m4381(__this, ___object, ___method, method) (( void (*) (Func_2_t763 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m21966_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m21967(__this, ___arg1, method) (( bool (*) (Func_2_t763 *, Toggle_t759 *, const MethodInfo*))Func_2_Invoke_m21968_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m21969(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t763 *, Toggle_t759 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m21970_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m21971(__this, ___result, method) (( bool (*) (Func_2_t763 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m21972_gshared)(__this, ___result, method)
