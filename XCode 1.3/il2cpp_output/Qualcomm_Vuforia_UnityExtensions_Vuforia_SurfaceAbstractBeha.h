﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.Surface
struct Surface_t492;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"

// Vuforia.SurfaceAbstractBehaviour
struct  SurfaceAbstractBehaviour_t436  : public SmartTerrainTrackableBehaviour_t917
{
	// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::mSurface
	Object_t * ___mSurface_12;
};
