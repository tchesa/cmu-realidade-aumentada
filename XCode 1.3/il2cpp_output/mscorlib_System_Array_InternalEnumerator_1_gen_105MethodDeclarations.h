﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29469_gshared (InternalEnumerator_1_t3518 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29469(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3518 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29469_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29470_gshared (InternalEnumerator_1_t3518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29470(__this, method) (( void (*) (InternalEnumerator_1_t3518 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29470_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29471_gshared (InternalEnumerator_1_t3518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29471(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3518 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29471_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29472_gshared (InternalEnumerator_1_t3518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29472(__this, method) (( void (*) (InternalEnumerator_1_t3518 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29472_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29473_gshared (InternalEnumerator_1_t3518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29473(__this, method) (( bool (*) (InternalEnumerator_1_t3518 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29473_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C" TableRange_t1889  InternalEnumerator_1_get_Current_m29474_gshared (InternalEnumerator_1_t3518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29474(__this, method) (( TableRange_t1889  (*) (InternalEnumerator_1_t3518 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29474_gshared)(__this, method)
