﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__49MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m29323(__this, ___dictionary, method) (( void (*) (Enumerator_t3496 *, Dictionary_2_t1529 *, const MethodInfo*))Enumerator__ctor_m29217_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m29324(__this, method) (( Object_t * (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m29218_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m29325(__this, method) (( void (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m29219_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m29326(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m29220_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m29327(__this, method) (( Object_t * (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m29221_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29328(__this, method) (( Object_t * (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m29329(__this, method) (( bool (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_MoveNext_m29223_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m29330(__this, method) (( KeyValuePair_2_t3493  (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_get_Current_m29224_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m29331(__this, method) (( String_t* (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_get_CurrentKey_m29225_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m29332(__this, method) (( bool (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_get_CurrentValue_m29226_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Reset()
#define Enumerator_Reset_m29333(__this, method) (( void (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_Reset_m29227_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m29334(__this, method) (( void (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_VerifyState_m29228_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m29335(__this, method) (( void (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_VerifyCurrent_m29229_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m29336(__this, method) (( void (*) (Enumerator_t3496 *, const MethodInfo*))Enumerator_Dispose_m29230_gshared)(__this, method)
