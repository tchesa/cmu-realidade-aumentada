﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC
struct U3CWaitFrameU3Ec__IteratorC_t330;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::.ctor()
extern "C" void U3CWaitFrameU3Ec__IteratorC__ctor_m1783 (U3CWaitFrameU3Ec__IteratorC_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitFrameU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1784 (U3CWaitFrameU3Ec__IteratorC_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitFrameU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1785 (U3CWaitFrameU3Ec__IteratorC_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::MoveNext()
extern "C" bool U3CWaitFrameU3Ec__IteratorC_MoveNext_m1786 (U3CWaitFrameU3Ec__IteratorC_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::Dispose()
extern "C" void U3CWaitFrameU3Ec__IteratorC_Dispose_m1787 (U3CWaitFrameU3Ec__IteratorC_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::Reset()
extern "C" void U3CWaitFrameU3Ec__IteratorC_Reset_m1788 (U3CWaitFrameU3Ec__IteratorC_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
