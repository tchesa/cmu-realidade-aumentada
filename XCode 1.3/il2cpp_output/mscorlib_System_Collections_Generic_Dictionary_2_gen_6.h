﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t335;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2601;
// IOSStoreProductView[]
struct IOSStoreProductViewU5BU5D_t2732;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2650;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,IOSStoreProductView,System.Collections.DictionaryEntry>
struct Transform_1_t2733;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>
struct  Dictionary_2_t132  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::table
	Int32U5BU5D_t335* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::linkSlots
	LinkU5BU5D_t2601* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::keySlots
	Int32U5BU5D_t335* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::valueSlots
	IOSStoreProductViewU5BU5D_t2732* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::serialization_info
	SerializationInfo_t1438 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t132_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<System.Int32,IOSStoreProductView>::<>f__am$cacheB
	Transform_1_t2733 * ___U3CU3Ef__amU24cacheB_15;
};
