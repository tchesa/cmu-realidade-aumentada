﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3476;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2606;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3760;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t3761;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t3481;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>
struct ValueCollection_t3485;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_49.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__49.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C" void Dictionary_2__ctor_m29087_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m29087(__this, method) (( void (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2__ctor_m29087_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m29088_gshared (Dictionary_2_t3476 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m29088(__this, ___comparer, method) (( void (*) (Dictionary_2_t3476 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m29088_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m29090_gshared (Dictionary_2_t3476 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m29090(__this, ___capacity, method) (( void (*) (Dictionary_2_t3476 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m29090_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m29092_gshared (Dictionary_2_t3476 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m29092(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3476 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m29092_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m29094_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m29094(__this, method) (( Object_t * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m29094_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m29096_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m29096(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m29096_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m29098_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m29098(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m29098_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m29100_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m29100(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m29100_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m29102_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m29102(__this, ___key, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m29102_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m29104_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m29104(__this, ___key, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m29104_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m29106_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m29106(__this, method) (( bool (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m29106_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m29108_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m29108(__this, method) (( Object_t * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m29108_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m29110_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m29110(__this, method) (( bool (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m29110_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m29112_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2_t3478  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m29112(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3476 *, KeyValuePair_2_t3478 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m29112_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m29114_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2_t3478  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m29114(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3476 *, KeyValuePair_2_t3478 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m29114_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m29116_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2U5BU5D_t3760* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m29116(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3476 *, KeyValuePair_2U5BU5D_t3760*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m29116_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29118_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2_t3478  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29118(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3476 *, KeyValuePair_2_t3478 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29118_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m29120_gshared (Dictionary_2_t3476 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m29120(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3476 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m29120_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m29122_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m29122(__this, method) (( Object_t * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m29122_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m29124_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m29124(__this, method) (( Object_t* (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m29124_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m29126_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m29126(__this, method) (( Object_t * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m29126_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m29128_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m29128(__this, method) (( int32_t (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_get_Count_m29128_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(TKey)
extern "C" bool Dictionary_2_get_Item_m29130_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m29130(__this, ___key, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m29130_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m29132_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m29132(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, bool, const MethodInfo*))Dictionary_2_set_Item_m29132_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m29134_gshared (Dictionary_2_t3476 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m29134(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3476 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m29134_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m29136_gshared (Dictionary_2_t3476 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m29136(__this, ___size, method) (( void (*) (Dictionary_2_t3476 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m29136_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m29138_gshared (Dictionary_2_t3476 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m29138(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3476 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m29138_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3478  Dictionary_2_make_pair_m29140_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m29140(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3478  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_make_pair_m29140_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m29142_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m29142(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_key_m29142_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_value(TKey,TValue)
extern "C" bool Dictionary_2_pick_value_m29144_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m29144(__this /* static, unused */, ___key, ___value, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_value_m29144_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m29146_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2U5BU5D_t3760* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m29146(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3476 *, KeyValuePair_2U5BU5D_t3760*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m29146_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Resize()
extern "C" void Dictionary_2_Resize_m29148_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m29148(__this, method) (( void (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_Resize_m29148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m29150_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_Add_m29150(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, bool, const MethodInfo*))Dictionary_2_Add_m29150_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C" void Dictionary_2_Clear_m29152_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m29152(__this, method) (( void (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_Clear_m29152_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m29154_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m29154(__this, ___key, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m29154_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m29156_gshared (Dictionary_2_t3476 * __this, bool ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m29156(__this, ___value, method) (( bool (*) (Dictionary_2_t3476 *, bool, const MethodInfo*))Dictionary_2_ContainsValue_m29156_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m29158_gshared (Dictionary_2_t3476 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m29158(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3476 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m29158_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m29160_gshared (Dictionary_2_t3476 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m29160(__this, ___sender, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m29160_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m29162_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m29162(__this, ___key, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m29162_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m29164_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, bool* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m29164(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m29164_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Keys()
extern "C" KeyCollection_t3481 * Dictionary_2_get_Keys_m29166_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m29166(__this, method) (( KeyCollection_t3481 * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_get_Keys_m29166_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Values()
extern "C" ValueCollection_t3485 * Dictionary_2_get_Values_m29168_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m29168(__this, method) (( ValueCollection_t3485 * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_get_Values_m29168_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m29170_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m29170(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m29170_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTValue(System.Object)
extern "C" bool Dictionary_2_ToTValue_m29172_gshared (Dictionary_2_t3476 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m29172(__this, ___value, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m29172_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m29174_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2_t3478  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m29174(__this, ___pair, method) (( bool (*) (Dictionary_2_t3476 *, KeyValuePair_2_t3478 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m29174_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t3483  Dictionary_2_GetEnumerator_m29176_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m29176(__this, method) (( Enumerator_t3483  (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_GetEnumerator_m29176_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m29178_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m29178(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m29178_gshared)(__this /* static, unused */, ___key, ___value, method)
