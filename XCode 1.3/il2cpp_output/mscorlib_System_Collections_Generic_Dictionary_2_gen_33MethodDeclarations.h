﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1209;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2650;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t3725;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>
struct IEnumerator_1_t3726;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct KeyCollection_t3242;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ValueCollection_t3246;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__39.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C" void Dictionary_2__ctor_m6742_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m6742(__this, method) (( void (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2__ctor_m6742_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m25918_gshared (Dictionary_2_t1209 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m25918(__this, ___comparer, method) (( void (*) (Dictionary_2_t1209 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25918_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m25919_gshared (Dictionary_2_t1209 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m25919(__this, ___capacity, method) (( void (*) (Dictionary_2_t1209 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m25919_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m25920_gshared (Dictionary_2_t1209 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m25920(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1209 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m25920_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m25921_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m25921(__this, method) (( Object_t * (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m25921_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m25922_gshared (Dictionary_2_t1209 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25922(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1209 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m25922_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25923_gshared (Dictionary_2_t1209 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25923(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1209 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m25923_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25924_gshared (Dictionary_2_t1209 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m25924(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1209 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m25924_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m25925_gshared (Dictionary_2_t1209 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m25925(__this, ___key, method) (( bool (*) (Dictionary_2_t1209 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m25925_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25926_gshared (Dictionary_2_t1209 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m25926(__this, ___key, method) (( void (*) (Dictionary_2_t1209 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m25926_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25927_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25927(__this, method) (( bool (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25927_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25928_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25928(__this, method) (( Object_t * (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25928_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25929_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25929(__this, method) (( bool (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25929_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25930_gshared (Dictionary_2_t1209 * __this, KeyValuePair_2_t3240  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25930(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1209 *, KeyValuePair_2_t3240 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25930_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25931_gshared (Dictionary_2_t1209 * __this, KeyValuePair_2_t3240  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25931(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1209 *, KeyValuePair_2_t3240 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25931_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25932_gshared (Dictionary_2_t1209 * __this, KeyValuePair_2U5BU5D_t3725* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25932(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1209 *, KeyValuePair_2U5BU5D_t3725*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25932_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25933_gshared (Dictionary_2_t1209 * __this, KeyValuePair_2_t3240  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25933(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1209 *, KeyValuePair_2_t3240 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25933_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25934_gshared (Dictionary_2_t1209 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25934(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1209 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m25934_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25935_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25935(__this, method) (( Object_t * (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25935_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25936_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25936(__this, method) (( Object_t* (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25936_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25937_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25937(__this, method) (( Object_t * (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25937_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m25938_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m25938(__this, method) (( int32_t (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_get_Count_m25938_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Item(TKey)
extern "C" TrackableResultData_t969  Dictionary_2_get_Item_m25939_gshared (Dictionary_2_t1209 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m25939(__this, ___key, method) (( TrackableResultData_t969  (*) (Dictionary_2_t1209 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m25939_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m25940_gshared (Dictionary_2_t1209 * __this, int32_t ___key, TrackableResultData_t969  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m25940(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1209 *, int32_t, TrackableResultData_t969 , const MethodInfo*))Dictionary_2_set_Item_m25940_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m25941_gshared (Dictionary_2_t1209 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m25941(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1209 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m25941_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m25942_gshared (Dictionary_2_t1209 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m25942(__this, ___size, method) (( void (*) (Dictionary_2_t1209 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m25942_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m25943_gshared (Dictionary_2_t1209 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m25943(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1209 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m25943_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3240  Dictionary_2_make_pair_m25944_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t969  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m25944(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3240  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t969 , const MethodInfo*))Dictionary_2_make_pair_m25944_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m25945_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t969  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m25945(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t969 , const MethodInfo*))Dictionary_2_pick_key_m25945_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::pick_value(TKey,TValue)
extern "C" TrackableResultData_t969  Dictionary_2_pick_value_m25946_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t969  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m25946(__this /* static, unused */, ___key, ___value, method) (( TrackableResultData_t969  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t969 , const MethodInfo*))Dictionary_2_pick_value_m25946_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m25947_gshared (Dictionary_2_t1209 * __this, KeyValuePair_2U5BU5D_t3725* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m25947(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1209 *, KeyValuePair_2U5BU5D_t3725*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m25947_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Resize()
extern "C" void Dictionary_2_Resize_m25948_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m25948(__this, method) (( void (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_Resize_m25948_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m25949_gshared (Dictionary_2_t1209 * __this, int32_t ___key, TrackableResultData_t969  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m25949(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1209 *, int32_t, TrackableResultData_t969 , const MethodInfo*))Dictionary_2_Add_m25949_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Clear()
extern "C" void Dictionary_2_Clear_m25950_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m25950(__this, method) (( void (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_Clear_m25950_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m25951_gshared (Dictionary_2_t1209 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m25951(__this, ___key, method) (( bool (*) (Dictionary_2_t1209 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m25951_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m25952_gshared (Dictionary_2_t1209 * __this, TrackableResultData_t969  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m25952(__this, ___value, method) (( bool (*) (Dictionary_2_t1209 *, TrackableResultData_t969 , const MethodInfo*))Dictionary_2_ContainsValue_m25952_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m25953_gshared (Dictionary_2_t1209 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m25953(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1209 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m25953_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m25954_gshared (Dictionary_2_t1209 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m25954(__this, ___sender, method) (( void (*) (Dictionary_2_t1209 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m25954_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m25955_gshared (Dictionary_2_t1209 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m25955(__this, ___key, method) (( bool (*) (Dictionary_2_t1209 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m25955_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m25956_gshared (Dictionary_2_t1209 * __this, int32_t ___key, TrackableResultData_t969 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m25956(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1209 *, int32_t, TrackableResultData_t969 *, const MethodInfo*))Dictionary_2_TryGetValue_m25956_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Keys()
extern "C" KeyCollection_t3242 * Dictionary_2_get_Keys_m25957_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m25957(__this, method) (( KeyCollection_t3242 * (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_get_Keys_m25957_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Values()
extern "C" ValueCollection_t3246 * Dictionary_2_get_Values_m25958_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m25958(__this, method) (( ValueCollection_t3246 * (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_get_Values_m25958_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m25959_gshared (Dictionary_2_t1209 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m25959(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1209 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m25959_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToTValue(System.Object)
extern "C" TrackableResultData_t969  Dictionary_2_ToTValue_m25960_gshared (Dictionary_2_t1209 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m25960(__this, ___value, method) (( TrackableResultData_t969  (*) (Dictionary_2_t1209 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m25960_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m25961_gshared (Dictionary_2_t1209 * __this, KeyValuePair_2_t3240  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m25961(__this, ___pair, method) (( bool (*) (Dictionary_2_t1209 *, KeyValuePair_2_t3240 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m25961_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetEnumerator()
extern "C" Enumerator_t3244  Dictionary_2_GetEnumerator_m25962_gshared (Dictionary_2_t1209 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m25962(__this, method) (( Enumerator_t3244  (*) (Dictionary_2_t1209 *, const MethodInfo*))Dictionary_2_GetEnumerator_m25962_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m25963_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t969  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m25963(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t969 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m25963_gshared)(__this /* static, unused */, ___key, ___value, method)
