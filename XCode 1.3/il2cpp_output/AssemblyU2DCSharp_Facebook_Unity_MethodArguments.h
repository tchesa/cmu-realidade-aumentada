﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t225;

#include "mscorlib_System_Object.h"

// Facebook.Unity.MethodArguments
struct  MethodArguments_t248  : public Object_t
{
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.MethodArguments::arguments
	Object_t* ___arguments_0;
};
