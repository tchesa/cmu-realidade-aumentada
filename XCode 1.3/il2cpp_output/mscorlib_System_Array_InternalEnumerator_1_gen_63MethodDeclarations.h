﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_63.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26119_gshared (InternalEnumerator_1_t3257 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26119(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3257 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26119_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26120_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26120(__this, method) (( void (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26120_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26121_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26121(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26121_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26122_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26122(__this, method) (( void (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26122_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26123_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26123(__this, method) (( bool (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26123_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" VirtualButtonData_t970  InternalEnumerator_1_get_Current_m26124_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26124(__this, method) (( VirtualButtonData_t970  (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26124_gshared)(__this, method)
