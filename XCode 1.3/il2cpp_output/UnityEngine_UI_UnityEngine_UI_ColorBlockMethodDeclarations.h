﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
#include "UnityEngine_UnityEngine_Color.h"

// UnityEngine.Color UnityEngine.UI.ColorBlock::get_normalColor()
extern "C" Color_t5  ColorBlock_get_normalColor_m3137 (ColorBlock_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_normalColor(UnityEngine.Color)
extern "C" void ColorBlock_set_normalColor_m3138 (ColorBlock_t682 * __this, Color_t5  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_highlightedColor()
extern "C" Color_t5  ColorBlock_get_highlightedColor_m3139 (ColorBlock_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_highlightedColor(UnityEngine.Color)
extern "C" void ColorBlock_set_highlightedColor_m3140 (ColorBlock_t682 * __this, Color_t5  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_pressedColor()
extern "C" Color_t5  ColorBlock_get_pressedColor_m3141 (ColorBlock_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_pressedColor(UnityEngine.Color)
extern "C" void ColorBlock_set_pressedColor_m3142 (ColorBlock_t682 * __this, Color_t5  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_disabledColor()
extern "C" Color_t5  ColorBlock_get_disabledColor_m3143 (ColorBlock_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_disabledColor(UnityEngine.Color)
extern "C" void ColorBlock_set_disabledColor_m3144 (ColorBlock_t682 * __this, Color_t5  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_colorMultiplier()
extern "C" float ColorBlock_get_colorMultiplier_m3145 (ColorBlock_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_colorMultiplier(System.Single)
extern "C" void ColorBlock_set_colorMultiplier_m3146 (ColorBlock_t682 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_fadeDuration()
extern "C" float ColorBlock_get_fadeDuration_m3147 (ColorBlock_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_fadeDuration(System.Single)
extern "C" void ColorBlock_set_fadeDuration_m3148 (ColorBlock_t682 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::get_defaultColorBlock()
extern "C" ColorBlock_t682  ColorBlock_get_defaultColorBlock_m3149 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
