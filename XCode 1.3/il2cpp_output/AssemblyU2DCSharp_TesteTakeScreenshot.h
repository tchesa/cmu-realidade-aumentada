﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t27;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// TesteTakeScreenshot
struct  TesteTakeScreenshot_t394  : public MonoBehaviour_t18
{
	// System.Int32 TesteTakeScreenshot::resWidth
	int32_t ___resWidth_1;
	// System.Int32 TesteTakeScreenshot::resHeight
	int32_t ___resHeight_2;
	// System.Int32 TesteTakeScreenshot::length
	int32_t ___length_3;
	// System.Boolean TesteTakeScreenshot::takeHiResShot
	bool ___takeHiResShot_4;
	// System.String TesteTakeScreenshot::lastTexture
	String_t* ___lastTexture_5;
	// UnityEngine.GameObject TesteTakeScreenshot::teste
	GameObject_t27 * ___teste_6;
};
