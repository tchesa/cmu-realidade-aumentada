﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.Mobile.Android.AndroidFacebook
struct AndroidFacebook_t249;

#include "AssemblyU2DCSharp_Facebook_Unity_MethodCall_1_gen_4.h"

// Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<Facebook.Unity.ILoginResult>
struct  JavaMethodCall_1_t537  : public MethodCall_1_t2779
{
	// Facebook.Unity.Mobile.Android.AndroidFacebook Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<Facebook.Unity.ILoginResult>::androidImpl
	AndroidFacebook_t249 * ___androidImpl_4;
};
