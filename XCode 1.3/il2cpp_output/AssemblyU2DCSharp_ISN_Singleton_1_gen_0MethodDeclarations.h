﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ISN_Singleton_1_gen_6MethodDeclarations.h"

// System.Void ISN_Singleton`1<IOSCamera>::.ctor()
#define ISN_Singleton_1__ctor_m2249(__this, method) (( void (*) (ISN_Singleton_1_t146 *, const MethodInfo*))ISN_Singleton_1__ctor_m16858_gshared)(__this, method)
// System.Void ISN_Singleton`1<IOSCamera>::.cctor()
#define ISN_Singleton_1__cctor_m17626(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1__cctor_m16859_gshared)(__this /* static, unused */, method)
// T ISN_Singleton`1<IOSCamera>::get_instance()
#define ISN_Singleton_1_get_instance_m2336(__this /* static, unused */, method) (( IOSCamera_t145 * (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_instance_m16860_gshared)(__this /* static, unused */, method)
// System.Boolean ISN_Singleton`1<IOSCamera>::get_HasInstance()
#define ISN_Singleton_1_get_HasInstance_m17627(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_HasInstance_m16861_gshared)(__this /* static, unused */, method)
// System.Boolean ISN_Singleton`1<IOSCamera>::get_IsDestroyed()
#define ISN_Singleton_1_get_IsDestroyed_m17628(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_IsDestroyed_m16862_gshared)(__this /* static, unused */, method)
// System.Void ISN_Singleton`1<IOSCamera>::OnDestroy()
#define ISN_Singleton_1_OnDestroy_m17629(__this, method) (( void (*) (ISN_Singleton_1_t146 *, const MethodInfo*))ISN_Singleton_1_OnDestroy_m16863_gshared)(__this, method)
// System.Void ISN_Singleton`1<IOSCamera>::OnApplicationQuit()
#define ISN_Singleton_1_OnApplicationQuit_m17630(__this, method) (( void (*) (ISN_Singleton_1_t146 *, const MethodInfo*))ISN_Singleton_1_OnApplicationQuit_m16864_gshared)(__this, method)
