﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransparencyAnimation
struct TransparencyAnimation_t340;

#include "codegen/il2cpp-codegen.h"

// System.Void TransparencyAnimation::.ctor()
extern "C" void TransparencyAnimation__ctor_m2019 (TransparencyAnimation_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransparencyAnimation::Start()
extern "C" void TransparencyAnimation_Start_m2020 (TransparencyAnimation_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransparencyAnimation::Update()
extern "C" void TransparencyAnimation_Update_m2021 (TransparencyAnimation_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransparencyAnimation::Show()
extern "C" void TransparencyAnimation_Show_m2022 (TransparencyAnimation_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransparencyAnimation::Hide()
extern "C" void TransparencyAnimation_Hide_m2023 (TransparencyAnimation_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransparencyAnimation::Reset()
extern "C" void TransparencyAnimation_Reset_m2024 (TransparencyAnimation_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
