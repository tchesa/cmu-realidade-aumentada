﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// AnimationControllerScript
struct AnimationControllerScript_t364;

#include "mscorlib_System_Object.h"

// AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14
struct  U3CAtivaParticulaPortalU3Ec__Iterator14_t369  : public Object_t
{
	// System.Int32 AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::$PC
	int32_t ___U24PC_0;
	// System.Object AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::$current
	Object_t * ___U24current_1;
	// AnimationControllerScript AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::<>f__this
	AnimationControllerScript_t364 * ___U3CU3Ef__this_2;
};
