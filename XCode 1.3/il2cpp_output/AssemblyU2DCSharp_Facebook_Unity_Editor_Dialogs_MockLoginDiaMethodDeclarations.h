﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey1A
struct U3CSendSuccessResultU3Ec__AnonStorey1A_t272;
// Facebook.Unity.IGraphResult
struct IGraphResult_t484;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey1A::.ctor()
extern "C" void U3CSendSuccessResultU3Ec__AnonStorey1A__ctor_m1570 (U3CSendSuccessResultU3Ec__AnonStorey1A_t272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey1A::<>m__33(Facebook.Unity.IGraphResult)
extern "C" void U3CSendSuccessResultU3Ec__AnonStorey1A_U3CU3Em__33_m1571 (U3CSendSuccessResultU3Ec__AnonStorey1A_t272 * __this, Object_t * ___permResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
