﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_EasingFunction_t9 ();
extern "C" void pinvoke_delegate_wrapper_ApplyTween_t13 ();
extern "C" void pinvoke_delegate_wrapper_BillingInitListener_t125 ();
extern "C" void pinvoke_delegate_wrapper_OnComplete_t269 ();
extern "C" void pinvoke_delegate_wrapper_EventHandlerFunction_t458 ();
extern "C" void pinvoke_delegate_wrapper_DataEventHandlerFunction_t459 ();
extern "C" void pinvoke_delegate_wrapper_InitDelegate_t241 ();
extern "C" void pinvoke_delegate_wrapper_HideUnityDelegate_t242 ();
extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t718 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t1316 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t1317 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t872 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t841 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t1374 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t548 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t1395 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t1416 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t1420 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t691 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t1576 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t1614 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1514 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t1649 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1718 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1797 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t1776 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t1777 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t1760 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1761 ();
extern "C" void pinvoke_delegate_wrapper_Action_t81 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t1855 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t12 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1921 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1929 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t2017 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t2018 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t2103 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t2117 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t2294 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t2480 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1858 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t2110 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t2481 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t2482 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t2484 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t2404 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t2485 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t2413 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2410 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t2039 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t2411 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1475 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[52] = 
{
	pinvoke_delegate_wrapper_EasingFunction_t9,
	pinvoke_delegate_wrapper_ApplyTween_t13,
	pinvoke_delegate_wrapper_BillingInitListener_t125,
	pinvoke_delegate_wrapper_OnComplete_t269,
	pinvoke_delegate_wrapper_EventHandlerFunction_t458,
	pinvoke_delegate_wrapper_DataEventHandlerFunction_t459,
	pinvoke_delegate_wrapper_InitDelegate_t241,
	pinvoke_delegate_wrapper_HideUnityDelegate_t242,
	pinvoke_delegate_wrapper_OnValidateInput_t718,
	pinvoke_delegate_wrapper_PCMReaderCallback_t1316,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t1317,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t872,
	pinvoke_delegate_wrapper_WillRenderCanvases_t841,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t1374,
	pinvoke_delegate_wrapper_WindowFunction_t548,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t1395,
	pinvoke_delegate_wrapper_LogCallback_t1416,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t1420,
	pinvoke_delegate_wrapper_UnityAction_t691,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t1576,
	pinvoke_delegate_wrapper_CostDelegate_t1614,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1514,
	pinvoke_delegate_wrapper_MatchEvaluator_t1649,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1718,
	pinvoke_delegate_wrapper_PrimalityTest_t1797,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t1776,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t1777,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t1760,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1761,
	pinvoke_delegate_wrapper_Action_t81,
	pinvoke_delegate_wrapper_Swapper_t1855,
	pinvoke_delegate_wrapper_AsyncCallback_t12,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1921,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1929,
	pinvoke_delegate_wrapper_ReadDelegate_t2017,
	pinvoke_delegate_wrapper_WriteDelegate_t2018,
	pinvoke_delegate_wrapper_AddEventAdapter_t2103,
	pinvoke_delegate_wrapper_GetterAdapter_t2117,
	pinvoke_delegate_wrapper_CallbackHandler_t2294,
	pinvoke_delegate_wrapper_PrimalityTest_t2480,
	pinvoke_delegate_wrapper_MemberFilter_t1858,
	pinvoke_delegate_wrapper_TypeFilter_t2110,
	pinvoke_delegate_wrapper_CrossContextDelegate_t2481,
	pinvoke_delegate_wrapper_HeaderHandler_t2482,
	pinvoke_delegate_wrapper_ThreadStart_t2484,
	pinvoke_delegate_wrapper_TimerCallback_t2404,
	pinvoke_delegate_wrapper_WaitCallback_t2485,
	pinvoke_delegate_wrapper_AppDomainInitializer_t2413,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2410,
	pinvoke_delegate_wrapper_EventHandler_t2039,
	pinvoke_delegate_wrapper_ResolveEventHandler_t2411,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1475,
};
