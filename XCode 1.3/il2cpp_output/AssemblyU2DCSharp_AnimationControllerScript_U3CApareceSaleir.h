﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// AnimationControllerScript/<ApareceSaleiro>c__Iterator12
struct  U3CApareceSaleiroU3Ec__Iterator12_t367  : public Object_t
{
	// System.Int32 AnimationControllerScript/<ApareceSaleiro>c__Iterator12::$PC
	int32_t ___U24PC_0;
	// System.Object AnimationControllerScript/<ApareceSaleiro>c__Iterator12::$current
	Object_t * ___U24current_1;
};
