﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.Mobile.Android.IAndroidJavaClass
struct IAndroidJavaClass_t251;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_MobileFacebook.h"

// Facebook.Unity.Mobile.Android.AndroidFacebook
struct  AndroidFacebook_t249  : public MobileFacebook_t250
{
	// System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::limitEventUsage
	bool ___limitEventUsage_5;
	// Facebook.Unity.Mobile.Android.IAndroidJavaClass Facebook.Unity.Mobile.Android.AndroidFacebook::facebookJava
	Object_t * ___facebookJava_6;
	// System.String Facebook.Unity.Mobile.Android.AndroidFacebook::<KeyHash>k__BackingField
	String_t* ___U3CKeyHashU3Ek__BackingField_7;
};
