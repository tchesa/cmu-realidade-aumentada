﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m18616_gshared (KeyValuePair_2_t2787 * __this, Object_t * ___key, float ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m18616(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2787 *, Object_t *, float, const MethodInfo*))KeyValuePair_2__ctor_m18616_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m18617_gshared (KeyValuePair_2_t2787 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m18617(__this, method) (( Object_t * (*) (KeyValuePair_2_t2787 *, const MethodInfo*))KeyValuePair_2_get_Key_m18617_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m18618_gshared (KeyValuePair_2_t2787 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m18618(__this, ___value, method) (( void (*) (KeyValuePair_2_t2787 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m18618_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C" float KeyValuePair_2_get_Value_m18619_gshared (KeyValuePair_2_t2787 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m18619(__this, method) (( float (*) (KeyValuePair_2_t2787 *, const MethodInfo*))KeyValuePair_2_get_Value_m18619_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m18620_gshared (KeyValuePair_2_t2787 * __this, float ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m18620(__this, ___value, method) (( void (*) (KeyValuePair_2_t2787 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m18620_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m18621_gshared (KeyValuePair_2_t2787 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m18621(__this, method) (( String_t* (*) (KeyValuePair_2_t2787 *, const MethodInfo*))KeyValuePair_2_ToString_m18621_gshared)(__this, method)
