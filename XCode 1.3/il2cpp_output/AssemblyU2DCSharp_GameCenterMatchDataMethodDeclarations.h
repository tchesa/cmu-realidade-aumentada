﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterMatchData
struct GameCenterMatchData_t109;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;

#include "codegen/il2cpp-codegen.h"

// System.Void GameCenterMatchData::.ctor(System.Collections.Generic.List`1<System.String>)
extern "C" void GameCenterMatchData__ctor_m704 (GameCenterMatchData_t109 * __this, List_1_t78 * ___ids, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GameCenterMatchData::get_playerIDs()
extern "C" List_1_t78 * GameCenterMatchData_get_playerIDs_m705 (GameCenterMatchData_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
