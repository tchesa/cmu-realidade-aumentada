﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_64.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_42.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26639_gshared (InternalEnumerator_1_t3293 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26639(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3293 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26639_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26640_gshared (InternalEnumerator_1_t3293 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26640(__this, method) (( void (*) (InternalEnumerator_1_t3293 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26640_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26641_gshared (InternalEnumerator_1_t3293 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26641(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3293 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26641_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26642_gshared (InternalEnumerator_1_t3293 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26642(__this, method) (( void (*) (InternalEnumerator_1_t3293 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26642_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26643_gshared (InternalEnumerator_1_t3293 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26643(__this, method) (( bool (*) (InternalEnumerator_1_t3293 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26643_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::get_Current()
extern "C" KeyValuePair_2_t3292  InternalEnumerator_1_get_Current_m26644_gshared (InternalEnumerator_1_t3293 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26644(__this, method) (( KeyValuePair_2_t3292  (*) (InternalEnumerator_1_t3293 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26644_gshared)(__this, method)
