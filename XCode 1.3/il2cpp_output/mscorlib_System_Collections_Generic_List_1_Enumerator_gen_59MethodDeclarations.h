﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t938;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_59.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22984_gshared (Enumerator_t3077 * __this, List_1_t938 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m22984(__this, ___l, method) (( void (*) (Enumerator_t3077 *, List_1_t938 *, const MethodInfo*))Enumerator__ctor_m22984_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m22985_gshared (Enumerator_t3077 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m22985(__this, method) (( void (*) (Enumerator_t3077 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m22985_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22986_gshared (Enumerator_t3077 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22986(__this, method) (( Object_t * (*) (Enumerator_t3077 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22986_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C" void Enumerator_Dispose_m22987_gshared (Enumerator_t3077 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22987(__this, method) (( void (*) (Enumerator_t3077 *, const MethodInfo*))Enumerator_Dispose_m22987_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::VerifyState()
extern "C" void Enumerator_VerifyState_m22988_gshared (Enumerator_t3077 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22988(__this, method) (( void (*) (Enumerator_t3077 *, const MethodInfo*))Enumerator_VerifyState_m22988_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22989_gshared (Enumerator_t3077 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22989(__this, method) (( bool (*) (Enumerator_t3077 *, const MethodInfo*))Enumerator_MoveNext_m22989_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern "C" int32_t Enumerator_get_Current_m22990_gshared (Enumerator_t3077 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22990(__this, method) (( int32_t (*) (Enumerator_t3077 *, const MethodInfo*))Enumerator_get_Current_m22990_gshared)(__this, method)
