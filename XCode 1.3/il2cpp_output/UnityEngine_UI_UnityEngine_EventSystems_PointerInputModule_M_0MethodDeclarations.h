﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t655;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::.ctor()
extern "C" void MouseButtonEventData__ctor_m3003 (MouseButtonEventData_t655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::PressedThisFrame()
extern "C" bool MouseButtonEventData_PressedThisFrame_m3004 (MouseButtonEventData_t655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::ReleasedThisFrame()
extern "C" bool MouseButtonEventData_ReleasedThisFrame_m3005 (MouseButtonEventData_t655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
