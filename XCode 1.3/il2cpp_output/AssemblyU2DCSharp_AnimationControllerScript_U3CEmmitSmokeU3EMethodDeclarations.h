﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript/<EmmitSmoke>c__Iterator15
struct U3CEmmitSmokeU3Ec__Iterator15_t370;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript/<EmmitSmoke>c__Iterator15::.ctor()
extern "C" void U3CEmmitSmokeU3Ec__Iterator15__ctor_m1876 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<EmmitSmoke>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CEmmitSmokeU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1877 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<EmmitSmoke>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CEmmitSmokeU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m1878 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationControllerScript/<EmmitSmoke>c__Iterator15::MoveNext()
extern "C" bool U3CEmmitSmokeU3Ec__Iterator15_MoveNext_m1879 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<EmmitSmoke>c__Iterator15::Dispose()
extern "C" void U3CEmmitSmokeU3Ec__Iterator15_Dispose_m1880 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<EmmitSmoke>c__Iterator15::Reset()
extern "C" void U3CEmmitSmokeU3Ec__Iterator15_Reset_m1881 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
