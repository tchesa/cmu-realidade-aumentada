﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.InitDelegate
struct InitDelegate_t241;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t242;
// Facebook.Unity.FacebookGameObject
struct FacebookGameObject_t229;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_ScriptableObject.h"

// Facebook.Unity.FB
struct  FB_t240  : public ScriptableObject_t77
{
};
struct FB_t240_StaticFields{
	// Facebook.Unity.InitDelegate Facebook.Unity.FB::onInitComplete
	InitDelegate_t241 * ___onInitComplete_1;
	// Facebook.Unity.HideUnityDelegate Facebook.Unity.FB::onHideUnity
	HideUnityDelegate_t242 * ___onHideUnity_2;
	// Facebook.Unity.FacebookGameObject Facebook.Unity.FB::facebook
	FacebookGameObject_t229 * ___facebook_3;
	// System.String Facebook.Unity.FB::authResponse
	String_t* ___authResponse_4;
	// System.Boolean Facebook.Unity.FB::isInitCalled
	bool ___isInitCalled_5;
	// System.String Facebook.Unity.FB::appId
	String_t* ___appId_6;
	// System.Boolean Facebook.Unity.FB::cookie
	bool ___cookie_7;
	// System.Boolean Facebook.Unity.FB::logging
	bool ___logging_8;
	// System.Boolean Facebook.Unity.FB::status
	bool ___status_9;
	// System.Boolean Facebook.Unity.FB::xfbml
	bool ___xfbml_10;
	// System.Boolean Facebook.Unity.FB::frictionlessRequests
	bool ___frictionlessRequests_11;
	// System.String Facebook.Unity.FB::facebookDomain
	String_t* ___facebookDomain_12;
};
