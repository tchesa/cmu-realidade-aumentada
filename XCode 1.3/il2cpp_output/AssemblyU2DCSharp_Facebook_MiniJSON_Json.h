﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t307;

#include "mscorlib_System_Object.h"

// Facebook.MiniJSON.Json
struct  Json_t306  : public Object_t
{
};
struct Json_t306_StaticFields{
	// System.Globalization.NumberFormatInfo Facebook.MiniJSON.Json::numberFormat
	NumberFormatInfo_t307 * ___numberFormat_0;
};
