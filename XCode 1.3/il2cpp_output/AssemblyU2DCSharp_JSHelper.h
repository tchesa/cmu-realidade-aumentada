﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// JSHelper
struct  JSHelper_t190  : public MonoBehaviour_t18
{
	// System.String JSHelper::leaderboardId
	String_t* ___leaderboardId_1;
	// System.String JSHelper::TEST_ACHIEVEMENT_1_ID
	String_t* ___TEST_ACHIEVEMENT_1_ID_2;
	// System.String JSHelper::TEST_ACHIEVEMENT_2_ID
	String_t* ___TEST_ACHIEVEMENT_2_ID_3;
};
