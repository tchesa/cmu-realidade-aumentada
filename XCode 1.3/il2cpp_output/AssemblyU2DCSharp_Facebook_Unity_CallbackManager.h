﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t225;

#include "mscorlib_System_Object.h"

// Facebook.Unity.CallbackManager
struct  CallbackManager_t224  : public Object_t
{
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.CallbackManager::facebookDelegates
	Object_t* ___facebookDelegates_0;
	// System.Int32 Facebook.Unity.CallbackManager::nextAsyncId
	int32_t ___nextAsyncId_1;
};
