﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ISN_Singleton_1_gen_6MethodDeclarations.h"

// System.Void ISN_Singleton`1<IOSNotificationController>::.ctor()
#define ISN_Singleton_1__ctor_m2240(__this, method) (( void (*) (ISN_Singleton_1_t141 *, const MethodInfo*))ISN_Singleton_1__ctor_m16858_gshared)(__this, method)
// System.Void ISN_Singleton`1<IOSNotificationController>::.cctor()
#define ISN_Singleton_1__cctor_m17619(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1__cctor_m16859_gshared)(__this /* static, unused */, method)
// T ISN_Singleton`1<IOSNotificationController>::get_instance()
#define ISN_Singleton_1_get_instance_m2338(__this /* static, unused */, method) (( IOSNotificationController_t140 * (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_instance_m16860_gshared)(__this /* static, unused */, method)
// System.Boolean ISN_Singleton`1<IOSNotificationController>::get_HasInstance()
#define ISN_Singleton_1_get_HasInstance_m17620(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_HasInstance_m16861_gshared)(__this /* static, unused */, method)
// System.Boolean ISN_Singleton`1<IOSNotificationController>::get_IsDestroyed()
#define ISN_Singleton_1_get_IsDestroyed_m17621(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))ISN_Singleton_1_get_IsDestroyed_m16862_gshared)(__this /* static, unused */, method)
// System.Void ISN_Singleton`1<IOSNotificationController>::OnDestroy()
#define ISN_Singleton_1_OnDestroy_m17622(__this, method) (( void (*) (ISN_Singleton_1_t141 *, const MethodInfo*))ISN_Singleton_1_OnDestroy_m16863_gshared)(__this, method)
// System.Void ISN_Singleton`1<IOSNotificationController>::OnApplicationQuit()
#define ISN_Singleton_1_OnApplicationQuit_m17623(__this, method) (( void (*) (ISN_Singleton_1_t141 *, const MethodInfo*))ISN_Singleton_1_OnApplicationQuit_m16864_gshared)(__this, method)
