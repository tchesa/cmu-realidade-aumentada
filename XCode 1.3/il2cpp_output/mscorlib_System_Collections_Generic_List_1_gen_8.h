﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.FacebookSettings/UrlSchemes[]
struct UrlSchemesU5BU5D_t2770;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>
struct  List_1_t247  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>::_items
	UrlSchemesU5BU5D_t2770* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>::_version
	int32_t ____version_3;
};
struct List_1_t247_StaticFields{
	// T[] System.Collections.Generic.List`1<Facebook.Unity.FacebookSettings/UrlSchemes>::EmptyArray
	UrlSchemesU5BU5D_t2770* ___EmptyArray_4;
};
