﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ShimEnumerator_t3303;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3290;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m26727_gshared (ShimEnumerator_t3303 * __this, Dictionary_2_t3290 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m26727(__this, ___host, method) (( void (*) (ShimEnumerator_t3303 *, Dictionary_2_t3290 *, const MethodInfo*))ShimEnumerator__ctor_m26727_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m26728_gshared (ShimEnumerator_t3303 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m26728(__this, method) (( bool (*) (ShimEnumerator_t3303 *, const MethodInfo*))ShimEnumerator_MoveNext_m26728_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Entry()
extern "C" DictionaryEntry_t58  ShimEnumerator_get_Entry_m26729_gshared (ShimEnumerator_t3303 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m26729(__this, method) (( DictionaryEntry_t58  (*) (ShimEnumerator_t3303 *, const MethodInfo*))ShimEnumerator_get_Entry_m26729_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m26730_gshared (ShimEnumerator_t3303 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m26730(__this, method) (( Object_t * (*) (ShimEnumerator_t3303 *, const MethodInfo*))ShimEnumerator_get_Key_m26730_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m26731_gshared (ShimEnumerator_t3303 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m26731(__this, method) (( Object_t * (*) (ShimEnumerator_t3303 *, const MethodInfo*))ShimEnumerator_get_Value_m26731_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m26732_gshared (ShimEnumerator_t3303 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m26732(__this, method) (( Object_t * (*) (ShimEnumerator_t3303 *, const MethodInfo*))ShimEnumerator_get_Current_m26732_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Reset()
extern "C" void ShimEnumerator_Reset_m26733_gshared (ShimEnumerator_t3303 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m26733(__this, method) (( void (*) (ShimEnumerator_t3303 *, const MethodInfo*))ShimEnumerator_Reset_m26733_gshared)(__this, method)
