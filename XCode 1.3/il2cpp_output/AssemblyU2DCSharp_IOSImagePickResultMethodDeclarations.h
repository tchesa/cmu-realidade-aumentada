﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSImagePickResult
struct IOSImagePickResult_t149;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t33;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSImagePickResult::.ctor(System.String)
extern "C" void IOSImagePickResult__ctor_m839 (IOSImagePickResult_t149 * __this, String_t* ___ImageData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D IOSImagePickResult::get_image()
extern "C" Texture2D_t33 * IOSImagePickResult_get_image_m840 (IOSImagePickResult_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
