﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t3554;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2537;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3765;
// System.Exception
struct Exception_t40;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m29757_gshared (ArrayReadOnlyList_1_t3554 * __this, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m29757(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t3554 *, CustomAttributeTypedArgumentU5BU5D_t2537*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m29757_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29758_gshared (ArrayReadOnlyList_1_t3554 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29758(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3554 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29758_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t2101  ArrayReadOnlyList_1_get_Item_m29759_gshared (ArrayReadOnlyList_1_t3554 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m29759(__this, ___index, method) (( CustomAttributeTypedArgument_t2101  (*) (ArrayReadOnlyList_1_t3554 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m29759_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m29760_gshared (ArrayReadOnlyList_1_t3554 * __this, int32_t ___index, CustomAttributeTypedArgument_t2101  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m29760(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t3554 *, int32_t, CustomAttributeTypedArgument_t2101 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m29760_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m29761_gshared (ArrayReadOnlyList_1_t3554 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m29761(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t3554 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m29761_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m29762_gshared (ArrayReadOnlyList_1_t3554 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m29762(__this, method) (( bool (*) (ArrayReadOnlyList_1_t3554 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m29762_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m29763_gshared (ArrayReadOnlyList_1_t3554 * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m29763(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3554 *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))ArrayReadOnlyList_1_Add_m29763_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m29764_gshared (ArrayReadOnlyList_1_t3554 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m29764(__this, method) (( void (*) (ArrayReadOnlyList_1_t3554 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m29764_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m29765_gshared (ArrayReadOnlyList_1_t3554 * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m29765(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3554 *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m29765_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m29766_gshared (ArrayReadOnlyList_1_t3554 * __this, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m29766(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3554 *, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m29766_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m29767_gshared (ArrayReadOnlyList_1_t3554 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m29767(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t3554 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m29767_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m29768_gshared (ArrayReadOnlyList_1_t3554 * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m29768(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t3554 *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m29768_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m29769_gshared (ArrayReadOnlyList_1_t3554 * __this, int32_t ___index, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m29769(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3554 *, int32_t, CustomAttributeTypedArgument_t2101 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m29769_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m29770_gshared (ArrayReadOnlyList_1_t3554 * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m29770(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3554 *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m29770_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m29771_gshared (ArrayReadOnlyList_1_t3554 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m29771(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3554 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m29771_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern "C" Exception_t40 * ArrayReadOnlyList_1_ReadOnlyError_m29772_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m29772(__this /* static, unused */, method) (( Exception_t40 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m29772_gshared)(__this /* static, unused */, method)
