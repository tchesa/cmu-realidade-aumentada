﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t435;
// System.Action
struct Action_t81;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
extern "C" void SmartTerrainTrackerAbstractBehaviour_Awake_m4736 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnEnable_m4737 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDisable_m4738 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m4739 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
extern "C" void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m4740 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, Action_t81 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
extern "C" void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m4741 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, Action_t81 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m4742 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m4743 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m4744 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnVuforiaInitialized()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnVuforiaInitialized_m4745 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnVuforiaStarted()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnVuforiaStarted_m4746 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnPause_m4747 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetAutomaticStart(System.Boolean)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m4748 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, bool ___autoStart, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_AutomaticStart()
extern "C" bool SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m4749 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetSmartTerrainScaleToMM(System.Single)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m4750 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, float ___scaleToMM, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_ScaleToMM()
extern "C" float SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m4751 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
extern "C" void SmartTerrainTrackerAbstractBehaviour__ctor_m2755 (SmartTerrainTrackerAbstractBehaviour_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
