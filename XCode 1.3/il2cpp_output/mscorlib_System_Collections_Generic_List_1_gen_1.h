﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnionAssets.FLE.DataEventHandlerFunction[]
struct DataEventHandlerFunctionU5BU5D_t2675;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>
struct  List_1_t461  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>::_items
	DataEventHandlerFunctionU5BU5D_t2675* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>::_version
	int32_t ____version_3;
};
struct List_1_t461_StaticFields{
	// T[] System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>::EmptyArray
	DataEventHandlerFunctionU5BU5D_t2675* ___EmptyArray_4;
};
