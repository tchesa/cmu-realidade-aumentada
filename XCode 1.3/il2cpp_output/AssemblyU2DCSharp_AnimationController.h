﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t27;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t359;
// UnityEngine.Animator
struct Animator_t360;
// BezierTraveler
struct BezierTraveler_t342;
// UnityEngine.Transform[]
struct TransformU5BU5D_t32;
// UnityEngine.ParticleSystem
struct ParticleSystem_t332;
// UnityEngine.TrailRenderer
struct TrailRenderer_t361;
// UnityEngine.AudioSource
struct AudioSource_t20;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "AssemblyU2DCSharp_AnimationController_Audios.h"

// AnimationController
struct  AnimationController_t357  : public MonoBehaviour_t18
{
	// System.Boolean AnimationController::play
	bool ___play_1;
	// System.Boolean AnimationController::stop
	bool ___stop_2;
	// UnityEngine.GameObject AnimationController::lamp
	GameObject_t27 * ___lamp_3;
	// System.Int32 AnimationController::lampMatIndex
	int32_t ___lampMatIndex_4;
	// UnityEngine.SkinnedMeshRenderer AnimationController::lampMesh
	SkinnedMeshRenderer_t359 * ___lampMesh_5;
	// UnityEngine.Animator AnimationController::lampAnimator
	Animator_t360 * ___lampAnimator_6;
	// UnityEngine.Vector3 AnimationController::lampInitialPosition
	Vector3_t6  ___lampInitialPosition_7;
	// BezierTraveler AnimationController::lampTraveler
	BezierTraveler_t342 * ___lampTraveler_8;
	// BezierTraveler AnimationController::crazyTraveler
	BezierTraveler_t342 * ___crazyTraveler_9;
	// UnityEngine.Transform[] AnimationController::thinkingPositions
	TransformU5BU5D_t32* ___thinkingPositions_10;
	// UnityEngine.ParticleSystem AnimationController::electricGround
	ParticleSystem_t332 * ___electricGround_11;
	// UnityEngine.ParticleSystem AnimationController::electricBall
	ParticleSystem_t332 * ___electricBall_12;
	// UnityEngine.ParticleSystem AnimationController::electricBolt
	ParticleSystem_t332 * ___electricBolt_13;
	// UnityEngine.ParticleSystem AnimationController::magicAura
	ParticleSystem_t332 * ___magicAura_14;
	// UnityEngine.ParticleSystem AnimationController::bolts
	ParticleSystem_t332 * ___bolts_15;
	// UnityEngine.ParticleSystem AnimationController::lampShine
	ParticleSystem_t332 * ___lampShine_16;
	// UnityEngine.GameObject AnimationController::cameraPosition
	GameObject_t27 * ___cameraPosition_17;
	// UnityEngine.Color AnimationController::turnOffColor
	Color_t5  ___turnOffColor_18;
	// UnityEngine.Color AnimationController::emissionColor
	Color_t5  ___emissionColor_19;
	// UnityEngine.TrailRenderer AnimationController::trail
	TrailRenderer_t361 * ___trail_20;
	// AnimationController/Audios AnimationController::audios
	Audios_t355  ___audios_21;
	// UnityEngine.AudioSource AnimationController::audioPasseio
	AudioSource_t20 * ___audioPasseio_22;
	// System.Boolean AnimationController::clicou
	bool ___clicou_23;
	// UnityEngine.GameObject AnimationController::listener
	GameObject_t27 * ___listener_24;
	// System.Collections.IEnumerator AnimationController::coroutine
	Object_t * ___coroutine_25;
};
