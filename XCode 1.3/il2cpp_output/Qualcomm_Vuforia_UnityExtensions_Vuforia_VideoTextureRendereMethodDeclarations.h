﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VideoTextureRendererAbstractBehaviour
struct VideoTextureRendererAbstractBehaviour_t447;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
extern "C" void VideoTextureRendererAbstractBehaviour_Awake_m6354 (VideoTextureRendererAbstractBehaviour_t447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
extern "C" void VideoTextureRendererAbstractBehaviour_Start_m6355 (VideoTextureRendererAbstractBehaviour_t447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
extern "C" void VideoTextureRendererAbstractBehaviour_Update_m6356 (VideoTextureRendererAbstractBehaviour_t447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
extern "C" void VideoTextureRendererAbstractBehaviour_OnDestroy_m6357 (VideoTextureRendererAbstractBehaviour_t447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m6358 (VideoTextureRendererAbstractBehaviour_t447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
extern "C" void VideoTextureRendererAbstractBehaviour__ctor_m2764 (VideoTextureRendererAbstractBehaviour_t447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
