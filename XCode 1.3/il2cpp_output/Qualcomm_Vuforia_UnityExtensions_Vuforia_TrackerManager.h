﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.TrackerManager
struct TrackerManager_t1058;

#include "mscorlib_System_Object.h"

// Vuforia.TrackerManager
struct  TrackerManager_t1058  : public Object_t
{
};
struct TrackerManager_t1058_StaticFields{
	// Vuforia.TrackerManager Vuforia.TrackerManager::mInstance
	TrackerManager_t1058 * ___mInstance_0;
};
