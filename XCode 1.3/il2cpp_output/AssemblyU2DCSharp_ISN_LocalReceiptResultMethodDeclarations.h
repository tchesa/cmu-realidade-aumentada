﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_LocalReceiptResult
struct ISN_LocalReceiptResult_t171;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_LocalReceiptResult::.ctor(System.String)
extern "C" void ISN_LocalReceiptResult__ctor_m939 (ISN_LocalReceiptResult_t171 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ISN_LocalReceiptResult::get_Receipt()
extern "C" ByteU5BU5D_t119* ISN_LocalReceiptResult_get_Receipt_m940 (ISN_LocalReceiptResult_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
