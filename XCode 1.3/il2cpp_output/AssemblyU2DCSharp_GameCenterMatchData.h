﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t78;

#include "mscorlib_System_Object.h"

// GameCenterMatchData
struct  GameCenterMatchData_t109  : public Object_t
{
	// System.Collections.Generic.List`1<System.String> GameCenterMatchData::_playerIDs
	List_1_t78 * ____playerIDs_0;
};
