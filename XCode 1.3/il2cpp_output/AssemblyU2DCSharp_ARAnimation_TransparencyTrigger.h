﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TransparencyAnimation
struct TransparencyAnimation_t340;

#include "mscorlib_System_Object.h"

// ARAnimation/TransparencyTrigger
struct  TransparencyTrigger_t339  : public Object_t
{
	// System.Single ARAnimation/TransparencyTrigger::show
	float ___show_0;
	// System.Single ARAnimation/TransparencyTrigger::hide
	float ___hide_1;
	// TransparencyAnimation ARAnimation/TransparencyTrigger::obj
	TransparencyAnimation_t340 * ___obj_2;
};
