﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_111.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29510_gshared (InternalEnumerator_1_t3528 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29510(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3528 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29510_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29511_gshared (InternalEnumerator_1_t3528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29511(__this, method) (( void (*) (InternalEnumerator_1_t3528 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29511_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29512_gshared (InternalEnumerator_1_t3528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29512(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3528 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29512_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29513_gshared (InternalEnumerator_1_t3528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29513(__this, method) (( void (*) (InternalEnumerator_1_t3528 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29513_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29514_gshared (InternalEnumerator_1_t3528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29514(__this, method) (( bool (*) (InternalEnumerator_1_t3528 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29514_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern "C" Slot_t1974  InternalEnumerator_1_get_Current_m29515_gshared (InternalEnumerator_1_t3528 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29515(__this, method) (( Slot_t1974  (*) (InternalEnumerator_1_t3528 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29515_gshared)(__this, method)
