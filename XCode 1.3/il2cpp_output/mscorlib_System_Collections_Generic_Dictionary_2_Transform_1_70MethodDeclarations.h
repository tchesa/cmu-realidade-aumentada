﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct Transform_1_t3302;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_42.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m26723_gshared (Transform_1_t3302 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m26723(__this, ___object, ___method, method) (( void (*) (Transform_1_t3302 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m26723_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t3292  Transform_1_Invoke_m26724_gshared (Transform_1_t3302 * __this, Object_t * ___key, ProfileData_t1064  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m26724(__this, ___key, ___value, method) (( KeyValuePair_2_t3292  (*) (Transform_1_t3302 *, Object_t *, ProfileData_t1064 , const MethodInfo*))Transform_1_Invoke_m26724_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m26725_gshared (Transform_1_t3302 * __this, Object_t * ___key, ProfileData_t1064  ___value, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m26725(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3302 *, Object_t *, ProfileData_t1064 , AsyncCallback_t12 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m26725_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3292  Transform_1_EndInvoke_m26726_gshared (Transform_1_t3302 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m26726(__this, ___result, method) (( KeyValuePair_2_t3292  (*) (Transform_1_t3302 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m26726_gshared)(__this, ___result, method)
