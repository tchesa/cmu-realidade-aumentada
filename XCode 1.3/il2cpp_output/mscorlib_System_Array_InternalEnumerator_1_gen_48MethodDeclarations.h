﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_48.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__4.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23754_gshared (InternalEnumerator_1_t3122 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23754(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3122 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23754_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23755_gshared (InternalEnumerator_1_t3122 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23755(__this, method) (( void (*) (InternalEnumerator_1_t3122 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23755_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23756_gshared (InternalEnumerator_1_t3122 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23756(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3122 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23756_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23757_gshared (InternalEnumerator_1_t3122 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23757(__this, method) (( void (*) (InternalEnumerator_1_t3122 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23757_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23758_gshared (InternalEnumerator_1_t3122 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23758(__this, method) (( bool (*) (InternalEnumerator_1_t3122 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23758_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::get_Current()
extern "C" WordResultData_t973  InternalEnumerator_1_get_Current_m23759_gshared (InternalEnumerator_1_t3122 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23759(__this, method) (( WordResultData_t973  (*) (InternalEnumerator_1_t3122 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23759_gshared)(__this, method)
