﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m28802(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3457 *, String_t*, GUIStyle_t184 *, const MethodInfo*))KeyValuePair_2__ctor_m16119_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m28803(__this, method) (( String_t* (*) (KeyValuePair_2_t3457 *, const MethodInfo*))KeyValuePair_2_get_Key_m16120_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m28804(__this, ___value, method) (( void (*) (KeyValuePair_2_t3457 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m16121_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m28805(__this, method) (( GUIStyle_t184 * (*) (KeyValuePair_2_t3457 *, const MethodInfo*))KeyValuePair_2_get_Value_m16122_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m28806(__this, ___value, method) (( void (*) (KeyValuePair_2_t3457 *, GUIStyle_t184 *, const MethodInfo*))KeyValuePair_2_set_Value_m16123_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m28807(__this, method) (( String_t* (*) (KeyValuePair_2_t3457 *, const MethodInfo*))KeyValuePair_2_ToString_m16124_gshared)(__this, method)
