﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2604;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15876_gshared (Enumerator_t2613 * __this, Dictionary_2_t2604 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15876(__this, ___dictionary, method) (( void (*) (Enumerator_t2613 *, Dictionary_2_t2604 *, const MethodInfo*))Enumerator__ctor_m15876_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15877_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15877(__this, method) (( Object_t * (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15877_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15878_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15878(__this, method) (( void (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15878_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t58  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15879_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15879(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15879_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15880_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15880(__this, method) (( Object_t * (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15880_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15881_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15881(__this, method) (( Object_t * (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15881_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15882_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15882(__this, method) (( bool (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_MoveNext_m15882_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t2607  Enumerator_get_Current_m15883_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15883(__this, method) (( KeyValuePair_2_t2607  (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_get_Current_m15883_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m15884_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15884(__this, method) (( Object_t * (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_get_CurrentKey_m15884_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m15885_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15885(__this, method) (( int32_t (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_get_CurrentValue_m15885_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C" void Enumerator_Reset_m15886_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_Reset_m15886(__this, method) (( void (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_Reset_m15886_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m15887_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15887(__this, method) (( void (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_VerifyState_m15887_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15888_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15888(__this, method) (( void (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_VerifyCurrent_m15888_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m15889_gshared (Enumerator_t2613 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15889(__this, method) (( void (*) (Enumerator_t2613 *, const MethodInfo*))Enumerator_Dispose_m15889_gshared)(__this, method)
