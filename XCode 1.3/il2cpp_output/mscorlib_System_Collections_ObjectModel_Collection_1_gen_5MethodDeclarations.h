﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>
struct Collection_1_t3277;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3273;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t1132;
// System.Collections.Generic.IList`1<Vuforia.TargetFinder/TargetSearchResult>
struct IList_1_t3276;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void Collection_1__ctor_m26388_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1__ctor_m26388(__this, method) (( void (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1__ctor_m26388_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26389_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26389(__this, method) (( bool (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26389_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m26390_gshared (Collection_1_t3277 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m26390(__this, ___array, ___index, method) (( void (*) (Collection_1_t3277 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m26390_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m26391_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m26391(__this, method) (( Object_t * (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m26391_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m26392_gshared (Collection_1_t3277 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m26392(__this, ___value, method) (( int32_t (*) (Collection_1_t3277 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m26392_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m26393_gshared (Collection_1_t3277 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m26393(__this, ___value, method) (( bool (*) (Collection_1_t3277 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m26393_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m26394_gshared (Collection_1_t3277 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m26394(__this, ___value, method) (( int32_t (*) (Collection_1_t3277 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m26394_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m26395_gshared (Collection_1_t3277 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m26395(__this, ___index, ___value, method) (( void (*) (Collection_1_t3277 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m26395_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m26396_gshared (Collection_1_t3277 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m26396(__this, ___value, method) (( void (*) (Collection_1_t3277 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m26396_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m26397_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m26397(__this, method) (( bool (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m26397_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m26398_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m26398(__this, method) (( Object_t * (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m26398_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m26399_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m26399(__this, method) (( bool (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m26399_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m26400_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m26400(__this, method) (( bool (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m26400_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m26401_gshared (Collection_1_t3277 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m26401(__this, ___index, method) (( Object_t * (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m26401_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m26402_gshared (Collection_1_t3277 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m26402(__this, ___index, ___value, method) (( void (*) (Collection_1_t3277 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m26402_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void Collection_1_Add_m26403_gshared (Collection_1_t3277 * __this, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define Collection_1_Add_m26403(__this, ___item, method) (( void (*) (Collection_1_t3277 *, TargetSearchResult_t1050 , const MethodInfo*))Collection_1_Add_m26403_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void Collection_1_Clear_m26404_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_Clear_m26404(__this, method) (( void (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_Clear_m26404_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m26405_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m26405(__this, method) (( void (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_ClearItems_m26405_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool Collection_1_Contains_m26406_gshared (Collection_1_t3277 * __this, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define Collection_1_Contains_m26406(__this, ___item, method) (( bool (*) (Collection_1_t3277 *, TargetSearchResult_t1050 , const MethodInfo*))Collection_1_Contains_m26406_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m26407_gshared (Collection_1_t3277 * __this, TargetSearchResultU5BU5D_t3273* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m26407(__this, ___array, ___index, method) (( void (*) (Collection_1_t3277 *, TargetSearchResultU5BU5D_t3273*, int32_t, const MethodInfo*))Collection_1_CopyTo_m26407_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m26408_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m26408(__this, method) (( Object_t* (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_GetEnumerator_m26408_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m26409_gshared (Collection_1_t3277 * __this, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m26409(__this, ___item, method) (( int32_t (*) (Collection_1_t3277 *, TargetSearchResult_t1050 , const MethodInfo*))Collection_1_IndexOf_m26409_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m26410_gshared (Collection_1_t3277 * __this, int32_t ___index, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define Collection_1_Insert_m26410(__this, ___index, ___item, method) (( void (*) (Collection_1_t3277 *, int32_t, TargetSearchResult_t1050 , const MethodInfo*))Collection_1_Insert_m26410_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m26411_gshared (Collection_1_t3277 * __this, int32_t ___index, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m26411(__this, ___index, ___item, method) (( void (*) (Collection_1_t3277 *, int32_t, TargetSearchResult_t1050 , const MethodInfo*))Collection_1_InsertItem_m26411_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool Collection_1_Remove_m26412_gshared (Collection_1_t3277 * __this, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define Collection_1_Remove_m26412(__this, ___item, method) (( bool (*) (Collection_1_t3277 *, TargetSearchResult_t1050 , const MethodInfo*))Collection_1_Remove_m26412_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m26413_gshared (Collection_1_t3277 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m26413(__this, ___index, method) (( void (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m26413_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m26414_gshared (Collection_1_t3277 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m26414(__this, ___index, method) (( void (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m26414_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m26415_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m26415(__this, method) (( int32_t (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_get_Count_m26415_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t1050  Collection_1_get_Item_m26416_gshared (Collection_1_t3277 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m26416(__this, ___index, method) (( TargetSearchResult_t1050  (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_get_Item_m26416_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m26417_gshared (Collection_1_t3277 * __this, int32_t ___index, TargetSearchResult_t1050  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m26417(__this, ___index, ___value, method) (( void (*) (Collection_1_t3277 *, int32_t, TargetSearchResult_t1050 , const MethodInfo*))Collection_1_set_Item_m26417_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m26418_gshared (Collection_1_t3277 * __this, int32_t ___index, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m26418(__this, ___index, ___item, method) (( void (*) (Collection_1_t3277 *, int32_t, TargetSearchResult_t1050 , const MethodInfo*))Collection_1_SetItem_m26418_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m26419_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m26419(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m26419_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ConvertItem(System.Object)
extern "C" TargetSearchResult_t1050  Collection_1_ConvertItem_m26420_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m26420(__this /* static, unused */, ___item, method) (( TargetSearchResult_t1050  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m26420_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m26421_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m26421(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m26421_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m26422_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m26422(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m26422_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m26423_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m26423(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m26423_gshared)(__this /* static, unused */, ___list, method)
