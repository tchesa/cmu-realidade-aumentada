﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Collections.Generic.IEnumerator`1<System.Int16>
struct IEnumerator_1_t3879;
// System.Array
struct Array_t;
// System.SByte[]
struct SByteU5BU5D_t2386;
// System.Collections.Generic.IEnumerator`1<System.SByte>
struct IEnumerator_1_t3880;
// System.Int64[]
struct Int64U5BU5D_t2511;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t3881;
// System.Object[]
struct ObjectU5BU5D_t34;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t3840;
// System.Comparison`1<System.Object>
struct Comparison_1_t2591;
// System.Predicate`1<System.Object>
struct Predicate_1_t2585;
// System.Action`1<System.Object>
struct Action_1_t2701;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t3513;
// System.Object
struct Object_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2578;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1891;
// System.Collections.Generic.IEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct IEnumerator_1_t3882;
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t1973;
// System.Collections.Generic.IEnumerator`1<System.Collections.Hashtable/Slot>
struct IEnumerator_1_t3883;
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t1978;
// System.Collections.Generic.IEnumerator`1<System.Collections.SortedList/Slot>
struct IEnumerator_1_t3884;
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t2056;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILTokenInfo>
struct IEnumerator_1_t3885;
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t2057;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>
struct IEnumerator_1_t3886;
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t2058;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct IEnumerator_1_t3887;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2537;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3765;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2538;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3767;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t2539;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct IComparer_1_t3888;
// System.Array/Swapper
struct Swapper_t1855;
// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t3553;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t2540;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct IComparer_1_t3889;
// System.Comparison`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparison_1_t3564;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t3567;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t3568;
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t2134;
// System.Collections.Generic.IEnumerator`1<System.Resources.ResourceReader/ResourceInfo>
struct IEnumerator_1_t3890;
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t2135;
// System.Collections.Generic.IEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>
struct IEnumerator_1_t3891;
// System.DateTime[]
struct DateTimeU5BU5D_t2560;
// System.Collections.Generic.IEnumerator`1<System.DateTime>
struct IEnumerator_1_t3892;
// System.Decimal[]
struct DecimalU5BU5D_t2561;
// System.Collections.Generic.IEnumerator`1<System.Decimal>
struct IEnumerator_1_t3893;
// System.TimeSpan[]
struct TimeSpanU5BU5D_t2562;
// System.Collections.Generic.IEnumerator`1<System.TimeSpan>
struct IEnumerator_1_t3894;
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t2563;
// System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
struct IEnumerator_1_t3895;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_100.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_100MethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_SByte.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_LocaleMethodDeclarations.h"
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
#include "mscorlib_System_SByteMethodDeclarations.h"
#include "mscorlib_System_RankException.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101MethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_Int64MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen_7.h"
#include "mscorlib_System_Predicate_1_gen_4.h"
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_29.h"
#include "mscorlib_System_Action_1_gen_29MethodDeclarations.h"
#include "mscorlib_System_Converter_2_gen.h"
#include "mscorlib_System_Converter_2_genMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen.h"
#include "mscorlib_System_InvalidOperationException.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_110.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_110MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_111.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_111MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_117.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_117MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_118.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_118MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_119.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_119MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgumentMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_126.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_126MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgumentMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_127.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_127MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeData.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen_0.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_15.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_15MethodDeclarations.h"
#include "mscorlib_System_Array_Swapper.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Array_SwapperMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Comparison_1_gen_72.h"
#include "mscorlib_System_Comparison_1_gen_72MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen_1.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_16.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_16MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen_73.h"
#include "mscorlib_System_Comparison_1_gen_73MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_gen.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_genMethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_gen.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_genMethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_128.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_128MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_129.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_129MethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_134.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_134MethodDeclarations.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_DecimalMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_135.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_135MethodDeclarations.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_136.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_136MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_137.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_137MethodDeclarations.h"

// System.Void System.Array::SetGenericValueImpl<System.Int16>(System.Int32,T&)
extern "C" void Array_SetGenericValueImpl_TisInt16_t561_m31137_gshared (Array_t * __this, int32_t ___pos, int16_t* ___value, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisInt16_t561_m31137(__this, ___pos, ___value, method) (( void (*) (Array_t *, int32_t, int16_t*, const MethodInfo*))Array_SetGenericValueImpl_TisInt16_t561_m31137_gshared)(__this, ___pos, ___value, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int16>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t561_m31138_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t561_m31138(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t561_m31138_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C" int8_t Array_InternalArray__get_Item_TisSByte_t559_m31139_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t559_m31139(__this, ___index, method) (( int8_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t559_m31139_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.SByte>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisSByte_t559_m31339_gshared (Array_t * __this, int32_t p0, int8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSByte_t559_m31339(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, int8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisSByte_t559_m31339_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.SByte>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisSByte_t559_m31140_gshared (Array_t * __this, int8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisSByte_t559_m31140(__this, ___item, method) (( void (*) (Array_t *, int8_t, const MethodInfo*))Array_InternalArray__ICollection_Add_TisSByte_t559_m31140_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.SByte>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisSByte_t559_m31141_gshared (Array_t * __this, int8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisSByte_t559_m31141(__this, ___item, method) (( bool (*) (Array_t *, int8_t, const MethodInfo*))Array_InternalArray__ICollection_Contains_TisSByte_t559_m31141_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.SByte>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSByte_t559_m31142_gshared (Array_t * __this, SByteU5BU5D_t2386* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisSByte_t559_m31142(__this, ___array, ___index, method) (( void (*) (Array_t *, SByteU5BU5D_t2386*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisSByte_t559_m31142_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.SByte>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisSByte_t559_m31143_gshared (Array_t * __this, int8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisSByte_t559_m31143(__this, ___item, method) (( bool (*) (Array_t *, int8_t, const MethodInfo*))Array_InternalArray__ICollection_Remove_TisSByte_t559_m31143_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.SByte>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisSByte_t559_m31144_gshared (Array_t * __this, int8_t ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisSByte_t559_m31144(__this, ___item, method) (( int32_t (*) (Array_t *, int8_t, const MethodInfo*))Array_InternalArray__IndexOf_TisSByte_t559_m31144_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.SByte>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisSByte_t559_m31145_gshared (Array_t * __this, int32_t ___index, int8_t ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisSByte_t559_m31145(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, int8_t, const MethodInfo*))Array_InternalArray__Insert_TisSByte_t559_m31145_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.SByte>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisSByte_t559_m31146_gshared (Array_t * __this, int32_t ___index, int8_t ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisSByte_t559_m31146(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, int8_t, const MethodInfo*))Array_InternalArray__set_Item_TisSByte_t559_m31146_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.SByte>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisSByte_t559_m31340_gshared (Array_t * __this, int32_t p0, int8_t* p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisSByte_t559_m31340(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, int8_t*, const MethodInfo*))Array_SetGenericValueImpl_TisSByte_t559_m31340_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.SByte>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t559_m31147_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t559_m31147(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t559_m31147_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C" int64_t Array_InternalArray__get_Item_TisInt64_t507_m31148_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t507_m31148(__this, ___index, method) (( int64_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t507_m31148_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Int64>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisInt64_t507_m31341_gshared (Array_t * __this, int32_t p0, int64_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisInt64_t507_m31341(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, int64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisInt64_t507_m31341_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Int64>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t507_m31149_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisInt64_t507_m31149(__this, ___item, method) (( void (*) (Array_t *, int64_t, const MethodInfo*))Array_InternalArray__ICollection_Add_TisInt64_t507_m31149_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Int64>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisInt64_t507_m31150_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisInt64_t507_m31150(__this, ___item, method) (( bool (*) (Array_t *, int64_t, const MethodInfo*))Array_InternalArray__ICollection_Contains_TisInt64_t507_m31150_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Int64>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t507_m31151_gshared (Array_t * __this, Int64U5BU5D_t2511* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisInt64_t507_m31151(__this, ___array, ___index, method) (( void (*) (Array_t *, Int64U5BU5D_t2511*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisInt64_t507_m31151_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Int64>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisInt64_t507_m31152_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisInt64_t507_m31152(__this, ___item, method) (( bool (*) (Array_t *, int64_t, const MethodInfo*))Array_InternalArray__ICollection_Remove_TisInt64_t507_m31152_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Int64>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisInt64_t507_m31153_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisInt64_t507_m31153(__this, ___item, method) (( int32_t (*) (Array_t *, int64_t, const MethodInfo*))Array_InternalArray__IndexOf_TisInt64_t507_m31153_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Int64>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisInt64_t507_m31154_gshared (Array_t * __this, int32_t ___index, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisInt64_t507_m31154(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, int64_t, const MethodInfo*))Array_InternalArray__Insert_TisInt64_t507_m31154_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Int64>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisInt64_t507_m31155_gshared (Array_t * __this, int32_t ___index, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisInt64_t507_m31155(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, int64_t, const MethodInfo*))Array_InternalArray__set_Item_TisInt64_t507_m31155_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Int64>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisInt64_t507_m31342_gshared (Array_t * __this, int32_t p0, int64_t* p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisInt64_t507_m31342(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, int64_t*, const MethodInfo*))Array_SetGenericValueImpl_TisInt64_t507_m31342_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int64>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t507_m31156_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t507_m31156(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t507_m31156_gshared)(__this, method)
// System.Void System.Array::Sort<System.Object>(T[])
extern "C" void Array_Sort_TisObject_t_m31157_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, const MethodInfo* method);
#define Array_Sort_TisObject_t_m31157(__this /* static, unused */, ___array, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, const MethodInfo*))Array_Sort_TisObject_t_m31157_gshared)(__this /* static, unused */, ___array, method)
// System.Void System.Array::Sort<System.Object,System.Object>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_Sort_TisObject_t_TisObject_t_m30235_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* p0, ObjectU5BU5D_t34* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_Sort_TisObject_t_TisObject_t_m30235(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisObject_t_TisObject_t_m30235_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[])
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31158_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___keys, ObjectU5BU5D_t34* ___items, const MethodInfo* method);
#define Array_Sort_TisObject_t_TisObject_t_m31158(__this /* static, unused */, ___keys, ___items, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, const MethodInfo*))Array_Sort_TisObject_t_TisObject_t_m31158_gshared)(__this /* static, unused */, ___keys, ___items, method)
// System.Void System.Array::Sort<System.Object>(T[],System.Collections.Generic.IComparer`1<T>)
extern "C" void Array_Sort_TisObject_t_m31159_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t* ___comparer, const MethodInfo* method);
#define Array_Sort_TisObject_t_m31159(__this /* static, unused */, ___array, ___comparer, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t*, const MethodInfo*))Array_Sort_TisObject_t_m31159_gshared)(__this /* static, unused */, ___array, ___comparer, method)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Collections.Generic.IComparer`1<TKey>)
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31160_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___keys, ObjectU5BU5D_t34* ___items, Object_t* ___comparer, const MethodInfo* method);
#define Array_Sort_TisObject_t_TisObject_t_m31160(__this /* static, unused */, ___keys, ___items, ___comparer, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, Object_t*, const MethodInfo*))Array_Sort_TisObject_t_TisObject_t_m31160_gshared)(__this /* static, unused */, ___keys, ___items, ___comparer, method)
// System.Void System.Array::Sort<System.Object>(T[],System.Int32,System.Int32)
extern "C" void Array_Sort_TisObject_t_m15427_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___index, int32_t ___length, const MethodInfo* method);
#define Array_Sort_TisObject_t_m15427(__this /* static, unused */, ___array, ___index, ___length, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, const MethodInfo*))Array_Sort_TisObject_t_m15427_gshared)(__this /* static, unused */, ___array, ___index, ___length, method)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Int32,System.Int32)
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31161_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___keys, ObjectU5BU5D_t34* ___items, int32_t ___index, int32_t ___length, const MethodInfo* method);
#define Array_Sort_TisObject_t_TisObject_t_m31161(__this /* static, unused */, ___keys, ___items, ___index, ___length, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, int32_t, int32_t, const MethodInfo*))Array_Sort_TisObject_t_TisObject_t_m31161_gshared)(__this /* static, unused */, ___keys, ___items, ___index, ___length, method)
// System.Void System.Array::Sort<System.Object>(T[],System.Comparison`1<T>)
extern "C" void Array_Sort_TisObject_t_m31162_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Comparison_1_t2591 * ___comparison, const MethodInfo* method);
#define Array_Sort_TisObject_t_m31162(__this /* static, unused */, ___array, ___comparison, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Comparison_1_t2591 *, const MethodInfo*))Array_Sort_TisObject_t_m31162_gshared)(__this /* static, unused */, ___array, ___comparison, method)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C" void Array_Sort_TisObject_t_m30267_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* p0, int32_t p1, Comparison_1_t2591 * p2, const MethodInfo* method);
#define Array_Sort_TisObject_t_m30267(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, Comparison_1_t2591 *, const MethodInfo*))Array_Sort_TisObject_t_m30267_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Boolean System.Array::TrueForAll<System.Object>(T[],System.Predicate`1<T>)
extern "C" bool Array_TrueForAll_TisObject_t_m31163_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_TrueForAll_TisObject_t_m31163(__this /* static, unused */, ___array, ___match, method) (( bool (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Predicate_1_t2585 *, const MethodInfo*))Array_TrueForAll_TisObject_t_m31163_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Void System.Array::ForEach<System.Object>(T[],System.Action`1<T>)
extern "C" void Array_ForEach_TisObject_t_m31164_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Action_1_t2701 * ___action, const MethodInfo* method);
#define Array_ForEach_TisObject_t_m31164(__this /* static, unused */, ___array, ___action, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Action_1_t2701 *, const MethodInfo*))Array_ForEach_TisObject_t_m31164_gshared)(__this /* static, unused */, ___array, ___action, method)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C" ObjectU5BU5D_t34* Array_ConvertAll_TisObject_t_TisObject_t_m31165_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Converter_2_t3513 * ___converter, const MethodInfo* method);
#define Array_ConvertAll_TisObject_t_TisObject_t_m31165(__this /* static, unused */, ___array, ___converter, method) (( ObjectU5BU5D_t34* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Converter_2_t3513 *, const MethodInfo*))Array_ConvertAll_TisObject_t_TisObject_t_m31165_gshared)(__this /* static, unused */, ___array, ___converter, method)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Predicate`1<T>)
extern "C" int32_t Array_FindLastIndex_TisObject_t_m31166_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_FindLastIndex_TisObject_t_m31166(__this /* static, unused */, ___array, ___match, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Predicate_1_t2585 *, const MethodInfo*))Array_FindLastIndex_TisObject_t_m31166_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Int32 System.Array::FindLastIndex<System.Object>(!!0[],System.Int32,System.Int32,System.Predicate`1<!!0>)
extern "C" int32_t Array_FindLastIndex_TisObject_t_m31167_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* p0, int32_t p1, int32_t p2, Predicate_1_t2585 * p3, const MethodInfo* method);
#define Array_FindLastIndex_TisObject_t_m31167(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Predicate_1_t2585 *, const MethodInfo*))Array_FindLastIndex_TisObject_t_m31167_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
extern "C" int32_t Array_FindLastIndex_TisObject_t_m31168_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___startIndex, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_FindLastIndex_TisObject_t_m31168(__this /* static, unused */, ___array, ___startIndex, ___match, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, Predicate_1_t2585 *, const MethodInfo*))Array_FindLastIndex_TisObject_t_m31168_gshared)(__this /* static, unused */, ___array, ___startIndex, ___match, method)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Predicate`1<T>)
extern "C" int32_t Array_FindIndex_TisObject_t_m31169_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_FindIndex_TisObject_t_m31169(__this /* static, unused */, ___array, ___match, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Predicate_1_t2585 *, const MethodInfo*))Array_FindIndex_TisObject_t_m31169_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Int32 System.Array::FindIndex<System.Object>(!!0[],System.Int32,System.Int32,System.Predicate`1<!!0>)
extern "C" int32_t Array_FindIndex_TisObject_t_m31170_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* p0, int32_t p1, int32_t p2, Predicate_1_t2585 * p3, const MethodInfo* method);
#define Array_FindIndex_TisObject_t_m31170(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Predicate_1_t2585 *, const MethodInfo*))Array_FindIndex_TisObject_t_m31170_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
extern "C" int32_t Array_FindIndex_TisObject_t_m31171_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___startIndex, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_FindIndex_TisObject_t_m31171(__this /* static, unused */, ___array, ___startIndex, ___match, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, Predicate_1_t2585 *, const MethodInfo*))Array_FindIndex_TisObject_t_m31171_gshared)(__this /* static, unused */, ___array, ___startIndex, ___match, method)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T)
extern "C" int32_t Array_BinarySearch_TisObject_t_m31172_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, const MethodInfo* method);
#define Array_BinarySearch_TisObject_t_m31172(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, const MethodInfo*))Array_BinarySearch_TisObject_t_m31172_gshared)(__this /* static, unused */, ___array, ___value, method)
// System.Int32 System.Array::BinarySearch<System.Object>(!!0[],System.Int32,System.Int32,!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" int32_t Array_BinarySearch_TisObject_t_m31173_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* p0, int32_t p1, int32_t p2, Object_t * p3, Object_t* p4, const MethodInfo* method);
#define Array_BinarySearch_TisObject_t_m31173(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))Array_BinarySearch_TisObject_t_m31173_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t Array_BinarySearch_TisObject_t_m31174_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, Object_t* ___comparer, const MethodInfo* method);
#define Array_BinarySearch_TisObject_t_m31174(__this /* static, unused */, ___array, ___value, ___comparer, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, Object_t*, const MethodInfo*))Array_BinarySearch_TisObject_t_m31174_gshared)(__this /* static, unused */, ___array, ___value, ___comparer, method)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T)
extern "C" int32_t Array_BinarySearch_TisObject_t_m31175_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___index, int32_t ___length, Object_t * ___value, const MethodInfo* method);
#define Array_BinarySearch_TisObject_t_m31175(__this /* static, unused */, ___array, ___index, ___length, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t *, const MethodInfo*))Array_BinarySearch_TisObject_t_m31175_gshared)(__this /* static, unused */, ___array, ___index, ___length, ___value, method)
// System.Int32 System.Array::IndexOf<System.Object>(T[],T)
extern "C" int32_t Array_IndexOf_TisObject_t_m15433_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, const MethodInfo* method);
#define Array_IndexOf_TisObject_t_m15433(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, const MethodInfo*))Array_IndexOf_TisObject_t_m15433_gshared)(__this /* static, unused */, ___array, ___value, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
extern "C" int32_t Array_IndexOf_TisObject_t_m15426_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* p0, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisObject_t_m15426(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisObject_t_m15426_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Int32 System.Array::IndexOf<System.Object>(T[],T,System.Int32)
extern "C" int32_t Array_IndexOf_TisObject_t_m31176_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method);
#define Array_IndexOf_TisObject_t_m31176(__this /* static, unused */, ___array, ___value, ___startIndex, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, int32_t, const MethodInfo*))Array_IndexOf_TisObject_t_m31176_gshared)(__this /* static, unused */, ___array, ___value, ___startIndex, method)
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T)
extern "C" int32_t Array_LastIndexOf_TisObject_t_m31177_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, const MethodInfo* method);
#define Array_LastIndexOf_TisObject_t_m31177(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, const MethodInfo*))Array_LastIndexOf_TisObject_t_m31177_gshared)(__this /* static, unused */, ___array, ___value, method)
// System.Int32 System.Array::LastIndexOf<System.Object>(!!0[],!!0,System.Int32)
extern "C" int32_t Array_LastIndexOf_TisObject_t_m31178_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* p0, Object_t * p1, int32_t p2, const MethodInfo* method);
#define Array_LastIndexOf_TisObject_t_m31178(__this /* static, unused */, p0, p1, p2, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, int32_t, const MethodInfo*))Array_LastIndexOf_TisObject_t_m31178_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::LastIndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
extern "C" int32_t Array_LastIndexOf_TisObject_t_m31179_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* p0, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_LastIndexOf_TisObject_t_m31179(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, int32_t, int32_t, const MethodInfo*))Array_LastIndexOf_TisObject_t_m31179_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern "C" ObjectU5BU5D_t34* Array_FindAll_TisObject_t_m31180_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_FindAll_TisObject_t_m31180(__this /* static, unused */, ___array, ___match, method) (( ObjectU5BU5D_t34* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Predicate_1_t2585 *, const MethodInfo*))Array_FindAll_TisObject_t_m31180_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C" void Array_Resize_TisObject_t_m30232_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisObject_t_m30232(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34**, int32_t, const MethodInfo*))Array_Resize_TisObject_t_m30232_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Array::Exists<System.Object>(T[],System.Predicate`1<T>)
extern "C" bool Array_Exists_TisObject_t_m31181_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_Exists_TisObject_t_m31181(__this /* static, unused */, ___array, ___match, method) (( bool (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Predicate_1_t2585 *, const MethodInfo*))Array_Exists_TisObject_t_m31181_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Object>(T[])
extern "C" ReadOnlyCollection_1_t2578 * Array_AsReadOnly_TisObject_t_m15447_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, const MethodInfo* method);
#define Array_AsReadOnly_TisObject_t_m15447(__this /* static, unused */, ___array, method) (( ReadOnlyCollection_1_t2578 * (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, const MethodInfo*))Array_AsReadOnly_TisObject_t_m15447_gshared)(__this /* static, unused */, ___array, method)
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
extern "C" Object_t * Array_Find_TisObject_t_m31182_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_Find_TisObject_t_m31182(__this /* static, unused */, ___array, ___match, method) (( Object_t * (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Predicate_1_t2585 *, const MethodInfo*))Array_Find_TisObject_t_m31182_gshared)(__this /* static, unused */, ___array, ___match, method)
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
extern "C" Object_t * Array_FindLast_TisObject_t_m31183_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define Array_FindLast_TisObject_t_m31183(__this /* static, unused */, ___array, ___match, method) (( Object_t * (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Predicate_1_t2585 *, const MethodInfo*))Array_FindLast_TisObject_t_m31183_gshared)(__this /* static, unused */, ___array, ___match, method)
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C" TableRange_t1889  Array_InternalArray__get_Item_TisTableRange_t1889_m31184_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t1889_m31184(__this, ___index, method) (( TableRange_t1889  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t1889_m31184_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisTableRange_t1889_m31343_gshared (Array_t * __this, int32_t p0, TableRange_t1889 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTableRange_t1889_m31343(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, TableRange_t1889 *, const MethodInfo*))Array_GetGenericValueImpl_TisTableRange_t1889_m31343_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t1889_m31185_gshared (Array_t * __this, TableRange_t1889  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTableRange_t1889_m31185(__this, ___item, method) (( void (*) (Array_t *, TableRange_t1889 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisTableRange_t1889_m31185_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTableRange_t1889_m31186_gshared (Array_t * __this, TableRange_t1889  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTableRange_t1889_m31186(__this, ___item, method) (( bool (*) (Array_t *, TableRange_t1889 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTableRange_t1889_m31186_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t1889_m31187_gshared (Array_t * __this, TableRangeU5BU5D_t1891* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTableRange_t1889_m31187(__this, ___array, ___index, method) (( void (*) (Array_t *, TableRangeU5BU5D_t1891*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTableRange_t1889_m31187_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTableRange_t1889_m31188_gshared (Array_t * __this, TableRange_t1889  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTableRange_t1889_m31188(__this, ___item, method) (( bool (*) (Array_t *, TableRange_t1889 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTableRange_t1889_m31188_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTableRange_t1889_m31189_gshared (Array_t * __this, TableRange_t1889  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTableRange_t1889_m31189(__this, ___item, method) (( int32_t (*) (Array_t *, TableRange_t1889 , const MethodInfo*))Array_InternalArray__IndexOf_TisTableRange_t1889_m31189_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTableRange_t1889_m31190_gshared (Array_t * __this, int32_t ___index, TableRange_t1889  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTableRange_t1889_m31190(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TableRange_t1889 , const MethodInfo*))Array_InternalArray__Insert_TisTableRange_t1889_m31190_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTableRange_t1889_m31191_gshared (Array_t * __this, int32_t ___index, TableRange_t1889  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTableRange_t1889_m31191(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TableRange_t1889 , const MethodInfo*))Array_InternalArray__set_Item_TisTableRange_t1889_m31191_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisTableRange_t1889_m31344_gshared (Array_t * __this, int32_t p0, TableRange_t1889 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisTableRange_t1889_m31344(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, TableRange_t1889 *, const MethodInfo*))Array_SetGenericValueImpl_TisTableRange_t1889_m31344_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1889_m31192_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1889_m31192(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1889_m31192_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C" Slot_t1966  Array_InternalArray__get_Item_TisSlot_t1966_m31193_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t1966_m31193(__this, ___index, method) (( Slot_t1966  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t1966_m31193_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisSlot_t1966_m31345_gshared (Array_t * __this, int32_t p0, Slot_t1966 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSlot_t1966_m31345(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Slot_t1966 *, const MethodInfo*))Array_GetGenericValueImpl_TisSlot_t1966_m31345_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1966_m31194_gshared (Array_t * __this, Slot_t1966  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisSlot_t1966_m31194(__this, ___item, method) (( void (*) (Array_t *, Slot_t1966 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisSlot_t1966_m31194_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t1966_m31195_gshared (Array_t * __this, Slot_t1966  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisSlot_t1966_m31195(__this, ___item, method) (( bool (*) (Array_t *, Slot_t1966 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisSlot_t1966_m31195_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1966_m31196_gshared (Array_t * __this, SlotU5BU5D_t1973* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisSlot_t1966_m31196(__this, ___array, ___index, method) (( void (*) (Array_t *, SlotU5BU5D_t1973*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisSlot_t1966_m31196_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t1966_m31197_gshared (Array_t * __this, Slot_t1966  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisSlot_t1966_m31197(__this, ___item, method) (( bool (*) (Array_t *, Slot_t1966 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisSlot_t1966_m31197_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t1966_m31198_gshared (Array_t * __this, Slot_t1966  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisSlot_t1966_m31198(__this, ___item, method) (( int32_t (*) (Array_t *, Slot_t1966 , const MethodInfo*))Array_InternalArray__IndexOf_TisSlot_t1966_m31198_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisSlot_t1966_m31199_gshared (Array_t * __this, int32_t ___index, Slot_t1966  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisSlot_t1966_m31199(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t1966 , const MethodInfo*))Array_InternalArray__Insert_TisSlot_t1966_m31199_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisSlot_t1966_m31200_gshared (Array_t * __this, int32_t ___index, Slot_t1966  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisSlot_t1966_m31200(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t1966 , const MethodInfo*))Array_InternalArray__set_Item_TisSlot_t1966_m31200_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisSlot_t1966_m31346_gshared (Array_t * __this, int32_t p0, Slot_t1966 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisSlot_t1966_m31346(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Slot_t1966 *, const MethodInfo*))Array_SetGenericValueImpl_TisSlot_t1966_m31346_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1966_m31201_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1966_m31201(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1966_m31201_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C" Slot_t1974  Array_InternalArray__get_Item_TisSlot_t1974_m31202_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t1974_m31202(__this, ___index, method) (( Slot_t1974  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t1974_m31202_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisSlot_t1974_m31347_gshared (Array_t * __this, int32_t p0, Slot_t1974 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSlot_t1974_m31347(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Slot_t1974 *, const MethodInfo*))Array_GetGenericValueImpl_TisSlot_t1974_m31347_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1974_m31203_gshared (Array_t * __this, Slot_t1974  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisSlot_t1974_m31203(__this, ___item, method) (( void (*) (Array_t *, Slot_t1974 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisSlot_t1974_m31203_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t1974_m31204_gshared (Array_t * __this, Slot_t1974  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisSlot_t1974_m31204(__this, ___item, method) (( bool (*) (Array_t *, Slot_t1974 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisSlot_t1974_m31204_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1974_m31205_gshared (Array_t * __this, SlotU5BU5D_t1978* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisSlot_t1974_m31205(__this, ___array, ___index, method) (( void (*) (Array_t *, SlotU5BU5D_t1978*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisSlot_t1974_m31205_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t1974_m31206_gshared (Array_t * __this, Slot_t1974  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisSlot_t1974_m31206(__this, ___item, method) (( bool (*) (Array_t *, Slot_t1974 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisSlot_t1974_m31206_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t1974_m31207_gshared (Array_t * __this, Slot_t1974  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisSlot_t1974_m31207(__this, ___item, method) (( int32_t (*) (Array_t *, Slot_t1974 , const MethodInfo*))Array_InternalArray__IndexOf_TisSlot_t1974_m31207_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisSlot_t1974_m31208_gshared (Array_t * __this, int32_t ___index, Slot_t1974  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisSlot_t1974_m31208(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t1974 , const MethodInfo*))Array_InternalArray__Insert_TisSlot_t1974_m31208_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisSlot_t1974_m31209_gshared (Array_t * __this, int32_t ___index, Slot_t1974  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisSlot_t1974_m31209(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t1974 , const MethodInfo*))Array_InternalArray__set_Item_TisSlot_t1974_m31209_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisSlot_t1974_m31348_gshared (Array_t * __this, int32_t p0, Slot_t1974 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisSlot_t1974_m31348(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Slot_t1974 *, const MethodInfo*))Array_SetGenericValueImpl_TisSlot_t1974_m31348_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1974_m31210_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1974_m31210(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1974_m31210_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C" ILTokenInfo_t2053  Array_InternalArray__get_Item_TisILTokenInfo_t2053_m31211_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t2053_m31211(__this, ___index, method) (( ILTokenInfo_t2053  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t2053_m31211_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisILTokenInfo_t2053_m31349_gshared (Array_t * __this, int32_t p0, ILTokenInfo_t2053 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisILTokenInfo_t2053_m31349(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ILTokenInfo_t2053 *, const MethodInfo*))Array_GetGenericValueImpl_TisILTokenInfo_t2053_m31349_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILTokenInfo>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisILTokenInfo_t2053_m31212_gshared (Array_t * __this, ILTokenInfo_t2053  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisILTokenInfo_t2053_m31212(__this, ___item, method) (( void (*) (Array_t *, ILTokenInfo_t2053 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisILTokenInfo_t2053_m31212_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILTokenInfo>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisILTokenInfo_t2053_m31213_gshared (Array_t * __this, ILTokenInfo_t2053  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisILTokenInfo_t2053_m31213(__this, ___item, method) (( bool (*) (Array_t *, ILTokenInfo_t2053 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisILTokenInfo_t2053_m31213_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILTokenInfo>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t2053_m31214_gshared (Array_t * __this, ILTokenInfoU5BU5D_t2056* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t2053_m31214(__this, ___array, ___index, method) (( void (*) (Array_t *, ILTokenInfoU5BU5D_t2056*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t2053_m31214_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILTokenInfo>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisILTokenInfo_t2053_m31215_gshared (Array_t * __this, ILTokenInfo_t2053  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisILTokenInfo_t2053_m31215(__this, ___item, method) (( bool (*) (Array_t *, ILTokenInfo_t2053 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisILTokenInfo_t2053_m31215_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILTokenInfo>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisILTokenInfo_t2053_m31216_gshared (Array_t * __this, ILTokenInfo_t2053  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisILTokenInfo_t2053_m31216(__this, ___item, method) (( int32_t (*) (Array_t *, ILTokenInfo_t2053 , const MethodInfo*))Array_InternalArray__IndexOf_TisILTokenInfo_t2053_m31216_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisILTokenInfo_t2053_m31217_gshared (Array_t * __this, int32_t ___index, ILTokenInfo_t2053  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisILTokenInfo_t2053_m31217(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ILTokenInfo_t2053 , const MethodInfo*))Array_InternalArray__Insert_TisILTokenInfo_t2053_m31217_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisILTokenInfo_t2053_m31218_gshared (Array_t * __this, int32_t ___index, ILTokenInfo_t2053  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisILTokenInfo_t2053_m31218(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ILTokenInfo_t2053 , const MethodInfo*))Array_InternalArray__set_Item_TisILTokenInfo_t2053_m31218_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisILTokenInfo_t2053_m31350_gshared (Array_t * __this, int32_t p0, ILTokenInfo_t2053 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisILTokenInfo_t2053_m31350(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ILTokenInfo_t2053 *, const MethodInfo*))Array_SetGenericValueImpl_TisILTokenInfo_t2053_m31350_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILTokenInfo>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t2053_m31219_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t2053_m31219(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t2053_m31219_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C" LabelData_t2055  Array_InternalArray__get_Item_TisLabelData_t2055_m31220_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t2055_m31220(__this, ___index, method) (( LabelData_t2055  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t2055_m31220_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisLabelData_t2055_m31351_gshared (Array_t * __this, int32_t p0, LabelData_t2055 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelData_t2055_m31351(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, LabelData_t2055 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelData_t2055_m31351_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisLabelData_t2055_m31221_gshared (Array_t * __this, LabelData_t2055  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisLabelData_t2055_m31221(__this, ___item, method) (( void (*) (Array_t *, LabelData_t2055 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisLabelData_t2055_m31221_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelData_t2055_m31222_gshared (Array_t * __this, LabelData_t2055  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisLabelData_t2055_m31222(__this, ___item, method) (( bool (*) (Array_t *, LabelData_t2055 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisLabelData_t2055_m31222_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelData_t2055_m31223_gshared (Array_t * __this, LabelDataU5BU5D_t2057* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisLabelData_t2055_m31223(__this, ___array, ___index, method) (( void (*) (Array_t *, LabelDataU5BU5D_t2057*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisLabelData_t2055_m31223_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelData_t2055_m31224_gshared (Array_t * __this, LabelData_t2055  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisLabelData_t2055_m31224(__this, ___item, method) (( bool (*) (Array_t *, LabelData_t2055 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisLabelData_t2055_m31224_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelData_t2055_m31225_gshared (Array_t * __this, LabelData_t2055  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisLabelData_t2055_m31225(__this, ___item, method) (( int32_t (*) (Array_t *, LabelData_t2055 , const MethodInfo*))Array_InternalArray__IndexOf_TisLabelData_t2055_m31225_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisLabelData_t2055_m31226_gshared (Array_t * __this, int32_t ___index, LabelData_t2055  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisLabelData_t2055_m31226(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelData_t2055 , const MethodInfo*))Array_InternalArray__Insert_TisLabelData_t2055_m31226_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisLabelData_t2055_m31227_gshared (Array_t * __this, int32_t ___index, LabelData_t2055  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisLabelData_t2055_m31227(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelData_t2055 , const MethodInfo*))Array_InternalArray__set_Item_TisLabelData_t2055_m31227_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisLabelData_t2055_m31352_gshared (Array_t * __this, int32_t p0, LabelData_t2055 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisLabelData_t2055_m31352(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, LabelData_t2055 *, const MethodInfo*))Array_SetGenericValueImpl_TisLabelData_t2055_m31352_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t2055_m31228_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t2055_m31228(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t2055_m31228_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C" LabelFixup_t2054  Array_InternalArray__get_Item_TisLabelFixup_t2054_m31229_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t2054_m31229(__this, ___index, method) (( LabelFixup_t2054  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t2054_m31229_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisLabelFixup_t2054_m31353_gshared (Array_t * __this, int32_t p0, LabelFixup_t2054 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelFixup_t2054_m31353(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, LabelFixup_t2054 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelFixup_t2054_m31353_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisLabelFixup_t2054_m31230_gshared (Array_t * __this, LabelFixup_t2054  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisLabelFixup_t2054_m31230(__this, ___item, method) (( void (*) (Array_t *, LabelFixup_t2054 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisLabelFixup_t2054_m31230_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelFixup_t2054_m31231_gshared (Array_t * __this, LabelFixup_t2054  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisLabelFixup_t2054_m31231(__this, ___item, method) (( bool (*) (Array_t *, LabelFixup_t2054 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisLabelFixup_t2054_m31231_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t2054_m31232_gshared (Array_t * __this, LabelFixupU5BU5D_t2058* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t2054_m31232(__this, ___array, ___index, method) (( void (*) (Array_t *, LabelFixupU5BU5D_t2058*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t2054_m31232_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelFixup_t2054_m31233_gshared (Array_t * __this, LabelFixup_t2054  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisLabelFixup_t2054_m31233(__this, ___item, method) (( bool (*) (Array_t *, LabelFixup_t2054 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisLabelFixup_t2054_m31233_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelFixup_t2054_m31234_gshared (Array_t * __this, LabelFixup_t2054  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisLabelFixup_t2054_m31234(__this, ___item, method) (( int32_t (*) (Array_t *, LabelFixup_t2054 , const MethodInfo*))Array_InternalArray__IndexOf_TisLabelFixup_t2054_m31234_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisLabelFixup_t2054_m31235_gshared (Array_t * __this, int32_t ___index, LabelFixup_t2054  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisLabelFixup_t2054_m31235(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelFixup_t2054 , const MethodInfo*))Array_InternalArray__Insert_TisLabelFixup_t2054_m31235_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisLabelFixup_t2054_m31236_gshared (Array_t * __this, int32_t ___index, LabelFixup_t2054  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisLabelFixup_t2054_m31236(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelFixup_t2054 , const MethodInfo*))Array_InternalArray__set_Item_TisLabelFixup_t2054_m31236_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisLabelFixup_t2054_m31354_gshared (Array_t * __this, int32_t p0, LabelFixup_t2054 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisLabelFixup_t2054_m31354(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, LabelFixup_t2054 *, const MethodInfo*))Array_SetGenericValueImpl_TisLabelFixup_t2054_m31354_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t2054_m31237_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t2054_m31237(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t2054_m31237_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C" CustomAttributeTypedArgument_t2101  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2101_m31238_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2101_m31238(__this, ___index, method) (( CustomAttributeTypedArgument_t2101  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2101_m31238_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t2101_m31355_gshared (Array_t * __this, int32_t p0, CustomAttributeTypedArgument_t2101 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t2101_m31355(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, CustomAttributeTypedArgument_t2101 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t2101_m31355_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeTypedArgument>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t2101_m31239_gshared (Array_t * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t2101_m31239(__this, ___item, method) (( void (*) (Array_t *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t2101_m31239_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeTypedArgument>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t2101_m31240_gshared (Array_t * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t2101_m31240(__this, ___item, method) (( bool (*) (Array_t *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t2101_m31240_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t2101_m31241_gshared (Array_t * __this, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t2101_m31241(__this, ___array, ___index, method) (( void (*) (Array_t *, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t2101_m31241_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeTypedArgument>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t2101_m31242_gshared (Array_t * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t2101_m31242(__this, ___item, method) (( bool (*) (Array_t *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t2101_m31242_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeTypedArgument>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t2101_m31243_gshared (Array_t * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t2101_m31243(__this, ___item, method) (( int32_t (*) (Array_t *, CustomAttributeTypedArgument_t2101 , const MethodInfo*))Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t2101_m31243_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t2101_m31244_gshared (Array_t * __this, int32_t ___index, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t2101_m31244(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, CustomAttributeTypedArgument_t2101 , const MethodInfo*))Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t2101_m31244_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t2101_m31245_gshared (Array_t * __this, int32_t ___index, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t2101_m31245(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, CustomAttributeTypedArgument_t2101 , const MethodInfo*))Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t2101_m31245_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisCustomAttributeTypedArgument_t2101_m31356_gshared (Array_t * __this, int32_t p0, CustomAttributeTypedArgument_t2101 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisCustomAttributeTypedArgument_t2101_m31356(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, CustomAttributeTypedArgument_t2101 *, const MethodInfo*))Array_SetGenericValueImpl_TisCustomAttributeTypedArgument_t2101_m31356_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeTypedArgument>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t2101_m31246_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t2101_m31246(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t2101_m31246_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C" CustomAttributeNamedArgument_t2100  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t2100_m31247_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t2100_m31247(__this, ___index, method) (( CustomAttributeNamedArgument_t2100  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t2100_m31247_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t2100_m31357_gshared (Array_t * __this, int32_t p0, CustomAttributeNamedArgument_t2100 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t2100_m31357(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, CustomAttributeNamedArgument_t2100 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t2100_m31357_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeNamedArgument>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t2100_m31248_gshared (Array_t * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t2100_m31248(__this, ___item, method) (( void (*) (Array_t *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t2100_m31248_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeNamedArgument>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t2100_m31249_gshared (Array_t * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t2100_m31249(__this, ___item, method) (( bool (*) (Array_t *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t2100_m31249_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t2100_m31250_gshared (Array_t * __this, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t2100_m31250(__this, ___array, ___index, method) (( void (*) (Array_t *, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t2100_m31250_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeNamedArgument>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t2100_m31251_gshared (Array_t * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t2100_m31251(__this, ___item, method) (( bool (*) (Array_t *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t2100_m31251_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeNamedArgument>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t2100_m31252_gshared (Array_t * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t2100_m31252(__this, ___item, method) (( int32_t (*) (Array_t *, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t2100_m31252_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t2100_m31253_gshared (Array_t * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t2100_m31253(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t2100_m31253_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t2100_m31254_gshared (Array_t * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t2100_m31254(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t2100_m31254_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisCustomAttributeNamedArgument_t2100_m31358_gshared (Array_t * __this, int32_t p0, CustomAttributeNamedArgument_t2100 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisCustomAttributeNamedArgument_t2100_m31358(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, CustomAttributeNamedArgument_t2100 *, const MethodInfo*))Array_SetGenericValueImpl_TisCustomAttributeNamedArgument_t2100_m31358_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeNamedArgument>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t2100_m31255_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t2100_m31255(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t2100_m31255_gshared)(__this, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C" CustomAttributeTypedArgumentU5BU5D_t2537* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t2101_m15429_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t2101_m15429(__this /* static, unused */, ___values, method) (( CustomAttributeTypedArgumentU5BU5D_t2537* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t2101_m15429_gshared)(__this /* static, unused */, ___values, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeTypedArgument>(T[])
extern "C" ReadOnlyCollection_1_t2539 * Array_AsReadOnly_TisCustomAttributeTypedArgument_t2101_m15430_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, const MethodInfo* method);
#define Array_AsReadOnly_TisCustomAttributeTypedArgument_t2101_m15430(__this /* static, unused */, ___array, method) (( ReadOnlyCollection_1_t2539 * (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, const MethodInfo*))Array_AsReadOnly_TisCustomAttributeTypedArgument_t2101_m15430_gshared)(__this /* static, unused */, ___array, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t2101_m31256_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537** ___array, int32_t ___newSize, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeTypedArgument_t2101_m31256(__this /* static, unused */, ___array, ___newSize, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeTypedArgument_t2101_m31256_gshared)(__this /* static, unused */, ___array, ___newSize, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(!!0[]&,System.Int32,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t2101_m31257_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537** p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeTypedArgument_t2101_m31257(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537**, int32_t, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeTypedArgument_t2101_m31257_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T,System.Int32,System.Int32)
extern "C" int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31258_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, CustomAttributeTypedArgument_t2101  ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31258(__this /* static, unused */, ___array, ___value, ___startIndex, ___count, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgument_t2101 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31258_gshared)(__this /* static, unused */, ___array, ___value, ___startIndex, ___count, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_m31259_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t2101_m31259(__this /* static, unused */, ___array, ___index, ___length, ___comparer, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t2101_m31259_gshared)(__this /* static, unused */, ___array, ___index, ___length, ___comparer, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31260_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* p0, CustomAttributeTypedArgumentU5BU5D_t2537* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31260(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31260_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeTypedArgument>(!!0[])
extern "C" Swapper_t1855 * Array_get_swapper_TisCustomAttributeTypedArgument_t2101_m31261_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* p0, const MethodInfo* method);
#define Array_get_swapper_TisCustomAttributeTypedArgument_t2101_m31261(__this /* static, unused */, p0, method) (( Swapper_t1855 * (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, const MethodInfo*))Array_get_swapper_TisCustomAttributeTypedArgument_t2101_m31261_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31262_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* p0, CustomAttributeTypedArgumentU5BU5D_t2537* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_qsort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31262(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_qsort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31262_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeTypedArgument>(!!0,!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" int32_t Array_compare_TisCustomAttributeTypedArgument_t2101_m31263_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgument_t2101  p0, CustomAttributeTypedArgument_t2101  p1, Object_t* p2, const MethodInfo* method);
#define Array_compare_TisCustomAttributeTypedArgument_t2101_m31263(__this /* static, unused */, p0, p1, p2, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101 , Object_t*, const MethodInfo*))Array_compare_TisCustomAttributeTypedArgument_t2101_m31263_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(!!0[],!!1[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31264_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* p0, CustomAttributeTypedArgumentU5BU5D_t2537* p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_swap_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31264(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, const MethodInfo*))Array_swap_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31264_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Comparison`1<T>)
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_m31265_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___length, Comparison_1_t3553 * ___comparison, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t2101_m31265(__this /* static, unused */, ___array, ___length, ___comparison, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, Comparison_1_t3553 *, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t2101_m31265_gshared)(__this /* static, unused */, ___array, ___length, ___comparison, method)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument>(!!0[],System.Int32,System.Int32,System.Comparison`1<!!0>)
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t2101_m31266_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* p0, int32_t p1, int32_t p2, Comparison_1_t3553 * p3, const MethodInfo* method);
#define Array_qsort_TisCustomAttributeTypedArgument_t2101_m31266(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Comparison_1_t3553 *, const MethodInfo*))Array_qsort_TisCustomAttributeTypedArgument_t2101_m31266_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument>(!!0[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t2101_m31267_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Array_swap_TisCustomAttributeTypedArgument_t2101_m31267(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, const MethodInfo*))Array_swap_TisCustomAttributeTypedArgument_t2101_m31267_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T)
extern "C" int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31268_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, CustomAttributeTypedArgument_t2101  ___value, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31268(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgument_t2101 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31268_gshared)(__this /* static, unused */, ___array, ___value, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C" CustomAttributeNamedArgumentU5BU5D_t2538* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t2100_m15431_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t2100_m15431(__this /* static, unused */, ___values, method) (( CustomAttributeNamedArgumentU5BU5D_t2538* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t2100_m15431_gshared)(__this /* static, unused */, ___values, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeNamedArgument>(T[])
extern "C" ReadOnlyCollection_1_t2540 * Array_AsReadOnly_TisCustomAttributeNamedArgument_t2100_m15432_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, const MethodInfo* method);
#define Array_AsReadOnly_TisCustomAttributeNamedArgument_t2100_m15432(__this /* static, unused */, ___array, method) (( ReadOnlyCollection_1_t2540 * (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, const MethodInfo*))Array_AsReadOnly_TisCustomAttributeNamedArgument_t2100_m15432_gshared)(__this /* static, unused */, ___array, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t2100_m31269_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538** ___array, int32_t ___newSize, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeNamedArgument_t2100_m31269(__this /* static, unused */, ___array, ___newSize, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeNamedArgument_t2100_m31269_gshared)(__this /* static, unused */, ___array, ___newSize, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(!!0[]&,System.Int32,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t2100_m31270_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538** p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeNamedArgument_t2100_m31270(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538**, int32_t, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeNamedArgument_t2100_m31270_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T,System.Int32,System.Int32)
extern "C" int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31271_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, CustomAttributeNamedArgument_t2100  ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31271(__this /* static, unused */, ___array, ___value, ___startIndex, ___count, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgument_t2100 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31271_gshared)(__this /* static, unused */, ___array, ___value, ___startIndex, ___count, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_m31272_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t2100_m31272(__this /* static, unused */, ___array, ___index, ___length, ___comparer, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t2100_m31272_gshared)(__this /* static, unused */, ___array, ___index, ___length, ___comparer, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31273_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* p0, CustomAttributeNamedArgumentU5BU5D_t2538* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31273(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31273_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeNamedArgument>(!!0[])
extern "C" Swapper_t1855 * Array_get_swapper_TisCustomAttributeNamedArgument_t2100_m31274_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* p0, const MethodInfo* method);
#define Array_get_swapper_TisCustomAttributeNamedArgument_t2100_m31274(__this /* static, unused */, p0, method) (( Swapper_t1855 * (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, const MethodInfo*))Array_get_swapper_TisCustomAttributeNamedArgument_t2100_m31274_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31275_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* p0, CustomAttributeNamedArgumentU5BU5D_t2538* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_qsort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31275(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_qsort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31275_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeNamedArgument>(!!0,!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" int32_t Array_compare_TisCustomAttributeNamedArgument_t2100_m31276_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgument_t2100  p0, CustomAttributeNamedArgument_t2100  p1, Object_t* p2, const MethodInfo* method);
#define Array_compare_TisCustomAttributeNamedArgument_t2100_m31276(__this /* static, unused */, p0, p1, p2, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgument_t2100 , CustomAttributeNamedArgument_t2100 , Object_t*, const MethodInfo*))Array_compare_TisCustomAttributeNamedArgument_t2100_m31276_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(!!0[],!!1[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31277_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* p0, CustomAttributeNamedArgumentU5BU5D_t2538* p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_swap_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31277(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, const MethodInfo*))Array_swap_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31277_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Comparison`1<T>)
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_m31278_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___length, Comparison_1_t3564 * ___comparison, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t2100_m31278(__this /* static, unused */, ___array, ___length, ___comparison, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, Comparison_1_t3564 *, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t2100_m31278_gshared)(__this /* static, unused */, ___array, ___length, ___comparison, method)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument>(!!0[],System.Int32,System.Int32,System.Comparison`1<!!0>)
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t2100_m31279_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* p0, int32_t p1, int32_t p2, Comparison_1_t3564 * p3, const MethodInfo* method);
#define Array_qsort_TisCustomAttributeNamedArgument_t2100_m31279(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Comparison_1_t3564 *, const MethodInfo*))Array_qsort_TisCustomAttributeNamedArgument_t2100_m31279_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument>(!!0[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t2100_m31280_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Array_swap_TisCustomAttributeNamedArgument_t2100_m31280(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, const MethodInfo*))Array_swap_TisCustomAttributeNamedArgument_t2100_m31280_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T)
extern "C" int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31281_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, CustomAttributeNamedArgument_t2100  ___value, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31281(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgument_t2100 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31281_gshared)(__this /* static, unused */, ___array, ___value, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C" ObjectU5BU5D_t34* CustomAttributeData_UnboxValues_TisObject_t_m31282_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisObject_t_m31282(__this /* static, unused */, ___values, method) (( ObjectU5BU5D_t34* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, const MethodInfo*))CustomAttributeData_UnboxValues_TisObject_t_m31282_gshared)(__this /* static, unused */, ___values, method)
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern "C" Object_t * MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m31283_gshared (Object_t * __this /* static, unused */, Getter_2_t3567 * ___getter, Object_t * ___obj, const MethodInfo* method);
#define MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m31283(__this /* static, unused */, ___getter, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, Getter_2_t3567 *, Object_t *, const MethodInfo*))MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m31283_gshared)(__this /* static, unused */, ___getter, ___obj, method)
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern "C" Object_t * MonoProperty_StaticGetterAdapterFrame_TisObject_t_m31284_gshared (Object_t * __this /* static, unused */, StaticGetter_1_t3568 * ___getter, Object_t * ___obj, const MethodInfo* method);
#define MonoProperty_StaticGetterAdapterFrame_TisObject_t_m31284(__this /* static, unused */, ___getter, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, StaticGetter_1_t3568 *, Object_t *, const MethodInfo*))MonoProperty_StaticGetterAdapterFrame_TisObject_t_m31284_gshared)(__this /* static, unused */, ___getter, ___obj, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C" ResourceInfo_t2130  Array_InternalArray__get_Item_TisResourceInfo_t2130_m31285_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t2130_m31285(__this, ___index, method) (( ResourceInfo_t2130  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t2130_m31285_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisResourceInfo_t2130_m31359_gshared (Array_t * __this, int32_t p0, ResourceInfo_t2130 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceInfo_t2130_m31359(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ResourceInfo_t2130 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceInfo_t2130_m31359_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceInfo>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisResourceInfo_t2130_m31286_gshared (Array_t * __this, ResourceInfo_t2130  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisResourceInfo_t2130_m31286(__this, ___item, method) (( void (*) (Array_t *, ResourceInfo_t2130 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisResourceInfo_t2130_m31286_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceInfo>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisResourceInfo_t2130_m31287_gshared (Array_t * __this, ResourceInfo_t2130  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisResourceInfo_t2130_m31287(__this, ___item, method) (( bool (*) (Array_t *, ResourceInfo_t2130 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisResourceInfo_t2130_m31287_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceInfo>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t2130_m31288_gshared (Array_t * __this, ResourceInfoU5BU5D_t2134* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t2130_m31288(__this, ___array, ___index, method) (( void (*) (Array_t *, ResourceInfoU5BU5D_t2134*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t2130_m31288_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceInfo>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisResourceInfo_t2130_m31289_gshared (Array_t * __this, ResourceInfo_t2130  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisResourceInfo_t2130_m31289(__this, ___item, method) (( bool (*) (Array_t *, ResourceInfo_t2130 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisResourceInfo_t2130_m31289_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceInfo>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisResourceInfo_t2130_m31290_gshared (Array_t * __this, ResourceInfo_t2130  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisResourceInfo_t2130_m31290(__this, ___item, method) (( int32_t (*) (Array_t *, ResourceInfo_t2130 , const MethodInfo*))Array_InternalArray__IndexOf_TisResourceInfo_t2130_m31290_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisResourceInfo_t2130_m31291_gshared (Array_t * __this, int32_t ___index, ResourceInfo_t2130  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisResourceInfo_t2130_m31291(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ResourceInfo_t2130 , const MethodInfo*))Array_InternalArray__Insert_TisResourceInfo_t2130_m31291_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisResourceInfo_t2130_m31292_gshared (Array_t * __this, int32_t ___index, ResourceInfo_t2130  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisResourceInfo_t2130_m31292(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ResourceInfo_t2130 , const MethodInfo*))Array_InternalArray__set_Item_TisResourceInfo_t2130_m31292_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisResourceInfo_t2130_m31360_gshared (Array_t * __this, int32_t p0, ResourceInfo_t2130 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisResourceInfo_t2130_m31360(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ResourceInfo_t2130 *, const MethodInfo*))Array_SetGenericValueImpl_TisResourceInfo_t2130_m31360_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceInfo>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t2130_m31293_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t2130_m31293(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t2130_m31293_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C" ResourceCacheItem_t2131  Array_InternalArray__get_Item_TisResourceCacheItem_t2131_m31294_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t2131_m31294(__this, ___index, method) (( ResourceCacheItem_t2131  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t2131_m31294_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisResourceCacheItem_t2131_m31361_gshared (Array_t * __this, int32_t p0, ResourceCacheItem_t2131 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceCacheItem_t2131_m31361(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ResourceCacheItem_t2131 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceCacheItem_t2131_m31361_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisResourceCacheItem_t2131_m31295_gshared (Array_t * __this, ResourceCacheItem_t2131  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisResourceCacheItem_t2131_m31295(__this, ___item, method) (( void (*) (Array_t *, ResourceCacheItem_t2131 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisResourceCacheItem_t2131_m31295_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t2131_m31296_gshared (Array_t * __this, ResourceCacheItem_t2131  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t2131_m31296(__this, ___item, method) (( bool (*) (Array_t *, ResourceCacheItem_t2131 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t2131_m31296_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceCacheItem>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t2131_m31297_gshared (Array_t * __this, ResourceCacheItemU5BU5D_t2135* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t2131_m31297(__this, ___array, ___index, method) (( void (*) (Array_t *, ResourceCacheItemU5BU5D_t2135*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t2131_m31297_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t2131_m31298_gshared (Array_t * __this, ResourceCacheItem_t2131  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t2131_m31298(__this, ___item, method) (( bool (*) (Array_t *, ResourceCacheItem_t2131 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t2131_m31298_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisResourceCacheItem_t2131_m31299_gshared (Array_t * __this, ResourceCacheItem_t2131  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisResourceCacheItem_t2131_m31299(__this, ___item, method) (( int32_t (*) (Array_t *, ResourceCacheItem_t2131 , const MethodInfo*))Array_InternalArray__IndexOf_TisResourceCacheItem_t2131_m31299_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisResourceCacheItem_t2131_m31300_gshared (Array_t * __this, int32_t ___index, ResourceCacheItem_t2131  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisResourceCacheItem_t2131_m31300(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ResourceCacheItem_t2131 , const MethodInfo*))Array_InternalArray__Insert_TisResourceCacheItem_t2131_m31300_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisResourceCacheItem_t2131_m31301_gshared (Array_t * __this, int32_t ___index, ResourceCacheItem_t2131  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisResourceCacheItem_t2131_m31301(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ResourceCacheItem_t2131 , const MethodInfo*))Array_InternalArray__set_Item_TisResourceCacheItem_t2131_m31301_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisResourceCacheItem_t2131_m31362_gshared (Array_t * __this, int32_t p0, ResourceCacheItem_t2131 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisResourceCacheItem_t2131_m31362(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ResourceCacheItem_t2131 *, const MethodInfo*))Array_SetGenericValueImpl_TisResourceCacheItem_t2131_m31362_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceCacheItem>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t2131_m31302_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t2131_m31302(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t2131_m31302_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C" DateTime_t220  Array_InternalArray__get_Item_TisDateTime_t220_m31303_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t220_m31303(__this, ___index, method) (( DateTime_t220  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t220_m31303_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisDateTime_t220_m31363_gshared (Array_t * __this, int32_t p0, DateTime_t220 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDateTime_t220_m31363(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, DateTime_t220 *, const MethodInfo*))Array_GetGenericValueImpl_TisDateTime_t220_m31363_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t220_m31304_gshared (Array_t * __this, DateTime_t220  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisDateTime_t220_m31304(__this, ___item, method) (( void (*) (Array_t *, DateTime_t220 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisDateTime_t220_m31304_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisDateTime_t220_m31305_gshared (Array_t * __this, DateTime_t220  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisDateTime_t220_m31305(__this, ___item, method) (( bool (*) (Array_t *, DateTime_t220 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisDateTime_t220_m31305_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t220_m31306_gshared (Array_t * __this, DateTimeU5BU5D_t2560* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisDateTime_t220_m31306(__this, ___array, ___index, method) (( void (*) (Array_t *, DateTimeU5BU5D_t2560*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisDateTime_t220_m31306_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisDateTime_t220_m31307_gshared (Array_t * __this, DateTime_t220  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisDateTime_t220_m31307(__this, ___item, method) (( bool (*) (Array_t *, DateTime_t220 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisDateTime_t220_m31307_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisDateTime_t220_m31308_gshared (Array_t * __this, DateTime_t220  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisDateTime_t220_m31308(__this, ___item, method) (( int32_t (*) (Array_t *, DateTime_t220 , const MethodInfo*))Array_InternalArray__IndexOf_TisDateTime_t220_m31308_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisDateTime_t220_m31309_gshared (Array_t * __this, int32_t ___index, DateTime_t220  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisDateTime_t220_m31309(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, DateTime_t220 , const MethodInfo*))Array_InternalArray__Insert_TisDateTime_t220_m31309_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisDateTime_t220_m31310_gshared (Array_t * __this, int32_t ___index, DateTime_t220  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisDateTime_t220_m31310(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, DateTime_t220 , const MethodInfo*))Array_InternalArray__set_Item_TisDateTime_t220_m31310_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisDateTime_t220_m31364_gshared (Array_t * __this, int32_t p0, DateTime_t220 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisDateTime_t220_m31364(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, DateTime_t220 *, const MethodInfo*))Array_SetGenericValueImpl_TisDateTime_t220_m31364_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t220_m31311_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t220_m31311(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t220_m31311_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C" Decimal_t564  Array_InternalArray__get_Item_TisDecimal_t564_m31312_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t564_m31312(__this, ___index, method) (( Decimal_t564  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t564_m31312_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisDecimal_t564_m31365_gshared (Array_t * __this, int32_t p0, Decimal_t564 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDecimal_t564_m31365(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Decimal_t564 *, const MethodInfo*))Array_GetGenericValueImpl_TisDecimal_t564_m31365_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t564_m31313_gshared (Array_t * __this, Decimal_t564  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisDecimal_t564_m31313(__this, ___item, method) (( void (*) (Array_t *, Decimal_t564 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisDecimal_t564_m31313_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisDecimal_t564_m31314_gshared (Array_t * __this, Decimal_t564  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisDecimal_t564_m31314(__this, ___item, method) (( bool (*) (Array_t *, Decimal_t564 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisDecimal_t564_m31314_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t564_m31315_gshared (Array_t * __this, DecimalU5BU5D_t2561* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisDecimal_t564_m31315(__this, ___array, ___index, method) (( void (*) (Array_t *, DecimalU5BU5D_t2561*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisDecimal_t564_m31315_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisDecimal_t564_m31316_gshared (Array_t * __this, Decimal_t564  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisDecimal_t564_m31316(__this, ___item, method) (( bool (*) (Array_t *, Decimal_t564 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisDecimal_t564_m31316_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisDecimal_t564_m31317_gshared (Array_t * __this, Decimal_t564  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisDecimal_t564_m31317(__this, ___item, method) (( int32_t (*) (Array_t *, Decimal_t564 , const MethodInfo*))Array_InternalArray__IndexOf_TisDecimal_t564_m31317_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisDecimal_t564_m31318_gshared (Array_t * __this, int32_t ___index, Decimal_t564  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisDecimal_t564_m31318(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Decimal_t564 , const MethodInfo*))Array_InternalArray__Insert_TisDecimal_t564_m31318_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisDecimal_t564_m31319_gshared (Array_t * __this, int32_t ___index, Decimal_t564  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisDecimal_t564_m31319(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Decimal_t564 , const MethodInfo*))Array_InternalArray__set_Item_TisDecimal_t564_m31319_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisDecimal_t564_m31366_gshared (Array_t * __this, int32_t p0, Decimal_t564 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisDecimal_t564_m31366(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Decimal_t564 *, const MethodInfo*))Array_SetGenericValueImpl_TisDecimal_t564_m31366_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t564_m31320_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t564_m31320(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t564_m31320_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C" TimeSpan_t565  Array_InternalArray__get_Item_TisTimeSpan_t565_m31321_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t565_m31321(__this, ___index, method) (( TimeSpan_t565  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t565_m31321_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisTimeSpan_t565_m31367_gshared (Array_t * __this, int32_t p0, TimeSpan_t565 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTimeSpan_t565_m31367(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, TimeSpan_t565 *, const MethodInfo*))Array_GetGenericValueImpl_TisTimeSpan_t565_m31367_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t565_m31322_gshared (Array_t * __this, TimeSpan_t565  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTimeSpan_t565_m31322(__this, ___item, method) (( void (*) (Array_t *, TimeSpan_t565 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisTimeSpan_t565_m31322_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTimeSpan_t565_m31323_gshared (Array_t * __this, TimeSpan_t565  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTimeSpan_t565_m31323(__this, ___item, method) (( bool (*) (Array_t *, TimeSpan_t565 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTimeSpan_t565_m31323_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t565_m31324_gshared (Array_t * __this, TimeSpanU5BU5D_t2562* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t565_m31324(__this, ___array, ___index, method) (( void (*) (Array_t *, TimeSpanU5BU5D_t2562*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t565_m31324_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTimeSpan_t565_m31325_gshared (Array_t * __this, TimeSpan_t565  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTimeSpan_t565_m31325(__this, ___item, method) (( bool (*) (Array_t *, TimeSpan_t565 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTimeSpan_t565_m31325_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTimeSpan_t565_m31326_gshared (Array_t * __this, TimeSpan_t565  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTimeSpan_t565_m31326(__this, ___item, method) (( int32_t (*) (Array_t *, TimeSpan_t565 , const MethodInfo*))Array_InternalArray__IndexOf_TisTimeSpan_t565_m31326_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t565_m31327_gshared (Array_t * __this, int32_t ___index, TimeSpan_t565  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTimeSpan_t565_m31327(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TimeSpan_t565 , const MethodInfo*))Array_InternalArray__Insert_TisTimeSpan_t565_m31327_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t565_m31328_gshared (Array_t * __this, int32_t ___index, TimeSpan_t565  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTimeSpan_t565_m31328(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TimeSpan_t565 , const MethodInfo*))Array_InternalArray__set_Item_TisTimeSpan_t565_m31328_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisTimeSpan_t565_m31368_gshared (Array_t * __this, int32_t p0, TimeSpan_t565 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisTimeSpan_t565_m31368(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, TimeSpan_t565 *, const MethodInfo*))Array_SetGenericValueImpl_TisTimeSpan_t565_m31368_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t565_m31329_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t565_m31329(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t565_m31329_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C" uint8_t Array_InternalArray__get_Item_TisTypeTag_t2267_m31330_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t2267_m31330(__this, ___index, method) (( uint8_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t2267_m31330_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisTypeTag_t2267_m31369_gshared (Array_t * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTypeTag_t2267_m31369(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, uint8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTypeTag_t2267_m31369_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTypeTag_t2267_m31331_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTypeTag_t2267_m31331(__this, ___item, method) (( void (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Add_TisTypeTag_t2267_m31331_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTypeTag_t2267_m31332_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTypeTag_t2267_m31332(__this, ___item, method) (( bool (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTypeTag_t2267_m31332_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTypeTag_t2267_m31333_gshared (Array_t * __this, TypeTagU5BU5D_t2563* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTypeTag_t2267_m31333(__this, ___array, ___index, method) (( void (*) (Array_t *, TypeTagU5BU5D_t2563*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTypeTag_t2267_m31333_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTypeTag_t2267_m31334_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTypeTag_t2267_m31334(__this, ___item, method) (( bool (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTypeTag_t2267_m31334_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTypeTag_t2267_m31335_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTypeTag_t2267_m31335(__this, ___item, method) (( int32_t (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__IndexOf_TisTypeTag_t2267_m31335_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTypeTag_t2267_m31336_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTypeTag_t2267_m31336(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, uint8_t, const MethodInfo*))Array_InternalArray__Insert_TisTypeTag_t2267_m31336_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTypeTag_t2267_m31337_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTypeTag_t2267_m31337(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, uint8_t, const MethodInfo*))Array_InternalArray__set_Item_TisTypeTag_t2267_m31337_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisTypeTag_t2267_m31370_gshared (Array_t * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisTypeTag_t2267_m31370(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, uint8_t*, const MethodInfo*))Array_SetGenericValueImpl_TisTypeTag_t2267_m31370_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t2267_m31338_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t2267_m31338(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t2267_m31338_gshared)(__this, method)
// System.Void System.Array::SetGenericValueImpl<System.Int16>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int16>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int16>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t561_m31138_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3510  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3510 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3510  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" int8_t Array_InternalArray__get_Item_TisSByte_t559_m31139_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (int8_t*)(&V_0));
		int8_t L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.SByte>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.SByte>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.SByte>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisSByte_t559_m31140_gshared (Array_t * __this, int8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.SByte>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.SByte>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisSByte_t559_m31141_gshared (Array_t * __this, int8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int8_t V_2 = 0x0;
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (int8_t*)(&V_2));
		int8_t L_5 = ___item;
		goto IL_004d;
	}
	{
		int8_t L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		int8_t L_7 = V_2;
		int8_t L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((int8_t*)(&___item));
		bool L_10 = SByte_Equals_m10447((int8_t*)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.SByte>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.SByte>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSByte_t559_m31142_gshared (Array_t * __this, SByteU5BU5D_t2386* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		SByteU5BU5D_t2386* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SByteU5BU5D_t2386* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		SByteU5BU5D_t2386* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		SByteU5BU5D_t2386* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SByteU5BU5D_t2386* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.SByte>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.SByte>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisSByte_t559_m31143_gshared (Array_t * __this, int8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.SByte>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.SByte>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisSByte_t559_m31144_gshared (Array_t * __this, int8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int8_t V_2 = 0x0;
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (int8_t*)(&V_2));
		int8_t L_5 = ___item;
		goto IL_005d;
	}
	{
		int8_t L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		int8_t L_10 = ___item;
		int8_t L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((int8_t*)(&V_2));
		bool L_13 = SByte_Equals_m10447((int8_t*)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.SByte>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.SByte>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisSByte_t559_m31145_gshared (Array_t * __this, int32_t ___index, int8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.SByte>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.SByte>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisSByte_t559_m31146_gshared (Array_t * __this, int32_t ___index, int8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		int8_t L_6 = ___item;
		int8_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (int8_t*)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.SByte>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.SByte>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.SByte>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t559_m31147_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3511  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3511 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3511  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" int64_t Array_InternalArray__get_Item_TisInt64_t507_m31148_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (int64_t*)(&V_0));
		int64_t L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Int64>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Int64>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Int64>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t507_m31149_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Int64>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Int64>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisInt64_t507_m31150_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (int64_t*)(&V_2));
		int64_t L_5 = ___item;
		goto IL_004d;
	}
	{
		int64_t L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		int64_t L_7 = V_2;
		int64_t L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((int64_t*)(&___item));
		bool L_10 = Int64_Equals_m10337((int64_t*)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Int64>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Int64>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t507_m31151_gshared (Array_t * __this, Int64U5BU5D_t2511* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int64U5BU5D_t2511* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Int64U5BU5D_t2511* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		Int64U5BU5D_t2511* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		Int64U5BU5D_t2511* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Int64U5BU5D_t2511* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Int64>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Int64>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisInt64_t507_m31152_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Int64>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Int64>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisInt64_t507_m31153_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (int64_t*)(&V_2));
		int64_t L_5 = ___item;
		goto IL_005d;
	}
	{
		int64_t L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		int64_t L_10 = ___item;
		int64_t L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((int64_t*)(&V_2));
		bool L_13 = Int64_Equals_m10337((int64_t*)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Int64>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Int64>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisInt64_t507_m31154_gshared (Array_t * __this, int32_t ___index, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Int64>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Int64>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisInt64_t507_m31155_gshared (Array_t * __this, int32_t ___index, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		int64_t L_6 = ___item;
		int64_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (int64_t*)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Int64>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int64>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int64>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t507_m31156_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3512  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3512 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3512  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Array::Sort<System.Object>(T[])
// System.Void System.Array::Sort<System.Object>(T[])
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" void Array_Sort_TisObject_t_m31157_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		ObjectU5BU5D_t34* L_3 = ___array;
		NullCheck(L_3);
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (ObjectU5BU5D_t34*)(ObjectU5BU5D_t34*)NULL, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[])
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[])
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2160;
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31158_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___keys, ObjectU5BU5D_t34* ___items, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral2160 = il2cpp_codegen_string_literal_from_index(2160);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___keys;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral2160, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___keys;
		ObjectU5BU5D_t34* L_3 = ___items;
		ObjectU5BU5D_t34* L_4 = ___keys;
		NullCheck(L_4);
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (ObjectU5BU5D_t34*)L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object>(T[],System.Collections.Generic.IComparer`1<T>)
// System.Void System.Array::Sort<System.Object>(T[],System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" void Array_Sort_TisObject_t_m31159_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		ObjectU5BU5D_t34* L_3 = ___array;
		NullCheck(L_3);
		Object_t* L_4 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (ObjectU5BU5D_t34*)(ObjectU5BU5D_t34*)NULL, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Object_t*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Collections.Generic.IComparer`1<TKey>)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Collections.Generic.IComparer`1<TKey>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2160;
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31160_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___keys, ObjectU5BU5D_t34* ___items, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral2160 = il2cpp_codegen_string_literal_from_index(2160);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___keys;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral2160, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___keys;
		ObjectU5BU5D_t34* L_3 = ___items;
		ObjectU5BU5D_t34* L_4 = ___keys;
		NullCheck(L_4);
		Object_t* L_5 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (ObjectU5BU5D_t34*)L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object>(T[],System.Int32,System.Int32)
// System.Void System.Array::Sort<System.Object>(T[],System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" void Array_Sort_TisObject_t_m15427_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___index, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		int32_t L_3 = ___index;
		int32_t L_4 = ___length;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (ObjectU5BU5D_t34*)(ObjectU5BU5D_t34*)NULL, (int32_t)L_3, (int32_t)L_4, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Int32,System.Int32)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Int32,System.Int32)
extern "C" void Array_Sort_TisObject_t_TisObject_t_m31161_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___keys, ObjectU5BU5D_t34* ___items, int32_t ___index, int32_t ___length, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t34* L_0 = ___keys;
		ObjectU5BU5D_t34* L_1 = ___items;
		int32_t L_2 = ___index;
		int32_t L_3 = ___length;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_0, (ObjectU5BU5D_t34*)L_1, (int32_t)L_2, (int32_t)L_3, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object>(T[],System.Comparison`1<T>)
// System.Void System.Array::Sort<System.Object>(T[],System.Comparison`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" void Array_Sort_TisObject_t_m31162_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Comparison_1_t2591 * ___comparison, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		ObjectU5BU5D_t34* L_3 = ___array;
		NullCheck(L_3);
		Comparison_1_t2591 * L_4 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, Comparison_1_t2591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Comparison_1_t2591 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Array::TrueForAll<System.Object>(T[],System.Predicate`1<T>)
// System.Boolean System.Array::TrueForAll<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2890;
extern "C" bool Array_TrueForAll_TisObject_t_m31163_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2890 = il2cpp_codegen_string_literal_from_index(2890);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t34* V_1 = {0};
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2585 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral2890, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t34* L_4 = ___array;
		V_1 = (ObjectU5BU5D_t34*)L_4;
		V_2 = (int32_t)0;
		goto IL_0045;
	}

IL_002b:
	{
		ObjectU5BU5D_t34* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		Predicate_1_t2585 * L_8 = ___match;
		Object_t * L_9 = V_0;
		NullCheck((Predicate_1_t2585 *)L_8);
		bool L_10 = (( bool (*) (Predicate_1_t2585 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2585 *)L_8, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_10)
		{
			goto IL_0041;
		}
	}
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_11 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_12 = V_2;
		ObjectU5BU5D_t34* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}
}
// System.Void System.Array::ForEach<System.Object>(T[],System.Action`1<T>)
// System.Void System.Array::ForEach<System.Object>(T[],System.Action`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral545;
extern "C" void Array_ForEach_TisObject_t_m31164_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Action_1_t2701 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral545 = il2cpp_codegen_string_literal_from_index(545);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t34* V_1 = {0};
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Action_1_t2701 * L_2 = ___action;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral545, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t34* L_4 = ___array;
		V_1 = (ObjectU5BU5D_t34*)L_4;
		V_2 = (int32_t)0;
		goto IL_003e;
	}

IL_002b:
	{
		ObjectU5BU5D_t34* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		Action_1_t2701 * L_8 = ___action;
		Object_t * L_9 = V_0;
		NullCheck((Action_1_t2701 *)L_8);
		(( void (*) (Action_1_t2701 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Action_1_t2701 *)L_8, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_11 = V_2;
		ObjectU5BU5D_t34* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		return;
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral3154;
extern "C" ObjectU5BU5D_t34* Array_ConvertAll_TisObject_t_TisObject_t_m31165_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Converter_2_t3513 * ___converter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral3154 = il2cpp_codegen_string_literal_from_index(3154);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Converter_2_t3513 * L_2 = ___converter;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral3154, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t34* L_4 = ___array;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t34* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t3513 * L_7 = ___converter;
		ObjectU5BU5D_t34* L_8 = ___array;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck((Converter_2_t3513 *)L_7);
		Object_t * L_11 = (( Object_t * (*) (Converter_2_t3513 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Converter_2_t3513 *)L_7, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_8, L_10, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, L_6, sizeof(Object_t *))) = (Object_t *)L_11;
		int32_t L_12 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_13 = V_1;
		ObjectU5BU5D_t34* L_14 = ___array;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t34* L_15 = V_0;
		return L_15;
	}
}
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Predicate`1<T>)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_FindLastIndex_TisObject_t_m31166_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		ObjectU5BU5D_t34* L_3 = ___array;
		NullCheck(L_3);
		Predicate_1_t2585 * L_4 = ___match;
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Predicate_1_t2585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Predicate_1_t2585 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Int32,System.Predicate`1<T>)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Int32,System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2890;
extern "C" int32_t Array_FindLastIndex_TisObject_t_m31167_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___startIndex, int32_t ___count, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2890 = il2cpp_codegen_string_literal_from_index(2890);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2585 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral2890, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___startIndex;
		ObjectU5BU5D_t34* L_5 = ___array;
		NullCheck(L_5);
		if ((((int32_t)L_4) > ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		int32_t L_7 = ___count;
		ObjectU5BU5D_t34* L_8 = ___array;
		NullCheck(L_8);
		if ((((int32_t)((int32_t)((int32_t)L_6+(int32_t)L_7))) <= ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		ArgumentOutOfRangeException_t1665 * L_9 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9264(L_9, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_003c:
	{
		int32_t L_10 = ___startIndex;
		int32_t L_11 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10+(int32_t)L_11))-(int32_t)1));
		goto IL_005f;
	}

IL_0047:
	{
		Predicate_1_t2585 * L_12 = ___match;
		ObjectU5BU5D_t34* L_13 = ___array;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		NullCheck((Predicate_1_t2585 *)L_12);
		bool L_16 = (( bool (*) (Predicate_1_t2585 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2585 *)L_12, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_13, L_15, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_16)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_17 = V_0;
		return L_17;
	}

IL_005b:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18-(int32_t)1));
	}

IL_005f:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = ___startIndex;
		if ((((int32_t)L_19) >= ((int32_t)L_20)))
		{
			goto IL_0047;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern "C" int32_t Array_FindLastIndex_TisObject_t_m31168_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___startIndex, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m14593(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_000c:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		int32_t L_3 = ___startIndex;
		ObjectU5BU5D_t34* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = ___startIndex;
		Predicate_1_t2585 * L_6 = ___match;
		int32_t L_7 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Predicate_1_t2585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (int32_t)L_3, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))-(int32_t)L_5)), (Predicate_1_t2585 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Predicate`1<T>)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_FindIndex_TisObject_t_m31169_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		ObjectU5BU5D_t34* L_3 = ___array;
		NullCheck(L_3);
		Predicate_1_t2585 * L_4 = ___match;
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Predicate_1_t2585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Predicate_1_t2585 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Int32,System.Predicate`1<T>)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Int32,System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2890;
extern "C" int32_t Array_FindIndex_TisObject_t_m31170_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___startIndex, int32_t ___count, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2890 = il2cpp_codegen_string_literal_from_index(2890);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2585 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral2890, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___startIndex;
		ObjectU5BU5D_t34* L_5 = ___array;
		NullCheck(L_5);
		if ((((int32_t)L_4) > ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		int32_t L_7 = ___count;
		ObjectU5BU5D_t34* L_8 = ___array;
		NullCheck(L_8);
		if ((((int32_t)((int32_t)((int32_t)L_6+(int32_t)L_7))) <= ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		ArgumentOutOfRangeException_t1665 * L_9 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9264(L_9, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_003c:
	{
		int32_t L_10 = ___startIndex;
		V_0 = (int32_t)L_10;
		goto IL_005b;
	}

IL_0043:
	{
		Predicate_1_t2585 * L_11 = ___match;
		ObjectU5BU5D_t34* L_12 = ___array;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NullCheck((Predicate_1_t2585 *)L_11);
		bool L_15 = (( bool (*) (Predicate_1_t2585 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2585 *)L_11, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_12, L_14, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_15)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_16 = V_0;
		return L_16;
	}

IL_0057:
	{
		int32_t L_17 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_18 = V_0;
		int32_t L_19 = ___startIndex;
		int32_t L_20 = ___count;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)((int32_t)L_19+(int32_t)L_20)))))
		{
			goto IL_0043;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_FindIndex_TisObject_t_m31171_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___startIndex, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		int32_t L_3 = ___startIndex;
		ObjectU5BU5D_t34* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = ___startIndex;
		Predicate_1_t2585 * L_6 = ___match;
		int32_t L_7 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Predicate_1_t2585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (int32_t)L_3, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))-(int32_t)L_5)), (Predicate_1_t2585 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_BinarySearch_TisObject_t_m31172_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		ObjectU5BU5D_t34* L_3 = ___array;
		NullCheck(L_3);
		Object_t * L_4 = ___value;
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Object_t *)L_4, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t532_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2147;
extern Il2CppCodeGenString* _stringLiteral2034;
extern Il2CppCodeGenString* _stringLiteral2148;
extern Il2CppCodeGenString* _stringLiteral2149;
extern Il2CppCodeGenString* _stringLiteral2151;
extern "C" int32_t Array_BinarySearch_TisObject_t_m31173_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___index, int32_t ___length, Object_t * ___value, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		InvalidOperationException_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(242);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2147 = il2cpp_codegen_string_literal_from_index(2147);
		_stringLiteral2034 = il2cpp_codegen_string_literal_from_index(2034);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		_stringLiteral2149 = il2cpp_codegen_string_literal_from_index(2149);
		_stringLiteral2151 = il2cpp_codegen_string_literal_from_index(2151);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t40 * V_4 = {0};
	int32_t V_5 = 0;
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2147, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_4 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_4, (String_t*)_stringLiteral1325, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___length;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_6 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_7 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_7, (String_t*)_stringLiteral2034, (String_t*)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0049:
	{
		int32_t L_8 = ___index;
		ObjectU5BU5D_t34* L_9 = ___array;
		NullCheck(L_9);
		int32_t L_10 = ___length;
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))-(int32_t)L_10)))))
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2149, /*hidden argument*/NULL);
		ArgumentException_t513 * L_12 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0064:
	{
		Object_t* L_13 = ___comparer;
		if (L_13)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		Comparer_1_t2586 * L_14 = (( Comparer_1_t2586 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		___comparer = (Object_t*)L_14;
	}

IL_0072:
	{
		int32_t L_15 = ___index;
		V_0 = (int32_t)L_15;
		int32_t L_16 = ___index;
		int32_t L_17 = ___length;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16+(int32_t)L_17))-(int32_t)1));
		V_2 = (int32_t)0;
	}

IL_007c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00bb;
		}

IL_0081:
		{
			int32_t L_18 = V_0;
			int32_t L_19 = V_1;
			int32_t L_20 = V_0;
			V_3 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_19-(int32_t)L_20))/(int32_t)2))));
			Object_t* L_21 = ___comparer;
			Object_t * L_22 = ___value;
			ObjectU5BU5D_t34* L_23 = ___array;
			int32_t L_24 = V_3;
			NullCheck(L_23);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
			int32_t L_25 = L_24;
			NullCheck((Object_t*)L_21);
			int32_t L_26 = (int32_t)InterfaceFuncInvoker2< int32_t, Object_t *, Object_t * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(T,T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Object_t*)L_21, (Object_t *)L_22, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_23, L_25, sizeof(Object_t *))));
			V_2 = (int32_t)L_26;
			int32_t L_27 = V_2;
			if (L_27)
			{
				goto IL_00a7;
			}
		}

IL_009f:
		{
			int32_t L_28 = V_3;
			V_5 = (int32_t)L_28;
			goto IL_00e3;
		}

IL_00a7:
		{
			int32_t L_29 = V_2;
			if ((((int32_t)L_29) >= ((int32_t)0)))
			{
				goto IL_00b7;
			}
		}

IL_00ae:
		{
			int32_t L_30 = V_3;
			V_1 = (int32_t)((int32_t)((int32_t)L_30-(int32_t)1));
			goto IL_00bb;
		}

IL_00b7:
		{
			int32_t L_31 = V_3;
			V_0 = (int32_t)((int32_t)((int32_t)L_31+(int32_t)1));
		}

IL_00bb:
		{
			int32_t L_32 = V_0;
			int32_t L_33 = V_1;
			if ((((int32_t)L_32) <= ((int32_t)L_33)))
			{
				goto IL_0081;
			}
		}

IL_00c2:
		{
			goto IL_00e0;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t40 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t40_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00c7;
		throw e;
	}

CATCH_00c7:
	{ // begin catch(System.Exception)
		{
			V_4 = (Exception_t40 *)((Exception_t40 *)__exception_local);
			String_t* L_34 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2151, /*hidden argument*/NULL);
			Exception_t40 * L_35 = V_4;
			InvalidOperationException_t532 * L_36 = (InvalidOperationException_t532 *)il2cpp_codegen_object_new (InvalidOperationException_t532_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m15017(L_36, (String_t*)L_34, (Exception_t40 *)L_35, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_36);
		}

IL_00db:
		{
			goto IL_00e0;
		}
	} // end catch (depth: 1)

IL_00e0:
	{
		int32_t L_37 = V_0;
		return ((~L_37));
	}

IL_00e3:
	{
		int32_t L_38 = V_5;
		return L_38;
	}
}
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T,System.Collections.Generic.IComparer`1<T>)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_BinarySearch_TisObject_t_m31174_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		ObjectU5BU5D_t34* L_3 = ___array;
		NullCheck(L_3);
		Object_t * L_4 = ___value;
		Object_t* L_5 = ___comparer;
		int32_t L_6 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Object_t *)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_6;
	}
}
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T)
extern "C" int32_t Array_BinarySearch_TisObject_t_m31175_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, int32_t ___index, int32_t ___length, Object_t * ___value, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		int32_t L_1 = ___index;
		int32_t L_2 = ___length;
		Object_t * L_3 = ___value;
		int32_t L_4 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_0, (int32_t)L_1, (int32_t)L_2, (Object_t *)L_3, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// System.Int32 System.Array::IndexOf<System.Object>(T[],T)
// System.Int32 System.Array::IndexOf<System.Object>(T[],T)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_IndexOf_TisObject_t_m15433_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		Object_t * L_3 = ___value;
		ObjectU5BU5D_t34* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (Object_t *)L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// System.Int32 System.Array::IndexOf<System.Object>(T[],T,System.Int32)
// System.Int32 System.Array::IndexOf<System.Object>(T[],T,System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_IndexOf_TisObject_t_m31176_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		Object_t * L_3 = ___value;
		int32_t L_4 = ___startIndex;
		ObjectU5BU5D_t34* L_5 = ___array;
		NullCheck(L_5);
		int32_t L_6 = ___startIndex;
		int32_t L_7 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (Object_t *)L_3, (int32_t)L_4, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))-(int32_t)L_6)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T)
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_LastIndexOf_TisObject_t_m31177_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))))
		{
			goto IL_001b;
		}
	}
	{
		return (-1);
	}

IL_001b:
	{
		ObjectU5BU5D_t34* L_3 = ___array;
		Object_t * L_4 = ___value;
		ObjectU5BU5D_t34* L_5 = ___array;
		NullCheck(L_5);
		int32_t L_6 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_3, (Object_t *)L_4, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_6;
	}
}
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T,System.Int32)
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T,System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_LastIndexOf_TisObject_t_m31178_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		Object_t * L_3 = ___value;
		int32_t L_4 = ___startIndex;
		int32_t L_5 = ___startIndex;
		int32_t L_6 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34*, Object_t *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34*)L_2, (Object_t *)L_3, (int32_t)L_4, (int32_t)((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_6;
	}
}
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T,System.Int32,System.Int32)
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T,System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_LastIndexOf_TisObject_t_m31179_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t2582 * V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_3 = ___startIndex;
		ObjectU5BU5D_t34* L_4 = ___array;
		NullCheck((Array_t *)L_4);
		int32_t L_5 = Array_GetLowerBound_m10928((Array_t *)L_4, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		ObjectU5BU5D_t34* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetUpperBound_m10940((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)L_6) > ((int32_t)L_8)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_9 = ___startIndex;
		int32_t L_10 = ___count;
		ObjectU5BU5D_t34* L_11 = ___array;
		NullCheck((Array_t *)L_11);
		int32_t L_12 = Array_GetLowerBound_m10928((Array_t *)L_11, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))+(int32_t)1))) >= ((int32_t)L_12)))
		{
			goto IL_0049;
		}
	}

IL_0043:
	{
		ArgumentOutOfRangeException_t1665 * L_13 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9264(L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		EqualityComparer_1_t2582 * L_14 = (( EqualityComparer_1_t2582 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (EqualityComparer_1_t2582 *)L_14;
		int32_t L_15 = ___startIndex;
		V_1 = (int32_t)L_15;
		goto IL_006f;
	}

IL_0056:
	{
		EqualityComparer_1_t2582 * L_16 = V_0;
		ObjectU5BU5D_t34* L_17 = ___array;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		Object_t * L_20 = ___value;
		NullCheck((EqualityComparer_1_t2582 *)L_16);
		bool L_21 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t2582 *)L_16, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_17, L_19, sizeof(Object_t *))), (Object_t *)L_20);
		if (!L_21)
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_22 = V_1;
		return L_22;
	}

IL_006b:
	{
		int32_t L_23 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_006f:
	{
		int32_t L_24 = V_1;
		int32_t L_25 = ___startIndex;
		int32_t L_26 = ___count;
		if ((((int32_t)L_24) >= ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_25-(int32_t)L_26))+(int32_t)1)))))
		{
			goto IL_0056;
		}
	}
	{
		return (-1);
	}
}
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2890;
extern "C" ObjectU5BU5D_t34* Array_FindAll_TisObject_t_m31180_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2890 = il2cpp_codegen_string_literal_from_index(2890);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t34* V_1 = {0};
	Object_t * V_2 = {0};
	ObjectU5BU5D_t34* V_3 = {0};
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2585 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral2890, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t34* L_4 = ___array;
		NullCheck(L_4);
		V_1 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))));
		ObjectU5BU5D_t34* L_5 = ___array;
		V_3 = (ObjectU5BU5D_t34*)L_5;
		V_4 = (int32_t)0;
		goto IL_005e;
	}

IL_0037:
	{
		ObjectU5BU5D_t34* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_2 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_6, L_8, sizeof(Object_t *)));
		Predicate_1_t2585 * L_9 = ___match;
		Object_t * L_10 = V_2;
		NullCheck((Predicate_1_t2585 *)L_9);
		bool L_11 = (( bool (*) (Predicate_1_t2585 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Predicate_1_t2585 *)L_9, (Object_t *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t34* L_12 = V_1;
		int32_t L_13 = V_0;
		int32_t L_14 = (int32_t)L_13;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		Object_t * L_15 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, L_14, sizeof(Object_t *))) = (Object_t *)L_15;
	}

IL_0058:
	{
		int32_t L_16 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_17 = V_4;
		ObjectU5BU5D_t34* L_18 = V_3;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_19 = V_0;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t34**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t34**)(&V_1), (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		ObjectU5BU5D_t34* L_20 = V_1;
		return L_20;
	}
}
// System.Boolean System.Array::Exists<System.Object>(T[],System.Predicate`1<T>)
// System.Boolean System.Array::Exists<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2890;
extern "C" bool Array_Exists_TisObject_t_m31181_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2890 = il2cpp_codegen_string_literal_from_index(2890);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t34* V_1 = {0};
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2585 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral2890, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t34* L_4 = ___array;
		V_1 = (ObjectU5BU5D_t34*)L_4;
		V_2 = (int32_t)0;
		goto IL_0045;
	}

IL_002b:
	{
		ObjectU5BU5D_t34* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		Predicate_1_t2585 * L_8 = ___match;
		Object_t * L_9 = V_0;
		NullCheck((Predicate_1_t2585 *)L_8);
		bool L_10 = (( bool (*) (Predicate_1_t2585 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2585 *)L_8, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		return 1;
	}

IL_0041:
	{
		int32_t L_11 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_12 = V_2;
		ObjectU5BU5D_t34* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		return 0;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Object>(T[])
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Object>(T[])
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" ReadOnlyCollection_1_t2578 * Array_AsReadOnly_TisObject_t_m15447_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t34* L_2 = ___array;
		ArrayReadOnlyList_1_t3514 * L_3 = (ArrayReadOnlyList_1_t3514 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		(( void (*) (ArrayReadOnlyList_1_t3514 *, ObjectU5BU5D_t34*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_3, (ObjectU5BU5D_t34*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		ReadOnlyCollection_1_t2578 * L_4 = (ReadOnlyCollection_1_t2578 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		(( void (*) (ReadOnlyCollection_1_t2578 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_4, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_4;
	}
}
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2890;
extern "C" Object_t * Array_Find_TisObject_t_m31182_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2890 = il2cpp_codegen_string_literal_from_index(2890);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t34* V_1 = {0};
	int32_t V_2 = 0;
	Object_t * V_3 = {0};
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2585 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral2890, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t34* L_4 = ___array;
		V_1 = (ObjectU5BU5D_t34*)L_4;
		V_2 = (int32_t)0;
		goto IL_0045;
	}

IL_002b:
	{
		ObjectU5BU5D_t34* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		Predicate_1_t2585 * L_8 = ___match;
		Object_t * L_9 = V_0;
		NullCheck((Predicate_1_t2585 *)L_8);
		bool L_10 = (( bool (*) (Predicate_1_t2585 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2585 *)L_8, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		Object_t * L_11 = V_0;
		return L_11;
	}

IL_0041:
	{
		int32_t L_12 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_13 = V_2;
		ObjectU5BU5D_t34* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_3));
		Object_t * L_15 = V_3;
		return L_15;
	}
}
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2890;
extern "C" Object_t * Array_FindLast_TisObject_t_m31183_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___array, Predicate_1_t2585 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2890 = il2cpp_codegen_string_literal_from_index(2890);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	{
		ObjectU5BU5D_t34* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2585 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t512 * L_3 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_3, (String_t*)_stringLiteral2890, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t34* L_4 = ___array;
		NullCheck(L_4);
		V_0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))-(int32_t)1));
		goto IL_004b;
	}

IL_002d:
	{
		Predicate_1_t2585 * L_5 = ___match;
		ObjectU5BU5D_t34* L_6 = ___array;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck((Predicate_1_t2585 *)L_5);
		bool L_9 = (( bool (*) (Predicate_1_t2585 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2585 *)L_5, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_6, L_8, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		ObjectU5BU5D_t34* L_10 = ___array;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		return (*(Object_t **)(Object_t **)SZArrayLdElema(L_10, L_12, sizeof(Object_t *)));
	}

IL_0047:
	{
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_004b:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_1));
		Object_t * L_15 = V_1;
		return L_15;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" TableRange_t1889  Array_InternalArray__get_Item_TisTableRange_t1889_m31184_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	TableRange_t1889  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (TableRange_t1889 *)(&V_0));
		TableRange_t1889  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Void System.Array::InternalArray__ICollection_Add<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t1889_m31185_gshared (Array_t * __this, TableRange_t1889  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTableRange_t1889_m31186_gshared (Array_t * __this, TableRange_t1889  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TableRange_t1889  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TableRange_t1889 *)(&V_2));
		TableRange_t1889  L_5 = ___item;
		goto IL_004d;
	}
	{
		TableRange_t1889  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		TableRange_t1889  L_7 = V_2;
		TableRange_t1889  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t1889_m31187_gshared (Array_t * __this, TableRangeU5BU5D_t1891* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		TableRangeU5BU5D_t1891* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t1891* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t1891* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		TableRangeU5BU5D_t1891* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t1891* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTableRange_t1889_m31188_gshared (Array_t * __this, TableRange_t1889  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisTableRange_t1889_m31189_gshared (Array_t * __this, TableRange_t1889  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TableRange_t1889  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TableRange_t1889 *)(&V_2));
		TableRange_t1889  L_5 = ___item;
		goto IL_005d;
	}
	{
		TableRange_t1889  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		TableRange_t1889  L_10 = ___item;
		TableRange_t1889  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisTableRange_t1889_m31190_gshared (Array_t * __this, int32_t ___index, TableRange_t1889  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisTableRange_t1889_m31191_gshared (Array_t * __this, int32_t ___index, TableRange_t1889  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		TableRange_t1889  L_6 = ___item;
		TableRange_t1889  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (TableRange_t1889 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1889_m31192_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3518  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3518 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3518  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" Slot_t1966  Array_InternalArray__get_Item_TisSlot_t1966_m31193_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	Slot_t1966  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Slot_t1966 *)(&V_0));
		Slot_t1966  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1966_m31194_gshared (Array_t * __this, Slot_t1966  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t1966_m31195_gshared (Array_t * __this, Slot_t1966  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t1966  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t1966 *)(&V_2));
		Slot_t1966  L_5 = ___item;
		goto IL_004d;
	}
	{
		Slot_t1966  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		Slot_t1966  L_7 = V_2;
		Slot_t1966  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1966_m31196_gshared (Array_t * __this, SlotU5BU5D_t1973* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlotU5BU5D_t1973* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1973* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1973* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		SlotU5BU5D_t1973* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1973* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t1966_m31197_gshared (Array_t * __this, Slot_t1966  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t1966_m31198_gshared (Array_t * __this, Slot_t1966  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t1966  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t1966 *)(&V_2));
		Slot_t1966  L_5 = ___item;
		goto IL_005d;
	}
	{
		Slot_t1966  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		Slot_t1966  L_10 = ___item;
		Slot_t1966  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisSlot_t1966_m31199_gshared (Array_t * __this, int32_t ___index, Slot_t1966  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisSlot_t1966_m31200_gshared (Array_t * __this, int32_t ___index, Slot_t1966  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		Slot_t1966  L_6 = ___item;
		Slot_t1966  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Slot_t1966 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1966_m31201_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3527  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3527 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3527  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" Slot_t1974  Array_InternalArray__get_Item_TisSlot_t1974_m31202_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	Slot_t1974  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Slot_t1974 *)(&V_0));
		Slot_t1974  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1974_m31203_gshared (Array_t * __this, Slot_t1974  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t1974_m31204_gshared (Array_t * __this, Slot_t1974  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t1974  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t1974 *)(&V_2));
		Slot_t1974  L_5 = ___item;
		goto IL_004d;
	}
	{
		Slot_t1974  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		Slot_t1974  L_7 = V_2;
		Slot_t1974  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1974_m31205_gshared (Array_t * __this, SlotU5BU5D_t1978* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlotU5BU5D_t1978* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1978* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1978* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		SlotU5BU5D_t1978* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1978* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t1974_m31206_gshared (Array_t * __this, Slot_t1974  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t1974_m31207_gshared (Array_t * __this, Slot_t1974  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t1974  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t1974 *)(&V_2));
		Slot_t1974  L_5 = ___item;
		goto IL_005d;
	}
	{
		Slot_t1974  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		Slot_t1974  L_10 = ___item;
		Slot_t1974  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisSlot_t1974_m31208_gshared (Array_t * __this, int32_t ___index, Slot_t1974  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisSlot_t1974_m31209_gshared (Array_t * __this, int32_t ___index, Slot_t1974  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		Slot_t1974  L_6 = ___item;
		Slot_t1974  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Slot_t1974 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1974_m31210_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3528  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3528 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3528  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" ILTokenInfo_t2053  Array_InternalArray__get_Item_TisILTokenInfo_t2053_m31211_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ILTokenInfo_t2053  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (ILTokenInfo_t2053 *)(&V_0));
		ILTokenInfo_t2053  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILTokenInfo>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILTokenInfo>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisILTokenInfo_t2053_m31212_gshared (Array_t * __this, ILTokenInfo_t2053  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILTokenInfo>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILTokenInfo>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisILTokenInfo_t2053_m31213_gshared (Array_t * __this, ILTokenInfo_t2053  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ILTokenInfo_t2053  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ILTokenInfo_t2053 *)(&V_2));
		ILTokenInfo_t2053  L_5 = ___item;
		goto IL_004d;
	}
	{
		ILTokenInfo_t2053  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		ILTokenInfo_t2053  L_7 = V_2;
		ILTokenInfo_t2053  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILTokenInfo>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILTokenInfo>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t2053_m31214_gshared (Array_t * __this, ILTokenInfoU5BU5D_t2056* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		ILTokenInfoU5BU5D_t2056* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ILTokenInfoU5BU5D_t2056* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		ILTokenInfoU5BU5D_t2056* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		ILTokenInfoU5BU5D_t2056* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ILTokenInfoU5BU5D_t2056* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILTokenInfo>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILTokenInfo>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisILTokenInfo_t2053_m31215_gshared (Array_t * __this, ILTokenInfo_t2053  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILTokenInfo>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILTokenInfo>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisILTokenInfo_t2053_m31216_gshared (Array_t * __this, ILTokenInfo_t2053  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ILTokenInfo_t2053  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ILTokenInfo_t2053 *)(&V_2));
		ILTokenInfo_t2053  L_5 = ___item;
		goto IL_005d;
	}
	{
		ILTokenInfo_t2053  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		ILTokenInfo_t2053  L_10 = ___item;
		ILTokenInfo_t2053  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisILTokenInfo_t2053_m31217_gshared (Array_t * __this, int32_t ___index, ILTokenInfo_t2053  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisILTokenInfo_t2053_m31218_gshared (Array_t * __this, int32_t ___index, ILTokenInfo_t2053  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		ILTokenInfo_t2053  L_6 = ___item;
		ILTokenInfo_t2053  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (ILTokenInfo_t2053 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILTokenInfo>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILTokenInfo>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t2053_m31219_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3534  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3534 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3534  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" LabelData_t2055  Array_InternalArray__get_Item_TisLabelData_t2055_m31220_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	LabelData_t2055  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (LabelData_t2055 *)(&V_0));
		LabelData_t2055  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisLabelData_t2055_m31221_gshared (Array_t * __this, LabelData_t2055  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelData_t2055_m31222_gshared (Array_t * __this, LabelData_t2055  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelData_t2055  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelData_t2055 *)(&V_2));
		LabelData_t2055  L_5 = ___item;
		goto IL_004d;
	}
	{
		LabelData_t2055  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		LabelData_t2055  L_7 = V_2;
		LabelData_t2055  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelData_t2055_m31223_gshared (Array_t * __this, LabelDataU5BU5D_t2057* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		LabelDataU5BU5D_t2057* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t2057* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t2057* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		LabelDataU5BU5D_t2057* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t2057* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelData_t2055_m31224_gshared (Array_t * __this, LabelData_t2055  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelData_t2055_m31225_gshared (Array_t * __this, LabelData_t2055  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelData_t2055  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelData_t2055 *)(&V_2));
		LabelData_t2055  L_5 = ___item;
		goto IL_005d;
	}
	{
		LabelData_t2055  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		LabelData_t2055  L_10 = ___item;
		LabelData_t2055  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisLabelData_t2055_m31226_gshared (Array_t * __this, int32_t ___index, LabelData_t2055  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisLabelData_t2055_m31227_gshared (Array_t * __this, int32_t ___index, LabelData_t2055  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		LabelData_t2055  L_6 = ___item;
		LabelData_t2055  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (LabelData_t2055 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t2055_m31228_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3535  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3535 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3535  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" LabelFixup_t2054  Array_InternalArray__get_Item_TisLabelFixup_t2054_m31229_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	LabelFixup_t2054  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (LabelFixup_t2054 *)(&V_0));
		LabelFixup_t2054  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisLabelFixup_t2054_m31230_gshared (Array_t * __this, LabelFixup_t2054  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelFixup_t2054_m31231_gshared (Array_t * __this, LabelFixup_t2054  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelFixup_t2054  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelFixup_t2054 *)(&V_2));
		LabelFixup_t2054  L_5 = ___item;
		goto IL_004d;
	}
	{
		LabelFixup_t2054  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		LabelFixup_t2054  L_7 = V_2;
		LabelFixup_t2054  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t2054_m31232_gshared (Array_t * __this, LabelFixupU5BU5D_t2058* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		LabelFixupU5BU5D_t2058* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t2058* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t2058* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		LabelFixupU5BU5D_t2058* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t2058* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelFixup_t2054_m31233_gshared (Array_t * __this, LabelFixup_t2054  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelFixup_t2054_m31234_gshared (Array_t * __this, LabelFixup_t2054  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelFixup_t2054  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelFixup_t2054 *)(&V_2));
		LabelFixup_t2054  L_5 = ___item;
		goto IL_005d;
	}
	{
		LabelFixup_t2054  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		LabelFixup_t2054  L_10 = ___item;
		LabelFixup_t2054  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisLabelFixup_t2054_m31235_gshared (Array_t * __this, int32_t ___index, LabelFixup_t2054  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisLabelFixup_t2054_m31236_gshared (Array_t * __this, int32_t ___index, LabelFixup_t2054  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		LabelFixup_t2054  L_6 = ___item;
		LabelFixup_t2054  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (LabelFixup_t2054 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t2054_m31237_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3536  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3536 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3536  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" CustomAttributeTypedArgument_t2101  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t2101_m31238_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeTypedArgument_t2101  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (CustomAttributeTypedArgument_t2101 *)(&V_0));
		CustomAttributeTypedArgument_t2101  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeTypedArgument>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeTypedArgument>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t2101_m31239_gshared (Array_t * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeTypedArgument>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeTypedArgument>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t2101_m31240_gshared (Array_t * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CustomAttributeTypedArgument_t2101  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (CustomAttributeTypedArgument_t2101 *)(&V_2));
		CustomAttributeTypedArgument_t2101  L_5 = ___item;
		goto IL_004d;
	}
	{
		CustomAttributeTypedArgument_t2101  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		CustomAttributeTypedArgument_t2101  L_7 = V_2;
		CustomAttributeTypedArgument_t2101  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((CustomAttributeTypedArgument_t2101 *)(&___item));
		bool L_10 = CustomAttributeTypedArgument_Equals_m12872((CustomAttributeTypedArgument_t2101 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t2101_m31241_gshared (Array_t * __this, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2537* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2537* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2537* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeTypedArgument>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeTypedArgument>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t2101_m31242_gshared (Array_t * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeTypedArgument>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeTypedArgument>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t2101_m31243_gshared (Array_t * __this, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CustomAttributeTypedArgument_t2101  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (CustomAttributeTypedArgument_t2101 *)(&V_2));
		CustomAttributeTypedArgument_t2101  L_5 = ___item;
		goto IL_005d;
	}
	{
		CustomAttributeTypedArgument_t2101  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		CustomAttributeTypedArgument_t2101  L_10 = ___item;
		CustomAttributeTypedArgument_t2101  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((CustomAttributeTypedArgument_t2101 *)(&V_2));
		bool L_13 = CustomAttributeTypedArgument_Equals_m12872((CustomAttributeTypedArgument_t2101 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t2101_m31244_gshared (Array_t * __this, int32_t ___index, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t2101_m31245_gshared (Array_t * __this, int32_t ___index, CustomAttributeTypedArgument_t2101  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		CustomAttributeTypedArgument_t2101  L_6 = ___item;
		CustomAttributeTypedArgument_t2101  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (CustomAttributeTypedArgument_t2101 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeTypedArgument>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeTypedArgument>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t2101_m31246_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3543  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3543 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3543  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" CustomAttributeNamedArgument_t2100  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t2100_m31247_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeNamedArgument_t2100  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (CustomAttributeNamedArgument_t2100 *)(&V_0));
		CustomAttributeNamedArgument_t2100  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeNamedArgument>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeNamedArgument>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t2100_m31248_gshared (Array_t * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeNamedArgument>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeNamedArgument>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t2100_m31249_gshared (Array_t * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CustomAttributeNamedArgument_t2100  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (CustomAttributeNamedArgument_t2100 *)(&V_2));
		CustomAttributeNamedArgument_t2100  L_5 = ___item;
		goto IL_004d;
	}
	{
		CustomAttributeNamedArgument_t2100  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		CustomAttributeNamedArgument_t2100  L_7 = V_2;
		CustomAttributeNamedArgument_t2100  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((CustomAttributeNamedArgument_t2100 *)(&___item));
		bool L_10 = CustomAttributeNamedArgument_Equals_m12869((CustomAttributeNamedArgument_t2100 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t2100_m31250_gshared (Array_t * __this, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t2538* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t2538* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t2538* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeNamedArgument>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeNamedArgument>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t2100_m31251_gshared (Array_t * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeNamedArgument>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeNamedArgument>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t2100_m31252_gshared (Array_t * __this, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CustomAttributeNamedArgument_t2100  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (CustomAttributeNamedArgument_t2100 *)(&V_2));
		CustomAttributeNamedArgument_t2100  L_5 = ___item;
		goto IL_005d;
	}
	{
		CustomAttributeNamedArgument_t2100  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		CustomAttributeNamedArgument_t2100  L_10 = ___item;
		CustomAttributeNamedArgument_t2100  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((CustomAttributeNamedArgument_t2100 *)(&V_2));
		bool L_13 = CustomAttributeNamedArgument_Equals_m12869((CustomAttributeNamedArgument_t2100 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t2100_m31253_gshared (Array_t * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t2100_m31254_gshared (Array_t * __this, int32_t ___index, CustomAttributeNamedArgument_t2100  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		CustomAttributeNamedArgument_t2100  L_6 = ___item;
		CustomAttributeNamedArgument_t2100  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (CustomAttributeNamedArgument_t2100 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeNamedArgument>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeNamedArgument>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t2100_m31255_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3544  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3544 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3544  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C" CustomAttributeTypedArgumentU5BU5D_t2537* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t2101_m15429_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___values, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t2537* V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t2537*)((CustomAttributeTypedArgumentU5BU5D_t2537*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t34* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		*((CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_1, L_2, sizeof(CustomAttributeTypedArgument_t2101 ))) = (CustomAttributeTypedArgument_t2101 )((*(CustomAttributeTypedArgument_t2101 *)((CustomAttributeTypedArgument_t2101 *)UnBox ((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t34* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_9 = V_0;
		return L_9;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeTypedArgument>(T[])
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeTypedArgument>(T[])
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" ReadOnlyCollection_1_t2539 * Array_AsReadOnly_TisCustomAttributeTypedArgument_t2101_m15430_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_2 = ___array;
		ArrayReadOnlyList_1_t3554 * L_3 = (ArrayReadOnlyList_1_t3554 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		(( void (*) (ArrayReadOnlyList_1_t3554 *, CustomAttributeTypedArgumentU5BU5D_t2537*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_3, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		ReadOnlyCollection_1_t2539 * L_4 = (ReadOnlyCollection_1_t2539 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		(( void (*) (ReadOnlyCollection_1_t2539 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_4, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_4;
	}
}
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t2101_m31256_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537** ___array, int32_t ___newSize, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t2537** G_B2_0 = {0};
	CustomAttributeTypedArgumentU5BU5D_t2537** G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	CustomAttributeTypedArgumentU5BU5D_t2537** G_B3_1 = {0};
	{
		CustomAttributeTypedArgumentU5BU5D_t2537** L_0 = ___array;
		CustomAttributeTypedArgumentU5BU5D_t2537** L_1 = ___array;
		G_B1_0 = L_0;
		if ((*((CustomAttributeTypedArgumentU5BU5D_t2537**)L_1)))
		{
			G_B2_0 = L_0;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_000e:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537** L_2 = ___array;
		NullCheck((*((CustomAttributeTypedArgumentU5BU5D_t2537**)L_2)));
		G_B3_0 = (((int32_t)((int32_t)(((Array_t *)(*((CustomAttributeTypedArgumentU5BU5D_t2537**)L_2)))->max_length))));
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		int32_t L_3 = ___newSize;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537**, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537**)G_B3_1, (int32_t)G_B3_0, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32,System.Int32)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t2101_m31257_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537** ___array, int32_t ___length, int32_t ___newSize, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeTypedArgumentU5BU5D_t2537* V_0 = {0};
	{
		int32_t L_0 = ___newSize;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_1 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9264(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_000d:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537** L_2 = ___array;
		if ((*((CustomAttributeTypedArgumentU5BU5D_t2537**)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537** L_3 = ___array;
		int32_t L_4 = ___newSize;
		*((Object_t **)(L_3)) = (Object_t *)((CustomAttributeTypedArgumentU5BU5D_t2537*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_4));
		return;
	}

IL_001d:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537** L_5 = ___array;
		NullCheck((*((CustomAttributeTypedArgumentU5BU5D_t2537**)L_5)));
		int32_t L_6 = ___newSize;
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)(*((CustomAttributeTypedArgumentU5BU5D_t2537**)L_5)))->max_length))))) == ((uint32_t)L_6))))
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		int32_t L_7 = ___newSize;
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t2537*)((CustomAttributeTypedArgumentU5BU5D_t2537*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_7));
		CustomAttributeTypedArgumentU5BU5D_t2537** L_8 = ___array;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_9 = V_0;
		int32_t L_10 = ___newSize;
		int32_t L_11 = ___length;
		int32_t L_12 = Math_Min_m10255(NULL /*static, unused*/, (int32_t)L_10, (int32_t)L_11, /*hidden argument*/NULL);
		Array_Copy_m286(NULL /*static, unused*/, (Array_t *)(Array_t *)(*((CustomAttributeTypedArgumentU5BU5D_t2537**)L_8)), (Array_t *)(Array_t *)L_9, (int32_t)L_12, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2537** L_13 = ___array;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_14 = V_0;
		*((Object_t **)(L_13)) = (Object_t *)L_14;
		return;
	}
}
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T,System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31258_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, CustomAttributeTypedArgument_t2101  ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	EqualityComparer_1_t3548 * V_1 = {0};
	int32_t V_2 = 0;
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = ___startIndex;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_4 = ___array;
		NullCheck((Array_t *)L_4);
		int32_t L_5 = Array_GetLowerBound_m10928((Array_t *)L_4, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetUpperBound_m10940((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		int32_t L_9 = ___count;
		if ((((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))) <= ((int32_t)((int32_t)((int32_t)L_8-(int32_t)L_9)))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		ArgumentOutOfRangeException_t1665 * L_10 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9264(L_10, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_10);
	}

IL_003c:
	{
		int32_t L_11 = ___startIndex;
		int32_t L_12 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)L_12));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		EqualityComparer_1_t3548 * L_13 = (( EqualityComparer_1_t3548 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (EqualityComparer_1_t3548 *)L_13;
		int32_t L_14 = ___startIndex;
		V_2 = (int32_t)L_14;
		goto IL_0066;
	}

IL_004d:
	{
		EqualityComparer_1_t3548 * L_15 = V_1;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_16 = ___array;
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		CustomAttributeTypedArgument_t2101  L_19 = ___value;
		NullCheck((EqualityComparer_1_t3548 *)L_15);
		bool L_20 = (bool)VirtFuncInvoker2< bool, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T) */, (EqualityComparer_1_t3548 *)L_15, (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_16, L_18, sizeof(CustomAttributeTypedArgument_t2101 ))), (CustomAttributeTypedArgument_t2101 )L_19);
		if (!L_20)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_0062:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_004d;
		}
	}
	{
		return (-1);
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_m31259_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_2 = ___array;
		int32_t L_3 = ___index;
		int32_t L_4 = ___length;
		Object_t* L_5 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_2, (CustomAttributeTypedArgumentU5BU5D_t2537*)(CustomAttributeTypedArgumentU5BU5D_t2537*)NULL, (int32_t)L_3, (int32_t)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t2512_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t335_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t493_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t532_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2160;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2034;
extern Il2CppCodeGenString* _stringLiteral2161;
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31260_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___keys, CustomAttributeTypedArgumentU5BU5D_t2537* ___items, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		DoubleU5BU5D_t2512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		Int32U5BU5D_t335_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(339);
		CharU5BU5D_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(66);
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		InvalidOperationException_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(242);
		_stringLiteral2160 = il2cpp_codegen_string_literal_from_index(2160);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2034 = il2cpp_codegen_string_literal_from_index(2034);
		_stringLiteral2161 = il2cpp_codegen_string_literal_from_index(2161);
		s_Il2CppMethodIntialized = true;
	}
	Swapper_t1855 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Exception_t40 * V_3 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___keys;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral2160, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_3 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_3, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___length;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_5 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_5, (String_t*)_stringLiteral2034, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0035:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_6 = ___keys;
		NullCheck(L_6);
		int32_t L_7 = ___index;
		int32_t L_8 = ___length;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))-(int32_t)L_7))) < ((int32_t)L_8)))
		{
			goto IL_0051;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_9 = ___items;
		if (!L_9)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_10 = ___index;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_11 = ___items;
		NullCheck(L_11);
		int32_t L_12 = ___length;
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length))))-(int32_t)L_12)))))
		{
			goto IL_0057;
		}
	}

IL_0051:
	{
		ArgumentException_t513 * L_13 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m14587(L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0057:
	{
		int32_t L_14 = ___length;
		if ((((int32_t)L_14) > ((int32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		return;
	}

IL_005f:
	{
		Object_t* L_15 = ___comparer;
		if (L_15)
		{
			goto IL_00d7;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_16 = ___items;
		if (L_16)
		{
			goto IL_0073;
		}
	}
	{
		V_0 = (Swapper_t1855 *)NULL;
		goto IL_007a;
	}

IL_0073:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_17 = ___items;
		Swapper_t1855 * L_18 = (( Swapper_t1855 * (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Swapper_t1855 *)L_18;
	}

IL_007a:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_19 = ___keys;
		if (!((DoubleU5BU5D_t2512*)IsInst(L_19, DoubleU5BU5D_t2512_il2cpp_TypeInfo_var)))
		{
			goto IL_0099;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_20 = ___keys;
		int32_t L_21 = ___index;
		int32_t L_22 = ___length;
		Swapper_t1855 * L_23 = V_0;
		Array_combsort_m10991(NULL /*static, unused*/, (DoubleU5BU5D_t2512*)((DoubleU5BU5D_t2512*)IsInst(L_20, DoubleU5BU5D_t2512_il2cpp_TypeInfo_var)), (int32_t)L_21, (int32_t)L_22, (Swapper_t1855 *)L_23, /*hidden argument*/NULL);
		return;
	}

IL_0099:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_24 = ___keys;
		if (!((Int32U5BU5D_t335*)IsInst(L_24, Int32U5BU5D_t335_il2cpp_TypeInfo_var)))
		{
			goto IL_00b8;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_25 = ___keys;
		int32_t L_26 = ___index;
		int32_t L_27 = ___length;
		Swapper_t1855 * L_28 = V_0;
		Array_combsort_m10992(NULL /*static, unused*/, (Int32U5BU5D_t335*)((Int32U5BU5D_t335*)IsInst(L_25, Int32U5BU5D_t335_il2cpp_TypeInfo_var)), (int32_t)L_26, (int32_t)L_27, (Swapper_t1855 *)L_28, /*hidden argument*/NULL);
		return;
	}

IL_00b8:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_29 = ___keys;
		if (!((CharU5BU5D_t493*)IsInst(L_29, CharU5BU5D_t493_il2cpp_TypeInfo_var)))
		{
			goto IL_00d7;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_30 = ___keys;
		int32_t L_31 = ___index;
		int32_t L_32 = ___length;
		Swapper_t1855 * L_33 = V_0;
		Array_combsort_m10993(NULL /*static, unused*/, (CharU5BU5D_t493*)((CharU5BU5D_t493*)IsInst(L_30, CharU5BU5D_t493_il2cpp_TypeInfo_var)), (int32_t)L_31, (int32_t)L_32, (Swapper_t1855 *)L_33, /*hidden argument*/NULL);
		return;
	}

IL_00d7:
	try
	{ // begin try (depth: 1)
		int32_t L_34 = ___index;
		V_1 = (int32_t)L_34;
		int32_t L_35 = ___index;
		int32_t L_36 = ___length;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35+(int32_t)L_36))-(int32_t)1));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_37 = ___keys;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_38 = ___items;
		int32_t L_39 = V_1;
		int32_t L_40 = V_2;
		Object_t* L_41 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_37, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_38, (int32_t)L_39, (int32_t)L_40, (Object_t*)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		goto IL_0106;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t40 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t40_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ef;
		throw e;
	}

CATCH_00ef:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t40 *)((Exception_t40 *)__exception_local);
			String_t* L_42 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2161, /*hidden argument*/NULL);
			Exception_t40 * L_43 = V_3;
			InvalidOperationException_t532 * L_44 = (InvalidOperationException_t532 *)il2cpp_codegen_object_new (InvalidOperationException_t532_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m15017(L_44, (String_t*)L_42, (Exception_t40 *)L_43, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_44);
		}

IL_0101:
		{
			goto IL_0106;
		}
	} // end catch (depth: 1)

IL_0106:
	{
		return;
	}
}
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeTypedArgument>(T[])
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeTypedArgument>(T[])
extern TypeInfo* Int32U5BU5D_t335_il2cpp_TypeInfo_var;
extern TypeInfo* Swapper_t1855_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t2512_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_int_swapper_m10986_MethodInfo_var;
extern const MethodInfo* Array_double_swapper_m10989_MethodInfo_var;
extern const MethodInfo* Array_slow_swapper_m10988_MethodInfo_var;
extern "C" Swapper_t1855 * Array_get_swapper_TisCustomAttributeTypedArgument_t2101_m31261_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t335_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(339);
		Swapper_t1855_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1281);
		DoubleU5BU5D_t2512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		Array_int_swapper_m10986_MethodInfo_var = il2cpp_codegen_method_info_from_index(1100);
		Array_double_swapper_m10989_MethodInfo_var = il2cpp_codegen_method_info_from_index(1101);
		Array_slow_swapper_m10988_MethodInfo_var = il2cpp_codegen_method_info_from_index(1103);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___array;
		if (!((Int32U5BU5D_t335*)IsInst(L_0, Int32U5BU5D_t335_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_1 = ___array;
		IntPtr_t L_2 = { (void*)Array_int_swapper_m10986_MethodInfo_var };
		Swapper_t1855 * L_3 = (Swapper_t1855 *)il2cpp_codegen_object_new (Swapper_t1855_il2cpp_TypeInfo_var);
		Swapper__ctor_m10906(L_3, (Object_t *)(Object_t *)L_1, (IntPtr_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_4 = ___array;
		if (!((DoubleU5BU5D_t2512*)IsInst(L_4, DoubleU5BU5D_t2512_il2cpp_TypeInfo_var)))
		{
			goto IL_0030;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_5 = ___array;
		IntPtr_t L_6 = { (void*)Array_double_swapper_m10989_MethodInfo_var };
		Swapper_t1855 * L_7 = (Swapper_t1855 *)il2cpp_codegen_object_new (Swapper_t1855_il2cpp_TypeInfo_var);
		Swapper__ctor_m10906(L_7, (Object_t *)(Object_t *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_8 = ___array;
		IntPtr_t L_9 = { (void*)Array_slow_swapper_m10988_MethodInfo_var };
		Swapper_t1855 * L_10 = (Swapper_t1855 *)il2cpp_codegen_object_new (Swapper_t1855_il2cpp_TypeInfo_var);
		Swapper__ctor_m10906(L_10, (Object_t *)(Object_t *)L_8, (IntPtr_t)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31262_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___keys, CustomAttributeTypedArgumentU5BU5D_t2537* ___items, int32_t ___low0, int32_t ___high0, Object_t* ___comparer, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CustomAttributeTypedArgument_t2101  V_3 = {0};
	{
		int32_t L_0 = ___low0;
		int32_t L_1 = ___high0;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_2 = ___low0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___high0;
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))/(int32_t)2))));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_7 = ___keys;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_7, L_9, sizeof(CustomAttributeTypedArgument_t2101 )));
	}

IL_001c:
	{
		goto IL_0025;
	}

IL_0021:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___high0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0041;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_13 = ___keys;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		CustomAttributeTypedArgument_t2101  L_16 = V_3;
		Object_t* L_17 = ___comparer;
		int32_t L_18 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_13, L_15, sizeof(CustomAttributeTypedArgument_t2101 ))), (CustomAttributeTypedArgument_t2101 )L_16, (Object_t*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_0041:
	{
		goto IL_004a;
	}

IL_0046:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_004a:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___low0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_0066;
		}
	}
	{
		CustomAttributeTypedArgument_t2101  L_22 = V_3;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_23 = ___keys;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		Object_t* L_26 = ___comparer;
		int32_t L_27 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgument_t2101 )L_22, (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_23, L_25, sizeof(CustomAttributeTypedArgument_t2101 ))), (Object_t*)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}

IL_0066:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) > ((int32_t)L_29)))
		{
			goto IL_0083;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_30 = ___keys;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_31 = ___items;
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_30, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_31, (int32_t)L_32, (int32_t)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_34 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_35-(int32_t)1));
		goto IL_0088;
	}

IL_0083:
	{
		goto IL_008d;
	}

IL_0088:
	{
		goto IL_001c;
	}

IL_008d:
	{
		int32_t L_36 = ___low0;
		int32_t L_37 = V_1;
		if ((((int32_t)L_36) >= ((int32_t)L_37)))
		{
			goto IL_009f;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_38 = ___keys;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_39 = ___items;
		int32_t L_40 = ___low0;
		int32_t L_41 = V_1;
		Object_t* L_42 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_38, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_39, (int32_t)L_40, (int32_t)L_41, (Object_t*)L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_009f:
	{
		int32_t L_43 = V_0;
		int32_t L_44 = ___high0;
		if ((((int32_t)L_43) >= ((int32_t)L_44)))
		{
			goto IL_00b1;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_45 = ___keys;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_46 = ___items;
		int32_t L_47 = V_0;
		int32_t L_48 = ___high0;
		Object_t* L_49 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_45, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_46, (int32_t)L_47, (int32_t)L_48, (Object_t*)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_00b1:
	{
		return;
	}
}
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeTypedArgument>(T,T,System.Collections.Generic.IComparer`1<T>)
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeTypedArgument>(T,T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* IComparable_t2534_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t532_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3772;
extern "C" int32_t Array_compare_TisCustomAttributeTypedArgument_t2101_m31263_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgument_t2101  ___value1, CustomAttributeTypedArgument_t2101  ___value2, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IComparable_t2534_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1278);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		InvalidOperationException_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(242);
		_stringLiteral3772 = il2cpp_codegen_string_literal_from_index(3772);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		Object_t* L_0 = ___comparer;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Object_t* L_1 = ___comparer;
		CustomAttributeTypedArgument_t2101  L_2 = ___value1;
		CustomAttributeTypedArgument_t2101  L_3 = ___value2;
		NullCheck((Object_t*)L_1);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101  >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Object_t*)L_1, (CustomAttributeTypedArgument_t2101 )L_2, (CustomAttributeTypedArgument_t2101 )L_3);
		return L_4;
	}

IL_000f:
	{
		CustomAttributeTypedArgument_t2101  L_5 = ___value1;
		goto IL_002d;
	}
	{
		CustomAttributeTypedArgument_t2101  L_6 = ___value2;
		goto IL_002b;
	}
	{
		G_B6_0 = 0;
		goto IL_002c;
	}

IL_002b:
	{
		G_B6_0 = (-1);
	}

IL_002c:
	{
		return G_B6_0;
	}

IL_002d:
	{
		CustomAttributeTypedArgument_t2101  L_7 = ___value2;
		goto IL_003a;
	}
	{
		return 1;
	}

IL_003a:
	{
		CustomAttributeTypedArgument_t2101  L_8 = ___value1;
		CustomAttributeTypedArgument_t2101  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_9);
		if (!((Object_t*)IsInst(L_10, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))
		{
			goto IL_005c;
		}
	}
	{
		CustomAttributeTypedArgument_t2101  L_11 = ___value1;
		CustomAttributeTypedArgument_t2101  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_12);
		CustomAttributeTypedArgument_t2101  L_14 = ___value2;
		NullCheck((Object_t*)((Object_t*)Castclass(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
		int32_t L_15 = (int32_t)InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t2101  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeTypedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Object_t*)((Object_t*)Castclass(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))), (CustomAttributeTypedArgument_t2101 )L_14);
		return L_15;
	}

IL_005c:
	{
		CustomAttributeTypedArgument_t2101  L_16 = ___value1;
		CustomAttributeTypedArgument_t2101  L_17 = L_16;
		Object_t * L_18 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_17);
		if (!((Object_t *)IsInst(L_18, IComparable_t2534_il2cpp_TypeInfo_var)))
		{
			goto IL_0083;
		}
	}
	{
		CustomAttributeTypedArgument_t2101  L_19 = ___value1;
		CustomAttributeTypedArgument_t2101  L_20 = L_19;
		Object_t * L_21 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_20);
		CustomAttributeTypedArgument_t2101  L_22 = ___value2;
		CustomAttributeTypedArgument_t2101  L_23 = L_22;
		Object_t * L_24 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_23);
		NullCheck((Object_t *)((Object_t *)Castclass(L_21, IComparable_t2534_il2cpp_TypeInfo_var)));
		int32_t L_25 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t2534_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_21, IComparable_t2534_il2cpp_TypeInfo_var)), (Object_t *)L_24);
		return L_25;
	}

IL_0083:
	{
		String_t* L_26 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral3772, /*hidden argument*/NULL);
		V_0 = (String_t*)L_26;
		String_t* L_27 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, (RuntimeTypeHandle_t1857 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 3)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m2404(NULL /*static, unused*/, (String_t*)L_27, (Object_t *)L_28, /*hidden argument*/NULL);
		InvalidOperationException_t532 * L_30 = (InvalidOperationException_t532 *)il2cpp_codegen_object_new (InvalidOperationException_t532_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2398(L_30, (String_t*)L_29, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_30);
	}
}
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(K[],V[],System.Int32,System.Int32)
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(K[],V[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t2101_TisCustomAttributeTypedArgument_t2101_m31264_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___keys, CustomAttributeTypedArgumentU5BU5D_t2537* ___items, int32_t ___i, int32_t ___j, const MethodInfo* method)
{
	CustomAttributeTypedArgument_t2101  V_0 = {0};
	CustomAttributeTypedArgument_t2101  V_1 = {0};
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___keys;
		int32_t L_1 = ___i;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_0, L_2, sizeof(CustomAttributeTypedArgument_t2101 )));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_3 = ___keys;
		int32_t L_4 = ___i;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_5 = ___keys;
		int32_t L_6 = ___j;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_3, L_4, sizeof(CustomAttributeTypedArgument_t2101 ))) = (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_5, L_7, sizeof(CustomAttributeTypedArgument_t2101 )));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_8 = ___keys;
		int32_t L_9 = ___j;
		CustomAttributeTypedArgument_t2101  L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_8, L_9, sizeof(CustomAttributeTypedArgument_t2101 ))) = (CustomAttributeTypedArgument_t2101 )L_10;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_11 = ___items;
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_12 = ___items;
		int32_t L_13 = ___i;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_1 = (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_12, L_14, sizeof(CustomAttributeTypedArgument_t2101 )));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_15 = ___items;
		int32_t L_16 = ___i;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_17 = ___items;
		int32_t L_18 = ___j;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		*((CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_15, L_16, sizeof(CustomAttributeTypedArgument_t2101 ))) = (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_17, L_19, sizeof(CustomAttributeTypedArgument_t2101 )));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_20 = ___items;
		int32_t L_21 = ___j;
		CustomAttributeTypedArgument_t2101  L_22 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		*((CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_20, L_21, sizeof(CustomAttributeTypedArgument_t2101 ))) = (CustomAttributeTypedArgument_t2101 )L_22;
	}

IL_0042:
	{
		return;
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Comparison`1<T>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Comparison`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t532_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3773;
extern Il2CppCodeGenString* _stringLiteral3774;
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t2101_m31265_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___length, Comparison_1_t3553 * ___comparison, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		InvalidOperationException_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(242);
		_stringLiteral3773 = il2cpp_codegen_string_literal_from_index(3773);
		_stringLiteral3774 = il2cpp_codegen_string_literal_from_index(3774);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Exception_t40 * V_2 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Comparison_1_t3553 * L_0 = ___comparison;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral3773, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___length;
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_3 = ___array;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) > ((int32_t)1)))
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		V_0 = (int32_t)0;
		int32_t L_4 = ___length;
		V_1 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_5 = ___array;
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		Comparison_1_t3553 * L_8 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Comparison_1_t3553 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_5, (int32_t)L_6, (int32_t)L_7, (Comparison_1_t3553 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t40 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t40_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0036;
		throw e;
	}

CATCH_0036:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t40 *)((Exception_t40 *)__exception_local);
			String_t* L_9 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral3774, /*hidden argument*/NULL);
			Exception_t40 * L_10 = V_2;
			InvalidOperationException_t532 * L_11 = (InvalidOperationException_t532 *)il2cpp_codegen_object_new (InvalidOperationException_t532_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m15017(L_11, (String_t*)L_9, (Exception_t40 *)L_10, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
		}

IL_0048:
		{
			goto IL_004d;
		}
	} // end catch (depth: 1)

IL_004d:
	{
		return;
	}
}
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t2101_m31266_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___low0, int32_t ___high0, Comparison_1_t3553 * ___comparison, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CustomAttributeTypedArgument_t2101  V_3 = {0};
	{
		int32_t L_0 = ___low0;
		int32_t L_1 = ___high0;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_2 = ___low0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___high0;
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))/(int32_t)2))));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_7 = ___array;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_7, L_9, sizeof(CustomAttributeTypedArgument_t2101 )));
	}

IL_001c:
	{
		goto IL_0025;
	}

IL_0021:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___high0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0040;
		}
	}
	{
		Comparison_1_t3553 * L_13 = ___comparison;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_14 = ___array;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		CustomAttributeTypedArgument_t2101  L_17 = V_3;
		NullCheck((Comparison_1_t3553 *)L_13);
		int32_t L_18 = (( int32_t (*) (Comparison_1_t3553 *, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Comparison_1_t3553 *)L_13, (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_14, L_16, sizeof(CustomAttributeTypedArgument_t2101 ))), (CustomAttributeTypedArgument_t2101 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_0040:
	{
		goto IL_0049;
	}

IL_0045:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_0049:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___low0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_0064;
		}
	}
	{
		Comparison_1_t3553 * L_22 = ___comparison;
		CustomAttributeTypedArgument_t2101  L_23 = V_3;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_24 = ___array;
		int32_t L_25 = V_1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		NullCheck((Comparison_1_t3553 *)L_22);
		int32_t L_27 = (( int32_t (*) (Comparison_1_t3553 *, CustomAttributeTypedArgument_t2101 , CustomAttributeTypedArgument_t2101 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Comparison_1_t3553 *)L_22, (CustomAttributeTypedArgument_t2101 )L_23, (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_24, L_26, sizeof(CustomAttributeTypedArgument_t2101 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}

IL_0064:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) > ((int32_t)L_29)))
		{
			goto IL_0080;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_30 = ___array;
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_30, (int32_t)L_31, (int32_t)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_33 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		int32_t L_34 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_34-(int32_t)1));
		goto IL_0085;
	}

IL_0080:
	{
		goto IL_008a;
	}

IL_0085:
	{
		goto IL_001c;
	}

IL_008a:
	{
		int32_t L_35 = ___low0;
		int32_t L_36 = V_1;
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_009a;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_37 = ___array;
		int32_t L_38 = ___low0;
		int32_t L_39 = V_1;
		Comparison_1_t3553 * L_40 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Comparison_1_t3553 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_37, (int32_t)L_38, (int32_t)L_39, (Comparison_1_t3553 *)L_40, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_009a:
	{
		int32_t L_41 = V_0;
		int32_t L_42 = ___high0;
		if ((((int32_t)L_41) >= ((int32_t)L_42)))
		{
			goto IL_00aa;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_43 = ___array;
		int32_t L_44 = V_0;
		int32_t L_45 = ___high0;
		Comparison_1_t3553 * L_46 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, int32_t, int32_t, Comparison_1_t3553 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_43, (int32_t)L_44, (int32_t)L_45, (Comparison_1_t3553 *)L_46, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_00aa:
	{
		return;
	}
}
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32)
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t2101_m31267_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, int32_t ___i, int32_t ___j, const MethodInfo* method)
{
	CustomAttributeTypedArgument_t2101  V_0 = {0};
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___array;
		int32_t L_1 = ___i;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_0, L_2, sizeof(CustomAttributeTypedArgument_t2101 )));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_3 = ___array;
		int32_t L_4 = ___i;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_5 = ___array;
		int32_t L_6 = ___j;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_3, L_4, sizeof(CustomAttributeTypedArgument_t2101 ))) = (CustomAttributeTypedArgument_t2101 )(*(CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_5, L_7, sizeof(CustomAttributeTypedArgument_t2101 )));
		CustomAttributeTypedArgumentU5BU5D_t2537* L_8 = ___array;
		int32_t L_9 = ___j;
		CustomAttributeTypedArgument_t2101  L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((CustomAttributeTypedArgument_t2101 *)(CustomAttributeTypedArgument_t2101 *)SZArrayLdElema(L_8, L_9, sizeof(CustomAttributeTypedArgument_t2101 ))) = (CustomAttributeTypedArgument_t2101 )L_10;
		return;
	}
}
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t2101_m31268_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537* ___array, CustomAttributeTypedArgument_t2101  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeTypedArgumentU5BU5D_t2537* L_2 = ___array;
		CustomAttributeTypedArgument_t2101  L_3 = ___value;
		CustomAttributeTypedArgumentU5BU5D_t2537* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2537*, CustomAttributeTypedArgument_t2101 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2537*)L_2, (CustomAttributeTypedArgument_t2101 )L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C" CustomAttributeNamedArgumentU5BU5D_t2538* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t2100_m15431_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___values, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t2538* V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t2538*)((CustomAttributeNamedArgumentU5BU5D_t2538*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t34* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		*((CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_1, L_2, sizeof(CustomAttributeNamedArgument_t2100 ))) = (CustomAttributeNamedArgument_t2100 )((*(CustomAttributeNamedArgument_t2100 *)((CustomAttributeNamedArgument_t2100 *)UnBox ((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t34* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_9 = V_0;
		return L_9;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeNamedArgument>(T[])
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeNamedArgument>(T[])
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" ReadOnlyCollection_1_t2540 * Array_AsReadOnly_TisCustomAttributeNamedArgument_t2100_m15432_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_2 = ___array;
		ArrayReadOnlyList_1_t3565 * L_3 = (ArrayReadOnlyList_1_t3565 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		(( void (*) (ArrayReadOnlyList_1_t3565 *, CustomAttributeNamedArgumentU5BU5D_t2538*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_3, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		ReadOnlyCollection_1_t2540 * L_4 = (ReadOnlyCollection_1_t2540 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		(( void (*) (ReadOnlyCollection_1_t2540 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_4, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_4;
	}
}
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t2100_m31269_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538** ___array, int32_t ___newSize, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t2538** G_B2_0 = {0};
	CustomAttributeNamedArgumentU5BU5D_t2538** G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	CustomAttributeNamedArgumentU5BU5D_t2538** G_B3_1 = {0};
	{
		CustomAttributeNamedArgumentU5BU5D_t2538** L_0 = ___array;
		CustomAttributeNamedArgumentU5BU5D_t2538** L_1 = ___array;
		G_B1_0 = L_0;
		if ((*((CustomAttributeNamedArgumentU5BU5D_t2538**)L_1)))
		{
			G_B2_0 = L_0;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_000e:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538** L_2 = ___array;
		NullCheck((*((CustomAttributeNamedArgumentU5BU5D_t2538**)L_2)));
		G_B3_0 = (((int32_t)((int32_t)(((Array_t *)(*((CustomAttributeNamedArgumentU5BU5D_t2538**)L_2)))->max_length))));
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		int32_t L_3 = ___newSize;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538**, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538**)G_B3_1, (int32_t)G_B3_0, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32,System.Int32)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t2100_m31270_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538** ___array, int32_t ___length, int32_t ___newSize, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeNamedArgumentU5BU5D_t2538* V_0 = {0};
	{
		int32_t L_0 = ___newSize;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_1 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9264(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_000d:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538** L_2 = ___array;
		if ((*((CustomAttributeNamedArgumentU5BU5D_t2538**)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538** L_3 = ___array;
		int32_t L_4 = ___newSize;
		*((Object_t **)(L_3)) = (Object_t *)((CustomAttributeNamedArgumentU5BU5D_t2538*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_4));
		return;
	}

IL_001d:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538** L_5 = ___array;
		NullCheck((*((CustomAttributeNamedArgumentU5BU5D_t2538**)L_5)));
		int32_t L_6 = ___newSize;
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)(*((CustomAttributeNamedArgumentU5BU5D_t2538**)L_5)))->max_length))))) == ((uint32_t)L_6))))
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		int32_t L_7 = ___newSize;
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t2538*)((CustomAttributeNamedArgumentU5BU5D_t2538*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_7));
		CustomAttributeNamedArgumentU5BU5D_t2538** L_8 = ___array;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_9 = V_0;
		int32_t L_10 = ___newSize;
		int32_t L_11 = ___length;
		int32_t L_12 = Math_Min_m10255(NULL /*static, unused*/, (int32_t)L_10, (int32_t)L_11, /*hidden argument*/NULL);
		Array_Copy_m286(NULL /*static, unused*/, (Array_t *)(Array_t *)(*((CustomAttributeNamedArgumentU5BU5D_t2538**)L_8)), (Array_t *)(Array_t *)L_9, (int32_t)L_12, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t2538** L_13 = ___array;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_14 = V_0;
		*((Object_t **)(L_13)) = (Object_t *)L_14;
		return;
	}
}
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T,System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31271_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, CustomAttributeNamedArgument_t2100  ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	EqualityComparer_1_t3559 * V_1 = {0};
	int32_t V_2 = 0;
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = ___startIndex;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_4 = ___array;
		NullCheck((Array_t *)L_4);
		int32_t L_5 = Array_GetLowerBound_m10928((Array_t *)L_4, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetUpperBound_m10940((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		int32_t L_9 = ___count;
		if ((((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))) <= ((int32_t)((int32_t)((int32_t)L_8-(int32_t)L_9)))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		ArgumentOutOfRangeException_t1665 * L_10 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9264(L_10, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_10);
	}

IL_003c:
	{
		int32_t L_11 = ___startIndex;
		int32_t L_12 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)L_12));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		EqualityComparer_1_t3559 * L_13 = (( EqualityComparer_1_t3559 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (EqualityComparer_1_t3559 *)L_13;
		int32_t L_14 = ___startIndex;
		V_2 = (int32_t)L_14;
		goto IL_0066;
	}

IL_004d:
	{
		EqualityComparer_1_t3559 * L_15 = V_1;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_16 = ___array;
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		CustomAttributeNamedArgument_t2100  L_19 = ___value;
		NullCheck((EqualityComparer_1_t3559 *)L_15);
		bool L_20 = (bool)VirtFuncInvoker2< bool, CustomAttributeNamedArgument_t2100 , CustomAttributeNamedArgument_t2100  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::Equals(T,T) */, (EqualityComparer_1_t3559 *)L_15, (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_16, L_18, sizeof(CustomAttributeNamedArgument_t2100 ))), (CustomAttributeNamedArgument_t2100 )L_19);
		if (!L_20)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_0062:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_004d;
		}
	}
	{
		return (-1);
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_m31272_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_2 = ___array;
		int32_t L_3 = ___index;
		int32_t L_4 = ___length;
		Object_t* L_5 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_2, (CustomAttributeNamedArgumentU5BU5D_t2538*)(CustomAttributeNamedArgumentU5BU5D_t2538*)NULL, (int32_t)L_3, (int32_t)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t2512_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t335_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t493_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t532_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2160;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2034;
extern Il2CppCodeGenString* _stringLiteral2161;
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31273_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___keys, CustomAttributeNamedArgumentU5BU5D_t2538* ___items, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		DoubleU5BU5D_t2512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		Int32U5BU5D_t335_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(339);
		CharU5BU5D_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(66);
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		InvalidOperationException_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(242);
		_stringLiteral2160 = il2cpp_codegen_string_literal_from_index(2160);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2034 = il2cpp_codegen_string_literal_from_index(2034);
		_stringLiteral2161 = il2cpp_codegen_string_literal_from_index(2161);
		s_Il2CppMethodIntialized = true;
	}
	Swapper_t1855 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Exception_t40 * V_3 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___keys;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral2160, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_3 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_3, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___length;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_5 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_5, (String_t*)_stringLiteral2034, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0035:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_6 = ___keys;
		NullCheck(L_6);
		int32_t L_7 = ___index;
		int32_t L_8 = ___length;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))-(int32_t)L_7))) < ((int32_t)L_8)))
		{
			goto IL_0051;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_9 = ___items;
		if (!L_9)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_10 = ___index;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_11 = ___items;
		NullCheck(L_11);
		int32_t L_12 = ___length;
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length))))-(int32_t)L_12)))))
		{
			goto IL_0057;
		}
	}

IL_0051:
	{
		ArgumentException_t513 * L_13 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m14587(L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0057:
	{
		int32_t L_14 = ___length;
		if ((((int32_t)L_14) > ((int32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		return;
	}

IL_005f:
	{
		Object_t* L_15 = ___comparer;
		if (L_15)
		{
			goto IL_00d7;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_16 = ___items;
		if (L_16)
		{
			goto IL_0073;
		}
	}
	{
		V_0 = (Swapper_t1855 *)NULL;
		goto IL_007a;
	}

IL_0073:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_17 = ___items;
		Swapper_t1855 * L_18 = (( Swapper_t1855 * (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Swapper_t1855 *)L_18;
	}

IL_007a:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_19 = ___keys;
		if (!((DoubleU5BU5D_t2512*)IsInst(L_19, DoubleU5BU5D_t2512_il2cpp_TypeInfo_var)))
		{
			goto IL_0099;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_20 = ___keys;
		int32_t L_21 = ___index;
		int32_t L_22 = ___length;
		Swapper_t1855 * L_23 = V_0;
		Array_combsort_m10991(NULL /*static, unused*/, (DoubleU5BU5D_t2512*)((DoubleU5BU5D_t2512*)IsInst(L_20, DoubleU5BU5D_t2512_il2cpp_TypeInfo_var)), (int32_t)L_21, (int32_t)L_22, (Swapper_t1855 *)L_23, /*hidden argument*/NULL);
		return;
	}

IL_0099:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_24 = ___keys;
		if (!((Int32U5BU5D_t335*)IsInst(L_24, Int32U5BU5D_t335_il2cpp_TypeInfo_var)))
		{
			goto IL_00b8;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_25 = ___keys;
		int32_t L_26 = ___index;
		int32_t L_27 = ___length;
		Swapper_t1855 * L_28 = V_0;
		Array_combsort_m10992(NULL /*static, unused*/, (Int32U5BU5D_t335*)((Int32U5BU5D_t335*)IsInst(L_25, Int32U5BU5D_t335_il2cpp_TypeInfo_var)), (int32_t)L_26, (int32_t)L_27, (Swapper_t1855 *)L_28, /*hidden argument*/NULL);
		return;
	}

IL_00b8:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_29 = ___keys;
		if (!((CharU5BU5D_t493*)IsInst(L_29, CharU5BU5D_t493_il2cpp_TypeInfo_var)))
		{
			goto IL_00d7;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_30 = ___keys;
		int32_t L_31 = ___index;
		int32_t L_32 = ___length;
		Swapper_t1855 * L_33 = V_0;
		Array_combsort_m10993(NULL /*static, unused*/, (CharU5BU5D_t493*)((CharU5BU5D_t493*)IsInst(L_30, CharU5BU5D_t493_il2cpp_TypeInfo_var)), (int32_t)L_31, (int32_t)L_32, (Swapper_t1855 *)L_33, /*hidden argument*/NULL);
		return;
	}

IL_00d7:
	try
	{ // begin try (depth: 1)
		int32_t L_34 = ___index;
		V_1 = (int32_t)L_34;
		int32_t L_35 = ___index;
		int32_t L_36 = ___length;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35+(int32_t)L_36))-(int32_t)1));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_37 = ___keys;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_38 = ___items;
		int32_t L_39 = V_1;
		int32_t L_40 = V_2;
		Object_t* L_41 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_37, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_38, (int32_t)L_39, (int32_t)L_40, (Object_t*)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		goto IL_0106;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t40 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t40_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ef;
		throw e;
	}

CATCH_00ef:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t40 *)((Exception_t40 *)__exception_local);
			String_t* L_42 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2161, /*hidden argument*/NULL);
			Exception_t40 * L_43 = V_3;
			InvalidOperationException_t532 * L_44 = (InvalidOperationException_t532 *)il2cpp_codegen_object_new (InvalidOperationException_t532_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m15017(L_44, (String_t*)L_42, (Exception_t40 *)L_43, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_44);
		}

IL_0101:
		{
			goto IL_0106;
		}
	} // end catch (depth: 1)

IL_0106:
	{
		return;
	}
}
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeNamedArgument>(T[])
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeNamedArgument>(T[])
extern TypeInfo* Int32U5BU5D_t335_il2cpp_TypeInfo_var;
extern TypeInfo* Swapper_t1855_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t2512_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_int_swapper_m10986_MethodInfo_var;
extern const MethodInfo* Array_double_swapper_m10989_MethodInfo_var;
extern const MethodInfo* Array_slow_swapper_m10988_MethodInfo_var;
extern "C" Swapper_t1855 * Array_get_swapper_TisCustomAttributeNamedArgument_t2100_m31274_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t335_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(339);
		Swapper_t1855_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1281);
		DoubleU5BU5D_t2512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		Array_int_swapper_m10986_MethodInfo_var = il2cpp_codegen_method_info_from_index(1100);
		Array_double_swapper_m10989_MethodInfo_var = il2cpp_codegen_method_info_from_index(1101);
		Array_slow_swapper_m10988_MethodInfo_var = il2cpp_codegen_method_info_from_index(1103);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___array;
		if (!((Int32U5BU5D_t335*)IsInst(L_0, Int32U5BU5D_t335_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_1 = ___array;
		IntPtr_t L_2 = { (void*)Array_int_swapper_m10986_MethodInfo_var };
		Swapper_t1855 * L_3 = (Swapper_t1855 *)il2cpp_codegen_object_new (Swapper_t1855_il2cpp_TypeInfo_var);
		Swapper__ctor_m10906(L_3, (Object_t *)(Object_t *)L_1, (IntPtr_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_4 = ___array;
		if (!((DoubleU5BU5D_t2512*)IsInst(L_4, DoubleU5BU5D_t2512_il2cpp_TypeInfo_var)))
		{
			goto IL_0030;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_5 = ___array;
		IntPtr_t L_6 = { (void*)Array_double_swapper_m10989_MethodInfo_var };
		Swapper_t1855 * L_7 = (Swapper_t1855 *)il2cpp_codegen_object_new (Swapper_t1855_il2cpp_TypeInfo_var);
		Swapper__ctor_m10906(L_7, (Object_t *)(Object_t *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_8 = ___array;
		IntPtr_t L_9 = { (void*)Array_slow_swapper_m10988_MethodInfo_var };
		Swapper_t1855 * L_10 = (Swapper_t1855 *)il2cpp_codegen_object_new (Swapper_t1855_il2cpp_TypeInfo_var);
		Swapper__ctor_m10906(L_10, (Object_t *)(Object_t *)L_8, (IntPtr_t)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31275_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___keys, CustomAttributeNamedArgumentU5BU5D_t2538* ___items, int32_t ___low0, int32_t ___high0, Object_t* ___comparer, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CustomAttributeNamedArgument_t2100  V_3 = {0};
	{
		int32_t L_0 = ___low0;
		int32_t L_1 = ___high0;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_2 = ___low0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___high0;
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))/(int32_t)2))));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_7 = ___keys;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_7, L_9, sizeof(CustomAttributeNamedArgument_t2100 )));
	}

IL_001c:
	{
		goto IL_0025;
	}

IL_0021:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___high0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0041;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_13 = ___keys;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		CustomAttributeNamedArgument_t2100  L_16 = V_3;
		Object_t* L_17 = ___comparer;
		int32_t L_18 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgument_t2100 , CustomAttributeNamedArgument_t2100 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_13, L_15, sizeof(CustomAttributeNamedArgument_t2100 ))), (CustomAttributeNamedArgument_t2100 )L_16, (Object_t*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_0041:
	{
		goto IL_004a;
	}

IL_0046:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_004a:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___low0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_0066;
		}
	}
	{
		CustomAttributeNamedArgument_t2100  L_22 = V_3;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_23 = ___keys;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		Object_t* L_26 = ___comparer;
		int32_t L_27 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgument_t2100 , CustomAttributeNamedArgument_t2100 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgument_t2100 )L_22, (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_23, L_25, sizeof(CustomAttributeNamedArgument_t2100 ))), (Object_t*)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}

IL_0066:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) > ((int32_t)L_29)))
		{
			goto IL_0083;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_30 = ___keys;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_31 = ___items;
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_30, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_31, (int32_t)L_32, (int32_t)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_34 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_35-(int32_t)1));
		goto IL_0088;
	}

IL_0083:
	{
		goto IL_008d;
	}

IL_0088:
	{
		goto IL_001c;
	}

IL_008d:
	{
		int32_t L_36 = ___low0;
		int32_t L_37 = V_1;
		if ((((int32_t)L_36) >= ((int32_t)L_37)))
		{
			goto IL_009f;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_38 = ___keys;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_39 = ___items;
		int32_t L_40 = ___low0;
		int32_t L_41 = V_1;
		Object_t* L_42 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_38, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_39, (int32_t)L_40, (int32_t)L_41, (Object_t*)L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_009f:
	{
		int32_t L_43 = V_0;
		int32_t L_44 = ___high0;
		if ((((int32_t)L_43) >= ((int32_t)L_44)))
		{
			goto IL_00b1;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_45 = ___keys;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_46 = ___items;
		int32_t L_47 = V_0;
		int32_t L_48 = ___high0;
		Object_t* L_49 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_45, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_46, (int32_t)L_47, (int32_t)L_48, (Object_t*)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_00b1:
	{
		return;
	}
}
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeNamedArgument>(T,T,System.Collections.Generic.IComparer`1<T>)
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeNamedArgument>(T,T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* IComparable_t2534_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t532_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3772;
extern "C" int32_t Array_compare_TisCustomAttributeNamedArgument_t2100_m31276_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgument_t2100  ___value1, CustomAttributeNamedArgument_t2100  ___value2, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IComparable_t2534_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1278);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		InvalidOperationException_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(242);
		_stringLiteral3772 = il2cpp_codegen_string_literal_from_index(3772);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		Object_t* L_0 = ___comparer;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Object_t* L_1 = ___comparer;
		CustomAttributeNamedArgument_t2100  L_2 = ___value1;
		CustomAttributeNamedArgument_t2100  L_3 = ___value2;
		NullCheck((Object_t*)L_1);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, CustomAttributeNamedArgument_t2100 , CustomAttributeNamedArgument_t2100  >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Object_t*)L_1, (CustomAttributeNamedArgument_t2100 )L_2, (CustomAttributeNamedArgument_t2100 )L_3);
		return L_4;
	}

IL_000f:
	{
		CustomAttributeNamedArgument_t2100  L_5 = ___value1;
		goto IL_002d;
	}
	{
		CustomAttributeNamedArgument_t2100  L_6 = ___value2;
		goto IL_002b;
	}
	{
		G_B6_0 = 0;
		goto IL_002c;
	}

IL_002b:
	{
		G_B6_0 = (-1);
	}

IL_002c:
	{
		return G_B6_0;
	}

IL_002d:
	{
		CustomAttributeNamedArgument_t2100  L_7 = ___value2;
		goto IL_003a;
	}
	{
		return 1;
	}

IL_003a:
	{
		CustomAttributeNamedArgument_t2100  L_8 = ___value1;
		CustomAttributeNamedArgument_t2100  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_9);
		if (!((Object_t*)IsInst(L_10, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))
		{
			goto IL_005c;
		}
	}
	{
		CustomAttributeNamedArgument_t2100  L_11 = ___value1;
		CustomAttributeNamedArgument_t2100  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_12);
		CustomAttributeNamedArgument_t2100  L_14 = ___value2;
		NullCheck((Object_t*)((Object_t*)Castclass(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
		int32_t L_15 = (int32_t)InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t2100  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeNamedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Object_t*)((Object_t*)Castclass(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))), (CustomAttributeNamedArgument_t2100 )L_14);
		return L_15;
	}

IL_005c:
	{
		CustomAttributeNamedArgument_t2100  L_16 = ___value1;
		CustomAttributeNamedArgument_t2100  L_17 = L_16;
		Object_t * L_18 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_17);
		if (!((Object_t *)IsInst(L_18, IComparable_t2534_il2cpp_TypeInfo_var)))
		{
			goto IL_0083;
		}
	}
	{
		CustomAttributeNamedArgument_t2100  L_19 = ___value1;
		CustomAttributeNamedArgument_t2100  L_20 = L_19;
		Object_t * L_21 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_20);
		CustomAttributeNamedArgument_t2100  L_22 = ___value2;
		CustomAttributeNamedArgument_t2100  L_23 = L_22;
		Object_t * L_24 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_23);
		NullCheck((Object_t *)((Object_t *)Castclass(L_21, IComparable_t2534_il2cpp_TypeInfo_var)));
		int32_t L_25 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t2534_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_21, IComparable_t2534_il2cpp_TypeInfo_var)), (Object_t *)L_24);
		return L_25;
	}

IL_0083:
	{
		String_t* L_26 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral3772, /*hidden argument*/NULL);
		V_0 = (String_t*)L_26;
		String_t* L_27 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, (RuntimeTypeHandle_t1857 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 3)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m2404(NULL /*static, unused*/, (String_t*)L_27, (Object_t *)L_28, /*hidden argument*/NULL);
		InvalidOperationException_t532 * L_30 = (InvalidOperationException_t532 *)il2cpp_codegen_object_new (InvalidOperationException_t532_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2398(L_30, (String_t*)L_29, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_30);
	}
}
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(K[],V[],System.Int32,System.Int32)
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(K[],V[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t2100_TisCustomAttributeNamedArgument_t2100_m31277_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___keys, CustomAttributeNamedArgumentU5BU5D_t2538* ___items, int32_t ___i, int32_t ___j, const MethodInfo* method)
{
	CustomAttributeNamedArgument_t2100  V_0 = {0};
	CustomAttributeNamedArgument_t2100  V_1 = {0};
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___keys;
		int32_t L_1 = ___i;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_0, L_2, sizeof(CustomAttributeNamedArgument_t2100 )));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_3 = ___keys;
		int32_t L_4 = ___i;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_5 = ___keys;
		int32_t L_6 = ___j;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_3, L_4, sizeof(CustomAttributeNamedArgument_t2100 ))) = (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_5, L_7, sizeof(CustomAttributeNamedArgument_t2100 )));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_8 = ___keys;
		int32_t L_9 = ___j;
		CustomAttributeNamedArgument_t2100  L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_8, L_9, sizeof(CustomAttributeNamedArgument_t2100 ))) = (CustomAttributeNamedArgument_t2100 )L_10;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_11 = ___items;
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_12 = ___items;
		int32_t L_13 = ___i;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_1 = (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_12, L_14, sizeof(CustomAttributeNamedArgument_t2100 )));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_15 = ___items;
		int32_t L_16 = ___i;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_17 = ___items;
		int32_t L_18 = ___j;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		*((CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_15, L_16, sizeof(CustomAttributeNamedArgument_t2100 ))) = (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_17, L_19, sizeof(CustomAttributeNamedArgument_t2100 )));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_20 = ___items;
		int32_t L_21 = ___j;
		CustomAttributeNamedArgument_t2100  L_22 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		*((CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_20, L_21, sizeof(CustomAttributeNamedArgument_t2100 ))) = (CustomAttributeNamedArgument_t2100 )L_22;
	}

IL_0042:
	{
		return;
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Comparison`1<T>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Comparison`1<T>)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t532_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3773;
extern Il2CppCodeGenString* _stringLiteral3774;
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t2100_m31278_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___length, Comparison_1_t3564 * ___comparison, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		InvalidOperationException_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(242);
		_stringLiteral3773 = il2cpp_codegen_string_literal_from_index(3773);
		_stringLiteral3774 = il2cpp_codegen_string_literal_from_index(3774);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Exception_t40 * V_2 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Comparison_1_t3564 * L_0 = ___comparison;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral3773, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___length;
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_3 = ___array;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) > ((int32_t)1)))
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		V_0 = (int32_t)0;
		int32_t L_4 = ___length;
		V_1 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_5 = ___array;
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		Comparison_1_t3564 * L_8 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Comparison_1_t3564 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_5, (int32_t)L_6, (int32_t)L_7, (Comparison_1_t3564 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t40 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t40_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0036;
		throw e;
	}

CATCH_0036:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t40 *)((Exception_t40 *)__exception_local);
			String_t* L_9 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral3774, /*hidden argument*/NULL);
			Exception_t40 * L_10 = V_2;
			InvalidOperationException_t532 * L_11 = (InvalidOperationException_t532 *)il2cpp_codegen_object_new (InvalidOperationException_t532_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m15017(L_11, (String_t*)L_9, (Exception_t40 *)L_10, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
		}

IL_0048:
		{
			goto IL_004d;
		}
	} // end catch (depth: 1)

IL_004d:
	{
		return;
	}
}
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t2100_m31279_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___low0, int32_t ___high0, Comparison_1_t3564 * ___comparison, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CustomAttributeNamedArgument_t2100  V_3 = {0};
	{
		int32_t L_0 = ___low0;
		int32_t L_1 = ___high0;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_2 = ___low0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___high0;
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))/(int32_t)2))));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_7 = ___array;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_7, L_9, sizeof(CustomAttributeNamedArgument_t2100 )));
	}

IL_001c:
	{
		goto IL_0025;
	}

IL_0021:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___high0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0040;
		}
	}
	{
		Comparison_1_t3564 * L_13 = ___comparison;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_14 = ___array;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		CustomAttributeNamedArgument_t2100  L_17 = V_3;
		NullCheck((Comparison_1_t3564 *)L_13);
		int32_t L_18 = (( int32_t (*) (Comparison_1_t3564 *, CustomAttributeNamedArgument_t2100 , CustomAttributeNamedArgument_t2100 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Comparison_1_t3564 *)L_13, (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_14, L_16, sizeof(CustomAttributeNamedArgument_t2100 ))), (CustomAttributeNamedArgument_t2100 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_0040:
	{
		goto IL_0049;
	}

IL_0045:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_0049:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___low0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_0064;
		}
	}
	{
		Comparison_1_t3564 * L_22 = ___comparison;
		CustomAttributeNamedArgument_t2100  L_23 = V_3;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_24 = ___array;
		int32_t L_25 = V_1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		NullCheck((Comparison_1_t3564 *)L_22);
		int32_t L_27 = (( int32_t (*) (Comparison_1_t3564 *, CustomAttributeNamedArgument_t2100 , CustomAttributeNamedArgument_t2100 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Comparison_1_t3564 *)L_22, (CustomAttributeNamedArgument_t2100 )L_23, (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_24, L_26, sizeof(CustomAttributeNamedArgument_t2100 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}

IL_0064:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) > ((int32_t)L_29)))
		{
			goto IL_0080;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_30 = ___array;
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_30, (int32_t)L_31, (int32_t)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_33 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		int32_t L_34 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_34-(int32_t)1));
		goto IL_0085;
	}

IL_0080:
	{
		goto IL_008a;
	}

IL_0085:
	{
		goto IL_001c;
	}

IL_008a:
	{
		int32_t L_35 = ___low0;
		int32_t L_36 = V_1;
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_009a;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_37 = ___array;
		int32_t L_38 = ___low0;
		int32_t L_39 = V_1;
		Comparison_1_t3564 * L_40 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Comparison_1_t3564 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_37, (int32_t)L_38, (int32_t)L_39, (Comparison_1_t3564 *)L_40, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_009a:
	{
		int32_t L_41 = V_0;
		int32_t L_42 = ___high0;
		if ((((int32_t)L_41) >= ((int32_t)L_42)))
		{
			goto IL_00aa;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_43 = ___array;
		int32_t L_44 = V_0;
		int32_t L_45 = ___high0;
		Comparison_1_t3564 * L_46 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, int32_t, int32_t, Comparison_1_t3564 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_43, (int32_t)L_44, (int32_t)L_45, (Comparison_1_t3564 *)L_46, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_00aa:
	{
		return;
	}
}
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32)
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t2100_m31280_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, int32_t ___i, int32_t ___j, const MethodInfo* method)
{
	CustomAttributeNamedArgument_t2100  V_0 = {0};
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___array;
		int32_t L_1 = ___i;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_0, L_2, sizeof(CustomAttributeNamedArgument_t2100 )));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_3 = ___array;
		int32_t L_4 = ___i;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_5 = ___array;
		int32_t L_6 = ___j;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_3, L_4, sizeof(CustomAttributeNamedArgument_t2100 ))) = (CustomAttributeNamedArgument_t2100 )(*(CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_5, L_7, sizeof(CustomAttributeNamedArgument_t2100 )));
		CustomAttributeNamedArgumentU5BU5D_t2538* L_8 = ___array;
		int32_t L_9 = ___j;
		CustomAttributeNamedArgument_t2100  L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((CustomAttributeNamedArgument_t2100 *)(CustomAttributeNamedArgument_t2100 *)SZArrayLdElema(L_8, L_9, sizeof(CustomAttributeNamedArgument_t2100 ))) = (CustomAttributeNamedArgument_t2100 )L_10;
		return;
	}
}
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern "C" int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t2100_m31281_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538* ___array, CustomAttributeNamedArgument_t2100  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeNamedArgumentU5BU5D_t2538* L_2 = ___array;
		CustomAttributeNamedArgument_t2100  L_3 = ___value;
		CustomAttributeNamedArgumentU5BU5D_t2538* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2538*, CustomAttributeNamedArgument_t2100 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2538*)L_2, (CustomAttributeNamedArgument_t2100 )L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C" ObjectU5BU5D_t34* CustomAttributeData_UnboxValues_TisObject_t_m31282_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t34* ___values, const MethodInfo* method)
{
	ObjectU5BU5D_t34* V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t34* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		ObjectU5BU5D_t34* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t34* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, L_2, sizeof(Object_t *))) = (Object_t *)((Object_t *)Castclass((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t34* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t34* L_9 = V_0;
		return L_9;
	}
}
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern "C" Object_t * MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m31283_gshared (Object_t * __this /* static, unused */, Getter_2_t3567 * ___getter, Object_t * ___obj, const MethodInfo* method)
{
	{
		Getter_2_t3567 * L_0 = ___getter;
		Object_t * L_1 = ___obj;
		NullCheck((Getter_2_t3567 *)L_0);
		Object_t * L_2 = (( Object_t * (*) (Getter_2_t3567 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Getter_2_t3567 *)L_0, (Object_t *)((Object_t *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern "C" Object_t * MonoProperty_StaticGetterAdapterFrame_TisObject_t_m31284_gshared (Object_t * __this /* static, unused */, StaticGetter_1_t3568 * ___getter, Object_t * ___obj, const MethodInfo* method)
{
	{
		StaticGetter_1_t3568 * L_0 = ___getter;
		NullCheck((StaticGetter_1_t3568 *)L_0);
		Object_t * L_1 = (( Object_t * (*) (StaticGetter_1_t3568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((StaticGetter_1_t3568 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" ResourceInfo_t2130  Array_InternalArray__get_Item_TisResourceInfo_t2130_m31285_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ResourceInfo_t2130  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (ResourceInfo_t2130 *)(&V_0));
		ResourceInfo_t2130  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceInfo>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceInfo>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisResourceInfo_t2130_m31286_gshared (Array_t * __this, ResourceInfo_t2130  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceInfo>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceInfo>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisResourceInfo_t2130_m31287_gshared (Array_t * __this, ResourceInfo_t2130  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ResourceInfo_t2130  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ResourceInfo_t2130 *)(&V_2));
		ResourceInfo_t2130  L_5 = ___item;
		goto IL_004d;
	}
	{
		ResourceInfo_t2130  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		ResourceInfo_t2130  L_7 = V_2;
		ResourceInfo_t2130  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceInfo>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceInfo>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t2130_m31288_gshared (Array_t * __this, ResourceInfoU5BU5D_t2134* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		ResourceInfoU5BU5D_t2134* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ResourceInfoU5BU5D_t2134* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		ResourceInfoU5BU5D_t2134* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		ResourceInfoU5BU5D_t2134* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ResourceInfoU5BU5D_t2134* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceInfo>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceInfo>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisResourceInfo_t2130_m31289_gshared (Array_t * __this, ResourceInfo_t2130  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceInfo>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceInfo>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisResourceInfo_t2130_m31290_gshared (Array_t * __this, ResourceInfo_t2130  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ResourceInfo_t2130  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ResourceInfo_t2130 *)(&V_2));
		ResourceInfo_t2130  L_5 = ___item;
		goto IL_005d;
	}
	{
		ResourceInfo_t2130  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		ResourceInfo_t2130  L_10 = ___item;
		ResourceInfo_t2130  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisResourceInfo_t2130_m31291_gshared (Array_t * __this, int32_t ___index, ResourceInfo_t2130  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisResourceInfo_t2130_m31292_gshared (Array_t * __this, int32_t ___index, ResourceInfo_t2130  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		ResourceInfo_t2130  L_6 = ___item;
		ResourceInfo_t2130  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (ResourceInfo_t2130 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceInfo>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceInfo>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t2130_m31293_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3569  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3569 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3569  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" ResourceCacheItem_t2131  Array_InternalArray__get_Item_TisResourceCacheItem_t2131_m31294_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ResourceCacheItem_t2131  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (ResourceCacheItem_t2131 *)(&V_0));
		ResourceCacheItem_t2131  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceCacheItem>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisResourceCacheItem_t2131_m31295_gshared (Array_t * __this, ResourceCacheItem_t2131  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceCacheItem>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t2131_m31296_gshared (Array_t * __this, ResourceCacheItem_t2131  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ResourceCacheItem_t2131  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ResourceCacheItem_t2131 *)(&V_2));
		ResourceCacheItem_t2131  L_5 = ___item;
		goto IL_004d;
	}
	{
		ResourceCacheItem_t2131  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		ResourceCacheItem_t2131  L_7 = V_2;
		ResourceCacheItem_t2131  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceCacheItem>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceCacheItem>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t2131_m31297_gshared (Array_t * __this, ResourceCacheItemU5BU5D_t2135* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		ResourceCacheItemU5BU5D_t2135* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ResourceCacheItemU5BU5D_t2135* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		ResourceCacheItemU5BU5D_t2135* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		ResourceCacheItemU5BU5D_t2135* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ResourceCacheItemU5BU5D_t2135* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceCacheItem>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t2131_m31298_gshared (Array_t * __this, ResourceCacheItem_t2131  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceCacheItem>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisResourceCacheItem_t2131_m31299_gshared (Array_t * __this, ResourceCacheItem_t2131  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ResourceCacheItem_t2131  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ResourceCacheItem_t2131 *)(&V_2));
		ResourceCacheItem_t2131  L_5 = ___item;
		goto IL_005d;
	}
	{
		ResourceCacheItem_t2131  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		ResourceCacheItem_t2131  L_10 = ___item;
		ResourceCacheItem_t2131  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisResourceCacheItem_t2131_m31300_gshared (Array_t * __this, int32_t ___index, ResourceCacheItem_t2131  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisResourceCacheItem_t2131_m31301_gshared (Array_t * __this, int32_t ___index, ResourceCacheItem_t2131  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		ResourceCacheItem_t2131  L_6 = ___item;
		ResourceCacheItem_t2131  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (ResourceCacheItem_t2131 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceCacheItem>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceCacheItem>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t2131_m31302_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3570  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3570 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3570  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" DateTime_t220  Array_InternalArray__get_Item_TisDateTime_t220_m31303_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t220  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (DateTime_t220 *)(&V_0));
		DateTime_t220  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t220_m31304_gshared (Array_t * __this, DateTime_t220  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisDateTime_t220_m31305_gshared (Array_t * __this, DateTime_t220  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t220  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (DateTime_t220 *)(&V_2));
		DateTime_t220  L_5 = ___item;
		goto IL_004d;
	}
	{
		DateTime_t220  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		DateTime_t220  L_7 = V_2;
		DateTime_t220  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((DateTime_t220 *)(&___item));
		bool L_10 = DateTime_Equals_m14895((DateTime_t220 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t220_m31306_gshared (Array_t * __this, DateTimeU5BU5D_t2560* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTimeU5BU5D_t2560* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2560* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2560* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		DateTimeU5BU5D_t2560* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2560* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisDateTime_t220_m31307_gshared (Array_t * __this, DateTime_t220  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisDateTime_t220_m31308_gshared (Array_t * __this, DateTime_t220  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t220  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (DateTime_t220 *)(&V_2));
		DateTime_t220  L_5 = ___item;
		goto IL_005d;
	}
	{
		DateTime_t220  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		DateTime_t220  L_10 = ___item;
		DateTime_t220  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((DateTime_t220 *)(&V_2));
		bool L_13 = DateTime_Equals_m14895((DateTime_t220 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisDateTime_t220_m31309_gshared (Array_t * __this, int32_t ___index, DateTime_t220  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisDateTime_t220_m31310_gshared (Array_t * __this, int32_t ___index, DateTime_t220  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		DateTime_t220  L_6 = ___item;
		DateTime_t220  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (DateTime_t220 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t220_m31311_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3575  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3575 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3575  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" Decimal_t564  Array_InternalArray__get_Item_TisDecimal_t564_m31312_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	Decimal_t564  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Decimal_t564 *)(&V_0));
		Decimal_t564  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t564_m31313_gshared (Array_t * __this, Decimal_t564  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisDecimal_t564_m31314_gshared (Array_t * __this, Decimal_t564  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Decimal_t564  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Decimal_t564 *)(&V_2));
		Decimal_t564  L_5 = ___item;
		goto IL_004d;
	}
	{
		Decimal_t564  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		Decimal_t564  L_7 = V_2;
		Decimal_t564  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Decimal_t564 *)(&___item));
		bool L_10 = Decimal_Equals_m10714((Decimal_t564 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t564_m31315_gshared (Array_t * __this, DecimalU5BU5D_t2561* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		DecimalU5BU5D_t2561* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2561* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2561* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		DecimalU5BU5D_t2561* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2561* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisDecimal_t564_m31316_gshared (Array_t * __this, Decimal_t564  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisDecimal_t564_m31317_gshared (Array_t * __this, Decimal_t564  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Decimal_t564  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Decimal_t564 *)(&V_2));
		Decimal_t564  L_5 = ___item;
		goto IL_005d;
	}
	{
		Decimal_t564  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		Decimal_t564  L_10 = ___item;
		Decimal_t564  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Decimal_t564 *)(&V_2));
		bool L_13 = Decimal_Equals_m10714((Decimal_t564 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisDecimal_t564_m31318_gshared (Array_t * __this, int32_t ___index, Decimal_t564  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisDecimal_t564_m31319_gshared (Array_t * __this, int32_t ___index, Decimal_t564  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		Decimal_t564  L_6 = ___item;
		Decimal_t564  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Decimal_t564 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t564_m31320_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3576  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3576 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3576  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" TimeSpan_t565  Array_InternalArray__get_Item_TisTimeSpan_t565_m31321_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t565  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (TimeSpan_t565 *)(&V_0));
		TimeSpan_t565  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t565_m31322_gshared (Array_t * __this, TimeSpan_t565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTimeSpan_t565_m31323_gshared (Array_t * __this, TimeSpan_t565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TimeSpan_t565  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TimeSpan_t565 *)(&V_2));
		TimeSpan_t565  L_5 = ___item;
		goto IL_004d;
	}
	{
		TimeSpan_t565  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		TimeSpan_t565  L_7 = V_2;
		TimeSpan_t565  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((TimeSpan_t565 *)(&___item));
		bool L_10 = TimeSpan_Equals_m15294((TimeSpan_t565 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t565_m31324_gshared (Array_t * __this, TimeSpanU5BU5D_t2562* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpanU5BU5D_t2562* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2562* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2562* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		TimeSpanU5BU5D_t2562* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2562* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTimeSpan_t565_m31325_gshared (Array_t * __this, TimeSpan_t565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisTimeSpan_t565_m31326_gshared (Array_t * __this, TimeSpan_t565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TimeSpan_t565  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TimeSpan_t565 *)(&V_2));
		TimeSpan_t565  L_5 = ___item;
		goto IL_005d;
	}
	{
		TimeSpan_t565  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		TimeSpan_t565  L_10 = ___item;
		TimeSpan_t565  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((TimeSpan_t565 *)(&V_2));
		bool L_13 = TimeSpan_Equals_m15294((TimeSpan_t565 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t565_m31327_gshared (Array_t * __this, int32_t ___index, TimeSpan_t565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t565_m31328_gshared (Array_t * __this, int32_t ___index, TimeSpan_t565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		TimeSpan_t565  L_6 = ___item;
		TimeSpan_t565  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (TimeSpan_t565 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t565_m31329_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3577  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3577 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3577  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" uint8_t Array_InternalArray__get_Item_TisTypeTag_t2267_m31330_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__ICollection_Add_TisTypeTag_t2267_m31331_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTypeTag_t2267_m31332_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (uint8_t*)(&V_2));
		uint8_t L_5 = ___item;
		goto IL_004d;
	}
	{
		uint8_t L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		uint8_t L_7 = V_2;
		uint8_t L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t512_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1323;
extern Il2CppCodeGenString* _stringLiteral2125;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral1325;
extern Il2CppCodeGenString* _stringLiteral2148;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTypeTag_t2267_m31333_gshared (Array_t * __this, TypeTagU5BU5D_t2563* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		_stringLiteral1323 = il2cpp_codegen_string_literal_from_index(1323);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeTagU5BU5D_t2563* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t512 * L_1 = (ArgumentNullException_t512 *)il2cpp_codegen_object_new (ArgumentNullException_t512_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2347(L_1, (String_t*)_stringLiteral1323, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_4 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2563* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2563* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m353((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t513 * L_11 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_11, (String_t*)_stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		TypeTagU5BU5D_t2563* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9146((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_15 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2148, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1665 * L_18 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9140(L_18, (String_t*)_stringLiteral1325, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2563* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m353((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m362(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTypeTag_t2267_m31334_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* RankException_t2465_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Array_InternalArray__IndexOf_TisTypeTag_t2267_m31335_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9146((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m11120(NULL /*static, unused*/, (String_t*)_stringLiteral2125, /*hidden argument*/NULL);
		RankException_t2465 * L_2 = (RankException_t2465 *)il2cpp_codegen_object_new (RankException_t2465_il2cpp_TypeInfo_var);
		RankException__ctor_m15248(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (uint8_t*)(&V_2));
		uint8_t L_5 = ___item;
		goto IL_005d;
	}
	{
		uint8_t L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		uint8_t L_10 = ___item;
		uint8_t L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10928((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348;
extern "C" void Array_InternalArray__Insert_TisTypeTag_t2267_m31336_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral1348 = il2cpp_codegen_string_literal_from_index(1348);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2362(L_0, (String_t*)_stringLiteral1348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void Array_InternalArray__set_Item_TisTypeTag_t2267_m31337_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t34* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9141((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1665 * L_2 = (ArgumentOutOfRangeException_t1665 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1665_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9145(L_2, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t34*)((ObjectU5BU5D_t34*)IsInst(__this, ObjectU5BU5D_t34_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t34* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t34* L_4 = V_0;
		int32_t L_5 = ___index;
		uint8_t L_6 = ___item;
		uint8_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (uint8_t*)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t2267_m31338_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3578  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3578 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3578  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
