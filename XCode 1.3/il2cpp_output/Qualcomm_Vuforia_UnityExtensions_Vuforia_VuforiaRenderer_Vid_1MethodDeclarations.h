﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void VideoBGCfgData_t991_marshal(const VideoBGCfgData_t991& unmarshaled, VideoBGCfgData_t991_marshaled& marshaled);
extern "C" void VideoBGCfgData_t991_marshal_back(const VideoBGCfgData_t991_marshaled& marshaled, VideoBGCfgData_t991& unmarshaled);
extern "C" void VideoBGCfgData_t991_marshal_cleanup(VideoBGCfgData_t991_marshaled& marshaled);
