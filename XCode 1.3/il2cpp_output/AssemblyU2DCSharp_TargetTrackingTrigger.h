﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t27;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// TargetTrackingTrigger
struct  TargetTrackingTrigger_t391  : public MonoBehaviour_t18
{
	// UnityEngine.GameObject TargetTrackingTrigger::triggerTarget
	GameObject_t27 * ___triggerTarget_1;
	// System.String TargetTrackingTrigger::targetFoundMessage
	String_t* ___targetFoundMessage_2;
	// System.String TargetTrackingTrigger::targetLostMessage
	String_t* ___targetLostMessage_3;
	// System.Boolean TargetTrackingTrigger::wasEnabled
	bool ___wasEnabled_4;
};
