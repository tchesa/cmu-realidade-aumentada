﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen_4MethodDeclarations.h"

// System.Void System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m4424(__this, ___object, ___method, method) (( void (*) (Func_2_t786 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m22077_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::Invoke(T)
#define Func_2_Invoke_m4425(__this, ___arg1, method) (( float (*) (Func_2_t786 *, Object_t *, const MethodInfo*))Func_2_Invoke_m22078_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m22079(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t786 *, Object_t *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m22080_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m22081(__this, ___result, method) (( float (*) (Func_2_t786 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m22082_gshared)(__this, ___result, method)
