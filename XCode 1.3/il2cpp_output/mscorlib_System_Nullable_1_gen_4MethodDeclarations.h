﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_4.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m15439_gshared (Nullable_1_t2528 * __this, TimeSpan_t565  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m15439(__this, ___value, method) (( void (*) (Nullable_1_t2528 *, TimeSpan_t565 , const MethodInfo*))Nullable_1__ctor_m15439_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m15440_gshared (Nullable_1_t2528 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m15440(__this, method) (( bool (*) (Nullable_1_t2528 *, const MethodInfo*))Nullable_1_get_HasValue_m15440_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t565  Nullable_1_get_Value_m15441_gshared (Nullable_1_t2528 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m15441(__this, method) (( TimeSpan_t565  (*) (Nullable_1_t2528 *, const MethodInfo*))Nullable_1_get_Value_m15441_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m30148_gshared (Nullable_1_t2528 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m30148(__this, ___other, method) (( bool (*) (Nullable_1_t2528 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m30148_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m30149_gshared (Nullable_1_t2528 * __this, Nullable_1_t2528  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m30149(__this, ___other, method) (( bool (*) (Nullable_1_t2528 *, Nullable_1_t2528 , const MethodInfo*))Nullable_1_Equals_m30149_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m30150_gshared (Nullable_1_t2528 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m30150(__this, method) (( int32_t (*) (Nullable_1_t2528 *, const MethodInfo*))Nullable_1_GetHashCode_m30150_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
extern "C" TimeSpan_t565  Nullable_1_GetValueOrDefault_m30151_gshared (Nullable_1_t2528 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m30151(__this, method) (( TimeSpan_t565  (*) (Nullable_1_t2528 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m30151_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m30152_gshared (Nullable_1_t2528 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m30152(__this, method) (( String_t* (*) (Nullable_1_t2528 *, const MethodInfo*))Nullable_1_ToString_m30152_gshared)(__this, method)
