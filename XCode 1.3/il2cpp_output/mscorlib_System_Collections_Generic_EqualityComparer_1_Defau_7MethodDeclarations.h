﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct DefaultComparer_t3252;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C" void DefaultComparer__ctor_m26058_gshared (DefaultComparer_t3252 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m26058(__this, method) (( void (*) (DefaultComparer_t3252 *, const MethodInfo*))DefaultComparer__ctor_m26058_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26059_gshared (DefaultComparer_t3252 * __this, TrackableResultData_t969  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m26059(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3252 *, TrackableResultData_t969 , const MethodInfo*))DefaultComparer_GetHashCode_m26059_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26060_gshared (DefaultComparer_t3252 * __this, TrackableResultData_t969  ___x, TrackableResultData_t969  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m26060(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3252 *, TrackableResultData_t969 , TrackableResultData_t969 , const MethodInfo*))DefaultComparer_Equals_m26060_gshared)(__this, ___x, ___y, method)
