﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSharedApplication
struct IOSSharedApplication_t153;
// System.String
struct String_t;
// ISN_CheckUrlResult
struct ISN_CheckUrlResult_t156;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSSharedApplication::.ctor()
extern "C" void IOSSharedApplication__ctor_m848 (IOSSharedApplication_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::.cctor()
extern "C" void IOSSharedApplication__cctor_m849 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::_ISN_checkUrl(System.String)
extern "C" void IOSSharedApplication__ISN_checkUrl_m850 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::_ISN_openUrl(System.String)
extern "C" void IOSSharedApplication__ISN_openUrl_m851 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::CheckUrl(System.String)
extern "C" void IOSSharedApplication_CheckUrl_m852 (IOSSharedApplication_t153 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::OpenUrl(System.String)
extern "C" void IOSSharedApplication_OpenUrl_m853 (IOSSharedApplication_t153 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::UrlCheckSuccess(System.String)
extern "C" void IOSSharedApplication_UrlCheckSuccess_m854 (IOSSharedApplication_t153 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::UrlCheckFailed(System.String)
extern "C" void IOSSharedApplication_UrlCheckFailed_m855 (IOSSharedApplication_t153 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::<OnUrlCheckResultAction>m__1F(ISN_CheckUrlResult)
extern "C" void IOSSharedApplication_U3COnUrlCheckResultActionU3Em__1F_m856 (Object_t * __this /* static, unused */, ISN_CheckUrlResult_t156 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
