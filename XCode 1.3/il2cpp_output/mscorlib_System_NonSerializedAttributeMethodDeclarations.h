﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.NonSerializedAttribute
struct NonSerializedAttribute_t2459;

#include "codegen/il2cpp-codegen.h"

// System.Void System.NonSerializedAttribute::.ctor()
extern "C" void NonSerializedAttribute__ctor_m15130 (NonSerializedAttribute_t2459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
