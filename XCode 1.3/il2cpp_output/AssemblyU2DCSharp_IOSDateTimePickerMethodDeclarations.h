﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSDateTimePicker
struct IOSDateTimePicker_t150;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_IOSDateTimePickerMode.h"
#include "mscorlib_System_DateTime.h"

// System.Void IOSDateTimePicker::.ctor()
extern "C" void IOSDateTimePicker__ctor_m841 (IOSDateTimePicker_t150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::_ISN_ShowDP(System.Int32)
extern "C" void IOSDateTimePicker__ISN_ShowDP_m842 (Object_t * __this /* static, unused */, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::Show(IOSDateTimePickerMode)
extern "C" void IOSDateTimePicker_Show_m843 (IOSDateTimePicker_t150 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::DateChangedEvent(System.String)
extern "C" void IOSDateTimePicker_DateChangedEvent_m844 (IOSDateTimePicker_t150 * __this, String_t* ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::PickerClosed(System.String)
extern "C" void IOSDateTimePicker_PickerClosed_m845 (IOSDateTimePicker_t150 * __this, String_t* ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::<OnDateChanged>m__1D(System.DateTime)
extern "C" void IOSDateTimePicker_U3COnDateChangedU3Em__1D_m846 (Object_t * __this /* static, unused */, DateTime_t220  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::<OnPickerClosed>m__1E(System.DateTime)
extern "C" void IOSDateTimePicker_U3COnPickerClosedU3Em__1E_m847 (Object_t * __this /* static, unused */, DateTime_t220  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
