﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "mscorlib_System_Object.h"

// IOSNotificationDeviceToken
struct  IOSNotificationDeviceToken_t143  : public Object_t
{
	// System.String IOSNotificationDeviceToken::_tokenString
	String_t* ____tokenString_0;
	// System.Byte[] IOSNotificationDeviceToken::_tokenBytes
	ByteU5BU5D_t119* ____tokenBytes_1;
};
