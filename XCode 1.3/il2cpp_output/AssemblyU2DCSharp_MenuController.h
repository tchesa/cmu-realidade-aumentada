﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ComponentTracker
struct ComponentTracker_t67;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// MenuController
struct  MenuController_t338  : public MonoBehaviour_t18
{
	// ComponentTracker MenuController::tracker
	ComponentTracker_t67 * ___tracker_1;
};
