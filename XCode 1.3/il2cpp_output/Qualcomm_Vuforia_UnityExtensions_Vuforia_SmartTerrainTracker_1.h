﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t1000;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0.h"

// Vuforia.SmartTerrainTrackerImpl
struct  SmartTerrainTrackerImpl_t1004  : public SmartTerrainTracker_t1003
{
	// System.Single Vuforia.SmartTerrainTrackerImpl::mScaleToMillimeter
	float ___mScaleToMillimeter_1;
	// Vuforia.SmartTerrainBuilderImpl Vuforia.SmartTerrainTrackerImpl::mSmartTerrainBuilder
	SmartTerrainBuilderImpl_t1000 * ___mSmartTerrainBuilder_2;
};
