﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
struct X509Certificate2Enumerator_t1550;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t1549;
// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t1545;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2Collection)
extern "C" void X509Certificate2Enumerator__ctor_m8497 (X509Certificate2Enumerator_t1550 * __this, X509Certificate2Collection_t1549 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m8498 (X509Certificate2Enumerator_t1550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.MoveNext()
extern "C" bool X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m8499 (X509Certificate2Enumerator_t1550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.Reset()
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m8500 (X509Certificate2Enumerator_t1550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::get_Current()
extern "C" X509Certificate2_t1545 * X509Certificate2Enumerator_get_Current_m8501 (X509Certificate2Enumerator_t1550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::MoveNext()
extern "C" bool X509Certificate2Enumerator_MoveNext_m8502 (X509Certificate2Enumerator_t1550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::Reset()
extern "C" void X509Certificate2Enumerator_Reset_m8503 (X509Certificate2Enumerator_t1550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
