﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.HMACMD5
struct HMACMD5_t2308;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.HMACMD5::.ctor()
extern "C" void HMACMD5__ctor_m13852 (HMACMD5_t2308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACMD5::.ctor(System.Byte[])
extern "C" void HMACMD5__ctor_m13853 (HMACMD5_t2308 * __this, ByteU5BU5D_t119* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
