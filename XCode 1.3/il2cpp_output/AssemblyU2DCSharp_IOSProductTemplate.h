﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// IOSProductTemplate
struct  IOSProductTemplate_t135  : public Object_t
{
	// System.String IOSProductTemplate::id
	String_t* ___id_0;
	// System.String IOSProductTemplate::title
	String_t* ___title_1;
	// System.String IOSProductTemplate::description
	String_t* ___description_2;
	// System.String IOSProductTemplate::price
	String_t* ___price_3;
	// System.String IOSProductTemplate::localizedPrice
	String_t* ___localizedPrice_4;
	// System.String IOSProductTemplate::currencySymbol
	String_t* ___currencySymbol_5;
	// System.String IOSProductTemplate::currencyCode
	String_t* ___currencyCode_6;
};
