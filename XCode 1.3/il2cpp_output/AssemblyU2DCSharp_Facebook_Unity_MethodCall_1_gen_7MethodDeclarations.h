﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.MethodCall`1<System.Object>
struct MethodCall_1_t2765;
// Facebook.Unity.FacebookBase
struct FacebookBase_t227;
// System.String
struct String_t;
// Facebook.Unity.FacebookDelegate`1<System.Object>
struct FacebookDelegate_1_t573;
// Facebook.Unity.MethodArguments
struct MethodArguments_t248;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.MethodCall`1<System.Object>::.ctor(Facebook.Unity.FacebookBase,System.String)
extern "C" void MethodCall_1__ctor_m18266_gshared (MethodCall_1_t2765 * __this, FacebookBase_t227 * ___facebookImpl, String_t* ___methodName, const MethodInfo* method);
#define MethodCall_1__ctor_m18266(__this, ___facebookImpl, ___methodName, method) (( void (*) (MethodCall_1_t2765 *, FacebookBase_t227 *, String_t*, const MethodInfo*))MethodCall_1__ctor_m18266_gshared)(__this, ___facebookImpl, ___methodName, method)
// System.String Facebook.Unity.MethodCall`1<System.Object>::get_MethodName()
extern "C" String_t* MethodCall_1_get_MethodName_m18267_gshared (MethodCall_1_t2765 * __this, const MethodInfo* method);
#define MethodCall_1_get_MethodName_m18267(__this, method) (( String_t* (*) (MethodCall_1_t2765 *, const MethodInfo*))MethodCall_1_get_MethodName_m18267_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_MethodName(System.String)
extern "C" void MethodCall_1_set_MethodName_m18268_gshared (MethodCall_1_t2765 * __this, String_t* ___value, const MethodInfo* method);
#define MethodCall_1_set_MethodName_m18268(__this, ___value, method) (( void (*) (MethodCall_1_t2765 *, String_t*, const MethodInfo*))MethodCall_1_set_MethodName_m18268_gshared)(__this, ___value, method)
// Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1<System.Object>::get_Callback()
extern "C" FacebookDelegate_1_t573 * MethodCall_1_get_Callback_m18269_gshared (MethodCall_1_t2765 * __this, const MethodInfo* method);
#define MethodCall_1_get_Callback_m18269(__this, method) (( FacebookDelegate_1_t573 * (*) (MethodCall_1_t2765 *, const MethodInfo*))MethodCall_1_get_Callback_m18269_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
extern "C" void MethodCall_1_set_Callback_m18270_gshared (MethodCall_1_t2765 * __this, FacebookDelegate_1_t573 * ___value, const MethodInfo* method);
#define MethodCall_1_set_Callback_m18270(__this, ___value, method) (( void (*) (MethodCall_1_t2765 *, FacebookDelegate_1_t573 *, const MethodInfo*))MethodCall_1_set_Callback_m18270_gshared)(__this, ___value, method)
// Facebook.Unity.FacebookBase Facebook.Unity.MethodCall`1<System.Object>::get_FacebookImpl()
extern "C" FacebookBase_t227 * MethodCall_1_get_FacebookImpl_m18271_gshared (MethodCall_1_t2765 * __this, const MethodInfo* method);
#define MethodCall_1_get_FacebookImpl_m18271(__this, method) (( FacebookBase_t227 * (*) (MethodCall_1_t2765 *, const MethodInfo*))MethodCall_1_get_FacebookImpl_m18271_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_FacebookImpl(Facebook.Unity.FacebookBase)
extern "C" void MethodCall_1_set_FacebookImpl_m18272_gshared (MethodCall_1_t2765 * __this, FacebookBase_t227 * ___value, const MethodInfo* method);
#define MethodCall_1_set_FacebookImpl_m18272(__this, ___value, method) (( void (*) (MethodCall_1_t2765 *, FacebookBase_t227 *, const MethodInfo*))MethodCall_1_set_FacebookImpl_m18272_gshared)(__this, ___value, method)
// Facebook.Unity.MethodArguments Facebook.Unity.MethodCall`1<System.Object>::get_Parameters()
extern "C" MethodArguments_t248 * MethodCall_1_get_Parameters_m18273_gshared (MethodCall_1_t2765 * __this, const MethodInfo* method);
#define MethodCall_1_get_Parameters_m18273(__this, method) (( MethodArguments_t248 * (*) (MethodCall_1_t2765 *, const MethodInfo*))MethodCall_1_get_Parameters_m18273_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_Parameters(Facebook.Unity.MethodArguments)
extern "C" void MethodCall_1_set_Parameters_m18274_gshared (MethodCall_1_t2765 * __this, MethodArguments_t248 * ___value, const MethodInfo* method);
#define MethodCall_1_set_Parameters_m18274(__this, ___value, method) (( void (*) (MethodCall_1_t2765 *, MethodArguments_t248 *, const MethodInfo*))MethodCall_1_set_Parameters_m18274_gshared)(__this, ___value, method)
