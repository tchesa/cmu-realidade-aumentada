﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativeMarketBridge
struct IOSNativeMarketBridge_t133;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNativeMarketBridge::.ctor()
extern "C" void IOSNativeMarketBridge__ctor_m756 (IOSNativeMarketBridge_t133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::_loadStore(System.String)
extern "C" void IOSNativeMarketBridge__loadStore_m757 (Object_t * __this /* static, unused */, String_t* ___ids, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::_restorePurchases()
extern "C" void IOSNativeMarketBridge__restorePurchases_m758 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::_buyProduct(System.String)
extern "C" void IOSNativeMarketBridge__buyProduct_m759 (Object_t * __this /* static, unused */, String_t* ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::_ISN_RequestInAppSettingState()
extern "C" void IOSNativeMarketBridge__ISN_RequestInAppSettingState_m760 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::_verifyLastPurchase(System.String)
extern "C" void IOSNativeMarketBridge__verifyLastPurchase_m761 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::loadStore(System.String)
extern "C" void IOSNativeMarketBridge_loadStore_m762 (Object_t * __this /* static, unused */, String_t* ___ids, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::buyProduct(System.String)
extern "C" void IOSNativeMarketBridge_buyProduct_m763 (Object_t * __this /* static, unused */, String_t* ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::restorePurchases()
extern "C" void IOSNativeMarketBridge_restorePurchases_m764 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::verifyLastPurchase(System.String)
extern "C" void IOSNativeMarketBridge_verifyLastPurchase_m765 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::ISN_RequestInAppSettingState()
extern "C" void IOSNativeMarketBridge_ISN_RequestInAppSettingState_m766 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
