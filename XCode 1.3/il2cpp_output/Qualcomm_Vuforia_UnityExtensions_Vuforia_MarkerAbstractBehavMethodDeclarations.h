﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t423;
// Vuforia.Marker
struct Marker_t1069;
// UnityEngine.Transform
struct Transform_t25;
// UnityEngine.GameObject
struct GameObject_t27;

#include "codegen/il2cpp-codegen.h"

// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::get_Marker()
extern "C" Object_t * MarkerAbstractBehaviour_get_Marker_m6173 (MarkerAbstractBehaviour_t423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::.ctor()
extern "C" void MarkerAbstractBehaviour__ctor_m2745 (MarkerAbstractBehaviour_t423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void MarkerAbstractBehaviour_InternalUnregisterTrackable_m6174 (MarkerAbstractBehaviour_t423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::CorrectScaleImpl()
extern "C" bool MarkerAbstractBehaviour_CorrectScaleImpl_m6175 (MarkerAbstractBehaviour_t423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.get_MarkerID()
extern "C" int32_t MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m6176 (MarkerAbstractBehaviour_t423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.SetMarkerID(System.Int32)
extern "C" bool MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m6177 (MarkerAbstractBehaviour_t423 * __this, int32_t ___markerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.InitializeMarker(Vuforia.Marker)
extern "C" void MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m6178 (MarkerAbstractBehaviour_t423 * __this, Object_t * ___marker, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m6179 (MarkerAbstractBehaviour_t423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m6180 (MarkerAbstractBehaviour_t423 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t25 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m6181 (MarkerAbstractBehaviour_t423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t27 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m6182 (MarkerAbstractBehaviour_t423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
