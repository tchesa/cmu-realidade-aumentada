﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpWebRequest
struct HttpWebRequest_t1519;
// System.Uri
struct Uri_t292;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Net.ServicePoint
struct ServicePoint_t1521;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Net.HttpWebRequest::.ctor(System.Uri)
extern "C" void HttpWebRequest__ctor_m8335 (HttpWebRequest_t1519 * __this, Uri_t292 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpWebRequest::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HttpWebRequest__ctor_m8336 (HttpWebRequest_t1519 * __this, SerializationInfo_t1438 * ___serializationInfo, StreamingContext_t1439  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpWebRequest::.cctor()
extern "C" void HttpWebRequest__cctor_m8337 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpWebRequest::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m8338 (HttpWebRequest_t1519 * __this, SerializationInfo_t1438 * ___serializationInfo, StreamingContext_t1439  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.HttpWebRequest::get_Address()
extern "C" Uri_t292 * HttpWebRequest_get_Address_m8339 (HttpWebRequest_t1519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.HttpWebRequest::get_ServicePoint()
extern "C" ServicePoint_t1521 * HttpWebRequest_get_ServicePoint_m8340 (HttpWebRequest_t1519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.HttpWebRequest::GetServicePoint()
extern "C" ServicePoint_t1521 * HttpWebRequest_GetServicePoint_m8341 (HttpWebRequest_t1519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpWebRequest::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HttpWebRequest_GetObjectData_m8342 (HttpWebRequest_t1519 * __this, SerializationInfo_t1438 * ___serializationInfo, StreamingContext_t1439  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
