﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// Popup
struct  Popup_t384  : public MonoBehaviour_t18
{
	// UnityEngine.Vector3 Popup::hiddenPosition
	Vector3_t6  ___hiddenPosition_1;
	// UnityEngine.Vector3 Popup::initialPosition
	Vector3_t6  ___initialPosition_2;
	// System.Single Popup::time
	float ___time_3;
	// System.Single Popup::delay
	float ___delay_4;
	// System.Boolean Popup::activated
	bool ___activated_5;
	// System.Boolean Popup::activate
	bool ___activate_6;
};
