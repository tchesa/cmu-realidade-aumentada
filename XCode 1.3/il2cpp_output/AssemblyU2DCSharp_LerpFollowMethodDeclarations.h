﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LerpFollow
struct LerpFollow_t382;

#include "codegen/il2cpp-codegen.h"

// System.Void LerpFollow::.ctor()
extern "C" void LerpFollow__ctor_m1959 (LerpFollow_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LerpFollow::Start()
extern "C" void LerpFollow_Start_m1960 (LerpFollow_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LerpFollow::Update()
extern "C" void LerpFollow_Update_m1961 (LerpFollow_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
