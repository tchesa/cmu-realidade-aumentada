﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2569;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C" void GenericComparer_1__ctor_m15435_gshared (GenericComparer_1_t2569 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m15435(__this, method) (( void (*) (GenericComparer_1_t2569 *, const MethodInfo*))GenericComparer_1__ctor_m15435_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m30114_gshared (GenericComparer_1_t2569 * __this, DateTime_t220  ___x, DateTime_t220  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m30114(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2569 *, DateTime_t220 , DateTime_t220 , const MethodInfo*))GenericComparer_1_Compare_m30114_gshared)(__this, ___x, ___y, method)
