﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseIOSFeaturePreview
struct BaseIOSFeaturePreview_t183;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseIOSFeaturePreview::.ctor()
extern "C" void BaseIOSFeaturePreview__ctor_m1035 (BaseIOSFeaturePreview_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseIOSFeaturePreview::InitStyles()
extern "C" void BaseIOSFeaturePreview_InitStyles_m1036 (BaseIOSFeaturePreview_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseIOSFeaturePreview::Start()
extern "C" void BaseIOSFeaturePreview_Start_m1037 (BaseIOSFeaturePreview_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseIOSFeaturePreview::UpdateToStartPos()
extern "C" void BaseIOSFeaturePreview_UpdateToStartPos_m1038 (BaseIOSFeaturePreview_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
