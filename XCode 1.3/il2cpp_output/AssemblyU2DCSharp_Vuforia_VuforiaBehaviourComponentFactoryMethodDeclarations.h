﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t419;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t425;
// UnityEngine.GameObject
struct GameObject_t27;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t449;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t440;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t415;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t423;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t427;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t401;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t457;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t438;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t429;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m2087 (VuforiaBehaviourComponentFactory_t419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t425 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m2088 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t449 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m2089 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t440 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m2090 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t415 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m2091 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t423 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m2092 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t427 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m2093 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t401 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m2094 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t457 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m2095 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t438 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2096 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t429 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m2097 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
