﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t3590;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m30136_gshared (DefaultComparer_t3590 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m30136(__this, method) (( void (*) (DefaultComparer_t3590 *, const MethodInfo*))DefaultComparer__ctor_m30136_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m30137_gshared (DefaultComparer_t3590 * __this, DateTimeOffset_t2423  ___x, DateTimeOffset_t2423  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m30137(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3590 *, DateTimeOffset_t2423 , DateTimeOffset_t2423 , const MethodInfo*))DefaultComparer_Compare_m30137_gshared)(__this, ___x, ___y, method)
