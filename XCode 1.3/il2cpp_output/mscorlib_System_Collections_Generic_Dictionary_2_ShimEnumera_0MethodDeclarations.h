﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t2643;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2632;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16191_gshared (ShimEnumerator_t2643 * __this, Dictionary_2_t2632 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16191(__this, ___host, method) (( void (*) (ShimEnumerator_t2643 *, Dictionary_2_t2632 *, const MethodInfo*))ShimEnumerator__ctor_m16191_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16192_gshared (ShimEnumerator_t2643 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16192(__this, method) (( bool (*) (ShimEnumerator_t2643 *, const MethodInfo*))ShimEnumerator_MoveNext_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t58  ShimEnumerator_get_Entry_m16193_gshared (ShimEnumerator_t2643 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16193(__this, method) (( DictionaryEntry_t58  (*) (ShimEnumerator_t2643 *, const MethodInfo*))ShimEnumerator_get_Entry_m16193_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16194_gshared (ShimEnumerator_t2643 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16194(__this, method) (( Object_t * (*) (ShimEnumerator_t2643 *, const MethodInfo*))ShimEnumerator_get_Key_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16195_gshared (ShimEnumerator_t2643 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16195(__this, method) (( Object_t * (*) (ShimEnumerator_t2643 *, const MethodInfo*))ShimEnumerator_get_Value_m16195_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16196_gshared (ShimEnumerator_t2643 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16196(__this, method) (( Object_t * (*) (ShimEnumerator_t2643 *, const MethodInfo*))ShimEnumerator_get_Current_m16196_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m16197_gshared (ShimEnumerator_t2643 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m16197(__this, method) (( void (*) (ShimEnumerator_t2643 *, const MethodInfo*))ShimEnumerator_Reset_m16197_gshared)(__this, method)
