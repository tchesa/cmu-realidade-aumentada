﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Uri
struct Uri_t292;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Uri Facebook.Unity.Constants::get_GraphUrl()
extern "C" Uri_t292 * Constants_get_GraphUrl_m1284 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Constants::get_GraphApiUserAgent()
extern "C" String_t* Constants_get_GraphApiUserAgent_m1285 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.Constants::get_IsMobile()
extern "C" bool Constants_get_IsMobile_m1286 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.Constants::get_IsEditor()
extern "C" bool Constants_get_IsEditor_m1287 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.Constants::get_IsWeb()
extern "C" bool Constants_get_IsWeb_m1288 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Constants::get_UnitySDKUserAgentSuffixLegacy()
extern "C" String_t* Constants_get_UnitySDKUserAgentSuffixLegacy_m1289 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Constants::get_UnitySDKUserAgent()
extern "C" String_t* Constants_get_UnitySDKUserAgent_m1290 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
