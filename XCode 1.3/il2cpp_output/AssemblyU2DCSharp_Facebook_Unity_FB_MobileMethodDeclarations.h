﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FB/Mobile
struct Mobile_t238;
// Facebook.Unity.Mobile.IMobileFacebook
struct IMobileFacebook_t480;
// System.Uri
struct Uri_t292;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppInviteResult>
struct FacebookDelegate_1_t479;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>
struct FacebookDelegate_1_t473;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode.h"

// System.Void Facebook.Unity.FB/Mobile::.ctor()
extern "C" void Mobile__ctor_m1294 (Mobile_t238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.ShareDialogMode Facebook.Unity.FB/Mobile::get_ShareDialogMode()
extern "C" int32_t Mobile_get_ShareDialogMode_m1295 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/Mobile::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern "C" void Mobile_set_ShareDialogMode_m1296 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.Mobile.IMobileFacebook Facebook.Unity.FB/Mobile::get_MobileFacebookImpl()
extern "C" Object_t * Mobile_get_MobileFacebookImpl_m1297 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/Mobile::AppInvite(System.Uri,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppInviteResult>)
extern "C" void Mobile_AppInvite_m1298 (Object_t * __this /* static, unused */, Uri_t292 * ___appLinkUrl, Uri_t292 * ___previewImageUrl, FacebookDelegate_1_t479 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/Mobile::FetchDeferredAppLinkData(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern "C" void Mobile_FetchDeferredAppLinkData_m1299 (Object_t * __this /* static, unused */, FacebookDelegate_1_t473 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
