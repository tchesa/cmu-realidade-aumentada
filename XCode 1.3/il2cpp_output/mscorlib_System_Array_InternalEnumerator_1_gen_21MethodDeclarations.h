﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_21.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18908_gshared (InternalEnumerator_1_t2812 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18908(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2812 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18908_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18909_gshared (InternalEnumerator_1_t2812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18909(__this, method) (( void (*) (InternalEnumerator_1_t2812 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18909_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18910_gshared (InternalEnumerator_1_t2812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18910(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2812 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18910_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18911_gshared (InternalEnumerator_1_t2812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18911(__this, method) (( void (*) (InternalEnumerator_1_t2812 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18911_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18912_gshared (InternalEnumerator_1_t2812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18912(__this, method) (( bool (*) (InternalEnumerator_1_t2812 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18912_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern "C" KeyValuePair_2_t2811  InternalEnumerator_1_get_Current_m18913_gshared (InternalEnumerator_1_t2812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18913(__this, method) (( KeyValuePair_2_t2811  (*) (InternalEnumerator_1_t2812 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18913_gshared)(__this, method)
