﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MeshCollider
struct MeshCollider_t918;
// UnityEngine.Mesh
struct Mesh_t601;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshCollider_set_sharedMesh_m6507 (MeshCollider_t918 * __this, Mesh_t601 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
