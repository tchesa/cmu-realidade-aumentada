﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// Vuforia.CloudRecoImageTargetImpl
struct  CloudRecoImageTargetImpl_t930  : public TrackableImpl_t904
{
	// UnityEngine.Vector3 Vuforia.CloudRecoImageTargetImpl::mSize
	Vector3_t6  ___mSize_2;
};
