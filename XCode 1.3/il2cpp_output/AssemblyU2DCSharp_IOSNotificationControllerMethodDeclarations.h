﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNotificationController
struct IOSNotificationController_t140;
// System.String
struct String_t;
// IOSNotificationDeviceToken
struct IOSNotificationDeviceToken_t143;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RemoteNotificationType.h"

// System.Void IOSNotificationController::.ctor()
extern "C" void IOSNotificationController__ctor_m787 (IOSNotificationController_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::.cctor()
extern "C" void IOSNotificationController__cctor_m788 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::_ISN_ScheduleNotification(System.Int32,System.String,System.Boolean,System.String,System.Int32)
extern "C" void IOSNotificationController__ISN_ScheduleNotification_m789 (Object_t * __this /* static, unused */, int32_t ___time, String_t* ___message, bool ___sound, String_t* ___nId, int32_t ___badges, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::_ISN_ShowNotificationBanner(System.String,System.String)
extern "C" void IOSNotificationController__ISN_ShowNotificationBanner_m790 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::_ISN_CancelNotifications()
extern "C" void IOSNotificationController__ISN_CancelNotifications_m791 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::_ISN_RequestNotificationPermissions()
extern "C" void IOSNotificationController__ISN_RequestNotificationPermissions_m792 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::_ISN_CancelNotificationById(System.String)
extern "C" void IOSNotificationController__ISN_CancelNotificationById_m793 (Object_t * __this /* static, unused */, String_t* ___nId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::_ISN_ApplicationIconBadgeNumber(System.Int32)
extern "C" void IOSNotificationController__ISN_ApplicationIconBadgeNumber_m794 (Object_t * __this /* static, unused */, int32_t ___badges, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::_ISN_RegisterForRemoteNotifications(System.Int32)
extern "C" void IOSNotificationController__ISN_RegisterForRemoteNotifications_m795 (Object_t * __this /* static, unused */, int32_t ___types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::Awake()
extern "C" void IOSNotificationController_Awake_m796 (IOSNotificationController_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::RegisterForRemoteNotifications(UnityEngine.RemoteNotificationType)
extern "C" void IOSNotificationController_RegisterForRemoteNotifications_m797 (IOSNotificationController_t140 * __this, int32_t ___notificationTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::RequestNotificationPermissions()
extern "C" void IOSNotificationController_RequestNotificationPermissions_m798 (IOSNotificationController_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::ShowNotificationBanner(System.String,System.String)
extern "C" void IOSNotificationController_ShowNotificationBanner_m799 (IOSNotificationController_t140 * __this, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::CancelNotifications()
extern "C" void IOSNotificationController_CancelNotifications_m800 (IOSNotificationController_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::CancelAllLocalNotifications()
extern "C" void IOSNotificationController_CancelAllLocalNotifications_m801 (IOSNotificationController_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::CancelLocalNotificationById(System.Int32)
extern "C" void IOSNotificationController_CancelLocalNotificationById_m802 (IOSNotificationController_t140 * __this, int32_t ___notificationId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSNotificationController::ScheduleNotification(System.Int32,System.String,System.Boolean,System.Int32)
extern "C" int32_t IOSNotificationController_ScheduleNotification_m803 (IOSNotificationController_t140 * __this, int32_t ___time, String_t* ___message, bool ___sound, int32_t ___badges, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::ApplicationIconBadgeNumber(System.Int32)
extern "C" void IOSNotificationController_ApplicationIconBadgeNumber_m804 (IOSNotificationController_t140 * __this, int32_t ___badges, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSNotificationController::get_GetNextId()
extern "C" int32_t IOSNotificationController_get_GetNextId_m805 (IOSNotificationController_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSNotificationController::get_AllowedNotificationsType()
extern "C" int32_t IOSNotificationController_get_AllowedNotificationsType_m806 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::OnDeviceTockeReceivedAction(IOSNotificationDeviceToken)
extern "C" void IOSNotificationController_OnDeviceTockeReceivedAction_m807 (IOSNotificationController_t140 * __this, IOSNotificationDeviceToken_t143 * ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::OnNotificationScheduleResultAction(System.String)
extern "C" void IOSNotificationController_OnNotificationScheduleResultAction_m808 (IOSNotificationController_t140 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::<OnDeviceTokenReceived>m__19(IOSNotificationDeviceToken)
extern "C" void IOSNotificationController_U3COnDeviceTokenReceivedU3Em__19_m809 (Object_t * __this /* static, unused */, IOSNotificationDeviceToken_t143 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationController::<OnNotificationScheduleResult>m__1A(ISN_Result)
extern "C" void IOSNotificationController_U3COnNotificationScheduleResultU3Em__1A_m810 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
