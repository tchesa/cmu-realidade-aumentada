﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t25;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// CFX_Demo_RotateCamera
struct  CFX_Demo_RotateCamera_t323  : public MonoBehaviour_t18
{
	// System.Single CFX_Demo_RotateCamera::speed
	float ___speed_2;
	// UnityEngine.Transform CFX_Demo_RotateCamera::rotationCenter
	Transform_t25 * ___rotationCenter_3;
};
struct CFX_Demo_RotateCamera_t323_StaticFields{
	// System.Boolean CFX_Demo_RotateCamera::rotating
	bool ___rotating_1;
};
