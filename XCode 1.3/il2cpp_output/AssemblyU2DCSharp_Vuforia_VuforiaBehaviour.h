﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t450;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha.h"

// Vuforia.VuforiaBehaviour
struct  VuforiaBehaviour_t450  : public VuforiaAbstractBehaviour_t451
{
};
struct VuforiaBehaviour_t450_StaticFields{
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::mVuforiaBehaviour
	VuforiaBehaviour_t450 * ___mVuforiaBehaviour_47;
};
