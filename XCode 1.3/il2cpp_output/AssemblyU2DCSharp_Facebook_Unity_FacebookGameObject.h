﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.IFacebookImplementation
struct IFacebookImplementation_t243;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Facebook.Unity.FacebookGameObject
struct  FacebookGameObject_t229  : public MonoBehaviour_t18
{
	// Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::<Facebook>k__BackingField
	Object_t * ___U3CFacebookU3Ek__BackingField_1;
	// System.Boolean Facebook.Unity.FacebookGameObject::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_2;
};
