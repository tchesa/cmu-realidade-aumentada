﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t724;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t3674;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t3672;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t866;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t2939;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t722;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t2940;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t2942;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m4218_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1__ctor_m4218(__this, method) (( void (*) (List_1_t724 *, const MethodInfo*))List_1__ctor_m4218_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m20972_gshared (List_1_t724 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m20972(__this, ___collection, method) (( void (*) (List_1_t724 *, Object_t*, const MethodInfo*))List_1__ctor_m20972_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m8141_gshared (List_1_t724 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m8141(__this, ___capacity, method) (( void (*) (List_1_t724 *, int32_t, const MethodInfo*))List_1__ctor_m8141_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m20973_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m20973(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m20973_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20974_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20974(__this, method) (( Object_t* (*) (List_1_t724 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m20975_gshared (List_1_t724 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m20975(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t724 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m20975_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m20976_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20976(__this, method) (( Object_t * (*) (List_1_t724 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m20976_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m20977_gshared (List_1_t724 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m20977(__this, ___item, method) (( int32_t (*) (List_1_t724 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m20977_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m20978_gshared (List_1_t724 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m20978(__this, ___item, method) (( bool (*) (List_1_t724 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m20978_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m20979_gshared (List_1_t724 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m20979(__this, ___item, method) (( int32_t (*) (List_1_t724 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m20979_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m20980_gshared (List_1_t724 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m20980(__this, ___index, ___item, method) (( void (*) (List_1_t724 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m20980_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m20981_gshared (List_1_t724 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m20981(__this, ___item, method) (( void (*) (List_1_t724 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m20981_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20982_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20982(__this, method) (( bool (*) (List_1_t724 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20982_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m20983_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20983(__this, method) (( bool (*) (List_1_t724 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m20983_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m20984_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m20984(__this, method) (( Object_t * (*) (List_1_t724 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m20984_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m20985_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m20985(__this, method) (( bool (*) (List_1_t724 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m20985_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m20986_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m20986(__this, method) (( bool (*) (List_1_t724 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m20986_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m20987_gshared (List_1_t724 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m20987(__this, ___index, method) (( Object_t * (*) (List_1_t724 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m20987_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m20988_gshared (List_1_t724 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m20988(__this, ___index, ___value, method) (( void (*) (List_1_t724 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m20988_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m20989_gshared (List_1_t724 * __this, UIVertex_t727  ___item, const MethodInfo* method);
#define List_1_Add_m20989(__this, ___item, method) (( void (*) (List_1_t724 *, UIVertex_t727 , const MethodInfo*))List_1_Add_m20989_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m20990_gshared (List_1_t724 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m20990(__this, ___newCount, method) (( void (*) (List_1_t724 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m20990_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m20991_gshared (List_1_t724 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m20991(__this, ___collection, method) (( void (*) (List_1_t724 *, Object_t*, const MethodInfo*))List_1_AddCollection_m20991_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m20992_gshared (List_1_t724 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m20992(__this, ___enumerable, method) (( void (*) (List_1_t724 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m20992_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m20993_gshared (List_1_t724 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m20993(__this, ___collection, method) (( void (*) (List_1_t724 *, Object_t*, const MethodInfo*))List_1_AddRange_m20993_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2939 * List_1_AsReadOnly_m20994_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m20994(__this, method) (( ReadOnlyCollection_1_t2939 * (*) (List_1_t724 *, const MethodInfo*))List_1_AsReadOnly_m20994_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m20995_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_Clear_m20995(__this, method) (( void (*) (List_1_t724 *, const MethodInfo*))List_1_Clear_m20995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m20996_gshared (List_1_t724 * __this, UIVertex_t727  ___item, const MethodInfo* method);
#define List_1_Contains_m20996(__this, ___item, method) (( bool (*) (List_1_t724 *, UIVertex_t727 , const MethodInfo*))List_1_Contains_m20996_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m20997_gshared (List_1_t724 * __this, UIVertexU5BU5D_t722* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m20997(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t724 *, UIVertexU5BU5D_t722*, int32_t, const MethodInfo*))List_1_CopyTo_m20997_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t727  List_1_Find_m20998_gshared (List_1_t724 * __this, Predicate_1_t2940 * ___match, const MethodInfo* method);
#define List_1_Find_m20998(__this, ___match, method) (( UIVertex_t727  (*) (List_1_t724 *, Predicate_1_t2940 *, const MethodInfo*))List_1_Find_m20998_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m20999_gshared (Object_t * __this /* static, unused */, Predicate_1_t2940 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m20999(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2940 *, const MethodInfo*))List_1_CheckMatch_m20999_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m21000_gshared (List_1_t724 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2940 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m21000(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t724 *, int32_t, int32_t, Predicate_1_t2940 *, const MethodInfo*))List_1_GetIndex_m21000_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t2941  List_1_GetEnumerator_m21001_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m21001(__this, method) (( Enumerator_t2941  (*) (List_1_t724 *, const MethodInfo*))List_1_GetEnumerator_m21001_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m21002_gshared (List_1_t724 * __this, UIVertex_t727  ___item, const MethodInfo* method);
#define List_1_IndexOf_m21002(__this, ___item, method) (( int32_t (*) (List_1_t724 *, UIVertex_t727 , const MethodInfo*))List_1_IndexOf_m21002_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m21003_gshared (List_1_t724 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m21003(__this, ___start, ___delta, method) (( void (*) (List_1_t724 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m21003_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m21004_gshared (List_1_t724 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m21004(__this, ___index, method) (( void (*) (List_1_t724 *, int32_t, const MethodInfo*))List_1_CheckIndex_m21004_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m21005_gshared (List_1_t724 * __this, int32_t ___index, UIVertex_t727  ___item, const MethodInfo* method);
#define List_1_Insert_m21005(__this, ___index, ___item, method) (( void (*) (List_1_t724 *, int32_t, UIVertex_t727 , const MethodInfo*))List_1_Insert_m21005_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m21006_gshared (List_1_t724 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m21006(__this, ___collection, method) (( void (*) (List_1_t724 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m21006_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m21007_gshared (List_1_t724 * __this, UIVertex_t727  ___item, const MethodInfo* method);
#define List_1_Remove_m21007(__this, ___item, method) (( bool (*) (List_1_t724 *, UIVertex_t727 , const MethodInfo*))List_1_Remove_m21007_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m21008_gshared (List_1_t724 * __this, Predicate_1_t2940 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m21008(__this, ___match, method) (( int32_t (*) (List_1_t724 *, Predicate_1_t2940 *, const MethodInfo*))List_1_RemoveAll_m21008_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m21009_gshared (List_1_t724 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m21009(__this, ___index, method) (( void (*) (List_1_t724 *, int32_t, const MethodInfo*))List_1_RemoveAt_m21009_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m21010_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_Reverse_m21010(__this, method) (( void (*) (List_1_t724 *, const MethodInfo*))List_1_Reverse_m21010_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m21011_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_Sort_m21011(__this, method) (( void (*) (List_1_t724 *, const MethodInfo*))List_1_Sort_m21011_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m21012_gshared (List_1_t724 * __this, Comparison_1_t2942 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m21012(__this, ___comparison, method) (( void (*) (List_1_t724 *, Comparison_1_t2942 *, const MethodInfo*))List_1_Sort_m21012_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t722* List_1_ToArray_m4278_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_ToArray_m4278(__this, method) (( UIVertexU5BU5D_t722* (*) (List_1_t724 *, const MethodInfo*))List_1_ToArray_m4278_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m21013_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m21013(__this, method) (( void (*) (List_1_t724 *, const MethodInfo*))List_1_TrimExcess_m21013_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m4156_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m4156(__this, method) (( int32_t (*) (List_1_t724 *, const MethodInfo*))List_1_get_Capacity_m4156_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m4157_gshared (List_1_t724 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m4157(__this, ___value, method) (( void (*) (List_1_t724 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4157_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m21014_gshared (List_1_t724 * __this, const MethodInfo* method);
#define List_1_get_Count_m21014(__this, method) (( int32_t (*) (List_1_t724 *, const MethodInfo*))List_1_get_Count_m21014_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t727  List_1_get_Item_m21015_gshared (List_1_t724 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m21015(__this, ___index, method) (( UIVertex_t727  (*) (List_1_t724 *, int32_t, const MethodInfo*))List_1_get_Item_m21015_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m21016_gshared (List_1_t724 * __this, int32_t ___index, UIVertex_t727  ___value, const MethodInfo* method);
#define List_1_set_Item_m21016(__this, ___index, ___value, method) (( void (*) (List_1_t724 *, int32_t, UIVertex_t727 , const MethodInfo*))List_1_set_Item_m21016_gshared)(__this, ___index, ___value, method)
