﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1289;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_65.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m28036_gshared (Enumerator_t3400 * __this, List_1_t1289 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m28036(__this, ___l, method) (( void (*) (Enumerator_t3400 *, List_1_t1289 *, const MethodInfo*))Enumerator__ctor_m28036_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m28037_gshared (Enumerator_t3400 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m28037(__this, method) (( void (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m28037_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28038_gshared (Enumerator_t3400 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m28038(__this, method) (( Object_t * (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28038_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m28039_gshared (Enumerator_t3400 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28039(__this, method) (( void (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_Dispose_m28039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m28040_gshared (Enumerator_t3400 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m28040(__this, method) (( void (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_VerifyState_m28040_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m28041_gshared (Enumerator_t3400 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m28041(__this, method) (( bool (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_MoveNext_m28041_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t857  Enumerator_get_Current_m28042_gshared (Enumerator_t3400 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m28042(__this, method) (( UILineInfo_t857  (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_get_Current_m28042_gshared)(__this, method)
