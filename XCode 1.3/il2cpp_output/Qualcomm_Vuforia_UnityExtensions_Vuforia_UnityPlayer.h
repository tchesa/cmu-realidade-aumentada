﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.IUnityPlayer
struct IUnityPlayer_t908;

#include "mscorlib_System_Object.h"

// Vuforia.UnityPlayer
struct  UnityPlayer_t907  : public Object_t
{
};
struct UnityPlayer_t907_StaticFields{
	// Vuforia.IUnityPlayer Vuforia.UnityPlayer::sPlayer
	Object_t * ___sPlayer_0;
};
