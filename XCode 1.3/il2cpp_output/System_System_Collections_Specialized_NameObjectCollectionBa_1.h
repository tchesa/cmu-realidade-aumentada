﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t1494;

#include "mscorlib_System_Object.h"

// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct  KeysCollection_t1495  : public Object_t
{
	// System.Collections.Specialized.NameObjectCollectionBase System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::m_collection
	NameObjectCollectionBase_t1494 * ___m_collection_0;
};
