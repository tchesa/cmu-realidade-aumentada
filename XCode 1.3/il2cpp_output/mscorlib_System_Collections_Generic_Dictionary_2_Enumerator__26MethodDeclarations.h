﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m22207(__this, ___dictionary, method) (( void (*) (Enumerator_t3027 *, Dictionary_2_t885 *, const MethodInfo*))Enumerator__ctor_m16145_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22208(__this, method) (( Object_t * (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16146_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m22209(__this, method) (( void (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16147_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22210(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16148_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22211(__this, method) (( Object_t * (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16149_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22212(__this, method) (( Object_t * (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m22213(__this, method) (( bool (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_MoveNext_m16151_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m22214(__this, method) (( KeyValuePair_2_t3025  (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_get_Current_m16152_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22215(__this, method) (( Camera_t510 * (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_get_CurrentKey_m16153_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22216(__this, method) (( VideoBackgroundAbstractBehaviour_t445 * (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_get_CurrentValue_m16154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::Reset()
#define Enumerator_Reset_m22217(__this, method) (( void (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_Reset_m16155_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m22218(__this, method) (( void (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_VerifyState_m16156_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22219(__this, method) (( void (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_VerifyCurrent_m16157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m22220(__this, method) (( void (*) (Enumerator_t3027 *, const MethodInfo*))Enumerator_Dispose_m16158_gshared)(__this, method)
