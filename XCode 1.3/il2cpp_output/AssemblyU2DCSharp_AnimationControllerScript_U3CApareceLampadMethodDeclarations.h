﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript/<ApareceLampada>c__IteratorF
struct U3CApareceLampadaU3Ec__IteratorF_t363;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript/<ApareceLampada>c__IteratorF::.ctor()
extern "C" void U3CApareceLampadaU3Ec__IteratorF__ctor_m1840 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<ApareceLampada>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CApareceLampadaU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1841 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationControllerScript/<ApareceLampada>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CApareceLampadaU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m1842 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationControllerScript/<ApareceLampada>c__IteratorF::MoveNext()
extern "C" bool U3CApareceLampadaU3Ec__IteratorF_MoveNext_m1843 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<ApareceLampada>c__IteratorF::Dispose()
extern "C" void U3CApareceLampadaU3Ec__IteratorF_Dispose_m1844 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript/<ApareceLampada>c__IteratorF::Reset()
extern "C" void U3CApareceLampadaU3Ec__IteratorF_Reset_m1845 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
