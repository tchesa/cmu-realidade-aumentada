﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t184;
// GCLeaderboard
struct GCLeaderboard_t121;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_GCCollectionType.h"

// LeaderboardCustomGUIExample
struct  LeaderboardCustomGUIExample_t189  : public MonoBehaviour_t18
{
	// System.String LeaderboardCustomGUIExample::leaderboardId
	String_t* ___leaderboardId_1;
	// System.Int32 LeaderboardCustomGUIExample::hiScore
	int32_t ___hiScore_2;
	// UnityEngine.GUIStyle LeaderboardCustomGUIExample::headerStyle
	GUIStyle_t184 * ___headerStyle_3;
	// UnityEngine.GUIStyle LeaderboardCustomGUIExample::boardStyle
	GUIStyle_t184 * ___boardStyle_4;
	// GCLeaderboard LeaderboardCustomGUIExample::loadedLeaderboard
	GCLeaderboard_t121 * ___loadedLeaderboard_5;
	// GCCollectionType LeaderboardCustomGUIExample::displayCollection
	int32_t ___displayCollection_6;
};
