﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterMultiplayer
struct GameCenterMultiplayer_t108;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.Object[]
struct ObjectU5BU5D_t34;
// GameCenterMatchData
struct GameCenterMatchData_t109;
// GameCenterDataPackage
struct GameCenterDataPackage_t123;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameCenterDataSendType.h"

// System.Void GameCenterMultiplayer::.ctor()
extern "C" void GameCenterMultiplayer__ctor_m647 (GameCenterMultiplayer_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::.cctor()
extern "C" void GameCenterMultiplayer__cctor_m648 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::_findMatch(System.Int32,System.Int32,System.Int32)
extern "C" void GameCenterMultiplayer__findMatch_m649 (Object_t * __this /* static, unused */, int32_t ___minPlayers, int32_t ___maxPlayers, int32_t ___playerGroup, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::_sendDataToAll(System.String,System.Int32)
extern "C" void GameCenterMultiplayer__sendDataToAll_m650 (Object_t * __this /* static, unused */, String_t* ___data, int32_t ___sendType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::_sendDataToPlayers(System.String,System.String,System.Int32)
extern "C" void GameCenterMultiplayer__sendDataToPlayers_m651 (Object_t * __this /* static, unused */, String_t* ___data, String_t* ___players, int32_t ___sendType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::_disconnect()
extern "C" void GameCenterMultiplayer__disconnect_m652 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameCenterMultiplayer GameCenterMultiplayer::get_instance()
extern "C" GameCenterMultiplayer_t108 * GameCenterMultiplayer_get_instance_m653 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::FindMatch(System.Int32,System.Int32,System.Int32)
extern "C" void GameCenterMultiplayer_FindMatch_m654 (GameCenterMultiplayer_t108 * __this, int32_t ___minPlayers, int32_t ___maxPlayers, int32_t ___playerGroup, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::SendDataToAll(System.Byte[],GameCenterDataSendType)
extern "C" void GameCenterMultiplayer_SendDataToAll_m655 (GameCenterMultiplayer_t108 * __this, ByteU5BU5D_t119* ___buffer, int32_t ___dataType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::sendDataToPlayers(System.Byte[],GameCenterDataSendType,System.Object[])
extern "C" void GameCenterMultiplayer_sendDataToPlayers_m656 (GameCenterMultiplayer_t108 * __this, ByteU5BU5D_t119* ___buffer, int32_t ___dataType, ObjectU5BU5D_t34* ___players, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::disconnect()
extern "C" void GameCenterMultiplayer_disconnect_m657 (GameCenterMultiplayer_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameCenterMatchData GameCenterMultiplayer::get_match()
extern "C" GameCenterMatchData_t109 * GameCenterMultiplayer_get_match_m658 (GameCenterMultiplayer_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::OnGameCenterPlayerConnected(System.String)
extern "C" void GameCenterMultiplayer_OnGameCenterPlayerConnected_m659 (GameCenterMultiplayer_t108 * __this, String_t* ___playerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::OnGameCenterPlayerDisconnected(System.String)
extern "C" void GameCenterMultiplayer_OnGameCenterPlayerDisconnected_m660 (GameCenterMultiplayer_t108 * __this, String_t* ___playerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::OnGameCenterMatchStarted(System.String)
extern "C" void GameCenterMultiplayer_OnGameCenterMatchStarted_m661 (GameCenterMultiplayer_t108 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::OnMatchDataReceived(System.String)
extern "C" void GameCenterMultiplayer_OnMatchDataReceived_m662 (GameCenterMultiplayer_t108 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::<OnPlayerConnected>m__10(System.String)
extern "C" void GameCenterMultiplayer_U3COnPlayerConnectedU3Em__10_m663 (Object_t * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::<OnPlayerDisconnected>m__11(System.String)
extern "C" void GameCenterMultiplayer_U3COnPlayerDisconnectedU3Em__11_m664 (Object_t * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::<OnMatchStarted>m__12(GameCenterMatchData)
extern "C" void GameCenterMultiplayer_U3COnMatchStartedU3Em__12_m665 (Object_t * __this /* static, unused */, GameCenterMatchData_t109 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterMultiplayer::<OnDataReceived>m__13(GameCenterDataPackage)
extern "C" void GameCenterMultiplayer_U3COnDataReceivedU3Em__13_m666 (Object_t * __this /* static, unused */, GameCenterDataPackage_t123 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
