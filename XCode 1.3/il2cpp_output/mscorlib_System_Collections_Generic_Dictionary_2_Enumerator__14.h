﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,GameCenterPlayerTemplate>
struct Dictionary_2_t106;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>
struct  Enumerator_t2700 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::dictionary
	Dictionary_2_t106 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GameCenterPlayerTemplate>::current
	KeyValuePair_2_t2697  ___current_3;
};
