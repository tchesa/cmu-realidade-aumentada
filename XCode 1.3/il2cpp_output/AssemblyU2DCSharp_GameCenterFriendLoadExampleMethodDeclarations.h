﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterFriendLoadExample
struct GameCenterFriendLoadExample_t188;
// UnionAssets.FLE.CEvent
struct CEvent_t74;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void GameCenterFriendLoadExample::.ctor()
extern "C" void GameCenterFriendLoadExample__ctor_m1069 (GameCenterFriendLoadExample_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterFriendLoadExample::Awake()
extern "C" void GameCenterFriendLoadExample_Awake_m1070 (GameCenterFriendLoadExample_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterFriendLoadExample::OnGUI()
extern "C" void GameCenterFriendLoadExample_OnGUI_m1071 (GameCenterFriendLoadExample_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterFriendLoadExample::OnAuth(UnionAssets.FLE.CEvent)
extern "C" void GameCenterFriendLoadExample_OnAuth_m1072 (GameCenterFriendLoadExample_t188 * __this, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterFriendLoadExample::OnFriendsListLoaded(ISN_Result)
extern "C" void GameCenterFriendLoadExample_OnFriendsListLoaded_m1073 (GameCenterFriendLoadExample_t188 * __this, ISN_Result_t114 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
