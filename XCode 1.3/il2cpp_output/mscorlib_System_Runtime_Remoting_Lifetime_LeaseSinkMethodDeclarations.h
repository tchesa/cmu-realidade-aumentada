﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Lifetime.LeaseSink
struct LeaseSink_t2196;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t2184;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Lifetime.LeaseSink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void LeaseSink__ctor_m13286 (LeaseSink_t2196 * __this, Object_t * ___nextSink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
