﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t45;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Vuforia.MaskOutAbstractBehaviour
struct  MaskOutAbstractBehaviour_t425  : public MonoBehaviour_t18
{
	// UnityEngine.Material Vuforia.MaskOutAbstractBehaviour::maskMaterial
	Material_t45 * ___maskMaterial_1;
};
