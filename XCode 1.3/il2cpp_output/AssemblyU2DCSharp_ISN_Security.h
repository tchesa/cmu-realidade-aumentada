﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<ISN_LocalReceiptResult>
struct Action_1_t159;
// System.Action`1<ISN_DeviceGUID>
struct Action_1_t160;
// System.Action`1<ISN_Result>
struct Action_1_t99;

#include "AssemblyU2DCSharp_ISN_Singleton_1_gen_3.h"

// ISN_Security
struct  ISN_Security_t157  : public ISN_Singleton_1_t158
{
};
struct ISN_Security_t157_StaticFields{
	// System.Action`1<ISN_LocalReceiptResult> ISN_Security::OnReceiptLoaded
	Action_1_t159 * ___OnReceiptLoaded_5;
	// System.Action`1<ISN_DeviceGUID> ISN_Security::OnGUIDLoaded
	Action_1_t160 * ___OnGUIDLoaded_6;
	// System.Action`1<ISN_Result> ISN_Security::OnReceiptRefreshComplete
	Action_1_t99 * ___OnReceiptRefreshComplete_7;
	// System.Action`1<ISN_LocalReceiptResult> ISN_Security::<>f__am$cache3
	Action_1_t159 * ___U3CU3Ef__amU24cache3_8;
	// System.Action`1<ISN_DeviceGUID> ISN_Security::<>f__am$cache4
	Action_1_t160 * ___U3CU3Ef__amU24cache4_9;
	// System.Action`1<ISN_Result> ISN_Security::<>f__am$cache5
	Action_1_t99 * ___U3CU3Ef__amU24cache5_10;
};
