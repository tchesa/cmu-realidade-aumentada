﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<IOSProductTemplate>
struct List_1_t131;
// IOSProductTemplate
struct IOSProductTemplate_t135;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<IOSProductTemplate>
struct  Enumerator_t498 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<IOSProductTemplate>::l
	List_1_t131 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<IOSProductTemplate>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<IOSProductTemplate>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<IOSProductTemplate>::current
	IOSProductTemplate_t135 * ___current_3;
};
