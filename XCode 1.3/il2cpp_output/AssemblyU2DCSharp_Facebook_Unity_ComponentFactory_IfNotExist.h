﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ComponentFactory_IfNotExist.h"

// Facebook.Unity.ComponentFactory/IfNotExist
struct  IfNotExist_t234 
{
	// System.Int32 Facebook.Unity.ComponentFactory/IfNotExist::value__
	int32_t ___value___1;
};
