﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// iCloudManager
struct iCloudManager_t180;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// ISN_Singleton`1<iCloudManager>
struct  ISN_Singleton_1_t181  : public EventDispatcher_t69
{
};
struct ISN_Singleton_1_t181_StaticFields{
	// T ISN_Singleton`1<iCloudManager>::_instance
	iCloudManager_t180 * ____instance_3;
	// System.Boolean ISN_Singleton`1<iCloudManager>::applicationIsQuitting
	bool ___applicationIsQuitting_4;
};
