﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_65.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26651_gshared (InternalEnumerator_1_t3294 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26651(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3294 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26651_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26652_gshared (InternalEnumerator_1_t3294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26652(__this, method) (( void (*) (InternalEnumerator_1_t3294 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m26652_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26653_gshared (InternalEnumerator_1_t3294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26653(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3294 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26653_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26654_gshared (InternalEnumerator_1_t3294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26654(__this, method) (( void (*) (InternalEnumerator_1_t3294 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26654_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26655_gshared (InternalEnumerator_1_t3294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26655(__this, method) (( bool (*) (InternalEnumerator_1_t3294 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26655_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" ProfileData_t1064  InternalEnumerator_1_get_Current_m26656_gshared (InternalEnumerator_1_t3294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26656(__this, method) (( ProfileData_t1064  (*) (InternalEnumerator_1_t3294 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26656_gshared)(__this, method)
