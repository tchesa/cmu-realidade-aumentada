﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m20862(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2934 *, Font_t684 *, List_1_t842 *, const MethodInfo*))KeyValuePair_2__ctor_m16119_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define KeyValuePair_2_get_Key_m20863(__this, method) (( Font_t684 * (*) (KeyValuePair_2_t2934 *, const MethodInfo*))KeyValuePair_2_get_Key_m16120_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20864(__this, ___value, method) (( void (*) (KeyValuePair_2_t2934 *, Font_t684 *, const MethodInfo*))KeyValuePair_2_set_Key_m16121_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define KeyValuePair_2_get_Value_m20865(__this, method) (( List_1_t842 * (*) (KeyValuePair_2_t2934 *, const MethodInfo*))KeyValuePair_2_get_Value_m16122_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20866(__this, ___value, method) (( void (*) (KeyValuePair_2_t2934 *, List_1_t842 *, const MethodInfo*))KeyValuePair_2_set_Value_m16123_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToString()
#define KeyValuePair_2_ToString_m20867(__this, method) (( String_t* (*) (KeyValuePair_2_t2934 *, const MethodInfo*))KeyValuePair_2_ToString_m16124_gshared)(__this, method)
