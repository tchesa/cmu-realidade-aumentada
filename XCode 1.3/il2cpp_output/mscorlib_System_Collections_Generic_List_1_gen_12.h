﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARAnimation/TransparencyTrigger[]
struct TransparencyTriggerU5BU5D_t346;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger>
struct  List_1_t347  : public Object_t
{
	// T[] System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger>::_items
	TransparencyTriggerU5BU5D_t346* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger>::_version
	int32_t ____version_3;
};
struct List_1_t347_StaticFields{
	// T[] System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger>::EmptyArray
	TransparencyTriggerU5BU5D_t346* ___EmptyArray_4;
};
