﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.SinkProviderData
struct SinkProviderData_t2180;
// System.String
struct String_t;
// System.Collections.IList
struct IList_t488;
// System.Collections.IDictionary
struct IDictionary_t487;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.SinkProviderData::.ctor(System.String)
extern "C" void SinkProviderData__ctor_m13218 (SinkProviderData_t2180 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Runtime.Remoting.Channels.SinkProviderData::get_Children()
extern "C" Object_t * SinkProviderData_get_Children_m13219 (SinkProviderData_t2180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Channels.SinkProviderData::get_Properties()
extern "C" Object_t * SinkProviderData_get_Properties_m13220 (SinkProviderData_t2180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
