﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// RotateScale
struct  RotateScale_t385  : public MonoBehaviour_t18
{
	// UnityEngine.Vector3 RotateScale::rotation
	Vector3_t6  ___rotation_1;
	// System.Single RotateScale::scale
	float ___scale_2;
	// UnityEngine.Vector3 RotateScale::_rotation
	Vector3_t6  ____rotation_3;
	// System.Single RotateScale::_scale
	float ____scale_4;
	// System.Boolean RotateScale::isPlaying
	bool ___isPlaying_5;
};
