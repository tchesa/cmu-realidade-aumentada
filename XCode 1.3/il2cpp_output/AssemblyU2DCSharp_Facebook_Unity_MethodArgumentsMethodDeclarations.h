﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.MethodArguments
struct MethodArguments_t248;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t225;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t221;
// System.Uri
struct Uri_t292;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t288;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.MethodArguments::.ctor()
extern "C" void MethodArguments__ctor_m1398 (MethodArguments_t248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.MethodArguments::.ctor(Facebook.Unity.MethodArguments)
extern "C" void MethodArguments__ctor_m1399 (MethodArguments_t248 * __this, MethodArguments_t248 * ___methodArgs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.MethodArguments::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C" void MethodArguments__ctor_m1400 (MethodArguments_t248 * __this, Object_t* ___arguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.MethodArguments::AddString(System.String,System.String)
extern "C" void MethodArguments_AddString_m1401 (MethodArguments_t248 * __this, String_t* ___argumentName, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.MethodArguments::AddCommaSeparatedList(System.String,System.Collections.Generic.IEnumerable`1<System.String>)
extern "C" void MethodArguments_AddCommaSeparatedList_m1402 (MethodArguments_t248 * __this, String_t* ___argumentName, Object_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.MethodArguments::AddDictionary(System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C" void MethodArguments_AddDictionary_m1403 (MethodArguments_t248 * __this, String_t* ___argumentName, Object_t* ___dict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.MethodArguments::AddUri(System.String,System.Uri)
extern "C" void MethodArguments_AddUri_m1404 (MethodArguments_t248 * __this, String_t* ___argumentName, Uri_t292 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.MethodArguments::ToJsonString()
extern "C" String_t* MethodArguments_ToJsonString_m1405 (MethodArguments_t248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.Unity.MethodArguments::ToStringDict(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C" Dictionary_2_t288 * MethodArguments_ToStringDict_m1406 (Object_t * __this /* static, unused */, Object_t* ___dict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
