﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t672;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Button/ButtonClickedEvent::.ctor()
extern "C" void ButtonClickedEvent__ctor_m3105 (ButtonClickedEvent_t672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
