﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationController/<IPlay>c__IteratorD
struct U3CIPlayU3Ec__IteratorD_t356;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationController/<IPlay>c__IteratorD::.ctor()
extern "C" void U3CIPlayU3Ec__IteratorD__ctor_m1818 (U3CIPlayU3Ec__IteratorD_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationController/<IPlay>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CIPlayU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1819 (U3CIPlayU3Ec__IteratorD_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnimationController/<IPlay>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CIPlayU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1820 (U3CIPlayU3Ec__IteratorD_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationController/<IPlay>c__IteratorD::MoveNext()
extern "C" bool U3CIPlayU3Ec__IteratorD_MoveNext_m1821 (U3CIPlayU3Ec__IteratorD_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController/<IPlay>c__IteratorD::Dispose()
extern "C" void U3CIPlayU3Ec__IteratorD_Dispose_m1822 (U3CIPlayU3Ec__IteratorD_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationController/<IPlay>c__IteratorD::Reset()
extern "C" void U3CIPlayU3Ec__IteratorD_Reset_m1823 (U3CIPlayU3Ec__IteratorD_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
