﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t486;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t476;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t554;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3602;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2578;
// System.Object[]
struct ObjectU5BU5D_t34;
// System.Predicate`1<System.Object>
struct Predicate_1_t2585;
// System.Comparison`1<System.Object>
struct Comparison_1_t2591;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m2529_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1__ctor_m2529(__this, method) (( void (*) (List_1_t486 *, const MethodInfo*))List_1__ctor_m2529_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m15449_gshared (List_1_t486 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m15449(__this, ___collection, method) (( void (*) (List_1_t486 *, Object_t*, const MethodInfo*))List_1__ctor_m15449_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m15451_gshared (List_1_t486 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m15451(__this, ___capacity, method) (( void (*) (List_1_t486 *, int32_t, const MethodInfo*))List_1__ctor_m15451_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m15453_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m15453(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15453_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15455_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15455(__this, method) (( Object_t* (*) (List_1_t486 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15457_gshared (List_1_t486 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m15457(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t486 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15457_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m15459_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15459(__this, method) (( Object_t * (*) (List_1_t486 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15459_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m15461_gshared (List_1_t486 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m15461(__this, ___item, method) (( int32_t (*) (List_1_t486 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m15461_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m15463_gshared (List_1_t486 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m15463(__this, ___item, method) (( bool (*) (List_1_t486 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m15463_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m15465_gshared (List_1_t486 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m15465(__this, ___item, method) (( int32_t (*) (List_1_t486 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m15465_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m15467_gshared (List_1_t486 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m15467(__this, ___index, ___item, method) (( void (*) (List_1_t486 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m15467_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m15469_gshared (List_1_t486 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m15469(__this, ___item, method) (( void (*) (List_1_t486 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m15469_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15471_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15471(__this, method) (( bool (*) (List_1_t486 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15471_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m15473_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15473(__this, method) (( bool (*) (List_1_t486 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15473_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m15475_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m15475(__this, method) (( Object_t * (*) (List_1_t486 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m15477_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m15477(__this, method) (( bool (*) (List_1_t486 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15477_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m15479_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m15479(__this, method) (( bool (*) (List_1_t486 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15479_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m15481_gshared (List_1_t486 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m15481(__this, ___index, method) (( Object_t * (*) (List_1_t486 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m15481_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m15483_gshared (List_1_t486 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m15483(__this, ___index, ___value, method) (( void (*) (List_1_t486 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m15483_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m15485_gshared (List_1_t486 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m15485(__this, ___item, method) (( void (*) (List_1_t486 *, Object_t *, const MethodInfo*))List_1_Add_m15485_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m15487_gshared (List_1_t486 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m15487(__this, ___newCount, method) (( void (*) (List_1_t486 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15487_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m15489_gshared (List_1_t486 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m15489(__this, ___collection, method) (( void (*) (List_1_t486 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15489_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m15491_gshared (List_1_t486 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m15491(__this, ___enumerable, method) (( void (*) (List_1_t486 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15491_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m15493_gshared (List_1_t486 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m15493(__this, ___collection, method) (( void (*) (List_1_t486 *, Object_t*, const MethodInfo*))List_1_AddRange_m15493_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2578 * List_1_AsReadOnly_m15495_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m15495(__this, method) (( ReadOnlyCollection_1_t2578 * (*) (List_1_t486 *, const MethodInfo*))List_1_AsReadOnly_m15495_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m15497_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_Clear_m15497(__this, method) (( void (*) (List_1_t486 *, const MethodInfo*))List_1_Clear_m15497_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m15499_gshared (List_1_t486 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m15499(__this, ___item, method) (( bool (*) (List_1_t486 *, Object_t *, const MethodInfo*))List_1_Contains_m15499_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m15501_gshared (List_1_t486 * __this, ObjectU5BU5D_t34* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m15501(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t486 *, ObjectU5BU5D_t34*, int32_t, const MethodInfo*))List_1_CopyTo_m15501_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m15503_gshared (List_1_t486 * __this, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define List_1_Find_m15503(__this, ___match, method) (( Object_t * (*) (List_1_t486 *, Predicate_1_t2585 *, const MethodInfo*))List_1_Find_m15503_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m15505_gshared (Object_t * __this /* static, unused */, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m15505(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2585 *, const MethodInfo*))List_1_CheckMatch_m15505_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m15507_gshared (List_1_t486 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m15507(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t486 *, int32_t, int32_t, Predicate_1_t2585 *, const MethodInfo*))List_1_GetIndex_m15507_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t552  List_1_GetEnumerator_m2487_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2487(__this, method) (( Enumerator_t552  (*) (List_1_t486 *, const MethodInfo*))List_1_GetEnumerator_m2487_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m15509_gshared (List_1_t486 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m15509(__this, ___item, method) (( int32_t (*) (List_1_t486 *, Object_t *, const MethodInfo*))List_1_IndexOf_m15509_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m15511_gshared (List_1_t486 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m15511(__this, ___start, ___delta, method) (( void (*) (List_1_t486 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15511_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m15513_gshared (List_1_t486 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m15513(__this, ___index, method) (( void (*) (List_1_t486 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15513_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m15515_gshared (List_1_t486 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m15515(__this, ___index, ___item, method) (( void (*) (List_1_t486 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m15515_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m15517_gshared (List_1_t486 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m15517(__this, ___collection, method) (( void (*) (List_1_t486 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15517_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m15519_gshared (List_1_t486 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m15519(__this, ___item, method) (( bool (*) (List_1_t486 *, Object_t *, const MethodInfo*))List_1_Remove_m15519_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m15521_gshared (List_1_t486 * __this, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m15521(__this, ___match, method) (( int32_t (*) (List_1_t486 *, Predicate_1_t2585 *, const MethodInfo*))List_1_RemoveAll_m15521_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m15523_gshared (List_1_t486 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m15523(__this, ___index, method) (( void (*) (List_1_t486 *, int32_t, const MethodInfo*))List_1_RemoveAt_m15523_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m15525_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_Reverse_m15525(__this, method) (( void (*) (List_1_t486 *, const MethodInfo*))List_1_Reverse_m15525_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m15527_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_Sort_m15527(__this, method) (( void (*) (List_1_t486 *, const MethodInfo*))List_1_Sort_m15527_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m15529_gshared (List_1_t486 * __this, Comparison_1_t2591 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m15529(__this, ___comparison, method) (( void (*) (List_1_t486 *, Comparison_1_t2591 *, const MethodInfo*))List_1_Sort_m15529_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t34* List_1_ToArray_m15531_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_ToArray_m15531(__this, method) (( ObjectU5BU5D_t34* (*) (List_1_t486 *, const MethodInfo*))List_1_ToArray_m15531_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m15533_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m15533(__this, method) (( void (*) (List_1_t486 *, const MethodInfo*))List_1_TrimExcess_m15533_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m15535_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m15535(__this, method) (( int32_t (*) (List_1_t486 *, const MethodInfo*))List_1_get_Capacity_m15535_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15537_gshared (List_1_t486 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m15537(__this, ___value, method) (( void (*) (List_1_t486 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15537_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m15539_gshared (List_1_t486 * __this, const MethodInfo* method);
#define List_1_get_Count_m15539(__this, method) (( int32_t (*) (List_1_t486 *, const MethodInfo*))List_1_get_Count_m15539_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m15541_gshared (List_1_t486 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m15541(__this, ___index, method) (( Object_t * (*) (List_1_t486 *, int32_t, const MethodInfo*))List_1_get_Item_m15541_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m15543_gshared (List_1_t486 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m15543(__this, ___index, ___value, method) (( void (*) (List_1_t486 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m15543_gshared)(__this, ___index, ___value, method)
