﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSRateUsPopUp
struct IOSRateUsPopUp_t167;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_IOSDialogResult.h"

// System.Void IOSRateUsPopUp::.ctor()
extern "C" void IOSRateUsPopUp__ctor_m890 (IOSRateUsPopUp_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSRateUsPopUp IOSRateUsPopUp::Create()
extern "C" IOSRateUsPopUp_t167 * IOSRateUsPopUp_Create_m891 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSRateUsPopUp IOSRateUsPopUp::Create(System.String,System.String)
extern "C" IOSRateUsPopUp_t167 * IOSRateUsPopUp_Create_m892 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSRateUsPopUp IOSRateUsPopUp::Create(System.String,System.String,System.String,System.String,System.String)
extern "C" IOSRateUsPopUp_t167 * IOSRateUsPopUp_Create_m893 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSRateUsPopUp::init()
extern "C" void IOSRateUsPopUp_init_m894 (IOSRateUsPopUp_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSRateUsPopUp::onPopUpCallBack(System.String)
extern "C" void IOSRateUsPopUp_onPopUpCallBack_m895 (IOSRateUsPopUp_t167 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSRateUsPopUp::<OnComplete>m__25(IOSDialogResult)
extern "C" void IOSRateUsPopUp_U3COnCompleteU3Em__25_m896 (Object_t * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
