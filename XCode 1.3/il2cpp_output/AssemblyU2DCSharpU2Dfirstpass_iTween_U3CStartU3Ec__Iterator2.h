﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// iTween
struct iTween_t15;

#include "mscorlib_System_Object.h"

// iTween/<Start>c__Iterator2
struct  U3CStartU3Ec__Iterator2_t17  : public Object_t
{
	// System.Int32 iTween/<Start>c__Iterator2::$PC
	int32_t ___U24PC_0;
	// System.Object iTween/<Start>c__Iterator2::$current
	Object_t * ___U24current_1;
	// iTween iTween/<Start>c__Iterator2::<>f__this
	iTween_t15 * ___U3CU3Ef__this_2;
};
