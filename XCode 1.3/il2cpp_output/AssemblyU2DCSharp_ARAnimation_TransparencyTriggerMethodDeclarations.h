﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARAnimation/TransparencyTrigger
struct TransparencyTrigger_t339;

#include "codegen/il2cpp-codegen.h"

// System.Void ARAnimation/TransparencyTrigger::.ctor()
extern "C" void TransparencyTrigger__ctor_m1807 (TransparencyTrigger_t339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
