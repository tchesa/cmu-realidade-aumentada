﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,GCScore>
struct Dictionary_2_t124;

#include "mscorlib_System_Object.h"

// ScoreCollection
struct  ScoreCollection_t122  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<System.Int32,GCScore> ScoreCollection::AllTimeScores
	Dictionary_2_t124 * ___AllTimeScores_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,GCScore> ScoreCollection::WeekScores
	Dictionary_2_t124 * ___WeekScores_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,GCScore> ScoreCollection::TodayScores
	Dictionary_2_t124 * ___TodayScores_2;
};
