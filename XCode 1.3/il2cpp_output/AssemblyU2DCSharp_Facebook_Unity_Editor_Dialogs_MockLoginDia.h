﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct MockLoginDialog_t273;

#include "mscorlib_System_Object.h"

// Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey1A
struct  U3CSendSuccessResultU3Ec__AnonStorey1A_t272  : public Object_t
{
	// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey1A::facebookID
	String_t* ___facebookID_0;
	// Facebook.Unity.Editor.Dialogs.MockLoginDialog Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey1A::<>f__this
	MockLoginDialog_t273 * ___U3CU3Ef__this_1;
};
