﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1210;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2650;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t3728;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>
struct IEnumerator_1_t3729;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct KeyCollection_t3258;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct ValueCollection_t3262;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_40.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__40.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor()
extern "C" void Dictionary_2__ctor_m6744_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m6744(__this, method) (( void (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2__ctor_m6744_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m26061_gshared (Dictionary_2_t1210 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m26061(__this, ___comparer, method) (( void (*) (Dictionary_2_t1210 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m26061_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m26062_gshared (Dictionary_2_t1210 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m26062(__this, ___capacity, method) (( void (*) (Dictionary_2_t1210 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m26062_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m26063_gshared (Dictionary_2_t1210 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m26063(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1210 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m26063_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m26064_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m26064(__this, method) (( Object_t * (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m26064_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m26065_gshared (Dictionary_2_t1210 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m26065(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1210 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m26065_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m26066_gshared (Dictionary_2_t1210 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m26066(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1210 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m26066_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m26067_gshared (Dictionary_2_t1210 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m26067(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1210 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m26067_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m26068_gshared (Dictionary_2_t1210 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m26068(__this, ___key, method) (( bool (*) (Dictionary_2_t1210 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m26068_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m26069_gshared (Dictionary_2_t1210 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m26069(__this, ___key, method) (( void (*) (Dictionary_2_t1210 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m26069_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26070_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26070(__this, method) (( bool (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26070_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26071_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26071(__this, method) (( Object_t * (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26071_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26072_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26072(__this, method) (( bool (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26072_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26073_gshared (Dictionary_2_t1210 * __this, KeyValuePair_2_t3255  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26073(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1210 *, KeyValuePair_2_t3255 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26073_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26074_gshared (Dictionary_2_t1210 * __this, KeyValuePair_2_t3255  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26074(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1210 *, KeyValuePair_2_t3255 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26074_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26075_gshared (Dictionary_2_t1210 * __this, KeyValuePair_2U5BU5D_t3728* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26075(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1210 *, KeyValuePair_2U5BU5D_t3728*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26075_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26076_gshared (Dictionary_2_t1210 * __this, KeyValuePair_2_t3255  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26076(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1210 *, KeyValuePair_2_t3255 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26076_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m26077_gshared (Dictionary_2_t1210 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m26077(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1210 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m26077_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26078_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26078(__this, method) (( Object_t * (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26078_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26079_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26079(__this, method) (( Object_t* (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26079_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26080_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26080(__this, method) (( Object_t * (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26080_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m26081_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m26081(__this, method) (( int32_t (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_get_Count_m26081_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Item(TKey)
extern "C" VirtualButtonData_t970  Dictionary_2_get_Item_m26082_gshared (Dictionary_2_t1210 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m26082(__this, ___key, method) (( VirtualButtonData_t970  (*) (Dictionary_2_t1210 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m26082_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m26083_gshared (Dictionary_2_t1210 * __this, int32_t ___key, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m26083(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1210 *, int32_t, VirtualButtonData_t970 , const MethodInfo*))Dictionary_2_set_Item_m26083_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m26084_gshared (Dictionary_2_t1210 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m26084(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1210 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m26084_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m26085_gshared (Dictionary_2_t1210 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m26085(__this, ___size, method) (( void (*) (Dictionary_2_t1210 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m26085_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m26086_gshared (Dictionary_2_t1210 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m26086(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1210 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m26086_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3255  Dictionary_2_make_pair_m26087_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m26087(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3255  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t970 , const MethodInfo*))Dictionary_2_make_pair_m26087_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m26088_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m26088(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t970 , const MethodInfo*))Dictionary_2_pick_key_m26088_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::pick_value(TKey,TValue)
extern "C" VirtualButtonData_t970  Dictionary_2_pick_value_m26089_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m26089(__this /* static, unused */, ___key, ___value, method) (( VirtualButtonData_t970  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t970 , const MethodInfo*))Dictionary_2_pick_value_m26089_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m26090_gshared (Dictionary_2_t1210 * __this, KeyValuePair_2U5BU5D_t3728* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m26090(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1210 *, KeyValuePair_2U5BU5D_t3728*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m26090_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Resize()
extern "C" void Dictionary_2_Resize_m26091_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m26091(__this, method) (( void (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_Resize_m26091_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m26092_gshared (Dictionary_2_t1210 * __this, int32_t ___key, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m26092(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1210 *, int32_t, VirtualButtonData_t970 , const MethodInfo*))Dictionary_2_Add_m26092_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Clear()
extern "C" void Dictionary_2_Clear_m26093_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m26093(__this, method) (( void (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_Clear_m26093_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m26094_gshared (Dictionary_2_t1210 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m26094(__this, ___key, method) (( bool (*) (Dictionary_2_t1210 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m26094_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m26095_gshared (Dictionary_2_t1210 * __this, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m26095(__this, ___value, method) (( bool (*) (Dictionary_2_t1210 *, VirtualButtonData_t970 , const MethodInfo*))Dictionary_2_ContainsValue_m26095_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m26096_gshared (Dictionary_2_t1210 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m26096(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1210 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m26096_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m26097_gshared (Dictionary_2_t1210 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m26097(__this, ___sender, method) (( void (*) (Dictionary_2_t1210 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m26097_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m26098_gshared (Dictionary_2_t1210 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m26098(__this, ___key, method) (( bool (*) (Dictionary_2_t1210 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m26098_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m26099_gshared (Dictionary_2_t1210 * __this, int32_t ___key, VirtualButtonData_t970 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m26099(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1210 *, int32_t, VirtualButtonData_t970 *, const MethodInfo*))Dictionary_2_TryGetValue_m26099_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Keys()
extern "C" KeyCollection_t3258 * Dictionary_2_get_Keys_m26100_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m26100(__this, method) (( KeyCollection_t3258 * (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_get_Keys_m26100_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Values()
extern "C" ValueCollection_t3262 * Dictionary_2_get_Values_m26101_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m26101(__this, method) (( ValueCollection_t3262 * (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_get_Values_m26101_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m26102_gshared (Dictionary_2_t1210 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m26102(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1210 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m26102_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToTValue(System.Object)
extern "C" VirtualButtonData_t970  Dictionary_2_ToTValue_m26103_gshared (Dictionary_2_t1210 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m26103(__this, ___value, method) (( VirtualButtonData_t970  (*) (Dictionary_2_t1210 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m26103_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m26104_gshared (Dictionary_2_t1210 * __this, KeyValuePair_2_t3255  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m26104(__this, ___pair, method) (( bool (*) (Dictionary_2_t1210 *, KeyValuePair_2_t3255 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m26104_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetEnumerator()
extern "C" Enumerator_t3260  Dictionary_2_GetEnumerator_m26105_gshared (Dictionary_2_t1210 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m26105(__this, method) (( Enumerator_t3260  (*) (Dictionary_2_t1210 *, const MethodInfo*))Dictionary_2_GetEnumerator_m26105_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m26106_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t970  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m26106(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t970 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m26106_gshared)(__this /* static, unused */, ___key, ___value, method)
