﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m25970_gshared (KeyValuePair_2_t3240 * __this, int32_t ___key, TrackableResultData_t969  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m25970(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3240 *, int32_t, TrackableResultData_t969 , const MethodInfo*))KeyValuePair_2__ctor_m25970_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m25971_gshared (KeyValuePair_2_t3240 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m25971(__this, method) (( int32_t (*) (KeyValuePair_2_t3240 *, const MethodInfo*))KeyValuePair_2_get_Key_m25971_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m25972_gshared (KeyValuePair_2_t3240 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m25972(__this, ___value, method) (( void (*) (KeyValuePair_2_t3240 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m25972_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C" TrackableResultData_t969  KeyValuePair_2_get_Value_m25973_gshared (KeyValuePair_2_t3240 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m25973(__this, method) (( TrackableResultData_t969  (*) (KeyValuePair_2_t3240 *, const MethodInfo*))KeyValuePair_2_get_Value_m25973_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m25974_gshared (KeyValuePair_2_t3240 * __this, TrackableResultData_t969  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m25974(__this, ___value, method) (( void (*) (KeyValuePair_2_t3240 *, TrackableResultData_t969 , const MethodInfo*))KeyValuePair_2_set_Value_m25974_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m25975_gshared (KeyValuePair_2_t3240 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m25975(__this, method) (( String_t* (*) (KeyValuePair_2_t3240 *, const MethodInfo*))KeyValuePair_2_ToString_m25975_gshared)(__this, method)
