﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Canvas.JsBridge
struct JsBridge_t232;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Canvas.JsBridge::.ctor()
extern "C" void JsBridge__ctor_m1270 (JsBridge_t232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::Start()
extern "C" void JsBridge_Start_m1271 (JsBridge_t232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnLoginComplete(System.String)
extern "C" void JsBridge_OnLoginComplete_m1272 (JsBridge_t232 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnFacebookAuthResponseChange(System.String)
extern "C" void JsBridge_OnFacebookAuthResponseChange_m1273 (JsBridge_t232 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnPayComplete(System.String)
extern "C" void JsBridge_OnPayComplete_m1274 (JsBridge_t232 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnAppRequestsComplete(System.String)
extern "C" void JsBridge_OnAppRequestsComplete_m1275 (JsBridge_t232 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnShareLinkComplete(System.String)
extern "C" void JsBridge_OnShareLinkComplete_m1276 (JsBridge_t232 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnGroupCreateComplete(System.String)
extern "C" void JsBridge_OnGroupCreateComplete_m1277 (JsBridge_t232 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnJoinGroupComplete(System.String)
extern "C" void JsBridge_OnJoinGroupComplete_m1278 (JsBridge_t232 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnFacebookFocus(System.String)
extern "C" void JsBridge_OnFacebookFocus_m1279 (JsBridge_t232 * __this, String_t* ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnInitComplete(System.String)
extern "C" void JsBridge_OnInitComplete_m1280 (JsBridge_t232 * __this, String_t* ___responseJsonData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.JsBridge::OnUrlResponse(System.String)
extern "C" void JsBridge_OnUrlResponse_m1281 (JsBridge_t232 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
