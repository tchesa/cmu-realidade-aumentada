﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t2050;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1864;

#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t1864 * UnmanagedMarshal_ToMarshalAsAttribute_m12773 (UnmanagedMarshal_t2050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
