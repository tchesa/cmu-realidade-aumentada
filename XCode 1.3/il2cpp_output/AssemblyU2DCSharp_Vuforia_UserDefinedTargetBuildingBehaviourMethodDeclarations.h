﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t442;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingBehaviour__ctor_m2114 (UserDefinedTargetBuildingBehaviour_t442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
