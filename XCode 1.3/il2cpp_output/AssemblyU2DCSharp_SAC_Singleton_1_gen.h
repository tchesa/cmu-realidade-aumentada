﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SA_ScreenShotMaker
struct SA_ScreenShotMaker_t216;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// SAC_Singleton`1<SA_ScreenShotMaker>
struct  SAC_Singleton_1_t217  : public EventDispatcher_t69
{
};
struct SAC_Singleton_1_t217_StaticFields{
	// T SAC_Singleton`1<SA_ScreenShotMaker>::_instance
	SA_ScreenShotMaker_t216 * ____instance_3;
};
