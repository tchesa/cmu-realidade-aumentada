﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SAC_Singleton`1<System.Object>
struct SAC_Singleton_1_t2756;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SAC_Singleton`1<System.Object>::.ctor()
extern "C" void SAC_Singleton_1__ctor_m18045_gshared (SAC_Singleton_1_t2756 * __this, const MethodInfo* method);
#define SAC_Singleton_1__ctor_m18045(__this, method) (( void (*) (SAC_Singleton_1_t2756 *, const MethodInfo*))SAC_Singleton_1__ctor_m18045_gshared)(__this, method)
// System.Void SAC_Singleton`1<System.Object>::.cctor()
extern "C" void SAC_Singleton_1__cctor_m18046_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SAC_Singleton_1__cctor_m18046(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SAC_Singleton_1__cctor_m18046_gshared)(__this /* static, unused */, method)
// T SAC_Singleton`1<System.Object>::get_instance()
extern "C" Object_t * SAC_Singleton_1_get_instance_m18047_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SAC_Singleton_1_get_instance_m18047(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))SAC_Singleton_1_get_instance_m18047_gshared)(__this /* static, unused */, method)
// System.Boolean SAC_Singleton`1<System.Object>::get_IsDestroyed()
extern "C" bool SAC_Singleton_1_get_IsDestroyed_m18048_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SAC_Singleton_1_get_IsDestroyed_m18048(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))SAC_Singleton_1_get_IsDestroyed_m18048_gshared)(__this /* static, unused */, method)
