﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,GameCenterPlayerTemplate>
struct Dictionary_2_t106;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GameCenterPlayerTemplate>
struct  KeyCollection_t2698  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GameCenterPlayerTemplate>::dictionary
	Dictionary_2_t106 * ___dictionary_0;
};
