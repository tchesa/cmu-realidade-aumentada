﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t433;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t431;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t910;

#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
extern "C" ReconstructionAbstractBehaviour_t431 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m4703 (ReconstructionFromTargetAbstractBehaviour_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
extern "C" Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m4704 (ReconstructionFromTargetAbstractBehaviour_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Awake_m4705 (ReconstructionFromTargetAbstractBehaviour_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m4706 (ReconstructionFromTargetAbstractBehaviour_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Initialize_m4707 (ReconstructionFromTargetAbstractBehaviour_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m4708 (ReconstructionFromTargetAbstractBehaviour_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C" void ReconstructionFromTargetAbstractBehaviour__ctor_m2754 (ReconstructionFromTargetAbstractBehaviour_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
