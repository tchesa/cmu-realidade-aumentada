﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.DecoderExceptionFallback
struct DecoderExceptionFallback_t2368;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2367;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.DecoderExceptionFallback::.ctor()
extern "C" void DecoderExceptionFallback__ctor_m14230 (DecoderExceptionFallback_t2368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderExceptionFallback::CreateFallbackBuffer()
extern "C" DecoderFallbackBuffer_t2367 * DecoderExceptionFallback_CreateFallbackBuffer_m14231 (DecoderExceptionFallback_t2368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.DecoderExceptionFallback::Equals(System.Object)
extern "C" bool DecoderExceptionFallback_Equals_m14232 (DecoderExceptionFallback_t2368 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderExceptionFallback::GetHashCode()
extern "C" int32_t DecoderExceptionFallback_GetHashCode_m14233 (DecoderExceptionFallback_t2368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
