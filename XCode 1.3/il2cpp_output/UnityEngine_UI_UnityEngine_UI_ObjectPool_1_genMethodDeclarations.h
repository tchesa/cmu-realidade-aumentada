﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m4023(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t641 *, UnityAction_1_t643 *, UnityAction_1_t643 *, const MethodInfo*))ObjectPool_1__ctor_m19733_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countAll()
#define ObjectPool_1_get_countAll_m19734(__this, method) (( int32_t (*) (ObjectPool_1_t641 *, const MethodInfo*))ObjectPool_1_get_countAll_m19735_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m19736(__this, ___value, method) (( void (*) (ObjectPool_1_t641 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m19737_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countActive()
#define ObjectPool_1_get_countActive_m19738(__this, method) (( int32_t (*) (ObjectPool_1_t641 *, const MethodInfo*))ObjectPool_1_get_countActive_m19739_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m19740(__this, method) (( int32_t (*) (ObjectPool_1_t641 *, const MethodInfo*))ObjectPool_1_get_countInactive_m19741_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Get()
#define ObjectPool_1_Get_m19742(__this, method) (( List_1_t799 * (*) (ObjectPool_1_t641 *, const MethodInfo*))ObjectPool_1_Get_m19743_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Release(T)
#define ObjectPool_1_Release_m19744(__this, ___element, method) (( void (*) (ObjectPool_1_t641 *, List_1_t799 *, const MethodInfo*))ObjectPool_1_Release_m19745_gshared)(__this, ___element, method)
