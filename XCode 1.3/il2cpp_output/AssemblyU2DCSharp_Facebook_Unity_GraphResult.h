﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t279;
// UnityEngine.Texture2D
struct Texture2D_t33;

#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase.h"

// Facebook.Unity.GraphResult
struct  GraphResult_t278  : public ResultBase_t275
{
	// System.Collections.Generic.IList`1<System.Object> Facebook.Unity.GraphResult::<ResultList>k__BackingField
	Object_t* ___U3CResultListU3Ek__BackingField_5;
	// UnityEngine.Texture2D Facebook.Unity.GraphResult::<Texture>k__BackingField
	Texture2D_t33 * ___U3CTextureU3Ek__BackingField_6;
};
