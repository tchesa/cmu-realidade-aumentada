﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "mscorlib_System_Object.h"

// GameCenterDataPackage
struct  GameCenterDataPackage_t123  : public Object_t
{
	// System.String GameCenterDataPackage::_playerID
	String_t* ____playerID_0;
	// System.Byte[] GameCenterDataPackage::_buffer
	ByteU5BU5D_t119* ____buffer_1;
};
