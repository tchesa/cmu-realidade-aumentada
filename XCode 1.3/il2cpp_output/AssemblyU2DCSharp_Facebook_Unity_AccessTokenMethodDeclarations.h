﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AccessToken
struct AccessToken_t219;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t221;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void Facebook.Unity.AccessToken::.ctor(System.String,System.String,System.DateTime,System.Collections.Generic.IEnumerable`1<System.String>)
extern "C" void AccessToken__ctor_m1212 (AccessToken_t219 * __this, String_t* ___tokenString, String_t* ___userId, DateTime_t220  ___expirationTime, Object_t* ___permissions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.AccessToken Facebook.Unity.AccessToken::get_CurrentAccessToken()
extern "C" AccessToken_t219 * AccessToken_get_CurrentAccessToken_m1213 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AccessToken::set_CurrentAccessToken(Facebook.Unity.AccessToken)
extern "C" void AccessToken_set_CurrentAccessToken_m1214 (Object_t * __this /* static, unused */, AccessToken_t219 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AccessToken::get_TokenString()
extern "C" String_t* AccessToken_get_TokenString_m1215 (AccessToken_t219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AccessToken::set_TokenString(System.String)
extern "C" void AccessToken_set_TokenString_m1216 (AccessToken_t219 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Facebook.Unity.AccessToken::get_ExpirationTime()
extern "C" DateTime_t220  AccessToken_get_ExpirationTime_m1217 (AccessToken_t219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AccessToken::set_ExpirationTime(System.DateTime)
extern "C" void AccessToken_set_ExpirationTime_m1218 (AccessToken_t219 * __this, DateTime_t220  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AccessToken::get_Permissions()
extern "C" Object_t* AccessToken_get_Permissions_m1219 (AccessToken_t219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AccessToken::set_Permissions(System.Collections.Generic.IEnumerable`1<System.String>)
extern "C" void AccessToken_set_Permissions_m1220 (AccessToken_t219 * __this, Object_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AccessToken::get_UserId()
extern "C" String_t* AccessToken_get_UserId_m1221 (AccessToken_t219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AccessToken::set_UserId(System.String)
extern "C" void AccessToken_set_UserId_m1222 (AccessToken_t219 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AccessToken::ToJson()
extern "C" String_t* AccessToken_ToJson_m1223 (AccessToken_t219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
