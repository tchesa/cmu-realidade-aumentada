﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// AnimationControllerScript/<DesapareceProveta>c__Iterator11
struct  U3CDesapareceProvetaU3Ec__Iterator11_t366  : public Object_t
{
	// System.Int32 AnimationControllerScript/<DesapareceProveta>c__Iterator11::$PC
	int32_t ___U24PC_0;
	// System.Object AnimationControllerScript/<DesapareceProveta>c__Iterator11::$current
	Object_t * ___U24current_1;
};
