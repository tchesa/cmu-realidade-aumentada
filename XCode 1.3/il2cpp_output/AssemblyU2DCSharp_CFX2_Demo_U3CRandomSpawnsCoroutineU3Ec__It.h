﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t27;
// System.Object
struct Object_t;
// CFX2_Demo
struct CFX2_Demo_t310;

#include "mscorlib_System_Object.h"

// CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7
struct  U3CRandomSpawnsCoroutineU3Ec__Iterator7_t309  : public Object_t
{
	// UnityEngine.GameObject CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::<particles>__0
	GameObject_t27 * ___U3CparticlesU3E__0_0;
	// System.Int32 CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::$PC
	int32_t ___U24PC_1;
	// System.Object CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::$current
	Object_t * ___U24current_2;
	// CFX2_Demo CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator7::<>f__this
	CFX2_Demo_t310 * ___U3CU3Ef__this_3;
};
