﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t410;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Vuforia.WireframeTrackableEventHandler
struct  WireframeTrackableEventHandler_t455  : public MonoBehaviour_t18
{
	// Vuforia.TrackableBehaviour Vuforia.WireframeTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t410 * ___mTrackableBehaviour_1;
};
