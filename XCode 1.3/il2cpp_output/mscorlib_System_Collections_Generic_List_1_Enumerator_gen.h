﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t26;
// System.Collections.Hashtable
struct Hashtable_t19;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>
struct  Enumerator_t62 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>::l
	List_1_t26 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>::current
	Hashtable_t19 * ___current_3;
};
