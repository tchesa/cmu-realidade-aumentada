﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t3130;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C" void DefaultComparer__ctor_m23915_gshared (DefaultComparer_t3130 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23915(__this, method) (( void (*) (DefaultComparer_t3130 *, const MethodInfo*))DefaultComparer__ctor_m23915_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m23916_gshared (DefaultComparer_t3130 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m23916(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3130 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m23916_gshared)(__this, ___x, ___y, method)
