﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t360;
// UnityEngine.GameObject
struct GameObject_t27;
// UnityEngine.ParticleSystem
struct ParticleSystem_t332;
// System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>
struct List_1_t371;
// ComponentTracker
struct ComponentTracker_t67;
// System.Comparison`1<AnimationControllerScript/TimeEvent>
struct Comparison_1_t372;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// AnimationControllerScript
struct  AnimationControllerScript_t364  : public MonoBehaviour_t18
{
	// System.Boolean AnimationControllerScript::on
	bool ___on_1;
	// System.Boolean AnimationControllerScript::off
	bool ___off_2;
	// UnityEngine.Animator AnimationControllerScript::anim
	Animator_t360 * ___anim_3;
	// System.Single AnimationControllerScript::startTime
	float ___startTime_4;
	// UnityEngine.GameObject AnimationControllerScript::electricityPoint
	GameObject_t27 * ___electricityPoint_5;
	// UnityEngine.GameObject AnimationControllerScript::fourtain
	GameObject_t27 * ___fourtain_6;
	// UnityEngine.ParticleSystem AnimationControllerScript::particlePortal
	ParticleSystem_t332 * ___particlePortal_7;
	// UnityEngine.ParticleSystem AnimationControllerScript::vortexPortal
	ParticleSystem_t332 * ___vortexPortal_8;
	// UnityEngine.ParticleSystem AnimationControllerScript::smoke
	ParticleSystem_t332 * ___smoke_9;
	// System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent> AnimationControllerScript::events
	List_1_t371 * ___events_10;
	// System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent> AnimationControllerScript::eventQueue
	List_1_t371 * ___eventQueue_11;
	// ComponentTracker AnimationControllerScript::tracker
	ComponentTracker_t67 * ___tracker_12;
};
struct AnimationControllerScript_t364_StaticFields{
	// System.Comparison`1<AnimationControllerScript/TimeEvent> AnimationControllerScript::<>f__am$cacheC
	Comparison_1_t372 * ___U3CU3Ef__amU24cacheC_13;
};
