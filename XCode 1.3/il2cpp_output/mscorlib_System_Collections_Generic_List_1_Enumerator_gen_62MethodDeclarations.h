﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m25125(__this, ___l, method) (( void (*) (Enumerator_t3202 *, List_1_t1046 *, const MethodInfo*))Enumerator__ctor_m15550_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m25126(__this, method) (( void (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15551_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25127(__this, method) (( Object_t * (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15552_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::Dispose()
#define Enumerator_Dispose_m25128(__this, method) (( void (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_Dispose_m15553_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::VerifyState()
#define Enumerator_VerifyState_m25129(__this, method) (( void (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_VerifyState_m15554_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::MoveNext()
#define Enumerator_MoveNext_m25130(__this, method) (( bool (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_MoveNext_m2489_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::get_Current()
#define Enumerator_get_Current_m25131(__this, method) (( TrackableBehaviour_t410 * (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_get_Current_m2488_gshared)(__this, method)
