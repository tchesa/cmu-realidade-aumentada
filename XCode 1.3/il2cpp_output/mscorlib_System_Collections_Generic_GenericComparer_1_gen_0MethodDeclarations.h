﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t2571;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericComparer_1__ctor_m15437_gshared (GenericComparer_1_t2571 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m15437(__this, method) (( void (*) (GenericComparer_1_t2571 *, const MethodInfo*))GenericComparer_1__ctor_m15437_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m30131_gshared (GenericComparer_1_t2571 * __this, DateTimeOffset_t2423  ___x, DateTimeOffset_t2423  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m30131(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2571 *, DateTimeOffset_t2423 , DateTimeOffset_t2423 , const MethodInfo*))GenericComparer_1_Compare_m30131_gshared)(__this, ___x, ___y, method)
