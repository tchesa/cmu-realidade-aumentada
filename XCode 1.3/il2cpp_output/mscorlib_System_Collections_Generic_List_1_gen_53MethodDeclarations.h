﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t1054;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t1115;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t1132;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ICollection_1_t3731;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ReadOnlyCollection_1_t3275;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3273;
// System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>
struct Predicate_1_t3280;
// System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparison_1_t3283;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_63.h"

// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void List_1__ctor_m6759_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1__ctor_m6759(__this, method) (( void (*) (List_1_t1054 *, const MethodInfo*))List_1__ctor_m6759_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m26302_gshared (List_1_t1054 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m26302(__this, ___collection, method) (( void (*) (List_1_t1054 *, Object_t*, const MethodInfo*))List_1__ctor_m26302_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m26303_gshared (List_1_t1054 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m26303(__this, ___capacity, method) (( void (*) (List_1_t1054 *, int32_t, const MethodInfo*))List_1__ctor_m26303_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern "C" void List_1__cctor_m26304_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m26304(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m26304_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26305_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26305(__this, method) (( Object_t* (*) (List_1_t1054 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26305_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m26306_gshared (List_1_t1054 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m26306(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1054 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m26306_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m26307_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m26307(__this, method) (( Object_t * (*) (List_1_t1054 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m26307_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m26308_gshared (List_1_t1054 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m26308(__this, ___item, method) (( int32_t (*) (List_1_t1054 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m26308_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m26309_gshared (List_1_t1054 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m26309(__this, ___item, method) (( bool (*) (List_1_t1054 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m26309_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m26310_gshared (List_1_t1054 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m26310(__this, ___item, method) (( int32_t (*) (List_1_t1054 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m26310_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m26311_gshared (List_1_t1054 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m26311(__this, ___index, ___item, method) (( void (*) (List_1_t1054 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m26311_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m26312_gshared (List_1_t1054 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m26312(__this, ___item, method) (( void (*) (List_1_t1054 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m26312_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26313_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26313(__this, method) (( bool (*) (List_1_t1054 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26313_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m26314_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m26314(__this, method) (( bool (*) (List_1_t1054 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m26314_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m26315_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m26315(__this, method) (( Object_t * (*) (List_1_t1054 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m26315_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m26316_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m26316(__this, method) (( bool (*) (List_1_t1054 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m26316_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m26317_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m26317(__this, method) (( bool (*) (List_1_t1054 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m26317_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m26318_gshared (List_1_t1054 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m26318(__this, ___index, method) (( Object_t * (*) (List_1_t1054 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m26318_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m26319_gshared (List_1_t1054 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m26319(__this, ___index, ___value, method) (( void (*) (List_1_t1054 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m26319_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void List_1_Add_m26320_gshared (List_1_t1054 * __this, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define List_1_Add_m26320(__this, ___item, method) (( void (*) (List_1_t1054 *, TargetSearchResult_t1050 , const MethodInfo*))List_1_Add_m26320_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m26321_gshared (List_1_t1054 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m26321(__this, ___newCount, method) (( void (*) (List_1_t1054 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m26321_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m26322_gshared (List_1_t1054 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m26322(__this, ___collection, method) (( void (*) (List_1_t1054 *, Object_t*, const MethodInfo*))List_1_AddCollection_m26322_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m26323_gshared (List_1_t1054 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m26323(__this, ___enumerable, method) (( void (*) (List_1_t1054 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m26323_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m26324_gshared (List_1_t1054 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m26324(__this, ___collection, method) (( void (*) (List_1_t1054 *, Object_t*, const MethodInfo*))List_1_AddRange_m26324_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3275 * List_1_AsReadOnly_m26325_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m26325(__this, method) (( ReadOnlyCollection_1_t3275 * (*) (List_1_t1054 *, const MethodInfo*))List_1_AsReadOnly_m26325_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void List_1_Clear_m26326_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_Clear_m26326(__this, method) (( void (*) (List_1_t1054 *, const MethodInfo*))List_1_Clear_m26326_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool List_1_Contains_m26327_gshared (List_1_t1054 * __this, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define List_1_Contains_m26327(__this, ___item, method) (( bool (*) (List_1_t1054 *, TargetSearchResult_t1050 , const MethodInfo*))List_1_Contains_m26327_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m26328_gshared (List_1_t1054 * __this, TargetSearchResultU5BU5D_t3273* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m26328(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1054 *, TargetSearchResultU5BU5D_t3273*, int32_t, const MethodInfo*))List_1_CopyTo_m26328_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Find(System.Predicate`1<T>)
extern "C" TargetSearchResult_t1050  List_1_Find_m26329_gshared (List_1_t1054 * __this, Predicate_1_t3280 * ___match, const MethodInfo* method);
#define List_1_Find_m26329(__this, ___match, method) (( TargetSearchResult_t1050  (*) (List_1_t1054 *, Predicate_1_t3280 *, const MethodInfo*))List_1_Find_m26329_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m26330_gshared (Object_t * __this /* static, unused */, Predicate_1_t3280 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m26330(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3280 *, const MethodInfo*))List_1_CheckMatch_m26330_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m26331_gshared (List_1_t1054 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3280 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m26331(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1054 *, int32_t, int32_t, Predicate_1_t3280 *, const MethodInfo*))List_1_GetIndex_m26331_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Enumerator_t3274  List_1_GetEnumerator_m26332_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m26332(__this, method) (( Enumerator_t3274  (*) (List_1_t1054 *, const MethodInfo*))List_1_GetEnumerator_m26332_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m26333_gshared (List_1_t1054 * __this, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define List_1_IndexOf_m26333(__this, ___item, method) (( int32_t (*) (List_1_t1054 *, TargetSearchResult_t1050 , const MethodInfo*))List_1_IndexOf_m26333_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m26334_gshared (List_1_t1054 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m26334(__this, ___start, ___delta, method) (( void (*) (List_1_t1054 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m26334_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m26335_gshared (List_1_t1054 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m26335(__this, ___index, method) (( void (*) (List_1_t1054 *, int32_t, const MethodInfo*))List_1_CheckIndex_m26335_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m26336_gshared (List_1_t1054 * __this, int32_t ___index, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define List_1_Insert_m26336(__this, ___index, ___item, method) (( void (*) (List_1_t1054 *, int32_t, TargetSearchResult_t1050 , const MethodInfo*))List_1_Insert_m26336_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m26337_gshared (List_1_t1054 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m26337(__this, ___collection, method) (( void (*) (List_1_t1054 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m26337_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool List_1_Remove_m26338_gshared (List_1_t1054 * __this, TargetSearchResult_t1050  ___item, const MethodInfo* method);
#define List_1_Remove_m26338(__this, ___item, method) (( bool (*) (List_1_t1054 *, TargetSearchResult_t1050 , const MethodInfo*))List_1_Remove_m26338_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m26339_gshared (List_1_t1054 * __this, Predicate_1_t3280 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m26339(__this, ___match, method) (( int32_t (*) (List_1_t1054 *, Predicate_1_t3280 *, const MethodInfo*))List_1_RemoveAll_m26339_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m26340_gshared (List_1_t1054 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m26340(__this, ___index, method) (( void (*) (List_1_t1054 *, int32_t, const MethodInfo*))List_1_RemoveAt_m26340_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Reverse()
extern "C" void List_1_Reverse_m26341_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_Reverse_m26341(__this, method) (( void (*) (List_1_t1054 *, const MethodInfo*))List_1_Reverse_m26341_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort()
extern "C" void List_1_Sort_m26342_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_Sort_m26342(__this, method) (( void (*) (List_1_t1054 *, const MethodInfo*))List_1_Sort_m26342_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m26343_gshared (List_1_t1054 * __this, Comparison_1_t3283 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m26343(__this, ___comparison, method) (( void (*) (List_1_t1054 *, Comparison_1_t3283 *, const MethodInfo*))List_1_Sort_m26343_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::ToArray()
extern "C" TargetSearchResultU5BU5D_t3273* List_1_ToArray_m26344_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_ToArray_m26344(__this, method) (( TargetSearchResultU5BU5D_t3273* (*) (List_1_t1054 *, const MethodInfo*))List_1_ToArray_m26344_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m26345_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m26345(__this, method) (( void (*) (List_1_t1054 *, const MethodInfo*))List_1_TrimExcess_m26345_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m26346_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m26346(__this, method) (( int32_t (*) (List_1_t1054 *, const MethodInfo*))List_1_get_Capacity_m26346_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m26347_gshared (List_1_t1054 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m26347(__this, ___value, method) (( void (*) (List_1_t1054 *, int32_t, const MethodInfo*))List_1_set_Capacity_m26347_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t List_1_get_Count_m26348_gshared (List_1_t1054 * __this, const MethodInfo* method);
#define List_1_get_Count_m26348(__this, method) (( int32_t (*) (List_1_t1054 *, const MethodInfo*))List_1_get_Count_m26348_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t1050  List_1_get_Item_m26349_gshared (List_1_t1054 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m26349(__this, ___index, method) (( TargetSearchResult_t1050  (*) (List_1_t1054 *, int32_t, const MethodInfo*))List_1_get_Item_m26349_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m26350_gshared (List_1_t1054 * __this, int32_t ___index, TargetSearchResult_t1050  ___value, const MethodInfo* method);
#define List_1_set_Item_m26350(__this, ___index, ___value, method) (( void (*) (List_1_t1054 *, int32_t, TargetSearchResult_t1050 , const MethodInfo*))List_1_set_Item_m26350_gshared)(__this, ___index, ___value, method)
