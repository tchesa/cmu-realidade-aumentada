﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopUpExamples
struct PopUpExamples_t210;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_IOSDialogResult.h"

// System.Void PopUpExamples::.ctor()
extern "C" void PopUpExamples__ctor_m1172 (PopUpExamples_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::Awake()
extern "C" void PopUpExamples_Awake_m1173 (PopUpExamples_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::OnGUI()
extern "C" void PopUpExamples_OnGUI_m1174 (PopUpExamples_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::HidePreloader()
extern "C" void PopUpExamples_HidePreloader_m1175 (PopUpExamples_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::dismissAlert()
extern "C" void PopUpExamples_dismissAlert_m1176 (PopUpExamples_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::onRatePopUpClose(IOSDialogResult)
extern "C" void PopUpExamples_onRatePopUpClose_m1177 (PopUpExamples_t210 * __this, int32_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::onDialogClose(IOSDialogResult)
extern "C" void PopUpExamples_onDialogClose_m1178 (PopUpExamples_t210 * __this, int32_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::onMessageClose()
extern "C" void PopUpExamples_onMessageClose_m1179 (PopUpExamples_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
