﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GameCenterPlayerTemplate
struct GameCenterPlayerTemplate_t107;

#include "AssemblyU2DCSharp_ISN_Result.h"

// ISN_UserInfoLoadResult
struct  ISN_UserInfoLoadResult_t120  : public ISN_Result_t114
{
	// System.String ISN_UserInfoLoadResult::_playerId
	String_t* ____playerId_2;
	// GameCenterPlayerTemplate ISN_UserInfoLoadResult::_tpl
	GameCenterPlayerTemplate_t107 * ____tpl_3;
};
