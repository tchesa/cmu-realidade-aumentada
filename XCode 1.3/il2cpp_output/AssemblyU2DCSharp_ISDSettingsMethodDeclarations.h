﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISDSettings
struct ISDSettings_t76;

#include "codegen/il2cpp-codegen.h"

// System.Void ISDSettings::.ctor()
extern "C" void ISDSettings__ctor_m513 (ISDSettings_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISDSettings ISDSettings::get_Instance()
extern "C" ISDSettings_t76 * ISDSettings_get_Instance_m514 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
