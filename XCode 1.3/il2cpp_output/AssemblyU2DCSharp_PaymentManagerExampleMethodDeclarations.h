﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PaymentManagerExample
struct PaymentManagerExample_t186;
// System.String
struct String_t;
// IOSStoreKitResponse
struct IOSStoreKitResponse_t137;
// UnionAssets.FLE.CEvent
struct CEvent_t74;
// ISN_Result
struct ISN_Result_t114;

#include "codegen/il2cpp-codegen.h"

// System.Void PaymentManagerExample::.ctor()
extern "C" void PaymentManagerExample__ctor_m1048 (PaymentManagerExample_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::.cctor()
extern "C" void PaymentManagerExample__cctor_m1049 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::init()
extern "C" void PaymentManagerExample_init_m1050 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::buyItem(System.String)
extern "C" void PaymentManagerExample_buyItem_m1051 (Object_t * __this /* static, unused */, String_t* ___productId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::UnlockProducts(System.String)
extern "C" void PaymentManagerExample_UnlockProducts_m1052 (Object_t * __this /* static, unused */, String_t* ___productIdentifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::OnTransactionComplete(IOSStoreKitResponse)
extern "C" void PaymentManagerExample_OnTransactionComplete_m1053 (Object_t * __this /* static, unused */, IOSStoreKitResponse_t137 * ___response, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::onRestoreTransactionFailed()
extern "C" void PaymentManagerExample_onRestoreTransactionFailed_m1054 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::onVerificationResponse(UnionAssets.FLE.CEvent)
extern "C" void PaymentManagerExample_onVerificationResponse_m1055 (Object_t * __this /* static, unused */, CEvent_t74 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::OnStoreKitInitComplete(ISN_Result)
extern "C" void PaymentManagerExample_OnStoreKitInitComplete_m1056 (Object_t * __this /* static, unused */, ISN_Result_t114 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
