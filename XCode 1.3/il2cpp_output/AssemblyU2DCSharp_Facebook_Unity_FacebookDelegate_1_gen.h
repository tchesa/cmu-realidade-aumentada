﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.ILoginResult
struct ILoginResult_t489;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>
struct  FacebookDelegate_1_t465  : public MulticastDelegate_t10
{
};
