﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1210;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_51.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m26139_gshared (Enumerator_t3259 * __this, Dictionary_2_t1210 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m26139(__this, ___host, method) (( void (*) (Enumerator_t3259 *, Dictionary_2_t1210 *, const MethodInfo*))Enumerator__ctor_m26139_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26140_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26140(__this, method) (( Object_t * (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26141_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m26141(__this, method) (( void (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m26141_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m26142_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26142(__this, method) (( void (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_Dispose_m26142_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26143_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26143(__this, method) (( bool (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_MoveNext_m26143_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" int32_t Enumerator_get_Current_m26144_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26144(__this, method) (( int32_t (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_get_Current_m26144_gshared)(__this, method)
