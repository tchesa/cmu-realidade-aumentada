﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1850;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_AttributeTargets.h"

// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
extern "C" void AttributeUsageAttribute__ctor_m10315 (AttributeUsageAttribute_t1850 * __this, int32_t ___validOn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_AllowMultiple()
extern "C" bool AttributeUsageAttribute_get_AllowMultiple_m10316 (AttributeUsageAttribute_t1850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m10317 (AttributeUsageAttribute_t1850 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_Inherited()
extern "C" bool AttributeUsageAttribute_get_Inherited_m10318 (AttributeUsageAttribute_t1850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
extern "C" void AttributeUsageAttribute_set_Inherited_m10319 (AttributeUsageAttribute_t1850 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
