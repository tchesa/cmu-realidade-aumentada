﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UnityEngine.Font>
struct Action_1_t843;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t1374;

#include "UnityEngine_UnityEngine_Object.h"

// UnityEngine.Font
struct  Font_t684  : public Object_t53
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t1374 * ___m_FontTextureRebuildCallback_2;
};
struct Font_t684_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t843 * ___textureRebuilt_1;
};
