﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ComponentTracker
struct ComponentTracker_t67;
// ComponentTrack
struct ComponentTrack_t66;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t27;

#include "codegen/il2cpp-codegen.h"

// System.Void ComponentTracker::.ctor()
extern "C" void ComponentTracker__ctor_m443 (ComponentTracker_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ComponentTracker ComponentTracker::get_Instance()
extern "C" ComponentTracker_t67 * ComponentTracker_get_Instance_m444 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ComponentTracker::Awake()
extern "C" void ComponentTracker_Awake_m445 (ComponentTracker_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ComponentTracker::Add(ComponentTrack,System.String)
extern "C" void ComponentTracker_Add_m446 (ComponentTracker_t67 * __this, ComponentTrack_t66 * ___localize, String_t* ___identifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ComponentTracker::GetObject(System.String)
extern "C" GameObject_t27 * ComponentTracker_GetObject_m447 (ComponentTracker_t67 * __this, String_t* ___identifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
