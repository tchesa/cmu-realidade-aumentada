﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t33;
// System.Object
struct Object_t;
// SA_ScreenShotMaker
struct SA_ScreenShotMaker_t216;

#include "mscorlib_System_Object.h"

// SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5
struct  U3CSaveScreenshotU3Ec__Iterator5_t215  : public Object_t
{
	// System.Int32 SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::<height>__1
	int32_t ___U3CheightU3E__1_1;
	// UnityEngine.Texture2D SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::<tex>__2
	Texture2D_t33 * ___U3CtexU3E__2_2;
	// System.Int32 SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::$PC
	int32_t ___U24PC_3;
	// System.Object SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::$current
	Object_t * ___U24current_4;
	// SA_ScreenShotMaker SA_ScreenShotMaker/<SaveScreenshot>c__Iterator5::<>f__this
	SA_ScreenShotMaker_t216 * ___U3CU3Ef__this_5;
};
