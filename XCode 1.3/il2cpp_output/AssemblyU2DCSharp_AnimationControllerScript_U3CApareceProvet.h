﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// AnimationControllerScript/<ApareceProveta>c__Iterator10
struct  U3CApareceProvetaU3Ec__Iterator10_t365  : public Object_t
{
	// System.Int32 AnimationControllerScript/<ApareceProveta>c__Iterator10::$PC
	int32_t ___U24PC_0;
	// System.Object AnimationControllerScript/<ApareceProveta>c__Iterator10::$current
	Object_t * ___U24current_1;
};
