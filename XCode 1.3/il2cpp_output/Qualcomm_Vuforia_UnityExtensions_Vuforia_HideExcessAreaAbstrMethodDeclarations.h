﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t413;
// UnityEngine.GameObject
struct GameObject_t27;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
extern "C" GameObject_t27 * HideExcessAreaAbstractBehaviour_CreateQuad_m4645 (HideExcessAreaAbstractBehaviour_t413 * __this, GameObject_t27 * ___parent, String_t* ___name, Vector3_t6  ___position, Quaternion_t51  ___rotation, Vector3_t6  ___scale, int32_t ___layer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesActive_m4646 (HideExcessAreaAbstractBehaviour_t413 * __this, bool ___activeflag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesRenderingActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesRenderingActive_m4647 (HideExcessAreaAbstractBehaviour_t413 * __this, bool ___activeflag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::isPlanesRenderingActive()
extern "C" bool HideExcessAreaAbstractBehaviour_isPlanesRenderingActive_m4648 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::ForceUpdateHideBehaviours()
extern "C" void HideExcessAreaAbstractBehaviour_ForceUpdateHideBehaviours_m4649 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnVuforiaStarted()
extern "C" void HideExcessAreaAbstractBehaviour_OnVuforiaStarted_m4650 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
extern "C" bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m4651 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPreCull()
extern "C" void HideExcessAreaAbstractBehaviour_OnPreCull_m4652 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPostRender()
extern "C" void HideExcessAreaAbstractBehaviour_OnPostRender_m4653 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
extern "C" void HideExcessAreaAbstractBehaviour_Start_m4654 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDisable()
extern "C" void HideExcessAreaAbstractBehaviour_OnDisable_m4655 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
extern "C" void HideExcessAreaAbstractBehaviour_OnDestroy_m4656 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
extern "C" void HideExcessAreaAbstractBehaviour_Update_m4657 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C" void HideExcessAreaAbstractBehaviour__ctor_m2718 (HideExcessAreaAbstractBehaviour_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
