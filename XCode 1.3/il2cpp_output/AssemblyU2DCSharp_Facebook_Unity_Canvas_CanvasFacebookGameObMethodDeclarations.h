﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Canvas.CanvasFacebookGameObject
struct CanvasFacebookGameObject_t228;
// Facebook.Unity.Canvas.ICanvasFacebookImplementation
struct ICanvasFacebookImplementation_t477;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::.ctor()
extern "C" void CanvasFacebookGameObject__ctor_m1261 (CanvasFacebookGameObject_t228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.Canvas.ICanvasFacebookImplementation Facebook.Unity.Canvas.CanvasFacebookGameObject::get_CanvasFacebookImpl()
extern "C" Object_t * CanvasFacebookGameObject_get_CanvasFacebookImpl_m1262 (CanvasFacebookGameObject_t228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnPayComplete(System.String)
extern "C" void CanvasFacebookGameObject_OnPayComplete_m1263 (CanvasFacebookGameObject_t228 * __this, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnFacebookAuthResponseChange(System.String)
extern "C" void CanvasFacebookGameObject_OnFacebookAuthResponseChange_m1264 (CanvasFacebookGameObject_t228 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnUrlResponse(System.String)
extern "C" void CanvasFacebookGameObject_OnUrlResponse_m1265 (CanvasFacebookGameObject_t228 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnHideUnity(System.Boolean)
extern "C" void CanvasFacebookGameObject_OnHideUnity_m1266 (CanvasFacebookGameObject_t228 * __this, bool ___hide, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnAwake()
extern "C" void CanvasFacebookGameObject_OnAwake_m1267 (CanvasFacebookGameObject_t228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
