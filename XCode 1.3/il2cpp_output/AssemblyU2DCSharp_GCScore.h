﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_GCCollectionType.h"
#include "AssemblyU2DCSharp_GCBoardTimeSpan.h"

// GCScore
struct  GCScore_t117  : public Object_t
{
	// System.Int32 GCScore::_rank
	int32_t ____rank_0;
	// System.String GCScore::_score
	String_t* ____score_1;
	// System.String GCScore::_playerId
	String_t* ____playerId_2;
	// System.String GCScore::_leaderboardId
	String_t* ____leaderboardId_3;
	// GCCollectionType GCScore::_collection
	int32_t ____collection_4;
	// GCBoardTimeSpan GCScore::_timeSpan
	int32_t ____timeSpan_5;
};
