﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnionAssets.FLE.CEvent
struct CEvent_t74;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnionAssets.FLE.IDispatcher
struct IDispatcher_t75;

#include "codegen/il2cpp-codegen.h"

// System.Void UnionAssets.FLE.CEvent::.ctor(System.Int32,System.String,System.Object,UnionAssets.FLE.IDispatcher)
extern "C" void CEvent__ctor_m501 (CEvent_t74 * __this, int32_t ___id, String_t* ___name, Object_t * ___data, Object_t * ___dispatcher, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.CEvent::stopPropagation()
extern "C" void CEvent_stopPropagation_m502 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.CEvent::stopImmediatePropagation()
extern "C" void CEvent_stopImmediatePropagation_m503 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnionAssets.FLE.CEvent::canBeDispatched(System.Object)
extern "C" bool CEvent_canBeDispatched_m504 (CEvent_t74 * __this, Object_t * ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnionAssets.FLE.CEvent::get_id()
extern "C" int32_t CEvent_get_id_m505 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnionAssets.FLE.CEvent::get_name()
extern "C" String_t* CEvent_get_name_m506 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnionAssets.FLE.CEvent::get_data()
extern "C" Object_t * CEvent_get_data_m507 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnionAssets.FLE.IDispatcher UnionAssets.FLE.CEvent::get_target()
extern "C" Object_t * CEvent_get_target_m508 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnionAssets.FLE.IDispatcher UnionAssets.FLE.CEvent::get_dispatcher()
extern "C" Object_t * CEvent_get_dispatcher_m509 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnionAssets.FLE.CEvent::get_currentTarget()
extern "C" Object_t * CEvent_get_currentTarget_m510 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnionAssets.FLE.CEvent::get_isStopped()
extern "C" bool CEvent_get_isStopped_m511 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnionAssets.FLE.CEvent::get_isLocked()
extern "C" bool CEvent_get_isLocked_m512 (CEvent_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
