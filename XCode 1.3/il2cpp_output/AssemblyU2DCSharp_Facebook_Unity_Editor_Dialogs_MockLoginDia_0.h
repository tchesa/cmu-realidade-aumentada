﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_Editor_EditorFacebookMockDi_0.h"

// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct  MockLoginDialog_t273  : public EditorFacebookMockDialog_t270
{
	// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::accessToken
	String_t* ___accessToken_3;
};
