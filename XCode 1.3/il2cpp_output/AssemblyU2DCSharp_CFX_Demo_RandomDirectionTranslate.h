﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// CFX_Demo_RandomDirectionTranslate
struct  CFX_Demo_RandomDirectionTranslate_t322  : public MonoBehaviour_t18
{
	// System.Single CFX_Demo_RandomDirectionTranslate::speed
	float ___speed_1;
	// UnityEngine.Vector3 CFX_Demo_RandomDirectionTranslate::baseDir
	Vector3_t6  ___baseDir_2;
	// UnityEngine.Vector3 CFX_Demo_RandomDirectionTranslate::axis
	Vector3_t6  ___axis_3;
	// System.Boolean CFX_Demo_RandomDirectionTranslate::gravity
	bool ___gravity_4;
	// UnityEngine.Vector3 CFX_Demo_RandomDirectionTranslate::dir
	Vector3_t6  ___dir_5;
};
