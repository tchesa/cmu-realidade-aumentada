﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct  Predicate_1_t1149  : public MulticastDelegate_t10
{
};
