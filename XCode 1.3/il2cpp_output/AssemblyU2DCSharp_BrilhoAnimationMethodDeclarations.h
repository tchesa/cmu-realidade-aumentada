﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BrilhoAnimation
struct BrilhoAnimation_t344;

#include "codegen/il2cpp-codegen.h"

// System.Void BrilhoAnimation::.ctor()
extern "C" void BrilhoAnimation__ctor_m1941 (BrilhoAnimation_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrilhoAnimation::Start()
extern "C" void BrilhoAnimation_Start_m1942 (BrilhoAnimation_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrilhoAnimation::Update()
extern "C" void BrilhoAnimation_Update_m1943 (BrilhoAnimation_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrilhoAnimation::Activate()
extern "C" void BrilhoAnimation_Activate_m1944 (BrilhoAnimation_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BrilhoAnimation::SetColorByAlpha(System.Single)
extern "C" void BrilhoAnimation_SetColorByAlpha_m1945 (BrilhoAnimation_t344 * __this, float ___alpha, const MethodInfo* method) IL2CPP_METHOD_ATTR;
