﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_134.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29990_gshared (InternalEnumerator_1_t3575 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29990(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3575 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29990_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29991_gshared (InternalEnumerator_1_t3575 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29991(__this, method) (( void (*) (InternalEnumerator_1_t3575 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29991_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29992_gshared (InternalEnumerator_1_t3575 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29992(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3575 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29992_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29993_gshared (InternalEnumerator_1_t3575 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29993(__this, method) (( void (*) (InternalEnumerator_1_t3575 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29993_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29994_gshared (InternalEnumerator_1_t3575 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29994(__this, method) (( bool (*) (InternalEnumerator_1_t3575 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29994_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C" DateTime_t220  InternalEnumerator_1_get_Current_m29995_gshared (InternalEnumerator_1_t3575 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29995(__this, method) (( DateTime_t220  (*) (InternalEnumerator_1_t3575 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29995_gshared)(__this, method)
