﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.StringBuilder
struct StringBuilder_t305;

#include "mscorlib_System_Object.h"

// Facebook.MiniJSON.Json/Serializer
struct  Serializer_t304  : public Object_t
{
	// System.Text.StringBuilder Facebook.MiniJSON.Json/Serializer::builder
	StringBuilder_t305 * ___builder_0;
};
