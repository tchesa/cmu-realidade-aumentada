﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_BaseIOSFeaturePreview.h"

// IOSNativePreviewBackButton
struct  IOSNativePreviewBackButton_t213  : public BaseIOSFeaturePreview_t183
{
	// System.String IOSNativePreviewBackButton::initialSceneName
	String_t* ___initialSceneName_11;
};
