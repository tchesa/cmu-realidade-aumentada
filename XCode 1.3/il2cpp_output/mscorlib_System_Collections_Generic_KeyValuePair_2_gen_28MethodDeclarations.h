﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m22888(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3074 *, int32_t, Image_t943 *, const MethodInfo*))KeyValuePair_2__ctor_m22787_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Key()
#define KeyValuePair_2_get_Key_m22889(__this, method) (( int32_t (*) (KeyValuePair_2_t3074 *, const MethodInfo*))KeyValuePair_2_get_Key_m22788_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22890(__this, ___value, method) (( void (*) (KeyValuePair_2_t3074 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m22789_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Value()
#define KeyValuePair_2_get_Value_m22891(__this, method) (( Image_t943 * (*) (KeyValuePair_2_t3074 *, const MethodInfo*))KeyValuePair_2_get_Value_m22790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22892(__this, ___value, method) (( void (*) (KeyValuePair_2_t3074 *, Image_t943 *, const MethodInfo*))KeyValuePair_2_set_Value_m22791_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToString()
#define KeyValuePair_2_ToString_m22893(__this, method) (( String_t* (*) (KeyValuePair_2_t3074 *, const MethodInfo*))KeyValuePair_2_ToString_m22792_gshared)(__this, method)
