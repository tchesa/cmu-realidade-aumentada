﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m24751(__this, ___dictionary, method) (( void (*) (Enumerator_t1175 *, Dictionary_2_t1021 *, const MethodInfo*))Enumerator__ctor_m16419_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24752(__this, method) (( Object_t * (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16420_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m24753(__this, method) (( void (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16421_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24754(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16422_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24755(__this, method) (( Object_t * (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16423_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24756(__this, method) (( Object_t * (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16424_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m6657(__this, method) (( bool (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_MoveNext_m16425_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m6654(__this, method) (( KeyValuePair_2_t1174  (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_get_Current_m16426_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24757(__this, method) (( int32_t (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_get_CurrentKey_m16427_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24758(__this, method) (( WordAbstractBehaviour_t457 * (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_get_CurrentValue_m16428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::Reset()
#define Enumerator_Reset_m24759(__this, method) (( void (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_Reset_m16429_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m24760(__this, method) (( void (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_VerifyState_m16430_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24761(__this, method) (( void (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_VerifyCurrent_m16431_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m6658(__this, method) (( void (*) (Enumerator_t1175 *, const MethodInfo*))Enumerator_Dispose_m16432_gshared)(__this, method)
