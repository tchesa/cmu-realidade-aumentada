﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct HorizontalOrVerticalLayoutGroup_t778;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m3849 (HorizontalOrVerticalLayoutGroup_t778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern "C" float HorizontalOrVerticalLayoutGroup_get_spacing_m3850 (HorizontalOrVerticalLayoutGroup_t778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m3851 (HorizontalOrVerticalLayoutGroup_t778 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m3852 (HorizontalOrVerticalLayoutGroup_t778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m3853 (HorizontalOrVerticalLayoutGroup_t778 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m3854 (HorizontalOrVerticalLayoutGroup_t778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m3855 (HorizontalOrVerticalLayoutGroup_t778 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3856 (HorizontalOrVerticalLayoutGroup_t778 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3857 (HorizontalOrVerticalLayoutGroup_t778 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method) IL2CPP_METHOD_ATTR;
