﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,GCScore>
struct Dictionary_2_t124;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GCScore>
struct  ValueCollection_t2725  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GCScore>::dictionary
	Dictionary_2_t124 * ___dictionary_0;
};
