﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AchievementTemplate
struct AchievementTemplate_t115;

#include "AssemblyU2DCSharp_ISN_Result.h"

// ISN_AchievementProgressResult
struct  ISN_AchievementProgressResult_t113  : public ISN_Result_t114
{
	// AchievementTemplate ISN_AchievementProgressResult::_tpl
	AchievementTemplate_t115 * ____tpl_2;
};
