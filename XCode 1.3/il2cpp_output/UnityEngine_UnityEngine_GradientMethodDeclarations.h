﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Gradient
struct Gradient_t1378;
struct Gradient_t1378_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m7500 (Gradient_t1378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m7501 (Gradient_t1378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m7502 (Gradient_t1378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m7503 (Gradient_t1378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void Gradient_t1378_marshal(const Gradient_t1378& unmarshaled, Gradient_t1378_marshaled& marshaled);
extern "C" void Gradient_t1378_marshal_back(const Gradient_t1378_marshaled& marshaled, Gradient_t1378& unmarshaled);
extern "C" void Gradient_t1378_marshal_cleanup(Gradient_t1378_marshaled& marshaled);
