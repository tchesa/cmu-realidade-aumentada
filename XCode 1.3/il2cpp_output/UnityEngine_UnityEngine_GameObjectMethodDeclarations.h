﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t27;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t1218;
// UnityEngine.Component
struct Component_t56;
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// UnityEngine.Component[]
struct ComponentU5BU5D_t54;
// UnityEngine.Transform
struct Transform_t25;
// UnityEngine.Rigidbody
struct Rigidbody_t50;
// UnityEngine.Light
struct Light_t47;
// UnityEngine.Renderer
struct Renderer_t46;
// UnityEngine.AudioSource
struct AudioSource_t20;
// UnityEngine.GUIText
struct GUIText_t44;
// UnityEngine.GUITexture
struct GUITexture_t43;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_PrimitiveType.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"

// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" void GameObject__ctor_m403 (GameObject_t27 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor()
extern "C" void GameObject__ctor_m6754 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C" void GameObject__ctor_m6775 (GameObject_t27 * __this, String_t* ___name, TypeU5BU5D_t1218* ___components, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
extern "C" GameObject_t27 * GameObject_CreatePrimitive_m6490 (Object_t * __this /* static, unused */, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C" Component_t56 * GameObject_GetComponent_m7192 (GameObject_t27 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
extern "C" Component_t56 * GameObject_GetComponentInChildren_m7193 (GameObject_t27 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_isStatic(System.Boolean)
extern "C" void GameObject_set_isStatic_m7194 (GameObject_t27 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::GetComponentsForListInternal(System.Type,System.Type,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C" void GameObject_GetComponentsForListInternal_m7195 (GameObject_t27 * __this, Type_t * ___searchType, Type_t * ___listElementType, bool ___recursive, bool ___includeInactive, bool ___reverse, Object_t * ___resultList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C" ComponentU5BU5D_t54* GameObject_GetComponentsInternal_m7196 (GameObject_t27 * __this, Type_t * ___type, bool ___isGenericTypeArray, bool ___recursive, bool ___includeInactive, bool ___reverse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" Transform_t25 * GameObject_get_transform_m305 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.GameObject::get_rigidbody()
extern "C" Rigidbody_t50 * GameObject_get_rigidbody_m394 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Light UnityEngine.GameObject::get_light()
extern "C" Light_t47 * GameObject_get_light_m316 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer UnityEngine.GameObject::get_renderer()
extern "C" Renderer_t46 * GameObject_get_renderer_m314 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource UnityEngine.GameObject::get_audio()
extern "C" AudioSource_t20 * GameObject_get_audio_m322 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIText UnityEngine.GameObject::get_guiText()
extern "C" GUIText_t44 * GameObject_get_guiText_m311 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUITexture UnityEngine.GameObject::get_guiTexture()
extern "C" GUITexture_t43 * GameObject_get_guiTexture_m308 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C" int32_t GameObject_get_layer_m4273 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C" void GameObject_set_layer_m4274 (GameObject_t27 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" void GameObject_SetActive_m2344 (GameObject_t27 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" bool GameObject_get_activeInHierarchy_m4031 (GameObject_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void GameObject_SendMessage_m431 (GameObject_t27 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String)
extern "C" void GameObject_SendMessage_m2680 (GameObject_t27 * __this, String_t* ___methodName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" Component_t56 * GameObject_AddComponent_m7197 (GameObject_t27 * __this, Type_t * ___componentType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C" Component_t56 * GameObject_Internal_AddComponentWithType_m7198 (GameObject_t27 * __this, Type_t * ___componentType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C" void GameObject_Internal_CreateGameObject_m7199 (Object_t * __this /* static, unused */, GameObject_t27 * ___mono, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
