﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TerrainData
struct TerrainData_t1331;
// UnityEngine.GameObject
struct GameObject_t27;

#include "codegen/il2cpp-codegen.h"

// System.Boolean UnityEngine.TerrainData::HasUser(UnityEngine.GameObject)
extern "C" bool TerrainData_HasUser_m7223 (TerrainData_t1331 * __this, GameObject_t27 * ___user, const MethodInfo* method) IL2CPP_METHOD_ATTR;
