﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativeAppEvents
struct IOSNativeAppEvents_t80;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNativeAppEvents::.ctor()
extern "C" void IOSNativeAppEvents__ctor_m516 (IOSNativeAppEvents_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::.cctor()
extern "C" void IOSNativeAppEvents__cctor_m517 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::_ISNsubscribe()
extern "C" void IOSNativeAppEvents__ISNsubscribe_m518 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSNativeAppEvents IOSNativeAppEvents::get_instance()
extern "C" IOSNativeAppEvents_t80 * IOSNativeAppEvents_get_instance_m519 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::Awake()
extern "C" void IOSNativeAppEvents_Awake_m520 (IOSNativeAppEvents_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationDidEnterBackground()
extern "C" void IOSNativeAppEvents_applicationDidEnterBackground_m521 (IOSNativeAppEvents_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationDidBecomeActive()
extern "C" void IOSNativeAppEvents_applicationDidBecomeActive_m522 (IOSNativeAppEvents_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationDidReceiveMemoryWarning()
extern "C" void IOSNativeAppEvents_applicationDidReceiveMemoryWarning_m523 (IOSNativeAppEvents_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationWillResignActive()
extern "C" void IOSNativeAppEvents_applicationWillResignActive_m524 (IOSNativeAppEvents_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationWillTerminate()
extern "C" void IOSNativeAppEvents_applicationWillTerminate_m525 (IOSNativeAppEvents_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationDidEnterBackground>m__0()
extern "C" void IOSNativeAppEvents_U3COnApplicationDidEnterBackgroundU3Em__0_m526 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationDidBecomeActive>m__1()
extern "C" void IOSNativeAppEvents_U3COnApplicationDidBecomeActiveU3Em__1_m527 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationDidReceiveMemoryWarning>m__2()
extern "C" void IOSNativeAppEvents_U3COnApplicationDidReceiveMemoryWarningU3Em__2_m528 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationWillResignActive>m__3()
extern "C" void IOSNativeAppEvents_U3COnApplicationWillResignActiveU3Em__3_m529 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationWillTerminate>m__4()
extern "C" void IOSNativeAppEvents_U3COnApplicationWillTerminateU3Em__4_m530 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
