﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t3282;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void DefaultComparer__ctor_m26440_gshared (DefaultComparer_t3282 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m26440(__this, method) (( void (*) (DefaultComparer_t3282 *, const MethodInfo*))DefaultComparer__ctor_m26440_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m26441_gshared (DefaultComparer_t3282 * __this, TargetSearchResult_t1050  ___x, TargetSearchResult_t1050  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m26441(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3282 *, TargetSearchResult_t1050 , TargetSearchResult_t1050 , const MethodInfo*))DefaultComparer_Compare_m26441_gshared)(__this, ___x, ___y, method)
