﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_ShurikenThreadFix
struct CFX_ShurikenThreadFix_t333;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_ShurikenThreadFix::.ctor()
extern "C" void CFX_ShurikenThreadFix__ctor_m1789 (CFX_ShurikenThreadFix_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_ShurikenThreadFix::OnEnable()
extern "C" void CFX_ShurikenThreadFix_OnEnable_m1790 (CFX_ShurikenThreadFix_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX_ShurikenThreadFix::WaitFrame()
extern "C" Object_t * CFX_ShurikenThreadFix_WaitFrame_m1791 (CFX_ShurikenThreadFix_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
