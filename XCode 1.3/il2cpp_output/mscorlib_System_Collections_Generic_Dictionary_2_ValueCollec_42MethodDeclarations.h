﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_34MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m17837(__this, ___dictionary, method) (( void (*) (ValueCollection_t2746 *, Dictionary_2_t174 *, const MethodInfo*))ValueCollection__ctor_m16437_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17838(__this, ___item, method) (( void (*) (ValueCollection_t2746 *, iAdBanner_t172 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16438_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17839(__this, method) (( void (*) (ValueCollection_t2746 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16439_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17840(__this, ___item, method) (( bool (*) (ValueCollection_t2746 *, iAdBanner_t172 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16440_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17841(__this, ___item, method) (( bool (*) (ValueCollection_t2746 *, iAdBanner_t172 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16441_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17842(__this, method) (( Object_t* (*) (ValueCollection_t2746 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16442_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m17843(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2746 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16443_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17844(__this, method) (( Object_t * (*) (ValueCollection_t2746 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16444_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17845(__this, method) (( bool (*) (ValueCollection_t2746 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16445_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17846(__this, method) (( bool (*) (ValueCollection_t2746 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16446_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m17847(__this, method) (( Object_t * (*) (ValueCollection_t2746 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16447_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m17848(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2746 *, iAdBannerU5BU5D_t2738*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m16448_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::GetEnumerator()
#define ValueCollection_GetEnumerator_m17849(__this, method) (( Enumerator_t3639  (*) (ValueCollection_t2746 *, const MethodInfo*))ValueCollection_GetEnumerator_m16449_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,iAdBanner>::get_Count()
#define ValueCollection_get_Count_m17850(__this, method) (( int32_t (*) (ValueCollection_t2746 *, const MethodInfo*))ValueCollection_get_Count_m16450_gshared)(__this, method)
