﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationControllerScript
struct AnimationControllerScript_t364;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// AnimationControllerScript/TimeEvent
struct TimeEvent_t362;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationControllerScript::.ctor()
extern "C" void AnimationControllerScript__ctor_m1882 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript::Start()
extern "C" void AnimationControllerScript_Start_m1883 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript::Update()
extern "C" void AnimationControllerScript_Update_m1884 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript::On()
extern "C" void AnimationControllerScript_On_m1885 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript::Off()
extern "C" void AnimationControllerScript_Off_m1886 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationControllerScript::ApareceLampada()
extern "C" Object_t * AnimationControllerScript_ApareceLampada_m1887 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationControllerScript::ApareceProveta()
extern "C" Object_t * AnimationControllerScript_ApareceProveta_m1888 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationControllerScript::DesapareceProveta()
extern "C" Object_t * AnimationControllerScript_DesapareceProveta_m1889 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationControllerScript::ApareceSaleiro()
extern "C" Object_t * AnimationControllerScript_ApareceSaleiro_m1890 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationControllerScript::LiquidoProveta()
extern "C" Object_t * AnimationControllerScript_LiquidoProveta_m1891 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationControllerScript::AtivaParticulaPortal()
extern "C" Object_t * AnimationControllerScript_AtivaParticulaPortal_m1892 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnimationControllerScript::EmmitSmoke()
extern "C" Object_t * AnimationControllerScript_EmmitSmoke_m1893 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationControllerScript::OnGUI()
extern "C" void AnimationControllerScript_OnGUI_m1894 (AnimationControllerScript_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AnimationControllerScript::<On>m__3A(AnimationControllerScript/TimeEvent,AnimationControllerScript/TimeEvent)
extern "C" int32_t AnimationControllerScript_U3COnU3Em__3A_m1895 (Object_t * __this /* static, unused */, TimeEvent_t362 * ___a, TimeEvent_t362 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
