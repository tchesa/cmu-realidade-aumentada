﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ILoadLevelEventHandler[]
struct ILoadLevelEventHandlerU5BU5D_t3194;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>
struct  List_1_t1033  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::_items
	ILoadLevelEventHandlerU5BU5D_t3194* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t1033_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::EmptyArray
	ILoadLevelEventHandlerU5BU5D_t3194* ___EmptyArray_4;
};
