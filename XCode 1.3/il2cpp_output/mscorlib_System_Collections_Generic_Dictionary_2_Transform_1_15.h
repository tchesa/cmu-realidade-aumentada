﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GCLeaderboard
struct GCLeaderboard_t121;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.String,GCLeaderboard,System.Collections.DictionaryEntry>
struct  Transform_1_t2703  : public MulticastDelegate_t10
{
};
