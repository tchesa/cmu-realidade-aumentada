﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// ISN_Error
struct  ISN_Error_t85  : public Object_t
{
	// System.Int32 ISN_Error::code
	int32_t ___code_0;
	// System.String ISN_Error::description
	String_t* ___description_1;
};
