﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t3058;
// System.Collections.Generic.IEqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>
struct IEqualityComparer_1_t3057;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1438;
// System.Collections.ICollection
struct ICollection_t1653;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
struct KeyValuePair_2U5BU5D_t3688;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>
struct IEnumerator_1_t3689;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1489;
// System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct KeyCollection_t3063;
// System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct ValueCollection_t3067;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m22689_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m22689(__this, method) (( void (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2__ctor_m22689_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22691_gshared (Dictionary_2_t3058 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22691(__this, ___comparer, method) (( void (*) (Dictionary_2_t3058 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22691_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m22693_gshared (Dictionary_2_t3058 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m22693(__this, ___capacity, method) (( void (*) (Dictionary_2_t3058 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22693_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m22695_gshared (Dictionary_2_t3058 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m22695(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3058 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2__ctor_m22695_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m22697_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m22697(__this, method) (( Object_t * (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m22697_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m22699_gshared (Dictionary_2_t3058 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22699(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3058 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22699_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22701_gshared (Dictionary_2_t3058 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22701(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3058 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22701_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22703_gshared (Dictionary_2_t3058 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22703(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3058 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22703_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m22705_gshared (Dictionary_2_t3058 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m22705(__this, ___key, method) (( bool (*) (Dictionary_2_t3058 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22705_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22707_gshared (Dictionary_2_t3058 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22707(__this, ___key, method) (( void (*) (Dictionary_2_t3058 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22707_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22709_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22709(__this, method) (( bool (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22709_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22711_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22711(__this, method) (( Object_t * (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22711_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22713_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22713(__this, method) (( bool (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22713_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22715_gshared (Dictionary_2_t3058 * __this, KeyValuePair_2_t3060  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22715(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3058 *, KeyValuePair_2_t3060 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22715_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22717_gshared (Dictionary_2_t3058 * __this, KeyValuePair_2_t3060  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22717(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3058 *, KeyValuePair_2_t3060 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22717_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22719_gshared (Dictionary_2_t3058 * __this, KeyValuePair_2U5BU5D_t3688* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22719(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3058 *, KeyValuePair_2U5BU5D_t3688*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22719_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22721_gshared (Dictionary_2_t3058 * __this, KeyValuePair_2_t3060  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22721(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3058 *, KeyValuePair_2_t3060 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22721_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22723_gshared (Dictionary_2_t3058 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22723(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3058 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22723_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22725_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22725(__this, method) (( Object_t * (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22725_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22727_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22727(__this, method) (( Object_t* (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22727_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22729_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22729(__this, method) (( Object_t * (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22729_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22731_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22731(__this, method) (( int32_t (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_get_Count_m22731_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m22733_gshared (Dictionary_2_t3058 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m22733(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3058 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m22733_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22735_gshared (Dictionary_2_t3058 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m22735(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3058 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m22735_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22737_gshared (Dictionary_2_t3058 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m22737(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3058 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22737_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22739_gshared (Dictionary_2_t3058 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m22739(__this, ___size, method) (( void (*) (Dictionary_2_t3058 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22739_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22741_gshared (Dictionary_2_t3058 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22741(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3058 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22741_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3060  Dictionary_2_make_pair_m22743_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m22743(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3060  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m22743_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m22745_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m22745(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m22745_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m22747_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m22747(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m22747_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22749_gshared (Dictionary_2_t3058 * __this, KeyValuePair_2U5BU5D_t3688* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m22749(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3058 *, KeyValuePair_2U5BU5D_t3688*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22749_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m22751_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m22751(__this, method) (( void (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_Resize_m22751_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22753_gshared (Dictionary_2_t3058 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m22753(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3058 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m22753_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m22755_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m22755(__this, method) (( void (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_Clear_m22755_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22757_gshared (Dictionary_2_t3058 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m22757(__this, ___key, method) (( bool (*) (Dictionary_2_t3058 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m22757_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22759_gshared (Dictionary_2_t3058 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m22759(__this, ___value, method) (( bool (*) (Dictionary_2_t3058 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m22759_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22761_gshared (Dictionary_2_t3058 * __this, SerializationInfo_t1438 * ___info, StreamingContext_t1439  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m22761(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3058 *, SerializationInfo_t1438 *, StreamingContext_t1439 , const MethodInfo*))Dictionary_2_GetObjectData_m22761_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22763_gshared (Dictionary_2_t3058 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22763(__this, ___sender, method) (( void (*) (Dictionary_2_t3058 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22763_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22765_gshared (Dictionary_2_t3058 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m22765(__this, ___key, method) (( bool (*) (Dictionary_2_t3058 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m22765_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22767_gshared (Dictionary_2_t3058 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m22767(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3058 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m22767_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Keys()
extern "C" KeyCollection_t3063 * Dictionary_2_get_Keys_m22769_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m22769(__this, method) (( KeyCollection_t3063 * (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_get_Keys_m22769_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Values()
extern "C" ValueCollection_t3067 * Dictionary_2_get_Values_m22770_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m22770(__this, method) (( ValueCollection_t3067 * (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_get_Values_m22770_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m22772_gshared (Dictionary_2_t3058 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m22772(__this, ___key, method) (( int32_t (*) (Dictionary_2_t3058 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22772_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m22774_gshared (Dictionary_2_t3058 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m22774(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t3058 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22774_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22776_gshared (Dictionary_2_t3058 * __this, KeyValuePair_2_t3060  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22776(__this, ___pair, method) (( bool (*) (Dictionary_2_t3058 *, KeyValuePair_2_t3060 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22776_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::GetEnumerator()
extern "C" Enumerator_t3065  Dictionary_2_GetEnumerator_m22778_gshared (Dictionary_2_t3058 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22778(__this, method) (( Enumerator_t3065  (*) (Dictionary_2_t3058 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22778_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t58  Dictionary_2_U3CCopyToU3Em__0_m22780_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22780(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t58  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22780_gshared)(__this /* static, unused */, ___key, ___value, method)
