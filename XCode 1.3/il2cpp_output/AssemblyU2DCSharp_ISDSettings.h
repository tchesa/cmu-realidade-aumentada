﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t78;
// ISDSettings
struct ISDSettings_t76;

#include "UnityEngine_UnityEngine_ScriptableObject.h"

// ISDSettings
struct  ISDSettings_t76  : public ScriptableObject_t77
{
	// System.Boolean ISDSettings::IsFrameworksSettingOpen
	bool ___IsFrameworksSettingOpen_5;
	// System.Boolean ISDSettings::IsLinkerSettingOpen
	bool ___IsLinkerSettingOpen_6;
	// System.Boolean ISDSettings::IsCompilerSettingsOpen
	bool ___IsCompilerSettingsOpen_7;
	// System.Collections.Generic.List`1<System.String> ISDSettings::frameworks
	List_1_t78 * ___frameworks_8;
	// System.Collections.Generic.List`1<System.String> ISDSettings::compileFlags
	List_1_t78 * ___compileFlags_9;
	// System.Collections.Generic.List`1<System.String> ISDSettings::linkFlags
	List_1_t78 * ___linkFlags_10;
};
struct ISDSettings_t76_StaticFields{
	// ISDSettings ISDSettings::instance
	ISDSettings_t76 * ___instance_11;
};
