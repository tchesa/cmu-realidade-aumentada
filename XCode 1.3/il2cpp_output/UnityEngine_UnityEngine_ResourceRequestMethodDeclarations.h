﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ResourceRequest
struct ResourceRequest_t1356;
// UnityEngine.Object
struct Object_t53;
struct Object_t53_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m7391 (ResourceRequest_t1356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t53 * ResourceRequest_get_asset_m7392 (ResourceRequest_t1356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
