﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t3393;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1440;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3750;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t860;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m27929_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1__ctor_m27929(__this, method) (( void (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1__ctor_m27929_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27930_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27930(__this, method) (( bool (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27930_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m27931_gshared (Collection_1_t3393 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m27931(__this, ___array, ___index, method) (( void (*) (Collection_1_t3393 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m27931_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m27932_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m27932(__this, method) (( Object_t * (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m27932_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m27933_gshared (Collection_1_t3393 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m27933(__this, ___value, method) (( int32_t (*) (Collection_1_t3393 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m27933_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m27934_gshared (Collection_1_t3393 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m27934(__this, ___value, method) (( bool (*) (Collection_1_t3393 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m27934_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m27935_gshared (Collection_1_t3393 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m27935(__this, ___value, method) (( int32_t (*) (Collection_1_t3393 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m27935_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m27936_gshared (Collection_1_t3393 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m27936(__this, ___index, ___value, method) (( void (*) (Collection_1_t3393 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m27936_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m27937_gshared (Collection_1_t3393 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m27937(__this, ___value, method) (( void (*) (Collection_1_t3393 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m27937_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m27938_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m27938(__this, method) (( bool (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m27938_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m27939_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m27939(__this, method) (( Object_t * (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m27939_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m27940_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m27940(__this, method) (( bool (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m27940_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m27941_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m27941(__this, method) (( bool (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m27941_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m27942_gshared (Collection_1_t3393 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m27942(__this, ___index, method) (( Object_t * (*) (Collection_1_t3393 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m27942_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m27943_gshared (Collection_1_t3393 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m27943(__this, ___index, ___value, method) (( void (*) (Collection_1_t3393 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m27943_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m27944_gshared (Collection_1_t3393 * __this, UICharInfo_t859  ___item, const MethodInfo* method);
#define Collection_1_Add_m27944(__this, ___item, method) (( void (*) (Collection_1_t3393 *, UICharInfo_t859 , const MethodInfo*))Collection_1_Add_m27944_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m27945_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_Clear_m27945(__this, method) (( void (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_Clear_m27945_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m27946_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m27946(__this, method) (( void (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_ClearItems_m27946_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m27947_gshared (Collection_1_t3393 * __this, UICharInfo_t859  ___item, const MethodInfo* method);
#define Collection_1_Contains_m27947(__this, ___item, method) (( bool (*) (Collection_1_t3393 *, UICharInfo_t859 , const MethodInfo*))Collection_1_Contains_m27947_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m27948_gshared (Collection_1_t3393 * __this, UICharInfoU5BU5D_t1440* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m27948(__this, ___array, ___index, method) (( void (*) (Collection_1_t3393 *, UICharInfoU5BU5D_t1440*, int32_t, const MethodInfo*))Collection_1_CopyTo_m27948_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m27949_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m27949(__this, method) (( Object_t* (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_GetEnumerator_m27949_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m27950_gshared (Collection_1_t3393 * __this, UICharInfo_t859  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m27950(__this, ___item, method) (( int32_t (*) (Collection_1_t3393 *, UICharInfo_t859 , const MethodInfo*))Collection_1_IndexOf_m27950_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m27951_gshared (Collection_1_t3393 * __this, int32_t ___index, UICharInfo_t859  ___item, const MethodInfo* method);
#define Collection_1_Insert_m27951(__this, ___index, ___item, method) (( void (*) (Collection_1_t3393 *, int32_t, UICharInfo_t859 , const MethodInfo*))Collection_1_Insert_m27951_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m27952_gshared (Collection_1_t3393 * __this, int32_t ___index, UICharInfo_t859  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m27952(__this, ___index, ___item, method) (( void (*) (Collection_1_t3393 *, int32_t, UICharInfo_t859 , const MethodInfo*))Collection_1_InsertItem_m27952_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m27953_gshared (Collection_1_t3393 * __this, UICharInfo_t859  ___item, const MethodInfo* method);
#define Collection_1_Remove_m27953(__this, ___item, method) (( bool (*) (Collection_1_t3393 *, UICharInfo_t859 , const MethodInfo*))Collection_1_Remove_m27953_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m27954_gshared (Collection_1_t3393 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m27954(__this, ___index, method) (( void (*) (Collection_1_t3393 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m27954_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m27955_gshared (Collection_1_t3393 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m27955(__this, ___index, method) (( void (*) (Collection_1_t3393 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m27955_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m27956_gshared (Collection_1_t3393 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m27956(__this, method) (( int32_t (*) (Collection_1_t3393 *, const MethodInfo*))Collection_1_get_Count_m27956_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t859  Collection_1_get_Item_m27957_gshared (Collection_1_t3393 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m27957(__this, ___index, method) (( UICharInfo_t859  (*) (Collection_1_t3393 *, int32_t, const MethodInfo*))Collection_1_get_Item_m27957_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m27958_gshared (Collection_1_t3393 * __this, int32_t ___index, UICharInfo_t859  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m27958(__this, ___index, ___value, method) (( void (*) (Collection_1_t3393 *, int32_t, UICharInfo_t859 , const MethodInfo*))Collection_1_set_Item_m27958_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m27959_gshared (Collection_1_t3393 * __this, int32_t ___index, UICharInfo_t859  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m27959(__this, ___index, ___item, method) (( void (*) (Collection_1_t3393 *, int32_t, UICharInfo_t859 , const MethodInfo*))Collection_1_SetItem_m27959_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m27960_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m27960(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m27960_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t859  Collection_1_ConvertItem_m27961_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m27961(__this /* static, unused */, ___item, method) (( UICharInfo_t859  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m27961_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m27962_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m27962(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m27962_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m27963_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m27963(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m27963_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m27964_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m27964(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m27964_gshared)(__this /* static, unused */, ___list, method)
