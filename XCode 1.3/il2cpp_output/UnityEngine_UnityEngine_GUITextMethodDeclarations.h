﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIText
struct GUIText_t44;
// UnityEngine.Material
struct Material_t45;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C" Material_t45 * GUIText_get_material_m312 (GUIText_t44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
