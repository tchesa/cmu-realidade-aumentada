﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSDateTimePicker
struct IOSDateTimePicker_t150;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// ISN_Singleton`1<IOSDateTimePicker>
struct  ISN_Singleton_1_t151  : public EventDispatcher_t69
{
};
struct ISN_Singleton_1_t151_StaticFields{
	// T ISN_Singleton`1<IOSDateTimePicker>::_instance
	IOSDateTimePicker_t150 * ____instance_3;
	// System.Boolean ISN_Singleton`1<IOSDateTimePicker>::applicationIsQuitting
	bool ___applicationIsQuitting_4;
};
