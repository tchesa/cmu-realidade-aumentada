﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m17118(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2704 *, String_t*, GCLeaderboard_t121 *, const MethodInfo*))KeyValuePair_2__ctor_m16119_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>::get_Key()
#define KeyValuePair_2_get_Key_m17119(__this, method) (( String_t* (*) (KeyValuePair_2_t2704 *, const MethodInfo*))KeyValuePair_2_get_Key_m16120_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17120(__this, ___value, method) (( void (*) (KeyValuePair_2_t2704 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m16121_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>::get_Value()
#define KeyValuePair_2_get_Value_m17121(__this, method) (( GCLeaderboard_t121 * (*) (KeyValuePair_2_t2704 *, const MethodInfo*))KeyValuePair_2_get_Value_m16122_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17122(__this, ___value, method) (( void (*) (KeyValuePair_2_t2704 *, GCLeaderboard_t121 *, const MethodInfo*))KeyValuePair_2_set_Value_m16123_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GCLeaderboard>::ToString()
#define KeyValuePair_2_ToString_m17123(__this, method) (( String_t* (*) (KeyValuePair_2_t2704 *, const MethodInfo*))KeyValuePair_2_ToString_m16124_gshared)(__this, method)
