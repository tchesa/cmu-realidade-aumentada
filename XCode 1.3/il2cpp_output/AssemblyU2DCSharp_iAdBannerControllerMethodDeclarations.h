﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iAdBannerController
struct iAdBannerController_t173;
// iAdBanner
struct iAdBanner_t172;
// System.Collections.Generic.List`1<iAdBanner>
struct List_1_t462;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"

// System.Void iAdBannerController::.ctor()
extern "C" void iAdBannerController__ctor_m966 (iAdBannerController_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::.cctor()
extern "C" void iAdBannerController__cctor_m967 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::_IADDestroyBanner(System.Int32)
extern "C" void iAdBannerController__IADDestroyBanner_m968 (Object_t * __this /* static, unused */, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::_IADStartInterstitialAd()
extern "C" void iAdBannerController__IADStartInterstitialAd_m969 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::_IADLoadInterstitialAd()
extern "C" void iAdBannerController__IADLoadInterstitialAd_m970 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::_IADShowInterstitialAd()
extern "C" void iAdBannerController__IADShowInterstitialAd_m971 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::Awake()
extern "C" void iAdBannerController_Awake_m972 (iAdBannerController_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// iAdBannerController iAdBannerController::get_instance()
extern "C" iAdBannerController_t173 * iAdBannerController_get_instance_m973 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// iAdBanner iAdBannerController::CreateAdBanner(UnityEngine.TextAnchor)
extern "C" iAdBanner_t172 * iAdBannerController_CreateAdBanner_m974 (iAdBannerController_t173 * __this, int32_t ___anchor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// iAdBanner iAdBannerController::CreateAdBanner(System.Int32,System.Int32)
extern "C" iAdBanner_t172 * iAdBannerController_CreateAdBanner_m975 (iAdBannerController_t173 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::DestroyBanner(System.Int32)
extern "C" void iAdBannerController_DestroyBanner_m976 (iAdBannerController_t173 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::StartInterstitialAd()
extern "C" void iAdBannerController_StartInterstitialAd_m977 (iAdBannerController_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::LoadInterstitialAd()
extern "C" void iAdBannerController_LoadInterstitialAd_m978 (iAdBannerController_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::ShowInterstitialAd()
extern "C" void iAdBannerController_ShowInterstitialAd_m979 (iAdBannerController_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 iAdBannerController::get_nextId()
extern "C" int32_t iAdBannerController_get_nextId_m980 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// iAdBanner iAdBannerController::GetBanner(System.Int32)
extern "C" iAdBanner_t172 * iAdBannerController_GetBanner_m981 (iAdBannerController_t173 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<iAdBanner> iAdBannerController::get_banners()
extern "C" List_1_t462 * iAdBannerController_get_banners_m982 (iAdBannerController_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::didFailToReceiveAdWithError(System.String)
extern "C" void iAdBannerController_didFailToReceiveAdWithError_m983 (iAdBannerController_t173 * __this, String_t* ___bannerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::bannerViewDidLoadAd(System.String)
extern "C" void iAdBannerController_bannerViewDidLoadAd_m984 (iAdBannerController_t173 * __this, String_t* ___bannerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::bannerViewWillLoadAd(System.String)
extern "C" void iAdBannerController_bannerViewWillLoadAd_m985 (iAdBannerController_t173 * __this, String_t* ___bannerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::bannerViewActionDidFinish(System.String)
extern "C" void iAdBannerController_bannerViewActionDidFinish_m986 (iAdBannerController_t173 * __this, String_t* ___bannerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::bannerViewActionShouldBegin(System.String)
extern "C" void iAdBannerController_bannerViewActionShouldBegin_m987 (iAdBannerController_t173 * __this, String_t* ___bannerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::interstitialdidFailWithError(System.String)
extern "C" void iAdBannerController_interstitialdidFailWithError_m988 (iAdBannerController_t173 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::interstitialAdWillLoad(System.String)
extern "C" void iAdBannerController_interstitialAdWillLoad_m989 (iAdBannerController_t173 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::interstitialAdDidLoad(System.String)
extern "C" void iAdBannerController_interstitialAdDidLoad_m990 (iAdBannerController_t173 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::interstitialAdActionDidFinish(System.String)
extern "C" void iAdBannerController_interstitialAdActionDidFinish_m991 (iAdBannerController_t173 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::<InterstitialDidFailWithErrorAction>m__2B()
extern "C" void iAdBannerController_U3CInterstitialDidFailWithErrorActionU3Em__2B_m992 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::<InterstitialAdWillLoadAction>m__2C()
extern "C" void iAdBannerController_U3CInterstitialAdWillLoadActionU3Em__2C_m993 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::<InterstitialAdDidLoadAction>m__2D()
extern "C" void iAdBannerController_U3CInterstitialAdDidLoadActionU3Em__2D_m994 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iAdBannerController::<InterstitialAdDidFinishAction>m__2E()
extern "C" void iAdBannerController_U3CInterstitialAdDidFinishActionU3Em__2E_m995 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
