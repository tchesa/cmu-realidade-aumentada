﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Byte>
struct List_1_t497;
// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t3628;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t3629;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t3630;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Byte>
struct ReadOnlyCollection_1_t2710;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.Predicate`1<System.Byte>
struct Predicate_1_t2716;
// System.Comparison`1<System.Byte>
struct Comparison_1_t2720;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_34.h"

// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor()
extern "C" void List_1__ctor_m2216_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1__ctor_m2216(__this, method) (( void (*) (List_1_t497 *, const MethodInfo*))List_1__ctor_m2216_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17178_gshared (List_1_t497 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17178(__this, ___collection, method) (( void (*) (List_1_t497 *, Object_t*, const MethodInfo*))List_1__ctor_m17178_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor(System.Int32)
extern "C" void List_1__ctor_m17179_gshared (List_1_t497 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m17179(__this, ___capacity, method) (( void (*) (List_1_t497 *, int32_t, const MethodInfo*))List_1__ctor_m17179_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::.cctor()
extern "C" void List_1__cctor_m17180_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17180(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17180_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Byte>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17181_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17181(__this, method) (( Object_t* (*) (List_1_t497 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17181_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17182_gshared (List_1_t497 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17182(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t497 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17182_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17183_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17183(__this, method) (( Object_t * (*) (List_1_t497 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17183_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17184_gshared (List_1_t497 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17184(__this, ___item, method) (( int32_t (*) (List_1_t497 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17184_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17185_gshared (List_1_t497 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17185(__this, ___item, method) (( bool (*) (List_1_t497 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17185_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17186_gshared (List_1_t497 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17186(__this, ___item, method) (( int32_t (*) (List_1_t497 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17186_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17187_gshared (List_1_t497 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17187(__this, ___index, ___item, method) (( void (*) (List_1_t497 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17187_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17188_gshared (List_1_t497 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17188(__this, ___item, method) (( void (*) (List_1_t497 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17188_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17189_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17189(__this, method) (( bool (*) (List_1_t497 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17189_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17190_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17190(__this, method) (( bool (*) (List_1_t497 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17191_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17191(__this, method) (( Object_t * (*) (List_1_t497 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17192_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17192(__this, method) (( bool (*) (List_1_t497 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17192_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17193_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17193(__this, method) (( bool (*) (List_1_t497 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17193_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17194_gshared (List_1_t497 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17194(__this, ___index, method) (( Object_t * (*) (List_1_t497 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17194_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17195_gshared (List_1_t497 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17195(__this, ___index, ___value, method) (( void (*) (List_1_t497 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17195_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Add(T)
extern "C" void List_1_Add_m17196_gshared (List_1_t497 * __this, uint8_t ___item, const MethodInfo* method);
#define List_1_Add_m17196(__this, ___item, method) (( void (*) (List_1_t497 *, uint8_t, const MethodInfo*))List_1_Add_m17196_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17197_gshared (List_1_t497 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17197(__this, ___newCount, method) (( void (*) (List_1_t497 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17197_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17198_gshared (List_1_t497 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17198(__this, ___collection, method) (( void (*) (List_1_t497 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17198_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17199_gshared (List_1_t497 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17199(__this, ___enumerable, method) (( void (*) (List_1_t497 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17199_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m17200_gshared (List_1_t497 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m17200(__this, ___collection, method) (( void (*) (List_1_t497 *, Object_t*, const MethodInfo*))List_1_AddRange_m17200_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Byte>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2710 * List_1_AsReadOnly_m17201_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17201(__this, method) (( ReadOnlyCollection_1_t2710 * (*) (List_1_t497 *, const MethodInfo*))List_1_AsReadOnly_m17201_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Clear()
extern "C" void List_1_Clear_m17202_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_Clear_m17202(__this, method) (( void (*) (List_1_t497 *, const MethodInfo*))List_1_Clear_m17202_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::Contains(T)
extern "C" bool List_1_Contains_m17203_gshared (List_1_t497 * __this, uint8_t ___item, const MethodInfo* method);
#define List_1_Contains_m17203(__this, ___item, method) (( bool (*) (List_1_t497 *, uint8_t, const MethodInfo*))List_1_Contains_m17203_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17204_gshared (List_1_t497 * __this, ByteU5BU5D_t119* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17204(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t497 *, ByteU5BU5D_t119*, int32_t, const MethodInfo*))List_1_CopyTo_m17204_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Byte>::Find(System.Predicate`1<T>)
extern "C" uint8_t List_1_Find_m17205_gshared (List_1_t497 * __this, Predicate_1_t2716 * ___match, const MethodInfo* method);
#define List_1_Find_m17205(__this, ___match, method) (( uint8_t (*) (List_1_t497 *, Predicate_1_t2716 *, const MethodInfo*))List_1_Find_m17205_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17206_gshared (Object_t * __this /* static, unused */, Predicate_1_t2716 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17206(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2716 *, const MethodInfo*))List_1_CheckMatch_m17206_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17207_gshared (List_1_t497 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2716 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17207(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t497 *, int32_t, int32_t, Predicate_1_t2716 *, const MethodInfo*))List_1_GetIndex_m17207_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Byte>::GetEnumerator()
extern "C" Enumerator_t2709  List_1_GetEnumerator_m17208_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17208(__this, method) (( Enumerator_t2709  (*) (List_1_t497 *, const MethodInfo*))List_1_GetEnumerator_m17208_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17209_gshared (List_1_t497 * __this, uint8_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m17209(__this, ___item, method) (( int32_t (*) (List_1_t497 *, uint8_t, const MethodInfo*))List_1_IndexOf_m17209_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17210_gshared (List_1_t497 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17210(__this, ___start, ___delta, method) (( void (*) (List_1_t497 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17210_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17211_gshared (List_1_t497 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17211(__this, ___index, method) (( void (*) (List_1_t497 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17211_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17212_gshared (List_1_t497 * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define List_1_Insert_m17212(__this, ___index, ___item, method) (( void (*) (List_1_t497 *, int32_t, uint8_t, const MethodInfo*))List_1_Insert_m17212_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17213_gshared (List_1_t497 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17213(__this, ___collection, method) (( void (*) (List_1_t497 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17213_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::Remove(T)
extern "C" bool List_1_Remove_m17214_gshared (List_1_t497 * __this, uint8_t ___item, const MethodInfo* method);
#define List_1_Remove_m17214(__this, ___item, method) (( bool (*) (List_1_t497 *, uint8_t, const MethodInfo*))List_1_Remove_m17214_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17215_gshared (List_1_t497 * __this, Predicate_1_t2716 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17215(__this, ___match, method) (( int32_t (*) (List_1_t497 *, Predicate_1_t2716 *, const MethodInfo*))List_1_RemoveAll_m17215_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17216_gshared (List_1_t497 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17216(__this, ___index, method) (( void (*) (List_1_t497 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17216_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Reverse()
extern "C" void List_1_Reverse_m17217_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_Reverse_m17217(__this, method) (( void (*) (List_1_t497 *, const MethodInfo*))List_1_Reverse_m17217_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Sort()
extern "C" void List_1_Sort_m17218_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_Sort_m17218(__this, method) (( void (*) (List_1_t497 *, const MethodInfo*))List_1_Sort_m17218_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17219_gshared (List_1_t497 * __this, Comparison_1_t2720 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17219(__this, ___comparison, method) (( void (*) (List_1_t497 *, Comparison_1_t2720 *, const MethodInfo*))List_1_Sort_m17219_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Byte>::ToArray()
extern "C" ByteU5BU5D_t119* List_1_ToArray_m2218_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_ToArray_m2218(__this, method) (( ByteU5BU5D_t119* (*) (List_1_t497 *, const MethodInfo*))List_1_ToArray_m2218_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::TrimExcess()
extern "C" void List_1_TrimExcess_m17220_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17220(__this, method) (( void (*) (List_1_t497 *, const MethodInfo*))List_1_TrimExcess_m17220_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m17221_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m17221(__this, method) (( int32_t (*) (List_1_t497 *, const MethodInfo*))List_1_get_Capacity_m17221_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m17222_gshared (List_1_t497 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m17222(__this, ___value, method) (( void (*) (List_1_t497 *, int32_t, const MethodInfo*))List_1_set_Capacity_m17222_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::get_Count()
extern "C" int32_t List_1_get_Count_m17223_gshared (List_1_t497 * __this, const MethodInfo* method);
#define List_1_get_Count_m17223(__this, method) (( int32_t (*) (List_1_t497 *, const MethodInfo*))List_1_get_Count_m17223_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Byte>::get_Item(System.Int32)
extern "C" uint8_t List_1_get_Item_m17224_gshared (List_1_t497 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17224(__this, ___index, method) (( uint8_t (*) (List_1_t497 *, int32_t, const MethodInfo*))List_1_get_Item_m17224_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17225_gshared (List_1_t497 * __this, int32_t ___index, uint8_t ___value, const MethodInfo* method);
#define List_1_set_Item_m17225(__this, ___index, ___value, method) (( void (*) (List_1_t497 *, int32_t, uint8_t, const MethodInfo*))List_1_set_Item_m17225_gshared)(__this, ___index, ___value, method)
