﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m16014(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2629 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15544_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16015(__this, method) (( void (*) (InternalEnumerator_1_t2629 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15545_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Material>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16016(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2629 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15546_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::Dispose()
#define InternalEnumerator_1_Dispose_m16017(__this, method) (( void (*) (InternalEnumerator_1_t2629 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15547_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Material>::MoveNext()
#define InternalEnumerator_1_MoveNext_m16018(__this, method) (( bool (*) (InternalEnumerator_1_t2629 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15548_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Material>::get_Current()
#define InternalEnumerator_1_get_Current_m16019(__this, method) (( Material_t45 * (*) (InternalEnumerator_1_t2629 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15549_gshared)(__this, method)
