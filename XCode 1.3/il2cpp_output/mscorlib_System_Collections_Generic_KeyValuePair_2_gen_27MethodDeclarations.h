﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m22787_gshared (KeyValuePair_2_t3060 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m22787(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3060 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m22787_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m22788_gshared (KeyValuePair_2_t3060 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m22788(__this, method) (( int32_t (*) (KeyValuePair_2_t3060 *, const MethodInfo*))KeyValuePair_2_get_Key_m22788_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m22789_gshared (KeyValuePair_2_t3060 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m22789(__this, ___value, method) (( void (*) (KeyValuePair_2_t3060 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m22789_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m22790_gshared (KeyValuePair_2_t3060 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m22790(__this, method) (( Object_t * (*) (KeyValuePair_2_t3060 *, const MethodInfo*))KeyValuePair_2_get_Value_m22790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m22791_gshared (KeyValuePair_2_t3060 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m22791(__this, ___value, method) (( void (*) (KeyValuePair_2_t3060 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m22791_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m22792_gshared (KeyValuePair_2_t3060 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m22792(__this, method) (( String_t* (*) (KeyValuePair_2_t3060 *, const MethodInfo*))KeyValuePair_2_ToString_m22792_gshared)(__this, method)
