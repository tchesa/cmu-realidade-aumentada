﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaRenderer
struct VuforiaRenderer_t993;
// Vuforia.VuforiaRendererImpl
struct VuforiaRendererImpl_t995;

#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::get_Instance()
extern "C" VuforiaRenderer_t993 * VuforiaRenderer_get_Instance_m5013 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRendererImpl Vuforia.VuforiaRenderer::get_InternalInstance()
extern "C" VuforiaRendererImpl_t995 * VuforiaRenderer_get_InternalInstance_m5014 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRenderer::.ctor()
extern "C" void VuforiaRenderer__ctor_m5015 (VuforiaRenderer_t993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRenderer::.cctor()
extern "C" void VuforiaRenderer__cctor_m5016 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
