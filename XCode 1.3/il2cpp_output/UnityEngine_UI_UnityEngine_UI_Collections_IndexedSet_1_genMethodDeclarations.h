﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
#define IndexedSet_1__ctor_m4110(__this, method) (( void (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1__ctor_m20584_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20585(__this, method) (( Object_t * (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20586_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m20587(__this, ___item, method) (( void (*) (IndexedSet_1_t679 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m20588_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m20589(__this, ___item, method) (( bool (*) (IndexedSet_1_t679 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m20590_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m20591(__this, method) (( Object_t* (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m20592_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m20593(__this, method) (( void (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_Clear_m20594_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m20595(__this, ___item, method) (( bool (*) (IndexedSet_1_t679 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m20596_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m20597(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t679 *, ICanvasElementU5BU5D_t2921*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m20598_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m20599(__this, method) (( int32_t (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_get_Count_m20600_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m20601(__this, method) (( bool (*) (IndexedSet_1_t679 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m20602_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m20603(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t679 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m20604_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m20605(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t679 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m20606_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m20607(__this, ___index, method) (( void (*) (IndexedSet_1_t679 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m20608_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m20609(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t679 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m20610_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m20611(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t679 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m20612_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m4115(__this, ___match, method) (( void (*) (IndexedSet_1_t679 *, Predicate_1_t681 *, const MethodInfo*))IndexedSet_1_RemoveAll_m20613_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m4116(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t679 *, Comparison_1_t680 *, const MethodInfo*))IndexedSet_1_Sort_m20614_gshared)(__this, ___sortLayoutFunction, method)
