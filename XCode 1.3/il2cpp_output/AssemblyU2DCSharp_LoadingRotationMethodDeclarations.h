﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingRotation
struct LoadingRotation_t383;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadingRotation::.ctor()
extern "C" void LoadingRotation__ctor_m1962 (LoadingRotation_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingRotation::Update()
extern "C" void LoadingRotation_Update_m1963 (LoadingRotation_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
