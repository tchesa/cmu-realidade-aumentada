﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSStoreResultCode
struct IOSStoreResultCode_t92;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSStoreResultCode::.ctor()
extern "C" void IOSStoreResultCode__ctor_m550 (IOSStoreResultCode_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
