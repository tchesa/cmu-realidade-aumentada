﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppEventHandlerExample
struct AppEventHandlerExample_t203;

#include "codegen/il2cpp-codegen.h"

// System.Void AppEventHandlerExample::.ctor()
extern "C" void AppEventHandlerExample__ctor_m1127 (AppEventHandlerExample_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppEventHandlerExample::Awake()
extern "C" void AppEventHandlerExample_Awake_m1128 (AppEventHandlerExample_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppEventHandlerExample::OnApplicationDidBecomeActive()
extern "C" void AppEventHandlerExample_OnApplicationDidBecomeActive_m1129 (AppEventHandlerExample_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppEventHandlerExample::OnApplicationDidReceiveMemoryWarning()
extern "C" void AppEventHandlerExample_OnApplicationDidReceiveMemoryWarning_m1130 (AppEventHandlerExample_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
