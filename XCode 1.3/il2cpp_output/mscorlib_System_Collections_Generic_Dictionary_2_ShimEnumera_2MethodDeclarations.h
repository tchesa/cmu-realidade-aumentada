﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>
struct ShimEnumerator_t2797;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t2785;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m18692_gshared (ShimEnumerator_t2797 * __this, Dictionary_2_t2785 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m18692(__this, ___host, method) (( void (*) (ShimEnumerator_t2797 *, Dictionary_2_t2785 *, const MethodInfo*))ShimEnumerator__ctor_m18692_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m18693_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m18693(__this, method) (( bool (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_MoveNext_m18693_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Entry()
extern "C" DictionaryEntry_t58  ShimEnumerator_get_Entry_m18694_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m18694(__this, method) (( DictionaryEntry_t58  (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_get_Entry_m18694_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m18695_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m18695(__this, method) (( Object_t * (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_get_Key_m18695_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m18696_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m18696(__this, method) (( Object_t * (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_get_Value_m18696_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m18697_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m18697(__this, method) (( Object_t * (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_get_Current_m18697_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::Reset()
extern "C" void ShimEnumerator_Reset_m18698_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m18698(__this, method) (( void (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_Reset_m18698_gshared)(__this, method)
