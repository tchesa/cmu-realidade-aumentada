﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t2576;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m15445_gshared (GenericComparer_1_t2576 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m15445(__this, method) (( void (*) (GenericComparer_1_t2576 *, const MethodInfo*))GenericComparer_1__ctor_m15445_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m30206_gshared (GenericComparer_1_t2576 * __this, TimeSpan_t565  ___x, TimeSpan_t565  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m30206(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2576 *, TimeSpan_t565 , TimeSpan_t565 , const MethodInfo*))GenericComparer_1_Compare_m30206_gshared)(__this, ___x, ___y, method)
