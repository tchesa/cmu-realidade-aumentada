﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativePopUpManager
struct IOSNativePopUpManager_t168;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNativePopUpManager::.ctor()
extern "C" void IOSNativePopUpManager__ctor_m898 (IOSNativePopUpManager_t168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::_ISN_ShowRateUsPopUp(System.String,System.String,System.String,System.String,System.String)
extern "C" void IOSNativePopUpManager__ISN_ShowRateUsPopUp_m899 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::_ISN_ShowDialog(System.String,System.String,System.String,System.String)
extern "C" void IOSNativePopUpManager__ISN_ShowDialog_m900 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::_ISN_ShowMessage(System.String,System.String,System.String)
extern "C" void IOSNativePopUpManager__ISN_ShowMessage_m901 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::_ISN_DismissCurrentAlert()
extern "C" void IOSNativePopUpManager__ISN_DismissCurrentAlert_m902 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::dismissCurrentAlert()
extern "C" void IOSNativePopUpManager_dismissCurrentAlert_m903 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::showRateUsPopUp(System.String,System.String,System.String,System.String,System.String)
extern "C" void IOSNativePopUpManager_showRateUsPopUp_m904 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::showDialog(System.String,System.String)
extern "C" void IOSNativePopUpManager_showDialog_m905 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::showDialog(System.String,System.String,System.String,System.String)
extern "C" void IOSNativePopUpManager_showDialog_m906 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::showMessage(System.String,System.String)
extern "C" void IOSNativePopUpManager_showMessage_m907 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePopUpManager::showMessage(System.String,System.String,System.String)
extern "C" void IOSNativePopUpManager_showMessage_m908 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
