﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t1535;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1548;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t1537;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1536;
// System.Security.Cryptography.Oid
struct Oid_t1538;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.Security.Cryptography.DSA
struct DSA_t1654;
// System.Security.Cryptography.RSA
struct RSA_t1655;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m8441 (PublicKey_t1535 * __this, X509Certificate_t1548 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t1537 * PublicKey_get_EncodedKeyValue_m8442 (PublicKey_t1535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t1537 * PublicKey_get_EncodedParameters_m8443 (PublicKey_t1535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t1536 * PublicKey_get_Key_m8444 (PublicKey_t1535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t1538 * PublicKey_get_Oid_m8445 (PublicKey_t1535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t119* PublicKey_GetUnsignedBigInteger_m8446 (Object_t * __this /* static, unused */, ByteU5BU5D_t119* ___integer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t1654 * PublicKey_DecodeDSA_m8447 (Object_t * __this /* static, unused */, ByteU5BU5D_t119* ___rawPublicKey, ByteU5BU5D_t119* ___rawParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t1655 * PublicKey_DecodeRSA_m8448 (Object_t * __this /* static, unused */, ByteU5BU5D_t119* ___rawPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
