﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m20227(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t2902 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m19763_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>::Invoke(T0)
#define UnityAction_1_Invoke_m20228(__this, ___arg0, method) (( void (*) (UnityAction_1_t2902 *, BaseEventData_t615 *, const MethodInfo*))UnityAction_1_Invoke_m19764_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m20229(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t2902 *, BaseEventData_t615 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m19765_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m20230(__this, ___result, method) (( void (*) (UnityAction_1_t2902 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m19766_gshared)(__this, ___result, method)
