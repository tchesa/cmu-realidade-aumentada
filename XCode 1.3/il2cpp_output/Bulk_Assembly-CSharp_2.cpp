﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AnimationController/<IPlayClipWithDelay>c__IteratorE
struct U3CIPlayClipWithDelayU3Ec__IteratorE_t358;
// System.Object
struct Object_t;
// AnimationController
struct AnimationController_t357;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t359;
// UnityEngine.Animator
struct Animator_t360;
// UnityEngine.AudioListener
struct AudioListener_t576;
// BoltAnimation
struct BoltAnimation_t378;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// UnityEngine.AudioClip
struct AudioClip_t31;
// AnimationControllerScript/TimeEvent
struct TimeEvent_t362;
// System.String
struct String_t;
// AnimationControllerScript/<ApareceLampada>c__IteratorF
struct U3CApareceLampadaU3Ec__IteratorF_t363;
// AnimationControllerScript/<ApareceProveta>c__Iterator10
struct U3CApareceProvetaU3Ec__Iterator10_t365;
// UnityEngine.Renderer
struct Renderer_t46;
// AnimationControllerScript/<DesapareceProveta>c__Iterator11
struct U3CDesapareceProvetaU3Ec__Iterator11_t366;
// AnimationControllerScript/<ApareceSaleiro>c__Iterator12
struct U3CApareceSaleiroU3Ec__Iterator12_t367;
// AnimationControllerScript/<LiquidoProveta>c__Iterator13
struct U3CLiquidoProvetaU3Ec__Iterator13_t368;
// UnityEngine.ParticleSystem
struct ParticleSystem_t332;
// AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14
struct U3CAtivaParticulaPortalU3Ec__Iterator14_t369;
// AnimationControllerScript/<EmmitSmoke>c__Iterator15
struct U3CEmmitSmokeU3Ec__Iterator15_t370;
// AnimationControllerScript
struct AnimationControllerScript_t364;
// ArUiController/<ShareAfterLogin>c__Iterator16
struct U3CShareAfterLoginU3Ec__Iterator16_t373;
// ArUiController/<ITakeScreenshot>c__Iterator17
struct U3CITakeScreenshotU3Ec__Iterator17_t375;
// UnityEngine.UI.RawImage
struct RawImage_t578;
// ArUiController
struct ArUiController_t374;
// UnityEngine.CanvasGroup
struct CanvasGroup_t579;
// UnityEngine.UI.Text
struct Text_t580;
// Popup
struct Popup_t384;
// Facebook.Unity.ILoginResult
struct ILoginResult_t489;
// Facebook.Unity.IGraphResult
struct IGraphResult_t484;
// ISN_Result
struct ISN_Result_t114;
// Bezier
struct Bezier_t376;
// BezierTraveler
struct BezierTraveler_t342;
// BrilhoAnimation
struct BrilhoAnimation_t344;
// FBServices/<Login>c__AnonStorey1B
struct U3CLoginU3Ec__AnonStorey1B_t379;
// FBServices
struct FBServices_t381;
// System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>
struct Action_2_t380;
// System.Byte[]
struct ByteU5BU5D_t119;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t294;
// LerpFollow
struct LerpFollow_t382;
// LoadingRotation
struct LoadingRotation_t383;
// RotateScale
struct RotateScale_t385;
// ShowHideSprite
struct ShowHideSprite_t386;
// UnityEngine.UI.Image
struct Image_t387;
// SinoidalAnimation
struct SinoidalAnimation_t388;
// StartUIController/<StartTransiction>c__Iterator18
struct U3CStartTransictionU3Ec__Iterator18_t389;
// StartUIController
struct StartUIController_t390;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t581;
// TargetTrackingTrigger
struct TargetTrackingTrigger_t391;
// Test
struct Test_t392;
// System.Action`1<System.Int32>
struct Action_1_t490;
// TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19
struct U3CITakeScreenshotU3Ec__Iterator19_t393;
// TesteTakeScreenshot
struct TesteTakeScreenshot_t394;
// UnityEngine.Camera
struct Camera_t510;
// TransparencyAnimation
struct TransparencyAnimation_t340;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t396;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t398;
// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t400;
// Vuforia.DatabaseLoadBehaviour
struct DatabaseLoadBehaviour_t402;
// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t404;
// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t405;
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t406;
// Vuforia.Prop
struct Prop_t491;
// Vuforia.Surface
struct Surface_t492;
// Vuforia.DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t409;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t410;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t587;
// System.Object[]
struct ObjectU5BU5D_t34;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t588;
// Vuforia.GLErrorHandler
struct GLErrorHandler_t411;
// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t412;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t414;
// Vuforia.AndroidUnityPlayer
struct AndroidUnityPlayer_t416;
// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t417;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t590;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t605;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t486;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t476;
// Vuforia.IOSUnityPlayer
struct IOSUnityPlayer_t418;
// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t419;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t425;
// UnityEngine.GameObject
struct GameObject_t27;
// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t424;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t449;
// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t448;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t440;
// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t439;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t415;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t423;
// Vuforia.MarkerBehaviour
struct MarkerBehaviour_t422;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t427;
// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t426;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t401;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t457;
// Vuforia.WordBehaviour
struct WordBehaviour_t456;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t438;
// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t437;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t429;
// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t428;
// Vuforia.KeepAliveBehaviour
struct KeepAliveBehaviour_t420;
// Vuforia.PropBehaviour
struct PropBehaviour_t407;
// Vuforia.ReconstructionFromTargetBehaviour
struct ReconstructionFromTargetBehaviour_t432;
// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t434;
// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t408;
// UnityEngine.MeshRenderer
struct MeshRenderer_t596;
// UnityEngine.MeshFilter
struct MeshFilter_t597;
// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t441;
// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t442;
// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t444;
// Vuforia.VideoTextureRenderer
struct VideoTextureRenderer_t446;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t450;
// Vuforia.WebCamBehaviour
struct WebCamBehaviour_t452;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t454;
// UnityEngine.Camera[]
struct CameraU5BU5D_t600;
// Vuforia.WireframeTrackableEventHandler
struct WireframeTrackableEventHandler_t455;
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t604;
// UnionAssets.FLE.EventHandlerFunction
struct EventHandlerFunction_t458;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// UnionAssets.FLE.DataEventHandlerFunction
struct DataEventHandlerFunction_t459;
// UnionAssets.FLE.CEvent
struct CEvent_t74;
// Facebook.Unity.InitDelegate
struct InitDelegate_t241;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t242;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_AnimationController_U3CIPlayClipWithDelayU.h"
#include "AssemblyU2DCSharp_AnimationController_U3CIPlayClipWithDelayUMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "UnityEngine_UnityEngine_AudioClip.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "AssemblyU2DCSharp_AnimationController.h"
#include "AssemblyU2DCSharp_AnimationControllerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_WebCamTextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer.h"
#include "UnityEngine_UnityEngine_Animator.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_AudioListener.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Behaviour.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "AssemblyU2DCSharp_AnimationController_Audios.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "UnityEngine_UnityEngine_ParticleSystemMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem.h"
#include "AssemblyU2DCSharp_BoltAnimation.h"
#include "mscorlib_System_String.h"
#include "UnityEngine_UnityEngine_TrailRenderer.h"
#include "AssemblyU2DCSharp_AnimationController_U3CIPlayU3Ec__IteratorMethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationController_U3CIPlayU3Ec__Iterator.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_TimeEvent.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_TimeEventMethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CApareceLampad.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CApareceLampadMethodDeclarations.h"
#include "AssemblyU2DCSharp_ComponentTrackerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationControllerScript.h"
#include "AssemblyU2DCSharp_ComponentTracker.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CApareceProvet.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CApareceProvetMethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CDesaparecePro.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CDesapareceProMethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CApareceSaleir.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CApareceSaleirMethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CLiquidoProvet.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CLiquidoProvetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CAtivaParticul.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CAtivaParticulMethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CEmmitSmokeU3E.h"
#include "AssemblyU2DCSharp_AnimationControllerScript_U3CEmmitSmokeU3EMethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationControllerScriptMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen_2MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Comparison_1_gen_2.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "AssemblyU2DCSharp_ArUiController_U3CShareAfterLoginU3Ec__Ite.h"
#include "AssemblyU2DCSharp_ArUiController_U3CShareAfterLoginU3Ec__IteMethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FBMethodDeclarations.h"
#include "AssemblyU2DCSharp_ArUiControllerMethodDeclarations.h"
#include "AssemblyU2DCSharp_ArUiController.h"
#include "AssemblyU2DCSharp_ArUiController_U3CITakeScreenshotU3Ec__Ite.h"
#include "AssemblyU2DCSharp_ArUiController_U3CITakeScreenshotU3Ec__IteMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "mscorlib_System_IO_FileMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImageMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup.h"
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
#include "AssemblyU2DCSharp_ISN_Singleton_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "AssemblyU2DCSharp_IOSCameraMethodDeclarations.h"
#include "AssemblyU2DCSharp_PopupMethodDeclarations.h"
#include "AssemblyU2DCSharp_IOSCamera.h"
#include "mscorlib_System_Action_1_gen.h"
#include "AssemblyU2DCSharp_ISN_Result.h"
#include "mscorlib_System_Delegate.h"
#include "AssemblyU2DCSharp_Popup.h"
#include "AssemblyU2DCSharp_FBServicesMethodDeclarations.h"
#include "AssemblyU2DCSharp_FBServices.h"
#include "System_Core_System_Action_2_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookDelegate_1_gen_7MethodDeclarations.h"
#include "System_Core_System_Action_2_gen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookDelegate_1_gen_7.h"
#include "AssemblyU2DCSharp_ISN_ResultMethodDeclarations.h"
#include "AssemblyU2DCSharp_IOSMessageMethodDeclarations.h"
#include "AssemblyU2DCSharp_IOSMessage.h"
#include "AssemblyU2DCSharp_Bezier.h"
#include "AssemblyU2DCSharp_BezierMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "AssemblyU2DCSharp_BezierTraveler.h"
#include "AssemblyU2DCSharp_BezierTravelerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "AssemblyU2DCSharp_BoltAnimationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "AssemblyU2DCSharp_BrilhoAnimation.h"
#include "AssemblyU2DCSharp_BrilhoAnimationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "AssemblyU2DCSharp_FBServices_U3CLoginU3Ec__AnonStorey1B.h"
#include "AssemblyU2DCSharp_FBServices_U3CLoginU3Ec__AnonStorey1BMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_2.h"
#include "AssemblyU2DCSharp_Facebook_Unity_InitDelegateMethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_InitDelegate.h"
#include "AssemblyU2DCSharp_Facebook_Unity_HideUnityDelegate.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookDelegate_1_genMethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookDelegate_1_gen.h"
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm.h"
#include "AssemblyU2DCSharp_Facebook_Unity_HttpMethod.h"
#include "AssemblyU2DCSharp_LerpFollow.h"
#include "AssemblyU2DCSharp_LerpFollowMethodDeclarations.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "AssemblyU2DCSharp_LoadingRotation.h"
#include "AssemblyU2DCSharp_LoadingRotationMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTweenMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EaseType.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "AssemblyU2DCSharp_RotateScale.h"
#include "AssemblyU2DCSharp_RotateScaleMethodDeclarations.h"
#include "AssemblyU2DCSharp_ShowHideSprite.h"
#include "AssemblyU2DCSharp_ShowHideSpriteMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic.h"
#include "AssemblyU2DCSharp_SinoidalAnimation.h"
#include "AssemblyU2DCSharp_SinoidalAnimationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space.h"
#include "AssemblyU2DCSharp_StartUIController_U3CStartTransictionU3Ec_.h"
#include "AssemblyU2DCSharp_StartUIController_U3CStartTransictionU3Ec_MethodDeclarations.h"
#include "AssemblyU2DCSharp_StartUIController.h"
#include "AssemblyU2DCSharp_StartUIControllerMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
#include "AssemblyU2DCSharp_TargetTrackingTrigger.h"
#include "AssemblyU2DCSharp_TargetTrackingTriggerMethodDeclarations.h"
#include "AssemblyU2DCSharp_Test.h"
#include "AssemblyU2DCSharp_TestMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_19MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_19.h"
#include "AssemblyU2DCSharp_TesteTakeScreenshot_U3CITakeScreenshotU3Ec.h"
#include "AssemblyU2DCSharp_TesteTakeScreenshot_U3CITakeScreenshotU3EcMethodDeclarations.h"
#include "AssemblyU2DCSharp_TesteTakeScreenshotMethodDeclarations.h"
#include "AssemblyU2DCSharp_TesteTakeScreenshot.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "AssemblyU2DCSharp_TransparencyAnimation.h"
#include "AssemblyU2DCSharp_TransparencyAnimationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader.h"
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBeMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBe.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstrMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DatabaseLoadAbstracMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DatabaseLoadAbstrac.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandlerMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_20MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBehaMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"
#include "mscorlib_System_Action_1_gen_20.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandlerMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_21MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstrMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_22MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
#include "mscorlib_System_Action_1_gen_21.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"
#include "mscorlib_System_Action_1_gen_22.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandlerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandlerMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstrMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstr.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstractMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnityMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviourMethodDeclarations.h"
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_14MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5MethodDeclarations.h"
#include "System_Core_System_ActionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_14.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
#include "mscorlib_System_Attribute.h"
#include "System_Core_System_Action.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "System_Core_System_Linq_Enumerable.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactoryMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentFMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBe.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehavMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBehaMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtiliMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstractMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstracMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavioMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviourMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromTMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBehaMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBehMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBehaMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer.h"
#include "UnityEngine_UnityEngine_MeshFilter.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviourMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBuMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbstMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst.h"
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRendererMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendereMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendere.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstraMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayer.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManager.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_EventHandlerFunction.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_EventHandlerFunctionMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_DataEventHandlerFunction.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_DataEventHandlerFunctionMethodDeclarations.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_CEvent.h"
#include "AssemblyU2DCSharp_Facebook_Unity_HideUnityDelegateMethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C" Object_t * GameObject_GetComponentInChildren_TisObject_t_m2795_gshared (GameObject_t27 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisObject_t_m2795(__this, method) (( Object_t * (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponentInChildren_TisObject_t_m2795_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.SkinnedMeshRenderer>()
#define GameObject_GetComponentInChildren_TisSkinnedMeshRenderer_t359_m2636(__this, method) (( SkinnedMeshRenderer_t359 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponentInChildren_TisObject_t_m2795_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m436_gshared (GameObject_t27 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m436(__this, method) (( Object_t * (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m436_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t360_m2637(__this, method) (( Animator_t360 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m436_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" Object_t * Object_FindObjectOfType_TisObject_t_m2796_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisObject_t_m2796(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m2796_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.AudioListener>()
#define Object_FindObjectOfType_TisAudioListener_t576_m2638(__this /* static, unused */, method) (( AudioListener_t576 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m2796_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioListener>()
#define GameObject_GetComponent_TisAudioListener_t576_m2639(__this, method) (( AudioListener_t576 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m436_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m437_gshared (Component_t56 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m437(__this, method) (( Object_t * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<BoltAnimation>()
#define Component_GetComponent_TisBoltAnimation_t378_m2606(__this, method) (( BoltAnimation_t378 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 ComponentTracker::GetElement<System.Object>(System.String)
extern "C" Object_t * ComponentTracker_GetElement_TisObject_t_m2797_gshared (ComponentTracker_t67 * __this, String_t* p0, const MethodInfo* method);
#define ComponentTracker_GetElement_TisObject_t_m2797(__this, p0, method) (( Object_t * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 ComponentTracker::GetElement<UnityEngine.Renderer>(System.String)
#define ComponentTracker_GetElement_TisRenderer_t46_m2642(__this, p0, method) (( Renderer_t46 * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 ComponentTracker::GetElement<UnityEngine.ParticleSystem>(System.String)
#define ComponentTracker_GetElement_TisParticleSystem_t332_m2643(__this, p0, method) (( ParticleSystem_t332 * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t360_m2645(__this, method) (( Animator_t360 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 ComponentTracker::GetElement<UnityEngine.UI.RawImage>(System.String)
#define ComponentTracker_GetElement_TisRawImage_t578_m2656(__this, p0, method) (( RawImage_t578 * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 ComponentTracker::GetElement<UnityEngine.CanvasGroup>(System.String)
#define ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(__this, p0, method) (( CanvasGroup_t579 * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 ComponentTracker::GetElement<UnityEngine.UI.Text>(System.String)
#define ComponentTracker_GetElement_TisText_t580_m2663(__this, p0, method) (( Text_t580 * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 ComponentTracker::GetElement<Popup>(System.String)
#define ComponentTracker_GetElement_TisPopup_t384_m2664(__this, p0, method) (( Popup_t384 * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t46_m2553(__this, method) (( Renderer_t46 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m438_gshared (GameObject_t27 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m438(__this, method) (( Object_t * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<FBServices>()
#define GameObject_AddComponent_TisFBServices_t381_m2669(__this, method) (( FBServices_t381 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t387_m2673(__this, method) (( Image_t387 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 ComponentTracker::GetElement<UnityEngine.UI.Image>(System.String)
#define ComponentTracker_GetElement_TisImage_t387_m2678(__this, p0, method) (( Image_t387 * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 ComponentTracker::GetElement<UnityEngine.UI.ScrollRect>(System.String)
#define ComponentTracker_GetElement_TisScrollRect_t581_m2679(__this, p0, method) (( ScrollRect_t581 * (*) (ComponentTracker_t67 *, String_t*, const MethodInfo*))ComponentTracker_GetElement_TisObject_t_m2797_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t510_m2686(__this, method) (( Camera_t510 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t46_m2323(__this, method) (( Renderer_t46 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m436_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
#define Component_GetComponent_TisReconstructionBehaviour_t406_m2704(__this, method) (( ReconstructionBehaviour_t406 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t410_m2713(__this, method) (( TrackableBehaviour_t410 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C" ObjectU5BU5D_t34* Component_GetComponentsInChildren_TisObject_t_m2798_gshared (Component_t56 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m2798(__this, p0, method) (( ObjectU5BU5D_t34* (*) (Component_t56 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2798_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t46_m2715(__this, p0, method) (( RendererU5BU5D_t587* (*) (Component_t56 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2798_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t316_m2716(__this, p0, method) (( ColliderU5BU5D_t588* (*) (Component_t56 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2798_gshared)(__this, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" List_1_t486 * Enumerable_ToList_TisObject_t_m2630_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_ToList_TisObject_t_m2630(__this /* static, unused */, p0, method) (( List_1_t486 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisObject_t_m2630_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t_m2727(__this /* static, unused */, p0, method) (( List_1_t590 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisObject_t_m2630_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
#define GameObject_AddComponent_TisMaskOutBehaviour_t424_m2734(__this, method) (( MaskOutBehaviour_t424 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
#define GameObject_AddComponent_TisVirtualButtonBehaviour_t448_m2735(__this, method) (( VirtualButtonBehaviour_t448 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
#define GameObject_AddComponent_TisTurnOffBehaviour_t439_m2736(__this, method) (( TurnOffBehaviour_t439 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_AddComponent_TisImageTargetBehaviour_t414_m2737(__this, method) (( ImageTargetBehaviour_t414 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
#define GameObject_AddComponent_TisMarkerBehaviour_t422_m2738(__this, method) (( MarkerBehaviour_t422 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
#define GameObject_AddComponent_TisMultiTargetBehaviour_t426_m2739(__this, method) (( MultiTargetBehaviour_t426 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
#define GameObject_AddComponent_TisCylinderTargetBehaviour_t400_m2740(__this, method) (( CylinderTargetBehaviour_t400 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
#define GameObject_AddComponent_TisWordBehaviour_t456_m2741(__this, method) (( WordBehaviour_t456 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
#define GameObject_AddComponent_TisTextRecoBehaviour_t437_m2742(__this, method) (( TextRecoBehaviour_t437 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
#define GameObject_AddComponent_TisObjectTargetBehaviour_t428_m2743(__this, method) (( ObjectTargetBehaviour_t428 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t596_m2759(__this, method) (( MeshRenderer_t596 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t597_m2760(__this, method) (( MeshFilter_t597 * (*) (Component_t56 *, const MethodInfo*))Component_GetComponent_TisObject_t_m437_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t417_m2771(__this, method) (( ComponentFactoryStarterBehaviour_t417 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m438_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<Vuforia.VuforiaBehaviour>()
#define Object_FindObjectOfType_TisVuforiaBehaviour_t450_m2772(__this /* static, unused */, method) (( VuforiaBehaviour_t450 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m2796_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t34* GameObject_GetComponentsInChildren_TisObject_t_m2799_gshared (GameObject_t27 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisObject_t_m2799(__this, method) (( ObjectU5BU5D_t34* (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m2799_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t510_m2776(__this, method) (( CameraU5BU5D_t600* (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m2799_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793(__this, p0, method) (( WireframeBehaviourU5BU5D_t604* (*) (Component_t56 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2798_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimationController/<IPlayClipWithDelay>c__IteratorE::.ctor()
extern "C" void U3CIPlayClipWithDelayU3Ec__IteratorE__ctor_m1824 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AnimationController/<IPlayClipWithDelay>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CIPlayClipWithDelayU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1825 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object AnimationController/<IPlayClipWithDelay>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CIPlayClipWithDelayU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m1826 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean AnimationController/<IPlayClipWithDelay>c__IteratorE::MoveNext()
extern TypeInfo* WaitForSeconds_t37_il2cpp_TypeInfo_var;
extern "C" bool U3CIPlayClipWithDelayU3Ec__IteratorE_MoveNext_m1827 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0056;
	}

IL_0021:
	{
		float L_2 = (__this->___delay_0);
		WaitForSeconds_t37 * L_3 = (WaitForSeconds_t37 *)il2cpp_codegen_object_new (WaitForSeconds_t37_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m295(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_4 = L_3;
		__this->___U24PC_3 = 1;
		goto IL_0058;
	}

IL_003e:
	{
		AudioClip_t31 * L_4 = (__this->___clip_1);
		Vector3_t6  L_5 = (__this->___position_2);
		AudioSource_PlayClipAtPoint_m2609(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		__this->___U24PC_3 = (-1);
	}

IL_0056:
	{
		return 0;
	}

IL_0058:
	{
		return 1;
	}
	// Dead block : IL_005a: ldloc.1
}
// System.Void AnimationController/<IPlayClipWithDelay>c__IteratorE::Dispose()
extern "C" void U3CIPlayClipWithDelayU3Ec__IteratorE_Dispose_m1828 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void AnimationController/<IPlayClipWithDelay>c__IteratorE::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CIPlayClipWithDelayU3Ec__IteratorE_Reset_m1829 (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AnimationController::.ctor()
extern "C" void AnimationController__ctor_m1830 (AnimationController_t357 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationController::Start()
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisSkinnedMeshRenderer_t359_m2636_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t360_m2637_MethodInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisAudioListener_t576_m2638_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var;
extern "C" void AnimationController_Start_m1831 (AnimationController_t357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		GameObject_GetComponentInChildren_TisSkinnedMeshRenderer_t359_m2636_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484082);
		GameObject_GetComponent_TisAnimator_t360_m2637_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484083);
		Object_FindObjectOfType_TisAudioListener_t576_m2638_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484084);
		GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484085);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebCamDeviceU5BU5D_t575* L_0 = WebCamTexture_get_devices_m2634(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		Object_t * L_2 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_1);
		MonoBehaviour_print_m2635(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t27 * L_3 = (__this->___lamp_3);
		NullCheck(L_3);
		SkinnedMeshRenderer_t359 * L_4 = GameObject_GetComponentInChildren_TisSkinnedMeshRenderer_t359_m2636(L_3, /*hidden argument*/GameObject_GetComponentInChildren_TisSkinnedMeshRenderer_t359_m2636_MethodInfo_var);
		__this->___lampMesh_5 = L_4;
		GameObject_t27 * L_5 = (__this->___lamp_3);
		NullCheck(L_5);
		Animator_t360 * L_6 = GameObject_GetComponent_TisAnimator_t360_m2637(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t360_m2637_MethodInfo_var);
		__this->___lampAnimator_6 = L_6;
		GameObject_t27 * L_7 = (__this->___lamp_3);
		NullCheck(L_7);
		Transform_t25 * L_8 = GameObject_get_transform_m305(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t6  L_9 = Transform_get_position_m334(L_8, /*hidden argument*/NULL);
		__this->___lampInitialPosition_7 = L_9;
		AudioListener_t576 * L_10 = Object_FindObjectOfType_TisAudioListener_t576_m2638(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisAudioListener_t576_m2638_MethodInfo_var);
		NullCheck(L_10);
		GameObject_t27 * L_11 = Component_get_gameObject_m306(L_10, /*hidden argument*/NULL);
		__this->___listener_24 = L_11;
		GameObject_t27 * L_12 = (__this->___listener_24);
		NullCheck(L_12);
		AudioListener_t576 * L_13 = GameObject_GetComponent_TisAudioListener_t576_m2639(L_12, /*hidden argument*/GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var);
		NullCheck(L_13);
		Behaviour_set_enabled_m408(L_13, 0, /*hidden argument*/NULL);
		SkinnedMeshRenderer_t359 * L_14 = (__this->___lampMesh_5);
		NullCheck(L_14);
		Renderer_set_enabled_m2324(L_14, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationController::Update()
extern "C" void AnimationController_Update_m1832 (AnimationController_t357 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___play_1);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		__this->___play_1 = 0;
		AnimationController_Play_m1833(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		bool L_1 = (__this->___stop_2);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		__this->___stop_2 = 0;
		AnimationController_Stop_m1835(__this, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void AnimationController::Play()
extern "C" void AnimationController_Play_m1833 (AnimationController_t357 * __this, const MethodInfo* method)
{
	{
		AnimationController_Play_m1834(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationController::Play(System.Boolean)
extern const MethodInfo* GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var;
extern "C" void AnimationController_Play_m1834 (AnimationController_t357 * __this, bool ___playSound, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484085);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___playSound;
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Audios_t355 * L_1 = &(__this->___audios_21);
		AudioClip_t31 * L_2 = (L_1->___Abertura_0);
		GameObject_t27 * L_3 = (__this->___listener_24);
		NullCheck(L_3);
		Transform_t25 * L_4 = GameObject_get_transform_m305(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t6  L_5 = Transform_get_position_m334(L_4, /*hidden argument*/NULL);
		AudioSource_PlayClipAtPoint_m2609(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
	}

IL_0026:
	{
		Object_t * L_6 = AnimationController_IPlay_m1836(__this, /*hidden argument*/NULL);
		__this->___coroutine_25 = L_6;
		Object_t * L_7 = (__this->___coroutine_25);
		MonoBehaviour_StartCoroutine_m2199(__this, L_7, /*hidden argument*/NULL);
		GameObject_t27 * L_8 = (__this->___listener_24);
		NullCheck(L_8);
		AudioListener_t576 * L_9 = GameObject_GetComponent_TisAudioListener_t576_m2639(L_8, /*hidden argument*/GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var);
		NullCheck(L_9);
		Behaviour_set_enabled_m408(L_9, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationController::Stop()
extern const MethodInfo* GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoltAnimation_t378_m2606_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral758;
extern "C" void AnimationController_Stop_m1835 (AnimationController_t357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484085);
		Component_GetComponent_TisBoltAnimation_t378_m2606_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484081);
		_stringLiteral758 = il2cpp_codegen_string_literal_from_index(758);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___coroutine_25);
		MonoBehaviour_StopCoroutine_m2640(__this, L_0, /*hidden argument*/NULL);
		GameObject_t27 * L_1 = (__this->___listener_24);
		NullCheck(L_1);
		AudioListener_t576 * L_2 = GameObject_GetComponent_TisAudioListener_t576_m2639(L_1, /*hidden argument*/GameObject_GetComponent_TisAudioListener_t576_m2639_MethodInfo_var);
		NullCheck(L_2);
		Behaviour_set_enabled_m408(L_2, 0, /*hidden argument*/NULL);
		GameObject_t27 * L_3 = (__this->___lamp_3);
		NullCheck(L_3);
		Transform_t25 * L_4 = GameObject_get_transform_m305(L_3, /*hidden argument*/NULL);
		Vector3_t6  L_5 = (__this->___lampInitialPosition_7);
		NullCheck(L_4);
		Transform_set_position_m340(L_4, L_5, /*hidden argument*/NULL);
		ParticleSystem_t332 * L_6 = (__this->___electricGround_11);
		NullCheck(L_6);
		ParticleSystem_Stop_m2618(L_6, /*hidden argument*/NULL);
		ParticleSystem_t332 * L_7 = (__this->___electricBall_12);
		NullCheck(L_7);
		ParticleSystem_Stop_m2618(L_7, /*hidden argument*/NULL);
		ParticleSystem_t332 * L_8 = (__this->___electricBolt_13);
		NullCheck(L_8);
		Transform_t25 * L_9 = Component_get_transform_m416(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t25 * L_10 = Transform_get_parent_m2605(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		BoltAnimation_t378 * L_11 = Component_GetComponent_TisBoltAnimation_t378_m2606(L_10, /*hidden argument*/Component_GetComponent_TisBoltAnimation_t378_m2606_MethodInfo_var);
		NullCheck(L_11);
		L_11->___play_3 = 0;
		ParticleSystem_t332 * L_12 = (__this->___magicAura_14);
		NullCheck(L_12);
		ParticleSystem_Stop_m2618(L_12, /*hidden argument*/NULL);
		ParticleSystem_t332 * L_13 = (__this->___bolts_15);
		NullCheck(L_13);
		ParticleSystem_Stop_m2618(L_13, /*hidden argument*/NULL);
		Animator_t360 * L_14 = (__this->___lampAnimator_6);
		NullCheck(L_14);
		Animator_SetTrigger_m2610(L_14, _stringLiteral758, /*hidden argument*/NULL);
		ParticleSystem_t332 * L_15 = (__this->___lampShine_16);
		NullCheck(L_15);
		ParticleSystem_Stop_m2618(L_15, /*hidden argument*/NULL);
		TrailRenderer_t361 * L_16 = (__this->___trail_20);
		NullCheck(L_16);
		Renderer_set_enabled_m2324(L_16, 0, /*hidden argument*/NULL);
		__this->___clicou_23 = 0;
		SkinnedMeshRenderer_t359 * L_17 = (__this->___lampMesh_5);
		NullCheck(L_17);
		Renderer_set_enabled_m2324(L_17, 0, /*hidden argument*/NULL);
		GameObject_t27 * L_18 = (__this->___lamp_3);
		NullCheck(L_18);
		Transform_t25 * L_19 = GameObject_get_transform_m305(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_parent_m2395(L_19, (Transform_t25 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AnimationController::IPlay()
extern TypeInfo* U3CIPlayU3Ec__IteratorD_t356_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationController_IPlay_m1836 (AnimationController_t357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CIPlayU3Ec__IteratorD_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(358);
		s_Il2CppMethodIntialized = true;
	}
	U3CIPlayU3Ec__IteratorD_t356 * V_0 = {0};
	{
		U3CIPlayU3Ec__IteratorD_t356 * L_0 = (U3CIPlayU3Ec__IteratorD_t356 *)il2cpp_codegen_object_new (U3CIPlayU3Ec__IteratorD_t356_il2cpp_TypeInfo_var);
		U3CIPlayU3Ec__IteratorD__ctor_m1818(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CIPlayU3Ec__IteratorD_t356 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_13 = __this;
		U3CIPlayU3Ec__IteratorD_t356 * L_2 = V_0;
		return L_2;
	}
}
// System.Void AnimationController::PlayClipWithDelay(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern "C" void AnimationController_PlayClipWithDelay_m1837 (AnimationController_t357 * __this, AudioClip_t31 * ___clip, Vector3_t6  ___position, float ___delay, const MethodInfo* method)
{
	{
		float L_0 = ___delay;
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		AudioClip_t31 * L_1 = ___clip;
		Vector3_t6  L_2 = ___position;
		AudioSource_PlayClipAtPoint_m2609(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0027;
	}

IL_0017:
	{
		AudioClip_t31 * L_3 = ___clip;
		Vector3_t6  L_4 = ___position;
		float L_5 = ___delay;
		Object_t * L_6 = AnimationController_IPlayClipWithDelay_m1838(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2199(__this, L_6, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Collections.IEnumerator AnimationController::IPlayClipWithDelay(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern TypeInfo* U3CIPlayClipWithDelayU3Ec__IteratorE_t358_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationController_IPlayClipWithDelay_m1838 (AnimationController_t357 * __this, AudioClip_t31 * ___clip, Vector3_t6  ___position, float ___delay, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(359);
		s_Il2CppMethodIntialized = true;
	}
	U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * V_0 = {0};
	{
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * L_0 = (U3CIPlayClipWithDelayU3Ec__IteratorE_t358 *)il2cpp_codegen_object_new (U3CIPlayClipWithDelayU3Ec__IteratorE_t358_il2cpp_TypeInfo_var);
		U3CIPlayClipWithDelayU3Ec__IteratorE__ctor_m1824(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * L_1 = V_0;
		float L_2 = ___delay;
		NullCheck(L_1);
		L_1->___delay_0 = L_2;
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * L_3 = V_0;
		AudioClip_t31 * L_4 = ___clip;
		NullCheck(L_3);
		L_3->___clip_1 = L_4;
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * L_5 = V_0;
		Vector3_t6  L_6 = ___position;
		NullCheck(L_5);
		L_5->___position_2 = L_6;
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * L_7 = V_0;
		float L_8 = ___delay;
		NullCheck(L_7);
		L_7->___U3CU24U3Edelay_5 = L_8;
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * L_9 = V_0;
		AudioClip_t31 * L_10 = ___clip;
		NullCheck(L_9);
		L_9->___U3CU24U3Eclip_6 = L_10;
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * L_11 = V_0;
		Vector3_t6  L_12 = ___position;
		NullCheck(L_11);
		L_11->___U3CU24U3Eposition_7 = L_12;
		U3CIPlayClipWithDelayU3Ec__IteratorE_t358 * L_13 = V_0;
		return L_13;
	}
}
// System.Void AnimationControllerScript/TimeEvent::.ctor(System.Single,System.String)
extern "C" void TimeEvent__ctor_m1839 (TimeEvent_t362 * __this, float ___timer, String_t* ___function, const MethodInfo* method)
{
	String_t* V_0 = {0};
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		float L_0 = ___timer;
		__this->___timer_1 = L_0;
		String_t* L_1 = ___function;
		String_t* L_2 = L_1;
		V_0 = L_2;
		__this->___function_2 = L_2;
		String_t* L_3 = V_0;
		__this->___name_0 = L_3;
		return;
	}
}
// System.Void AnimationControllerScript/<ApareceLampada>c__IteratorF::.ctor()
extern "C" void U3CApareceLampadaU3Ec__IteratorF__ctor_m1840 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AnimationControllerScript/<ApareceLampada>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CApareceLampadaU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1841 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object AnimationControllerScript/<ApareceLampada>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CApareceLampadaU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m1842 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean AnimationControllerScript/<ApareceLampada>c__IteratorF::MoveNext()
extern TypeInfo* GameObject_t27_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForEndOfFrame_t500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral759;
extern "C" bool U3CApareceLampadaU3Ec__IteratorF_MoveNext_m1843 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		WaitForEndOfFrame_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		_stringLiteral759 = il2cpp_codegen_string_literal_from_index(759);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_009d;
		}
	}
	{
		goto IL_00a4;
	}

IL_0021:
	{
		AnimationControllerScript_t364 * L_2 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		ComponentTracker_t67 * L_3 = (L_2->___tracker_12);
		NullCheck(L_3);
		GameObject_t27 * L_4 = ComponentTracker_GetObject_m447(L_3, _stringLiteral759, /*hidden argument*/NULL);
		__this->___U3ClampU3E__0_0 = L_4;
		AnimationControllerScript_t364 * L_5 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_5);
		GameObject_t27 * L_6 = (L_5->___electricityPoint_5);
		GameObject_t27 * L_7 = (__this->___U3ClampU3E__0_0);
		NullCheck(L_7);
		Transform_t25 * L_8 = GameObject_get_transform_m305(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t6  L_9 = Transform_get_position_m334(L_8, /*hidden argument*/NULL);
		GameObject_t27 * L_10 = (__this->___U3ClampU3E__0_0);
		NullCheck(L_10);
		Transform_t25 * L_11 = GameObject_get_transform_m305(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Quaternion_t51  L_12 = Transform_get_rotation_m2607(L_11, /*hidden argument*/NULL);
		Object_t53 * L_13 = Object_Instantiate_m2641(NULL /*static, unused*/, L_6, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(((GameObject_t27 *)IsInstSealed(L_13, GameObject_t27_il2cpp_TypeInfo_var)));
		Transform_t25 * L_14 = GameObject_get_transform_m305(((GameObject_t27 *)IsInstSealed(L_13, GameObject_t27_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GameObject_t27 * L_15 = (__this->___U3ClampU3E__0_0);
		NullCheck(L_15);
		Transform_t25 * L_16 = GameObject_get_transform_m305(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_parent_m2395(L_14, L_16, /*hidden argument*/NULL);
		WaitForEndOfFrame_t500 * L_17 = (WaitForEndOfFrame_t500 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t500_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m2246(L_17, /*hidden argument*/NULL);
		__this->___U24current_2 = L_17;
		__this->___U24PC_1 = 1;
		goto IL_00a6;
	}

IL_009d:
	{
		__this->___U24PC_1 = (-1);
	}

IL_00a4:
	{
		return 0;
	}

IL_00a6:
	{
		return 1;
	}
	// Dead block : IL_00a8: ldloc.1
}
// System.Void AnimationControllerScript/<ApareceLampada>c__IteratorF::Dispose()
extern "C" void U3CApareceLampadaU3Ec__IteratorF_Dispose_m1844 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void AnimationControllerScript/<ApareceLampada>c__IteratorF::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CApareceLampadaU3Ec__IteratorF_Reset_m1845 (U3CApareceLampadaU3Ec__IteratorF_t363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AnimationControllerScript/<ApareceProveta>c__Iterator10::.ctor()
extern "C" void U3CApareceProvetaU3Ec__Iterator10__ctor_m1846 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AnimationControllerScript/<ApareceProveta>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CApareceProvetaU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1847 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object AnimationControllerScript/<ApareceProveta>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CApareceProvetaU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m1848 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean AnimationControllerScript/<ApareceProveta>c__Iterator10::MoveNext()
extern const MethodInfo* ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral760;
extern "C" bool U3CApareceProvetaU3Ec__Iterator10_MoveNext_m1849 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484086);
		_stringLiteral760 = il2cpp_codegen_string_literal_from_index(760);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0050;
	}

IL_0021:
	{
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_0052;
	}

IL_0034:
	{
		ComponentTracker_t67 * L_2 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Renderer_t46 * L_3 = ComponentTracker_GetElement_TisRenderer_t46_m2642(L_2, _stringLiteral760, /*hidden argument*/ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var);
		NullCheck(L_3);
		Renderer_set_enabled_m2324(L_3, 1, /*hidden argument*/NULL);
		__this->___U24PC_0 = (-1);
	}

IL_0050:
	{
		return 0;
	}

IL_0052:
	{
		return 1;
	}
	// Dead block : IL_0054: ldloc.1
}
// System.Void AnimationControllerScript/<ApareceProveta>c__Iterator10::Dispose()
extern "C" void U3CApareceProvetaU3Ec__Iterator10_Dispose_m1850 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void AnimationControllerScript/<ApareceProveta>c__Iterator10::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CApareceProvetaU3Ec__Iterator10_Reset_m1851 (U3CApareceProvetaU3Ec__Iterator10_t365 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AnimationControllerScript/<DesapareceProveta>c__Iterator11::.ctor()
extern "C" void U3CDesapareceProvetaU3Ec__Iterator11__ctor_m1852 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AnimationControllerScript/<DesapareceProveta>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDesapareceProvetaU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1853 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object AnimationControllerScript/<DesapareceProveta>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDesapareceProvetaU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m1854 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean AnimationControllerScript/<DesapareceProveta>c__Iterator11::MoveNext()
extern const MethodInfo* ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral760;
extern "C" bool U3CDesapareceProvetaU3Ec__Iterator11_MoveNext_m1855 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484086);
		_stringLiteral760 = il2cpp_codegen_string_literal_from_index(760);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0050;
	}

IL_0021:
	{
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_0052;
	}

IL_0034:
	{
		ComponentTracker_t67 * L_2 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Renderer_t46 * L_3 = ComponentTracker_GetElement_TisRenderer_t46_m2642(L_2, _stringLiteral760, /*hidden argument*/ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var);
		NullCheck(L_3);
		Renderer_set_enabled_m2324(L_3, 0, /*hidden argument*/NULL);
		__this->___U24PC_0 = (-1);
	}

IL_0050:
	{
		return 0;
	}

IL_0052:
	{
		return 1;
	}
	// Dead block : IL_0054: ldloc.1
}
// System.Void AnimationControllerScript/<DesapareceProveta>c__Iterator11::Dispose()
extern "C" void U3CDesapareceProvetaU3Ec__Iterator11_Dispose_m1856 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void AnimationControllerScript/<DesapareceProveta>c__Iterator11::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CDesapareceProvetaU3Ec__Iterator11_Reset_m1857 (U3CDesapareceProvetaU3Ec__Iterator11_t366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AnimationControllerScript/<ApareceSaleiro>c__Iterator12::.ctor()
extern "C" void U3CApareceSaleiroU3Ec__Iterator12__ctor_m1858 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AnimationControllerScript/<ApareceSaleiro>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CApareceSaleiroU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1859 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object AnimationControllerScript/<ApareceSaleiro>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CApareceSaleiroU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m1860 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean AnimationControllerScript/<ApareceSaleiro>c__Iterator12::MoveNext()
extern const MethodInfo* ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral761;
extern "C" bool U3CApareceSaleiroU3Ec__Iterator12_MoveNext_m1861 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484086);
		_stringLiteral761 = il2cpp_codegen_string_literal_from_index(761);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0050;
	}

IL_0021:
	{
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_0052;
	}

IL_0034:
	{
		ComponentTracker_t67 * L_2 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Renderer_t46 * L_3 = ComponentTracker_GetElement_TisRenderer_t46_m2642(L_2, _stringLiteral761, /*hidden argument*/ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var);
		NullCheck(L_3);
		Renderer_set_enabled_m2324(L_3, 1, /*hidden argument*/NULL);
		__this->___U24PC_0 = (-1);
	}

IL_0050:
	{
		return 0;
	}

IL_0052:
	{
		return 1;
	}
	// Dead block : IL_0054: ldloc.1
}
// System.Void AnimationControllerScript/<ApareceSaleiro>c__Iterator12::Dispose()
extern "C" void U3CApareceSaleiroU3Ec__Iterator12_Dispose_m1862 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void AnimationControllerScript/<ApareceSaleiro>c__Iterator12::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CApareceSaleiroU3Ec__Iterator12_Reset_m1863 (U3CApareceSaleiroU3Ec__Iterator12_t367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AnimationControllerScript/<LiquidoProveta>c__Iterator13::.ctor()
extern "C" void U3CLiquidoProvetaU3Ec__Iterator13__ctor_m1864 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AnimationControllerScript/<LiquidoProveta>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLiquidoProvetaU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1865 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object AnimationControllerScript/<LiquidoProveta>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLiquidoProvetaU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m1866 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean AnimationControllerScript/<LiquidoProveta>c__Iterator13::MoveNext()
extern TypeInfo* GameObject_t27_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t37_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisParticleSystem_t332_m2643_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral762;
extern Il2CppCodeGenString* _stringLiteral69;
extern "C" bool U3CLiquidoProvetaU3Ec__Iterator13_MoveNext_m1867 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		WaitForSeconds_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ComponentTracker_GetElement_TisParticleSystem_t332_m2643_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484087);
		_stringLiteral762 = il2cpp_codegen_string_literal_from_index(762);
		_stringLiteral69 = il2cpp_codegen_string_literal_from_index(69);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0105;
		}
	}
	{
		goto IL_0117;
	}

IL_0021:
	{
		AnimationControllerScript_t364 * L_2 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_2);
		ComponentTracker_t67 * L_3 = (L_2->___tracker_12);
		NullCheck(L_3);
		ParticleSystem_t332 * L_4 = ComponentTracker_GetElement_TisParticleSystem_t332_m2643(L_3, _stringLiteral762, /*hidden argument*/ComponentTracker_GetElement_TisParticleSystem_t332_m2643_MethodInfo_var);
		__this->___U3CliquidU3E__0_0 = L_4;
		AnimationControllerScript_t364 * L_5 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_5);
		GameObject_t27 * L_6 = (L_5->___fourtain_6);
		Object_t53 * L_7 = Object_Instantiate_m2322(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->___U3CfU3E__1_1 = ((GameObject_t27 *)IsInstSealed(L_7, GameObject_t27_il2cpp_TypeInfo_var));
		GameObject_t27 * L_8 = (__this->___U3CfU3E__1_1);
		NullCheck(L_8);
		Transform_t25 * L_9 = GameObject_get_transform_m305(L_8, /*hidden argument*/NULL);
		AnimationControllerScript_t364 * L_10 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_10);
		ComponentTracker_t67 * L_11 = (L_10->___tracker_12);
		NullCheck(L_11);
		GameObject_t27 * L_12 = ComponentTracker_GetObject_m447(L_11, _stringLiteral69, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t25 * L_13 = GameObject_get_transform_m305(L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_parent_m2395(L_9, L_13, /*hidden argument*/NULL);
		GameObject_t27 * L_14 = (__this->___U3CfU3E__1_1);
		NullCheck(L_14);
		Transform_t25 * L_15 = GameObject_get_transform_m305(L_14, /*hidden argument*/NULL);
		Vector3_t6  L_16 = {0};
		Vector3__ctor_m335(&L_16, (0.0f), (0.8020619f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localPosition_m339(L_15, L_16, /*hidden argument*/NULL);
		GameObject_t27 * L_17 = (__this->___U3CfU3E__1_1);
		NullCheck(L_17);
		Transform_t25 * L_18 = GameObject_get_transform_m305(L_17, /*hidden argument*/NULL);
		Vector3_t6  L_19 = {0};
		Vector3__ctor_m335(&L_19, (90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_localEulerAngles_m343(L_18, L_19, /*hidden argument*/NULL);
		GameObject_t27 * L_20 = (__this->___U3CfU3E__1_1);
		NullCheck(L_20);
		Transform_t25 * L_21 = GameObject_get_transform_m305(L_20, /*hidden argument*/NULL);
		Vector3_t6  L_22 = Vector3_get_one_m2644(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localScale_m341(L_21, L_22, /*hidden argument*/NULL);
		ParticleSystem_t332 * L_23 = (__this->___U3CliquidU3E__0_0);
		NullCheck(L_23);
		ParticleSystem_Play_m2604(L_23, /*hidden argument*/NULL);
		WaitForSeconds_t37 * L_24 = (WaitForSeconds_t37 *)il2cpp_codegen_object_new (WaitForSeconds_t37_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m295(L_24, (1.2f), /*hidden argument*/NULL);
		__this->___U24current_3 = L_24;
		__this->___U24PC_2 = 1;
		goto IL_0119;
	}

IL_0105:
	{
		ParticleSystem_t332 * L_25 = (__this->___U3CliquidU3E__0_0);
		NullCheck(L_25);
		ParticleSystem_Stop_m2618(L_25, /*hidden argument*/NULL);
		__this->___U24PC_2 = (-1);
	}

IL_0117:
	{
		return 0;
	}

IL_0119:
	{
		return 1;
	}
	// Dead block : IL_011b: ldloc.1
}
// System.Void AnimationControllerScript/<LiquidoProveta>c__Iterator13::Dispose()
extern "C" void U3CLiquidoProvetaU3Ec__Iterator13_Dispose_m1868 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void AnimationControllerScript/<LiquidoProveta>c__Iterator13::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CLiquidoProvetaU3Ec__Iterator13_Reset_m1869 (U3CLiquidoProvetaU3Ec__Iterator13_t368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::.ctor()
extern "C" void U3CAtivaParticulaPortalU3Ec__Iterator14__ctor_m1870 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAtivaParticulaPortalU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1871 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAtivaParticulaPortalU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m1872 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::MoveNext()
extern TypeInfo* WaitForSeconds_t37_il2cpp_TypeInfo_var;
extern "C" bool U3CAtivaParticulaPortalU3Ec__Iterator14_MoveNext_m1873 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_005d;
		}
	}
	{
		goto IL_0084;
	}

IL_0021:
	{
		AnimationControllerScript_t364 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		ParticleSystem_t332 * L_3 = (L_2->___particlePortal_7);
		NullCheck(L_3);
		ParticleSystem_Play_m2604(L_3, /*hidden argument*/NULL);
		AnimationControllerScript_t364 * L_4 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_4);
		ParticleSystem_t332 * L_5 = (L_4->___vortexPortal_8);
		NullCheck(L_5);
		ParticleSystem_Play_m2604(L_5, /*hidden argument*/NULL);
		WaitForSeconds_t37 * L_6 = (WaitForSeconds_t37 *)il2cpp_codegen_object_new (WaitForSeconds_t37_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m295(L_6, (8.0f), /*hidden argument*/NULL);
		__this->___U24current_1 = L_6;
		__this->___U24PC_0 = 1;
		goto IL_0086;
	}

IL_005d:
	{
		AnimationControllerScript_t364 * L_7 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_7);
		ParticleSystem_t332 * L_8 = (L_7->___particlePortal_7);
		NullCheck(L_8);
		ParticleSystem_Stop_m2618(L_8, /*hidden argument*/NULL);
		AnimationControllerScript_t364 * L_9 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_9);
		ParticleSystem_t332 * L_10 = (L_9->___vortexPortal_8);
		NullCheck(L_10);
		ParticleSystem_Stop_m2618(L_10, /*hidden argument*/NULL);
		__this->___U24PC_0 = (-1);
	}

IL_0084:
	{
		return 0;
	}

IL_0086:
	{
		return 1;
	}
	// Dead block : IL_0088: ldloc.1
}
// System.Void AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::Dispose()
extern "C" void U3CAtivaParticulaPortalU3Ec__Iterator14_Dispose_m1874 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void AnimationControllerScript/<AtivaParticulaPortal>c__Iterator14::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CAtivaParticulaPortalU3Ec__Iterator14_Reset_m1875 (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AnimationControllerScript/<EmmitSmoke>c__Iterator15::.ctor()
extern "C" void U3CEmmitSmokeU3Ec__Iterator15__ctor_m1876 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AnimationControllerScript/<EmmitSmoke>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CEmmitSmokeU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1877 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object AnimationControllerScript/<EmmitSmoke>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CEmmitSmokeU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m1878 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean AnimationControllerScript/<EmmitSmoke>c__Iterator15::MoveNext()
extern "C" bool U3CEmmitSmokeU3Ec__Iterator15_MoveNext_m1879 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_004b;
	}

IL_0021:
	{
		AnimationControllerScript_t364 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		ParticleSystem_t332 * L_3 = (L_2->___smoke_9);
		NullCheck(L_3);
		ParticleSystem_Play_m2604(L_3, /*hidden argument*/NULL);
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_004d;
	}

IL_0044:
	{
		__this->___U24PC_0 = (-1);
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		return 1;
	}
	// Dead block : IL_004f: ldloc.1
}
// System.Void AnimationControllerScript/<EmmitSmoke>c__Iterator15::Dispose()
extern "C" void U3CEmmitSmokeU3Ec__Iterator15_Dispose_m1880 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void AnimationControllerScript/<EmmitSmoke>c__Iterator15::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CEmmitSmokeU3Ec__Iterator15_Reset_m1881 (U3CEmmitSmokeU3Ec__Iterator15_t370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AnimationControllerScript::.ctor()
extern "C" void AnimationControllerScript__ctor_m1882 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationControllerScript::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t360_m2645_MethodInfo_var;
extern "C" void AnimationControllerScript_Start_m1883 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAnimator_t360_m2645_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484088);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tracker_12 = L_0;
		Animator_t360 * L_1 = Component_GetComponent_TisAnimator_t360_m2645(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t360_m2645_MethodInfo_var);
		__this->___anim_3 = L_1;
		return;
	}
}
// System.Void AnimationControllerScript::Update()
extern "C" void AnimationControllerScript_Update_m1884 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___on_1);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		__this->___on_1 = 0;
		AnimationControllerScript_On_m1885(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		bool L_1 = (__this->___off_2);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		__this->___off_2 = 0;
		AnimationControllerScript_Off_m1886(__this, /*hidden argument*/NULL);
	}

IL_0030:
	{
		List_1_t371 * L_2 = (__this->___eventQueue_11);
		if (!L_2)
		{
			goto IL_00ca;
		}
	}
	{
		List_1_t371 * L_3 = (__this->___eventQueue_11);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::get_Count() */, L_3);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_00ca;
		}
	}
	{
		List_1_t371 * L_5 = (__this->___eventQueue_11);
		NullCheck(L_5);
		TimeEvent_t362 * L_6 = (TimeEvent_t362 *)VirtFuncInvoker1< TimeEvent_t362 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::get_Item(System.Int32) */, L_5, 0);
		NullCheck(L_6);
		float L_7 = (L_6->___timer_1);
		float L_8 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = (__this->___startTime_4);
		if ((!(((float)L_7) < ((float)((float)((float)L_8-(float)L_9))))))
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_0097;
	}

IL_0073:
	{
		List_1_t371 * L_10 = (__this->___eventQueue_11);
		NullCheck(L_10);
		TimeEvent_t362 * L_11 = (TimeEvent_t362 *)VirtFuncInvoker1< TimeEvent_t362 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::get_Item(System.Int32) */, L_10, 0);
		NullCheck(L_11);
		String_t* L_12 = (L_11->___function_2);
		MonoBehaviour_StartCoroutine_m297(__this, L_12, /*hidden argument*/NULL);
		List_1_t371 * L_13 = (__this->___eventQueue_11);
		NullCheck(L_13);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::RemoveAt(System.Int32) */, L_13, 0);
	}

IL_0097:
	{
		List_1_t371 * L_14 = (__this->___eventQueue_11);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::get_Count() */, L_14);
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_00ca;
		}
	}
	{
		List_1_t371 * L_16 = (__this->___eventQueue_11);
		NullCheck(L_16);
		TimeEvent_t362 * L_17 = (TimeEvent_t362 *)VirtFuncInvoker1< TimeEvent_t362 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<AnimationControllerScript/TimeEvent>::get_Item(System.Int32) */, L_16, 0);
		NullCheck(L_17);
		float L_18 = (L_17->___timer_1);
		float L_19 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_20 = (__this->___startTime_4);
		if ((((float)L_18) < ((float)((float)((float)L_19-(float)L_20)))))
		{
			goto IL_0073;
		}
	}

IL_00ca:
	{
		return;
	}
}
// System.Void AnimationControllerScript::On()
extern TypeInfo* List_1_t371_il2cpp_TypeInfo_var;
extern TypeInfo* AnimationControllerScript_t364_il2cpp_TypeInfo_var;
extern TypeInfo* Comparison_1_t372_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2647_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2648_MethodInfo_var;
extern const MethodInfo* AnimationControllerScript_U3COnU3Em__3A_m1895_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m2649_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2650_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral763;
extern Il2CppCodeGenString* _stringLiteral764;
extern Il2CppCodeGenString* _stringLiteral760;
extern "C" void AnimationControllerScript_On_m1885 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(361);
		AnimationControllerScript_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(362);
		Comparison_1_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(363);
		ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484086);
		List_1_ToArray_m2647_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484089);
		List_1__ctor_m2648_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484090);
		AnimationControllerScript_U3COnU3Em__3A_m1895_MethodInfo_var = il2cpp_codegen_method_info_from_index(443);
		Comparison_1__ctor_m2649_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484092);
		List_1_Sort_m2650_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484093);
		_stringLiteral763 = il2cpp_codegen_string_literal_from_index(763);
		_stringLiteral764 = il2cpp_codegen_string_literal_from_index(764);
		_stringLiteral760 = il2cpp_codegen_string_literal_from_index(760);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t371 * G_B2_0 = {0};
	List_1_t371 * G_B1_0 = {0};
	{
		MonoBehaviour_print_m2635(NULL /*static, unused*/, _stringLiteral763, /*hidden argument*/NULL);
		Animator_t360 * L_0 = (__this->___anim_3);
		NullCheck(L_0);
		Animator_SetBool_m2646(L_0, _stringLiteral764, 1, /*hidden argument*/NULL);
		ComponentTracker_t67 * L_1 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t46 * L_2 = ComponentTracker_GetElement_TisRenderer_t46_m2642(L_1, _stringLiteral760, /*hidden argument*/ComponentTracker_GetElement_TisRenderer_t46_m2642_MethodInfo_var);
		NullCheck(L_2);
		Renderer_set_enabled_m2324(L_2, 0, /*hidden argument*/NULL);
		List_1_t371 * L_3 = (__this->___events_10);
		NullCheck(L_3);
		TimeEventU5BU5D_t577* L_4 = List_1_ToArray_m2647(L_3, /*hidden argument*/List_1_ToArray_m2647_MethodInfo_var);
		List_1_t371 * L_5 = (List_1_t371 *)il2cpp_codegen_object_new (List_1_t371_il2cpp_TypeInfo_var);
		List_1__ctor_m2648(L_5, (Object_t*)(Object_t*)L_4, /*hidden argument*/List_1__ctor_m2648_MethodInfo_var);
		__this->___eventQueue_11 = L_5;
		List_1_t371 * L_6 = (__this->___eventQueue_11);
		Comparison_1_t372 * L_7 = ((AnimationControllerScript_t364_StaticFields*)AnimationControllerScript_t364_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheC_13;
		G_B1_0 = L_6;
		if (L_7)
		{
			G_B2_0 = L_6;
			goto IL_0064;
		}
	}
	{
		IntPtr_t L_8 = { (void*)AnimationControllerScript_U3COnU3Em__3A_m1895_MethodInfo_var };
		Comparison_1_t372 * L_9 = (Comparison_1_t372 *)il2cpp_codegen_object_new (Comparison_1_t372_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m2649(L_9, NULL, L_8, /*hidden argument*/Comparison_1__ctor_m2649_MethodInfo_var);
		((AnimationControllerScript_t364_StaticFields*)AnimationControllerScript_t364_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheC_13 = L_9;
		G_B2_0 = G_B1_0;
	}

IL_0064:
	{
		Comparison_1_t372 * L_10 = ((AnimationControllerScript_t364_StaticFields*)AnimationControllerScript_t364_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheC_13;
		NullCheck(G_B2_0);
		List_1_Sort_m2650(G_B2_0, L_10, /*hidden argument*/List_1_Sort_m2650_MethodInfo_var);
		float L_11 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___startTime_4 = L_11;
		return;
	}
}
// System.Void AnimationControllerScript::Off()
extern TypeInfo* List_1_t371_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2651_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral765;
extern Il2CppCodeGenString* _stringLiteral764;
extern "C" void AnimationControllerScript_Off_m1886 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(361);
		List_1__ctor_m2651_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484094);
		_stringLiteral765 = il2cpp_codegen_string_literal_from_index(765);
		_stringLiteral764 = il2cpp_codegen_string_literal_from_index(764);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m2635(NULL /*static, unused*/, _stringLiteral765, /*hidden argument*/NULL);
		List_1_t371 * L_0 = (List_1_t371 *)il2cpp_codegen_object_new (List_1_t371_il2cpp_TypeInfo_var);
		List_1__ctor_m2651(L_0, /*hidden argument*/List_1__ctor_m2651_MethodInfo_var);
		__this->___eventQueue_11 = L_0;
		Animator_t360 * L_1 = (__this->___anim_3);
		NullCheck(L_1);
		Animator_SetBool_m2646(L_1, _stringLiteral764, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AnimationControllerScript::ApareceLampada()
extern TypeInfo* U3CApareceLampadaU3Ec__IteratorF_t363_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationControllerScript_ApareceLampada_m1887 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CApareceLampadaU3Ec__IteratorF_t363_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(364);
		s_Il2CppMethodIntialized = true;
	}
	U3CApareceLampadaU3Ec__IteratorF_t363 * V_0 = {0};
	{
		U3CApareceLampadaU3Ec__IteratorF_t363 * L_0 = (U3CApareceLampadaU3Ec__IteratorF_t363 *)il2cpp_codegen_object_new (U3CApareceLampadaU3Ec__IteratorF_t363_il2cpp_TypeInfo_var);
		U3CApareceLampadaU3Ec__IteratorF__ctor_m1840(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CApareceLampadaU3Ec__IteratorF_t363 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CApareceLampadaU3Ec__IteratorF_t363 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator AnimationControllerScript::ApareceProveta()
extern TypeInfo* U3CApareceProvetaU3Ec__Iterator10_t365_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationControllerScript_ApareceProveta_m1888 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CApareceProvetaU3Ec__Iterator10_t365_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(365);
		s_Il2CppMethodIntialized = true;
	}
	U3CApareceProvetaU3Ec__Iterator10_t365 * V_0 = {0};
	{
		U3CApareceProvetaU3Ec__Iterator10_t365 * L_0 = (U3CApareceProvetaU3Ec__Iterator10_t365 *)il2cpp_codegen_object_new (U3CApareceProvetaU3Ec__Iterator10_t365_il2cpp_TypeInfo_var);
		U3CApareceProvetaU3Ec__Iterator10__ctor_m1846(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CApareceProvetaU3Ec__Iterator10_t365 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator AnimationControllerScript::DesapareceProveta()
extern TypeInfo* U3CDesapareceProvetaU3Ec__Iterator11_t366_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationControllerScript_DesapareceProveta_m1889 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDesapareceProvetaU3Ec__Iterator11_t366_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	U3CDesapareceProvetaU3Ec__Iterator11_t366 * V_0 = {0};
	{
		U3CDesapareceProvetaU3Ec__Iterator11_t366 * L_0 = (U3CDesapareceProvetaU3Ec__Iterator11_t366 *)il2cpp_codegen_object_new (U3CDesapareceProvetaU3Ec__Iterator11_t366_il2cpp_TypeInfo_var);
		U3CDesapareceProvetaU3Ec__Iterator11__ctor_m1852(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDesapareceProvetaU3Ec__Iterator11_t366 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator AnimationControllerScript::ApareceSaleiro()
extern TypeInfo* U3CApareceSaleiroU3Ec__Iterator12_t367_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationControllerScript_ApareceSaleiro_m1890 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CApareceSaleiroU3Ec__Iterator12_t367_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(367);
		s_Il2CppMethodIntialized = true;
	}
	U3CApareceSaleiroU3Ec__Iterator12_t367 * V_0 = {0};
	{
		U3CApareceSaleiroU3Ec__Iterator12_t367 * L_0 = (U3CApareceSaleiroU3Ec__Iterator12_t367 *)il2cpp_codegen_object_new (U3CApareceSaleiroU3Ec__Iterator12_t367_il2cpp_TypeInfo_var);
		U3CApareceSaleiroU3Ec__Iterator12__ctor_m1858(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CApareceSaleiroU3Ec__Iterator12_t367 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator AnimationControllerScript::LiquidoProveta()
extern TypeInfo* U3CLiquidoProvetaU3Ec__Iterator13_t368_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationControllerScript_LiquidoProveta_m1891 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CLiquidoProvetaU3Ec__Iterator13_t368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		s_Il2CppMethodIntialized = true;
	}
	U3CLiquidoProvetaU3Ec__Iterator13_t368 * V_0 = {0};
	{
		U3CLiquidoProvetaU3Ec__Iterator13_t368 * L_0 = (U3CLiquidoProvetaU3Ec__Iterator13_t368 *)il2cpp_codegen_object_new (U3CLiquidoProvetaU3Ec__Iterator13_t368_il2cpp_TypeInfo_var);
		U3CLiquidoProvetaU3Ec__Iterator13__ctor_m1864(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLiquidoProvetaU3Ec__Iterator13_t368 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		U3CLiquidoProvetaU3Ec__Iterator13_t368 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator AnimationControllerScript::AtivaParticulaPortal()
extern TypeInfo* U3CAtivaParticulaPortalU3Ec__Iterator14_t369_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationControllerScript_AtivaParticulaPortal_m1892 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAtivaParticulaPortalU3Ec__Iterator14_t369_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(369);
		s_Il2CppMethodIntialized = true;
	}
	U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * V_0 = {0};
	{
		U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * L_0 = (U3CAtivaParticulaPortalU3Ec__Iterator14_t369 *)il2cpp_codegen_object_new (U3CAtivaParticulaPortalU3Ec__Iterator14_t369_il2cpp_TypeInfo_var);
		U3CAtivaParticulaPortalU3Ec__Iterator14__ctor_m1870(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CAtivaParticulaPortalU3Ec__Iterator14_t369 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator AnimationControllerScript::EmmitSmoke()
extern TypeInfo* U3CEmmitSmokeU3Ec__Iterator15_t370_il2cpp_TypeInfo_var;
extern "C" Object_t * AnimationControllerScript_EmmitSmoke_m1893 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CEmmitSmokeU3Ec__Iterator15_t370_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	U3CEmmitSmokeU3Ec__Iterator15_t370 * V_0 = {0};
	{
		U3CEmmitSmokeU3Ec__Iterator15_t370 * L_0 = (U3CEmmitSmokeU3Ec__Iterator15_t370 *)il2cpp_codegen_object_new (U3CEmmitSmokeU3Ec__Iterator15_t370_il2cpp_TypeInfo_var);
		U3CEmmitSmokeU3Ec__Iterator15__ctor_m1876(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEmmitSmokeU3Ec__Iterator15_t370 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CEmmitSmokeU3Ec__Iterator15_t370 * L_2 = V_0;
		return L_2;
	}
}
// System.Void AnimationControllerScript::OnGUI()
extern "C" void AnimationControllerScript_OnGUI_m1894 (AnimationControllerScript_t364 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 AnimationControllerScript::<On>m__3A(AnimationControllerScript/TimeEvent,AnimationControllerScript/TimeEvent)
extern "C" int32_t AnimationControllerScript_U3COnU3Em__3A_m1895 (Object_t * __this /* static, unused */, TimeEvent_t362 * ___a, TimeEvent_t362 * ___b, const MethodInfo* method)
{
	{
		TimeEvent_t362 * L_0 = ___a;
		NullCheck(L_0);
		float* L_1 = &(L_0->___timer_1);
		TimeEvent_t362 * L_2 = ___b;
		NullCheck(L_2);
		float L_3 = (L_2->___timer_1);
		int32_t L_4 = Single_CompareTo_m2603(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void ArUiController/<ShareAfterLogin>c__Iterator16::.ctor()
extern "C" void U3CShareAfterLoginU3Ec__Iterator16__ctor_m1896 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ArUiController/<ShareAfterLogin>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CShareAfterLoginU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1897 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object ArUiController/<ShareAfterLogin>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CShareAfterLoginU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m1898 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean ArUiController/<ShareAfterLogin>c__Iterator16::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t500_il2cpp_TypeInfo_var;
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern "C" bool U3CShareAfterLoginU3Ec__Iterator16_MoveNext_m1899 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0059;
	}

IL_0021:
	{
		goto IL_003d;
	}

IL_0026:
	{
		WaitForEndOfFrame_t500 * L_2 = (WaitForEndOfFrame_t500 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t500_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m2246(L_2, /*hidden argument*/NULL);
		__this->___U24current_1 = L_2;
		__this->___U24PC_0 = 1;
		goto IL_005b;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		bool L_3 = FB_get_IsLoggedIn_m1307(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArUiController_t374 * L_4 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_4);
		ArUiController_Share_OnClick_m1922(L_4, /*hidden argument*/NULL);
		__this->___U24PC_0 = (-1);
	}

IL_0059:
	{
		return 0;
	}

IL_005b:
	{
		return 1;
	}
	// Dead block : IL_005d: ldloc.1
}
// System.Void ArUiController/<ShareAfterLogin>c__Iterator16::Dispose()
extern "C" void U3CShareAfterLoginU3Ec__Iterator16_Dispose_m1900 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void ArUiController/<ShareAfterLogin>c__Iterator16::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CShareAfterLoginU3Ec__Iterator16_Reset_m1901 (U3CShareAfterLoginU3Ec__Iterator16_t373 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void ArUiController/<ITakeScreenshot>c__Iterator17::.ctor()
extern "C" void U3CITakeScreenshotU3Ec__Iterator17__ctor_m1902 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ArUiController/<ITakeScreenshot>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CITakeScreenshotU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1903 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object ArUiController/<ITakeScreenshot>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CITakeScreenshotU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m1904 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean ArUiController/<ITakeScreenshot>c__Iterator17::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t500_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t37_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t33_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisRawImage_t578_m2656_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral766;
extern Il2CppCodeGenString* _stringLiteral767;
extern Il2CppCodeGenString* _stringLiteral768;
extern Il2CppCodeGenString* _stringLiteral769;
extern Il2CppCodeGenString* _stringLiteral770;
extern Il2CppCodeGenString* _stringLiteral771;
extern Il2CppCodeGenString* _stringLiteral772;
extern "C" bool U3CITakeScreenshotU3Ec__Iterator17_MoveNext_m1905 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		WaitForSeconds_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Texture2D_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		ComponentTracker_GetElement_TisRawImage_t578_m2656_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484095);
		_stringLiteral766 = il2cpp_codegen_string_literal_from_index(766);
		_stringLiteral767 = il2cpp_codegen_string_literal_from_index(767);
		_stringLiteral768 = il2cpp_codegen_string_literal_from_index(768);
		_stringLiteral769 = il2cpp_codegen_string_literal_from_index(769);
		_stringLiteral770 = il2cpp_codegen_string_literal_from_index(770);
		_stringLiteral771 = il2cpp_codegen_string_literal_from_index(771);
		_stringLiteral772 = il2cpp_codegen_string_literal_from_index(772);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0090;
		}
		if (L_1 == 2)
		{
			goto IL_00cb;
		}
	}
	{
		goto IL_0165;
	}

IL_0025:
	{
		ArUiController_t374 * L_2 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		ComponentTracker_t67 * L_3 = (L_2->___tracker_3);
		NullCheck(L_3);
		GameObject_t27 * L_4 = ComponentTracker_GetObject_m447(L_3, _stringLiteral766, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m2344(L_4, 0, /*hidden argument*/NULL);
		int32_t L_5 = Application_get_platform_m2652(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)7))))
		{
			goto IL_0060;
		}
	}
	{
		ArUiController_t374 * L_6 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_6);
		String_t* L_7 = ArUiController_get_tempFileName_m1911(L_6, /*hidden argument*/NULL);
		Application_CaptureScreenshot_m2653(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_0060:
	{
		Application_CaptureScreenshot_m2653(NULL /*static, unused*/, _stringLiteral767, /*hidden argument*/NULL);
	}

IL_006a:
	{
		MonoBehaviour_print_m2635(NULL /*static, unused*/, _stringLiteral768, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_0079:
	{
		WaitForEndOfFrame_t500 * L_8 = (WaitForEndOfFrame_t500 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t500_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m2246(L_8, /*hidden argument*/NULL);
		__this->___U24current_2 = L_8;
		__this->___U24PC_1 = 1;
		goto IL_0167;
	}

IL_0090:
	{
		ArUiController_t374 * L_9 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_9);
		String_t* L_10 = ArUiController_get_tempFileName_m1911(L_9, /*hidden argument*/NULL);
		bool L_11 = File_Exists_m2654(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0079;
		}
	}
	{
		MonoBehaviour_print_m2635(NULL /*static, unused*/, _stringLiteral769, /*hidden argument*/NULL);
		WaitForSeconds_t37 * L_12 = (WaitForSeconds_t37 *)il2cpp_codegen_object_new (WaitForSeconds_t37_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m295(L_12, (1.0f), /*hidden argument*/NULL);
		__this->___U24current_2 = L_12;
		__this->___U24PC_1 = 2;
		goto IL_0167;
	}

IL_00cb:
	{
		MonoBehaviour_print_m2635(NULL /*static, unused*/, _stringLiteral770, /*hidden argument*/NULL);
		ArUiController_t374 * L_13 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_13);
		ComponentTracker_t67 * L_14 = (L_13->___tracker_3);
		NullCheck(L_14);
		GameObject_t27 * L_15 = ComponentTracker_GetObject_m447(L_14, _stringLiteral766, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_SetActive_m2344(L_15, 1, /*hidden argument*/NULL);
		ArUiController_t374 * L_16 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_16);
		ComponentTracker_t67 * L_17 = (L_16->___tracker_3);
		NullCheck(L_17);
		GameObject_t27 * L_18 = ComponentTracker_GetObject_m447(L_17, _stringLiteral771, /*hidden argument*/NULL);
		NullCheck(L_18);
		GameObject_SetActive_m2344(L_18, 1, /*hidden argument*/NULL);
		int32_t L_19 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t33 * L_21 = (Texture2D_t33 *)il2cpp_codegen_object_new (Texture2D_t33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m398(L_21, L_19, L_20, 3, 0, /*hidden argument*/NULL);
		__this->___U3CtexU3E__0_0 = L_21;
		Texture2D_t33 * L_22 = (__this->___U3CtexU3E__0_0);
		ArUiController_t374 * L_23 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_23);
		String_t* L_24 = ArUiController_get_tempFileName_m1911(L_23, /*hidden argument*/NULL);
		ByteU5BU5D_t119* L_25 = File_ReadAllBytes_m2655(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		Texture2D_LoadImage_m2223(L_22, L_25, /*hidden argument*/NULL);
		ArUiController_t374 * L_26 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_26);
		ComponentTracker_t67 * L_27 = (L_26->___tracker_3);
		NullCheck(L_27);
		RawImage_t578 * L_28 = ComponentTracker_GetElement_TisRawImage_t578_m2656(L_27, _stringLiteral772, /*hidden argument*/ComponentTracker_GetElement_TisRawImage_t578_m2656_MethodInfo_var);
		Texture2D_t33 * L_29 = (__this->___U3CtexU3E__0_0);
		NullCheck(L_28);
		RawImage_set_texture_m2657(L_28, L_29, /*hidden argument*/NULL);
		__this->___U24PC_1 = (-1);
	}

IL_0165:
	{
		return 0;
	}

IL_0167:
	{
		return 1;
	}
	// Dead block : IL_0169: ldloc.1
}
// System.Void ArUiController/<ITakeScreenshot>c__Iterator17::Dispose()
extern "C" void U3CITakeScreenshotU3Ec__Iterator17_Dispose_m1906 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void ArUiController/<ITakeScreenshot>c__Iterator17::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CITakeScreenshotU3Ec__Iterator17_Reset_m1907 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void ArUiController::.ctor()
extern "C" void ArUiController__ctor_m1908 (ArUiController_t374 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// ArUiController ArUiController::get_Instance()
extern TypeInfo* ArUiController_t374_il2cpp_TypeInfo_var;
extern "C" ArUiController_t374 * ArUiController_get_Instance_m1909 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArUiController_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArUiController_t374 * L_0 = ((ArUiController_t374_StaticFields*)ArUiController_t374_il2cpp_TypeInfo_var->static_fields)->___instance_1;
		return L_0;
	}
}
// System.Void ArUiController::InitializeSingleton()
extern TypeInfo* ArUiController_t374_il2cpp_TypeInfo_var;
extern "C" void ArUiController_InitializeSingleton_m1910 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArUiController_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArUiController_t374 * L_0 = ((ArUiController_t374_StaticFields*)ArUiController_t374_il2cpp_TypeInfo_var->static_fields)->___instance_1;
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Object_Destroy_m401(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_0021;
	}

IL_001b:
	{
		((ArUiController_t374_StaticFields*)ArUiController_t374_il2cpp_TypeInfo_var->static_fields)->___instance_1 = __this;
	}

IL_0021:
	{
		return;
	}
}
// System.String ArUiController::get_tempFileName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral773;
extern Il2CppCodeGenString* _stringLiteral774;
extern "C" String_t* ArUiController_get_tempFileName_m1911 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral773 = il2cpp_codegen_string_literal_from_index(773);
		_stringLiteral774 = il2cpp_codegen_string_literal_from_index(774);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Application_get_persistentDataPath_m2658(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2403(NULL /*static, unused*/, _stringLiteral773, L_0, _stringLiteral774, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ArUiController::Awake()
extern "C" void ArUiController_Awake_m1912 (ArUiController_t374 * __this, const MethodInfo* method)
{
	{
		ArUiController_InitializeSingleton_m1910(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::Start()
extern const MethodInfo* ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral771;
extern Il2CppCodeGenString* _stringLiteral750;
extern Il2CppCodeGenString* _stringLiteral775;
extern "C" void ArUiController_Start_m1913 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484096);
		_stringLiteral771 = il2cpp_codegen_string_literal_from_index(771);
		_stringLiteral750 = il2cpp_codegen_string_literal_from_index(750);
		_stringLiteral775 = il2cpp_codegen_string_literal_from_index(775);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tracker_3 = L_0;
		ComponentTracker_t67 * L_1 = (__this->___tracker_3);
		NullCheck(L_1);
		GameObject_t27 * L_2 = ComponentTracker_GetObject_m447(L_1, _stringLiteral771, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m2344(L_2, 0, /*hidden argument*/NULL);
		ComponentTracker_t67 * L_3 = (__this->___tracker_3);
		NullCheck(L_3);
		GameObject_t27 * L_4 = ComponentTracker_GetObject_m447(L_3, _stringLiteral750, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m2344(L_4, 0, /*hidden argument*/NULL);
		String_t* L_5 = ArUiController_get_tempFileName_m1911(__this, /*hidden argument*/NULL);
		bool L_6 = File_Exists_m2654(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		String_t* L_7 = ArUiController_get_tempFileName_m1911(__this, /*hidden argument*/NULL);
		File_Delete_m2659(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0052:
	{
		ComponentTracker_t67 * L_8 = (__this->___tracker_3);
		NullCheck(L_8);
		CanvasGroup_t579 * L_9 = ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(L_8, _stringLiteral775, /*hidden argument*/ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var);
		NullCheck(L_9);
		CanvasGroup_set_alpha_m2661(L_9, (0.0f), /*hidden argument*/NULL);
		ComponentTracker_t67 * L_10 = (__this->___tracker_3);
		NullCheck(L_10);
		CanvasGroup_t579 * L_11 = ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(L_10, _stringLiteral775, /*hidden argument*/ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var);
		NullCheck(L_11);
		CanvasGroup_set_blocksRaycasts_m2662(L_11, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::Update()
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisText_t580_m2663_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral776;
extern Il2CppCodeGenString* _stringLiteral777;
extern Il2CppCodeGenString* _stringLiteral778;
extern Il2CppCodeGenString* _stringLiteral779;
extern Il2CppCodeGenString* _stringLiteral780;
extern Il2CppCodeGenString* _stringLiteral781;
extern "C" void ArUiController_Update_m1914 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		ComponentTracker_GetElement_TisText_t580_m2663_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484097);
		_stringLiteral776 = il2cpp_codegen_string_literal_from_index(776);
		_stringLiteral777 = il2cpp_codegen_string_literal_from_index(777);
		_stringLiteral778 = il2cpp_codegen_string_literal_from_index(778);
		_stringLiteral779 = il2cpp_codegen_string_literal_from_index(779);
		_stringLiteral780 = il2cpp_codegen_string_literal_from_index(780);
		_stringLiteral781 = il2cpp_codegen_string_literal_from_index(781);
		s_Il2CppMethodIntialized = true;
	}
	Text_t580 * G_B2_0 = {0};
	Text_t580 * G_B1_0 = {0};
	String_t* G_B3_0 = {0};
	Text_t580 * G_B3_1 = {0};
	Text_t580 * G_B5_0 = {0};
	Text_t580 * G_B4_0 = {0};
	String_t* G_B6_0 = {0};
	Text_t580 * G_B6_1 = {0};
	{
		ComponentTracker_t67 * L_0 = (__this->___tracker_3);
		NullCheck(L_0);
		Text_t580 * L_1 = ComponentTracker_GetElement_TisText_t580_m2663(L_0, _stringLiteral776, /*hidden argument*/ComponentTracker_GetElement_TisText_t580_m2663_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		bool L_2 = FB_get_IsLoggedIn_m1307(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_0024;
		}
	}
	{
		G_B3_0 = _stringLiteral777;
		G_B3_1 = G_B1_0;
		goto IL_0029;
	}

IL_0024:
	{
		G_B3_0 = _stringLiteral778;
		G_B3_1 = G_B2_0;
	}

IL_0029:
	{
		NullCheck(G_B3_1);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B3_1, G_B3_0);
		ComponentTracker_t67 * L_3 = (__this->___tracker_3);
		NullCheck(L_3);
		Text_t580 * L_4 = ComponentTracker_GetElement_TisText_t580_m2663(L_3, _stringLiteral779, /*hidden argument*/ComponentTracker_GetElement_TisText_t580_m2663_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		bool L_5 = FB_get_IsLoggedIn_m1307(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		if (!L_5)
		{
			G_B5_0 = L_4;
			goto IL_0052;
		}
	}
	{
		G_B6_0 = _stringLiteral780;
		G_B6_1 = G_B4_0;
		goto IL_0057;
	}

IL_0052:
	{
		G_B6_0 = _stringLiteral781;
		G_B6_1 = G_B5_0;
	}

IL_0057:
	{
		NullCheck(G_B6_1);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B6_1, G_B6_0);
		return;
	}
}
// System.Void ArUiController::Return_OnClick()
extern "C" void ArUiController_Return_OnClick_m1915 (ArUiController_t374 * __this, const MethodInfo* method)
{
	{
		Application_LoadLevel_m2591(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::Capture_OnClick()
extern "C" void ArUiController_Capture_OnClick_m1916 (ArUiController_t374 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = ArUiController_ITakeScreenshot_m1928(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2199(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::CaptureCancel_OnClick()
extern Il2CppCodeGenString* _stringLiteral771;
extern "C" void ArUiController_CaptureCancel_OnClick_m1917 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral771 = il2cpp_codegen_string_literal_from_index(771);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = (__this->___tracker_3);
		NullCheck(L_0);
		GameObject_t27 * L_1 = ComponentTracker_GetObject_m447(L_0, _stringLiteral771, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2344(L_1, 0, /*hidden argument*/NULL);
		String_t* L_2 = ArUiController_get_tempFileName_m1911(__this, /*hidden argument*/NULL);
		bool L_3 = File_Exists_m2654(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_4 = ArUiController_get_tempFileName_m1911(__this, /*hidden argument*/NULL);
		File_Delete_m2659(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void ArUiController::Info_OnClick()
extern Il2CppCodeGenString* _stringLiteral766;
extern Il2CppCodeGenString* _stringLiteral750;
extern "C" void ArUiController_Info_OnClick_m1918 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral766 = il2cpp_codegen_string_literal_from_index(766);
		_stringLiteral750 = il2cpp_codegen_string_literal_from_index(750);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = (__this->___tracker_3);
		NullCheck(L_0);
		GameObject_t27 * L_1 = ComponentTracker_GetObject_m447(L_0, _stringLiteral766, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2344(L_1, 0, /*hidden argument*/NULL);
		ComponentTracker_t67 * L_2 = (__this->___tracker_3);
		NullCheck(L_2);
		GameObject_t27 * L_3 = ComponentTracker_GetObject_m447(L_2, _stringLiteral750, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2344(L_3, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::Info_Link_OnClick()
extern Il2CppCodeGenString* _stringLiteral752;
extern "C" void ArUiController_Info_Link_OnClick_m1919 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral752 = il2cpp_codegen_string_literal_from_index(752);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_OpenURL_m2496(NULL /*static, unused*/, _stringLiteral752, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::InfoBack_OnClick()
extern Il2CppCodeGenString* _stringLiteral750;
extern Il2CppCodeGenString* _stringLiteral766;
extern "C" void ArUiController_InfoBack_OnClick_m1920 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral750 = il2cpp_codegen_string_literal_from_index(750);
		_stringLiteral766 = il2cpp_codegen_string_literal_from_index(766);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = (__this->___tracker_3);
		NullCheck(L_0);
		GameObject_t27 * L_1 = ComponentTracker_GetObject_m447(L_0, _stringLiteral750, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2344(L_1, 0, /*hidden argument*/NULL);
		ComponentTracker_t67 * L_2 = (__this->___tracker_3);
		NullCheck(L_2);
		GameObject_t27 * L_3 = ComponentTracker_GetObject_m447(L_2, _stringLiteral766, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2344(L_3, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::Save_OnClick()
extern TypeInfo* ISN_Singleton_1_t146_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t99_il2cpp_TypeInfo_var;
extern const MethodInfo* ISN_Singleton_1_get_instance_m2336_MethodInfo_var;
extern const MethodInfo* ArUiController_OnImageSaved_m1929_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2177_MethodInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral782;
extern "C" void ArUiController_Save_OnClick_m1921 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ISN_Singleton_1_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(126);
		Action_1_t99_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		ISN_Singleton_1_get_instance_m2336_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483916);
		ArUiController_OnImageSaved_m1929_MethodInfo_var = il2cpp_codegen_method_info_from_index(450);
		Action_1__ctor_m2177_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483736);
		ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484099);
		_stringLiteral782 = il2cpp_codegen_string_literal_from_index(782);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ArUiController_get_tempFileName_m1911(__this, /*hidden argument*/NULL);
		bool L_1 = File_Exists_m2654(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0065;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ISN_Singleton_1_t146_il2cpp_TypeInfo_var);
		IOSCamera_t145 * L_2 = ISN_Singleton_1_get_instance_m2336(NULL /*static, unused*/, /*hidden argument*/ISN_Singleton_1_get_instance_m2336_MethodInfo_var);
		IOSCamera_t145 * L_3 = L_2;
		NullCheck(L_3);
		Action_1_t99 * L_4 = (L_3->___OnImageSaved_8);
		IntPtr_t L_5 = { (void*)ArUiController_OnImageSaved_m1929_MethodInfo_var };
		Action_1_t99 * L_6 = (Action_1_t99 *)il2cpp_codegen_object_new (Action_1_t99_il2cpp_TypeInfo_var);
		Action_1__ctor_m2177(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m2177_MethodInfo_var);
		Delegate_t506 * L_7 = Delegate_Combine_m2300(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->___OnImageSaved_8 = ((Action_1_t99 *)CastclassSealed(L_7, Action_1_t99_il2cpp_TypeInfo_var));
		IOSCamera_t145 * L_8 = ISN_Singleton_1_get_instance_m2336(NULL /*static, unused*/, /*hidden argument*/ISN_Singleton_1_get_instance_m2336_MethodInfo_var);
		String_t* L_9 = ArUiController_get_tempFileName_m1911(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t119* L_10 = File_ReadAllBytes_m2655(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IOSCamera_SaveTextureToCameraRoll_m827(L_8, L_10, /*hidden argument*/NULL);
		ComponentTracker_t67 * L_11 = (__this->___tracker_3);
		NullCheck(L_11);
		Popup_t384 * L_12 = ComponentTracker_GetElement_TisPopup_t384_m2664(L_11, _stringLiteral782, /*hidden argument*/ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var);
		NullCheck(L_12);
		Popup_Activate_m1967(L_12, (3.0f), /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void ArUiController::Share_OnClick()
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral775;
extern "C" void ArUiController_Share_OnClick_m1922 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484096);
		_stringLiteral775 = il2cpp_codegen_string_literal_from_index(775);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsInitialized_m1308(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		FBServices_t381 * L_1 = FBServices_get_Current_m1949(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		FBServices_Init_m1952(L_1, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		ComponentTracker_t67 * L_2 = (__this->___tracker_3);
		NullCheck(L_2);
		CanvasGroup_t579 * L_3 = ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(L_2, _stringLiteral775, /*hidden argument*/ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var);
		NullCheck(L_3);
		CanvasGroup_set_alpha_m2661(L_3, (1.0f), /*hidden argument*/NULL);
		ComponentTracker_t67 * L_4 = (__this->___tracker_3);
		NullCheck(L_4);
		CanvasGroup_t579 * L_5 = ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(L_4, _stringLiteral775, /*hidden argument*/ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var);
		NullCheck(L_5);
		CanvasGroup_set_blocksRaycasts_m2662(L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::Login_OnClick()
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t380_il2cpp_TypeInfo_var;
extern TypeInfo* FacebookDelegate_1_t294_il2cpp_TypeInfo_var;
extern const MethodInfo* ArUiController_LoginCallback_m1925_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m2665_MethodInfo_var;
extern const MethodInfo* ArUiController_ApiCallback_m1927_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2497_MethodInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral783;
extern Il2CppCodeGenString* _stringLiteral784;
extern Il2CppCodeGenString* _stringLiteral775;
extern "C" void ArUiController_Login_OnClick_m1923 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		Action_2_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(376);
		FacebookDelegate_1_t294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(294);
		ArUiController_LoginCallback_m1925_MethodInfo_var = il2cpp_codegen_method_info_from_index(452);
		Action_2__ctor_m2665_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484101);
		ArUiController_ApiCallback_m1927_MethodInfo_var = il2cpp_codegen_method_info_from_index(454);
		FacebookDelegate_1__ctor_m2497_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484030);
		ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484099);
		ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484096);
		_stringLiteral783 = il2cpp_codegen_string_literal_from_index(783);
		_stringLiteral784 = il2cpp_codegen_string_literal_from_index(784);
		_stringLiteral775 = il2cpp_codegen_string_literal_from_index(775);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsLoggedIn_m1307(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		FBServices_t381 * L_1 = FBServices_get_Current_m1949(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)ArUiController_LoginCallback_m1925_MethodInfo_var };
		Action_2_t380 * L_3 = (Action_2_t380 *)il2cpp_codegen_object_new (Action_2_t380_il2cpp_TypeInfo_var);
		Action_2__ctor_m2665(L_3, __this, L_2, /*hidden argument*/Action_2__ctor_m2665_MethodInfo_var);
		NullCheck(L_1);
		FBServices_Login_m1954(L_1, L_3, /*hidden argument*/NULL);
		goto IL_00bb;
	}

IL_0025:
	{
		float L_4 = (__this->___sharedDelay_2);
		float L_5 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_00bb;
		}
	}
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral783, /*hidden argument*/NULL);
		FBServices_t381 * L_6 = FBServices_get_Current_m1949(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = ArUiController_get_tempFileName_m1911(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t119* L_8 = File_ReadAllBytes_m2655(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_9 = { (void*)ArUiController_ApiCallback_m1927_MethodInfo_var };
		FacebookDelegate_1_t294 * L_10 = (FacebookDelegate_1_t294 *)il2cpp_codegen_object_new (FacebookDelegate_1_t294_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2497(L_10, __this, L_9, /*hidden argument*/FacebookDelegate_1__ctor_m2497_MethodInfo_var);
		NullCheck(L_6);
		FBServices_API_m1957(L_6, L_8, L_10, /*hidden argument*/NULL);
		ComponentTracker_t67 * L_11 = (__this->___tracker_3);
		NullCheck(L_11);
		Popup_t384 * L_12 = ComponentTracker_GetElement_TisPopup_t384_m2664(L_11, _stringLiteral784, /*hidden argument*/ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var);
		NullCheck(L_12);
		Popup_Activate_m1967(L_12, (0.0f), /*hidden argument*/NULL);
		float L_13 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___sharedDelay_2 = ((float)((float)L_13+(float)(1.0f)));
		ComponentTracker_t67 * L_14 = (__this->___tracker_3);
		NullCheck(L_14);
		CanvasGroup_t579 * L_15 = ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(L_14, _stringLiteral775, /*hidden argument*/ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var);
		NullCheck(L_15);
		CanvasGroup_set_alpha_m2661(L_15, (0.0f), /*hidden argument*/NULL);
		ComponentTracker_t67 * L_16 = (__this->___tracker_3);
		NullCheck(L_16);
		CanvasGroup_t579 * L_17 = ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(L_16, _stringLiteral775, /*hidden argument*/ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var);
		NullCheck(L_17);
		CanvasGroup_set_blocksRaycasts_m2662(L_17, 0, /*hidden argument*/NULL);
	}

IL_00bb:
	{
		return;
	}
}
// System.Void ArUiController::CancelLogin_OnClick()
extern const MethodInfo* ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral775;
extern "C" void ArUiController_CancelLogin_OnClick_m1924 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484096);
		_stringLiteral775 = il2cpp_codegen_string_literal_from_index(775);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = (__this->___tracker_3);
		NullCheck(L_0);
		CanvasGroup_t579 * L_1 = ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(L_0, _stringLiteral775, /*hidden argument*/ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var);
		NullCheck(L_1);
		CanvasGroup_set_alpha_m2661(L_1, (0.0f), /*hidden argument*/NULL);
		ComponentTracker_t67 * L_2 = (__this->___tracker_3);
		NullCheck(L_2);
		CanvasGroup_t579 * L_3 = ComponentTracker_GetElement_TisCanvasGroup_t579_m2660(L_2, _stringLiteral775, /*hidden argument*/ComponentTracker_GetElement_TisCanvasGroup_t579_m2660_MethodInfo_var);
		NullCheck(L_3);
		CanvasGroup_set_blocksRaycasts_m2662(L_3, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArUiController::LoginCallback(System.Boolean,Facebook.Unity.ILoginResult)
extern const MethodInfo* ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral785;
extern Il2CppCodeGenString* _stringLiteral786;
extern "C" void ArUiController_LoginCallback_m1925 (ArUiController_t374 * __this, bool ___successful, Object_t * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484099);
		_stringLiteral785 = il2cpp_codegen_string_literal_from_index(785);
		_stringLiteral786 = il2cpp_codegen_string_literal_from_index(786);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___successful;
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		ComponentTracker_t67 * L_1 = (__this->___tracker_3);
		NullCheck(L_1);
		Popup_t384 * L_2 = ComponentTracker_GetElement_TisPopup_t384_m2664(L_1, _stringLiteral785, /*hidden argument*/ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var);
		NullCheck(L_2);
		Popup_Activate_m1967(L_2, (3.0f), /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_0025:
	{
		ComponentTracker_t67 * L_3 = (__this->___tracker_3);
		NullCheck(L_3);
		Popup_t384 * L_4 = ComponentTracker_GetElement_TisPopup_t384_m2664(L_3, _stringLiteral786, /*hidden argument*/ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var);
		NullCheck(L_4);
		Popup_Activate_m1967(L_4, (3.0f), /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Collections.IEnumerator ArUiController::ShareAfterLogin()
extern TypeInfo* U3CShareAfterLoginU3Ec__Iterator16_t373_il2cpp_TypeInfo_var;
extern "C" Object_t * ArUiController_ShareAfterLogin_m1926 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CShareAfterLoginU3Ec__Iterator16_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(377);
		s_Il2CppMethodIntialized = true;
	}
	U3CShareAfterLoginU3Ec__Iterator16_t373 * V_0 = {0};
	{
		U3CShareAfterLoginU3Ec__Iterator16_t373 * L_0 = (U3CShareAfterLoginU3Ec__Iterator16_t373 *)il2cpp_codegen_object_new (U3CShareAfterLoginU3Ec__Iterator16_t373_il2cpp_TypeInfo_var);
		U3CShareAfterLoginU3Ec__Iterator16__ctor_m1896(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShareAfterLoginU3Ec__Iterator16_t373 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CShareAfterLoginU3Ec__Iterator16_t373 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ArUiController::ApiCallback(Facebook.Unity.IGraphResult)
extern TypeInfo* IResult_t464_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral784;
extern Il2CppCodeGenString* _stringLiteral787;
extern Il2CppCodeGenString* _stringLiteral788;
extern "C" void ArUiController_ApiCallback_m1927 (ArUiController_t374 * __this, Object_t * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IResult_t464_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484099);
		_stringLiteral784 = il2cpp_codegen_string_literal_from_index(784);
		_stringLiteral787 = il2cpp_codegen_string_literal_from_index(787);
		_stringLiteral788 = il2cpp_codegen_string_literal_from_index(788);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = (__this->___tracker_3);
		NullCheck(L_0);
		Popup_t384 * L_1 = ComponentTracker_GetElement_TisPopup_t384_m2664(L_0, _stringLiteral784, /*hidden argument*/ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var);
		NullCheck(L_1);
		Popup_Deactivate_m1968(L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ___result;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t464_il2cpp_TypeInfo_var, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2149(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		ComponentTracker_t67 * L_5 = (__this->___tracker_3);
		NullCheck(L_5);
		Popup_t384 * L_6 = ComponentTracker_GetElement_TisPopup_t384_m2664(L_5, _stringLiteral787, /*hidden argument*/ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var);
		NullCheck(L_6);
		Popup_Activate_m1967(L_6, (3.0f), /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0044:
	{
		ComponentTracker_t67 * L_7 = (__this->___tracker_3);
		NullCheck(L_7);
		Popup_t384 * L_8 = ComponentTracker_GetElement_TisPopup_t384_m2664(L_7, _stringLiteral788, /*hidden argument*/ComponentTracker_GetElement_TisPopup_t384_m2664_MethodInfo_var);
		NullCheck(L_8);
		Popup_Activate_m1967(L_8, (3.0f), /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Collections.IEnumerator ArUiController::ITakeScreenshot()
extern TypeInfo* U3CITakeScreenshotU3Ec__Iterator17_t375_il2cpp_TypeInfo_var;
extern "C" Object_t * ArUiController_ITakeScreenshot_m1928 (ArUiController_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CITakeScreenshotU3Ec__Iterator17_t375_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(378);
		s_Il2CppMethodIntialized = true;
	}
	U3CITakeScreenshotU3Ec__Iterator17_t375 * V_0 = {0};
	{
		U3CITakeScreenshotU3Ec__Iterator17_t375 * L_0 = (U3CITakeScreenshotU3Ec__Iterator17_t375 *)il2cpp_codegen_object_new (U3CITakeScreenshotU3Ec__Iterator17_t375_il2cpp_TypeInfo_var);
		U3CITakeScreenshotU3Ec__Iterator17__ctor_m1902(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CITakeScreenshotU3Ec__Iterator17_t375 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CITakeScreenshotU3Ec__Iterator17_t375 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ArUiController::OnImageSaved(ISN_Result)
extern TypeInfo* ISN_Singleton_1_t146_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t99_il2cpp_TypeInfo_var;
extern const MethodInfo* ISN_Singleton_1_get_instance_m2336_MethodInfo_var;
extern const MethodInfo* ArUiController_OnImageSaved_m1929_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2177_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral213;
extern Il2CppCodeGenString* _stringLiteral384;
extern Il2CppCodeGenString* _stringLiteral382;
extern Il2CppCodeGenString* _stringLiteral385;
extern "C" void ArUiController_OnImageSaved_m1929 (ArUiController_t374 * __this, ISN_Result_t114 * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ISN_Singleton_1_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(126);
		Action_1_t99_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		ISN_Singleton_1_get_instance_m2336_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483916);
		ArUiController_OnImageSaved_m1929_MethodInfo_var = il2cpp_codegen_method_info_from_index(450);
		Action_1__ctor_m2177_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483736);
		_stringLiteral213 = il2cpp_codegen_string_literal_from_index(213);
		_stringLiteral384 = il2cpp_codegen_string_literal_from_index(384);
		_stringLiteral382 = il2cpp_codegen_string_literal_from_index(382);
		_stringLiteral385 = il2cpp_codegen_string_literal_from_index(385);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ISN_Singleton_1_t146_il2cpp_TypeInfo_var);
		IOSCamera_t145 * L_0 = ISN_Singleton_1_get_instance_m2336(NULL /*static, unused*/, /*hidden argument*/ISN_Singleton_1_get_instance_m2336_MethodInfo_var);
		IOSCamera_t145 * L_1 = L_0;
		NullCheck(L_1);
		Action_1_t99 * L_2 = (L_1->___OnImageSaved_8);
		IntPtr_t L_3 = { (void*)ArUiController_OnImageSaved_m1929_MethodInfo_var };
		Action_1_t99 * L_4 = (Action_1_t99 *)il2cpp_codegen_object_new (Action_1_t99_il2cpp_TypeInfo_var);
		Action_1__ctor_m2177(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m2177_MethodInfo_var);
		Delegate_t506 * L_5 = Delegate_Remove_m2302(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___OnImageSaved_8 = ((Action_1_t99 *)CastclassSealed(L_5, Action_1_t99_il2cpp_TypeInfo_var));
		ISN_Result_t114 * L_6 = ___result;
		NullCheck(L_6);
		bool L_7 = ISN_Result_get_IsSucceeded_m934(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		IOSMessage_Create_m885(NULL /*static, unused*/, _stringLiteral213, _stringLiteral384, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0046:
	{
		IOSMessage_Create_m885(NULL /*static, unused*/, _stringLiteral382, _stringLiteral385, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void ArUiController::OnGUI()
extern "C" void ArUiController_OnGUI_m1930 (ArUiController_t374 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Bezier::.ctor()
extern "C" void Bezier__ctor_m1931 (Bezier_t376 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Bezier::GetBezierPosition(System.Single)
extern Il2CppCodeGenString* _stringLiteral633;
extern "C" Vector3_t6  Bezier_GetBezierPosition_m1932 (Bezier_t376 * __this, float ___progress, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral633 = il2cpp_codegen_string_literal_from_index(633);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t6  V_1 = {0};
	Vector3_t6  V_2 = {0};
	Vector3_t6  V_3 = {0};
	Vector3_t6  V_4 = {0};
	Vector3_t6  V_5 = {0};
	Vector3_t6  V_6 = {0};
	{
		float L_0 = ___progress;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0026;
		}
	}
	{
		float L_1 = ___progress;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_print_m2635(NULL /*static, unused*/, _stringLiteral633, /*hidden argument*/NULL);
		Vector3_t6  L_2 = Vector3_get_zero_m300(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0026:
	{
		float L_3 = ___progress;
		V_0 = L_3;
		Transform_t25 * L_4 = (__this->___p1_2);
		NullCheck(L_4);
		Vector3_t6  L_5 = Transform_get_position_m334(L_4, /*hidden argument*/NULL);
		Transform_t25 * L_6 = (__this->___p2_3);
		NullCheck(L_6);
		Vector3_t6  L_7 = Transform_get_position_m334(L_6, /*hidden argument*/NULL);
		float L_8 = V_0;
		Vector3_t6  L_9 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_5, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Transform_t25 * L_10 = (__this->___p2_3);
		NullCheck(L_10);
		Vector3_t6  L_11 = Transform_get_position_m334(L_10, /*hidden argument*/NULL);
		Transform_t25 * L_12 = (__this->___p3_4);
		NullCheck(L_12);
		Vector3_t6  L_13 = Transform_get_position_m334(L_12, /*hidden argument*/NULL);
		float L_14 = V_0;
		Vector3_t6  L_15 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_11, L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		Transform_t25 * L_16 = (__this->___p3_4);
		NullCheck(L_16);
		Vector3_t6  L_17 = Transform_get_position_m334(L_16, /*hidden argument*/NULL);
		Transform_t25 * L_18 = (__this->___p4_5);
		NullCheck(L_18);
		Vector3_t6  L_19 = Transform_get_position_m334(L_18, /*hidden argument*/NULL);
		float L_20 = V_0;
		Vector3_t6  L_21 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_17, L_19, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		Vector3_t6  L_22 = V_1;
		Vector3_t6  L_23 = V_2;
		float L_24 = V_0;
		Vector3_t6  L_25 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_4 = L_25;
		Vector3_t6  L_26 = V_2;
		Vector3_t6  L_27 = V_3;
		float L_28 = V_0;
		Vector3_t6  L_29 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		Vector3_t6  L_30 = V_4;
		Vector3_t6  L_31 = V_5;
		float L_32 = V_0;
		Vector3_t6  L_33 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_30, L_31, L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		Vector3_t6  L_34 = V_6;
		return L_34;
	}
}
// System.Void Bezier::OnDrawGizmos()
extern "C" void Bezier_OnDrawGizmos_m1933 (Bezier_t376 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Transform_t25 * L_0 = (__this->___p1_2);
		bool L_1 = Object_op_Equality_m427(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0044;
		}
	}
	{
		Transform_t25 * L_2 = (__this->___p2_3);
		bool L_3 = Object_op_Equality_m427(NULL /*static, unused*/, L_2, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		Transform_t25 * L_4 = (__this->___p3_4);
		bool L_5 = Object_op_Equality_m427(NULL /*static, unused*/, L_4, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0044;
		}
	}
	{
		Transform_t25 * L_6 = (__this->___p4_5);
		bool L_7 = Object_op_Equality_m427(NULL /*static, unused*/, L_6, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0045;
		}
	}

IL_0044:
	{
		return;
	}

IL_0045:
	{
		Color_t5  L_8 = Color_get_red_m2327(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m418(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Transform_t25 * L_9 = (__this->___p1_2);
		NullCheck(L_9);
		Vector3_t6  L_10 = Transform_get_position_m334(L_9, /*hidden argument*/NULL);
		Gizmos_DrawSphere_m2666(NULL /*static, unused*/, L_10, (0.1f), /*hidden argument*/NULL);
		Transform_t25 * L_11 = (__this->___p2_3);
		NullCheck(L_11);
		Vector3_t6  L_12 = Transform_get_position_m334(L_11, /*hidden argument*/NULL);
		Gizmos_DrawSphere_m2666(NULL /*static, unused*/, L_12, (0.1f), /*hidden argument*/NULL);
		Transform_t25 * L_13 = (__this->___p1_2);
		NullCheck(L_13);
		Vector3_t6  L_14 = Transform_get_position_m334(L_13, /*hidden argument*/NULL);
		Transform_t25 * L_15 = (__this->___p2_3);
		NullCheck(L_15);
		Vector3_t6  L_16 = Transform_get_position_m334(L_15, /*hidden argument*/NULL);
		Gizmos_DrawLine_m419(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		Color_t5  L_17 = Color_get_blue_m2667(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m418(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Transform_t25 * L_18 = (__this->___p3_4);
		NullCheck(L_18);
		Vector3_t6  L_19 = Transform_get_position_m334(L_18, /*hidden argument*/NULL);
		Gizmos_DrawSphere_m2666(NULL /*static, unused*/, L_19, (0.1f), /*hidden argument*/NULL);
		Transform_t25 * L_20 = (__this->___p4_5);
		NullCheck(L_20);
		Vector3_t6  L_21 = Transform_get_position_m334(L_20, /*hidden argument*/NULL);
		Gizmos_DrawSphere_m2666(NULL /*static, unused*/, L_21, (0.1f), /*hidden argument*/NULL);
		Transform_t25 * L_22 = (__this->___p3_4);
		NullCheck(L_22);
		Vector3_t6  L_23 = Transform_get_position_m334(L_22, /*hidden argument*/NULL);
		Transform_t25 * L_24 = (__this->___p4_5);
		NullCheck(L_24);
		Vector3_t6  L_25 = Transform_get_position_m334(L_24, /*hidden argument*/NULL);
		Gizmos_DrawLine_m419(NULL /*static, unused*/, L_23, L_25, /*hidden argument*/NULL);
		int32_t L_26 = (__this->___iterations_1);
		V_0 = ((float)((float)(1.0f)/(float)(((float)((float)L_26)))));
		V_1 = (0.0f);
		goto IL_0126;
	}

IL_00fc:
	{
		Color_t5  L_27 = Color_get_red_m2327(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t5  L_28 = Color_get_blue_m2667(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_29 = V_1;
		Color_t5  L_30 = Color_Lerp_m2611(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		Gizmos_set_color_m418(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		float L_31 = V_1;
		Vector3_t6  L_32 = Bezier_GetBezierPosition_m1932(__this, L_31, /*hidden argument*/NULL);
		Gizmos_DrawSphere_m2666(NULL /*static, unused*/, L_32, (0.1f), /*hidden argument*/NULL);
		float L_33 = V_1;
		float L_34 = V_0;
		V_1 = ((float)((float)L_33+(float)L_34));
	}

IL_0126:
	{
		float L_35 = V_1;
		if ((((float)L_35) <= ((float)(1.0f))))
		{
			goto IL_00fc;
		}
	}
	{
		return;
	}
}
// System.Void BezierTraveler::.ctor()
extern "C" void BezierTraveler__ctor_m1934 (BezierTraveler_t342 * __this, const MethodInfo* method)
{
	{
		__this->___speed_2 = (1.0f);
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single BezierTraveler::get_normalizedProgress()
extern "C" float BezierTraveler_get_normalizedProgress_m1935 (BezierTraveler_t342 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___progress_3);
		BezierU5BU5D_t377* L_1 = (__this->___beziers_1);
		NullCheck(L_1);
		return ((float)((float)L_0/(float)(((float)((float)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))))));
	}
}
// System.Boolean BezierTraveler::get_isPlaying()
extern "C" bool BezierTraveler_get_isPlaying_m1936 (BezierTraveler_t342 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___progress_3);
		BezierU5BU5D_t377* L_1 = (__this->___beziers_1);
		NullCheck(L_1);
		return ((((float)L_0) < ((float)((float)((float)(1.0f)*(float)(((float)((float)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))))))))? 1 : 0);
	}
}
// System.Void BezierTraveler::Update()
extern "C" void BezierTraveler_Update_m1937 (BezierTraveler_t342 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		bool L_0 = (__this->___teste_4);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		__this->___teste_4 = 0;
		__this->___progress_3 = (0.0f);
	}

IL_001d:
	{
		bool L_1 = BezierTraveler_get_isPlaying_m1936(__this, /*hidden argument*/NULL);
		__this->____isPlaying_5 = L_1;
		float L_2 = (__this->___progress_3);
		BezierU5BU5D_t377* L_3 = (__this->___beziers_1);
		NullCheck(L_3);
		if ((!(((float)L_2) < ((float)((float)((float)(1.0f)*(float)(((float)((float)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))))))))))))
		{
			goto IL_00f0;
		}
	}
	{
		Transform_t25 * L_4 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		BezierU5BU5D_t377* L_5 = (__this->___beziers_1);
		float L_6 = (__this->___progress_3);
		int32_t L_7 = Mathf_FloorToInt_m287(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_7);
		int32_t L_8 = L_7;
		float L_9 = (__this->___progress_3);
		float L_10 = (__this->___progress_3);
		float L_11 = floorf(L_10);
		NullCheck((*(Bezier_t376 **)(Bezier_t376 **)SZArrayLdElema(L_5, L_8, sizeof(Bezier_t376 *))));
		Vector3_t6  L_12 = Bezier_GetBezierPosition_m1932((*(Bezier_t376 **)(Bezier_t376 **)SZArrayLdElema(L_5, L_8, sizeof(Bezier_t376 *))), ((float)((float)L_9-(float)L_11)), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m340(L_4, L_12, /*hidden argument*/NULL);
		float L_13 = (__this->___progress_3);
		BezierU5BU5D_t377* L_14 = (__this->___beziers_1);
		NullCheck(L_14);
		float L_15 = Mathf_Clamp_m375(NULL /*static, unused*/, ((float)((float)L_13+(float)(0.1f))), (0.0f), ((float)((float)(1.0f)*(float)(((float)((float)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length))))))))), /*hidden argument*/NULL);
		V_0 = L_15;
		float L_16 = V_0;
		BezierU5BU5D_t377* L_17 = (__this->___beziers_1);
		NullCheck(L_17);
		if ((!(((float)L_16) < ((float)((float)((float)(1.0f)*(float)(((float)((float)(((int32_t)((int32_t)(((Array_t *)L_17)->max_length)))))))))))))
		{
			goto IL_00d7;
		}
	}
	{
		Transform_t25 * L_18 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		BezierU5BU5D_t377* L_19 = (__this->___beziers_1);
		float L_20 = V_0;
		int32_t L_21 = Mathf_FloorToInt_m287(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_21);
		int32_t L_22 = L_21;
		float L_23 = V_0;
		float L_24 = V_0;
		float L_25 = floorf(L_24);
		NullCheck((*(Bezier_t376 **)(Bezier_t376 **)SZArrayLdElema(L_19, L_22, sizeof(Bezier_t376 *))));
		Vector3_t6  L_26 = Bezier_GetBezierPosition_m1932((*(Bezier_t376 **)(Bezier_t376 **)SZArrayLdElema(L_19, L_22, sizeof(Bezier_t376 *))), ((float)((float)L_23-(float)L_25)), /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_LookAt_m2614(L_18, L_26, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		float L_27 = (__this->___progress_3);
		float L_28 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_29 = (__this->___speed_2);
		__this->___progress_3 = ((float)((float)L_27+(float)((float)((float)L_28*(float)L_29))));
	}

IL_00f0:
	{
		return;
	}
}
// System.Void BezierTraveler::Play()
extern "C" void BezierTraveler_Play_m1938 (BezierTraveler_t342 * __this, const MethodInfo* method)
{
	{
		__this->___progress_3 = (0.0f);
		return;
	}
}
// System.Void BoltAnimation::.ctor()
extern "C" void BoltAnimation__ctor_m1939 (BoltAnimation_t378 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BoltAnimation::Update()
extern "C" void BoltAnimation_Update_m1940 (BoltAnimation_t378 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___play_3);
		bool L_1 = (__this->____play_4);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0027;
		}
	}
	{
		bool L_2 = (__this->___play_3);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		float L_3 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->____time_2 = L_3;
	}

IL_0027:
	{
		bool L_4 = (__this->___play_3);
		__this->____play_4 = L_4;
		bool L_5 = (__this->___play_3);
		if (!L_5)
		{
			goto IL_00a2;
		}
	}
	{
		float L_6 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = (__this->____time_2);
		if ((!(((float)L_6) >= ((float)L_7))))
		{
			goto IL_00a2;
		}
	}
	{
		float L_8 = (__this->____time_2);
		float L_9 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = (__this->____time_2);
		float L_11 = (__this->___delay_1);
		__this->____time_2 = ((float)((float)L_8+(float)((float)((float)((float)((float)L_9-(float)L_10))+(float)L_11))));
		Transform_t25 * L_12 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		float L_13 = Random_Range_m384(NULL /*static, unused*/, (0.0f), (360.0f), /*hidden argument*/NULL);
		Vector3_t6  L_14 = {0};
		Vector3__ctor_m335(&L_14, L_13, (270.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_eulerAngles_m333(L_12, L_14, /*hidden argument*/NULL);
		ParticleSystem_t332 * L_15 = (__this->___particle_5);
		NullCheck(L_15);
		ParticleSystem_Play_m2604(L_15, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void BrilhoAnimation::.ctor()
extern "C" void BrilhoAnimation__ctor_m1941 (BrilhoAnimation_t344 * __this, const MethodInfo* method)
{
	{
		__this->___fade_2 = (1.0f);
		__this->___delay_3 = (1.0f);
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BrilhoAnimation::Start()
extern const MethodInfo* Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral789;
extern "C" void BrilhoAnimation_Start_m1942 (BrilhoAnimation_t344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484047);
		_stringLiteral789 = il2cpp_codegen_string_literal_from_index(789);
		s_Il2CppMethodIntialized = true;
	}
	Color_t5  V_0 = {0};
	{
		Renderer_t46 * L_0 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_0);
		Material_t45 * L_1 = Renderer_get_material_m315(L_0, /*hidden argument*/NULL);
		__this->___mat_1 = L_1;
		Material_t45 * L_2 = (__this->___mat_1);
		NullCheck(L_2);
		Color_t5  L_3 = Material_GetColor_m351(L_2, _stringLiteral789, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = ((&V_0)->___a_3);
		__this->___maxAlpha_5 = L_4;
		BrilhoAnimation_SetColorByAlpha_m1945(__this, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void BrilhoAnimation::Update()
extern "C" void BrilhoAnimation_Update_m1943 (BrilhoAnimation_t344 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___activated_4);
		bool L_1 = (__this->____activated_6);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_003e;
		}
	}
	{
		bool L_2 = (__this->___activated_4);
		__this->____activated_6 = L_2;
		BrilhoAnimation_SetColorByAlpha_m1945(__this, (0.0f), /*hidden argument*/NULL);
		bool L_3 = (__this->___activated_4);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		float L_4 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___startedTime_7 = L_4;
	}

IL_003e:
	{
		bool L_5 = (__this->____activated_6);
		if (!L_5)
		{
			goto IL_0118;
		}
	}
	{
		float L_6 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = (__this->___startedTime_7);
		float L_8 = (__this->___fade_2);
		if ((!(((float)((float)((float)L_6-(float)L_7))) < ((float)L_8))))
		{
			goto IL_0085;
		}
	}
	{
		float L_9 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = (__this->___startedTime_7);
		float L_11 = (__this->___fade_2);
		float L_12 = (__this->___maxAlpha_5);
		BrilhoAnimation_SetColorByAlpha_m1945(__this, ((float)((float)((float)((float)((float)((float)L_9-(float)L_10))/(float)L_11))*(float)L_12)), /*hidden argument*/NULL);
		goto IL_0118;
	}

IL_0085:
	{
		float L_13 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = (__this->___startedTime_7);
		float L_15 = (__this->___fade_2);
		float L_16 = (__this->___delay_3);
		if ((!(((float)((float)((float)L_13-(float)L_14))) < ((float)((float)((float)L_15+(float)L_16))))))
		{
			goto IL_00b4;
		}
	}
	{
		float L_17 = (__this->___maxAlpha_5);
		BrilhoAnimation_SetColorByAlpha_m1945(__this, L_17, /*hidden argument*/NULL);
		goto IL_0118;
	}

IL_00b4:
	{
		float L_18 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = (__this->___startedTime_7);
		float L_20 = (__this->___fade_2);
		float L_21 = (__this->___delay_3);
		if ((!(((float)((float)((float)L_18-(float)L_19))) < ((float)((float)((float)((float)((float)L_20*(float)(2.0f)))+(float)L_21))))))
		{
			goto IL_0111;
		}
	}
	{
		float L_22 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = (__this->___startedTime_7);
		float L_24 = (__this->___delay_3);
		float L_25 = (__this->___fade_2);
		float L_26 = (__this->___fade_2);
		float L_27 = (__this->___maxAlpha_5);
		BrilhoAnimation_SetColorByAlpha_m1945(__this, ((float)((float)((float)((float)(1.0f)-(float)((float)((float)((float)((float)((float)((float)((float)((float)L_22-(float)L_23))-(float)L_24))-(float)L_25))/(float)L_26))))*(float)L_27)), /*hidden argument*/NULL);
		goto IL_0118;
	}

IL_0111:
	{
		__this->___activated_4 = 0;
	}

IL_0118:
	{
		return;
	}
}
// System.Void BrilhoAnimation::Activate()
extern "C" void BrilhoAnimation_Activate_m1944 (BrilhoAnimation_t344 * __this, const MethodInfo* method)
{
	{
		__this->___activated_4 = 1;
		return;
	}
}
// System.Void BrilhoAnimation::SetColorByAlpha(System.Single)
extern Il2CppCodeGenString* _stringLiteral789;
extern "C" void BrilhoAnimation_SetColorByAlpha_m1945 (BrilhoAnimation_t344 * __this, float ___alpha, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral789 = il2cpp_codegen_string_literal_from_index(789);
		s_Il2CppMethodIntialized = true;
	}
	Color_t5  V_0 = {0};
	Color_t5  V_1 = {0};
	Color_t5  V_2 = {0};
	{
		Material_t45 * L_0 = (__this->___mat_1);
		Material_t45 * L_1 = (__this->___mat_1);
		NullCheck(L_1);
		Color_t5  L_2 = Material_GetColor_m351(L_1, _stringLiteral789, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((&V_0)->___r_0);
		Material_t45 * L_4 = (__this->___mat_1);
		NullCheck(L_4);
		Color_t5  L_5 = Material_GetColor_m351(L_4, _stringLiteral789, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = ((&V_1)->___g_1);
		Material_t45 * L_7 = (__this->___mat_1);
		NullCheck(L_7);
		Color_t5  L_8 = Material_GetColor_m351(L_7, _stringLiteral789, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = ((&V_2)->___b_2);
		float L_10 = ___alpha;
		Color_t5  L_11 = {0};
		Color__ctor_m405(&L_11, L_3, L_6, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetColor_m374(L_0, _stringLiteral789, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBServices/<Login>c__AnonStorey1B::.ctor()
extern "C" void U3CLoginU3Ec__AnonStorey1B__ctor_m1946 (U3CLoginU3Ec__AnonStorey1B_t379 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBServices/<Login>c__AnonStorey1B::<>m__3B(Facebook.Unity.ILoginResult)
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m2668_MethodInfo_var;
extern "C" void U3CLoginU3Ec__AnonStorey1B_U3CU3Em__3B_m1947 (U3CLoginU3Ec__AnonStorey1B_t379 * __this, Object_t * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		Action_2_Invoke_m2668_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484103);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t380 * L_0 = (__this->___callback_0);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		bool L_1 = FB_get_IsLoggedIn_m1307(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t * L_2 = ___result;
		NullCheck(L_0);
		Action_2_Invoke_m2668(L_0, L_1, L_2, /*hidden argument*/Action_2_Invoke_m2668_MethodInfo_var);
		return;
	}
}
// System.Void FBServices::.ctor()
extern "C" void FBServices__ctor_m1948 (FBServices_t381 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// FBServices FBServices::get_Current()
extern TypeInfo* FBServices_t381_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t27_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisFBServices_t381_m2669_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral790;
extern "C" FBServices_t381 * FBServices_get_Current_m1949 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FBServices_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		GameObject_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		GameObject_AddComponent_TisFBServices_t381_m2669_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484104);
		_stringLiteral790 = il2cpp_codegen_string_literal_from_index(790);
		s_Il2CppMethodIntialized = true;
	}
	{
		FBServices_t381 * L_0 = ((FBServices_t381_StaticFields*)FBServices_t381_il2cpp_TypeInfo_var->static_fields)->___current_1;
		bool L_1 = Object_op_Implicit_m301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t27 * L_2 = (GameObject_t27 *)il2cpp_codegen_object_new (GameObject_t27_il2cpp_TypeInfo_var);
		GameObject__ctor_m403(L_2, _stringLiteral790, /*hidden argument*/NULL);
		NullCheck(L_2);
		FBServices_t381 * L_3 = GameObject_AddComponent_TisFBServices_t381_m2669(L_2, /*hidden argument*/GameObject_AddComponent_TisFBServices_t381_m2669_MethodInfo_var);
		((FBServices_t381_StaticFields*)FBServices_t381_il2cpp_TypeInfo_var->static_fields)->___current_1 = L_3;
	}

IL_0023:
	{
		FBServices_t381 * L_4 = ((FBServices_t381_StaticFields*)FBServices_t381_il2cpp_TypeInfo_var->static_fields)->___current_1;
		return L_4;
	}
}
// System.Void FBServices::Instantiate()
extern TypeInfo* FBServices_t381_il2cpp_TypeInfo_var;
extern "C" void FBServices_Instantiate_m1950 (FBServices_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FBServices_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	{
		FBServices_t381 * L_0 = ((FBServices_t381_StaticFields*)FBServices_t381_il2cpp_TypeInfo_var->static_fields)->___current_1;
		bool L_1 = Object_op_Equality_m427(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		((FBServices_t381_StaticFields*)FBServices_t381_il2cpp_TypeInfo_var->static_fields)->___current_1 = __this;
		goto IL_0021;
	}

IL_001b:
	{
		Object_Destroy_m401(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void FBServices::Awake()
extern TypeInfo* List_1_t78_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2160_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral791;
extern Il2CppCodeGenString* _stringLiteral792;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void FBServices_Awake_m1951 (FBServices_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		List_1__ctor_m2160_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483726);
		_stringLiteral791 = il2cpp_codegen_string_literal_from_index(791);
		_stringLiteral792 = il2cpp_codegen_string_literal_from_index(792);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t78 * V_0 = {0};
	{
		FBServices_Instantiate_m1950(__this, /*hidden argument*/NULL);
		List_1_t78 * L_0 = (List_1_t78 *)il2cpp_codegen_object_new (List_1_t78_il2cpp_TypeInfo_var);
		List_1__ctor_m2160(L_0, /*hidden argument*/List_1__ctor_m2160_MethodInfo_var);
		V_0 = L_0;
		List_1_t78 * L_1 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_1, _stringLiteral791);
		List_1_t78 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_2, _stringLiteral792);
		List_1_t78 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_3, _stringLiteral793);
		List_1_t78 * L_4 = V_0;
		__this->___requested_permissions_2 = L_4;
		return;
	}
}
// System.Void FBServices::Init()
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern TypeInfo* InitDelegate_t241_il2cpp_TypeInfo_var;
extern const MethodInfo* FBServices_Logout_m1956_MethodInfo_var;
extern "C" void FBServices_Init_m1952 (FBServices_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		InitDelegate_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(380);
		FBServices_Logout_m1956_MethodInfo_var = il2cpp_codegen_method_info_from_index(457);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsInitialized_m1308(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_1 = { (void*)FBServices_Logout_m1956_MethodInfo_var };
		InitDelegate_t241 * L_2 = (InitDelegate_t241 *)il2cpp_codegen_object_new (InitDelegate_t241_il2cpp_TypeInfo_var);
		InitDelegate__ctor_m2141(L_2, __this, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		FB_Init_m1314(NULL /*static, unused*/, L_2, (HideUnityDelegate_t242 *)NULL, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void FBServices::OnHideUnity()
extern "C" void FBServices_OnHideUnity_m1953 (FBServices_t381 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FBServices::Login(System.Action`2<System.Boolean,Facebook.Unity.ILoginResult>)
extern TypeInfo* U3CLoginU3Ec__AnonStorey1B_t379_il2cpp_TypeInfo_var;
extern TypeInfo* FacebookDelegate_1_t465_il2cpp_TypeInfo_var;
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLoginU3Ec__AnonStorey1B_U3CU3Em__3B_m1947_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2670_MethodInfo_var;
extern "C" void FBServices_Login_m1954 (FBServices_t381 * __this, Action_2_t380 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CLoginU3Ec__AnonStorey1B_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		FacebookDelegate_1_t465_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(382);
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		U3CLoginU3Ec__AnonStorey1B_U3CU3Em__3B_m1947_MethodInfo_var = il2cpp_codegen_method_info_from_index(458);
		FacebookDelegate_1__ctor_m2670_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484107);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoginU3Ec__AnonStorey1B_t379 * V_0 = {0};
	{
		U3CLoginU3Ec__AnonStorey1B_t379 * L_0 = (U3CLoginU3Ec__AnonStorey1B_t379 *)il2cpp_codegen_object_new (U3CLoginU3Ec__AnonStorey1B_t379_il2cpp_TypeInfo_var);
		U3CLoginU3Ec__AnonStorey1B__ctor_m1946(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoginU3Ec__AnonStorey1B_t379 * L_1 = V_0;
		Action_2_t380 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		List_1_t78 * L_3 = (__this->___requested_permissions_2);
		U3CLoginU3Ec__AnonStorey1B_t379 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)U3CLoginU3Ec__AnonStorey1B_U3CU3Em__3B_m1947_MethodInfo_var };
		FacebookDelegate_1_t465 * L_6 = (FacebookDelegate_1_t465 *)il2cpp_codegen_object_new (FacebookDelegate_1_t465_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2670(L_6, L_4, L_5, /*hidden argument*/FacebookDelegate_1__ctor_m2670_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		FB_LogInWithPublishPermissions_m1316(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBServices::LoginCallback(Facebook.Unity.ILoginResult)
extern "C" void FBServices_LoginCallback_m1955 (FBServices_t381 * __this, Object_t * ___result, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FBServices::Logout()
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern "C" void FBServices_Logout_m1956 (FBServices_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		FB_LogOut_m1318(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArUiController_t374 * L_0 = ArUiController_get_Instance_m1909(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArUiController_Share_OnClick_m1922(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBServices::API(System.Byte[],Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern TypeInfo* WWWForm_t293_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FB_t240_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral794;
extern Il2CppCodeGenString* _stringLiteral795;
extern Il2CppCodeGenString* _stringLiteral526;
extern Il2CppCodeGenString* _stringLiteral796;
extern "C" void FBServices_API_m1957 (FBServices_t381 * __this, ByteU5BU5D_t119* ___image, FacebookDelegate_1_t294 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t293_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(251);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		FB_t240_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		_stringLiteral794 = il2cpp_codegen_string_literal_from_index(794);
		_stringLiteral795 = il2cpp_codegen_string_literal_from_index(795);
		_stringLiteral526 = il2cpp_codegen_string_literal_from_index(526);
		_stringLiteral796 = il2cpp_codegen_string_literal_from_index(796);
		s_Il2CppMethodIntialized = true;
	}
	WWWForm_t293 * V_0 = {0};
	{
		WWWForm_t293 * L_0 = (WWWForm_t293 *)il2cpp_codegen_object_new (WWWForm_t293_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2406(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		WWWForm_t293 * L_1 = V_0;
		ByteU5BU5D_t119* L_2 = ___image;
		NullCheck(L_1);
		WWWForm_AddBinaryData_m2671(L_1, _stringLiteral794, L_2, _stringLiteral795, /*hidden argument*/NULL);
		WWWForm_t293 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		WWWForm_AddField_m2407(L_3, _stringLiteral526, L_4, /*hidden argument*/NULL);
		FacebookDelegate_1_t294 * L_5 = ___callback;
		WWWForm_t293 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FB_t240_il2cpp_TypeInfo_var);
		FB_API_m1325(NULL /*static, unused*/, _stringLiteral796, 1, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FBServices::OnGUI()
extern "C" void FBServices_OnGUI_m1958 (FBServices_t381 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LerpFollow::.ctor()
extern "C" void LerpFollow__ctor_m1959 (LerpFollow_t382 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LerpFollow::Start()
extern "C" void LerpFollow_Start_m1960 (LerpFollow_t382 * __this, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_parent_m2395(L_0, (Transform_t25 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LerpFollow::Update()
extern "C" void LerpFollow_Update_m1961 (LerpFollow_t382 * __this, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Transform_t25 * L_1 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t6  L_2 = Transform_get_position_m334(L_1, /*hidden argument*/NULL);
		Transform_t25 * L_3 = (__this->___target_1);
		NullCheck(L_3);
		Vector3_t6  L_4 = Transform_get_position_m334(L_3, /*hidden argument*/NULL);
		float L_5 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = (__this->___movementSpeed_2);
		Vector3_t6  L_7 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_2, L_4, ((float)((float)L_5*(float)L_6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m340(L_0, L_7, /*hidden argument*/NULL);
		Transform_t25 * L_8 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Transform_t25 * L_9 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t51  L_10 = Transform_get_rotation_m2607(L_9, /*hidden argument*/NULL);
		Transform_t25 * L_11 = (__this->___target_1);
		NullCheck(L_11);
		Quaternion_t51  L_12 = Transform_get_rotation_m2607(L_11, /*hidden argument*/NULL);
		float L_13 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = (__this->___rotationSpeed_3);
		Quaternion_t51  L_15 = Quaternion_Lerp_m2672(NULL /*static, unused*/, L_10, L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_rotation_m381(L_8, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadingRotation::.ctor()
extern "C" void LoadingRotation__ctor_m1962 (LoadingRotation_t383 * __this, const MethodInfo* method)
{
	{
		__this->___speed_2 = (1.0f);
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadingRotation::Update()
extern "C" void LoadingRotation_Update_m1963 (LoadingRotation_t383 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___realRotation_3);
		float L_1 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = (__this->___speed_2);
		float L_3 = (__this->___rotationStep_1);
		__this->___realRotation_3 = ((float)((float)L_0+(float)((float)((float)((float)((float)L_1*(float)L_2))*(float)L_3))));
		float L_4 = (__this->___realRotation_3);
		if ((!(((float)L_4) > ((float)(360.0f)))))
		{
			goto IL_0042;
		}
	}
	{
		float L_5 = (__this->___realRotation_3);
		__this->___realRotation_3 = ((float)((float)L_5-(float)(360.0f)));
	}

IL_0042:
	{
		Transform_t25 * L_6 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		float L_7 = (__this->___realRotation_3);
		float L_8 = (__this->___rotationStep_1);
		float L_9 = (__this->___rotationStep_1);
		Vector3_t6  L_10 = {0};
		Vector3__ctor_m335(&L_10, (0.0f), (0.0f), ((float)((float)(360.0f)-(float)((float)((float)(((float)((float)(((int32_t)((int32_t)((float)((float)L_7/(float)L_8))))))))*(float)L_9)))), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_eulerAngles_m333(L_6, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Popup::.ctor()
extern "C" void Popup__ctor_m1964 (Popup_t384 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Popup::Start()
extern "C" void Popup_Start_m1965 (Popup_t384 * __this, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t6  L_1 = Transform_get_localPosition_m338(L_0, /*hidden argument*/NULL);
		__this->___initialPosition_2 = L_1;
		Transform_t25 * L_2 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Vector3_t6  L_3 = (__this->___hiddenPosition_1);
		NullCheck(L_2);
		Transform_set_localPosition_m339(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Popup::Update()
extern "C" void Popup_Update_m1966 (Popup_t384 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___activate_6);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		__this->___activate_6 = 0;
		float L_1 = (__this->___delay_4);
		Popup_Activate_m1967(__this, L_1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Popup::Activate(System.Single)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern TypeInfo* EaseType_t1_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t41_il2cpp_TypeInfo_var;
extern TypeInfo* iTween_t15_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral797;
extern Il2CppCodeGenString* _stringLiteral37;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral17;
extern Il2CppCodeGenString* _stringLiteral45;
extern "C" void Popup_Activate_m1967 (Popup_t384 * __this, float ___deactivate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		EaseType_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Boolean_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		iTween_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral797 = il2cpp_codegen_string_literal_from_index(797);
		_stringLiteral37 = il2cpp_codegen_string_literal_from_index(37);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		_stringLiteral45 = il2cpp_codegen_string_literal_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___activated_5);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_1 = ___deactivate;
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_002a;
		}
	}
	{
		float L_2 = ___deactivate;
		float L_3 = (__this->___time_3);
		MonoBehaviour_Invoke_m2231(__this, _stringLiteral797, ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
	}

IL_002a:
	{
		__this->___activated_5 = 1;
		GameObject_t27 * L_4 = Component_get_gameObject_m306(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t34* L_5 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 8));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, _stringLiteral37);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral37;
		ObjectU5BU5D_t34* L_6 = L_5;
		Vector3_t6 * L_7 = &(__this->___initialPosition_2);
		float L_8 = (L_7->___y_2);
		float L_9 = L_8;
		Object_t * L_10 = Box(Single_t36_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t34* L_11 = L_6;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, _stringLiteral2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2;
		ObjectU5BU5D_t34* L_12 = L_11;
		float L_13 = (__this->___time_3);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t36_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t34* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, _stringLiteral17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral17;
		ObjectU5BU5D_t34* L_17 = L_16;
		int32_t L_18 = ((int32_t)27);
		Object_t * L_19 = Box(EaseType_t1_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 5);
		ArrayElementTypeCheck (L_17, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 5, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t34* L_20 = L_17;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 6);
		ArrayElementTypeCheck (L_20, _stringLiteral45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral45;
		ObjectU5BU5D_t34* L_21 = L_20;
		bool L_22 = 1;
		Object_t * L_23 = Box(Boolean_t41_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 7);
		ArrayElementTypeCheck (L_21, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 7, sizeof(Object_t *))) = (Object_t *)L_23;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t15_il2cpp_TypeInfo_var);
		Hashtable_t19 * L_24 = iTween_Hash_m225(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		iTween_MoveTo_m56(NULL /*static, unused*/, L_4, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Popup::Deactivate()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern TypeInfo* EaseType_t1_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t41_il2cpp_TypeInfo_var;
extern TypeInfo* iTween_t15_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral37;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral17;
extern Il2CppCodeGenString* _stringLiteral45;
extern Il2CppCodeGenString* _stringLiteral798;
extern "C" void Popup_Deactivate_m1968 (Popup_t384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		EaseType_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Boolean_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		iTween_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral37 = il2cpp_codegen_string_literal_from_index(37);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		_stringLiteral45 = il2cpp_codegen_string_literal_from_index(45);
		_stringLiteral798 = il2cpp_codegen_string_literal_from_index(798);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___activated_5);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		GameObject_t27 * L_1 = Component_get_gameObject_m306(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t34* L_2 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 8));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, _stringLiteral37);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral37;
		ObjectU5BU5D_t34* L_3 = L_2;
		Vector3_t6 * L_4 = &(__this->___hiddenPosition_1);
		float L_5 = (L_4->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t36_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, _stringLiteral2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2;
		ObjectU5BU5D_t34* L_9 = L_8;
		float L_10 = (__this->___time_3);
		float L_11 = L_10;
		Object_t * L_12 = Box(Single_t36_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 3, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t34* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral17;
		ObjectU5BU5D_t34* L_14 = L_13;
		int32_t L_15 = ((int32_t)26);
		Object_t * L_16 = Box(EaseType_t1_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 5);
		ArrayElementTypeCheck (L_14, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 5, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t34* L_17 = L_14;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 6);
		ArrayElementTypeCheck (L_17, _stringLiteral45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral45;
		ObjectU5BU5D_t34* L_18 = L_17;
		bool L_19 = 1;
		Object_t * L_20 = Box(Boolean_t41_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 7);
		ArrayElementTypeCheck (L_18, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 7, sizeof(Object_t *))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t15_il2cpp_TypeInfo_var);
		Hashtable_t19 * L_21 = iTween_Hash_m225(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		iTween_MoveTo_m56(NULL /*static, unused*/, L_1, L_21, /*hidden argument*/NULL);
		float L_22 = (__this->___time_3);
		MonoBehaviour_Invoke_m2231(__this, _stringLiteral798, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Popup::SetActivetFalse()
extern "C" void Popup_SetActivetFalse_m1969 (Popup_t384 * __this, const MethodInfo* method)
{
	{
		__this->___activated_5 = 0;
		return;
	}
}
// System.Void RotateScale::.ctor()
extern "C" void RotateScale__ctor_m1970 (RotateScale_t385 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RotateScale::Awake()
extern "C" void RotateScale_Awake_m1971 (RotateScale_t385 * __this, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Transform_t25 * L_0 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t6  L_1 = Transform_get_localEulerAngles_m342(L_0, /*hidden argument*/NULL);
		__this->____rotation_3 = L_1;
		Transform_t25 * L_2 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t6  L_3 = Transform_get_localScale_m336(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = ((&V_0)->___x_1);
		__this->____scale_4 = L_4;
		return;
	}
}
// System.Void RotateScale::Update()
extern "C" void RotateScale_Update_m1972 (RotateScale_t385 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isPlaying_5);
		if (!L_0)
		{
			goto IL_0056;
		}
	}
	{
		Transform_t25 * L_1 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Vector3_t6  L_2 = (__this->___rotation_1);
		float L_3 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6  L_4 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_m2578(L_1, L_4, /*hidden argument*/NULL);
		Transform_t25 * L_5 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Transform_t25 * L_6 = L_5;
		NullCheck(L_6);
		Vector3_t6  L_7 = Transform_get_localScale_m336(L_6, /*hidden argument*/NULL);
		Vector3_t6  L_8 = Vector3_get_one_m2644(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = (__this->___scale_2);
		Vector3_t6  L_10 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		float L_11 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6  L_12 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Vector3_t6  L_13 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_7, L_12, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localScale_m341(L_6, L_13, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void RotateScale::Play()
extern "C" void RotateScale_Play_m1973 (RotateScale_t385 * __this, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = (__this->____rotation_3);
		NullCheck(L_0);
		Transform_set_localEulerAngles_m343(L_0, L_1, /*hidden argument*/NULL);
		Transform_t25 * L_2 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Vector3_t6  L_3 = Vector3_get_one_m2644(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = (__this->____scale_4);
		Vector3_t6  L_5 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localScale_m341(L_2, L_5, /*hidden argument*/NULL);
		__this->___isPlaying_5 = 1;
		return;
	}
}
// System.Void RotateScale::Stop()
extern "C" void RotateScale_Stop_m1974 (RotateScale_t385 * __this, const MethodInfo* method)
{
	{
		__this->___isPlaying_5 = 0;
		Transform_t25 * L_0 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = (__this->____rotation_3);
		NullCheck(L_0);
		Transform_set_localEulerAngles_m343(L_0, L_1, /*hidden argument*/NULL);
		Transform_t25 * L_2 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Vector3_t6  L_3 = Vector3_get_one_m2644(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = (__this->____scale_4);
		Vector3_t6  L_5 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localScale_m341(L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowHideSprite::.ctor()
extern "C" void ShowHideSprite__ctor_m1975 (ShowHideSprite_t386 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowHideSprite::Show()
extern "C" void ShowHideSprite_Show_m1976 (ShowHideSprite_t386 * __this, const MethodInfo* method)
{
	{
		__this->___showing_1 = 1;
		return;
	}
}
// System.Void ShowHideSprite::Hide()
extern "C" void ShowHideSprite_Hide_m1977 (ShowHideSprite_t386 * __this, const MethodInfo* method)
{
	{
		__this->___showing_1 = 0;
		return;
	}
}
// System.Void ShowHideSprite::Start()
extern const MethodInfo* Component_GetComponent_TisImage_t387_m2673_MethodInfo_var;
extern "C" void ShowHideSprite_Start_m1978 (ShowHideSprite_t386 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisImage_t387_m2673_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484108);
		s_Il2CppMethodIntialized = true;
	}
	{
		Image_t387 * L_0 = Component_GetComponent_TisImage_t387_m2673(__this, /*hidden argument*/Component_GetComponent_TisImage_t387_m2673_MethodInfo_var);
		__this->___image_5 = L_0;
		Image_t387 * L_1 = (__this->___image_5);
		NullCheck(L_1);
		Color_t5  L_2 = Graphic_get_color_m2674(L_1, /*hidden argument*/NULL);
		__this->___color_3 = L_2;
		Color_t5 * L_3 = &(__this->___color_3);
		float L_4 = (L_3->___r_0);
		Color_t5 * L_5 = &(__this->___color_3);
		float L_6 = (L_5->___g_1);
		Color_t5 * L_7 = &(__this->___color_3);
		float L_8 = (L_7->___b_2);
		Color_t5  L_9 = {0};
		Color__ctor_m405(&L_9, L_4, L_6, L_8, (0.0f), /*hidden argument*/NULL);
		__this->___transparent_4 = L_9;
		return;
	}
}
// System.Void ShowHideSprite::Update()
extern "C" void ShowHideSprite_Update_m1979 (ShowHideSprite_t386 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___showing_1);
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		Image_t387 * L_1 = (__this->___image_5);
		NullCheck(L_1);
		Color_t5  L_2 = Graphic_get_color_m2674(L_1, /*hidden argument*/NULL);
		Color_t5  L_3 = (__this->___color_3);
		bool L_4 = Color_op_Inequality_m2675(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		Image_t387 * L_5 = (__this->___image_5);
		Image_t387 * L_6 = (__this->___image_5);
		NullCheck(L_6);
		Color_t5  L_7 = Graphic_get_color_m2674(L_6, /*hidden argument*/NULL);
		Color_t5  L_8 = (__this->___color_3);
		float L_9 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = (__this->___showSpeed_2);
		Color_t5  L_11 = Color_Lerp_m2611(NULL /*static, unused*/, L_7, L_8, ((float)((float)L_9*(float)L_10)), /*hidden argument*/NULL);
		NullCheck(L_5);
		Graphic_set_color_m2676(L_5, L_11, /*hidden argument*/NULL);
		goto IL_00ab;
	}

IL_0058:
	{
		bool L_12 = (__this->___showing_1);
		if (L_12)
		{
			goto IL_00ab;
		}
	}
	{
		Image_t387 * L_13 = (__this->___image_5);
		NullCheck(L_13);
		Color_t5  L_14 = Graphic_get_color_m2674(L_13, /*hidden argument*/NULL);
		Color_t5  L_15 = (__this->___transparent_4);
		bool L_16 = Color_op_Inequality_m2675(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ab;
		}
	}
	{
		Image_t387 * L_17 = (__this->___image_5);
		Image_t387 * L_18 = (__this->___image_5);
		NullCheck(L_18);
		Color_t5  L_19 = Graphic_get_color_m2674(L_18, /*hidden argument*/NULL);
		Color_t5  L_20 = (__this->___transparent_4);
		float L_21 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = (__this->___showSpeed_2);
		Color_t5  L_23 = Color_Lerp_m2611(NULL /*static, unused*/, L_19, L_20, ((float)((float)L_21*(float)L_22)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Graphic_set_color_m2676(L_17, L_23, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		return;
	}
}
// System.Void SinoidalAnimation::.ctor()
extern "C" void SinoidalAnimation__ctor_m1980 (SinoidalAnimation_t388 * __this, const MethodInfo* method)
{
	{
		__this->___speed_2 = (1.0f);
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SinoidalAnimation::Start()
extern "C" void SinoidalAnimation_Start_m1981 (SinoidalAnimation_t388 * __this, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t6  L_1 = Transform_get_localPosition_m338(L_0, /*hidden argument*/NULL);
		__this->___initialPosition_3 = L_1;
		return;
	}
}
// System.Void SinoidalAnimation::Update()
extern "C" void SinoidalAnimation_Update_m1982 (SinoidalAnimation_t388 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = (__this->___speed_2);
		float L_2 = sinf(((float)((float)L_0*(float)L_1)));
		float L_3 = (__this->___range_1);
		V_0 = ((float)((float)L_2*(float)L_3));
		Transform_t25 * L_4 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		Vector3_t6  L_5 = (__this->___initialPosition_3);
		NullCheck(L_4);
		Transform_set_localPosition_m339(L_4, L_5, /*hidden argument*/NULL);
		Transform_t25 * L_6 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		float L_7 = V_0;
		NullCheck(L_6);
		Transform_Translate_m2677(L_6, (0.0f), L_7, (0.0f), 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUIController/<StartTransiction>c__Iterator18::.ctor()
extern "C" void U3CStartTransictionU3Ec__Iterator18__ctor_m1983 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object StartUIController/<StartTransiction>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartTransictionU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1984 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object StartUIController/<StartTransiction>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartTransictionU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m1985 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean StartUIController/<StartTransiction>c__Iterator18::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t500_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisImage_t387_m2678_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral799;
extern "C" bool U3CStartTransictionU3Ec__Iterator18_MoveNext_m1986 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		ComponentTracker_GetElement_TisImage_t387_m2678_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484109);
		_stringLiteral799 = il2cpp_codegen_string_literal_from_index(799);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00d2;
		}
	}
	{
		goto IL_0101;
	}

IL_0021:
	{
		StartUIController_t390 * L_2 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_2);
		ComponentTracker_t67 * L_3 = (L_2->___tracker_2);
		NullCheck(L_3);
		Image_t387 * L_4 = ComponentTracker_GetElement_TisImage_t387_m2678(L_3, _stringLiteral799, /*hidden argument*/ComponentTracker_GetElement_TisImage_t387_m2678_MethodInfo_var);
		__this->___U3CblackU3E__0_0 = L_4;
		Image_t387 * L_5 = (__this->___U3CblackU3E__0_0);
		NullCheck(L_5);
		GameObject_t27 * L_6 = Component_get_gameObject_m306(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m2344(L_6, 1, /*hidden argument*/NULL);
		Image_t387 * L_7 = (__this->___U3CblackU3E__0_0);
		Color_t5  L_8 = {0};
		Color__ctor_m405(&L_8, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Graphic_set_color_m2676(L_7, L_8, /*hidden argument*/NULL);
		__this->___U3CiU3E__1_1 = (0.0f);
		goto IL_00e4;
	}

IL_0081:
	{
		StartUIController_t390 * L_9 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_9);
		ComponentTracker_t67 * L_10 = (L_9->___tracker_2);
		NullCheck(L_10);
		Image_t387 * L_11 = ComponentTracker_GetElement_TisImage_t387_m2678(L_10, _stringLiteral799, /*hidden argument*/ComponentTracker_GetElement_TisImage_t387_m2678_MethodInfo_var);
		float L_12 = (__this->___U3CiU3E__1_1);
		Color_t5  L_13 = {0};
		Color__ctor_m405(&L_13, (0.0f), (0.0f), (0.0f), ((float)((float)L_12*(float)(0.63f))), /*hidden argument*/NULL);
		NullCheck(L_11);
		Graphic_set_color_m2676(L_11, L_13, /*hidden argument*/NULL);
		WaitForEndOfFrame_t500 * L_14 = (WaitForEndOfFrame_t500 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t500_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m2246(L_14, /*hidden argument*/NULL);
		__this->___U24current_3 = L_14;
		__this->___U24PC_2 = 1;
		goto IL_0103;
	}

IL_00d2:
	{
		float L_15 = (__this->___U3CiU3E__1_1);
		float L_16 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CiU3E__1_1 = ((float)((float)L_15+(float)L_16));
	}

IL_00e4:
	{
		float L_17 = (__this->___U3CiU3E__1_1);
		if ((((float)L_17) < ((float)(1.0f))))
		{
			goto IL_0081;
		}
	}
	{
		Application_LoadLevel_m2591(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		__this->___U24PC_2 = (-1);
	}

IL_0101:
	{
		return 0;
	}

IL_0103:
	{
		return 1;
	}
	// Dead block : IL_0105: ldloc.1
}
// System.Void StartUIController/<StartTransiction>c__Iterator18::Dispose()
extern "C" void U3CStartTransictionU3Ec__Iterator18_Dispose_m1987 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void StartUIController/<StartTransiction>c__Iterator18::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CStartTransictionU3Ec__Iterator18_Reset_m1988 (U3CStartTransictionU3Ec__Iterator18_t389 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void StartUIController::.ctor()
extern "C" void StartUIController__ctor_m1989 (StartUIController_t390 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUIController::Start()
extern Il2CppCodeGenString* _stringLiteral799;
extern "C" void StartUIController_Start_m1990 (StartUIController_t390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral799 = il2cpp_codegen_string_literal_from_index(799);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tracker_2 = L_0;
		ComponentTracker_t67 * L_1 = (__this->___tracker_2);
		NullCheck(L_1);
		GameObject_t27 * L_2 = ComponentTracker_GetObject_m447(L_1, _stringLiteral799, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m2344(L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUIController::Update()
extern const MethodInfo* ComponentTracker_GetElement_TisScrollRect_t581_m2679_MethodInfo_var;
extern const MethodInfo* ComponentTracker_GetElement_TisText_t580_m2663_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral800;
extern Il2CppCodeGenString* _stringLiteral801;
extern Il2CppCodeGenString* _stringLiteral802;
extern Il2CppCodeGenString* _stringLiteral803;
extern "C" void StartUIController_Update_m1991 (StartUIController_t390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentTracker_GetElement_TisScrollRect_t581_m2679_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484110);
		ComponentTracker_GetElement_TisText_t580_m2663_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484097);
		_stringLiteral800 = il2cpp_codegen_string_literal_from_index(800);
		_stringLiteral801 = il2cpp_codegen_string_literal_from_index(801);
		_stringLiteral802 = il2cpp_codegen_string_literal_from_index(802);
		_stringLiteral803 = il2cpp_codegen_string_literal_from_index(803);
		s_Il2CppMethodIntialized = true;
	}
	ScrollRect_t581 * V_0 = {0};
	Vector3_t6  V_1 = {0};
	Vector3_t6  V_2 = {0};
	Text_t580 * G_B10_0 = {0};
	Text_t580 * G_B9_0 = {0};
	String_t* G_B11_0 = {0};
	Text_t580 * G_B11_1 = {0};
	{
		ComponentTracker_t67 * L_0 = (__this->___tracker_2);
		NullCheck(L_0);
		ScrollRect_t581 * L_1 = ComponentTracker_GetElement_TisScrollRect_t581_m2679(L_0, _stringLiteral800, /*hidden argument*/ComponentTracker_GetElement_TisScrollRect_t581_m2679_MethodInfo_var);
		V_0 = L_1;
		bool L_2 = (__this->___infoDrag_6);
		if (L_2)
		{
			goto IL_0086;
		}
	}
	{
		bool L_3 = (__this->___info_1);
		if (L_3)
		{
			goto IL_0059;
		}
	}
	{
		ScrollRect_t581 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t25 * L_5 = Component_get_transform_m416(L_4, /*hidden argument*/NULL);
		ScrollRect_t581 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t25 * L_7 = Component_get_transform_m416(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t6  L_8 = Transform_get_localPosition_m338(L_7, /*hidden argument*/NULL);
		Vector3_t6  L_9 = (__this->___infoHiddenPosition_4);
		float L_10 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = (__this->___infoSpeed_5);
		Vector3_t6  L_12 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_8, L_9, ((float)((float)L_10*(float)L_11)), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localPosition_m339(L_5, L_12, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0059:
	{
		ScrollRect_t581 * L_13 = V_0;
		NullCheck(L_13);
		Transform_t25 * L_14 = Component_get_transform_m416(L_13, /*hidden argument*/NULL);
		ScrollRect_t581 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t25 * L_16 = Component_get_transform_m416(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t6  L_17 = Transform_get_localPosition_m338(L_16, /*hidden argument*/NULL);
		Vector3_t6  L_18 = (__this->___infoShowedPosition_3);
		float L_19 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_20 = (__this->___infoSpeed_5);
		Vector3_t6  L_21 = Vector3_Lerp_m2613(NULL /*static, unused*/, L_17, L_18, ((float)((float)L_19*(float)L_20)), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localPosition_m339(L_14, L_21, /*hidden argument*/NULL);
	}

IL_0086:
	{
		ScrollRect_t581 * L_22 = V_0;
		NullCheck(L_22);
		Transform_t25 * L_23 = Component_get_transform_m416(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t6  L_24 = Transform_get_localPosition_m338(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		float L_25 = ((&V_1)->___y_2);
		Vector3_t6 * L_26 = &(__this->___infoShowedPosition_3);
		float L_27 = (L_26->___y_2);
		if ((!(((float)L_25) > ((float)L_27))))
		{
			goto IL_00ba;
		}
	}
	{
		ScrollRect_t581 * L_28 = V_0;
		NullCheck(L_28);
		Transform_t25 * L_29 = Component_get_transform_m416(L_28, /*hidden argument*/NULL);
		Vector3_t6  L_30 = (__this->___infoShowedPosition_3);
		NullCheck(L_29);
		Transform_set_localPosition_m339(L_29, L_30, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		ScrollRect_t581 * L_31 = V_0;
		NullCheck(L_31);
		Transform_t25 * L_32 = Component_get_transform_m416(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t6  L_33 = Transform_get_localPosition_m338(L_32, /*hidden argument*/NULL);
		V_2 = L_33;
		float L_34 = ((&V_2)->___y_2);
		Vector3_t6 * L_35 = &(__this->___infoHiddenPosition_4);
		float L_36 = (L_35->___y_2);
		if ((!(((float)L_34) < ((float)L_36))))
		{
			goto IL_00ee;
		}
	}
	{
		ScrollRect_t581 * L_37 = V_0;
		NullCheck(L_37);
		Transform_t25 * L_38 = Component_get_transform_m416(L_37, /*hidden argument*/NULL);
		Vector3_t6  L_39 = (__this->___infoHiddenPosition_4);
		NullCheck(L_38);
		Transform_set_localPosition_m339(L_38, L_39, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		ComponentTracker_t67 * L_40 = (__this->___tracker_2);
		NullCheck(L_40);
		Text_t580 * L_41 = ComponentTracker_GetElement_TisText_t580_m2663(L_40, _stringLiteral801, /*hidden argument*/ComponentTracker_GetElement_TisText_t580_m2663_MethodInfo_var);
		bool L_42 = (__this->___info_1);
		G_B9_0 = L_41;
		if (!L_42)
		{
			G_B10_0 = L_41;
			goto IL_0113;
		}
	}
	{
		G_B11_0 = _stringLiteral802;
		G_B11_1 = G_B9_0;
		goto IL_0118;
	}

IL_0113:
	{
		G_B11_0 = _stringLiteral803;
		G_B11_1 = G_B10_0;
	}

IL_0118:
	{
		NullCheck(G_B11_1);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B11_1, G_B11_0);
		return;
	}
}
// System.Void StartUIController::Seta_OnClick()
extern "C" void StartUIController_Seta_OnClick_m1992 (StartUIController_t390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___info_1);
		__this->___info_1 = ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		return;
	}
}
// System.Void StartUIController::InfoGroup_OnBeginDrag()
extern "C" void StartUIController_InfoGroup_OnBeginDrag_m1993 (StartUIController_t390 * __this, const MethodInfo* method)
{
	{
		__this->___infoDrag_6 = 1;
		return;
	}
}
// System.Void StartUIController::InfoGroup_OnEndGrag()
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral800;
extern "C" void StartUIController_InfoGroup_OnEndGrag_m1994 (StartUIController_t390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral800 = il2cpp_codegen_string_literal_from_index(800);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6  V_0 = {0};
	Vector3_t6  V_1 = {0};
	Vector3_t6  V_2 = {0};
	{
		__this->___infoDrag_6 = 0;
		bool L_0 = (__this->___info_1);
		if (!L_0)
		{
			goto IL_004a;
		}
	}
	{
		ComponentTracker_t67 * L_1 = (__this->___tracker_2);
		NullCheck(L_1);
		GameObject_t27 * L_2 = ComponentTracker_GetObject_m447(L_1, _stringLiteral800, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t25 * L_3 = GameObject_get_transform_m305(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t6  L_4 = Transform_get_localPosition_m338(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = ((&V_0)->___y_2);
		if ((!(((float)L_5) < ((float)(-400.0f)))))
		{
			goto IL_004a;
		}
	}
	{
		__this->___info_1 = 0;
		goto IL_00b4;
	}

IL_004a:
	{
		bool L_6 = (__this->___info_1);
		if (L_6)
		{
			goto IL_00b4;
		}
	}
	{
		ComponentTracker_t67 * L_7 = (__this->___tracker_2);
		NullCheck(L_7);
		GameObject_t27 * L_8 = ComponentTracker_GetObject_m447(L_7, _stringLiteral800, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t25 * L_9 = GameObject_get_transform_m305(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t6  L_10 = Transform_get_localPosition_m338(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = ((&V_1)->___y_2);
		if ((!(((float)L_11) > ((float)(-1500.0f)))))
		{
			goto IL_00b4;
		}
	}
	{
		ComponentTracker_t67 * L_12 = (__this->___tracker_2);
		NullCheck(L_12);
		GameObject_t27 * L_13 = ComponentTracker_GetObject_m447(L_12, _stringLiteral800, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t25 * L_14 = GameObject_get_transform_m305(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t6  L_15 = Transform_get_localPosition_m338(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = ((&V_2)->___y_2);
		float L_17 = L_16;
		Object_t * L_18 = Box(Single_t36_il2cpp_TypeInfo_var, &L_17);
		MonoBehaviour_print_m2635(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		__this->___info_1 = 1;
	}

IL_00b4:
	{
		return;
	}
}
// System.Void StartUIController::Iniciar_OnClick()
extern "C" void StartUIController_Iniciar_OnClick_m1995 (StartUIController_t390 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = StartUIController_StartTransiction_m1997(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2199(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUIController::Vimeo_OnClick()
extern Il2CppCodeGenString* _stringLiteral804;
extern Il2CppCodeGenString* _stringLiteral805;
extern "C" void StartUIController_Vimeo_OnClick_m1996 (StartUIController_t390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral804 = il2cpp_codegen_string_literal_from_index(804);
		_stringLiteral805 = il2cpp_codegen_string_literal_from_index(805);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m2635(NULL /*static, unused*/, _stringLiteral804, /*hidden argument*/NULL);
		Application_OpenURL_m2496(NULL /*static, unused*/, _stringLiteral805, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator StartUIController::StartTransiction()
extern TypeInfo* U3CStartTransictionU3Ec__Iterator18_t389_il2cpp_TypeInfo_var;
extern "C" Object_t * StartUIController_StartTransiction_m1997 (StartUIController_t390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CStartTransictionU3Ec__Iterator18_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(385);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartTransictionU3Ec__Iterator18_t389 * V_0 = {0};
	{
		U3CStartTransictionU3Ec__Iterator18_t389 * L_0 = (U3CStartTransictionU3Ec__Iterator18_t389 *)il2cpp_codegen_object_new (U3CStartTransictionU3Ec__Iterator18_t389_il2cpp_TypeInfo_var);
		U3CStartTransictionU3Ec__Iterator18__ctor_m1983(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartTransictionU3Ec__Iterator18_t389 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		U3CStartTransictionU3Ec__Iterator18_t389 * L_2 = V_0;
		return L_2;
	}
}
// System.Void StartUIController::test()
extern TypeInfo* Vector3_t6_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void StartUIController_test_m1998 (StartUIController_t390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentTracker_t67 * L_0 = (__this->___tracker_2);
		NullCheck(L_0);
		GameObject_t27 * L_1 = ComponentTracker_GetObject_m447(L_0, _stringLiteral806, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t25 * L_2 = GameObject_get_transform_m305(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t6  L_3 = Transform_get_localScale_m336(L_2, /*hidden argument*/NULL);
		Vector3_t6  L_4 = L_3;
		Object_t * L_5 = Box(Vector3_t6_il2cpp_TypeInfo_var, &L_4);
		MonoBehaviour_print_m2635(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TargetTrackingTrigger::.ctor()
extern "C" void TargetTrackingTrigger__ctor_m1999 (TargetTrackingTrigger_t391 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TargetTrackingTrigger::Update()
extern "C" void TargetTrackingTrigger_Update_m2000 (TargetTrackingTrigger_t391 * __this, const MethodInfo* method)
{
	{
		Renderer_t46 * L_0 = Component_get_renderer_m349(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Renderer_get_enabled_m2554(L_0, /*hidden argument*/NULL);
		bool L_2 = (__this->___wasEnabled_4);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0037;
		}
	}
	{
		Renderer_t46 * L_3 = Component_get_renderer_m349(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = Renderer_get_enabled_m2554(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		TargetTrackingTrigger_OnEnabled_m2001(__this, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_0031:
	{
		TargetTrackingTrigger_OnDisabled_m2002(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		Renderer_t46 * L_5 = Component_get_renderer_m349(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Renderer_get_enabled_m2554(L_5, /*hidden argument*/NULL);
		__this->___wasEnabled_4 = L_6;
		return;
	}
}
// System.Void TargetTrackingTrigger::OnEnabled()
extern "C" void TargetTrackingTrigger_OnEnabled_m2001 (TargetTrackingTrigger_t391 * __this, const MethodInfo* method)
{
	{
		GameObject_t27 * L_0 = (__this->___triggerTarget_1);
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t27 * L_2 = (__this->___triggerTarget_1);
		String_t* L_3 = (__this->___targetFoundMessage_2);
		NullCheck(L_2);
		GameObject_SendMessage_m2680(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void TargetTrackingTrigger::OnDisabled()
extern "C" void TargetTrackingTrigger_OnDisabled_m2002 (TargetTrackingTrigger_t391 * __this, const MethodInfo* method)
{
	{
		GameObject_t27 * L_0 = (__this->___triggerTarget_1);
		bool L_1 = Object_op_Inequality_m395(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t27 * L_2 = (__this->___triggerTarget_1);
		String_t* L_3 = (__this->___targetLostMessage_3);
		NullCheck(L_2);
		GameObject_SendMessage_m2680(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void Test::.ctor()
extern "C" void Test__ctor_m2003 (Test_t392 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Test::Start()
extern TypeInfo* Action_1_t490_il2cpp_TypeInfo_var;
extern const MethodInfo* Test_Callback_m2006_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2681_MethodInfo_var;
extern "C" void Test_Start_m2004 (Test_t392 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t490_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(386);
		Test_Callback_m2006_MethodInfo_var = il2cpp_codegen_method_info_from_index(463);
		Action_1__ctor_m2681_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484112);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { (void*)Test_Callback_m2006_MethodInfo_var };
		Action_1_t490 * L_1 = (Action_1_t490 *)il2cpp_codegen_object_new (Action_1_t490_il2cpp_TypeInfo_var);
		Action_1__ctor_m2681(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m2681_MethodInfo_var);
		Test_Request_m2005(__this, ((int32_t)12), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Test::Request(System.Int32,System.Action`1<System.Int32>)
extern const MethodInfo* Action_1_Invoke_m2682_MethodInfo_var;
extern "C" void Test_Request_m2005 (Test_t392 * __this, int32_t ___param, Action_1_t490 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m2682_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484113);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t490 * L_0 = ___callback;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		Action_1_t490 * L_1 = ___callback;
		int32_t L_2 = ___param;
		NullCheck(L_1);
		Action_1_Invoke_m2682(L_1, L_2, /*hidden argument*/Action_1_Invoke_m2682_MethodInfo_var);
	}

IL_000d:
	{
		return;
	}
}
// System.Void Test::Callback(System.Int32)
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral807;
extern "C" void Test_Callback_m2006 (Test_t392 * __this, int32_t ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral807 = il2cpp_codegen_string_literal_from_index(807);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___i;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2404(NULL /*static, unused*/, _stringLiteral807, L_2, /*hidden argument*/NULL);
		MonoBehaviour_print_m2635(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::.ctor()
extern "C" void U3CITakeScreenshotU3Ec__Iterator19__ctor_m2007 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CITakeScreenshotU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2008 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CITakeScreenshotU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m2009 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral766;
extern "C" bool U3CITakeScreenshotU3Ec__Iterator19_MoveNext_m2010 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		_stringLiteral766 = il2cpp_codegen_string_literal_from_index(766);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_0089;
	}

IL_0021:
	{
		ComponentTracker_t67 * L_2 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t27 * L_3 = ComponentTracker_GetObject_m447(L_2, _stringLiteral766, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2344(L_3, 0, /*hidden argument*/NULL);
		TesteTakeScreenshot_t394 * L_4 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_4);
		int32_t L_5 = (L_4->___resWidth_1);
		TesteTakeScreenshot_t394 * L_6 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_6);
		int32_t L_7 = (L_6->___resHeight_2);
		String_t* L_8 = TesteTakeScreenshot_ScreenShotName_m2015(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		Application_CaptureScreenshot_m2653(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		WaitForEndOfFrame_t500 * L_9 = (WaitForEndOfFrame_t500 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t500_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m2246(L_9, /*hidden argument*/NULL);
		__this->___U24current_1 = L_9;
		__this->___U24PC_0 = 1;
		goto IL_008b;
	}

IL_006d:
	{
		ComponentTracker_t67 * L_10 = ComponentTracker_get_Instance_m444(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_t27 * L_11 = ComponentTracker_GetObject_m447(L_10, _stringLiteral766, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_SetActive_m2344(L_11, 1, /*hidden argument*/NULL);
		__this->___U24PC_0 = (-1);
	}

IL_0089:
	{
		return 0;
	}

IL_008b:
	{
		return 1;
	}
	// Dead block : IL_008d: ldloc.1
}
// System.Void TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::Dispose()
extern "C" void U3CITakeScreenshotU3Ec__Iterator19_Dispose_m2011 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::Reset()
extern TypeInfo* NotSupportedException_t38_il2cpp_TypeInfo_var;
extern "C" void U3CITakeScreenshotU3Ec__Iterator19_Reset_m2012 (U3CITakeScreenshotU3Ec__Iterator19_t393 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t38 * L_0 = (NotSupportedException_t38 *)il2cpp_codegen_object_new (NotSupportedException_t38_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m296(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void TesteTakeScreenshot::.ctor()
extern "C" void TesteTakeScreenshot__ctor_m2013 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method)
{
	{
		__this->___resWidth_1 = ((int32_t)2550);
		__this->___resHeight_2 = ((int32_t)3300);
		__this->___length_3 = ((int32_t)24);
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TesteTakeScreenshot::Start()
extern "C" void TesteTakeScreenshot_Start_m2014 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___resWidth_1 = L_0;
		int32_t L_1 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___resHeight_2 = L_1;
		return;
	}
}
// System.String TesteTakeScreenshot::ScreenShotName(System.Int32,System.Int32)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t220_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral808;
extern Il2CppCodeGenString* _stringLiteral809;
extern "C" String_t* TesteTakeScreenshot_ScreenShotName_m2015 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		DateTime_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral808 = il2cpp_codegen_string_literal_from_index(808);
		_stringLiteral809 = il2cpp_codegen_string_literal_from_index(809);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t220  V_0 = {0};
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = Application_get_dataPath_m2683(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t34* L_2 = L_0;
		int32_t L_3 = ___width;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t34* L_6 = L_2;
		int32_t L_7 = ___height;
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t34* L_10 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t220_il2cpp_TypeInfo_var);
		DateTime_t220  L_11 = DateTime_get_Now_m2490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_11;
		String_t* L_12 = DateTime_ToString_m2684((&V_0), _stringLiteral809, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 3, sizeof(Object_t *))) = (Object_t *)L_12;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m2511(NULL /*static, unused*/, _stringLiteral808, L_10, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void TesteTakeScreenshot::TakeHiResShot()
extern "C" void TesteTakeScreenshot_TakeHiResShot_m2016 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method)
{
	{
		__this->___takeHiResShot_4 = 1;
		return;
	}
}
// System.Void TesteTakeScreenshot::LateUpdate()
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RenderTexture_t582_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t33_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t510_m2686_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t46_m2323_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral810;
extern "C" void TesteTakeScreenshot_LateUpdate_m2017 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		RenderTexture_t582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(388);
		Texture2D_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		Component_GetComponent_TisCamera_t510_m2686_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484114);
		GameObject_GetComponent_TisRenderer_t46_m2323_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483901);
		_stringLiteral810 = il2cpp_codegen_string_literal_from_index(810);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t582 * V_0 = {0};
	Texture2D_t33 * V_1 = {0};
	ByteU5BU5D_t119* V_2 = {0};
	String_t* V_3 = {0};
	Texture2D_t33 * V_4 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2557(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Object_t * L_1 = TesteTakeScreenshot_ITakeScreenshot_m2018(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2199(__this, L_1, /*hidden argument*/NULL);
	}

IL_0019:
	{
		bool L_2 = (__this->___takeHiResShot_4);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetKeyDown_m2557(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		__this->___takeHiResShot_4 = ((int32_t)((int32_t)L_2|(int32_t)L_3));
		bool L_4 = (__this->___takeHiResShot_4);
		if (!L_4)
		{
			goto IL_0152;
		}
	}
	{
		String_t* L_5 = (__this->___lastTexture_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m2149(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_7 = (__this->___lastTexture_5);
		File_Delete_m2659(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0053:
	{
		int32_t L_8 = (__this->___resWidth_1);
		int32_t L_9 = (__this->___resHeight_2);
		int32_t L_10 = (__this->___length_3);
		RenderTexture_t582 * L_11 = (RenderTexture_t582 *)il2cpp_codegen_object_new (RenderTexture_t582_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m2685(L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		Camera_t510 * L_12 = Component_GetComponent_TisCamera_t510_m2686(__this, /*hidden argument*/Component_GetComponent_TisCamera_t510_m2686_MethodInfo_var);
		RenderTexture_t582 * L_13 = V_0;
		NullCheck(L_12);
		Camera_set_targetTexture_m2687(L_12, L_13, /*hidden argument*/NULL);
		int32_t L_14 = (__this->___resWidth_1);
		int32_t L_15 = (__this->___resHeight_2);
		Texture2D_t33 * L_16 = (Texture2D_t33 *)il2cpp_codegen_object_new (Texture2D_t33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m398(L_16, L_14, L_15, ((int32_t)13), 0, /*hidden argument*/NULL);
		V_1 = L_16;
		Camera_t510 * L_17 = Component_GetComponent_TisCamera_t510_m2686(__this, /*hidden argument*/Component_GetComponent_TisCamera_t510_m2686_MethodInfo_var);
		NullCheck(L_17);
		Camera_Render_m2688(L_17, /*hidden argument*/NULL);
		RenderTexture_t582 * L_18 = V_0;
		RenderTexture_set_active_m2689(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Texture2D_t33 * L_19 = V_1;
		int32_t L_20 = (__this->___resWidth_1);
		int32_t L_21 = (__this->___resHeight_2);
		Rect_t30  L_22 = {0};
		Rect__ctor_m387(&L_22, (0.0f), (0.0f), (((float)((float)L_20))), (((float)((float)L_21))), /*hidden argument*/NULL);
		NullCheck(L_19);
		Texture2D_ReadPixels_m2247(L_19, L_22, 0, 0, /*hidden argument*/NULL);
		Camera_t510 * L_23 = Component_GetComponent_TisCamera_t510_m2686(__this, /*hidden argument*/Component_GetComponent_TisCamera_t510_m2686_MethodInfo_var);
		NullCheck(L_23);
		Camera_set_targetTexture_m2687(L_23, (RenderTexture_t582 *)NULL, /*hidden argument*/NULL);
		RenderTexture_set_active_m2689(NULL /*static, unused*/, (RenderTexture_t582 *)NULL, /*hidden argument*/NULL);
		RenderTexture_t582 * L_24 = V_0;
		Object_Destroy_m401(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Texture2D_t33 * L_25 = V_1;
		NullCheck(L_25);
		ByteU5BU5D_t119* L_26 = Texture2D_EncodeToPNG_m2251(L_25, /*hidden argument*/NULL);
		V_2 = L_26;
		int32_t L_27 = (__this->___resWidth_1);
		int32_t L_28 = (__this->___resHeight_2);
		String_t* L_29 = TesteTakeScreenshot_ScreenShotName_m2015(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		V_3 = L_29;
		String_t* L_30 = V_3;
		ByteU5BU5D_t119* L_31 = V_2;
		File_WriteAllBytes_m2690(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		String_t* L_32 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Format_m2404(NULL /*static, unused*/, _stringLiteral810, L_32, /*hidden argument*/NULL);
		Debug_Log_m2193(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		__this->___takeHiResShot_4 = 0;
		int32_t L_34 = (__this->___resWidth_1);
		int32_t L_35 = (__this->___resHeight_2);
		Texture2D_t33 * L_36 = (Texture2D_t33 *)il2cpp_codegen_object_new (Texture2D_t33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m398(L_36, L_34, L_35, 3, 0, /*hidden argument*/NULL);
		V_4 = L_36;
		Texture2D_t33 * L_37 = V_4;
		String_t* L_38 = V_3;
		ByteU5BU5D_t119* L_39 = File_ReadAllBytes_m2655(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Texture2D_LoadImage_m2223(L_37, L_39, /*hidden argument*/NULL);
		GameObject_t27 * L_40 = (__this->___teste_6);
		NullCheck(L_40);
		Renderer_t46 * L_41 = GameObject_GetComponent_TisRenderer_t46_m2323(L_40, /*hidden argument*/GameObject_GetComponent_TisRenderer_t46_m2323_MethodInfo_var);
		NullCheck(L_41);
		Material_t45 * L_42 = Renderer_get_material_m315(L_41, /*hidden argument*/NULL);
		Texture2D_t33 * L_43 = V_4;
		NullCheck(L_42);
		Material_set_mainTexture_m2691(L_42, L_43, /*hidden argument*/NULL);
		String_t* L_44 = V_3;
		__this->___lastTexture_5 = L_44;
	}

IL_0152:
	{
		return;
	}
}
// System.Collections.IEnumerator TesteTakeScreenshot::ITakeScreenshot()
extern TypeInfo* U3CITakeScreenshotU3Ec__Iterator19_t393_il2cpp_TypeInfo_var;
extern "C" Object_t * TesteTakeScreenshot_ITakeScreenshot_m2018 (TesteTakeScreenshot_t394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CITakeScreenshotU3Ec__Iterator19_t393_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(389);
		s_Il2CppMethodIntialized = true;
	}
	U3CITakeScreenshotU3Ec__Iterator19_t393 * V_0 = {0};
	{
		U3CITakeScreenshotU3Ec__Iterator19_t393 * L_0 = (U3CITakeScreenshotU3Ec__Iterator19_t393 *)il2cpp_codegen_object_new (U3CITakeScreenshotU3Ec__Iterator19_t393_il2cpp_TypeInfo_var);
		U3CITakeScreenshotU3Ec__Iterator19__ctor_m2007(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CITakeScreenshotU3Ec__Iterator19_t393 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CITakeScreenshotU3Ec__Iterator19_t393 * L_2 = V_0;
		return L_2;
	}
}
// System.Void TransparencyAnimation::.ctor()
extern "C" void TransparencyAnimation__ctor_m2019 (TransparencyAnimation_t340 * __this, const MethodInfo* method)
{
	{
		__this->___fade_4 = (1.0f);
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TransparencyAnimation::Start()
extern TypeInfo* ShaderU5BU5D_t395_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var;
extern "C" void TransparencyAnimation_Start_m2020 (TransparencyAnimation_t340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ShaderU5BU5D_t395_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484047);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Renderer_t46 * L_0 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_0);
		MaterialU5BU5D_t49* L_1 = Renderer_get_materials_m350(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		__this->___shaders_1 = ((ShaderU5BU5D_t395*)SZArrayNew(ShaderU5BU5D_t395_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_0 = 0;
		goto IL_003d;
	}

IL_001f:
	{
		ShaderU5BU5D_t395* L_2 = (__this->___shaders_1);
		int32_t L_3 = V_0;
		Renderer_t46 * L_4 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_4);
		MaterialU5BU5D_t49* L_5 = Renderer_get_materials_m350(L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_5, L_7, sizeof(Material_t45 *))));
		Shader_t583 * L_8 = Material_get_shader_m2692((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_5, L_7, sizeof(Material_t45 *))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_8);
		*((Shader_t583 **)(Shader_t583 **)SZArrayLdElema(L_2, L_3, sizeof(Shader_t583 *))) = (Shader_t583 *)L_8;
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		Renderer_t46 * L_11 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_11);
		MaterialU5BU5D_t49* L_12 = Renderer_get_materials_m350(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_001f;
		}
	}
	{
		TransparencyAnimation_Reset_m2024(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TransparencyAnimation::Update()
extern const MethodInfo* Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var;
extern "C" void TransparencyAnimation_Update_m2021 (TransparencyAnimation_t340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484047);
		s_Il2CppMethodIntialized = true;
	}
	Material_t45 * V_0 = {0};
	MaterialU5BU5D_t49* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Material_t45 * V_4 = {0};
	MaterialU5BU5D_t49* V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	Color_t5  V_8 = {0};
	Color_t5  V_9 = {0};
	Color_t5  V_10 = {0};
	Color_t5  V_11 = {0};
	Color_t5  V_12 = {0};
	Color_t5  V_13 = {0};
	Color_t5  V_14 = {0};
	Color_t5  V_15 = {0};
	Color_t5  V_16 = {0};
	Color_t5  V_17 = {0};
	Color_t5  V_18 = {0};
	Color_t5  V_19 = {0};
	{
		bool L_0 = (__this->___show_2);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		__this->___show_2 = 0;
		TransparencyAnimation_Show_m2022(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		bool L_1 = (__this->___hide_3);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		__this->___hide_3 = 0;
		TransparencyAnimation_Hide_m2023(__this, /*hidden argument*/NULL);
	}

IL_0030:
	{
		bool L_2 = (__this->___showing_5);
		if (!L_2)
		{
			goto IL_0172;
		}
	}
	{
		float L_3 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = (__this->___startTime_7);
		float L_5 = (__this->___fade_4);
		if ((!(((float)((float)((float)L_3-(float)L_4))) <= ((float)L_5))))
		{
			goto IL_00c6;
		}
	}
	{
		Renderer_t46 * L_6 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_6);
		MaterialU5BU5D_t49* L_7 = Renderer_get_materials_m350(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_00b8;
	}

IL_0065:
	{
		MaterialU5BU5D_t49* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_0 = (*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_8, L_10, sizeof(Material_t45 *)));
		Material_t45 * L_11 = V_0;
		Material_t45 * L_12 = V_0;
		NullCheck(L_12);
		Color_t5  L_13 = Material_get_color_m313(L_12, /*hidden argument*/NULL);
		V_8 = L_13;
		float L_14 = ((&V_8)->___r_0);
		Material_t45 * L_15 = V_0;
		NullCheck(L_15);
		Color_t5  L_16 = Material_get_color_m313(L_15, /*hidden argument*/NULL);
		V_9 = L_16;
		float L_17 = ((&V_9)->___g_1);
		Material_t45 * L_18 = V_0;
		NullCheck(L_18);
		Color_t5  L_19 = Material_get_color_m313(L_18, /*hidden argument*/NULL);
		V_10 = L_19;
		float L_20 = ((&V_10)->___b_2);
		float L_21 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = (__this->___startTime_7);
		float L_23 = (__this->___fade_4);
		Color_t5  L_24 = {0};
		Color__ctor_m405(&L_24, L_14, L_17, L_20, ((float)((float)((float)((float)L_21-(float)L_22))/(float)L_23)), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_set_color_m319(L_11, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00b8:
	{
		int32_t L_26 = V_2;
		MaterialU5BU5D_t49* L_27 = V_1;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_27)->max_length)))))))
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0172;
	}

IL_00c6:
	{
		V_3 = 0;
		goto IL_0158;
	}

IL_00cd:
	{
		Renderer_t46 * L_28 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_28);
		MaterialU5BU5D_t49* L_29 = Renderer_get_materials_m350(L_28, /*hidden argument*/NULL);
		int32_t L_30 = V_3;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		Renderer_t46 * L_32 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_32);
		MaterialU5BU5D_t49* L_33 = Renderer_get_materials_m350(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_3;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_33, L_35, sizeof(Material_t45 *))));
		Color_t5  L_36 = Material_get_color_m313((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_33, L_35, sizeof(Material_t45 *))), /*hidden argument*/NULL);
		V_11 = L_36;
		float L_37 = ((&V_11)->___r_0);
		Renderer_t46 * L_38 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_38);
		MaterialU5BU5D_t49* L_39 = Renderer_get_materials_m350(L_38, /*hidden argument*/NULL);
		int32_t L_40 = V_3;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = L_40;
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_39, L_41, sizeof(Material_t45 *))));
		Color_t5  L_42 = Material_get_color_m313((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_39, L_41, sizeof(Material_t45 *))), /*hidden argument*/NULL);
		V_12 = L_42;
		float L_43 = ((&V_12)->___g_1);
		Renderer_t46 * L_44 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_44);
		MaterialU5BU5D_t49* L_45 = Renderer_get_materials_m350(L_44, /*hidden argument*/NULL);
		int32_t L_46 = V_3;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_45, L_47, sizeof(Material_t45 *))));
		Color_t5  L_48 = Material_get_color_m313((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_45, L_47, sizeof(Material_t45 *))), /*hidden argument*/NULL);
		V_13 = L_48;
		float L_49 = ((&V_13)->___b_2);
		Color_t5  L_50 = {0};
		Color__ctor_m405(&L_50, L_37, L_43, L_49, (1.0f), /*hidden argument*/NULL);
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_29, L_31, sizeof(Material_t45 *))));
		Material_set_color_m319((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_29, L_31, sizeof(Material_t45 *))), L_50, /*hidden argument*/NULL);
		Renderer_t46 * L_51 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_51);
		MaterialU5BU5D_t49* L_52 = Renderer_get_materials_m350(L_51, /*hidden argument*/NULL);
		int32_t L_53 = V_3;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = L_53;
		ShaderU5BU5D_t395* L_55 = (__this->___shaders_1);
		int32_t L_56 = V_3;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		int32_t L_57 = L_56;
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_52, L_54, sizeof(Material_t45 *))));
		Material_set_shader_m2693((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_52, L_54, sizeof(Material_t45 *))), (*(Shader_t583 **)(Shader_t583 **)SZArrayLdElema(L_55, L_57, sizeof(Shader_t583 *))), /*hidden argument*/NULL);
		int32_t L_58 = V_3;
		V_3 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_0158:
	{
		int32_t L_59 = V_3;
		Renderer_t46 * L_60 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_60);
		MaterialU5BU5D_t49* L_61 = Renderer_get_materials_m350(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		if ((((int32_t)L_59) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_61)->max_length)))))))
		{
			goto IL_00cd;
		}
	}
	{
		__this->___showing_5 = 0;
	}

IL_0172:
	{
		bool L_62 = (__this->___hiding_6);
		if (!L_62)
		{
			goto IL_02d0;
		}
	}
	{
		float L_63 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_64 = (__this->___startTime_7);
		float L_65 = (__this->___fade_4);
		if ((!(((float)((float)((float)L_63-(float)L_64))) <= ((float)L_65))))
		{
			goto IL_021b;
		}
	}
	{
		Renderer_t46 * L_66 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_66);
		MaterialU5BU5D_t49* L_67 = Renderer_get_materials_m350(L_66, /*hidden argument*/NULL);
		V_5 = L_67;
		V_6 = 0;
		goto IL_020b;
	}

IL_01a9:
	{
		MaterialU5BU5D_t49* L_68 = V_5;
		int32_t L_69 = V_6;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, L_69);
		int32_t L_70 = L_69;
		V_4 = (*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_68, L_70, sizeof(Material_t45 *)));
		Material_t45 * L_71 = V_4;
		Material_t45 * L_72 = V_4;
		NullCheck(L_72);
		Color_t5  L_73 = Material_get_color_m313(L_72, /*hidden argument*/NULL);
		V_14 = L_73;
		float L_74 = ((&V_14)->___r_0);
		Material_t45 * L_75 = V_4;
		NullCheck(L_75);
		Color_t5  L_76 = Material_get_color_m313(L_75, /*hidden argument*/NULL);
		V_15 = L_76;
		float L_77 = ((&V_15)->___g_1);
		Material_t45 * L_78 = V_4;
		NullCheck(L_78);
		Color_t5  L_79 = Material_get_color_m313(L_78, /*hidden argument*/NULL);
		V_16 = L_79;
		float L_80 = ((&V_16)->___b_2);
		float L_81 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_82 = (__this->___startTime_7);
		float L_83 = (__this->___fade_4);
		Color_t5  L_84 = {0};
		Color__ctor_m405(&L_84, L_74, L_77, L_80, ((float)((float)(1.0f)-(float)((float)((float)((float)((float)L_81-(float)L_82))/(float)L_83)))), /*hidden argument*/NULL);
		NullCheck(L_71);
		Material_set_color_m319(L_71, L_84, /*hidden argument*/NULL);
		int32_t L_85 = V_6;
		V_6 = ((int32_t)((int32_t)L_85+(int32_t)1));
	}

IL_020b:
	{
		int32_t L_86 = V_6;
		MaterialU5BU5D_t49* L_87 = V_5;
		NullCheck(L_87);
		if ((((int32_t)L_86) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_87)->max_length)))))))
		{
			goto IL_01a9;
		}
	}
	{
		goto IL_02d0;
	}

IL_021b:
	{
		V_7 = 0;
		goto IL_029a;
	}

IL_0223:
	{
		Renderer_t46 * L_88 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_88);
		MaterialU5BU5D_t49* L_89 = Renderer_get_materials_m350(L_88, /*hidden argument*/NULL);
		int32_t L_90 = V_7;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, L_90);
		int32_t L_91 = L_90;
		Renderer_t46 * L_92 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_92);
		MaterialU5BU5D_t49* L_93 = Renderer_get_materials_m350(L_92, /*hidden argument*/NULL);
		int32_t L_94 = V_7;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, L_94);
		int32_t L_95 = L_94;
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_93, L_95, sizeof(Material_t45 *))));
		Color_t5  L_96 = Material_get_color_m313((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_93, L_95, sizeof(Material_t45 *))), /*hidden argument*/NULL);
		V_17 = L_96;
		float L_97 = ((&V_17)->___r_0);
		Renderer_t46 * L_98 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_98);
		MaterialU5BU5D_t49* L_99 = Renderer_get_materials_m350(L_98, /*hidden argument*/NULL);
		int32_t L_100 = V_7;
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, L_100);
		int32_t L_101 = L_100;
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_99, L_101, sizeof(Material_t45 *))));
		Color_t5  L_102 = Material_get_color_m313((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_99, L_101, sizeof(Material_t45 *))), /*hidden argument*/NULL);
		V_18 = L_102;
		float L_103 = ((&V_18)->___g_1);
		Renderer_t46 * L_104 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_104);
		MaterialU5BU5D_t49* L_105 = Renderer_get_materials_m350(L_104, /*hidden argument*/NULL);
		int32_t L_106 = V_7;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, L_106);
		int32_t L_107 = L_106;
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_105, L_107, sizeof(Material_t45 *))));
		Color_t5  L_108 = Material_get_color_m313((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_105, L_107, sizeof(Material_t45 *))), /*hidden argument*/NULL);
		V_19 = L_108;
		float L_109 = ((&V_19)->___b_2);
		Color_t5  L_110 = {0};
		Color__ctor_m405(&L_110, L_97, L_103, L_109, (0.0f), /*hidden argument*/NULL);
		NullCheck((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_89, L_91, sizeof(Material_t45 *))));
		Material_set_color_m319((*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_89, L_91, sizeof(Material_t45 *))), L_110, /*hidden argument*/NULL);
		int32_t L_111 = V_7;
		V_7 = ((int32_t)((int32_t)L_111+(int32_t)1));
	}

IL_029a:
	{
		int32_t L_112 = V_7;
		Renderer_t46 * L_113 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_113);
		MaterialU5BU5D_t49* L_114 = Renderer_get_materials_m350(L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		if ((((int32_t)L_112) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_114)->max_length)))))))
		{
			goto IL_0223;
		}
	}
	{
		RotateScale_t385 * L_115 = (__this->___rotateScale_8);
		bool L_116 = Object_op_Implicit_m301(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		if (!L_116)
		{
			goto IL_02c9;
		}
	}
	{
		RotateScale_t385 * L_117 = (__this->___rotateScale_8);
		NullCheck(L_117);
		RotateScale_Stop_m1974(L_117, /*hidden argument*/NULL);
	}

IL_02c9:
	{
		__this->___hiding_6 = 0;
	}

IL_02d0:
	{
		return;
	}
}
// System.Void TransparencyAnimation::Show()
extern const MethodInfo* Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral811;
extern "C" void TransparencyAnimation_Show_m2022 (TransparencyAnimation_t340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484047);
		_stringLiteral811 = il2cpp_codegen_string_literal_from_index(811);
		s_Il2CppMethodIntialized = true;
	}
	Material_t45 * V_0 = {0};
	MaterialU5BU5D_t49* V_1 = {0};
	int32_t V_2 = 0;
	Color_t5  V_3 = {0};
	Color_t5  V_4 = {0};
	Color_t5  V_5 = {0};
	{
		Renderer_t46 * L_0 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_0);
		MaterialU5BU5D_t49* L_1 = Renderer_get_materials_m350(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0067;
	}

IL_0013:
	{
		MaterialU5BU5D_t49* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_2, L_4, sizeof(Material_t45 *)));
		Material_t45 * L_5 = V_0;
		Shader_t583 * L_6 = Shader_Find_m2694(NULL /*static, unused*/, _stringLiteral811, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_shader_m2693(L_5, L_6, /*hidden argument*/NULL);
		Material_t45 * L_7 = V_0;
		Material_t45 * L_8 = V_0;
		NullCheck(L_8);
		Color_t5  L_9 = Material_get_color_m313(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = ((&V_3)->___r_0);
		Material_t45 * L_11 = V_0;
		NullCheck(L_11);
		Color_t5  L_12 = Material_get_color_m313(L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		float L_13 = ((&V_4)->___g_1);
		Material_t45 * L_14 = V_0;
		NullCheck(L_14);
		Color_t5  L_15 = Material_get_color_m313(L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = ((&V_5)->___b_2);
		Color_t5  L_17 = {0};
		Color__ctor_m405(&L_17, L_10, L_13, L_16, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_set_color_m319(L_7, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_19 = V_2;
		MaterialU5BU5D_t49* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_20)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		__this->___hiding_6 = 0;
		__this->___showing_5 = 1;
		float L_21 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___startTime_7 = L_21;
		RotateScale_t385 * L_22 = (__this->___rotateScale_8);
		bool L_23 = Object_op_Implicit_m301(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00a4;
		}
	}
	{
		RotateScale_t385 * L_24 = (__this->___rotateScale_8);
		NullCheck(L_24);
		RotateScale_Play_m1973(L_24, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		return;
	}
}
// System.Void TransparencyAnimation::Hide()
extern const MethodInfo* Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral811;
extern "C" void TransparencyAnimation_Hide_m2023 (TransparencyAnimation_t340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484047);
		_stringLiteral811 = il2cpp_codegen_string_literal_from_index(811);
		s_Il2CppMethodIntialized = true;
	}
	Material_t45 * V_0 = {0};
	MaterialU5BU5D_t49* V_1 = {0};
	int32_t V_2 = 0;
	Color_t5  V_3 = {0};
	Color_t5  V_4 = {0};
	Color_t5  V_5 = {0};
	{
		Renderer_t46 * L_0 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_0);
		MaterialU5BU5D_t49* L_1 = Renderer_get_materials_m350(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0067;
	}

IL_0013:
	{
		MaterialU5BU5D_t49* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_2, L_4, sizeof(Material_t45 *)));
		Material_t45 * L_5 = V_0;
		Shader_t583 * L_6 = Shader_Find_m2694(NULL /*static, unused*/, _stringLiteral811, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_shader_m2693(L_5, L_6, /*hidden argument*/NULL);
		Material_t45 * L_7 = V_0;
		Material_t45 * L_8 = V_0;
		NullCheck(L_8);
		Color_t5  L_9 = Material_get_color_m313(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = ((&V_3)->___r_0);
		Material_t45 * L_11 = V_0;
		NullCheck(L_11);
		Color_t5  L_12 = Material_get_color_m313(L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		float L_13 = ((&V_4)->___g_1);
		Material_t45 * L_14 = V_0;
		NullCheck(L_14);
		Color_t5  L_15 = Material_get_color_m313(L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = ((&V_5)->___b_2);
		Color_t5  L_17 = {0};
		Color__ctor_m405(&L_17, L_10, L_13, L_16, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_set_color_m319(L_7, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_19 = V_2;
		MaterialU5BU5D_t49* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_20)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		__this->___showing_5 = 0;
		__this->___hiding_6 = 1;
		float L_21 = Time_get_time_m294(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___startTime_7 = L_21;
		return;
	}
}
// System.Void TransparencyAnimation::Reset()
extern const MethodInfo* Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral811;
extern "C" void TransparencyAnimation_Reset_m2024 (TransparencyAnimation_t340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484047);
		_stringLiteral811 = il2cpp_codegen_string_literal_from_index(811);
		s_Il2CppMethodIntialized = true;
	}
	Material_t45 * V_0 = {0};
	MaterialU5BU5D_t49* V_1 = {0};
	int32_t V_2 = 0;
	Color_t5  V_3 = {0};
	Color_t5  V_4 = {0};
	Color_t5  V_5 = {0};
	{
		__this->___showing_5 = 0;
		__this->___hiding_6 = 0;
		Renderer_t46 * L_0 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		NullCheck(L_0);
		MaterialU5BU5D_t49* L_1 = Renderer_get_materials_m350(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0075;
	}

IL_0021:
	{
		MaterialU5BU5D_t49* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(Material_t45 **)(Material_t45 **)SZArrayLdElema(L_2, L_4, sizeof(Material_t45 *)));
		Material_t45 * L_5 = V_0;
		Shader_t583 * L_6 = Shader_Find_m2694(NULL /*static, unused*/, _stringLiteral811, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_shader_m2693(L_5, L_6, /*hidden argument*/NULL);
		Material_t45 * L_7 = V_0;
		Material_t45 * L_8 = V_0;
		NullCheck(L_8);
		Color_t5  L_9 = Material_get_color_m313(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = ((&V_3)->___r_0);
		Material_t45 * L_11 = V_0;
		NullCheck(L_11);
		Color_t5  L_12 = Material_get_color_m313(L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		float L_13 = ((&V_4)->___g_1);
		Material_t45 * L_14 = V_0;
		NullCheck(L_14);
		Color_t5  L_15 = Material_get_color_m313(L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = ((&V_5)->___b_2);
		Color_t5  L_17 = {0};
		Color__ctor_m405(&L_17, L_10, L_13, L_16, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_set_color_m319(L_7, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0075:
	{
		int32_t L_19 = V_2;
		MaterialU5BU5D_t49* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_20)->max_length)))))))
		{
			goto IL_0021;
		}
	}
	{
		RotateScale_t385 * L_21 = (__this->___rotateScale_8);
		bool L_22 = Object_op_Implicit_m301(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0099;
		}
	}
	{
		RotateScale_t385 * L_23 = (__this->___rotateScale_8);
		NullCheck(L_23);
		RotateScale_Stop_m1974(L_23, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern TypeInfo* BackgroundPlaneAbstractBehaviour_t397_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneBehaviour__ctor_m2025 (BackgroundPlaneBehaviour_t396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BackgroundPlaneAbstractBehaviour_t397_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(392);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t397_il2cpp_TypeInfo_var);
		BackgroundPlaneAbstractBehaviour__ctor_m2695(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern "C" void CloudRecoBehaviour__ctor_m2026 (CloudRecoBehaviour_t398 * __this, const MethodInfo* method)
{
	{
		CloudRecoAbstractBehaviour__ctor_m2696(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C" void CylinderTargetBehaviour__ctor_m2027 (CylinderTargetBehaviour_t400 * __this, const MethodInfo* method)
{
	{
		CylinderTargetAbstractBehaviour__ctor_m2697(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DatabaseLoadBehaviour::.ctor()
extern "C" void DatabaseLoadBehaviour__ctor_m2028 (DatabaseLoadBehaviour_t402 * __this, const MethodInfo* method)
{
	{
		DatabaseLoadAbstractBehaviour__ctor_m2698(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DatabaseLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DatabaseLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m2029 (DatabaseLoadBehaviour_t402 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultInitializationErrorHandler__ctor_m2030 (DefaultInitializationErrorHandler_t404 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mErrorText_2 = L_0;
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern const Il2CppType* VuforiaAbstractBehaviour_t451_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* VuforiaAbstractBehaviour_t451_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t584_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2037_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2699_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_Awake_m2031 (DefaultInitializationErrorHandler_t404 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaAbstractBehaviour_t451_0_0_0_var = il2cpp_codegen_type_from_index(393);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		VuforiaAbstractBehaviour_t451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(393);
		Action_1_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(395);
		DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2037_MethodInfo_var = il2cpp_codegen_method_info_from_index(467);
		Action_1__ctor_m2699_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484116);
		s_Il2CppMethodIntialized = true;
	}
	VuforiaAbstractBehaviour_t451 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(VuforiaAbstractBehaviour_t451_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_1 = Object_FindObjectOfType_m2210(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((VuforiaAbstractBehaviour_t451 *)CastclassClass(L_1, VuforiaAbstractBehaviour_t451_il2cpp_TypeInfo_var));
		VuforiaAbstractBehaviour_t451 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m301(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		VuforiaAbstractBehaviour_t451 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2037_MethodInfo_var };
		Action_1_t584 * L_6 = (Action_1_t584 *)il2cpp_codegen_object_new (Action_1_t584_il2cpp_TypeInfo_var);
		Action_1__ctor_m2699(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m2699_MethodInfo_var);
		NullCheck(L_4);
		VuforiaAbstractBehaviour_RegisterVuforiaInitErrorCallback_m2700(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern TypeInfo* WindowFunction_t548_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_DrawWindowContent_m2034_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral812;
extern "C" void DefaultInitializationErrorHandler_OnGUI_m2032 (DefaultInitializationErrorHandler_t404 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WindowFunction_t548_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(286);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		DefaultInitializationErrorHandler_DrawWindowContent_m2034_MethodInfo_var = il2cpp_codegen_method_info_from_index(469);
		_stringLiteral812 = il2cpp_codegen_string_literal_from_index(812);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mErrorOccurred_3);
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_3 = {0};
		Rect__ctor_m387(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)DefaultInitializationErrorHandler_DrawWindowContent_m2034_MethodInfo_var };
		WindowFunction_t548 * L_5 = (WindowFunction_t548 *)il2cpp_codegen_object_new (WindowFunction_t548_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m2473(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_Window_m2701(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral812, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern const Il2CppType* VuforiaAbstractBehaviour_t451_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* VuforiaAbstractBehaviour_t451_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t584_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2037_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2699_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_OnDestroy_m2033 (DefaultInitializationErrorHandler_t404 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaAbstractBehaviour_t451_0_0_0_var = il2cpp_codegen_type_from_index(393);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		VuforiaAbstractBehaviour_t451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(393);
		Action_1_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(395);
		DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2037_MethodInfo_var = il2cpp_codegen_method_info_from_index(467);
		Action_1__ctor_m2699_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484116);
		s_Il2CppMethodIntialized = true;
	}
	VuforiaAbstractBehaviour_t451 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(VuforiaAbstractBehaviour_t451_0_0_0_var), /*hidden argument*/NULL);
		Object_t53 * L_1 = Object_FindObjectOfType_m2210(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((VuforiaAbstractBehaviour_t451 *)CastclassClass(L_1, VuforiaAbstractBehaviour_t451_il2cpp_TypeInfo_var));
		VuforiaAbstractBehaviour_t451 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m301(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		VuforiaAbstractBehaviour_t451 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2037_MethodInfo_var };
		Action_1_t584 * L_6 = (Action_1_t584 *)il2cpp_codegen_object_new (Action_1_t584_il2cpp_TypeInfo_var);
		Action_1__ctor_m2699(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m2699_MethodInfo_var);
		NullCheck(L_4);
		VuforiaAbstractBehaviour_UnregisterVuforiaInitErrorCallback_m2702(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral813;
extern "C" void DefaultInitializationErrorHandler_DrawWindowContent_m2034 (DefaultInitializationErrorHandler_t404 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral813 = il2cpp_codegen_string_literal_from_index(813);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_2 = {0};
		Rect__ctor_m387(&L_2, (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		String_t* L_3 = (__this->___mErrorText_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_Label_m2304(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_6 = {0};
		Rect__ctor_m387(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m2298(NULL /*static, unused*/, L_6, _stringLiteral813, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		Application_Quit_m2703(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral814;
extern Il2CppCodeGenString* _stringLiteral815;
extern Il2CppCodeGenString* _stringLiteral816;
extern Il2CppCodeGenString* _stringLiteral817;
extern Il2CppCodeGenString* _stringLiteral818;
extern Il2CppCodeGenString* _stringLiteral819;
extern Il2CppCodeGenString* _stringLiteral820;
extern Il2CppCodeGenString* _stringLiteral821;
extern Il2CppCodeGenString* _stringLiteral822;
extern Il2CppCodeGenString* _stringLiteral823;
extern Il2CppCodeGenString* _stringLiteral824;
extern "C" void DefaultInitializationErrorHandler_SetErrorCode_m2035 (DefaultInitializationErrorHandler_t404 * __this, int32_t ___errorCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral814 = il2cpp_codegen_string_literal_from_index(814);
		_stringLiteral815 = il2cpp_codegen_string_literal_from_index(815);
		_stringLiteral816 = il2cpp_codegen_string_literal_from_index(816);
		_stringLiteral817 = il2cpp_codegen_string_literal_from_index(817);
		_stringLiteral818 = il2cpp_codegen_string_literal_from_index(818);
		_stringLiteral819 = il2cpp_codegen_string_literal_from_index(819);
		_stringLiteral820 = il2cpp_codegen_string_literal_from_index(820);
		_stringLiteral821 = il2cpp_codegen_string_literal_from_index(821);
		_stringLiteral822 = il2cpp_codegen_string_literal_from_index(822);
		_stringLiteral823 = il2cpp_codegen_string_literal_from_index(823);
		_stringLiteral824 = il2cpp_codegen_string_literal_from_index(824);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		String_t* L_0 = (__this->___mErrorText_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m409(NULL /*static, unused*/, _stringLiteral814, L_0, /*hidden argument*/NULL);
		Debug_LogError_m302(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___errorCode;
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 0)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 1)
		{
			goto IL_00ad;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 2)
		{
			goto IL_009d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 3)
		{
			goto IL_007d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 5)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 6)
		{
			goto IL_005d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 7)
		{
			goto IL_00bd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 8)
		{
			goto IL_00cd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 9)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ed;
	}

IL_004d:
	{
		__this->___mErrorText_2 = _stringLiteral815;
		goto IL_00ed;
	}

IL_005d:
	{
		__this->___mErrorText_2 = _stringLiteral816;
		goto IL_00ed;
	}

IL_006d:
	{
		__this->___mErrorText_2 = _stringLiteral817;
		goto IL_00ed;
	}

IL_007d:
	{
		__this->___mErrorText_2 = _stringLiteral818;
		goto IL_00ed;
	}

IL_008d:
	{
		__this->___mErrorText_2 = _stringLiteral819;
		goto IL_00ed;
	}

IL_009d:
	{
		__this->___mErrorText_2 = _stringLiteral820;
		goto IL_00ed;
	}

IL_00ad:
	{
		__this->___mErrorText_2 = _stringLiteral821;
		goto IL_00ed;
	}

IL_00bd:
	{
		__this->___mErrorText_2 = _stringLiteral822;
		goto IL_00ed;
	}

IL_00cd:
	{
		__this->___mErrorText_2 = _stringLiteral823;
		goto IL_00ed;
	}

IL_00dd:
	{
		__this->___mErrorText_2 = _stringLiteral824;
		goto IL_00ed;
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C" void DefaultInitializationErrorHandler_SetErrorOccurred_m2036 (DefaultInitializationErrorHandler_t404 * __this, bool ___errorOccurred, const MethodInfo* method)
{
	{
		bool L_0 = ___errorOccurred;
		__this->___mErrorOccurred_3 = L_0;
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_OnVuforiaInitializationError_m2037 (DefaultInitializationErrorHandler_t404 * __this, int32_t ___initError, const MethodInfo* method)
{
	{
		int32_t L_0 = ___initError;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___initError;
		DefaultInitializationErrorHandler_SetErrorCode_m2035(__this, L_1, /*hidden argument*/NULL);
		DefaultInitializationErrorHandler_SetErrorOccurred_m2036(__this, 1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C" void DefaultSmartTerrainEventHandler__ctor_m2038 (DefaultSmartTerrainEventHandler_t405 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern TypeInfo* Action_1_t585_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t586_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisReconstructionBehaviour_t406_m2704_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m2041_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2705_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2042_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2707_MethodInfo_var;
extern "C" void DefaultSmartTerrainEventHandler_Start_m2039 (DefaultSmartTerrainEventHandler_t405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(399);
		Action_1_t586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(400);
		Component_GetComponent_TisReconstructionBehaviour_t406_m2704_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484118);
		DefaultSmartTerrainEventHandler_OnPropCreated_m2041_MethodInfo_var = il2cpp_codegen_method_info_from_index(471);
		Action_1__ctor_m2705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484120);
		DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2042_MethodInfo_var = il2cpp_codegen_method_info_from_index(473);
		Action_1__ctor_m2707_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484122);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t406 * L_0 = Component_GetComponent_TisReconstructionBehaviour_t406_m2704(__this, /*hidden argument*/Component_GetComponent_TisReconstructionBehaviour_t406_m2704_MethodInfo_var);
		__this->___mReconstructionBehaviour_1 = L_0;
		ReconstructionBehaviour_t406 * L_1 = (__this->___mReconstructionBehaviour_1);
		bool L_2 = Object_op_Implicit_m301(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		ReconstructionBehaviour_t406 * L_3 = (__this->___mReconstructionBehaviour_1);
		IntPtr_t L_4 = { (void*)DefaultSmartTerrainEventHandler_OnPropCreated_m2041_MethodInfo_var };
		Action_1_t585 * L_5 = (Action_1_t585 *)il2cpp_codegen_object_new (Action_1_t585_il2cpp_TypeInfo_var);
		Action_1__ctor_m2705(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m2705_MethodInfo_var);
		NullCheck(L_3);
		ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m2706(L_3, L_5, /*hidden argument*/NULL);
		ReconstructionBehaviour_t406 * L_6 = (__this->___mReconstructionBehaviour_1);
		IntPtr_t L_7 = { (void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2042_MethodInfo_var };
		Action_1_t586 * L_8 = (Action_1_t586 *)il2cpp_codegen_object_new (Action_1_t586_il2cpp_TypeInfo_var);
		Action_1__ctor_m2707(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m2707_MethodInfo_var);
		NullCheck(L_6);
		ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m2708(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern TypeInfo* Action_1_t585_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t586_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m2041_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2705_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2042_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2707_MethodInfo_var;
extern "C" void DefaultSmartTerrainEventHandler_OnDestroy_m2040 (DefaultSmartTerrainEventHandler_t405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(399);
		Action_1_t586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(400);
		DefaultSmartTerrainEventHandler_OnPropCreated_m2041_MethodInfo_var = il2cpp_codegen_method_info_from_index(471);
		Action_1__ctor_m2705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484120);
		DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2042_MethodInfo_var = il2cpp_codegen_method_info_from_index(473);
		Action_1__ctor_m2707_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484122);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t406 * L_0 = (__this->___mReconstructionBehaviour_1);
		bool L_1 = Object_op_Implicit_m301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		ReconstructionBehaviour_t406 * L_2 = (__this->___mReconstructionBehaviour_1);
		IntPtr_t L_3 = { (void*)DefaultSmartTerrainEventHandler_OnPropCreated_m2041_MethodInfo_var };
		Action_1_t585 * L_4 = (Action_1_t585 *)il2cpp_codegen_object_new (Action_1_t585_il2cpp_TypeInfo_var);
		Action_1__ctor_m2705(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m2705_MethodInfo_var);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m2709(L_2, L_4, /*hidden argument*/NULL);
		ReconstructionBehaviour_t406 * L_5 = (__this->___mReconstructionBehaviour_1);
		IntPtr_t L_6 = { (void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2042_MethodInfo_var };
		Action_1_t586 * L_7 = (Action_1_t586 *)il2cpp_codegen_object_new (Action_1_t586_il2cpp_TypeInfo_var);
		Action_1__ctor_m2707(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m2707_MethodInfo_var);
		NullCheck(L_5);
		ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m2710(L_5, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern "C" void DefaultSmartTerrainEventHandler_OnPropCreated_m2041 (DefaultSmartTerrainEventHandler_t405 * __this, Object_t * ___prop, const MethodInfo* method)
{
	{
		ReconstructionBehaviour_t406 * L_0 = (__this->___mReconstructionBehaviour_1);
		bool L_1 = Object_op_Implicit_m301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t406 * L_2 = (__this->___mReconstructionBehaviour_1);
		PropBehaviour_t407 * L_3 = (__this->___PropTemplate_2);
		Object_t * L_4 = ___prop;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateProp_m2711(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern "C" void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2042 (DefaultSmartTerrainEventHandler_t405 * __this, Object_t * ___surface, const MethodInfo* method)
{
	{
		ReconstructionBehaviour_t406 * L_0 = (__this->___mReconstructionBehaviour_1);
		bool L_1 = Object_op_Implicit_m301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t406 * L_2 = (__this->___mReconstructionBehaviour_1);
		SurfaceBehaviour_t408 * L_3 = (__this->___SurfaceTemplate_3);
		Object_t * L_4 = ___surface;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateSurface_m2712(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern "C" void DefaultTrackableEventHandler__ctor_m2043 (DefaultTrackableEventHandler_t409 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t410_m2713_MethodInfo_var;
extern "C" void DefaultTrackableEventHandler_Start_m2044 (DefaultTrackableEventHandler_t409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t410_m2713_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484123);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t410 * L_0 = Component_GetComponent_TisTrackableBehaviour_t410_m2713(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t410_m2713_MethodInfo_var);
		__this->___mTrackableBehaviour_1 = L_0;
		TrackableBehaviour_t410 * L_1 = (__this->___mTrackableBehaviour_1);
		bool L_2 = Object_op_Implicit_m301(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t410 * L_3 = (__this->___mTrackableBehaviour_1);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m2714(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void DefaultTrackableEventHandler_OnTrackableStateChanged_m2045 (DefaultTrackableEventHandler_t409 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		DefaultTrackableEventHandler_OnTrackingFound_m2046(__this, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		DefaultTrackableEventHandler_OnTrackingLost_m2047(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral825;
extern Il2CppCodeGenString* _stringLiteral826;
extern "C" void DefaultTrackableEventHandler_OnTrackingFound_m2046 (DefaultTrackableEventHandler_t409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484125);
		_stringLiteral825 = il2cpp_codegen_string_literal_from_index(825);
		_stringLiteral826 = il2cpp_codegen_string_literal_from_index(826);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t587* V_0 = {0};
	ColliderU5BU5D_t588* V_1 = {0};
	Renderer_t46 * V_2 = {0};
	RendererU5BU5D_t587* V_3 = {0};
	int32_t V_4 = 0;
	Collider_t316 * V_5 = {0};
	ColliderU5BU5D_t588* V_6 = {0};
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t587* L_0 = Component_GetComponentsInChildren_TisRenderer_t46_m2715(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t588* L_1 = Component_GetComponentsInChildren_TisCollider_t316_m2716(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var);
		V_1 = L_1;
		RendererU5BU5D_t587* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t587* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = (*(Renderer_t46 **)(Renderer_t46 **)SZArrayLdElema(L_3, L_5, sizeof(Renderer_t46 *)));
		Renderer_t46 * L_6 = V_2;
		NullCheck(L_6);
		Renderer_set_enabled_m2324(L_6, 1, /*hidden argument*/NULL);
		int32_t L_7 = V_4;
		V_4 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_8 = V_4;
		RendererU5BU5D_t587* L_9 = V_3;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t588* L_10 = V_1;
		V_6 = L_10;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t588* L_11 = V_6;
		int32_t L_12 = V_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		V_5 = (*(Collider_t316 **)(Collider_t316 **)SZArrayLdElema(L_11, L_13, sizeof(Collider_t316 *)));
		Collider_t316 * L_14 = V_5;
		NullCheck(L_14);
		Collider_set_enabled_m2717(L_14, 1, /*hidden argument*/NULL);
		int32_t L_15 = V_7;
		V_7 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_16 = V_7;
		ColliderU5BU5D_t588* L_17 = V_6;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_17)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t410 * L_18 = (__this->___mTrackableBehaviour_1);
		NullCheck(L_18);
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m2152(NULL /*static, unused*/, _stringLiteral825, L_19, _stringLiteral826, /*hidden argument*/NULL);
		Debug_Log_m2193(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral825;
extern Il2CppCodeGenString* _stringLiteral827;
extern "C" void DefaultTrackableEventHandler_OnTrackingLost_m2047 (DefaultTrackableEventHandler_t409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484125);
		_stringLiteral825 = il2cpp_codegen_string_literal_from_index(825);
		_stringLiteral827 = il2cpp_codegen_string_literal_from_index(827);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t587* V_0 = {0};
	ColliderU5BU5D_t588* V_1 = {0};
	Renderer_t46 * V_2 = {0};
	RendererU5BU5D_t587* V_3 = {0};
	int32_t V_4 = 0;
	Collider_t316 * V_5 = {0};
	ColliderU5BU5D_t588* V_6 = {0};
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t587* L_0 = Component_GetComponentsInChildren_TisRenderer_t46_m2715(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t588* L_1 = Component_GetComponentsInChildren_TisCollider_t316_m2716(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var);
		V_1 = L_1;
		RendererU5BU5D_t587* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t587* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = (*(Renderer_t46 **)(Renderer_t46 **)SZArrayLdElema(L_3, L_5, sizeof(Renderer_t46 *)));
		Renderer_t46 * L_6 = V_2;
		NullCheck(L_6);
		Renderer_set_enabled_m2324(L_6, 0, /*hidden argument*/NULL);
		int32_t L_7 = V_4;
		V_4 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_8 = V_4;
		RendererU5BU5D_t587* L_9 = V_3;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t588* L_10 = V_1;
		V_6 = L_10;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t588* L_11 = V_6;
		int32_t L_12 = V_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		V_5 = (*(Collider_t316 **)(Collider_t316 **)SZArrayLdElema(L_11, L_13, sizeof(Collider_t316 *)));
		Collider_t316 * L_14 = V_5;
		NullCheck(L_14);
		Collider_set_enabled_m2717(L_14, 0, /*hidden argument*/NULL);
		int32_t L_15 = V_7;
		V_7 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_16 = V_7;
		ColliderU5BU5D_t588* L_17 = V_6;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_17)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t410 * L_18 = (__this->___mTrackableBehaviour_1);
		NullCheck(L_18);
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m2152(NULL /*static, unused*/, _stringLiteral825, L_19, _stringLiteral827, /*hidden argument*/NULL);
		Debug_Log_m2193(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.ctor()
extern "C" void GLErrorHandler__ctor_m2048 (GLErrorHandler_t411 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GLErrorHandler_t411_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler__cctor_m2049 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		GLErrorHandler_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(402);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((GLErrorHandler_t411_StaticFields*)GLErrorHandler_t411_il2cpp_TypeInfo_var->static_fields)->___mErrorText_2 = L_0;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern TypeInfo* GLErrorHandler_t411_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler_SetError_m2050 (Object_t * __this /* static, unused */, String_t* ___errorText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(402);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___errorText;
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t411_il2cpp_TypeInfo_var);
		((GLErrorHandler_t411_StaticFields*)GLErrorHandler_t411_il2cpp_TypeInfo_var->static_fields)->___mErrorText_2 = L_0;
		((GLErrorHandler_t411_StaticFields*)GLErrorHandler_t411_il2cpp_TypeInfo_var->static_fields)->___mErrorOccurred_3 = 1;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern TypeInfo* GLErrorHandler_t411_il2cpp_TypeInfo_var;
extern TypeInfo* WindowFunction_t548_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern const MethodInfo* GLErrorHandler_DrawWindowContent_m2052_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral828;
extern "C" void GLErrorHandler_OnGUI_m2051 (GLErrorHandler_t411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(402);
		WindowFunction_t548_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(286);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		GLErrorHandler_DrawWindowContent_m2052_MethodInfo_var = il2cpp_codegen_method_info_from_index(478);
		_stringLiteral828 = il2cpp_codegen_string_literal_from_index(828);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t411_il2cpp_TypeInfo_var);
		bool L_0 = ((GLErrorHandler_t411_StaticFields*)GLErrorHandler_t411_il2cpp_TypeInfo_var->static_fields)->___mErrorOccurred_3;
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_3 = {0};
		Rect__ctor_m387(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)GLErrorHandler_DrawWindowContent_m2052_MethodInfo_var };
		WindowFunction_t548 * L_5 = (WindowFunction_t548 *)il2cpp_codegen_object_new (WindowFunction_t548_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m2473(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_Window_m2701(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral828, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern TypeInfo* GLErrorHandler_t411_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t505_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral813;
extern "C" void GLErrorHandler_DrawWindowContent_m2052 (GLErrorHandler_t411 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(402);
		GUI_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral813 = il2cpp_codegen_string_literal_from_index(813);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_2 = {0};
		Rect__ctor_m387(&L_2, (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t411_il2cpp_TypeInfo_var);
		String_t* L_3 = ((GLErrorHandler_t411_StaticFields*)GLErrorHandler_t411_il2cpp_TypeInfo_var->static_fields)->___mErrorText_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t505_il2cpp_TypeInfo_var);
		GUI_Label_m2304(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m396(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m397(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t30  L_6 = {0};
		Rect__ctor_m387(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m2298(NULL /*static, unused*/, L_6, _stringLiteral813, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		Application_Quit_m2703(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C" void HideExcessAreaBehaviour__ctor_m2053 (HideExcessAreaBehaviour_t412 * __this, const MethodInfo* method)
{
	{
		HideExcessAreaAbstractBehaviour__ctor_m2718(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C" void ImageTargetBehaviour__ctor_m2054 (ImageTargetBehaviour_t414 * __this, const MethodInfo* method)
{
	{
		ImageTargetAbstractBehaviour__ctor_m2719(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C" void AndroidUnityPlayer__ctor_m2055 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern "C" void AndroidUnityPlayer_LoadNativeLibraries_m2056 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_LoadNativeLibrariesFromJava_m2064(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern "C" void AndroidUnityPlayer_InitializePlatform_m2057 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_InitAndroidPlatform_m2065(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
extern "C" int32_t AndroidUnityPlayer_Start_m2058 (AndroidUnityPlayer_t416 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___licenseKey;
		int32_t L_1 = AndroidUnityPlayer_InitVuforia_m2066(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m2067(__this, /*hidden argument*/NULL);
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		return (int32_t)(L_3);
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t589_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_Update_m2059 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t589_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m2067(__this, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m2721(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		AndroidUnityPlayer_ResetUnityScreenOrientation_m2068(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		AndroidUnityPlayer_CheckOrientation_m2069(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		int32_t L_3 = (__this->___mFramesSinceLastOrientationReset_4);
		__this->___mFramesSinceLastOrientationReset_4 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern "C" void AndroidUnityPlayer_OnPause_m2060 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnPause_m2722(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern "C" void AndroidUnityPlayer_OnResume_m2061 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnResume_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern "C" void AndroidUnityPlayer_OnDestroy_m2062 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_Deinit_m2724(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern "C" void AndroidUnityPlayer_Dispose_m2063 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern "C" void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m2064 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern "C" void AndroidUnityPlayer_InitAndroidPlatform_m2065 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 Vuforia.AndroidUnityPlayer::InitVuforia(System.String)
extern "C" int32_t AndroidUnityPlayer_InitVuforia_m2066 (AndroidUnityPlayer_t416 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t589_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_InitializeSurface_m2067 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t589_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m2725(NULL /*static, unused*/, /*hidden argument*/NULL);
		AndroidUnityPlayer_ResetUnityScreenOrientation_m2068(__this, /*hidden argument*/NULL);
		AndroidUnityPlayer_CheckOrientation_m2069(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern "C" void AndroidUnityPlayer_ResetUnityScreenOrientation_m2068 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m2721(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mScreenOrientation_2 = L_0;
		__this->___mFramesSinceLastOrientationReset_4 = 0;
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern TypeInfo* SurfaceUtilities_t589_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_CheckOrientation_m2069 (AndroidUnityPlayer_t416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = (__this->___mFramesSinceLastOrientationReset_4);
		V_0 = ((((int32_t)L_0) < ((int32_t)((int32_t)25)))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		V_0 = ((((int32_t)L_2) > ((int32_t)((int32_t)60)))? 1 : 0);
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = (__this->___mScreenOrientation_2);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_2 = L_5;
		int32_t L_6 = V_2;
		int32_t L_7 = (__this->___mJavaScreenOrientation_3);
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_2;
		__this->___mJavaScreenOrientation_3 = L_8;
		int32_t L_9 = (__this->___mJavaScreenOrientation_3);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t589_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m2726(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->___mFramesSinceLastJavaOrientationCheck_5 = 0;
		goto IL_0063;
	}

IL_0055:
	{
		int32_t L_10 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		__this->___mFramesSinceLastJavaOrientationCheck_5 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C" void ComponentFactoryStarterBehaviour__ctor_m2070 (ComponentFactoryStarterBehaviour_t417 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern const Il2CppType* Action_t81_0_0_0_var;
extern TypeInfo* Attribute_t592_il2cpp_TypeInfo_var;
extern TypeInfo* FactorySetter_t594_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t81_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t591_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisMethodInfo_t_m2727_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m2728_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2729_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2730_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2732_MethodInfo_var;
extern "C" void ComponentFactoryStarterBehaviour_Awake_m2071 (ComponentFactoryStarterBehaviour_t417 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t81_0_0_0_var = il2cpp_codegen_type_from_index(64);
		Attribute_t592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(405);
		FactorySetter_t594_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(406);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Action_t81_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(64);
		Enumerator_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(407);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		Enumerable_ToList_TisMethodInfo_t_m2727_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484127);
		List_1_AddRange_m2728_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484128);
		List_1_GetEnumerator_m2729_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484129);
		Enumerator_get_Current_m2730_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484130);
		Enumerator_MoveNext_m2732_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484131);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t590 * V_0 = {0};
	MethodInfo_t * V_1 = {0};
	Enumerator_t591  V_2 = {0};
	Attribute_t592 * V_3 = {0};
	ObjectU5BU5D_t34* V_4 = {0};
	int32_t V_5 = 0;
	Action_t81 * V_6 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = Object_GetType_m303(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MethodInfoU5BU5D_t593* L_1 = (MethodInfoU5BU5D_t593*)VirtFuncInvoker1< MethodInfoU5BU5D_t593*, int32_t >::Invoke(51 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)38));
		List_1_t590 * L_2 = Enumerable_ToList_TisMethodInfo_t_m2727(NULL /*static, unused*/, (Object_t*)(Object_t*)L_1, /*hidden argument*/Enumerable_ToList_TisMethodInfo_t_m2727_MethodInfo_var);
		V_0 = L_2;
		List_1_t590 * L_3 = V_0;
		Type_t * L_4 = Object_GetType_m303(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		MethodInfoU5BU5D_t593* L_5 = (MethodInfoU5BU5D_t593*)VirtFuncInvoker1< MethodInfoU5BU5D_t593*, int32_t >::Invoke(51 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_4, ((int32_t)22));
		NullCheck(L_3);
		List_1_AddRange_m2728(L_3, (Object_t*)(Object_t*)L_5, /*hidden argument*/List_1_AddRange_m2728_MethodInfo_var);
		List_1_t590 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t591  L_7 = List_1_GetEnumerator_m2729(L_6, /*hidden argument*/List_1_GetEnumerator_m2729_MethodInfo_var);
		V_2 = L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0032:
		{
			MethodInfo_t * L_8 = Enumerator_get_Current_m2730((&V_2), /*hidden argument*/Enumerator_get_Current_m2730_MethodInfo_var);
			V_1 = L_8;
			MethodInfo_t * L_9 = V_1;
			NullCheck(L_9);
			ObjectU5BU5D_t34* L_10 = (ObjectU5BU5D_t34*)VirtFuncInvoker1< ObjectU5BU5D_t34*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_9, 1);
			V_4 = L_10;
			V_5 = 0;
			goto IL_008d;
		}

IL_004b:
		{
			ObjectU5BU5D_t34* L_11 = V_4;
			int32_t L_12 = V_5;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
			int32_t L_13 = L_12;
			V_3 = ((Attribute_t592 *)CastclassClass((*(Object_t **)(Object_t **)SZArrayLdElema(L_11, L_13, sizeof(Object_t *))), Attribute_t592_il2cpp_TypeInfo_var));
			Attribute_t592 * L_14 = V_3;
			if (!((FactorySetter_t594 *)IsInstClass(L_14, FactorySetter_t594_il2cpp_TypeInfo_var)))
			{
				goto IL_0087;
			}
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_15 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Action_t81_0_0_0_var), /*hidden argument*/NULL);
			MethodInfo_t * L_16 = V_1;
			Delegate_t506 * L_17 = Delegate_CreateDelegate_m2731(NULL /*static, unused*/, L_15, __this, L_16, /*hidden argument*/NULL);
			V_6 = ((Action_t81 *)IsInstSealed(L_17, Action_t81_il2cpp_TypeInfo_var));
			Action_t81 * L_18 = V_6;
			if (!L_18)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			Action_t81 * L_19 = V_6;
			NullCheck(L_19);
			Action_Invoke_m2167(L_19, /*hidden argument*/NULL);
		}

IL_0087:
		{
			int32_t L_20 = V_5;
			V_5 = ((int32_t)((int32_t)L_20+(int32_t)1));
		}

IL_008d:
		{
			int32_t L_21 = V_5;
			ObjectU5BU5D_t34* L_22 = V_4;
			NullCheck(L_22);
			if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))))))
			{
				goto IL_004b;
			}
		}

IL_0098:
		{
			bool L_23 = Enumerator_MoveNext_m2732((&V_2), /*hidden argument*/Enumerator_MoveNext_m2732_MethodInfo_var);
			if (L_23)
			{
				goto IL_0032;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB5, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_t591  L_24 = V_2;
		Enumerator_t591  L_25 = L_24;
		Object_t * L_26 = Box(Enumerator_t591_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_26);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_00b5:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern TypeInfo* VuforiaBehaviourComponentFactory_t419_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral829;
extern "C" void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m2072 (ComponentFactoryStarterBehaviour_t417 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaBehaviourComponentFactory_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(408);
		_stringLiteral829 = il2cpp_codegen_string_literal_from_index(829);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m2193(NULL /*static, unused*/, _stringLiteral829, /*hidden argument*/NULL);
		VuforiaBehaviourComponentFactory_t419 * L_0 = (VuforiaBehaviourComponentFactory_t419 *)il2cpp_codegen_object_new (VuforiaBehaviourComponentFactory_t419_il2cpp_TypeInfo_var);
		VuforiaBehaviourComponentFactory__ctor_m2087(L_0, /*hidden argument*/NULL);
		BehaviourComponentFactory_set_Instance_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C" void IOSUnityPlayer__ctor_m2073 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern "C" void IOSUnityPlayer_LoadNativeLibraries_m2074 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern "C" void IOSUnityPlayer_InitializePlatform_m2075 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	{
		IOSUnityPlayer_setPlatFormNative_m2084(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
extern "C" int32_t IOSUnityPlayer_Start_m2076 (IOSUnityPlayer_t418 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m2721(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___licenseKey;
		int32_t L_2 = IOSUnityPlayer_initQCARiOS_m2085(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m2082(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		return (int32_t)(L_4);
	}
}
// System.Void Vuforia.IOSUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t589_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_Update_m2077 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t589_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m2082(__this, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m2721(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		IOSUnityPlayer_SetUnityScreenOrientation_m2083(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern "C" void IOSUnityPlayer_Dispose_m2078 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern "C" void IOSUnityPlayer_OnPause_m2079 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnPause_m2722(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern "C" void IOSUnityPlayer_OnResume_m2080 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnResume_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern "C" void IOSUnityPlayer_OnDestroy_m2081 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_Deinit_m2724(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t589_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_InitializeSurface_m2082 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t589_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m2725(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOSUnityPlayer_SetUnityScreenOrientation_m2083(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern TypeInfo* SurfaceUtilities_t589_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_SetUnityScreenOrientation_m2083 (IOSUnityPlayer_t418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_orientation_m2721(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mScreenOrientation_0 = L_0;
		int32_t L_1 = (__this->___mScreenOrientation_0);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t589_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m2726(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		IOSUnityPlayer_setSurfaceOrientationiOS_m2086(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C" {void DEFAULT_CALL setPlatFormNative();}
extern "C" void IOSUnityPlayer_setPlatFormNative_m2084 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setPlatFormNative;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPlatFormNative'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
extern "C" {int32_t DEFAULT_CALL initQCARiOS(int32_t, char*);}
extern "C" int32_t IOSUnityPlayer_initQCARiOS_m2085 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, String_t* ___licenseKey, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)initQCARiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initQCARiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Marshaling of parameter '___licenseKey' to native representation
	char* ____licenseKey_marshaled = { 0 };
	____licenseKey_marshaled = il2cpp_codegen_marshal_string(___licenseKey);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___screenOrientation, ____licenseKey_marshaled);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

	// Marshaling cleanup of parameter '___licenseKey' native representation
	il2cpp_codegen_marshal_free(____licenseKey_marshaled);
	____licenseKey_marshaled = NULL;

	return _return_value;
}
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C" {void DEFAULT_CALL setSurfaceOrientationiOS(int32_t);}
extern "C" void IOSUnityPlayer_setSurfaceOrientationiOS_m2086 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setSurfaceOrientationiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSurfaceOrientationiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___screenOrientation);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

}
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m2087 (VuforiaBehaviourComponentFactory_t419 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMaskOutBehaviour_t424_m2734_MethodInfo_var;
extern "C" MaskOutAbstractBehaviour_t425 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m2088 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMaskOutBehaviour_t424_m2734_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		MaskOutBehaviour_t424 * L_1 = GameObject_AddComponent_TisMaskOutBehaviour_t424_m2734(L_0, /*hidden argument*/GameObject_AddComponent_TisMaskOutBehaviour_t424_m2734_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisVirtualButtonBehaviour_t448_m2735_MethodInfo_var;
extern "C" VirtualButtonAbstractBehaviour_t449 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m2089 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisVirtualButtonBehaviour_t448_m2735_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484133);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		VirtualButtonBehaviour_t448 * L_1 = GameObject_AddComponent_TisVirtualButtonBehaviour_t448_m2735(L_0, /*hidden argument*/GameObject_AddComponent_TisVirtualButtonBehaviour_t448_m2735_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTurnOffBehaviour_t439_m2736_MethodInfo_var;
extern "C" TurnOffAbstractBehaviour_t440 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m2090 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisTurnOffBehaviour_t439_m2736_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484134);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		TurnOffBehaviour_t439 * L_1 = GameObject_AddComponent_TisTurnOffBehaviour_t439_m2736(L_0, /*hidden argument*/GameObject_AddComponent_TisTurnOffBehaviour_t439_m2736_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisImageTargetBehaviour_t414_m2737_MethodInfo_var;
extern "C" ImageTargetAbstractBehaviour_t415 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m2091 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisImageTargetBehaviour_t414_m2737_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484135);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		ImageTargetBehaviour_t414 * L_1 = GameObject_AddComponent_TisImageTargetBehaviour_t414_m2737(L_0, /*hidden argument*/GameObject_AddComponent_TisImageTargetBehaviour_t414_m2737_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMarkerBehaviour_t422_m2738_MethodInfo_var;
extern "C" MarkerAbstractBehaviour_t423 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m2092 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMarkerBehaviour_t422_m2738_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484136);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		MarkerBehaviour_t422 * L_1 = GameObject_AddComponent_TisMarkerBehaviour_t422_m2738(L_0, /*hidden argument*/GameObject_AddComponent_TisMarkerBehaviour_t422_m2738_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMultiTargetBehaviour_t426_m2739_MethodInfo_var;
extern "C" MultiTargetAbstractBehaviour_t427 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m2093 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMultiTargetBehaviour_t426_m2739_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484137);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		MultiTargetBehaviour_t426 * L_1 = GameObject_AddComponent_TisMultiTargetBehaviour_t426_m2739(L_0, /*hidden argument*/GameObject_AddComponent_TisMultiTargetBehaviour_t426_m2739_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisCylinderTargetBehaviour_t400_m2740_MethodInfo_var;
extern "C" CylinderTargetAbstractBehaviour_t401 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m2094 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisCylinderTargetBehaviour_t400_m2740_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484138);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		CylinderTargetBehaviour_t400 * L_1 = GameObject_AddComponent_TisCylinderTargetBehaviour_t400_m2740(L_0, /*hidden argument*/GameObject_AddComponent_TisCylinderTargetBehaviour_t400_m2740_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisWordBehaviour_t456_m2741_MethodInfo_var;
extern "C" WordAbstractBehaviour_t457 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m2095 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisWordBehaviour_t456_m2741_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484139);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		WordBehaviour_t456 * L_1 = GameObject_AddComponent_TisWordBehaviour_t456_m2741(L_0, /*hidden argument*/GameObject_AddComponent_TisWordBehaviour_t456_m2741_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTextRecoBehaviour_t437_m2742_MethodInfo_var;
extern "C" TextRecoAbstractBehaviour_t438 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2096 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisTextRecoBehaviour_t437_m2742_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484140);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		TextRecoBehaviour_t437 * L_1 = GameObject_AddComponent_TisTextRecoBehaviour_t437_m2742(L_0, /*hidden argument*/GameObject_AddComponent_TisTextRecoBehaviour_t437_m2742_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisObjectTargetBehaviour_t428_m2743_MethodInfo_var;
extern "C" ObjectTargetAbstractBehaviour_t429 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m2097 (VuforiaBehaviourComponentFactory_t419 * __this, GameObject_t27 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisObjectTargetBehaviour_t428_m2743_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484141);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t27 * L_0 = ___gameObject;
		NullCheck(L_0);
		ObjectTargetBehaviour_t428 * L_1 = GameObject_AddComponent_TisObjectTargetBehaviour_t428_m2743(L_0, /*hidden argument*/GameObject_AddComponent_TisObjectTargetBehaviour_t428_m2743_MethodInfo_var);
		return L_1;
	}
}
// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern "C" void KeepAliveBehaviour__ctor_m2098 (KeepAliveBehaviour_t420 * __this, const MethodInfo* method)
{
	{
		KeepAliveAbstractBehaviour__ctor_m2744(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MarkerBehaviour::.ctor()
extern "C" void MarkerBehaviour__ctor_m2099 (MarkerBehaviour_t422 * __this, const MethodInfo* method)
{
	{
		MarkerAbstractBehaviour__ctor_m2745(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C" void MaskOutBehaviour__ctor_m2100 (MaskOutBehaviour_t424 * __this, const MethodInfo* method)
{
	{
		MaskOutAbstractBehaviour__ctor_m2746(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::Start()
extern TypeInfo* VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var;
extern TypeInfo* MaterialU5BU5D_t49_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var;
extern "C" void MaskOutBehaviour_Start_m2101 (MaskOutBehaviour_t424 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(419);
		MaterialU5BU5D_t49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484047);
		s_Il2CppMethodIntialized = true;
	}
	Renderer_t46 * V_0 = {0};
	int32_t V_1 = 0;
	MaterialU5BU5D_t49* V_2 = {0};
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m2747(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Renderer_t46 * L_1 = Component_GetComponent_TisRenderer_t46_m2553(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t46_m2553_MethodInfo_var);
		V_0 = L_1;
		Renderer_t46 * L_2 = V_0;
		NullCheck(L_2);
		MaterialU5BU5D_t49* L_3 = Renderer_get_materials_m350(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Array_t *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		Renderer_t46 * L_5 = V_0;
		Material_t45 * L_6 = (((MaskOutAbstractBehaviour_t425 *)__this)->___maskMaterial_1);
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m2748(L_5, L_6, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0032:
	{
		int32_t L_7 = V_1;
		V_2 = ((MaterialU5BU5D_t49*)SZArrayNew(MaterialU5BU5D_t49_il2cpp_TypeInfo_var, L_7));
		V_3 = 0;
		goto IL_004d;
	}

IL_0040:
	{
		MaterialU5BU5D_t49* L_8 = V_2;
		int32_t L_9 = V_3;
		Material_t45 * L_10 = (((MaskOutAbstractBehaviour_t425 *)__this)->___maskMaterial_1);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_10);
		*((Material_t45 **)(Material_t45 **)SZArrayLdElema(L_8, L_9, sizeof(Material_t45 *))) = (Material_t45 *)L_10;
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t46 * L_14 = V_0;
		MaterialU5BU5D_t49* L_15 = V_2;
		NullCheck(L_14);
		Renderer_set_sharedMaterials_m2749(L_14, L_15, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C" void MultiTargetBehaviour__ctor_m2102 (MultiTargetBehaviour_t426 * __this, const MethodInfo* method)
{
	{
		MultiTargetAbstractBehaviour__ctor_m2750(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C" void ObjectTargetBehaviour__ctor_m2103 (ObjectTargetBehaviour_t428 * __this, const MethodInfo* method)
{
	{
		ObjectTargetAbstractBehaviour__ctor_m2751(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PropBehaviour::.ctor()
extern "C" void PropBehaviour__ctor_m2104 (PropBehaviour_t407 * __this, const MethodInfo* method)
{
	{
		PropAbstractBehaviour__ctor_m2752(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C" void ReconstructionBehaviour__ctor_m2105 (ReconstructionBehaviour_t406 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour__ctor_m2753(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern "C" void ReconstructionFromTargetBehaviour__ctor_m2106 (ReconstructionFromTargetBehaviour_t432 * __this, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour__ctor_m2754(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern "C" void SmartTerrainTrackerBehaviour__ctor_m2107 (SmartTerrainTrackerBehaviour_t434 * __this, const MethodInfo* method)
{
	{
		SmartTerrainTrackerAbstractBehaviour__ctor_m2755(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C" void SurfaceBehaviour__ctor_m2108 (SurfaceBehaviour_t408 * __this, const MethodInfo* method)
{
	{
		SurfaceAbstractBehaviour__ctor_m2756(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C" void TextRecoBehaviour__ctor_m2109 (TextRecoBehaviour_t437 * __this, const MethodInfo* method)
{
	{
		TextRecoAbstractBehaviour__ctor_m2757(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C" void TurnOffBehaviour__ctor_m2110 (TurnOffBehaviour_t439 * __this, const MethodInfo* method)
{
	{
		TurnOffAbstractBehaviour__ctor_m2758(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern TypeInfo* VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t596_m2759_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var;
extern "C" void TurnOffBehaviour_Awake_m2111 (TurnOffBehaviour_t439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(419);
		Component_GetComponent_TisMeshRenderer_t596_m2759_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484142);
		Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484143);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t596 * V_0 = {0};
	MeshFilter_t597 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m2747(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MeshRenderer_t596 * L_1 = Component_GetComponent_TisMeshRenderer_t596_m2759(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t596_m2759_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t596 * L_2 = V_0;
		Object_Destroy_m401(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		MeshFilter_t597 * L_3 = Component_GetComponent_TisMeshFilter_t597_m2760(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var);
		V_1 = L_3;
		MeshFilter_t597 * L_4 = V_1;
		Object_Destroy_m401(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C" void TurnOffWordBehaviour__ctor_m2112 (TurnOffWordBehaviour_t441 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern TypeInfo* VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t596_m2759_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral345;
extern "C" void TurnOffWordBehaviour_Awake_m2113 (TurnOffWordBehaviour_t441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(419);
		Component_GetComponent_TisMeshRenderer_t596_m2759_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484142);
		_stringLiteral345 = il2cpp_codegen_string_literal_from_index(345);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t596 * V_0 = {0};
	Transform_t25 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m2747(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		MeshRenderer_t596 * L_1 = Component_GetComponent_TisMeshRenderer_t596_m2759(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t596_m2759_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t596 * L_2 = V_0;
		Object_Destroy_m401(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Transform_t25 * L_3 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t25 * L_4 = Transform_FindChild_m2761(L_3, _stringLiteral345, /*hidden argument*/NULL);
		V_1 = L_4;
		Transform_t25 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m395(NULL /*static, unused*/, L_5, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Transform_t25 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t27 * L_8 = Component_get_gameObject_m306(L_7, /*hidden argument*/NULL);
		Object_Destroy_m401(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingBehaviour__ctor_m2114 (UserDefinedTargetBuildingBehaviour_t442 * __this, const MethodInfo* method)
{
	{
		UserDefinedTargetBuildingAbstractBehaviour__ctor_m2762(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern "C" void VideoBackgroundBehaviour__ctor_m2115 (VideoBackgroundBehaviour_t444 * __this, const MethodInfo* method)
{
	{
		VideoBackgroundAbstractBehaviour__ctor_m2763(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern "C" void VideoTextureRenderer__ctor_m2116 (VideoTextureRenderer_t446 * __this, const MethodInfo* method)
{
	{
		VideoTextureRendererAbstractBehaviour__ctor_m2764(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C" void VirtualButtonBehaviour__ctor_m2117 (VirtualButtonBehaviour_t448 * __this, const MethodInfo* method)
{
	{
		VirtualButtonAbstractBehaviour__ctor_m2765(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C" void VuforiaBehaviour__ctor_m2118 (VuforiaBehaviour_t450 * __this, const MethodInfo* method)
{
	{
		VuforiaAbstractBehaviour__ctor_m2766(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C" void VuforiaBehaviour__cctor_m2119 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern TypeInfo* NullUnityPlayer_t598_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidUnityPlayer_t416_il2cpp_TypeInfo_var;
extern TypeInfo* IOSUnityPlayer_t418_il2cpp_TypeInfo_var;
extern TypeInfo* VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var;
extern TypeInfo* PlayModeUnityPlayer_t599_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t417_m2771_MethodInfo_var;
extern "C" void VuforiaBehaviour_Awake_m2120 (VuforiaBehaviour_t450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullUnityPlayer_t598_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(424);
		AndroidUnityPlayer_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(425);
		IOSUnityPlayer_t418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(426);
		VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(419);
		PlayModeUnityPlayer_t599_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(427);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t417_m2771_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484144);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		NullUnityPlayer_t598 * L_0 = (NullUnityPlayer_t598 *)il2cpp_codegen_object_new (NullUnityPlayer_t598_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m2767(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m2652(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t416 * L_2 = (AndroidUnityPlayer_t416 *)il2cpp_codegen_object_new (AndroidUnityPlayer_t416_il2cpp_TypeInfo_var);
		AndroidUnityPlayer__ctor_m2055(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0043;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m2652(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t418 * L_4 = (IOSUnityPlayer_t418 *)il2cpp_codegen_object_new (IOSUnityPlayer_t418_il2cpp_TypeInfo_var);
		IOSUnityPlayer__ctor_m2073(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0043;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t595_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2768(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		PlayModeUnityPlayer_t599 * L_6 = (PlayModeUnityPlayer_t599 *)il2cpp_codegen_object_new (PlayModeUnityPlayer_t599_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2769(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0043:
	{
		Object_t * L_7 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m2770(__this, L_7, /*hidden argument*/NULL);
		GameObject_t27 * L_8 = Component_get_gameObject_m306(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t417_m2771(L_8, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t417_m2771_MethodInfo_var);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern TypeInfo* VuforiaBehaviour_t450_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisVuforiaBehaviour_t450_m2772_MethodInfo_var;
extern "C" VuforiaBehaviour_t450 * VuforiaBehaviour_get_Instance_m2121 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaBehaviour_t450_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(428);
		Object_FindObjectOfType_TisVuforiaBehaviour_t450_m2772_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484145);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t450_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t450 * L_0 = ((VuforiaBehaviour_t450_StaticFields*)VuforiaBehaviour_t450_il2cpp_TypeInfo_var->static_fields)->___mVuforiaBehaviour_47;
		bool L_1 = Object_op_Equality_m427(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		VuforiaBehaviour_t450 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t450_m2772(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t450_m2772_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t450_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t450_StaticFields*)VuforiaBehaviour_t450_il2cpp_TypeInfo_var->static_fields)->___mVuforiaBehaviour_47 = L_2;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t450_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t450 * L_3 = ((VuforiaBehaviour_t450_StaticFields*)VuforiaBehaviour_t450_il2cpp_TypeInfo_var->static_fields)->___mVuforiaBehaviour_47;
		return L_3;
	}
}
// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C" void WebCamBehaviour__ctor_m2122 (WebCamBehaviour_t452 * __this, const MethodInfo* method)
{
	{
		WebCamAbstractBehaviour__ctor_m2773(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C" void WireframeBehaviour__ctor_m2123 (WireframeBehaviour_t454 * __this, const MethodInfo* method)
{
	{
		__this->___ShowLines_2 = 1;
		Color_t5  L_0 = Color_get_green_m2309(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___LineColor_3 = L_0;
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t45_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral830;
extern Il2CppCodeGenString* _stringLiteral104;
extern Il2CppCodeGenString* _stringLiteral831;
extern "C" void WireframeBehaviour_CreateLineMaterial_m2124 (WireframeBehaviour_t454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Material_t45_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		_stringLiteral830 = il2cpp_codegen_string_literal_from_index(830);
		_stringLiteral104 = il2cpp_codegen_string_literal_from_index(104);
		_stringLiteral831 = il2cpp_codegen_string_literal_from_index(831);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, ((int32_t)9)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral830);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral830;
		ObjectU5BU5D_t34* L_1 = L_0;
		Color_t5 * L_2 = &(__this->___LineColor_3);
		float L_3 = (L_2->___r_0);
		float L_4 = L_3;
		Object_t * L_5 = Box(Single_t36_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t34* L_6 = L_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral104);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral104;
		ObjectU5BU5D_t34* L_7 = L_6;
		Color_t5 * L_8 = &(__this->___LineColor_3);
		float L_9 = (L_8->___g_1);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t36_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral104);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral104;
		ObjectU5BU5D_t34* L_13 = L_12;
		Color_t5 * L_14 = &(__this->___LineColor_3);
		float L_15 = (L_14->___b_2);
		float L_16 = L_15;
		Object_t * L_17 = Box(Single_t36_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t34* L_18 = L_13;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, _stringLiteral104);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral104;
		ObjectU5BU5D_t34* L_19 = L_18;
		Color_t5 * L_20 = &(__this->___LineColor_3);
		float L_21 = (L_20->___a_3);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t36_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 7, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t34* L_24 = L_19;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		ArrayElementTypeCheck (L_24, _stringLiteral831);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral831;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m2254(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Material_t45 * L_26 = (Material_t45 *)il2cpp_codegen_object_new (Material_t45_il2cpp_TypeInfo_var);
		Material__ctor_m2774(L_26, L_25, /*hidden argument*/NULL);
		__this->___mLineMaterial_1 = L_26;
		Material_t45 * L_27 = (__this->___mLineMaterial_1);
		NullCheck(L_27);
		Object_set_hideFlags_m2253(L_27, ((int32_t)13), /*hidden argument*/NULL);
		Material_t45 * L_28 = (__this->___mLineMaterial_1);
		NullCheck(L_28);
		Shader_t583 * L_29 = Material_get_shader_m2692(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Object_set_hideFlags_m2253(L_29, ((int32_t)13), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern TypeInfo* VuforiaManager_t602_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t510_m2776_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var;
extern "C" void WireframeBehaviour_OnRenderObject_m2125 (WireframeBehaviour_t454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaManager_t602_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(429);
		GameObject_GetComponentsInChildren_TisCamera_t510_m2776_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484146);
		Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484143);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t27 * V_0 = {0};
	CameraU5BU5D_t600* V_1 = {0};
	bool V_2 = false;
	Camera_t510 * V_3 = {0};
	CameraU5BU5D_t600* V_4 = {0};
	int32_t V_5 = 0;
	MeshFilter_t597 * V_6 = {0};
	Mesh_t601 * V_7 = {0};
	Vector3U5BU5D_t8* V_8 = {0};
	Int32U5BU5D_t335* V_9 = {0};
	int32_t V_10 = 0;
	Vector3_t6  V_11 = {0};
	Vector3_t6  V_12 = {0};
	Vector3_t6  V_13 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t602_il2cpp_TypeInfo_var);
		VuforiaManager_t602 * L_0 = VuforiaManager_get_Instance_m2775(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t25 * L_1 = (Transform_t25 *)VirtFuncInvoker0< Transform_t25 * >::Invoke(8 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t27 * L_2 = Component_get_gameObject_m306(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t27 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t600* L_4 = GameObject_GetComponentsInChildren_TisCamera_t510_m2776(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t510_m2776_MethodInfo_var);
		V_1 = L_4;
		V_2 = 0;
		CameraU5BU5D_t600* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t600* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = (*(Camera_t510 **)(Camera_t510 **)SZArrayLdElema(L_6, L_8, sizeof(Camera_t510 *)));
		Camera_t510 * L_9 = Camera_get_current_m2777(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t510 * L_10 = V_3;
		bool L_11 = Object_op_Equality_m427(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = 1;
	}

IL_003c:
	{
		int32_t L_12 = V_5;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_5;
		CameraU5BU5D_t600* L_14 = V_4;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_15 = V_2;
		if (L_15)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_16 = (__this->___ShowLines_2);
		if (L_16)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t597 * L_17 = Component_GetComponent_TisMeshFilter_t597_m2760(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var);
		V_6 = L_17;
		MeshFilter_t597 * L_18 = V_6;
		bool L_19 = Object_op_Implicit_m301(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t45 * L_20 = (__this->___mLineMaterial_1);
		bool L_21 = Object_op_Equality_m427(NULL /*static, unused*/, L_20, (Object_t53 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_008c;
		}
	}
	{
		WireframeBehaviour_CreateLineMaterial_m2124(__this, /*hidden argument*/NULL);
	}

IL_008c:
	{
		MeshFilter_t597 * L_22 = V_6;
		NullCheck(L_22);
		Mesh_t601 * L_23 = MeshFilter_get_sharedMesh_m2778(L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		Mesh_t601 * L_24 = V_7;
		NullCheck(L_24);
		Vector3U5BU5D_t8* L_25 = Mesh_get_vertices_m2779(L_24, /*hidden argument*/NULL);
		V_8 = L_25;
		Mesh_t601 * L_26 = V_7;
		NullCheck(L_26);
		Int32U5BU5D_t335* L_27 = Mesh_get_triangles_m2780(L_26, /*hidden argument*/NULL);
		V_9 = L_27;
		GL_PushMatrix_m2781(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t25 * L_28 = Component_get_transform_m416(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Matrix4x4_t603  L_29 = Transform_get_localToWorldMatrix_m2782(L_28, /*hidden argument*/NULL);
		GL_MultMatrix_m2783(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t45 * L_30 = (__this->___mLineMaterial_1);
		NullCheck(L_30);
		Material_SetPass_m2784(L_30, 0, /*hidden argument*/NULL);
		GL_Begin_m2785(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_0144;
	}

IL_00d7:
	{
		Vector3U5BU5D_t8* L_31 = V_8;
		Int32U5BU5D_t335* L_32 = V_9;
		int32_t L_33 = V_10;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		int32_t L_34 = L_33;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34, sizeof(int32_t))));
		V_11 = (*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34, sizeof(int32_t))), sizeof(Vector3_t6 ))));
		Vector3U5BU5D_t8* L_35 = V_8;
		Int32U5BU5D_t335* L_36 = V_9;
		int32_t L_37 = V_10;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38, sizeof(int32_t))));
		V_12 = (*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38, sizeof(int32_t))), sizeof(Vector3_t6 ))));
		Vector3U5BU5D_t8* L_39 = V_8;
		Int32U5BU5D_t335* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)((int32_t)L_41+(int32_t)2)));
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)2));
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42, sizeof(int32_t))));
		V_13 = (*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42, sizeof(int32_t))), sizeof(Vector3_t6 ))));
		Vector3_t6  L_43 = V_11;
		GL_Vertex_m2786(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		Vector3_t6  L_44 = V_12;
		GL_Vertex_m2786(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		Vector3_t6  L_45 = V_12;
		GL_Vertex_m2786(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Vector3_t6  L_46 = V_13;
		GL_Vertex_m2786(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		Vector3_t6  L_47 = V_13;
		GL_Vertex_m2786(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Vector3_t6  L_48 = V_11;
		GL_Vertex_m2786(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		int32_t L_49 = V_10;
		V_10 = ((int32_t)((int32_t)L_49+(int32_t)3));
	}

IL_0144:
	{
		int32_t L_50 = V_10;
		Int32U5BU5D_t335* L_51 = V_9;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_51)->max_length)))))))
		{
			goto IL_00d7;
		}
	}
	{
		GL_End_m2787(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m2788(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var;
extern "C" void WireframeBehaviour_OnDrawGizmos_m2126 (WireframeBehaviour_t454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484143);
		s_Il2CppMethodIntialized = true;
	}
	MeshFilter_t597 * V_0 = {0};
	Mesh_t601 * V_1 = {0};
	Vector3U5BU5D_t8* V_2 = {0};
	Int32U5BU5D_t335* V_3 = {0};
	int32_t V_4 = 0;
	Vector3_t6  V_5 = {0};
	Vector3_t6  V_6 = {0};
	Vector3_t6  V_7 = {0};
	{
		bool L_0 = (__this->___ShowLines_2);
		if (!L_0)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m2789(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ed;
		}
	}
	{
		MeshFilter_t597 * L_2 = Component_GetComponent_TisMeshFilter_t597_m2760(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t597_m2760_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t597 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m301(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t27 * L_5 = Component_get_gameObject_m306(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t25 * L_6 = GameObject_get_transform_m305(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t6  L_7 = Transform_get_position_m334(L_6, /*hidden argument*/NULL);
		GameObject_t27 * L_8 = Component_get_gameObject_m306(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t25 * L_9 = GameObject_get_transform_m305(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t51  L_10 = Transform_get_rotation_m2607(L_9, /*hidden argument*/NULL);
		GameObject_t27 * L_11 = Component_get_gameObject_m306(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t25 * L_12 = GameObject_get_transform_m305(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t6  L_13 = Transform_get_lossyScale_m2790(L_12, /*hidden argument*/NULL);
		Matrix4x4_t603  L_14 = Matrix4x4_TRS_m2791(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m2792(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t5  L_15 = (__this->___LineColor_3);
		Gizmos_set_color_m418(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t597 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t601 * L_17 = MeshFilter_get_sharedMesh_m2778(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t601 * L_18 = V_1;
		NullCheck(L_18);
		Vector3U5BU5D_t8* L_19 = Mesh_get_vertices_m2779(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Mesh_t601 * L_20 = V_1;
		NullCheck(L_20);
		Int32U5BU5D_t335* L_21 = Mesh_get_triangles_m2780(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		V_4 = 0;
		goto IL_00e3;
	}

IL_008b:
	{
		Vector3U5BU5D_t8* L_22 = V_2;
		Int32U5BU5D_t335* L_23 = V_3;
		int32_t L_24 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25, sizeof(int32_t))));
		V_5 = (*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25, sizeof(int32_t))), sizeof(Vector3_t6 ))));
		Vector3U5BU5D_t8* L_26 = V_2;
		Int32U5BU5D_t335* L_27 = V_3;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)L_28+(int32_t)1)));
		int32_t L_29 = ((int32_t)((int32_t)L_28+(int32_t)1));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29, sizeof(int32_t))));
		V_6 = (*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29, sizeof(int32_t))), sizeof(Vector3_t6 ))));
		Vector3U5BU5D_t8* L_30 = V_2;
		Int32U5BU5D_t335* L_31 = V_3;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)L_32+(int32_t)2)));
		int32_t L_33 = ((int32_t)((int32_t)L_32+(int32_t)2));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33, sizeof(int32_t))));
		V_7 = (*(Vector3_t6 *)((Vector3_t6 *)(Vector3_t6 *)SZArrayLdElema(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33, sizeof(int32_t))), sizeof(Vector3_t6 ))));
		Vector3_t6  L_34 = V_5;
		Vector3_t6  L_35 = V_6;
		Gizmos_DrawLine_m419(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		Vector3_t6  L_36 = V_6;
		Vector3_t6  L_37 = V_7;
		Gizmos_DrawLine_m419(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t6  L_38 = V_7;
		Vector3_t6  L_39 = V_5;
		Gizmos_DrawLine_m419(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)3));
	}

IL_00e3:
	{
		int32_t L_41 = V_4;
		Int32U5BU5D_t335* L_42 = V_3;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))))))
		{
			goto IL_008b;
		}
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C" void WireframeTrackableEventHandler__ctor_m2127 (WireframeTrackableEventHandler_t455 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t410_m2713_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_Start_m2128 (WireframeTrackableEventHandler_t455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t410_m2713_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484123);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t410 * L_0 = Component_GetComponent_TisTrackableBehaviour_t410_m2713(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t410_m2713_MethodInfo_var);
		__this->___mTrackableBehaviour_1 = L_0;
		TrackableBehaviour_t410 * L_1 = (__this->___mTrackableBehaviour_1);
		bool L_2 = Object_op_Implicit_m301(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t410 * L_3 = (__this->___mTrackableBehaviour_1);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m2714(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void WireframeTrackableEventHandler_OnTrackableStateChanged_m2129 (WireframeTrackableEventHandler_t455 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m2130(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m2131(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral825;
extern Il2CppCodeGenString* _stringLiteral826;
extern "C" void WireframeTrackableEventHandler_OnTrackingFound_m2130 (WireframeTrackableEventHandler_t455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484125);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484147);
		_stringLiteral825 = il2cpp_codegen_string_literal_from_index(825);
		_stringLiteral826 = il2cpp_codegen_string_literal_from_index(826);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t587* V_0 = {0};
	ColliderU5BU5D_t588* V_1 = {0};
	WireframeBehaviourU5BU5D_t604* V_2 = {0};
	Renderer_t46 * V_3 = {0};
	RendererU5BU5D_t587* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t316 * V_6 = {0};
	ColliderU5BU5D_t588* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t454 * V_9 = {0};
	WireframeBehaviourU5BU5D_t604* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t587* L_0 = Component_GetComponentsInChildren_TisRenderer_t46_m2715(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t588* L_1 = Component_GetComponentsInChildren_TisCollider_t316_m2716(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t604* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t587* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t587* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t46 **)(Renderer_t46 **)SZArrayLdElema(L_4, L_6, sizeof(Renderer_t46 *)));
		Renderer_t46 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m2324(L_7, 1, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t587* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t588* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t588* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t316 **)(Collider_t316 **)SZArrayLdElema(L_12, L_14, sizeof(Collider_t316 *)));
		Collider_t316 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m2717(L_15, 1, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t588* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t604* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t604* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t454 **)(WireframeBehaviour_t454 **)SZArrayLdElema(L_20, L_22, sizeof(WireframeBehaviour_t454 *)));
		WireframeBehaviour_t454 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m408(L_23, 1, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t604* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t410 * L_27 = (__this->___mTrackableBehaviour_1);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m2152(NULL /*static, unused*/, _stringLiteral825, L_28, _stringLiteral826, /*hidden argument*/NULL);
		Debug_Log_m2193(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral825;
extern Il2CppCodeGenString* _stringLiteral827;
extern "C" void WireframeTrackableEventHandler_OnTrackingLost_m2131 (WireframeTrackableEventHandler_t455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484125);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484147);
		_stringLiteral825 = il2cpp_codegen_string_literal_from_index(825);
		_stringLiteral827 = il2cpp_codegen_string_literal_from_index(827);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t587* V_0 = {0};
	ColliderU5BU5D_t588* V_1 = {0};
	WireframeBehaviourU5BU5D_t604* V_2 = {0};
	Renderer_t46 * V_3 = {0};
	RendererU5BU5D_t587* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t316 * V_6 = {0};
	ColliderU5BU5D_t588* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t454 * V_9 = {0};
	WireframeBehaviourU5BU5D_t604* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t587* L_0 = Component_GetComponentsInChildren_TisRenderer_t46_m2715(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t46_m2715_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t588* L_1 = Component_GetComponentsInChildren_TisCollider_t316_m2716(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t316_m2716_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t604* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t454_m2793_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t587* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t587* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t46 **)(Renderer_t46 **)SZArrayLdElema(L_4, L_6, sizeof(Renderer_t46 *)));
		Renderer_t46 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m2324(L_7, 0, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t587* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t588* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t588* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t316 **)(Collider_t316 **)SZArrayLdElema(L_12, L_14, sizeof(Collider_t316 *)));
		Collider_t316 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m2717(L_15, 0, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t588* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t604* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t604* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t454 **)(WireframeBehaviour_t454 **)SZArrayLdElema(L_20, L_22, sizeof(WireframeBehaviour_t454 *)));
		WireframeBehaviour_t454 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m408(L_23, 0, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t604* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t410 * L_27 = (__this->___mTrackableBehaviour_1);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m2152(NULL /*static, unused*/, _stringLiteral825, L_28, _stringLiteral827, /*hidden argument*/NULL);
		Debug_Log_m2193(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordBehaviour::.ctor()
extern "C" void WordBehaviour__ctor_m2132 (WordBehaviour_t456 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m2794(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnionAssets.FLE.EventHandlerFunction::.ctor(System.Object,System.IntPtr)
extern "C" void EventHandlerFunction__ctor_m2133 (EventHandlerFunction_t458 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnionAssets.FLE.EventHandlerFunction::Invoke()
extern "C" void EventHandlerFunction_Invoke_m2134 (EventHandlerFunction_t458 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		EventHandlerFunction_Invoke_m2134((EventHandlerFunction_t458 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_EventHandlerFunction_t458(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnionAssets.FLE.EventHandlerFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * EventHandlerFunction_BeginInvoke_m2135 (EventHandlerFunction_t458 * __this, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnionAssets.FLE.EventHandlerFunction::EndInvoke(System.IAsyncResult)
extern "C" void EventHandlerFunction_EndInvoke_m2136 (EventHandlerFunction_t458 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnionAssets.FLE.DataEventHandlerFunction::.ctor(System.Object,System.IntPtr)
extern "C" void DataEventHandlerFunction__ctor_m2137 (DataEventHandlerFunction_t459 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnionAssets.FLE.DataEventHandlerFunction::Invoke(UnionAssets.FLE.CEvent)
extern "C" void DataEventHandlerFunction_Invoke_m2138 (DataEventHandlerFunction_t459 * __this, CEvent_t74 * ___e, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DataEventHandlerFunction_Invoke_m2138((DataEventHandlerFunction_t459 *)__this->___prev_9,___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, CEvent_t74 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, CEvent_t74 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_DataEventHandlerFunction_t459(Il2CppObject* delegate, CEvent_t74 * ___e)
{
	// Marshaling of parameter '___e' to native representation
	CEvent_t74 * ____e_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnionAssets.FLE.CEvent'."));
}
// System.IAsyncResult UnionAssets.FLE.DataEventHandlerFunction::BeginInvoke(UnionAssets.FLE.CEvent,System.AsyncCallback,System.Object)
extern "C" Object_t * DataEventHandlerFunction_BeginInvoke_m2139 (DataEventHandlerFunction_t459 * __this, CEvent_t74 * ___e, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___e;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnionAssets.FLE.DataEventHandlerFunction::EndInvoke(System.IAsyncResult)
extern "C" void DataEventHandlerFunction_EndInvoke_m2140 (DataEventHandlerFunction_t459 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void Facebook.Unity.InitDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void InitDelegate__ctor_m2141 (InitDelegate_t241 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Facebook.Unity.InitDelegate::Invoke()
extern "C" void InitDelegate_Invoke_m2142 (InitDelegate_t241 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		InitDelegate_Invoke_m2142((InitDelegate_t241 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_InitDelegate_t241(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult Facebook.Unity.InitDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * InitDelegate_BeginInvoke_m2143 (InitDelegate_t241 * __this, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Facebook.Unity.InitDelegate::EndInvoke(System.IAsyncResult)
extern "C" void InitDelegate_EndInvoke_m2144 (InitDelegate_t241 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void Facebook.Unity.HideUnityDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void HideUnityDelegate__ctor_m2145 (HideUnityDelegate_t242 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Facebook.Unity.HideUnityDelegate::Invoke(System.Boolean)
extern "C" void HideUnityDelegate_Invoke_m2146 (HideUnityDelegate_t242 * __this, bool ___isUnityShown, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		HideUnityDelegate_Invoke_m2146((HideUnityDelegate_t242 *)__this->___prev_9,___isUnityShown, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ___isUnityShown, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___isUnityShown,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___isUnityShown, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___isUnityShown,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_HideUnityDelegate_t242(Il2CppObject* delegate, bool ___isUnityShown)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___isUnityShown' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___isUnityShown);

	// Marshaling cleanup of parameter '___isUnityShown' native representation

}
// System.IAsyncResult Facebook.Unity.HideUnityDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t41_il2cpp_TypeInfo_var;
extern "C" Object_t * HideUnityDelegate_BeginInvoke_m2147 (HideUnityDelegate_t242 * __this, bool ___isUnityShown, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t41_il2cpp_TypeInfo_var, &___isUnityShown);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Facebook.Unity.HideUnityDelegate::EndInvoke(System.IAsyncResult)
extern "C" void HideUnityDelegate_EndInvoke_m2148 (HideUnityDelegate_t242 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
