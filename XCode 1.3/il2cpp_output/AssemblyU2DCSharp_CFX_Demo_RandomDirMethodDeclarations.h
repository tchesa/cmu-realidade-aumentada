﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_RandomDir
struct CFX_Demo_RandomDir_t321;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_RandomDir::.ctor()
extern "C" void CFX_Demo_RandomDir__ctor_m1756 (CFX_Demo_RandomDir_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RandomDir::Awake()
extern "C" void CFX_Demo_RandomDir_Awake_m1757 (CFX_Demo_RandomDir_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
