﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BasePackage
struct BasePackage_t199;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void BasePackage::.ctor()
extern "C" void BasePackage__ctor_m1118 (BasePackage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BasePackage::initWriter()
extern "C" void BasePackage_initWriter_m1119 (BasePackage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] BasePackage::getBytes()
extern "C" ByteU5BU5D_t119* BasePackage_getBytes_m1120 (BasePackage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BasePackage::send()
extern "C" void BasePackage_send_m1121 (BasePackage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BasePackage::writeInt(System.Int32)
extern "C" void BasePackage_writeInt_m1122 (BasePackage_t199 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BasePackage::writeString(System.String)
extern "C" void BasePackage_writeString_m1123 (BasePackage_t199 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BasePackage::writeFloat(System.Single)
extern "C" void BasePackage_writeFloat_m1124 (BasePackage_t199 * __this, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
