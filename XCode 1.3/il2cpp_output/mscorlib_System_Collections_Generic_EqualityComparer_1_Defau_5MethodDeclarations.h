﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>
struct DefaultComparer_t3073;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void DefaultComparer__ctor_m22881_gshared (DefaultComparer_t3073 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22881(__this, method) (( void (*) (DefaultComparer_t3073 *, const MethodInfo*))DefaultComparer__ctor_m22881_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22882_gshared (DefaultComparer_t3073 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22882(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3073 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m22882_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22883_gshared (DefaultComparer_t3073 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m22883(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3073 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m22883_gshared)(__this, ___x, ___y, method)
