﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_MobileFacebook.h"

// Facebook.Unity.Mobile.IOS.IOSFacebook
struct  IOSFacebook_t261  : public MobileFacebook_t250
{
	// System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::limitEventUsage
	bool ___limitEventUsage_6;
};
