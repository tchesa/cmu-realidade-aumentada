﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<IOSImagePickResult>
struct Action_1_t147;
// System.Action`1<ISN_Result>
struct Action_1_t99;

#include "AssemblyU2DCSharp_ISN_Singleton_1_gen_0.h"

// IOSCamera
struct  IOSCamera_t145  : public ISN_Singleton_1_t146
{
	// System.Action`1<IOSImagePickResult> IOSCamera::OnImagePicked
	Action_1_t147 * ___OnImagePicked_7;
	// System.Action`1<ISN_Result> IOSCamera::OnImageSaved
	Action_1_t99 * ___OnImageSaved_8;
};
struct IOSCamera_t145_StaticFields{
	// System.Action`1<IOSImagePickResult> IOSCamera::<>f__am$cache2
	Action_1_t147 * ___U3CU3Ef__amU24cache2_9;
	// System.Action`1<ISN_Result> IOSCamera::<>f__am$cache3
	Action_1_t99 * ___U3CU3Ef__amU24cache3_10;
};
