﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_2.h"

// System.Void System.Nullable`1<System.Single>::.ctor(T)
extern "C" void Nullable_1__ctor_m18342_gshared (Nullable_1_t474 * __this, float ___value, const MethodInfo* method);
#define Nullable_1__ctor_m18342(__this, ___value, method) (( void (*) (Nullable_1_t474 *, float, const MethodInfo*))Nullable_1__ctor_m18342_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m2449_gshared (Nullable_1_t474 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2449(__this, method) (( bool (*) (Nullable_1_t474 *, const MethodInfo*))Nullable_1_get_HasValue_m2449_gshared)(__this, method)
// T System.Nullable`1<System.Single>::get_Value()
extern "C" float Nullable_1_get_Value_m2450_gshared (Nullable_1_t474 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2450(__this, method) (( float (*) (Nullable_1_t474 *, const MethodInfo*))Nullable_1_get_Value_m2450_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m18343_gshared (Nullable_1_t474 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18343(__this, ___other, method) (( bool (*) (Nullable_1_t474 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m18343_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m18344_gshared (Nullable_1_t474 * __this, Nullable_1_t474  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18344(__this, ___other, method) (( bool (*) (Nullable_1_t474 *, Nullable_1_t474 , const MethodInfo*))Nullable_1_Equals_m18344_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.Single>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m18345_gshared (Nullable_1_t474 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m18345(__this, method) (( int32_t (*) (Nullable_1_t474 *, const MethodInfo*))Nullable_1_GetHashCode_m18345_gshared)(__this, method)
// T System.Nullable`1<System.Single>::GetValueOrDefault()
extern "C" float Nullable_1_GetValueOrDefault_m18346_gshared (Nullable_1_t474 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m18346(__this, method) (( float (*) (Nullable_1_t474 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m18346_gshared)(__this, method)
// System.String System.Nullable`1<System.Single>::ToString()
extern "C" String_t* Nullable_1_ToString_m18347_gshared (Nullable_1_t474 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m18347(__this, method) (( String_t* (*) (Nullable_1_t474 *, const MethodInfo*))Nullable_1_ToString_m18347_gshared)(__this, method)
