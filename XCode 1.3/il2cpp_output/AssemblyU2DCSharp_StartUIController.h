﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ComponentTracker
struct ComponentTracker_t67;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// StartUIController
struct  StartUIController_t390  : public MonoBehaviour_t18
{
	// System.Boolean StartUIController::info
	bool ___info_1;
	// ComponentTracker StartUIController::tracker
	ComponentTracker_t67 * ___tracker_2;
	// UnityEngine.Vector3 StartUIController::infoShowedPosition
	Vector3_t6  ___infoShowedPosition_3;
	// UnityEngine.Vector3 StartUIController::infoHiddenPosition
	Vector3_t6  ___infoHiddenPosition_4;
	// System.Single StartUIController::infoSpeed
	float ___infoSpeed_5;
	// System.Boolean StartUIController::infoDrag
	bool ___infoDrag_6;
};
