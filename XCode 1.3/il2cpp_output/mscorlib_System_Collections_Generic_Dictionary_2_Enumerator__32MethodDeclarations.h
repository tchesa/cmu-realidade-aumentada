﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3149;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m24271_gshared (Enumerator_t3156 * __this, Dictionary_2_t3149 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m24271(__this, ___dictionary, method) (( void (*) (Enumerator_t3156 *, Dictionary_2_t3149 *, const MethodInfo*))Enumerator__ctor_m24271_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24272_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24272(__this, method) (( Object_t * (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24272_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m24273_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m24273(__this, method) (( void (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24273_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t58  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24274_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24274(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24274_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24275_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24275(__this, method) (( Object_t * (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24275_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24276_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24276(__this, method) (( Object_t * (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24276_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24277_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24277(__this, method) (( bool (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_MoveNext_m24277_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" KeyValuePair_2_t3151  Enumerator_get_Current_m24278_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m24278(__this, method) (( KeyValuePair_2_t3151  (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_get_Current_m24278_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m24279_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m24279(__this, method) (( Object_t * (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_get_CurrentKey_m24279_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentValue()
extern "C" uint16_t Enumerator_get_CurrentValue_m24280_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m24280(__this, method) (( uint16_t (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_get_CurrentValue_m24280_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Reset()
extern "C" void Enumerator_Reset_m24281_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_Reset_m24281(__this, method) (( void (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_Reset_m24281_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyState()
extern "C" void Enumerator_VerifyState_m24282_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m24282(__this, method) (( void (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_VerifyState_m24282_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m24283_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m24283(__this, method) (( void (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_VerifyCurrent_m24283_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m24284_gshared (Enumerator_t3156 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m24284(__this, method) (( void (*) (Enumerator_t3156 *, const MethodInfo*))Enumerator_Dispose_m24284_gshared)(__this, method)
