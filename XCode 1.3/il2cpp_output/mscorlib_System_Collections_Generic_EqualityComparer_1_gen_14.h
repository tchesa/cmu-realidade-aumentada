﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct EqualityComparer_1_t3490;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct  EqualityComparer_1_t3490  : public Object_t
{
};
struct EqualityComparer_1_t3490_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Boolean>::_default
	EqualityComparer_1_t3490 * ____default_0;
};
