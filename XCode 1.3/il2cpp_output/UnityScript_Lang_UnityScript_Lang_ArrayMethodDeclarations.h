﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityScript.Lang.Array
struct Array_t608;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityScript.Lang.Array::.ctor()
extern "C" void Array__ctor_m2806 (Array_t608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityScript.Lang.Array::push(System.Object)
extern "C" int32_t Array_push_m2807 (Array_t608 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityScript.Lang.Array::ToString()
extern "C" String_t* Array_ToString_m8208 (Array_t608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityScript.Lang.Array::Join(System.String)
extern "C" String_t* Array_Join_m8209 (Array_t608 * __this, String_t* ___seperator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityScript.Lang.Array::join(System.String)
extern "C" String_t* Array_join_m2808 (Array_t608 * __this, String_t* ___seperator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.Array::OnValidate(System.Object)
extern "C" void Array_OnValidate_m8210 (Array_t608 * __this, Object_t * ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
