﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iCloudManager
struct iCloudManager_t180;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t119;
// ISN_Result
struct ISN_Result_t114;
// iCloudData
struct iCloudData_t179;

#include "codegen/il2cpp-codegen.h"

// System.Void iCloudManager::.ctor()
extern "C" void iCloudManager__ctor_m1015 (iCloudManager_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_initCloud()
extern "C" void iCloudManager__initCloud_m1016 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_setString(System.String,System.String)
extern "C" void iCloudManager__setString_m1017 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_setDouble(System.String,System.Single)
extern "C" void iCloudManager__setDouble_m1018 (Object_t * __this /* static, unused */, String_t* ___key, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_setData(System.String,System.String)
extern "C" void iCloudManager__setData_m1019 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_requestDataForKey(System.String)
extern "C" void iCloudManager__requestDataForKey_m1020 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::Awake()
extern "C" void iCloudManager_Awake_m1021 (iCloudManager_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::init()
extern "C" void iCloudManager_init_m1022 (iCloudManager_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::setString(System.String,System.String)
extern "C" void iCloudManager_setString_m1023 (iCloudManager_t180 * __this, String_t* ___key, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::setFloat(System.String,System.Single)
extern "C" void iCloudManager_setFloat_m1024 (iCloudManager_t180 * __this, String_t* ___key, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::setData(System.String,System.Byte[])
extern "C" void iCloudManager_setData_m1025 (iCloudManager_t180 * __this, String_t* ___key, ByteU5BU5D_t119* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::requestDataForKey(System.String)
extern "C" void iCloudManager_requestDataForKey_m1026 (iCloudManager_t180 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudInit()
extern "C" void iCloudManager_OnCloudInit_m1027 (iCloudManager_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudInitFail()
extern "C" void iCloudManager_OnCloudInitFail_m1028 (iCloudManager_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudDataChanged()
extern "C" void iCloudManager_OnCloudDataChanged_m1029 (iCloudManager_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudData(System.String)
extern "C" void iCloudManager_OnCloudData_m1030 (iCloudManager_t180 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudDataEmpty(System.String)
extern "C" void iCloudManager_OnCloudDataEmpty_m1031 (iCloudManager_t180 * __this, String_t* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::<OnCloundInitAction>m__2F(ISN_Result)
extern "C" void iCloudManager_U3COnCloundInitActionU3Em__2F_m1032 (Object_t * __this /* static, unused */, ISN_Result_t114 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::<OnCloundDataChangedAction>m__30()
extern "C" void iCloudManager_U3COnCloundDataChangedActionU3Em__30_m1033 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::<OnCloudDataReceivedAction>m__31(iCloudData)
extern "C" void iCloudManager_U3COnCloudDataReceivedActionU3Em__31_m1034 (Object_t * __this /* static, unused */, iCloudData_t179 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
