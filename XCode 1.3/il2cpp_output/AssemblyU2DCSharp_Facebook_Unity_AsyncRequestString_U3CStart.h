﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IEnumerator_1_t291;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t288;
// UnityEngine.WWW
struct WWW_t289;
// System.Object
struct Object_t;
// Facebook.Unity.AsyncRequestString
struct AsyncRequestString_t290;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// Facebook.Unity.AsyncRequestString/<Start>c__Iterator6
struct  U3CStartU3Ec__Iterator6_t286  : public Object_t
{
	// System.String Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::<urlParams>__0
	String_t* ___U3CurlParamsU3E__0_0;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::<$s_39>__1
	Object_t* ___U3CU24s_39U3E__1_1;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::<pair>__2
	KeyValuePair_2_t287  ___U3CpairU3E__2_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::<headers>__3
	Dictionary_2_t288 * ___U3CheadersU3E__3_3;
	// UnityEngine.WWW Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::<www>__4
	WWW_t289 * ___U3CwwwU3E__4_4;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::<$s_40>__5
	Object_t* ___U3CU24s_40U3E__5_5;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::<pair>__6
	KeyValuePair_2_t287  ___U3CpairU3E__6_6;
	// System.Int32 Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::$PC
	int32_t ___U24PC_7;
	// System.Object Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::$current
	Object_t * ___U24current_8;
	// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString/<Start>c__Iterator6::<>f__this
	AsyncRequestString_t290 * ___U3CU3Ef__this_9;
};
