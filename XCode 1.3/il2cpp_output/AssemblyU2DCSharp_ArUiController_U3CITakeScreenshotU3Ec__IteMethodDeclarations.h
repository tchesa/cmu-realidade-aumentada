﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ArUiController/<ITakeScreenshot>c__Iterator17
struct U3CITakeScreenshotU3Ec__Iterator17_t375;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ArUiController/<ITakeScreenshot>c__Iterator17::.ctor()
extern "C" void U3CITakeScreenshotU3Ec__Iterator17__ctor_m1902 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ArUiController/<ITakeScreenshot>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CITakeScreenshotU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1903 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ArUiController/<ITakeScreenshot>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CITakeScreenshotU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m1904 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ArUiController/<ITakeScreenshot>c__Iterator17::MoveNext()
extern "C" bool U3CITakeScreenshotU3Ec__Iterator17_MoveNext_m1905 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController/<ITakeScreenshot>c__Iterator17::Dispose()
extern "C" void U3CITakeScreenshotU3Ec__Iterator17_Dispose_m1906 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArUiController/<ITakeScreenshot>c__Iterator17::Reset()
extern "C" void U3CITakeScreenshotU3Ec__Iterator17_Reset_m1907 (U3CITakeScreenshotU3Ec__Iterator17_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
