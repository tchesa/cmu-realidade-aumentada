﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARAnimation/TransparencyTrigger[]
struct TransparencyTriggerU5BU5D_t346;
// System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger>
struct List_1_t347;
// ARAnimation/TrailTrigger[]
struct TrailTriggerU5BU5D_t348;
// System.Collections.Generic.List`1<ARAnimation/TrailTrigger>
struct List_1_t349;
// ARAnimation/ShineTrigger[]
struct ShineTriggerU5BU5D_t350;
// System.Collections.Generic.List`1<ARAnimation/ShineTrigger>
struct List_1_t351;
// System.Comparison`1<ARAnimation/TransparencyTrigger>
struct Comparison_1_t352;
// System.Comparison`1<ARAnimation/TrailTrigger>
struct Comparison_1_t353;
// System.Comparison`1<ARAnimation/ShineTrigger>
struct Comparison_1_t354;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// ARAnimation
struct  ARAnimation_t345  : public MonoBehaviour_t18
{
	// ARAnimation/TransparencyTrigger[] ARAnimation::showingObjects
	TransparencyTriggerU5BU5D_t346* ___showingObjects_1;
	// System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger> ARAnimation::showingList
	List_1_t347 * ___showingList_2;
	// System.Collections.Generic.List`1<ARAnimation/TransparencyTrigger> ARAnimation::hidingList
	List_1_t347 * ___hidingList_3;
	// ARAnimation/TrailTrigger[] ARAnimation::trails
	TrailTriggerU5BU5D_t348* ___trails_4;
	// System.Collections.Generic.List`1<ARAnimation/TrailTrigger> ARAnimation::trailList
	List_1_t349 * ___trailList_5;
	// ARAnimation/ShineTrigger[] ARAnimation::shines
	ShineTriggerU5BU5D_t350* ___shines_6;
	// System.Collections.Generic.List`1<ARAnimation/ShineTrigger> ARAnimation::shineList
	List_1_t351 * ___shineList_7;
	// System.Boolean ARAnimation::isPlaying
	bool ___isPlaying_8;
	// System.Single ARAnimation::startTime
	float ___startTime_9;
	// System.Boolean ARAnimation::play
	bool ___play_10;
	// System.Boolean ARAnimation::stop
	bool ___stop_11;
};
struct ARAnimation_t345_StaticFields{
	// System.Comparison`1<ARAnimation/TransparencyTrigger> ARAnimation::<>f__am$cacheB
	Comparison_1_t352 * ___U3CU3Ef__amU24cacheB_12;
	// System.Comparison`1<ARAnimation/TransparencyTrigger> ARAnimation::<>f__am$cacheC
	Comparison_1_t352 * ___U3CU3Ef__amU24cacheC_13;
	// System.Comparison`1<ARAnimation/TrailTrigger> ARAnimation::<>f__am$cacheD
	Comparison_1_t353 * ___U3CU3Ef__amU24cacheD_14;
	// System.Comparison`1<ARAnimation/ShineTrigger> ARAnimation::<>f__am$cacheE
	Comparison_1_t354 * ___U3CU3Ef__amU24cacheE_15;
};
