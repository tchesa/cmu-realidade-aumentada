﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Vuforia.VirtualButton>
struct List_1_t1135;
// Vuforia.VirtualButton
struct VirtualButton_t1061;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>
struct  Enumerator_t3052 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::l
	List_1_t1135 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::current
	VirtualButton_t1061 * ___current_3;
};
