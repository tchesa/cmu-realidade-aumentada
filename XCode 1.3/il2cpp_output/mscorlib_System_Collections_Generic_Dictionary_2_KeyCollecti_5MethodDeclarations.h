﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2604;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15870_gshared (Enumerator_t2612 * __this, Dictionary_2_t2604 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15870(__this, ___host, method) (( void (*) (Enumerator_t2612 *, Dictionary_2_t2604 *, const MethodInfo*))Enumerator__ctor_m15870_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15871_gshared (Enumerator_t2612 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15871(__this, method) (( Object_t * (*) (Enumerator_t2612 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15871_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15872_gshared (Enumerator_t2612 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15872(__this, method) (( void (*) (Enumerator_t2612 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15872_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m15873_gshared (Enumerator_t2612 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15873(__this, method) (( void (*) (Enumerator_t2612 *, const MethodInfo*))Enumerator_Dispose_m15873_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15874_gshared (Enumerator_t2612 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15874(__this, method) (( bool (*) (Enumerator_t2612 *, const MethodInfo*))Enumerator_MoveNext_m15874_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15875_gshared (Enumerator_t2612 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15875(__this, method) (( Object_t * (*) (Enumerator_t2612 *, const MethodInfo*))Enumerator_get_Current_m15875_gshared)(__this, method)
