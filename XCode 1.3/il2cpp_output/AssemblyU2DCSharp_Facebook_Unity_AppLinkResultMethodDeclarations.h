﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AppLinkResult
struct AppLinkResult_t276;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t225;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.AppLinkResult::.ctor(System.String)
extern "C" void AppLinkResult__ctor_m1578 (AppLinkResult_t276 * __this, String_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppLinkResult::get_Url()
extern "C" String_t* AppLinkResult_get_Url_m1579 (AppLinkResult_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppLinkResult::set_Url(System.String)
extern "C" void AppLinkResult_set_Url_m1580 (AppLinkResult_t276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppLinkResult::get_TargetUrl()
extern "C" String_t* AppLinkResult_get_TargetUrl_m1581 (AppLinkResult_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppLinkResult::set_TargetUrl(System.String)
extern "C" void AppLinkResult_set_TargetUrl_m1582 (AppLinkResult_t276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppLinkResult::get_Ref()
extern "C" String_t* AppLinkResult_get_Ref_m1583 (AppLinkResult_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppLinkResult::set_Ref(System.String)
extern "C" void AppLinkResult_set_Ref_m1584 (AppLinkResult_t276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.AppLinkResult::get_Extras()
extern "C" Object_t* AppLinkResult_get_Extras_m1585 (AppLinkResult_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppLinkResult::set_Extras(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C" void AppLinkResult_set_Extras_m1586 (AppLinkResult_t276 * __this, Object_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
