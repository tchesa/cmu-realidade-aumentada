﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ARAnimation/ShineTrigger>
struct List_1_t351;
// ARAnimation/ShineTrigger
struct ShineTrigger_t343;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<ARAnimation/ShineTrigger>
struct  Enumerator_t2837 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<ARAnimation/ShineTrigger>::l
	List_1_t351 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<ARAnimation/ShineTrigger>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<ARAnimation/ShineTrigger>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<ARAnimation/ShineTrigger>::current
	ShineTrigger_t343 * ___current_3;
};
