﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t1059;
// Vuforia.StateManager
struct StateManager_t1042;

#include "codegen/il2cpp-codegen.h"

// Vuforia.StateManager Vuforia.TrackerManagerImpl::GetStateManager()
extern "C" StateManager_t1042 * TrackerManagerImpl_GetStateManager_m6111 (TrackerManagerImpl_t1059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManagerImpl::.ctor()
extern "C" void TrackerManagerImpl__ctor_m6112 (TrackerManagerImpl_t1059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
