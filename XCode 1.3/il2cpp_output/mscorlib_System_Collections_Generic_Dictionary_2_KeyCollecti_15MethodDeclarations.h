﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_7MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m17124(__this, ___dictionary, method) (( void (*) (KeyCollection_t2705 *, Dictionary_2_t105 *, const MethodInfo*))KeyCollection__ctor_m16125_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17125(__this, ___item, method) (( void (*) (KeyCollection_t2705 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16126_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17126(__this, method) (( void (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16127_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17127(__this, ___item, method) (( bool (*) (KeyCollection_t2705 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16128_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17128(__this, ___item, method) (( bool (*) (KeyCollection_t2705 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16129_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17129(__this, method) (( Object_t* (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16130_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m17130(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2705 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m16131_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17131(__this, method) (( Object_t * (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16132_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17132(__this, method) (( bool (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16133_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17133(__this, method) (( bool (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16134_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m17134(__this, method) (( Object_t * (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m16135_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m17135(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2705 *, StringU5BU5D_t260*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m16136_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::GetEnumerator()
#define KeyCollection_GetEnumerator_m17136(__this, method) (( Enumerator_t3626  (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_GetEnumerator_m16137_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GCLeaderboard>::get_Count()
#define KeyCollection_get_Count_m17137(__this, method) (( int32_t (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_get_Count_m16138_gshared)(__this, method)
