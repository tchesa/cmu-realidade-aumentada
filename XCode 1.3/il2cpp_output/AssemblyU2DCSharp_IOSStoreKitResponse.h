﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// IOSStoreKitError
struct IOSStoreKitError_t136;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_InAppPurchaseState.h"

// IOSStoreKitResponse
struct  IOSStoreKitResponse_t137  : public Object_t
{
	// System.String IOSStoreKitResponse::productIdentifier
	String_t* ___productIdentifier_0;
	// InAppPurchaseState IOSStoreKitResponse::state
	int32_t ___state_1;
	// System.String IOSStoreKitResponse::receipt
	String_t* ___receipt_2;
	// System.String IOSStoreKitResponse::transactionIdentifier
	String_t* ___transactionIdentifier_3;
	// IOSStoreKitError IOSStoreKitResponse::error
	IOSStoreKitError_t136 * ___error_4;
};
