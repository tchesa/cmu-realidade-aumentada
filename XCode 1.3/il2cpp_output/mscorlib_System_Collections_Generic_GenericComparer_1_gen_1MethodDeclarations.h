﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.Guid>
struct GenericComparer_1_t2573;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
extern "C" void GenericComparer_1__ctor_m15442_gshared (GenericComparer_1_t2573 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m15442(__this, method) (( void (*) (GenericComparer_1_t2573 *, const MethodInfo*))GenericComparer_1__ctor_m15442_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m30153_gshared (GenericComparer_1_t2573 * __this, Guid_t61  ___x, Guid_t61  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m30153(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2573 *, Guid_t61 , Guid_t61 , const MethodInfo*))GenericComparer_1_Compare_m30153_gshared)(__this, ___x, ___y, method)
