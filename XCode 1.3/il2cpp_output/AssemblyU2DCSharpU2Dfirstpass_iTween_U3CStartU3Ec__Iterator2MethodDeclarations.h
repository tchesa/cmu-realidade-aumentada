﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<Start>c__Iterator2
struct U3CStartU3Ec__Iterator2_t17;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<Start>c__Iterator2::.ctor()
extern "C" void U3CStartU3Ec__Iterator2__ctor_m23 (U3CStartU3Ec__Iterator2_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m24 (U3CStartU3Ec__Iterator2_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m25 (U3CStartU3Ec__Iterator2_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<Start>c__Iterator2::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator2_MoveNext_m26 (U3CStartU3Ec__Iterator2_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__Iterator2::Dispose()
extern "C" void U3CStartU3Ec__Iterator2_Dispose_m27 (U3CStartU3Ec__Iterator2_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__Iterator2::Reset()
extern "C" void U3CStartU3Ec__Iterator2_Reset_m28 (U3CStartU3Ec__Iterator2_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
