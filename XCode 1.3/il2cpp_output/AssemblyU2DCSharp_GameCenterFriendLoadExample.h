﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t184;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// GameCenterFriendLoadExample
struct  GameCenterFriendLoadExample_t188  : public MonoBehaviour_t18
{
	// System.String GameCenterFriendLoadExample::ChallengeLeaderboard
	String_t* ___ChallengeLeaderboard_1;
	// System.String GameCenterFriendLoadExample::ChallengeAchievement
	String_t* ___ChallengeAchievement_2;
	// UnityEngine.GUIStyle GameCenterFriendLoadExample::headerStyle
	GUIStyle_t184 * ___headerStyle_3;
	// UnityEngine.GUIStyle GameCenterFriendLoadExample::boardStyle
	GUIStyle_t184 * ___boardStyle_4;
	// System.Boolean GameCenterFriendLoadExample::renderFriendsList
	bool ___renderFriendsList_5;
};
