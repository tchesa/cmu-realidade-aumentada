﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t2920;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t554;
// System.Object[]
struct ObjectU5BU5D_t34;
// System.Predicate`1<System.Object>
struct Predicate_1_t2585;
// System.Comparison`1<System.Object>
struct Comparison_1_t2591;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m20584_gshared (IndexedSet_1_t2920 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m20584(__this, method) (( void (*) (IndexedSet_1_t2920 *, const MethodInfo*))IndexedSet_1__ctor_m20584_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20586_gshared (IndexedSet_1_t2920 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20586(__this, method) (( Object_t * (*) (IndexedSet_1_t2920 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20586_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m20588_gshared (IndexedSet_1_t2920 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m20588(__this, ___item, method) (( void (*) (IndexedSet_1_t2920 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m20588_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m20590_gshared (IndexedSet_1_t2920 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m20590(__this, ___item, method) (( bool (*) (IndexedSet_1_t2920 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m20590_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m20592_gshared (IndexedSet_1_t2920 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m20592(__this, method) (( Object_t* (*) (IndexedSet_1_t2920 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m20592_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m20594_gshared (IndexedSet_1_t2920 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m20594(__this, method) (( void (*) (IndexedSet_1_t2920 *, const MethodInfo*))IndexedSet_1_Clear_m20594_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m20596_gshared (IndexedSet_1_t2920 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m20596(__this, ___item, method) (( bool (*) (IndexedSet_1_t2920 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m20596_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m20598_gshared (IndexedSet_1_t2920 * __this, ObjectU5BU5D_t34* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m20598(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t2920 *, ObjectU5BU5D_t34*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m20598_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m20600_gshared (IndexedSet_1_t2920 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m20600(__this, method) (( int32_t (*) (IndexedSet_1_t2920 *, const MethodInfo*))IndexedSet_1_get_Count_m20600_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m20602_gshared (IndexedSet_1_t2920 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m20602(__this, method) (( bool (*) (IndexedSet_1_t2920 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m20602_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m20604_gshared (IndexedSet_1_t2920 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m20604(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t2920 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m20604_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m20606_gshared (IndexedSet_1_t2920 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m20606(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t2920 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m20606_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m20608_gshared (IndexedSet_1_t2920 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m20608(__this, ___index, method) (( void (*) (IndexedSet_1_t2920 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m20608_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m20610_gshared (IndexedSet_1_t2920 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m20610(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t2920 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m20610_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m20612_gshared (IndexedSet_1_t2920 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m20612(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t2920 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m20612_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m20613_gshared (IndexedSet_1_t2920 * __this, Predicate_1_t2585 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m20613(__this, ___match, method) (( void (*) (IndexedSet_1_t2920 *, Predicate_1_t2585 *, const MethodInfo*))IndexedSet_1_RemoveAll_m20613_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m20614_gshared (IndexedSet_1_t2920 * __this, Comparison_1_t2591 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m20614(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t2920 *, Comparison_1_t2591 *, const MethodInfo*))IndexedSet_1_Sort_m20614_gshared)(__this, ___sortLayoutFunction, method)
