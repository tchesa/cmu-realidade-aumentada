﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
struct Dictionary_2_t958;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTracker.h"

// Vuforia.MarkerTrackerImpl
struct  MarkerTrackerImpl_t957  : public MarkerTracker_t956
{
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker> Vuforia.MarkerTrackerImpl::mMarkerDict
	Dictionary_2_t958 * ___mMarkerDict_1;
};
