﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_t326;
// System.Collections.IEnumerator
struct IEnumerator_t35;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_AutoDestructShuriken::.ctor()
extern "C" void CFX_AutoDestructShuriken__ctor_m1772 (CFX_AutoDestructShuriken_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoDestructShuriken::OnEnable()
extern "C" void CFX_AutoDestructShuriken_OnEnable_m1773 (CFX_AutoDestructShuriken_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX_AutoDestructShuriken::CheckIfAlive()
extern "C" Object_t * CFX_AutoDestructShuriken_CheckIfAlive_m1774 (CFX_AutoDestructShuriken_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
