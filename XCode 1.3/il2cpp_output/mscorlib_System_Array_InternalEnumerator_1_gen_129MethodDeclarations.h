﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_129.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"

// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29960_gshared (InternalEnumerator_1_t3570 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29960(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3570 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29960_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29961_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29961(__this, method) (( void (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29961_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29962_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29962(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29962_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29963_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29963(__this, method) (( void (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29963_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29964_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29964(__this, method) (( bool (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29964_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern "C" ResourceCacheItem_t2131  InternalEnumerator_1_get_Current_m29965_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29965(__this, method) (( ResourceCacheItem_t2131  (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29965_gshared)(__this, method)
