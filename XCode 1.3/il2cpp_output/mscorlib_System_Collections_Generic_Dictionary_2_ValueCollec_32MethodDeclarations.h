﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2632;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16177_gshared (Enumerator_t2641 * __this, Dictionary_2_t2632 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16177(__this, ___host, method) (( void (*) (Enumerator_t2641 *, Dictionary_2_t2632 *, const MethodInfo*))Enumerator__ctor_m16177_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16178_gshared (Enumerator_t2641 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16178(__this, method) (( Object_t * (*) (Enumerator_t2641 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16178_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m16179_gshared (Enumerator_t2641 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m16179(__this, method) (( void (*) (Enumerator_t2641 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16180_gshared (Enumerator_t2641 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16180(__this, method) (( void (*) (Enumerator_t2641 *, const MethodInfo*))Enumerator_Dispose_m16180_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16181_gshared (Enumerator_t2641 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16181(__this, method) (( bool (*) (Enumerator_t2641 *, const MethodInfo*))Enumerator_MoveNext_m16181_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16182_gshared (Enumerator_t2641 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16182(__this, method) (( Object_t * (*) (Enumerator_t2641 *, const MethodInfo*))Enumerator_get_Current_m16182_gshared)(__this, method)
