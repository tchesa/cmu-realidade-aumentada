﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOSNotificationController
struct IOSNotificationController_t140;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// ISN_Singleton`1<IOSNotificationController>
struct  ISN_Singleton_1_t141  : public EventDispatcher_t69
{
};
struct ISN_Singleton_1_t141_StaticFields{
	// T ISN_Singleton`1<IOSNotificationController>::_instance
	IOSNotificationController_t140 * ____instance_3;
	// System.Boolean ISN_Singleton`1<IOSNotificationController>::applicationIsQuitting
	bool ___applicationIsQuitting_4;
};
