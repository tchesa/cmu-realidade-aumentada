﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_UserInfoLoadResult
struct ISN_UserInfoLoadResult_t120;
// System.String
struct String_t;
// GameCenterPlayerTemplate
struct GameCenterPlayerTemplate_t107;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_UserInfoLoadResult::.ctor(System.String)
extern "C" void ISN_UserInfoLoadResult__ctor_m678 (ISN_UserInfoLoadResult_t120 * __this, String_t* ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_UserInfoLoadResult::.ctor(GameCenterPlayerTemplate)
extern "C" void ISN_UserInfoLoadResult__ctor_m679 (ISN_UserInfoLoadResult_t120 * __this, GameCenterPlayerTemplate_t107 * ___tpl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_UserInfoLoadResult::get_playerId()
extern "C" String_t* ISN_UserInfoLoadResult_get_playerId_m680 (ISN_UserInfoLoadResult_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameCenterPlayerTemplate ISN_UserInfoLoadResult::get_playerTemplate()
extern "C" GameCenterPlayerTemplate_t107 * ISN_UserInfoLoadResult_get_playerTemplate_m681 (ISN_UserInfoLoadResult_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
