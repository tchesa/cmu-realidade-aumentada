﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t726;
// System.String
struct String_t;
// UnityEngine.Event
struct Event_t725;
struct Event_t725_marshaled;
// System.Object
struct Object_t;
// UnityEngine.UnhandledExceptionHandler
struct UnhandledExceptionHandler_t1406;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t1445;
// System.Exception
struct Exception_t40;
// System.Single[]
struct SingleU5BU5D_t23;
// UnityEngine.ParticleSystem
struct ParticleSystem_t332;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t331;
// UnityEngine.Transform
struct Transform_t25;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t1446;
// UnityEngine.TextAsset
struct TextAsset_t522;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t1408;
// UnityEngine.SerializeField
struct SerializeField_t1409;
// UnityEngine.Shader
struct Shader_t583;
// UnityEngine.Material
struct Material_t45;
// UnityEngine.Texture
struct Texture_t731;
// UnityEngine.Sprite
struct Sprite_t709;
// UnityEngine.Texture2D
struct Texture2D_t33;
// UnityEngine.WWWForm
struct WWWForm_t293;
// System.Text.Encoding
struct Encoding_t1442;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.Collections.Hashtable
struct Hashtable_t19;
// System.Object[]
struct ObjectU5BU5D_t34;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1340;
struct AsyncOperation_t1340_marshaled;
// UnityEngine.Application/LogCallback
struct LogCallback_t1416;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// UnityEngine.Behaviour
struct Behaviour_t875;
// UnityEngine.Camera
struct Camera_t510;
// UnityEngine.RenderTexture
struct RenderTexture_t582;
// UnityEngine.Camera[]
struct CameraU5BU5D_t600;
// UnityEngine.Object
struct Object_t53;
struct Object_t53_marshaled;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t1420;
// UnityEngine.Display
struct Display_t1421;
// System.IntPtr[]
struct IntPtrU5BU5D_t1447;
// UnityEngine.NotConvertedAttribute
struct NotConvertedAttribute_t1423;
// UnityEngine.NotRenamedAttribute
struct NotRenamedAttribute_t1424;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t18;
// UnityEngine.Coroutine
struct Coroutine_t39;
struct Coroutine_t39_marshaled;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1153;
// System.Type
struct Type_t;
// UnityEngine.Component
struct Component_t56;
// UnityEngine.Rigidbody
struct Rigidbody_t50;
// UnityEngine.Light
struct Light_t47;
// UnityEngine.Renderer
struct Renderer_t46;
// UnityEngine.AudioSource
struct AudioSource_t20;
// UnityEngine.GUIText
struct GUIText_t44;
// UnityEngine.GUITexture
struct GUITexture_t43;
// UnityEngine.GameObject
struct GameObject_t27;
// UnityEngine.Component[]
struct ComponentU5BU5D_t54;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t826;
// UnityEngine.Transform/Enumerator
struct Enumerator_t1427;
// UnityEngine.YieldInstruction
struct YieldInstruction_t1349;
struct YieldInstruction_t1349_marshaled;
// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t1430;
// UnityEngine.Events.UnityAction
struct UnityAction_t691;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstrucMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_ConvertMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_iPhoneGeneration.h"
#include "UnityEngine_UnityEngine_iPhoneGenerationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Char.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCodeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiersMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gizmos.h"
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_RemoteNotificationType.h"
#include "UnityEngine_UnityEngine_RemoteNotificationTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandlerMethodDeclarations.h"
#include "mscorlib_System_AppDomainMethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
#include "mscorlib_System_AppDomain.h"
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "mscorlib_System_Byte.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane.h"
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "mscorlib_System_Double.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem.h"
#include "UnityEngine_UnityEngine_ParticleSystemMethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_68MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_68.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_TextAsset.h"
#include "UnityEngine_UnityEngine_TextAssetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariablesMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_SerializeField.h"
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader.h"
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_Sprite.h"
#include "UnityEngine_UnityEngine_SpriteMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
#include "UnityEngine_UnityEngine_SpriteRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm.h"
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_69MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_69.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_2.h"
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
#include "mscorlib_System_Text_Encoding.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWTranscoderMethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream.h"
#include "UnityEngine_UnityEngine_WWWTranscoder.h"
#include "UnityEngine_UnityEngine_CacheIndex.h"
#include "UnityEngine_UnityEngine_CacheIndexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString.h"
#include "UnityEngine_UnityEngine_AsyncOperation.h"
#include "UnityEngine_UnityEngine_AsyncOperationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction.h"
#include "UnityEngine_UnityEngine_NetworkReachability.h"
#include "UnityEngine_UnityEngine_NetworkReachabilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_Application.h"
#include "UnityEngine_UnityEngine_UserAuthorization.h"
#include "UnityEngine_UnityEngine_UserAuthorizationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_Debug.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegate.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display.h"
#include "UnityEngine_UnityEngine_DisplayMethodDeclarations.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_NotConvertedAttribute.h"
#include "UnityEngine_UnityEngine_NotConvertedAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_NotRenamedAttribute.h"
#include "UnityEngine_UnityEngine_NotRenamedAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_TouchPhaseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_IMECompositionMode.h"
#include "UnityEngine_UnityEngine_IMECompositionModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ReferenceData.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "UnityEngine_UnityEngine_Rigidbody.h"
#include "UnityEngine_UnityEngine_Light.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "UnityEngine_UnityEngine_AudioSource.h"
#include "UnityEngine_UnityEngine_GUIText.h"
#include "UnityEngine_UnityEngine_GUITexture.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_25.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_LightMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "UnityEngine_UnityEngine_Space.h"
#include "UnityEngine_UnityEngine_Time.h"
#include "UnityEngine_UnityEngine_Random.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Motion.h"
#include "UnityEngine_UnityEngine_MotionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m436_gshared (GameObject_t27 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m436(__this, method) (( Object_t * (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m436_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t332_m2562(__this, method) (( ParticleSystem_t332 * (*) (GameObject_t27 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m436_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern TypeInfo* TouchScreenKeyboard_InternalConstructorHelperArguments_t1400_il2cpp_TypeInfo_var;
extern TypeInfo* TouchScreenKeyboardType_t854_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t494_il2cpp_TypeInfo_var;
extern "C" void TouchScreenKeyboard__ctor_m7761 (TouchScreenKeyboard_t726 * __this, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, bool ___alert, String_t* ___textPlaceholder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TouchScreenKeyboard_InternalConstructorHelperArguments_t1400_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(969);
		TouchScreenKeyboardType_t854_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(559);
		Convert_t494_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(68);
		s_Il2CppMethodIntialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1400  V_0 = {0};
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t1400_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(TouchScreenKeyboardType_t854_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t494_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m8191(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->___keyboardType_0 = L_3;
		bool L_4 = ___autocorrection;
		uint32_t L_5 = Convert_ToUInt32_m8192(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->___autocorrection_1 = L_5;
		bool L_6 = ___multiline;
		uint32_t L_7 = Convert_ToUInt32_m8192(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->___multiline_2 = L_7;
		bool L_8 = ___secure;
		uint32_t L_9 = Convert_ToUInt32_m8192(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->___secure_3 = L_9;
		bool L_10 = ___alert;
		uint32_t L_11 = Convert_ToUInt32_m8192(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->___alert_4 = L_11;
		String_t* L_12 = ___text;
		String_t* L_13 = ___textPlaceholder;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m7764(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C" void TouchScreenKeyboard_Destroy_m7762 (TouchScreenKeyboard_t726 * __this, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m7762_ftn) (TouchScreenKeyboard_t726 *);
	static TouchScreenKeyboard_Destroy_m7762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m7762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C" void TouchScreenKeyboard_Finalize_m7763 (TouchScreenKeyboard_t726 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m7762(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C" void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m7764 (TouchScreenKeyboard_t726 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t1400 * ___arguments, String_t* ___text, String_t* ___textPlaceholder, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m7764_ftn) (TouchScreenKeyboard_t726 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t1400 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m7764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m7764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments, ___text, ___textPlaceholder);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C" bool TouchScreenKeyboard_get_isSupported_m4243 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = Application_get_platform_m2652(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 0)
		{
			goto IL_0055;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 1)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 2)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 3)
		{
			goto IL_0055;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 4)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 5)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 6)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 7)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 8)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 9)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 10)
		{
			goto IL_0057;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 11)
		{
			goto IL_0057;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 12)
		{
			goto IL_0057;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 13)
		{
			goto IL_0055;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 14)
		{
			goto IL_0055;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 15)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0059;
	}

IL_0055:
	{
		return 1;
	}

IL_0057:
	{
		return 0;
	}

IL_0059:
	{
		return 0;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t726 * TouchScreenKeyboard_Open_m4298 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		String_t* L_1 = ___text;
		int32_t L_2 = ___keyboardType;
		bool L_3 = ___autocorrection;
		bool L_4 = ___multiline;
		bool L_5 = ___secure;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t726 * L_8 = TouchScreenKeyboard_Open_m7765(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t726 * TouchScreenKeyboard_Open_m4299 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		V_2 = 0;
		String_t* L_1 = ___text;
		int32_t L_2 = ___keyboardType;
		bool L_3 = ___autocorrection;
		bool L_4 = ___multiline;
		bool L_5 = V_2;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t726 * L_8 = TouchScreenKeyboard_Open_m7765(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern TypeInfo* TouchScreenKeyboard_t726_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t726 * TouchScreenKeyboard_Open_m7765 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, bool ___alert, String_t* ___textPlaceholder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TouchScreenKeyboard_t726_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(970);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		int32_t L_1 = ___keyboardType;
		bool L_2 = ___autocorrection;
		bool L_3 = ___multiline;
		bool L_4 = ___secure;
		bool L_5 = ___alert;
		String_t* L_6 = ___textPlaceholder;
		TouchScreenKeyboard_t726 * L_7 = (TouchScreenKeyboard_t726 *)il2cpp_codegen_object_new (TouchScreenKeyboard_t726_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m7761(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C" String_t* TouchScreenKeyboard_get_text_m4222 (TouchScreenKeyboard_t726 * __this, const MethodInfo* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m4222_ftn) (TouchScreenKeyboard_t726 *);
	static TouchScreenKeyboard_get_text_m4222_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m4222_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C" void TouchScreenKeyboard_set_text_m4223 (TouchScreenKeyboard_t726 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m4223_ftn) (TouchScreenKeyboard_t726 *, String_t*);
	static TouchScreenKeyboard_set_text_m4223_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m4223_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C" void TouchScreenKeyboard_set_hideInput_m4297 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m4297_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m4297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m4297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C" bool TouchScreenKeyboard_get_active_m4221 (TouchScreenKeyboard_t726 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m4221_ftn) (TouchScreenKeyboard_t726 *);
	static TouchScreenKeyboard_get_active_m4221_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m4221_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C" void TouchScreenKeyboard_set_active_m4296 (TouchScreenKeyboard_t726 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m4296_ftn) (TouchScreenKeyboard_t726 *, bool);
	static TouchScreenKeyboard_set_active_m4296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m4296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C" bool TouchScreenKeyboard_get_done_m4245 (TouchScreenKeyboard_t726 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m4245_ftn) (TouchScreenKeyboard_t726 *);
	static TouchScreenKeyboard_get_done_m4245_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m4245_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C" bool TouchScreenKeyboard_get_wasCanceled_m4244 (TouchScreenKeyboard_t726 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m4244_ftn) (TouchScreenKeyboard_t726 *);
	static TouchScreenKeyboard_get_wasCanceled_m4244_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m4244_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::.ctor()
extern "C" void Event__ctor_m4219 (Event_t725 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		Event_Init_m7768(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(UnityEngine.Event)
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1221;
extern "C" void Event__ctor_m7766 (Event_t725 * __this, Event_t725 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		_stringLiteral1221 = il2cpp_codegen_string_literal_from_index(1221);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		Event_t725 * L_0 = ___other;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentException_t513 * L_1 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_1, _stringLiteral1221, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Event_t725 * L_2 = ___other;
		Event_InitCopy_m7771(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(System.IntPtr)
extern "C" void Event__ctor_m7767 (Event_t725 * __this, IntPtr_t ___ptr, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___ptr;
		Event_InitPtr_m7772(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Init()
extern "C" void Event_Init_m7768 (Event_t725 * __this, const MethodInfo* method)
{
	typedef void (*Event_Init_m7768_ftn) (Event_t725 *);
	static Event_Init_m7768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Init_m7768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::Finalize()
extern "C" void Event_Finalize_m7769 (Event_t725 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Event_Cleanup_m7770(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Event::Cleanup()
extern "C" void Event_Cleanup_m7770 (Event_t725 * __this, const MethodInfo* method)
{
	typedef void (*Event_Cleanup_m7770_ftn) (Event_t725 *);
	static Event_Cleanup_m7770_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Cleanup_m7770_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::InitCopy(UnityEngine.Event)
extern "C" void Event_InitCopy_m7771 (Event_t725 * __this, Event_t725 * ___other, const MethodInfo* method)
{
	typedef void (*Event_InitCopy_m7771_ftn) (Event_t725 *, Event_t725 *);
	static Event_InitCopy_m7771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_InitCopy_m7771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::InitCopy(UnityEngine.Event)");
	_il2cpp_icall_func(__this, ___other);
}
// System.Void UnityEngine.Event::InitPtr(System.IntPtr)
extern "C" void Event_InitPtr_m7772 (Event_t725 * __this, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_InitPtr_m7772_ftn) (Event_t725 *, IntPtr_t);
	static Event_InitPtr_m7772_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_InitPtr_m7772_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::InitPtr(System.IntPtr)");
	_il2cpp_icall_func(__this, ___ptr);
}
// UnityEngine.EventType UnityEngine.Event::get_rawType()
extern "C" int32_t Event_get_rawType_m4259 (Event_t725 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_rawType_m4259_ftn) (Event_t725 *);
	static Event_get_rawType_m4259_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_rawType_m4259_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_rawType()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C" int32_t Event_get_type_m7773 (Event_t725 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_type_m7773_ftn) (Event_t725 *);
	static Event_get_type_m7773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_type_m7773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_type()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
extern "C" void Event_set_type_m7774 (Event_t725 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_type_m7774_ftn) (Event_t725 *, int32_t);
	static Event_set_type_m7774_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_type_m7774_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_type(UnityEngine.EventType)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
extern "C" int32_t Event_GetTypeForControl_m7775 (Event_t725 * __this, int32_t ___controlID, const MethodInfo* method)
{
	typedef int32_t (*Event_GetTypeForControl_m7775_ftn) (Event_t725 *, int32_t);
	static Event_GetTypeForControl_m7775_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetTypeForControl_m7775_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetTypeForControl(System.Int32)");
	return _il2cpp_icall_func(__this, ___controlID);
}
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C" Vector2_t29  Event_get_mousePosition_m7776 (Event_t725 * __this, const MethodInfo* method)
{
	Vector2_t29  V_0 = {0};
	{
		Event_Internal_GetMousePosition_m7780(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_mousePosition(UnityEngine.Vector2)
extern "C" void Event_set_mousePosition_m7777 (Event_t725 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = ___value;
		Event_Internal_SetMousePosition_m7778(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Internal_SetMousePosition(UnityEngine.Vector2)
extern "C" void Event_Internal_SetMousePosition_m7778 (Event_t725 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		Event_INTERNAL_CALL_Internal_SetMousePosition_m7779(NULL /*static, unused*/, __this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)
extern "C" void Event_INTERNAL_CALL_Internal_SetMousePosition_m7779 (Object_t * __this /* static, unused */, Event_t725 * ___self, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*Event_INTERNAL_CALL_Internal_SetMousePosition_m7779_ftn) (Event_t725 *, Vector2_t29 *);
	static Event_INTERNAL_CALL_Internal_SetMousePosition_m7779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_INTERNAL_CALL_Internal_SetMousePosition_m7779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___value);
}
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMousePosition_m7780 (Event_t725 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMousePosition_m7780_ftn) (Event_t725 *, Vector2_t29 *);
	static Event_Internal_GetMousePosition_m7780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMousePosition_m7780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.Event::get_delta()
extern "C" Vector2_t29  Event_get_delta_m7781 (Event_t725 * __this, const MethodInfo* method)
{
	Vector2_t29  V_0 = {0};
	{
		Event_Internal_GetMouseDelta_m7785(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_delta(UnityEngine.Vector2)
extern "C" void Event_set_delta_m7782 (Event_t725 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = ___value;
		Event_Internal_SetMouseDelta_m7783(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Internal_SetMouseDelta(UnityEngine.Vector2)
extern "C" void Event_Internal_SetMouseDelta_m7783 (Event_t725 * __this, Vector2_t29  ___value, const MethodInfo* method)
{
	{
		Event_INTERNAL_CALL_Internal_SetMouseDelta_m7784(NULL /*static, unused*/, __this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)
extern "C" void Event_INTERNAL_CALL_Internal_SetMouseDelta_m7784 (Object_t * __this /* static, unused */, Event_t725 * ___self, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*Event_INTERNAL_CALL_Internal_SetMouseDelta_m7784_ftn) (Event_t725 *, Vector2_t29 *);
	static Event_INTERNAL_CALL_Internal_SetMouseDelta_m7784_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_INTERNAL_CALL_Internal_SetMouseDelta_m7784_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___value);
}
// System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMouseDelta_m7785 (Event_t725 * __this, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMouseDelta_m7785_ftn) (Event_t725 *, Vector2_t29 *);
	static Event_Internal_GetMouseDelta_m7785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMouseDelta_m7785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Ray UnityEngine.Event::get_mouseRay()
extern "C" Ray_t567  Event_get_mouseRay_m7786 (Event_t725 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Vector3_get_up_m284(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6  L_1 = Vector3_get_up_m284(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t567  L_2 = {0};
		Ray__ctor_m7933(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Event::set_mouseRay(UnityEngine.Ray)
extern "C" void Event_set_mouseRay_m7787 (Event_t725 * __this, Ray_t567  ___value, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 UnityEngine.Event::get_button()
extern "C" int32_t Event_get_button_m7788 (Event_t725 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_button_m7788_ftn) (Event_t725 *);
	static Event_get_button_m7788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_button_m7788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_button()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_button(System.Int32)
extern "C" void Event_set_button_m7789 (Event_t725 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_button_m7789_ftn) (Event_t725 *, int32_t);
	static Event_set_button_m7789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_button_m7789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_button(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C" int32_t Event_get_modifiers_m4255 (Event_t725 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_modifiers_m4255_ftn) (Event_t725 *);
	static Event_get_modifiers_m4255_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_modifiers_m4255_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_modifiers()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
extern "C" void Event_set_modifiers_m7790 (Event_t725 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_modifiers_m7790_ftn) (Event_t725 *, int32_t);
	static Event_set_modifiers_m7790_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_modifiers_m7790_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Event::get_pressure()
extern "C" float Event_get_pressure_m7791 (Event_t725 * __this, const MethodInfo* method)
{
	typedef float (*Event_get_pressure_m7791_ftn) (Event_t725 *);
	static Event_get_pressure_m7791_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_pressure_m7791_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_pressure()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_pressure(System.Single)
extern "C" void Event_set_pressure_m7792 (Event_t725 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Event_set_pressure_m7792_ftn) (Event_t725 *, float);
	static Event_set_pressure_m7792_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_pressure_m7792_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_pressure(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Event::get_clickCount()
extern "C" int32_t Event_get_clickCount_m7793 (Event_t725 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_clickCount_m7793_ftn) (Event_t725 *);
	static Event_get_clickCount_m7793_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_clickCount_m7793_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_clickCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_clickCount(System.Int32)
extern "C" void Event_set_clickCount_m7794 (Event_t725 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_clickCount_m7794_ftn) (Event_t725 *, int32_t);
	static Event_set_clickCount_m7794_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_clickCount_m7794_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_clickCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Char UnityEngine.Event::get_character()
extern "C" uint16_t Event_get_character_m4257 (Event_t725 * __this, const MethodInfo* method)
{
	typedef uint16_t (*Event_get_character_m4257_ftn) (Event_t725 *);
	static Event_get_character_m4257_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_character_m4257_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_character()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_character(System.Char)
extern "C" void Event_set_character_m7795 (Event_t725 * __this, uint16_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_character_m7795_ftn) (Event_t725 *, uint16_t);
	static Event_set_character_m7795_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_character_m7795_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_character(System.Char)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Event::get_commandName()
extern "C" String_t* Event_get_commandName_m7796 (Event_t725 * __this, const MethodInfo* method)
{
	typedef String_t* (*Event_get_commandName_m7796_ftn) (Event_t725 *);
	static Event_get_commandName_m7796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_commandName_m7796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_commandName()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_commandName(System.String)
extern "C" void Event_set_commandName_m7797 (Event_t725 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Event_set_commandName_m7797_ftn) (Event_t725 *, String_t*);
	static Event_set_commandName_m7797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_commandName_m7797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_commandName(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C" int32_t Event_get_keyCode_m4256 (Event_t725 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_keyCode_m4256_ftn) (Event_t725 *);
	static Event_get_keyCode_m4256_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_keyCode_m4256_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_keyCode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
extern "C" void Event_set_keyCode_m7798 (Event_t725 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_keyCode_m7798_ftn) (Event_t725 *, int32_t);
	static Event_set_keyCode_m7798_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_keyCode_m7798_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Event::get_shift()
extern "C" bool Event_get_shift_m7799 (Event_t725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_shift(System.Boolean)
extern "C" void Event_set_shift_m7800 (Event_t725 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_control()
extern "C" bool Event_get_control_m7801 (Event_t725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_control(System.Boolean)
extern "C" void Event_set_control_m7802 (Event_t725 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-3))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_2|(int32_t)2)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_alt()
extern "C" bool Event_get_alt_m7803 (Event_t725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_alt(System.Boolean)
extern "C" void Event_set_alt_m7804 (Event_t725 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-5))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_2|(int32_t)4)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_command()
extern "C" bool Event_get_command_m7805 (Event_t725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)8))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_command(System.Boolean)
extern "C" void Event_set_command_m7806 (Event_t725 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-9))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_2|(int32_t)8)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_capsLock()
extern "C" bool Event_get_capsLock_m7807 (Event_t725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)32)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_capsLock(System.Boolean)
extern "C" void Event_set_capsLock_m7808 (Event_t725 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_2|(int32_t)((int32_t)32))), /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_numeric()
extern "C" bool Event_get_numeric_m7809 (Event_t725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_numeric(System.Boolean)
extern "C" void Event_set_numeric_m7810 (Event_t725 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m7790(__this, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_functionKey()
extern "C" bool Event_get_functionKey_m7811 (Event_t725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern TypeInfo* Event_t725_il2cpp_TypeInfo_var;
extern "C" Event_t725 * Event_get_current_m7812 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(551);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t725 * L_0 = ((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_current(UnityEngine.Event)
extern TypeInfo* Event_t725_il2cpp_TypeInfo_var;
extern "C" void Event_set_current_m7813 (Object_t * __this /* static, unused */, Event_t725 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(551);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t725 * L_0 = ___value;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Event_t725 * L_1 = ___value;
		((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_1;
		goto IL_001b;
	}

IL_0011:
	{
		Event_t725 * L_2 = ((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
	}

IL_001b:
	{
		Event_t725 * L_3 = ((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m7814(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C" void Event_Internal_SetNativeEvent_m7814 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_Internal_SetNativeEvent_m7814_ftn) (IntPtr_t);
	static Event_Internal_SetNativeEvent_m7814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_SetNativeEvent_m7814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)");
	_il2cpp_icall_func(___ptr);
}
// System.Void UnityEngine.Event::Internal_MakeMasterEventCurrent()
extern TypeInfo* Event_t725_il2cpp_TypeInfo_var;
extern "C" void Event_Internal_MakeMasterEventCurrent_m7815 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(551);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t725 * L_0 = ((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Event_t725 * L_1 = (Event_t725 *)il2cpp_codegen_object_new (Event_t725_il2cpp_TypeInfo_var);
		Event__ctor_m4219(L_1, /*hidden argument*/NULL);
		((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2 = L_1;
	}

IL_0014:
	{
		Event_t725 * L_2 = ((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
		Event_t725 * L_3 = ((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m7814(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Use()
extern "C" void Event_Use_m7816 (Event_t725 * __this, const MethodInfo* method)
{
	typedef void (*Event_Use_m7816_ftn) (Event_t725 *);
	static Event_Use_m7816_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Use_m7816_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Use()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
extern "C" bool Event_PopEvent_m4260 (Object_t * __this /* static, unused */, Event_t725 * ___outEvent, const MethodInfo* method)
{
	typedef bool (*Event_PopEvent_m4260_ftn) (Event_t725 *);
	static Event_PopEvent_m4260_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_PopEvent_m4260_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::PopEvent(UnityEngine.Event)");
	return _il2cpp_icall_func(___outEvent);
}
// System.Int32 UnityEngine.Event::GetEventCount()
extern "C" int32_t Event_GetEventCount_m7817 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Event_GetEventCount_m7817_ftn) ();
	static Event_GetEventCount_m7817_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetEventCount_m7817_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetEventCount()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Event::get_isKey()
extern "C" bool Event_get_isKey_m7818 (Event_t725 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)5))? 1 : 0);
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C" bool Event_get_isMouse_m7819 (Event_t725 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B5_0 = 1;
	}

IL_0022:
	{
		return G_B5_0;
	}
}
// UnityEngine.Event UnityEngine.Event::KeyboardEvent(System.String)
extern const Il2CppType* KeyCode_t1401_0_0_0_var;
extern TypeInfo* Event_t725_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t28_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t63_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m332_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1222;
extern Il2CppCodeGenString* _stringLiteral1223;
extern Il2CppCodeGenString* _stringLiteral1224;
extern Il2CppCodeGenString* _stringLiteral1225;
extern Il2CppCodeGenString* _stringLiteral1226;
extern Il2CppCodeGenString* _stringLiteral1227;
extern Il2CppCodeGenString* _stringLiteral1228;
extern Il2CppCodeGenString* _stringLiteral1229;
extern Il2CppCodeGenString* _stringLiteral1230;
extern Il2CppCodeGenString* _stringLiteral1231;
extern Il2CppCodeGenString* _stringLiteral1232;
extern Il2CppCodeGenString* _stringLiteral1233;
extern Il2CppCodeGenString* _stringLiteral1234;
extern Il2CppCodeGenString* _stringLiteral1235;
extern Il2CppCodeGenString* _stringLiteral1236;
extern Il2CppCodeGenString* _stringLiteral1237;
extern Il2CppCodeGenString* _stringLiteral1238;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral1239;
extern Il2CppCodeGenString* _stringLiteral1240;
extern Il2CppCodeGenString* _stringLiteral1241;
extern Il2CppCodeGenString* _stringLiteral1242;
extern Il2CppCodeGenString* _stringLiteral1243;
extern Il2CppCodeGenString* _stringLiteral1244;
extern Il2CppCodeGenString* _stringLiteral1245;
extern Il2CppCodeGenString* _stringLiteral1246;
extern Il2CppCodeGenString* _stringLiteral1247;
extern Il2CppCodeGenString* _stringLiteral1248;
extern Il2CppCodeGenString* _stringLiteral1249;
extern Il2CppCodeGenString* _stringLiteral685;
extern Il2CppCodeGenString* _stringLiteral1250;
extern Il2CppCodeGenString* _stringLiteral1251;
extern Il2CppCodeGenString* _stringLiteral1252;
extern Il2CppCodeGenString* _stringLiteral1253;
extern Il2CppCodeGenString* _stringLiteral1254;
extern Il2CppCodeGenString* _stringLiteral1255;
extern Il2CppCodeGenString* _stringLiteral1256;
extern Il2CppCodeGenString* _stringLiteral1257;
extern Il2CppCodeGenString* _stringLiteral1258;
extern Il2CppCodeGenString* _stringLiteral1259;
extern Il2CppCodeGenString* _stringLiteral1260;
extern Il2CppCodeGenString* _stringLiteral1261;
extern Il2CppCodeGenString* _stringLiteral1262;
extern Il2CppCodeGenString* _stringLiteral1263;
extern Il2CppCodeGenString* _stringLiteral1264;
extern Il2CppCodeGenString* _stringLiteral1265;
extern Il2CppCodeGenString* _stringLiteral1266;
extern Il2CppCodeGenString* _stringLiteral1267;
extern Il2CppCodeGenString* _stringLiteral80;
extern Il2CppCodeGenString* _stringLiteral1268;
extern "C" Event_t725 * Event_KeyboardEvent_m7820 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyCode_t1401_0_0_0_var = il2cpp_codegen_type_from_index(971);
		Event_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(551);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Dictionary_2_t28_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Enum_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Dictionary_2__ctor_m332_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		_stringLiteral1222 = il2cpp_codegen_string_literal_from_index(1222);
		_stringLiteral1223 = il2cpp_codegen_string_literal_from_index(1223);
		_stringLiteral1224 = il2cpp_codegen_string_literal_from_index(1224);
		_stringLiteral1225 = il2cpp_codegen_string_literal_from_index(1225);
		_stringLiteral1226 = il2cpp_codegen_string_literal_from_index(1226);
		_stringLiteral1227 = il2cpp_codegen_string_literal_from_index(1227);
		_stringLiteral1228 = il2cpp_codegen_string_literal_from_index(1228);
		_stringLiteral1229 = il2cpp_codegen_string_literal_from_index(1229);
		_stringLiteral1230 = il2cpp_codegen_string_literal_from_index(1230);
		_stringLiteral1231 = il2cpp_codegen_string_literal_from_index(1231);
		_stringLiteral1232 = il2cpp_codegen_string_literal_from_index(1232);
		_stringLiteral1233 = il2cpp_codegen_string_literal_from_index(1233);
		_stringLiteral1234 = il2cpp_codegen_string_literal_from_index(1234);
		_stringLiteral1235 = il2cpp_codegen_string_literal_from_index(1235);
		_stringLiteral1236 = il2cpp_codegen_string_literal_from_index(1236);
		_stringLiteral1237 = il2cpp_codegen_string_literal_from_index(1237);
		_stringLiteral1238 = il2cpp_codegen_string_literal_from_index(1238);
		_stringLiteral34 = il2cpp_codegen_string_literal_from_index(34);
		_stringLiteral1239 = il2cpp_codegen_string_literal_from_index(1239);
		_stringLiteral1240 = il2cpp_codegen_string_literal_from_index(1240);
		_stringLiteral1241 = il2cpp_codegen_string_literal_from_index(1241);
		_stringLiteral1242 = il2cpp_codegen_string_literal_from_index(1242);
		_stringLiteral1243 = il2cpp_codegen_string_literal_from_index(1243);
		_stringLiteral1244 = il2cpp_codegen_string_literal_from_index(1244);
		_stringLiteral1245 = il2cpp_codegen_string_literal_from_index(1245);
		_stringLiteral1246 = il2cpp_codegen_string_literal_from_index(1246);
		_stringLiteral1247 = il2cpp_codegen_string_literal_from_index(1247);
		_stringLiteral1248 = il2cpp_codegen_string_literal_from_index(1248);
		_stringLiteral1249 = il2cpp_codegen_string_literal_from_index(1249);
		_stringLiteral685 = il2cpp_codegen_string_literal_from_index(685);
		_stringLiteral1250 = il2cpp_codegen_string_literal_from_index(1250);
		_stringLiteral1251 = il2cpp_codegen_string_literal_from_index(1251);
		_stringLiteral1252 = il2cpp_codegen_string_literal_from_index(1252);
		_stringLiteral1253 = il2cpp_codegen_string_literal_from_index(1253);
		_stringLiteral1254 = il2cpp_codegen_string_literal_from_index(1254);
		_stringLiteral1255 = il2cpp_codegen_string_literal_from_index(1255);
		_stringLiteral1256 = il2cpp_codegen_string_literal_from_index(1256);
		_stringLiteral1257 = il2cpp_codegen_string_literal_from_index(1257);
		_stringLiteral1258 = il2cpp_codegen_string_literal_from_index(1258);
		_stringLiteral1259 = il2cpp_codegen_string_literal_from_index(1259);
		_stringLiteral1260 = il2cpp_codegen_string_literal_from_index(1260);
		_stringLiteral1261 = il2cpp_codegen_string_literal_from_index(1261);
		_stringLiteral1262 = il2cpp_codegen_string_literal_from_index(1262);
		_stringLiteral1263 = il2cpp_codegen_string_literal_from_index(1263);
		_stringLiteral1264 = il2cpp_codegen_string_literal_from_index(1264);
		_stringLiteral1265 = il2cpp_codegen_string_literal_from_index(1265);
		_stringLiteral1266 = il2cpp_codegen_string_literal_from_index(1266);
		_stringLiteral1267 = il2cpp_codegen_string_literal_from_index(1267);
		_stringLiteral80 = il2cpp_codegen_string_literal_from_index(80);
		_stringLiteral1268 = il2cpp_codegen_string_literal_from_index(1268);
		s_Il2CppMethodIntialized = true;
	}
	Event_t725 * V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = {0};
	uint16_t V_4 = 0x0;
	String_t* V_5 = {0};
	Dictionary_2_t28 * V_6 = {0};
	int32_t V_7 = 0;
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Event_t725 * L_0 = (Event_t725 *)il2cpp_codegen_object_new (Event_t725_il2cpp_TypeInfo_var);
		Event__ctor_m4219(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t725 * L_1 = V_0;
		NullCheck(L_1);
		Event_set_type_m7774(L_1, 4, /*hidden argument*/NULL);
		String_t* L_2 = ___key;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_3 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_5 = String_op_Equality_m385(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0025;
		}
	}

IL_0023:
	{
		Event_t725 * L_6 = V_0;
		return L_6;
	}

IL_0025:
	{
		V_1 = 0;
		V_2 = 0;
	}

IL_0029:
	{
		V_2 = 1;
		int32_t L_7 = V_1;
		String_t* L_8 = ___key;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m410(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_003e;
		}
	}
	{
		V_2 = 0;
		goto IL_00d8;
	}

IL_003e:
	{
		String_t* L_10 = ___key;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		uint16_t L_12 = String_get_Chars_m2170(L_10, L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		uint16_t L_13 = V_4;
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)35))) == 0)
		{
			goto IL_00b4;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)35))) == 1)
		{
			goto IL_0061;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)35))) == 2)
		{
			goto IL_009d;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)35))) == 3)
		{
			goto IL_006f;
		}
	}

IL_0061:
	{
		uint16_t L_14 = V_4;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)94))))
		{
			goto IL_0086;
		}
	}
	{
		goto IL_00cb;
	}

IL_006f:
	{
		Event_t725 * L_15 = V_0;
		Event_t725 * L_16 = L_15;
		NullCheck(L_16);
		int32_t L_17 = Event_get_modifiers_m4255(L_16, /*hidden argument*/NULL);
		NullCheck(L_16);
		Event_set_modifiers_m7790(L_16, ((int32_t)((int32_t)L_17|(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
		goto IL_00d2;
	}

IL_0086:
	{
		Event_t725 * L_19 = V_0;
		Event_t725 * L_20 = L_19;
		NullCheck(L_20);
		int32_t L_21 = Event_get_modifiers_m4255(L_20, /*hidden argument*/NULL);
		NullCheck(L_20);
		Event_set_modifiers_m7790(L_20, ((int32_t)((int32_t)L_21|(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
		goto IL_00d2;
	}

IL_009d:
	{
		Event_t725 * L_23 = V_0;
		Event_t725 * L_24 = L_23;
		NullCheck(L_24);
		int32_t L_25 = Event_get_modifiers_m4255(L_24, /*hidden argument*/NULL);
		NullCheck(L_24);
		Event_set_modifiers_m7790(L_24, ((int32_t)((int32_t)L_25|(int32_t)8)), /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
		goto IL_00d2;
	}

IL_00b4:
	{
		Event_t725 * L_27 = V_0;
		Event_t725 * L_28 = L_27;
		NullCheck(L_28);
		int32_t L_29 = Event_get_modifiers_m4255(L_28, /*hidden argument*/NULL);
		NullCheck(L_28);
		Event_set_modifiers_m7790(L_28, ((int32_t)((int32_t)L_29|(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30+(int32_t)1));
		goto IL_00d2;
	}

IL_00cb:
	{
		V_2 = 0;
		goto IL_00d2;
	}

IL_00d2:
	{
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_0029;
		}
	}

IL_00d8:
	{
		String_t* L_32 = ___key;
		int32_t L_33 = V_1;
		String_t* L_34 = ___key;
		NullCheck(L_34);
		int32_t L_35 = String_get_Length_m410(L_34, /*hidden argument*/NULL);
		int32_t L_36 = V_1;
		NullCheck(L_32);
		String_t* L_37 = String_Substring_m411(L_32, L_33, ((int32_t)((int32_t)L_35-(int32_t)L_36)), /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_38 = String_ToLower_m412(L_37, /*hidden argument*/NULL);
		V_3 = L_38;
		String_t* L_39 = V_3;
		V_5 = L_39;
		String_t* L_40 = V_5;
		if (!L_40)
		{
			goto IL_09f0;
		}
	}
	{
		Dictionary_2_t28 * L_41 = ((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3;
		if (L_41)
		{
			goto IL_03b6;
		}
	}
	{
		Dictionary_2_t28 * L_42 = (Dictionary_2_t28 *)il2cpp_codegen_object_new (Dictionary_2_t28_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m332(L_42, ((int32_t)49), /*hidden argument*/Dictionary_2__ctor_m332_MethodInfo_var);
		V_6 = L_42;
		Dictionary_2_t28 * L_43 = V_6;
		NullCheck(L_43);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_43, _stringLiteral1222, 0);
		Dictionary_2_t28 * L_44 = V_6;
		NullCheck(L_44);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_44, _stringLiteral1223, 1);
		Dictionary_2_t28 * L_45 = V_6;
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_45, _stringLiteral1224, 2);
		Dictionary_2_t28 * L_46 = V_6;
		NullCheck(L_46);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_46, _stringLiteral1225, 3);
		Dictionary_2_t28 * L_47 = V_6;
		NullCheck(L_47);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_47, _stringLiteral1226, 4);
		Dictionary_2_t28 * L_48 = V_6;
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_48, _stringLiteral1227, 5);
		Dictionary_2_t28 * L_49 = V_6;
		NullCheck(L_49);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_49, _stringLiteral1228, 6);
		Dictionary_2_t28 * L_50 = V_6;
		NullCheck(L_50);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_50, _stringLiteral1229, 7);
		Dictionary_2_t28 * L_51 = V_6;
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_51, _stringLiteral1230, 8);
		Dictionary_2_t28 * L_52 = V_6;
		NullCheck(L_52);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_52, _stringLiteral1231, ((int32_t)9));
		Dictionary_2_t28 * L_53 = V_6;
		NullCheck(L_53);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_53, _stringLiteral1232, ((int32_t)10));
		Dictionary_2_t28 * L_54 = V_6;
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_54, _stringLiteral1233, ((int32_t)11));
		Dictionary_2_t28 * L_55 = V_6;
		NullCheck(L_55);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_55, _stringLiteral1234, ((int32_t)12));
		Dictionary_2_t28 * L_56 = V_6;
		NullCheck(L_56);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_56, _stringLiteral1235, ((int32_t)13));
		Dictionary_2_t28 * L_57 = V_6;
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_57, _stringLiteral1236, ((int32_t)14));
		Dictionary_2_t28 * L_58 = V_6;
		NullCheck(L_58);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_58, _stringLiteral1237, ((int32_t)15));
		Dictionary_2_t28 * L_59 = V_6;
		NullCheck(L_59);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_59, _stringLiteral1238, ((int32_t)16));
		Dictionary_2_t28 * L_60 = V_6;
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_60, _stringLiteral34, ((int32_t)17));
		Dictionary_2_t28 * L_61 = V_6;
		NullCheck(L_61);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_61, _stringLiteral1239, ((int32_t)18));
		Dictionary_2_t28 * L_62 = V_6;
		NullCheck(L_62);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_62, _stringLiteral1240, ((int32_t)19));
		Dictionary_2_t28 * L_63 = V_6;
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_63, _stringLiteral1241, ((int32_t)20));
		Dictionary_2_t28 * L_64 = V_6;
		NullCheck(L_64);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_64, _stringLiteral1242, ((int32_t)21));
		Dictionary_2_t28 * L_65 = V_6;
		NullCheck(L_65);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_65, _stringLiteral1243, ((int32_t)22));
		Dictionary_2_t28 * L_66 = V_6;
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_66, _stringLiteral1244, ((int32_t)23));
		Dictionary_2_t28 * L_67 = V_6;
		NullCheck(L_67);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_67, _stringLiteral1245, ((int32_t)24));
		Dictionary_2_t28 * L_68 = V_6;
		NullCheck(L_68);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_68, _stringLiteral1246, ((int32_t)25));
		Dictionary_2_t28 * L_69 = V_6;
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_69, _stringLiteral1247, ((int32_t)26));
		Dictionary_2_t28 * L_70 = V_6;
		NullCheck(L_70);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_70, _stringLiteral1248, ((int32_t)27));
		Dictionary_2_t28 * L_71 = V_6;
		NullCheck(L_71);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_71, _stringLiteral1249, ((int32_t)28));
		Dictionary_2_t28 * L_72 = V_6;
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_72, _stringLiteral685, ((int32_t)29));
		Dictionary_2_t28 * L_73 = V_6;
		NullCheck(L_73);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_73, _stringLiteral1250, ((int32_t)30));
		Dictionary_2_t28 * L_74 = V_6;
		NullCheck(L_74);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_74, _stringLiteral1251, ((int32_t)31));
		Dictionary_2_t28 * L_75 = V_6;
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_75, _stringLiteral1252, ((int32_t)32));
		Dictionary_2_t28 * L_76 = V_6;
		NullCheck(L_76);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_76, _stringLiteral1253, ((int32_t)33));
		Dictionary_2_t28 * L_77 = V_6;
		NullCheck(L_77);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_77, _stringLiteral1254, ((int32_t)34));
		Dictionary_2_t28 * L_78 = V_6;
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_78, _stringLiteral1255, ((int32_t)35));
		Dictionary_2_t28 * L_79 = V_6;
		NullCheck(L_79);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_79, _stringLiteral1256, ((int32_t)36));
		Dictionary_2_t28 * L_80 = V_6;
		NullCheck(L_80);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_80, _stringLiteral1257, ((int32_t)37));
		Dictionary_2_t28 * L_81 = V_6;
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_81, _stringLiteral1258, ((int32_t)38));
		Dictionary_2_t28 * L_82 = V_6;
		NullCheck(L_82);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_82, _stringLiteral1259, ((int32_t)39));
		Dictionary_2_t28 * L_83 = V_6;
		NullCheck(L_83);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_83, _stringLiteral1260, ((int32_t)40));
		Dictionary_2_t28 * L_84 = V_6;
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_84, _stringLiteral1261, ((int32_t)41));
		Dictionary_2_t28 * L_85 = V_6;
		NullCheck(L_85);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_85, _stringLiteral1262, ((int32_t)42));
		Dictionary_2_t28 * L_86 = V_6;
		NullCheck(L_86);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_86, _stringLiteral1263, ((int32_t)43));
		Dictionary_2_t28 * L_87 = V_6;
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_87, _stringLiteral1264, ((int32_t)44));
		Dictionary_2_t28 * L_88 = V_6;
		NullCheck(L_88);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_88, _stringLiteral1265, ((int32_t)45));
		Dictionary_2_t28 * L_89 = V_6;
		NullCheck(L_89);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_89, _stringLiteral1266, ((int32_t)46));
		Dictionary_2_t28 * L_90 = V_6;
		NullCheck(L_90);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_90, _stringLiteral1267, ((int32_t)47));
		Dictionary_2_t28 * L_91 = V_6;
		NullCheck(L_91);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_91, _stringLiteral80, ((int32_t)48));
		Dictionary_2_t28 * L_92 = V_6;
		((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3 = L_92;
	}

IL_03b6:
	{
		Dictionary_2_t28 * L_93 = ((Event_t725_StaticFields*)Event_t725_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3;
		String_t* L_94 = V_5;
		NullCheck(L_93);
		bool L_95 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_93, L_94, (&V_7));
		if (!L_95)
		{
			goto IL_09f0;
		}
	}
	{
		int32_t L_96 = V_7;
		if (L_96 == 0)
		{
			goto IL_0499;
		}
		if (L_96 == 1)
		{
			goto IL_04b1;
		}
		if (L_96 == 2)
		{
			goto IL_04c9;
		}
		if (L_96 == 3)
		{
			goto IL_04e1;
		}
		if (L_96 == 4)
		{
			goto IL_04f9;
		}
		if (L_96 == 5)
		{
			goto IL_0511;
		}
		if (L_96 == 6)
		{
			goto IL_0529;
		}
		if (L_96 == 7)
		{
			goto IL_0541;
		}
		if (L_96 == 8)
		{
			goto IL_0559;
		}
		if (L_96 == 9)
		{
			goto IL_0571;
		}
		if (L_96 == 10)
		{
			goto IL_0589;
		}
		if (L_96 == 11)
		{
			goto IL_05a1;
		}
		if (L_96 == 12)
		{
			goto IL_05b9;
		}
		if (L_96 == 13)
		{
			goto IL_05d1;
		}
		if (L_96 == 14)
		{
			goto IL_05e9;
		}
		if (L_96 == 15)
		{
			goto IL_0601;
		}
		if (L_96 == 16)
		{
			goto IL_0619;
		}
		if (L_96 == 17)
		{
			goto IL_0631;
		}
		if (L_96 == 18)
		{
			goto IL_0650;
		}
		if (L_96 == 19)
		{
			goto IL_066f;
		}
		if (L_96 == 20)
		{
			goto IL_068e;
		}
		if (L_96 == 21)
		{
			goto IL_06ad;
		}
		if (L_96 == 22)
		{
			goto IL_06cc;
		}
		if (L_96 == 23)
		{
			goto IL_06eb;
		}
		if (L_96 == 24)
		{
			goto IL_070a;
		}
		if (L_96 == 25)
		{
			goto IL_0729;
		}
		if (L_96 == 26)
		{
			goto IL_0748;
		}
		if (L_96 == 27)
		{
			goto IL_0767;
		}
		if (L_96 == 28)
		{
			goto IL_0786;
		}
		if (L_96 == 29)
		{
			goto IL_07a1;
		}
		if (L_96 == 30)
		{
			goto IL_07bd;
		}
		if (L_96 == 31)
		{
			goto IL_07ca;
		}
		if (L_96 == 32)
		{
			goto IL_07e9;
		}
		if (L_96 == 33)
		{
			goto IL_0808;
		}
		if (L_96 == 34)
		{
			goto IL_0827;
		}
		if (L_96 == 35)
		{
			goto IL_0846;
		}
		if (L_96 == 36)
		{
			goto IL_0865;
		}
		if (L_96 == 37)
		{
			goto IL_0884;
		}
		if (L_96 == 38)
		{
			goto IL_08a3;
		}
		if (L_96 == 39)
		{
			goto IL_08c2;
		}
		if (L_96 == 40)
		{
			goto IL_08e1;
		}
		if (L_96 == 41)
		{
			goto IL_0900;
		}
		if (L_96 == 42)
		{
			goto IL_091f;
		}
		if (L_96 == 43)
		{
			goto IL_093e;
		}
		if (L_96 == 44)
		{
			goto IL_095d;
		}
		if (L_96 == 45)
		{
			goto IL_097c;
		}
		if (L_96 == 46)
		{
			goto IL_099b;
		}
		if (L_96 == 47)
		{
			goto IL_09a8;
		}
		if (L_96 == 48)
		{
			goto IL_09cc;
		}
	}
	{
		goto IL_09f0;
	}

IL_0499:
	{
		Event_t725 * L_97 = V_0;
		NullCheck(L_97);
		Event_set_character_m7795(L_97, ((int32_t)48), /*hidden argument*/NULL);
		Event_t725 * L_98 = V_0;
		NullCheck(L_98);
		Event_set_keyCode_m7798(L_98, ((int32_t)256), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_04b1:
	{
		Event_t725 * L_99 = V_0;
		NullCheck(L_99);
		Event_set_character_m7795(L_99, ((int32_t)49), /*hidden argument*/NULL);
		Event_t725 * L_100 = V_0;
		NullCheck(L_100);
		Event_set_keyCode_m7798(L_100, ((int32_t)257), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_04c9:
	{
		Event_t725 * L_101 = V_0;
		NullCheck(L_101);
		Event_set_character_m7795(L_101, ((int32_t)50), /*hidden argument*/NULL);
		Event_t725 * L_102 = V_0;
		NullCheck(L_102);
		Event_set_keyCode_m7798(L_102, ((int32_t)258), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_04e1:
	{
		Event_t725 * L_103 = V_0;
		NullCheck(L_103);
		Event_set_character_m7795(L_103, ((int32_t)51), /*hidden argument*/NULL);
		Event_t725 * L_104 = V_0;
		NullCheck(L_104);
		Event_set_keyCode_m7798(L_104, ((int32_t)259), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_04f9:
	{
		Event_t725 * L_105 = V_0;
		NullCheck(L_105);
		Event_set_character_m7795(L_105, ((int32_t)52), /*hidden argument*/NULL);
		Event_t725 * L_106 = V_0;
		NullCheck(L_106);
		Event_set_keyCode_m7798(L_106, ((int32_t)260), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0511:
	{
		Event_t725 * L_107 = V_0;
		NullCheck(L_107);
		Event_set_character_m7795(L_107, ((int32_t)53), /*hidden argument*/NULL);
		Event_t725 * L_108 = V_0;
		NullCheck(L_108);
		Event_set_keyCode_m7798(L_108, ((int32_t)261), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0529:
	{
		Event_t725 * L_109 = V_0;
		NullCheck(L_109);
		Event_set_character_m7795(L_109, ((int32_t)54), /*hidden argument*/NULL);
		Event_t725 * L_110 = V_0;
		NullCheck(L_110);
		Event_set_keyCode_m7798(L_110, ((int32_t)262), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0541:
	{
		Event_t725 * L_111 = V_0;
		NullCheck(L_111);
		Event_set_character_m7795(L_111, ((int32_t)55), /*hidden argument*/NULL);
		Event_t725 * L_112 = V_0;
		NullCheck(L_112);
		Event_set_keyCode_m7798(L_112, ((int32_t)263), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0559:
	{
		Event_t725 * L_113 = V_0;
		NullCheck(L_113);
		Event_set_character_m7795(L_113, ((int32_t)56), /*hidden argument*/NULL);
		Event_t725 * L_114 = V_0;
		NullCheck(L_114);
		Event_set_keyCode_m7798(L_114, ((int32_t)264), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0571:
	{
		Event_t725 * L_115 = V_0;
		NullCheck(L_115);
		Event_set_character_m7795(L_115, ((int32_t)57), /*hidden argument*/NULL);
		Event_t725 * L_116 = V_0;
		NullCheck(L_116);
		Event_set_keyCode_m7798(L_116, ((int32_t)265), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0589:
	{
		Event_t725 * L_117 = V_0;
		NullCheck(L_117);
		Event_set_character_m7795(L_117, ((int32_t)46), /*hidden argument*/NULL);
		Event_t725 * L_118 = V_0;
		NullCheck(L_118);
		Event_set_keyCode_m7798(L_118, ((int32_t)266), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_05a1:
	{
		Event_t725 * L_119 = V_0;
		NullCheck(L_119);
		Event_set_character_m7795(L_119, ((int32_t)47), /*hidden argument*/NULL);
		Event_t725 * L_120 = V_0;
		NullCheck(L_120);
		Event_set_keyCode_m7798(L_120, ((int32_t)267), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_05b9:
	{
		Event_t725 * L_121 = V_0;
		NullCheck(L_121);
		Event_set_character_m7795(L_121, ((int32_t)45), /*hidden argument*/NULL);
		Event_t725 * L_122 = V_0;
		NullCheck(L_122);
		Event_set_keyCode_m7798(L_122, ((int32_t)269), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_05d1:
	{
		Event_t725 * L_123 = V_0;
		NullCheck(L_123);
		Event_set_character_m7795(L_123, ((int32_t)43), /*hidden argument*/NULL);
		Event_t725 * L_124 = V_0;
		NullCheck(L_124);
		Event_set_keyCode_m7798(L_124, ((int32_t)270), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_05e9:
	{
		Event_t725 * L_125 = V_0;
		NullCheck(L_125);
		Event_set_character_m7795(L_125, ((int32_t)61), /*hidden argument*/NULL);
		Event_t725 * L_126 = V_0;
		NullCheck(L_126);
		Event_set_keyCode_m7798(L_126, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0601:
	{
		Event_t725 * L_127 = V_0;
		NullCheck(L_127);
		Event_set_character_m7795(L_127, ((int32_t)61), /*hidden argument*/NULL);
		Event_t725 * L_128 = V_0;
		NullCheck(L_128);
		Event_set_keyCode_m7798(L_128, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0619:
	{
		Event_t725 * L_129 = V_0;
		NullCheck(L_129);
		Event_set_character_m7795(L_129, ((int32_t)10), /*hidden argument*/NULL);
		Event_t725 * L_130 = V_0;
		NullCheck(L_130);
		Event_set_keyCode_m7798(L_130, ((int32_t)271), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0631:
	{
		Event_t725 * L_131 = V_0;
		NullCheck(L_131);
		Event_set_keyCode_m7798(L_131, ((int32_t)273), /*hidden argument*/NULL);
		Event_t725 * L_132 = V_0;
		Event_t725 * L_133 = L_132;
		NullCheck(L_133);
		int32_t L_134 = Event_get_modifiers_m4255(L_133, /*hidden argument*/NULL);
		NullCheck(L_133);
		Event_set_modifiers_m7790(L_133, ((int32_t)((int32_t)L_134|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0650:
	{
		Event_t725 * L_135 = V_0;
		NullCheck(L_135);
		Event_set_keyCode_m7798(L_135, ((int32_t)274), /*hidden argument*/NULL);
		Event_t725 * L_136 = V_0;
		Event_t725 * L_137 = L_136;
		NullCheck(L_137);
		int32_t L_138 = Event_get_modifiers_m4255(L_137, /*hidden argument*/NULL);
		NullCheck(L_137);
		Event_set_modifiers_m7790(L_137, ((int32_t)((int32_t)L_138|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_066f:
	{
		Event_t725 * L_139 = V_0;
		NullCheck(L_139);
		Event_set_keyCode_m7798(L_139, ((int32_t)276), /*hidden argument*/NULL);
		Event_t725 * L_140 = V_0;
		Event_t725 * L_141 = L_140;
		NullCheck(L_141);
		int32_t L_142 = Event_get_modifiers_m4255(L_141, /*hidden argument*/NULL);
		NullCheck(L_141);
		Event_set_modifiers_m7790(L_141, ((int32_t)((int32_t)L_142|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_068e:
	{
		Event_t725 * L_143 = V_0;
		NullCheck(L_143);
		Event_set_keyCode_m7798(L_143, ((int32_t)275), /*hidden argument*/NULL);
		Event_t725 * L_144 = V_0;
		Event_t725 * L_145 = L_144;
		NullCheck(L_145);
		int32_t L_146 = Event_get_modifiers_m4255(L_145, /*hidden argument*/NULL);
		NullCheck(L_145);
		Event_set_modifiers_m7790(L_145, ((int32_t)((int32_t)L_146|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_06ad:
	{
		Event_t725 * L_147 = V_0;
		NullCheck(L_147);
		Event_set_keyCode_m7798(L_147, ((int32_t)277), /*hidden argument*/NULL);
		Event_t725 * L_148 = V_0;
		Event_t725 * L_149 = L_148;
		NullCheck(L_149);
		int32_t L_150 = Event_get_modifiers_m4255(L_149, /*hidden argument*/NULL);
		NullCheck(L_149);
		Event_set_modifiers_m7790(L_149, ((int32_t)((int32_t)L_150|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_06cc:
	{
		Event_t725 * L_151 = V_0;
		NullCheck(L_151);
		Event_set_keyCode_m7798(L_151, ((int32_t)278), /*hidden argument*/NULL);
		Event_t725 * L_152 = V_0;
		Event_t725 * L_153 = L_152;
		NullCheck(L_153);
		int32_t L_154 = Event_get_modifiers_m4255(L_153, /*hidden argument*/NULL);
		NullCheck(L_153);
		Event_set_modifiers_m7790(L_153, ((int32_t)((int32_t)L_154|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_06eb:
	{
		Event_t725 * L_155 = V_0;
		NullCheck(L_155);
		Event_set_keyCode_m7798(L_155, ((int32_t)279), /*hidden argument*/NULL);
		Event_t725 * L_156 = V_0;
		Event_t725 * L_157 = L_156;
		NullCheck(L_157);
		int32_t L_158 = Event_get_modifiers_m4255(L_157, /*hidden argument*/NULL);
		NullCheck(L_157);
		Event_set_modifiers_m7790(L_157, ((int32_t)((int32_t)L_158|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_070a:
	{
		Event_t725 * L_159 = V_0;
		NullCheck(L_159);
		Event_set_keyCode_m7798(L_159, ((int32_t)281), /*hidden argument*/NULL);
		Event_t725 * L_160 = V_0;
		Event_t725 * L_161 = L_160;
		NullCheck(L_161);
		int32_t L_162 = Event_get_modifiers_m4255(L_161, /*hidden argument*/NULL);
		NullCheck(L_161);
		Event_set_modifiers_m7790(L_161, ((int32_t)((int32_t)L_162|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0729:
	{
		Event_t725 * L_163 = V_0;
		NullCheck(L_163);
		Event_set_keyCode_m7798(L_163, ((int32_t)280), /*hidden argument*/NULL);
		Event_t725 * L_164 = V_0;
		Event_t725 * L_165 = L_164;
		NullCheck(L_165);
		int32_t L_166 = Event_get_modifiers_m4255(L_165, /*hidden argument*/NULL);
		NullCheck(L_165);
		Event_set_modifiers_m7790(L_165, ((int32_t)((int32_t)L_166|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0748:
	{
		Event_t725 * L_167 = V_0;
		NullCheck(L_167);
		Event_set_keyCode_m7798(L_167, ((int32_t)280), /*hidden argument*/NULL);
		Event_t725 * L_168 = V_0;
		Event_t725 * L_169 = L_168;
		NullCheck(L_169);
		int32_t L_170 = Event_get_modifiers_m4255(L_169, /*hidden argument*/NULL);
		NullCheck(L_169);
		Event_set_modifiers_m7790(L_169, ((int32_t)((int32_t)L_170|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0767:
	{
		Event_t725 * L_171 = V_0;
		NullCheck(L_171);
		Event_set_keyCode_m7798(L_171, ((int32_t)281), /*hidden argument*/NULL);
		Event_t725 * L_172 = V_0;
		Event_t725 * L_173 = L_172;
		NullCheck(L_173);
		int32_t L_174 = Event_get_modifiers_m4255(L_173, /*hidden argument*/NULL);
		NullCheck(L_173);
		Event_set_modifiers_m7790(L_173, ((int32_t)((int32_t)L_174|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0786:
	{
		Event_t725 * L_175 = V_0;
		NullCheck(L_175);
		Event_set_keyCode_m7798(L_175, 8, /*hidden argument*/NULL);
		Event_t725 * L_176 = V_0;
		Event_t725 * L_177 = L_176;
		NullCheck(L_177);
		int32_t L_178 = Event_get_modifiers_m4255(L_177, /*hidden argument*/NULL);
		NullCheck(L_177);
		Event_set_modifiers_m7790(L_177, ((int32_t)((int32_t)L_178|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_07a1:
	{
		Event_t725 * L_179 = V_0;
		NullCheck(L_179);
		Event_set_keyCode_m7798(L_179, ((int32_t)127), /*hidden argument*/NULL);
		Event_t725 * L_180 = V_0;
		Event_t725 * L_181 = L_180;
		NullCheck(L_181);
		int32_t L_182 = Event_get_modifiers_m4255(L_181, /*hidden argument*/NULL);
		NullCheck(L_181);
		Event_set_modifiers_m7790(L_181, ((int32_t)((int32_t)L_182|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_07bd:
	{
		Event_t725 * L_183 = V_0;
		NullCheck(L_183);
		Event_set_keyCode_m7798(L_183, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_07ca:
	{
		Event_t725 * L_184 = V_0;
		NullCheck(L_184);
		Event_set_keyCode_m7798(L_184, ((int32_t)282), /*hidden argument*/NULL);
		Event_t725 * L_185 = V_0;
		Event_t725 * L_186 = L_185;
		NullCheck(L_186);
		int32_t L_187 = Event_get_modifiers_m4255(L_186, /*hidden argument*/NULL);
		NullCheck(L_186);
		Event_set_modifiers_m7790(L_186, ((int32_t)((int32_t)L_187|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_07e9:
	{
		Event_t725 * L_188 = V_0;
		NullCheck(L_188);
		Event_set_keyCode_m7798(L_188, ((int32_t)283), /*hidden argument*/NULL);
		Event_t725 * L_189 = V_0;
		Event_t725 * L_190 = L_189;
		NullCheck(L_190);
		int32_t L_191 = Event_get_modifiers_m4255(L_190, /*hidden argument*/NULL);
		NullCheck(L_190);
		Event_set_modifiers_m7790(L_190, ((int32_t)((int32_t)L_191|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0808:
	{
		Event_t725 * L_192 = V_0;
		NullCheck(L_192);
		Event_set_keyCode_m7798(L_192, ((int32_t)284), /*hidden argument*/NULL);
		Event_t725 * L_193 = V_0;
		Event_t725 * L_194 = L_193;
		NullCheck(L_194);
		int32_t L_195 = Event_get_modifiers_m4255(L_194, /*hidden argument*/NULL);
		NullCheck(L_194);
		Event_set_modifiers_m7790(L_194, ((int32_t)((int32_t)L_195|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0827:
	{
		Event_t725 * L_196 = V_0;
		NullCheck(L_196);
		Event_set_keyCode_m7798(L_196, ((int32_t)285), /*hidden argument*/NULL);
		Event_t725 * L_197 = V_0;
		Event_t725 * L_198 = L_197;
		NullCheck(L_198);
		int32_t L_199 = Event_get_modifiers_m4255(L_198, /*hidden argument*/NULL);
		NullCheck(L_198);
		Event_set_modifiers_m7790(L_198, ((int32_t)((int32_t)L_199|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0846:
	{
		Event_t725 * L_200 = V_0;
		NullCheck(L_200);
		Event_set_keyCode_m7798(L_200, ((int32_t)286), /*hidden argument*/NULL);
		Event_t725 * L_201 = V_0;
		Event_t725 * L_202 = L_201;
		NullCheck(L_202);
		int32_t L_203 = Event_get_modifiers_m4255(L_202, /*hidden argument*/NULL);
		NullCheck(L_202);
		Event_set_modifiers_m7790(L_202, ((int32_t)((int32_t)L_203|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0865:
	{
		Event_t725 * L_204 = V_0;
		NullCheck(L_204);
		Event_set_keyCode_m7798(L_204, ((int32_t)287), /*hidden argument*/NULL);
		Event_t725 * L_205 = V_0;
		Event_t725 * L_206 = L_205;
		NullCheck(L_206);
		int32_t L_207 = Event_get_modifiers_m4255(L_206, /*hidden argument*/NULL);
		NullCheck(L_206);
		Event_set_modifiers_m7790(L_206, ((int32_t)((int32_t)L_207|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0884:
	{
		Event_t725 * L_208 = V_0;
		NullCheck(L_208);
		Event_set_keyCode_m7798(L_208, ((int32_t)288), /*hidden argument*/NULL);
		Event_t725 * L_209 = V_0;
		Event_t725 * L_210 = L_209;
		NullCheck(L_210);
		int32_t L_211 = Event_get_modifiers_m4255(L_210, /*hidden argument*/NULL);
		NullCheck(L_210);
		Event_set_modifiers_m7790(L_210, ((int32_t)((int32_t)L_211|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_08a3:
	{
		Event_t725 * L_212 = V_0;
		NullCheck(L_212);
		Event_set_keyCode_m7798(L_212, ((int32_t)289), /*hidden argument*/NULL);
		Event_t725 * L_213 = V_0;
		Event_t725 * L_214 = L_213;
		NullCheck(L_214);
		int32_t L_215 = Event_get_modifiers_m4255(L_214, /*hidden argument*/NULL);
		NullCheck(L_214);
		Event_set_modifiers_m7790(L_214, ((int32_t)((int32_t)L_215|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_08c2:
	{
		Event_t725 * L_216 = V_0;
		NullCheck(L_216);
		Event_set_keyCode_m7798(L_216, ((int32_t)290), /*hidden argument*/NULL);
		Event_t725 * L_217 = V_0;
		Event_t725 * L_218 = L_217;
		NullCheck(L_218);
		int32_t L_219 = Event_get_modifiers_m4255(L_218, /*hidden argument*/NULL);
		NullCheck(L_218);
		Event_set_modifiers_m7790(L_218, ((int32_t)((int32_t)L_219|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_08e1:
	{
		Event_t725 * L_220 = V_0;
		NullCheck(L_220);
		Event_set_keyCode_m7798(L_220, ((int32_t)291), /*hidden argument*/NULL);
		Event_t725 * L_221 = V_0;
		Event_t725 * L_222 = L_221;
		NullCheck(L_222);
		int32_t L_223 = Event_get_modifiers_m4255(L_222, /*hidden argument*/NULL);
		NullCheck(L_222);
		Event_set_modifiers_m7790(L_222, ((int32_t)((int32_t)L_223|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_0900:
	{
		Event_t725 * L_224 = V_0;
		NullCheck(L_224);
		Event_set_keyCode_m7798(L_224, ((int32_t)292), /*hidden argument*/NULL);
		Event_t725 * L_225 = V_0;
		Event_t725 * L_226 = L_225;
		NullCheck(L_226);
		int32_t L_227 = Event_get_modifiers_m4255(L_226, /*hidden argument*/NULL);
		NullCheck(L_226);
		Event_set_modifiers_m7790(L_226, ((int32_t)((int32_t)L_227|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_091f:
	{
		Event_t725 * L_228 = V_0;
		NullCheck(L_228);
		Event_set_keyCode_m7798(L_228, ((int32_t)293), /*hidden argument*/NULL);
		Event_t725 * L_229 = V_0;
		Event_t725 * L_230 = L_229;
		NullCheck(L_230);
		int32_t L_231 = Event_get_modifiers_m4255(L_230, /*hidden argument*/NULL);
		NullCheck(L_230);
		Event_set_modifiers_m7790(L_230, ((int32_t)((int32_t)L_231|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_093e:
	{
		Event_t725 * L_232 = V_0;
		NullCheck(L_232);
		Event_set_keyCode_m7798(L_232, ((int32_t)294), /*hidden argument*/NULL);
		Event_t725 * L_233 = V_0;
		Event_t725 * L_234 = L_233;
		NullCheck(L_234);
		int32_t L_235 = Event_get_modifiers_m4255(L_234, /*hidden argument*/NULL);
		NullCheck(L_234);
		Event_set_modifiers_m7790(L_234, ((int32_t)((int32_t)L_235|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_095d:
	{
		Event_t725 * L_236 = V_0;
		NullCheck(L_236);
		Event_set_keyCode_m7798(L_236, ((int32_t)295), /*hidden argument*/NULL);
		Event_t725 * L_237 = V_0;
		Event_t725 * L_238 = L_237;
		NullCheck(L_238);
		int32_t L_239 = Event_get_modifiers_m4255(L_238, /*hidden argument*/NULL);
		NullCheck(L_238);
		Event_set_modifiers_m7790(L_238, ((int32_t)((int32_t)L_239|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_097c:
	{
		Event_t725 * L_240 = V_0;
		NullCheck(L_240);
		Event_set_keyCode_m7798(L_240, ((int32_t)296), /*hidden argument*/NULL);
		Event_t725 * L_241 = V_0;
		Event_t725 * L_242 = L_241;
		NullCheck(L_242);
		int32_t L_243 = Event_get_modifiers_m4255(L_242, /*hidden argument*/NULL);
		NullCheck(L_242);
		Event_set_modifiers_m7790(L_242, ((int32_t)((int32_t)L_243|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_099b:
	{
		Event_t725 * L_244 = V_0;
		NullCheck(L_244);
		Event_set_keyCode_m7798(L_244, ((int32_t)27), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_09a8:
	{
		Event_t725 * L_245 = V_0;
		NullCheck(L_245);
		Event_set_character_m7795(L_245, ((int32_t)10), /*hidden argument*/NULL);
		Event_t725 * L_246 = V_0;
		NullCheck(L_246);
		Event_set_keyCode_m7798(L_246, ((int32_t)13), /*hidden argument*/NULL);
		Event_t725 * L_247 = V_0;
		Event_t725 * L_248 = L_247;
		NullCheck(L_248);
		int32_t L_249 = Event_get_modifiers_m4255(L_248, /*hidden argument*/NULL);
		NullCheck(L_248);
		Event_set_modifiers_m7790(L_248, ((int32_t)((int32_t)L_249&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_09cc:
	{
		Event_t725 * L_250 = V_0;
		NullCheck(L_250);
		Event_set_keyCode_m7798(L_250, ((int32_t)32), /*hidden argument*/NULL);
		Event_t725 * L_251 = V_0;
		NullCheck(L_251);
		Event_set_character_m7795(L_251, ((int32_t)32), /*hidden argument*/NULL);
		Event_t725 * L_252 = V_0;
		Event_t725 * L_253 = L_252;
		NullCheck(L_253);
		int32_t L_254 = Event_get_modifiers_m4255(L_253, /*hidden argument*/NULL);
		NullCheck(L_253);
		Event_set_modifiers_m7790(L_253, ((int32_t)((int32_t)L_254&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a76;
	}

IL_09f0:
	{
		String_t* L_255 = V_3;
		NullCheck(L_255);
		int32_t L_256 = String_get_Length_m410(L_255, /*hidden argument*/NULL);
		if ((((int32_t)L_256) == ((int32_t)1)))
		{
			goto IL_0a41;
		}
	}

IL_09fc:
	try
	{ // begin try (depth: 1)
		Event_t725 * L_257 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_258 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(KeyCode_t1401_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_259 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t63_il2cpp_TypeInfo_var);
		Object_t * L_260 = Enum_Parse_m429(NULL /*static, unused*/, L_258, L_259, 1, /*hidden argument*/NULL);
		NullCheck(L_257);
		Event_set_keyCode_m7798(L_257, ((*(int32_t*)((int32_t*)UnBox (L_260, Int32_t59_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0a3c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t40 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t513_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0a1d;
		throw e;
	}

CATCH_0a1d:
	{ // begin catch(System.ArgumentException)
		ObjectU5BU5D_t34* L_261 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 1));
		String_t* L_262 = V_3;
		NullCheck(L_261);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_261, 0);
		ArrayElementTypeCheck (L_261, L_262);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_261, 0, sizeof(Object_t *))) = (Object_t *)L_262;
		String_t* L_263 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1268, L_261, /*hidden argument*/NULL);
		Debug_LogError_m302(NULL /*static, unused*/, L_263, /*hidden argument*/NULL);
		goto IL_0a3c;
	} // end catch (depth: 1)

IL_0a3c:
	{
		goto IL_0a71;
	}

IL_0a41:
	{
		Event_t725 * L_264 = V_0;
		String_t* L_265 = V_3;
		NullCheck(L_265);
		String_t* L_266 = String_ToLower_m412(L_265, /*hidden argument*/NULL);
		NullCheck(L_266);
		uint16_t L_267 = String_get_Chars_m2170(L_266, 0, /*hidden argument*/NULL);
		NullCheck(L_264);
		Event_set_character_m7795(L_264, L_267, /*hidden argument*/NULL);
		Event_t725 * L_268 = V_0;
		Event_t725 * L_269 = V_0;
		NullCheck(L_269);
		uint16_t L_270 = Event_get_character_m4257(L_269, /*hidden argument*/NULL);
		NullCheck(L_268);
		Event_set_keyCode_m7798(L_268, L_270, /*hidden argument*/NULL);
		Event_t725 * L_271 = V_0;
		NullCheck(L_271);
		int32_t L_272 = Event_get_modifiers_m4255(L_271, /*hidden argument*/NULL);
		if (!L_272)
		{
			goto IL_0a71;
		}
	}
	{
		Event_t725 * L_273 = V_0;
		NullCheck(L_273);
		Event_set_character_m7795(L_273, 0, /*hidden argument*/NULL);
	}

IL_0a71:
	{
		goto IL_0a76;
	}

IL_0a76:
	{
		Event_t725 * L_274 = V_0;
		return L_274;
	}
}
// System.Int32 UnityEngine.Event::GetHashCode()
extern "C" int32_t Event_GetHashCode_m7821 (Event_t725 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t29  V_1 = {0};
	{
		V_0 = 1;
		bool L_0 = Event_get_isKey_m7818(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = Event_get_keyCode_m4256(__this, /*hidden argument*/NULL);
		V_0 = (((int32_t)((uint16_t)L_1)));
	}

IL_0015:
	{
		bool L_2 = Event_get_isMouse_m7819(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector2_t29  L_3 = Event_get_mousePosition_m7776(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Vector2_GetHashCode_m7836((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)((int32_t)37)))|(int32_t)L_6));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Event::Equals(System.Object)
extern TypeInfo* Event_t725_il2cpp_TypeInfo_var;
extern "C" bool Event_Equals_m7822 (Event_t725 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(551);
		s_Il2CppMethodIntialized = true;
	}
	Event_t725 * V_0 = {0};
	int32_t G_B13_0 = 0;
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		bool L_2 = Object_ReferenceEquals_m8193(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		Object_t * L_3 = ___obj;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m303(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = Object_GetType_m303(__this, /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return 0;
	}

IL_0029:
	{
		Object_t * L_6 = ___obj;
		V_0 = ((Event_t725 *)CastclassSealed(L_6, Event_t725_il2cpp_TypeInfo_var));
		int32_t L_7 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		Event_t725 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m7773(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)L_9))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_10 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_t725 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = Event_get_modifiers_m4255(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)L_12)))
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		return 0;
	}

IL_0054:
	{
		bool L_13 = Event_get_isKey_m7818(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0082;
		}
	}
	{
		int32_t L_14 = Event_get_keyCode_m4256(__this, /*hidden argument*/NULL);
		Event_t725 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = Event_get_keyCode_m4256(L_15, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)L_16))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_17 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		Event_t725 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = Event_get_modifiers_m4255(L_18, /*hidden argument*/NULL);
		G_B13_0 = ((((int32_t)L_17) == ((int32_t)L_19))? 1 : 0);
		goto IL_0081;
	}

IL_0080:
	{
		G_B13_0 = 0;
	}

IL_0081:
	{
		return G_B13_0;
	}

IL_0082:
	{
		bool L_20 = Event_get_isMouse_m7819(__this, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_009f;
		}
	}
	{
		Vector2_t29  L_21 = Event_get_mousePosition_m7776(__this, /*hidden argument*/NULL);
		Event_t725 * L_22 = V_0;
		NullCheck(L_22);
		Vector2_t29  L_23 = Event_get_mousePosition_m7776(L_22, /*hidden argument*/NULL);
		bool L_24 = Vector2_op_Equality_m4436(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		return L_24;
	}

IL_009f:
	{
		return 0;
	}
}
// System.String UnityEngine.Event::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t1402_il2cpp_TypeInfo_var;
extern TypeInfo* EventModifiers_t1403_il2cpp_TypeInfo_var;
extern TypeInfo* KeyCode_t1401_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t59_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t29_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1269;
extern Il2CppCodeGenString* _stringLiteral1270;
extern Il2CppCodeGenString* _stringLiteral1271;
extern Il2CppCodeGenString* _stringLiteral1272;
extern Il2CppCodeGenString* _stringLiteral1273;
extern Il2CppCodeGenString* _stringLiteral1274;
extern Il2CppCodeGenString* _stringLiteral1275;
extern "C" String_t* Event_ToString_m7823 (Event_t725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		EventType_t1402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(959);
		EventModifiers_t1403_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(972);
		KeyCode_t1401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(971);
		Int32_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Vector2_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral1269 = il2cpp_codegen_string_literal_from_index(1269);
		_stringLiteral1270 = il2cpp_codegen_string_literal_from_index(1270);
		_stringLiteral1271 = il2cpp_codegen_string_literal_from_index(1271);
		_stringLiteral1272 = il2cpp_codegen_string_literal_from_index(1272);
		_stringLiteral1273 = il2cpp_codegen_string_literal_from_index(1273);
		_stringLiteral1274 = il2cpp_codegen_string_literal_from_index(1274);
		_stringLiteral1275 = il2cpp_codegen_string_literal_from_index(1275);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Event_get_isKey_m7818(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00c0;
		}
	}
	{
		uint16_t L_1 = Event_get_character_m4257(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		ObjectU5BU5D_t34* L_2 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 3));
		int32_t L_3 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t34* L_6 = L_2;
		int32_t L_7 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(EventModifiers_t1403_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t34* L_10 = L_6;
		int32_t L_11 = Event_get_keyCode_m4256(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(KeyCode_t1401_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 2, sizeof(Object_t *))) = (Object_t *)L_13;
		String_t* L_14 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1269, L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0051:
	{
		ObjectU5BU5D_t34* L_15 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 8));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral1270);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral1270;
		ObjectU5BU5D_t34* L_16 = L_15;
		int32_t L_17 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 1, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t34* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, _stringLiteral1271);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral1271;
		ObjectU5BU5D_t34* L_21 = L_20;
		uint16_t L_22 = Event_get_character_m4257(__this, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(Int32_t59_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		ArrayElementTypeCheck (L_21, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 3, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t34* L_25 = L_21;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, _stringLiteral1272);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral1272;
		ObjectU5BU5D_t34* L_26 = L_25;
		int32_t L_27 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		Object_t * L_29 = Box(EventModifiers_t1403_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		ArrayElementTypeCheck (L_26, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_26, 5, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t34* L_30 = L_26;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 6);
		ArrayElementTypeCheck (L_30, _stringLiteral1273);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral1273;
		ObjectU5BU5D_t34* L_31 = L_30;
		int32_t L_32 = Event_get_keyCode_m4256(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Object_t * L_34 = Box(KeyCode_t1401_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 7);
		ArrayElementTypeCheck (L_31, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 7, sizeof(Object_t *))) = (Object_t *)L_34;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m2254(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		String_t* L_36 = UnityString_Format_m7985(NULL /*static, unused*/, L_35, ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return L_36;
	}

IL_00c0:
	{
		bool L_37 = Event_get_isMouse_m7819(__this, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0106;
		}
	}
	{
		ObjectU5BU5D_t34* L_38 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 3));
		int32_t L_39 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		int32_t L_40 = L_39;
		Object_t * L_41 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		ArrayElementTypeCheck (L_38, L_41);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_38, 0, sizeof(Object_t *))) = (Object_t *)L_41;
		ObjectU5BU5D_t34* L_42 = L_38;
		Vector2_t29  L_43 = Event_get_mousePosition_m7776(__this, /*hidden argument*/NULL);
		Vector2_t29  L_44 = L_43;
		Object_t * L_45 = Box(Vector2_t29_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 1);
		ArrayElementTypeCheck (L_42, L_45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 1, sizeof(Object_t *))) = (Object_t *)L_45;
		ObjectU5BU5D_t34* L_46 = L_42;
		int32_t L_47 = Event_get_modifiers_m4255(__this, /*hidden argument*/NULL);
		int32_t L_48 = L_47;
		Object_t * L_49 = Box(EventModifiers_t1403_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 2);
		ArrayElementTypeCheck (L_46, L_49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_46, 2, sizeof(Object_t *))) = (Object_t *)L_49;
		String_t* L_50 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1274, L_46, /*hidden argument*/NULL);
		return L_50;
	}

IL_0106:
	{
		int32_t L_51 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_51) == ((int32_t)((int32_t)14))))
		{
			goto IL_0120;
		}
	}
	{
		int32_t L_52 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_52) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0148;
		}
	}

IL_0120:
	{
		ObjectU5BU5D_t34* L_53 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 2));
		int32_t L_54 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		int32_t L_55 = L_54;
		Object_t * L_56 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_55);
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
		ArrayElementTypeCheck (L_53, L_56);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_53, 0, sizeof(Object_t *))) = (Object_t *)L_56;
		ObjectU5BU5D_t34* L_57 = L_53;
		String_t* L_58 = Event_get_commandName_m7796(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 1);
		ArrayElementTypeCheck (L_57, L_58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_57, 1, sizeof(Object_t *))) = (Object_t *)L_58;
		String_t* L_59 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1275, L_57, /*hidden argument*/NULL);
		return L_59;
	}

IL_0148:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		int32_t L_61 = Event_get_type_m7773(__this, /*hidden argument*/NULL);
		int32_t L_62 = L_61;
		Object_t * L_63 = Box(EventType_t1402_il2cpp_TypeInfo_var, &L_62);
		String_t* L_64 = String_Concat_m2203(NULL /*static, unused*/, L_60, L_63, /*hidden argument*/NULL);
		return L_64;
	}
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t725_marshal(const Event_t725& unmarshaled, Event_t725_marshaled& marshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___s_Current_1Exception);
}
extern "C" void Event_t725_marshal_back(const Event_t725_marshaled& marshaled, Event_t725& unmarshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___s_Current_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t725_marshal_cleanup(Event_t725_marshaled& marshaled)
{
}
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawLine_m419 (Object_t * __this /* static, unused */, Vector3_t6  ___from, Vector3_t6  ___to, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_CALL_DrawLine_m7824(NULL /*static, unused*/, (&___from), (&___to), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawLine_m7824 (Object_t * __this /* static, unused */, Vector3_t6 * ___from, Vector3_t6 * ___to, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawLine_m7824_ftn) (Vector3_t6 *, Vector3_t6 *);
	static Gizmos_INTERNAL_CALL_DrawLine_m7824_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawLine_m7824_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___from, ___to);
}
// System.Void UnityEngine.Gizmos::DrawSphere(UnityEngine.Vector3,System.Single)
extern "C" void Gizmos_DrawSphere_m2666 (Object_t * __this /* static, unused */, Vector3_t6  ___center, float ___radius, const MethodInfo* method)
{
	{
		float L_0 = ___radius;
		Gizmos_INTERNAL_CALL_DrawSphere_m7825(NULL /*static, unused*/, (&___center), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)
extern "C" void Gizmos_INTERNAL_CALL_DrawSphere_m7825 (Object_t * __this /* static, unused */, Vector3_t6 * ___center, float ___radius, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawSphere_m7825_ftn) (Vector3_t6 *, float);
	static Gizmos_INTERNAL_CALL_DrawSphere_m7825_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawSphere_m7825_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___center, ___radius);
}
// System.Void UnityEngine.Gizmos::DrawWireCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawWireCube_m6475 (Object_t * __this /* static, unused */, Vector3_t6  ___center, Vector3_t6  ___size, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_CALL_DrawWireCube_m7826(NULL /*static, unused*/, (&___center), (&___size), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawWireCube_m7826 (Object_t * __this /* static, unused */, Vector3_t6 * ___center, Vector3_t6 * ___size, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawWireCube_m7826_ftn) (Vector3_t6 *, Vector3_t6 *);
	static Gizmos_INTERNAL_CALL_DrawWireCube_m7826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawWireCube_m7826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___center, ___size);
}
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawCube_m6480 (Object_t * __this /* static, unused */, Vector3_t6  ___center, Vector3_t6  ___size, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_CALL_DrawCube_m7827(NULL /*static, unused*/, (&___center), (&___size), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawCube_m7827 (Object_t * __this /* static, unused */, Vector3_t6 * ___center, Vector3_t6 * ___size, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawCube_m7827_ftn) (Vector3_t6 *, Vector3_t6 *);
	static Gizmos_INTERNAL_CALL_DrawCube_m7827_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawCube_m7827_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___center, ___size);
}
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C" void Gizmos_set_color_m418 (Object_t * __this /* static, unused */, Color_t5  ___value, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_set_color_m7828(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Gizmos_INTERNAL_set_color_m7828 (Object_t * __this /* static, unused */, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_set_color_m7828_ftn) (Color_t5 *);
	static Gizmos_INTERNAL_set_color_m7828_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_set_color_m7828_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Gizmos::set_matrix(UnityEngine.Matrix4x4)
extern "C" void Gizmos_set_matrix_m2792 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___value, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_set_matrix_m7829(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_set_matrix(UnityEngine.Matrix4x4&)
extern "C" void Gizmos_INTERNAL_set_matrix_m7829 (Object_t * __this /* static, unused */, Matrix4x4_t603 * ___value, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_set_matrix_m7829_ftn) (Matrix4x4_t603 *);
	static Gizmos_INTERNAL_set_matrix_m7829_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_set_matrix_m7829_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_set_matrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.UnhandledExceptionHandler::.ctor()
extern "C" void UnhandledExceptionHandler__ctor_m7830 (UnhandledExceptionHandler_t1406 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern TypeInfo* UnhandledExceptionEventHandler_t1475_il2cpp_TypeInfo_var;
extern const MethodInfo* UnhandledExceptionHandler_HandleUnhandledException_m7832_MethodInfo_var;
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m7831 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnhandledExceptionEventHandler_t1475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(973);
		UnhandledExceptionHandler_HandleUnhandledException_m7832_MethodInfo_var = il2cpp_codegen_method_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	{
		AppDomain_t1474 * L_0 = AppDomain_get_CurrentDomain_m8194(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = { (void*)UnhandledExceptionHandler_HandleUnhandledException_m7832_MethodInfo_var };
		UnhandledExceptionEventHandler_t1475 * L_2 = (UnhandledExceptionEventHandler_t1475 *)il2cpp_codegen_object_new (UnhandledExceptionEventHandler_t1475_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m8195(L_2, NULL, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AppDomain_add_UnhandledException_m8196(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern TypeInfo* Exception_t40_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1276;
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m7832 (Object_t * __this /* static, unused */, Object_t * ___sender, UnhandledExceptionEventArgs_t1445 * ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(212);
		_stringLiteral1276 = il2cpp_codegen_string_literal_from_index(1276);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t40 * V_0 = {0};
	{
		UnhandledExceptionEventArgs_t1445 * L_0 = ___args;
		NullCheck(L_0);
		Object_t * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m8197(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t40 *)IsInstClass(L_1, Exception_t40_il2cpp_TypeInfo_var));
		Exception_t40 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Exception_t40 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m7833(NULL /*static, unused*/, _stringLiteral1276, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m7834(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1277;
extern "C" void UnhandledExceptionHandler_PrintException_m7833 (Object_t * __this /* static, unused */, String_t* ___title, Exception_t40 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1277 = il2cpp_codegen_string_literal_from_index(1277);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		Exception_t40 * L_1 = ___e;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m409(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Debug_LogError_m302(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Exception_t40 * L_4 = ___e;
		NullCheck(L_4);
		Exception_t40 * L_5 = (Exception_t40 *)VirtFuncInvoker0< Exception_t40 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_4);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Exception_t40 * L_6 = ___e;
		NullCheck(L_6);
		Exception_t40 * L_7 = (Exception_t40 *)VirtFuncInvoker0< Exception_t40 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_6);
		UnhandledExceptionHandler_PrintException_m7833(NULL /*static, unused*/, _stringLiteral1277, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m7834 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m7834_ftn) ();
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m7834_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m7834_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" void Vector2__ctor_m356 (Vector2_t29 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		return;
	}
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1278;
extern "C" float Vector2_get_Item_m4203 (Vector2_t29 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1278 = il2cpp_codegen_string_literal_from_index(1278);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		goto IL_0022;
	}

IL_0014:
	{
		float L_3 = (__this->___x_1);
		return L_3;
	}

IL_001b:
	{
		float L_4 = (__this->___y_2);
		return L_4;
	}

IL_0022:
	{
		IndexOutOfRangeException_t1476 * L_5 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_5, _stringLiteral1278, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}
}
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1278;
extern "C" void Vector2_set_Item_m4212 (Vector2_t29 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1278 = il2cpp_codegen_string_literal_from_index(1278);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		goto IL_002c;
	}

IL_0014:
	{
		float L_3 = ___value;
		__this->___x_1 = L_3;
		goto IL_0037;
	}

IL_0020:
	{
		float L_4 = ___value;
		__this->___y_2 = L_4;
		goto IL_0037;
	}

IL_002c:
	{
		IndexOutOfRangeException_t1476 * L_5 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_5, _stringLiteral1278, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0037:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t29  Vector2_Scale_m4288 (Object_t * __this /* static, unused */, Vector2_t29  ___a, Vector2_t29  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t29  L_4 = {0};
		Vector2__ctor_m356(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.Vector2::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1279;
extern "C" String_t* Vector2_ToString_m7835 (Vector2_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1279 = il2cpp_codegen_string_literal_from_index(1279);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 2));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t36_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t36_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1279, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C" int32_t Vector2_GetHashCode_m7836 (Vector2_t29 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m8176(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m8176(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern TypeInfo* Vector2_t29_il2cpp_TypeInfo_var;
extern "C" bool Vector2_Equals_m7837 (Vector2_t29 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t29  V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector2_t29_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector2_t29 *)((Vector2_t29 *)UnBox (L_1, Vector2_t29_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m8199(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m8199(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		return G_B5_0;
	}
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C" float Vector2_get_sqrMagnitude_m4032 (Vector2_t29 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::SqrMagnitude(UnityEngine.Vector2)
extern "C" float Vector2_SqrMagnitude_m7838 (Object_t * __this /* static, unused */, Vector2_t29  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" Vector2_t29  Vector2_get_zero_m4028 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = {0};
		Vector2__ctor_m356(&L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C" Vector2_t29  Vector2_get_one_m4200 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = {0};
		Vector2__ctor_m356(&L_0, (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C" Vector2_t29  Vector2_get_up_m4407 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = {0};
		Vector2__ctor_m356(&L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t29  Vector2_op_Addition_m391 (Object_t * __this /* static, unused */, Vector2_t29  ___a, Vector2_t29  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t29  L_4 = {0};
		Vector2__ctor_m356(&L_4, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t29  Vector2_op_Subtraction_m389 (Object_t * __this /* static, unused */, Vector2_t29  ___a, Vector2_t29  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t29  L_4 = {0};
		Vector2__ctor_m356(&L_4, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" Vector2_t29  Vector2_op_Multiply_m390 (Object_t * __this /* static, unused */, Vector2_t29  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		Vector2_t29  L_4 = {0};
		Vector2__ctor_m356(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C" Vector2_t29  Vector2_op_Division_m4254 (Object_t * __this /* static, unused */, Vector2_t29  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		Vector2_t29  L_4 = {0};
		Vector2__ctor_m356(&L_4, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" bool Vector2_op_Equality_m4436 (Object_t * __this /* static, unused */, Vector2_t29  ___lhs, Vector2_t29  ___rhs, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = ___lhs;
		Vector2_t29  L_1 = ___rhs;
		Vector2_t29  L_2 = Vector2_op_Subtraction_m389(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m7838(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" bool Vector2_op_Inequality_m4281 (Object_t * __this /* static, unused */, Vector2_t29  ___lhs, Vector2_t29  ___rhs, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = ___lhs;
		Vector2_t29  L_1 = ___rhs;
		Vector2_t29  L_2 = Vector2_op_Subtraction_m389(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m7838(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" Vector2_t29  Vector2_op_Implicit_m4043 (Object_t * __this /* static, unused */, Vector3_t6  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector2_t29  L_2 = {0};
		Vector2__ctor_m356(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" Vector3_t6  Vector2_op_Implicit_m4082 (Object_t * __this /* static, unused */, Vector2_t29  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector3_t6  L_2 = {0};
		Vector3__ctor_m335(&L_2, L_0, L_1, (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" void Vector3__ctor_m335 (Vector3_t6 * __this, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		return;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C" void Vector3__ctor_m4145 (Vector3_t6 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		__this->___z_3 = (0.0f);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6  Vector3_Lerp_m2613 (Object_t * __this /* static, unused */, Vector3_t6  ___from, Vector3_t6  ___to, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		float L_1 = Mathf_Clamp01_m435(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t = L_1;
		float L_2 = ((&___from)->___x_1);
		float L_3 = ((&___to)->___x_1);
		float L_4 = ((&___from)->___x_1);
		float L_5 = ___t;
		float L_6 = ((&___from)->___y_2);
		float L_7 = ((&___to)->___y_2);
		float L_8 = ((&___from)->___y_2);
		float L_9 = ___t;
		float L_10 = ((&___from)->___z_3);
		float L_11 = ((&___to)->___z_3);
		float L_12 = ((&___from)->___z_3);
		float L_13 = ___t;
		Vector3_t6  L_14 = {0};
		Vector3__ctor_m335(&L_14, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1280;
extern "C" float Vector3_get_Item_m4325 (Vector3_t6 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1280 = il2cpp_codegen_string_literal_from_index(1280);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0020;
		}
		if (L_1 == 2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		float L_2 = (__this->___x_1);
		return L_2;
	}

IL_0020:
	{
		float L_3 = (__this->___y_2);
		return L_3;
	}

IL_0027:
	{
		float L_4 = (__this->___z_3);
		return L_4;
	}

IL_002e:
	{
		IndexOutOfRangeException_t1476 * L_5 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_5, _stringLiteral1280, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}
}
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1280;
extern "C" void Vector3_set_Item_m4326 (Vector3_t6 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1280 = il2cpp_codegen_string_literal_from_index(1280);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_003d;
	}

IL_0019:
	{
		float L_2 = ___value;
		__this->___x_1 = L_2;
		goto IL_0048;
	}

IL_0025:
	{
		float L_3 = ___value;
		__this->___y_2 = L_3;
		goto IL_0048;
	}

IL_0031:
	{
		float L_4 = ___value;
		__this->___z_3 = L_4;
		goto IL_0048;
	}

IL_003d:
	{
		IndexOutOfRangeException_t1476 * L_5 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_5, _stringLiteral1280, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0048:
	{
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_Scale_m365 (Object_t * __this /* static, unused */, Vector3_t6  ___a, Vector3_t6  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t6  L_6 = {0};
		Vector3__ctor_m335(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Vector3::Scale(UnityEngine.Vector3)
extern "C" void Vector3_Scale_m2567 (Vector3_t6 * __this, Vector3_t6  ___scale, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = ((&___scale)->___x_1);
		__this->___x_1 = ((float)((float)L_0*(float)L_1));
		float L_2 = (__this->___y_2);
		float L_3 = ((&___scale)->___y_2);
		__this->___y_2 = ((float)((float)L_2*(float)L_3));
		float L_4 = (__this->___z_3);
		float L_5 = ((&___scale)->___z_3);
		__this->___z_3 = ((float)((float)L_4*(float)L_5));
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_Cross_m6602 (Object_t * __this /* static, unused */, Vector3_t6  ___lhs, Vector3_t6  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___y_2);
		float L_1 = ((&___rhs)->___z_3);
		float L_2 = ((&___lhs)->___z_3);
		float L_3 = ((&___rhs)->___y_2);
		float L_4 = ((&___lhs)->___z_3);
		float L_5 = ((&___rhs)->___x_1);
		float L_6 = ((&___lhs)->___x_1);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = ((&___lhs)->___x_1);
		float L_9 = ((&___rhs)->___y_2);
		float L_10 = ((&___lhs)->___y_2);
		float L_11 = ((&___rhs)->___x_1);
		Vector3_t6  L_12 = {0};
		Vector3__ctor_m335(&L_12, ((float)((float)((float)((float)L_0*(float)L_1))-(float)((float)((float)L_2*(float)L_3)))), ((float)((float)((float)((float)L_4*(float)L_5))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)L_8*(float)L_9))-(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C" int32_t Vector3_GetHashCode_m7839 (Vector3_t6 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m8176(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m8176(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m8176(L_4, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern TypeInfo* Vector3_t6_il2cpp_TypeInfo_var;
extern "C" bool Vector3_Equals_m7840 (Vector3_t6 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6  V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector3_t6_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector3_t6 *)((Vector3_t6 *)UnBox (L_1, Vector3_t6_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m8199(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m8199(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m8199(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_0057;
	}

IL_0056:
	{
		G_B6_0 = 0;
	}

IL_0057:
	{
		return G_B6_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_Normalize_m7841 (Object_t * __this /* static, unused */, Vector3_t6  ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t6  L_0 = ___value;
		float L_1 = Vector3_Magnitude_m7845(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_001a;
		}
	}
	{
		Vector3_t6  L_3 = ___value;
		float L_4 = V_0;
		Vector3_t6  L_5 = Vector3_op_Division_m6474(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		Vector3_t6  L_6 = Vector3_get_zero_m300(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Vector3::Normalize()
extern "C" void Vector3_Normalize_m7842 (Vector3_t6 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Vector3_Magnitude_m7845(NULL /*static, unused*/, (*(Vector3_t6 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_002e;
		}
	}
	{
		float L_2 = V_0;
		Vector3_t6  L_3 = Vector3_op_Division_m6474(NULL /*static, unused*/, (*(Vector3_t6 *)__this), L_2, /*hidden argument*/NULL);
		(*(Vector3_t6 *)__this) = L_3;
		goto IL_0039;
	}

IL_002e:
	{
		Vector3_t6  L_4 = Vector3_get_zero_m300(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector3_t6 *)__this) = L_4;
	}

IL_0039:
	{
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" Vector3_t6  Vector3_get_normalized_m2574 (Vector3_t6 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Vector3_Normalize_m7841(NULL /*static, unused*/, (*(Vector3_t6 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Vector3::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1281;
extern "C" String_t* Vector3_ToString_m7843 (Vector3_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1281 = il2cpp_codegen_string_literal_from_index(1281);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 3));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t36_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t36_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t36_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1281, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String UnityEngine.Vector3::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1282;
extern "C" String_t* Vector3_ToString_m7844 (Vector3_t6 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1282 = il2cpp_codegen_string_literal_from_index(1282);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 3));
		float* L_1 = &(__this->___x_1);
		String_t* L_2 = ___format;
		String_t* L_3 = Single_ToString_m8200(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float* L_5 = &(__this->___y_2);
		String_t* L_6 = ___format;
		String_t* L_7 = Single_ToString_m8200(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		float* L_9 = &(__this->___z_3);
		String_t* L_10 = ___format;
		String_t* L_11 = Single_ToString_m8200(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1282, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" float Vector3_Dot_m4168 (Object_t * __this /* static, unused */, Vector3_t6  ___lhs, Vector3_t6  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___y_2);
		float L_3 = ((&___rhs)->___y_2);
		float L_4 = ((&___lhs)->___z_3);
		float L_5 = ((&___rhs)->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" float Vector3_Angle_m6468 (Object_t * __this /* static, unused */, Vector3_t6  ___from, Vector3_t6  ___to, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Vector3_get_normalized_m2574((&___from), /*hidden argument*/NULL);
		Vector3_t6  L_1 = Vector3_get_normalized_m2574((&___to), /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m4168(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Mathf_Clamp_m375(NULL /*static, unused*/, L_2, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = acosf(L_3);
		return ((float)((float)L_4*(float)(57.29578f)));
	}
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" float Vector3_Distance_m344 (Object_t * __this /* static, unused */, Vector3_t6  ___a, Vector3_t6  ___b, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3__ctor_m335((&V_0), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		float L_6 = ((&V_0)->___x_1);
		float L_7 = ((&V_0)->___x_1);
		float L_8 = ((&V_0)->___y_2);
		float L_9 = ((&V_0)->___y_2);
		float L_10 = ((&V_0)->___z_3);
		float L_11 = ((&V_0)->___z_3);
		float L_12 = sqrtf(((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11)))));
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern "C" float Vector3_Magnitude_m7845 (Object_t * __this /* static, unused */, Vector3_t6  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___a)->___z_3);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C" float Vector3_get_magnitude_m6603 (Vector3_t6 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		float L_4 = (__this->___z_3);
		float L_5 = (__this->___z_3);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C" float Vector3_SqrMagnitude_m7846 (Object_t * __this /* static, unused */, Vector3_t6  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___a)->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C" float Vector3_get_sqrMagnitude_m4352 (Vector3_t6 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		float L_4 = (__this->___z_3);
		float L_5 = (__this->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_Min_m4335 (Object_t * __this /* static, unused */, Vector3_t6  ___lhs, Vector3_t6  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = Mathf_Min_m376(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ((&___lhs)->___y_2);
		float L_4 = ((&___rhs)->___y_2);
		float L_5 = Mathf_Min_m376(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = Mathf_Min_m376(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t6  L_9 = {0};
		Vector3__ctor_m335(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_Max_m4336 (Object_t * __this /* static, unused */, Vector3_t6  ___lhs, Vector3_t6  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = Mathf_Max_m4353(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ((&___lhs)->___y_2);
		float L_4 = ((&___rhs)->___y_2);
		float L_5 = Mathf_Max_m4353(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = Mathf_Max_m4353(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t6  L_9 = {0};
		Vector3__ctor_m335(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" Vector3_t6  Vector3_get_zero_m300 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = {0};
		Vector3__ctor_m335(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C" Vector3_t6  Vector3_get_one_m2644 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = {0};
		Vector3__ctor_m335(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C" Vector3_t6  Vector3_get_forward_m2566 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = {0};
		Vector3__ctor_m335(&L_0, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C" Vector3_t6  Vector3_get_back_m7847 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = {0};
		Vector3__ctor_m335(&L_0, (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" Vector3_t6  Vector3_get_up_m284 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = {0};
		Vector3__ctor_m335(&L_0, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C" Vector3_t6  Vector3_get_down_m4356 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = {0};
		Vector3__ctor_m335(&L_0, (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C" Vector3_t6  Vector3_get_left_m4354 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = {0};
		Vector3__ctor_m335(&L_0, (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C" Vector3_t6  Vector3_get_right_m4355 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = {0};
		Vector3__ctor_m335(&L_0, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_op_Addition_m291 (Object_t * __this /* static, unused */, Vector3_t6  ___a, Vector3_t6  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t6  L_6 = {0};
		Vector3__ctor_m335(&L_6, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_op_Subtraction_m292 (Object_t * __this /* static, unused */, Vector3_t6  ___a, Vector3_t6  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t6  L_6 = {0};
		Vector3__ctor_m335(&L_6, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_op_UnaryNegation_m289 (Object_t * __this /* static, unused */, Vector3_t6  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___y_2);
		float L_2 = ((&___a)->___z_3);
		Vector3_t6  L_3 = {0};
		Vector3__ctor_m335(&L_3, ((-L_0)), ((-L_1)), ((-L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6  Vector3_op_Multiply_m293 (Object_t * __this /* static, unused */, Vector3_t6  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t6  L_6 = {0};
		Vector3__ctor_m335(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C" Vector3_t6  Vector3_op_Multiply_m290 (Object_t * __this /* static, unused */, float ___d, Vector3_t6  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t6  L_6 = {0};
		Vector3__ctor_m335(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6  Vector3_op_Division_m6474 (Object_t * __this /* static, unused */, Vector3_t6  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t6  L_6 = {0};
		Vector3__ctor_m335(&L_6, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool Vector3_op_Equality_m363 (Object_t * __this /* static, unused */, Vector3_t6  ___lhs, Vector3_t6  ___rhs, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___lhs;
		Vector3_t6  L_1 = ___rhs;
		Vector3_t6  L_2 = Vector3_op_Subtraction_m292(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m7846(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool Vector3_op_Inequality_m337 (Object_t * __this /* static, unused */, Vector3_t6  ___lhs, Vector3_t6  ___rhs, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___lhs;
		Vector3_t6  L_1 = ___rhs;
		Vector3_t6  L_2 = Vector3_op_Subtraction_m292(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m7846(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Color__ctor_m405 (Color_t5 * __this, float ___r, float ___g, float ___b, float ___a, const MethodInfo* method)
{
	{
		float L_0 = ___r;
		__this->___r_0 = L_0;
		float L_1 = ___g;
		__this->___g_1 = L_1;
		float L_2 = ___b;
		__this->___b_2 = L_2;
		float L_3 = ___a;
		__this->___a_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C" void Color__ctor_m6489 (Color_t5 * __this, float ___r, float ___g, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ___r;
		__this->___r_0 = L_0;
		float L_1 = ___g;
		__this->___g_1 = L_1;
		float L_2 = ___b;
		__this->___b_2 = L_2;
		__this->___a_3 = (1.0f);
		return;
	}
}
// System.String UnityEngine.Color::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1283;
extern "C" String_t* Color_ToString_m7848 (Color_t5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1283 = il2cpp_codegen_string_literal_from_index(1283);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___r_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t36_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float L_5 = (__this->___g_1);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t36_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		float L_9 = (__this->___b_2);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t36_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		float L_13 = (__this->___a_3);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t36_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1283, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C" int32_t Color_GetHashCode_m7849 (Color_t5 * __this, const MethodInfo* method)
{
	Vector4_t822  V_0 = {0};
	{
		Vector4_t822  L_0 = Color_op_Implicit_m7850(NULL /*static, unused*/, (*(Color_t5 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m7925((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern TypeInfo* Color_t5_il2cpp_TypeInfo_var;
extern "C" bool Color_Equals_m4152 (Color_t5 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	Color_t5  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Color_t5_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Color_t5 *)((Color_t5 *)UnBox (L_1, Color_t5_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___r_0);
		float L_3 = ((&V_0)->___r_0);
		bool L_4 = Single_Equals_m8199(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___g_1);
		float L_6 = ((&V_0)->___g_1);
		bool L_7 = Single_Equals_m8199(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___b_2);
		float L_9 = ((&V_0)->___b_2);
		bool L_10 = Single_Equals_m8199(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___a_3);
		float L_12 = ((&V_0)->___a_3);
		bool L_13 = Single_Equals_m8199(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C" Color_t5  Color_Lerp_m2611 (Object_t * __this /* static, unused */, Color_t5  ___a, Color_t5  ___b, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		float L_1 = Mathf_Clamp01_m435(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t = L_1;
		float L_2 = ((&___a)->___r_0);
		float L_3 = ((&___b)->___r_0);
		float L_4 = ((&___a)->___r_0);
		float L_5 = ___t;
		float L_6 = ((&___a)->___g_1);
		float L_7 = ((&___b)->___g_1);
		float L_8 = ((&___a)->___g_1);
		float L_9 = ___t;
		float L_10 = ((&___a)->___b_2);
		float L_11 = ((&___b)->___b_2);
		float L_12 = ((&___a)->___b_2);
		float L_13 = ___t;
		float L_14 = ((&___a)->___a_3);
		float L_15 = ((&___b)->___a_3);
		float L_16 = ((&___a)->___a_3);
		float L_17 = ___t;
		Color_t5  L_18 = {0};
		Color__ctor_m405(&L_18, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		return L_18;
	}
}
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C" Color_t5  Color_get_red_m2327 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t5  L_0 = {0};
		Color__ctor_m405(&L_0, (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C" Color_t5  Color_get_green_m2309 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t5  L_0 = {0};
		Color__ctor_m405(&L_0, (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_blue()
extern "C" Color_t5  Color_get_blue_m2667 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t5  L_0 = {0};
		Color__ctor_m405(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" Color_t5  Color_get_white_m283 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t5  L_0 = {0};
		Color__ctor_m405(&L_0, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C" Color_t5  Color_get_black_m406 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t5  L_0 = {0};
		Color__ctor_m405(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_gray()
extern "C" Color_t5  Color_get_gray_m6476 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t5  L_0 = {0};
		Color__ctor_m405(&L_0, (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Color::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1280;
extern "C" float Color_get_Item_m6532 (Color_t5 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1280 = il2cpp_codegen_string_literal_from_index(1280);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0024;
		}
		if (L_1 == 2)
		{
			goto IL_002b;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0039;
	}

IL_001d:
	{
		float L_2 = (__this->___r_0);
		return L_2;
	}

IL_0024:
	{
		float L_3 = (__this->___g_1);
		return L_3;
	}

IL_002b:
	{
		float L_4 = (__this->___b_2);
		return L_4;
	}

IL_0032:
	{
		float L_5 = (__this->___a_3);
		return L_5;
	}

IL_0039:
	{
		IndexOutOfRangeException_t1476 * L_6 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_6, _stringLiteral1280, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}
}
// System.Void UnityEngine.Color::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1280;
extern "C" void Color_set_Item_m6531 (Color_t5 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1280 = il2cpp_codegen_string_literal_from_index(1280);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0035;
		}
		if (L_1 == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_001d:
	{
		float L_2 = ___value;
		__this->___r_0 = L_2;
		goto IL_0058;
	}

IL_0029:
	{
		float L_3 = ___value;
		__this->___g_1 = L_3;
		goto IL_0058;
	}

IL_0035:
	{
		float L_4 = ___value;
		__this->___b_2 = L_4;
		goto IL_0058;
	}

IL_0041:
	{
		float L_5 = ___value;
		__this->___a_3 = L_5;
		goto IL_0058;
	}

IL_004d:
	{
		IndexOutOfRangeException_t1476 * L_6 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_6, _stringLiteral1280, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0058:
	{
		return;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C" Color_t5  Color_op_Multiply_m4349 (Object_t * __this /* static, unused */, Color_t5  ___a, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___r_0);
		float L_1 = ___b;
		float L_2 = ((&___a)->___g_1);
		float L_3 = ___b;
		float L_4 = ((&___a)->___b_2);
		float L_5 = ___b;
		float L_6 = ((&___a)->___a_3);
		float L_7 = ___b;
		Color_t5  L_8 = {0};
		Color__ctor_m405(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Color::op_Inequality(UnityEngine.Color,UnityEngine.Color)
extern "C" bool Color_op_Inequality_m2675 (Object_t * __this /* static, unused */, Color_t5  ___lhs, Color_t5  ___rhs, const MethodInfo* method)
{
	{
		Color_t5  L_0 = ___lhs;
		Vector4_t822  L_1 = Color_op_Implicit_m7850(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t5  L_2 = ___rhs;
		Vector4_t822  L_3 = Color_op_Implicit_m7850(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_4 = Vector4_op_Inequality_m7932(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C" Vector4_t822  Color_op_Implicit_m7850 (Object_t * __this /* static, unused */, Color_t5  ___c, const MethodInfo* method)
{
	{
		float L_0 = ((&___c)->___r_0);
		float L_1 = ((&___c)->___g_1);
		float L_2 = ((&___c)->___b_2);
		float L_3 = ((&___c)->___a_3);
		Vector4_t822  L_4 = {0};
		Vector4__ctor_m4143(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C" void Color32__ctor_m4118 (Color32_t829 * __this, uint8_t ___r, uint8_t ___g, uint8_t ___b, uint8_t ___a, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___r;
		__this->___r_0 = L_0;
		uint8_t L_1 = ___g;
		__this->___g_1 = L_1;
		uint8_t L_2 = ___b;
		__this->___b_2 = L_2;
		uint8_t L_3 = ___a;
		__this->___a_3 = L_3;
		return;
	}
}
// System.String UnityEngine.Color32::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t560_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1284;
extern "C" String_t* Color32_ToString_m7851 (Color32_t829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Byte_t560_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		_stringLiteral1284 = il2cpp_codegen_string_literal_from_index(1284);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 4));
		uint8_t L_1 = (__this->___r_0);
		uint8_t L_2 = L_1;
		Object_t * L_3 = Box(Byte_t560_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		uint8_t L_5 = (__this->___g_1);
		uint8_t L_6 = L_5;
		Object_t * L_7 = Box(Byte_t560_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		uint8_t L_9 = (__this->___b_2);
		uint8_t L_10 = L_9;
		Object_t * L_11 = Box(Byte_t560_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		uint8_t L_13 = (__this->___a_3);
		uint8_t L_14 = L_13;
		Object_t * L_15 = Box(Byte_t560_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1284, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C" Color32_t829  Color32_op_Implicit_m4144 (Object_t * __this /* static, unused */, Color_t5  ___c, const MethodInfo* method)
{
	{
		float L_0 = ((&___c)->___r_0);
		float L_1 = Mathf_Clamp01_m435(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ((&___c)->___g_1);
		float L_3 = Mathf_Clamp01_m435(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = ((&___c)->___b_2);
		float L_5 = Mathf_Clamp01_m435(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___c)->___a_3);
		float L_7 = Mathf_Clamp01_m435(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t829  L_8 = {0};
		Color32__ctor_m4118(&L_8, (((int32_t)((uint8_t)((float)((float)L_1*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_3*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_5*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_7*(float)(255.0f)))))), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C" Color_t5  Color32_op_Implicit_m4119 (Object_t * __this /* static, unused */, Color32_t829  ___c, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((&___c)->___r_0);
		uint8_t L_1 = ((&___c)->___g_1);
		uint8_t L_2 = ((&___c)->___b_2);
		uint8_t L_3 = ((&___c)->___a_3);
		Color_t5  L_4 = {0};
		Color__ctor_m405(&L_4, ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Quaternion__ctor_m7852 (Quaternion_t51 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		float L_3 = ___w;
		__this->___w_4 = L_3;
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" Quaternion_t51  Quaternion_get_identity_m6442 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t51  L_0 = {0};
		Quaternion__ctor_m7852(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" float Quaternion_Dot_m7853 (Object_t * __this /* static, unused */, Quaternion_t51  ___a, Quaternion_t51  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C" Quaternion_t51  Quaternion_AngleAxis_m6432 (Object_t * __this /* static, unused */, float ___angle, Vector3_t6  ___axis, const MethodInfo* method)
{
	{
		float L_0 = ___angle;
		Quaternion_t51  L_1 = Quaternion_INTERNAL_CALL_AngleAxis_m7854(NULL /*static, unused*/, L_0, (&___axis), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&)
extern "C" Quaternion_t51  Quaternion_INTERNAL_CALL_AngleAxis_m7854 (Object_t * __this /* static, unused */, float ___angle, Vector3_t6 * ___axis, const MethodInfo* method)
{
	typedef Quaternion_t51  (*Quaternion_INTERNAL_CALL_AngleAxis_m7854_ftn) (float, Vector3_t6 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m7854_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m7854_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___angle, ___axis);
}
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C" void Quaternion_ToAngleAxis_m6503 (Quaternion_t51 * __this, float* ___angle, Vector3_t6 * ___axis, const MethodInfo* method)
{
	{
		Vector3_t6 * L_0 = ___axis;
		float* L_1 = ___angle;
		Quaternion_Internal_ToAxisAngleRad_m7865(NULL /*static, unused*/, (*(Quaternion_t51 *)__this), L_0, L_1, /*hidden argument*/NULL);
		float* L_2 = ___angle;
		float* L_3 = ___angle;
		*((float*)(L_2)) = (float)((float)((float)(*((float*)L_3))*(float)(57.29578f)));
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Quaternion_t51  Quaternion_LookRotation_m6756 (Object_t * __this /* static, unused */, Vector3_t6  ___forward, Vector3_t6  ___upwards, const MethodInfo* method)
{
	{
		Quaternion_t51  L_0 = Quaternion_INTERNAL_CALL_LookRotation_m7855(NULL /*static, unused*/, (&___forward), (&___upwards), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Quaternion_t51  Quaternion_INTERNAL_CALL_LookRotation_m7855 (Object_t * __this /* static, unused */, Vector3_t6 * ___forward, Vector3_t6 * ___upwards, const MethodInfo* method)
{
	typedef Quaternion_t51  (*Quaternion_INTERNAL_CALL_LookRotation_m7855_ftn) (Vector3_t6 *, Vector3_t6 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m7855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m7855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___forward, ___upwards);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t51  Quaternion_Lerp_m2672 (Object_t * __this /* static, unused */, Quaternion_t51  ___from, Quaternion_t51  ___to, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		Quaternion_t51  L_1 = Quaternion_INTERNAL_CALL_Lerp_m7856(NULL /*static, unused*/, (&___from), (&___to), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
extern "C" Quaternion_t51  Quaternion_INTERNAL_CALL_Lerp_m7856 (Object_t * __this /* static, unused */, Quaternion_t51 * ___from, Quaternion_t51 * ___to, float ___t, const MethodInfo* method)
{
	typedef Quaternion_t51  (*Quaternion_INTERNAL_CALL_Lerp_m7856_ftn) (Quaternion_t51 *, Quaternion_t51 *, float);
	static Quaternion_INTERNAL_CALL_Lerp_m7856_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m7856_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)");
	return _il2cpp_icall_func(___from, ___to, ___t);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C" Quaternion_t51  Quaternion_Inverse_m4350 (Object_t * __this /* static, unused */, Quaternion_t51  ___rotation, const MethodInfo* method)
{
	{
		Quaternion_t51  L_0 = Quaternion_INTERNAL_CALL_Inverse_m7857(NULL /*static, unused*/, (&___rotation), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)
extern "C" Quaternion_t51  Quaternion_INTERNAL_CALL_Inverse_m7857 (Object_t * __this /* static, unused */, Quaternion_t51 * ___rotation, const MethodInfo* method)
{
	typedef Quaternion_t51  (*Quaternion_INTERNAL_CALL_Inverse_m7857_ftn) (Quaternion_t51 *);
	static Quaternion_INTERNAL_CALL_Inverse_m7857_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m7857_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___rotation);
}
// System.String UnityEngine.Quaternion::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1285;
extern "C" String_t* Quaternion_ToString_m7858 (Quaternion_t51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1285 = il2cpp_codegen_string_literal_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t36_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t36_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t36_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		float L_13 = (__this->___w_4);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t36_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1285, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C" Vector3_t6  Quaternion_get_eulerAngles_m7859 (Quaternion_t51 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Quaternion_Internal_ToEulerRad_m7861(NULL /*static, unused*/, (*(Quaternion_t51 *)__this), /*hidden argument*/NULL);
		Vector3_t6  L_1 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C" Quaternion_t51  Quaternion_Euler_m7860 (Object_t * __this /* static, unused */, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		float L_1 = ___y;
		float L_2 = ___z;
		Vector3_t6  L_3 = {0};
		Vector3__ctor_m335(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t6  L_4 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t51  L_5 = Quaternion_Internal_FromEulerRad_m7863(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C" Quaternion_t51  Quaternion_Euler_m379 (Object_t * __this /* static, unused */, Vector3_t6  ___euler, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___euler;
		Vector3_t6  L_1 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t51  L_2 = Quaternion_Internal_FromEulerRad_m7863(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C" Vector3_t6  Quaternion_Internal_ToEulerRad_m7861 (Object_t * __this /* static, unused */, Quaternion_t51  ___rotation, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m7862(NULL /*static, unused*/, (&___rotation), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)
extern "C" Vector3_t6  Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m7862 (Object_t * __this /* static, unused */, Quaternion_t51 * ___rotation, const MethodInfo* method)
{
	typedef Vector3_t6  (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m7862_ftn) (Quaternion_t51 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m7862_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m7862_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___rotation);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C" Quaternion_t51  Quaternion_Internal_FromEulerRad_m7863 (Object_t * __this /* static, unused */, Vector3_t6  ___euler, const MethodInfo* method)
{
	{
		Quaternion_t51  L_0 = Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m7864(NULL /*static, unused*/, (&___euler), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)
extern "C" Quaternion_t51  Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m7864 (Object_t * __this /* static, unused */, Vector3_t6 * ___euler, const MethodInfo* method)
{
	typedef Quaternion_t51  (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m7864_ftn) (Vector3_t6 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m7864_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m7864_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___euler);
}
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C" void Quaternion_Internal_ToAxisAngleRad_m7865 (Object_t * __this /* static, unused */, Quaternion_t51  ___q, Vector3_t6 * ___axis, float* ___angle, const MethodInfo* method)
{
	{
		Vector3_t6 * L_0 = ___axis;
		float* L_1 = ___angle;
		Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m7866(NULL /*static, unused*/, (&___q), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C" void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m7866 (Object_t * __this /* static, unused */, Quaternion_t51 * ___q, Vector3_t6 * ___axis, float* ___angle, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m7866_ftn) (Quaternion_t51 *, Vector3_t6 *, float*);
	static Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m7866_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m7866_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)");
	_il2cpp_icall_func(___q, ___axis, ___angle);
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C" int32_t Quaternion_GetHashCode_m7867 (Quaternion_t51 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m8176(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m8176(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m8176(L_4, /*hidden argument*/NULL);
		float* L_6 = &(__this->___w_4);
		int32_t L_7 = Single_GetHashCode_m8176(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern TypeInfo* Quaternion_t51_il2cpp_TypeInfo_var;
extern "C" bool Quaternion_Equals_m7868 (Quaternion_t51 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quaternion_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(975);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t51  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Quaternion_t51_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Quaternion_t51 *)((Quaternion_t51 *)UnBox (L_1, Quaternion_t51_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m8199(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m8199(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m8199(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___w_4);
		float L_12 = ((&V_0)->___w_4);
		bool L_13 = Single_Equals_m8199(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" Quaternion_t51  Quaternion_op_Multiply_m6443 (Object_t * __this /* static, unused */, Quaternion_t51  ___lhs, Quaternion_t51  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___w_4);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___x_1);
		float L_3 = ((&___rhs)->___w_4);
		float L_4 = ((&___lhs)->___y_2);
		float L_5 = ((&___rhs)->___z_3);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___y_2);
		float L_8 = ((&___lhs)->___w_4);
		float L_9 = ((&___rhs)->___y_2);
		float L_10 = ((&___lhs)->___y_2);
		float L_11 = ((&___rhs)->___w_4);
		float L_12 = ((&___lhs)->___z_3);
		float L_13 = ((&___rhs)->___x_1);
		float L_14 = ((&___lhs)->___x_1);
		float L_15 = ((&___rhs)->___z_3);
		float L_16 = ((&___lhs)->___w_4);
		float L_17 = ((&___rhs)->___z_3);
		float L_18 = ((&___lhs)->___z_3);
		float L_19 = ((&___rhs)->___w_4);
		float L_20 = ((&___lhs)->___x_1);
		float L_21 = ((&___rhs)->___y_2);
		float L_22 = ((&___lhs)->___y_2);
		float L_23 = ((&___rhs)->___x_1);
		float L_24 = ((&___lhs)->___w_4);
		float L_25 = ((&___rhs)->___w_4);
		float L_26 = ((&___lhs)->___x_1);
		float L_27 = ((&___rhs)->___x_1);
		float L_28 = ((&___lhs)->___y_2);
		float L_29 = ((&___rhs)->___y_2);
		float L_30 = ((&___lhs)->___z_3);
		float L_31 = ((&___rhs)->___z_3);
		Quaternion_t51  L_32 = {0};
		Quaternion__ctor_m7852(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Vector3_t6  Quaternion_op_Multiply_m4167 (Object_t * __this /* static, unused */, Quaternion_t51  ___rotation, Vector3_t6  ___point, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t6  V_12 = {0};
	{
		float L_0 = ((&___rotation)->___x_1);
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = ((&___rotation)->___y_2);
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = ((&___rotation)->___z_3);
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = ((&___rotation)->___x_1);
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = ((&___rotation)->___y_2);
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = ((&___rotation)->___z_3);
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = ((&___rotation)->___x_1);
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = ((&___rotation)->___x_1);
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = ((&___rotation)->___y_2);
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = ((&___rotation)->___w_4);
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = ((&___rotation)->___w_4);
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = ((&___rotation)->___w_4);
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = ((&___point)->___x_1);
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = ((&___point)->___y_2);
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = ((&___point)->___z_3);
		(&V_12)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = ((&___point)->___x_1);
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = ((&___point)->___y_2);
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = ((&___point)->___z_3);
		(&V_12)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = ((&___point)->___x_1);
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = ((&___point)->___y_2);
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = ((&___point)->___z_3);
		(&V_12)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47))));
		Vector3_t6  L_48 = V_12;
		return L_48;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" bool Quaternion_op_Inequality_m4280 (Object_t * __this /* static, unused */, Quaternion_t51  ___lhs, Quaternion_t51  ___rhs, const MethodInfo* method)
{
	{
		Quaternion_t51  L_0 = ___lhs;
		Quaternion_t51  L_1 = ___rhs;
		float L_2 = Quaternion_Dot_m7853(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Rect__ctor_m387 (Rect_t30 * __this, float ___left, float ___top, float ___width, float ___height, const MethodInfo* method)
{
	{
		float L_0 = ___left;
		__this->___m_XMin_0 = L_0;
		float L_1 = ___top;
		__this->___m_YMin_1 = L_1;
		float L_2 = ___width;
		__this->___m_Width_2 = L_2;
		float L_3 = ___height;
		__this->___m_Height_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C" void Rect__ctor_m6819 (Rect_t30 * __this, Rect_t30  ___source, const MethodInfo* method)
{
	{
		float L_0 = ((&___source)->___m_XMin_0);
		__this->___m_XMin_0 = L_0;
		float L_1 = ((&___source)->___m_YMin_1);
		__this->___m_YMin_1 = L_1;
		float L_2 = ((&___source)->___m_Width_2);
		__this->___m_Width_2 = L_2;
		float L_3 = ((&___source)->___m_Height_3);
		__this->___m_Height_3 = L_3;
		return;
	}
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C" Rect_t30  Rect_MinMaxRect_m7869 (Object_t * __this /* static, unused */, float ___left, float ___top, float ___right, float ___bottom, const MethodInfo* method)
{
	{
		float L_0 = ___left;
		float L_1 = ___top;
		float L_2 = ___right;
		float L_3 = ___left;
		float L_4 = ___bottom;
		float L_5 = ___top;
		Rect_t30  L_6 = {0};
		Rect__ctor_m387(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Single UnityEngine.Rect::get_x()
extern "C" float Rect_get_x_m366 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C" void Rect_set_x_m367 (Rect_t30 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_XMin_0 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_y()
extern "C" float Rect_get_y_m368 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_YMin_1);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C" void Rect_set_y_m369 (Rect_t30 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_YMin_1 = L_0;
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C" Vector2_t29  Rect_get_position_m4201 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		float L_1 = (__this->___m_YMin_1);
		Vector2_t29  L_2 = {0};
		Vector2__ctor_m356(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C" Vector2_t29  Rect_get_center_m4312 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m366(__this, /*hidden argument*/NULL);
		float L_1 = (__this->___m_Width_2);
		float L_2 = Rect_get_y_m368(__this, /*hidden argument*/NULL);
		float L_3 = (__this->___m_Height_3);
		Vector2_t29  L_4 = {0};
		Vector2__ctor_m356(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.Rect::get_width()
extern "C" float Rect_get_width_m370 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C" void Rect_set_width_m371 (Rect_t30 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Width_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_height()
extern "C" float Rect_get_height_m372 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Height_3);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C" void Rect_set_height_m373 (Rect_t30 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Height_3 = L_0;
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C" Vector2_t29  Rect_get_size_m4199 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		float L_1 = (__this->___m_Height_3);
		Vector2_t29  L_2 = {0};
		Vector2__ctor_m356(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C" float Rect_get_xMin_m4217 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		return L_0;
	}
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C" float Rect_get_yMin_m4216 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_YMin_1);
		return L_0;
	}
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C" float Rect_get_xMax_m4208 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		float L_1 = (__this->___m_XMin_0);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C" float Rect_get_yMax_m4209 (Rect_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Height_3);
		float L_1 = (__this->___m_YMin_1);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.String UnityEngine.Rect::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1286;
extern "C" String_t* Rect_ToString_m7870 (Rect_t30 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1286 = il2cpp_codegen_string_literal_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 4));
		float L_1 = Rect_get_x_m366(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t36_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float L_5 = Rect_get_y_m368(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t36_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		float L_9 = Rect_get_width_m370(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t36_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		float L_13 = Rect_get_height_m372(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t36_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1286, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C" bool Rect_Contains_m7871 (Rect_t30 * __this, Vector2_t29  ___point, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___point)->___x_1);
		float L_1 = Rect_get_xMin_m4217(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ((&___point)->___x_1);
		float L_3 = Rect_get_xMax_m4208(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = ((&___point)->___y_2);
		float L_5 = Rect_get_yMin_m4216(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ((&___point)->___y_2);
		float L_7 = Rect_get_yMax_m4209(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C" bool Rect_Contains_m7872 (Rect_t30 * __this, Vector3_t6  ___point, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___point)->___x_1);
		float L_1 = Rect_get_xMin_m4217(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ((&___point)->___x_1);
		float L_3 = Rect_get_xMax_m4208(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = ((&___point)->___y_2);
		float L_5 = Rect_get_yMin_m4216(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ((&___point)->___y_2);
		float L_7 = Rect_get_yMax_m4209(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return G_B5_0;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C" int32_t Rect_GetHashCode_m7873 (Rect_t30 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m366(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m8176((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m370(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m8176((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m368(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m8176((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m372(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m8176((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern TypeInfo* Rect_t30_il2cpp_TypeInfo_var;
extern "C" bool Rect_Equals_m7874 (Rect_t30 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t30_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t30  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Rect_t30_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Rect_t30 *)((Rect_t30 *)UnBox (L_1, Rect_t30_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m366(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m366((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m8199((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m368(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m368((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m8199((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m370(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m370((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m8199((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m372(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m372((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m8199((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return G_B7_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C" bool Rect_op_Inequality_m6496 (Object_t * __this /* static, unused */, Rect_t30  ___lhs, Rect_t30  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m366((&___lhs), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m366((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_2 = Rect_get_y_m368((&___lhs), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m368((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004e;
		}
	}
	{
		float L_4 = Rect_get_width_m370((&___lhs), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m370((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004e;
		}
	}
	{
		float L_6 = Rect_get_height_m372((&___lhs), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m372((&___rhs), /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)((((float)L_6) == ((float)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B5_0 = 1;
	}

IL_004f:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C" bool Rect_op_Equality_m4304 (Object_t * __this /* static, unused */, Rect_t30  ___lhs, Rect_t30  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m366((&___lhs), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m366((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m368((&___lhs), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m368((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m370((&___lhs), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m370((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m372((&___lhs), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m372((&___rhs), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return G_B5_0;
	}
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C" float Matrix4x4_get_Item_m6840 (Matrix4x4_t603 * __this, int32_t ___row, int32_t ___column, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row;
		int32_t L_1 = ___column;
		float L_2 = Matrix4x4_get_Item_m6429(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C" void Matrix4x4_set_Item_m6841 (Matrix4x4_t603 * __this, int32_t ___row, int32_t ___column, float ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row;
		int32_t L_1 = ___column;
		float L_2 = ___value;
		Matrix4x4_set_Item_m6427(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1287;
extern "C" float Matrix4x4_get_Item_m6429 (Matrix4x4_t603 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1287 = il2cpp_codegen_string_literal_from_index(1287);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0054;
		}
		if (L_1 == 2)
		{
			goto IL_005b;
		}
		if (L_1 == 3)
		{
			goto IL_0062;
		}
		if (L_1 == 4)
		{
			goto IL_0069;
		}
		if (L_1 == 5)
		{
			goto IL_0070;
		}
		if (L_1 == 6)
		{
			goto IL_0077;
		}
		if (L_1 == 7)
		{
			goto IL_007e;
		}
		if (L_1 == 8)
		{
			goto IL_0085;
		}
		if (L_1 == 9)
		{
			goto IL_008c;
		}
		if (L_1 == 10)
		{
			goto IL_0093;
		}
		if (L_1 == 11)
		{
			goto IL_009a;
		}
		if (L_1 == 12)
		{
			goto IL_00a1;
		}
		if (L_1 == 13)
		{
			goto IL_00a8;
		}
		if (L_1 == 14)
		{
			goto IL_00af;
		}
		if (L_1 == 15)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00bd;
	}

IL_004d:
	{
		float L_2 = (__this->___m00_0);
		return L_2;
	}

IL_0054:
	{
		float L_3 = (__this->___m10_1);
		return L_3;
	}

IL_005b:
	{
		float L_4 = (__this->___m20_2);
		return L_4;
	}

IL_0062:
	{
		float L_5 = (__this->___m30_3);
		return L_5;
	}

IL_0069:
	{
		float L_6 = (__this->___m01_4);
		return L_6;
	}

IL_0070:
	{
		float L_7 = (__this->___m11_5);
		return L_7;
	}

IL_0077:
	{
		float L_8 = (__this->___m21_6);
		return L_8;
	}

IL_007e:
	{
		float L_9 = (__this->___m31_7);
		return L_9;
	}

IL_0085:
	{
		float L_10 = (__this->___m02_8);
		return L_10;
	}

IL_008c:
	{
		float L_11 = (__this->___m12_9);
		return L_11;
	}

IL_0093:
	{
		float L_12 = (__this->___m22_10);
		return L_12;
	}

IL_009a:
	{
		float L_13 = (__this->___m32_11);
		return L_13;
	}

IL_00a1:
	{
		float L_14 = (__this->___m03_12);
		return L_14;
	}

IL_00a8:
	{
		float L_15 = (__this->___m13_13);
		return L_15;
	}

IL_00af:
	{
		float L_16 = (__this->___m23_14);
		return L_16;
	}

IL_00b6:
	{
		float L_17 = (__this->___m33_15);
		return L_17;
	}

IL_00bd:
	{
		IndexOutOfRangeException_t1476 * L_18 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_18, _stringLiteral1287, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1287;
extern "C" void Matrix4x4_set_Item_m6427 (Matrix4x4_t603 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1287 = il2cpp_codegen_string_literal_from_index(1287);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
		if (L_1 == 2)
		{
			goto IL_0065;
		}
		if (L_1 == 3)
		{
			goto IL_0071;
		}
		if (L_1 == 4)
		{
			goto IL_007d;
		}
		if (L_1 == 5)
		{
			goto IL_0089;
		}
		if (L_1 == 6)
		{
			goto IL_0095;
		}
		if (L_1 == 7)
		{
			goto IL_00a1;
		}
		if (L_1 == 8)
		{
			goto IL_00ad;
		}
		if (L_1 == 9)
		{
			goto IL_00b9;
		}
		if (L_1 == 10)
		{
			goto IL_00c5;
		}
		if (L_1 == 11)
		{
			goto IL_00d1;
		}
		if (L_1 == 12)
		{
			goto IL_00dd;
		}
		if (L_1 == 13)
		{
			goto IL_00e9;
		}
		if (L_1 == 14)
		{
			goto IL_00f5;
		}
		if (L_1 == 15)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_010d;
	}

IL_004d:
	{
		float L_2 = ___value;
		__this->___m00_0 = L_2;
		goto IL_0118;
	}

IL_0059:
	{
		float L_3 = ___value;
		__this->___m10_1 = L_3;
		goto IL_0118;
	}

IL_0065:
	{
		float L_4 = ___value;
		__this->___m20_2 = L_4;
		goto IL_0118;
	}

IL_0071:
	{
		float L_5 = ___value;
		__this->___m30_3 = L_5;
		goto IL_0118;
	}

IL_007d:
	{
		float L_6 = ___value;
		__this->___m01_4 = L_6;
		goto IL_0118;
	}

IL_0089:
	{
		float L_7 = ___value;
		__this->___m11_5 = L_7;
		goto IL_0118;
	}

IL_0095:
	{
		float L_8 = ___value;
		__this->___m21_6 = L_8;
		goto IL_0118;
	}

IL_00a1:
	{
		float L_9 = ___value;
		__this->___m31_7 = L_9;
		goto IL_0118;
	}

IL_00ad:
	{
		float L_10 = ___value;
		__this->___m02_8 = L_10;
		goto IL_0118;
	}

IL_00b9:
	{
		float L_11 = ___value;
		__this->___m12_9 = L_11;
		goto IL_0118;
	}

IL_00c5:
	{
		float L_12 = ___value;
		__this->___m22_10 = L_12;
		goto IL_0118;
	}

IL_00d1:
	{
		float L_13 = ___value;
		__this->___m32_11 = L_13;
		goto IL_0118;
	}

IL_00dd:
	{
		float L_14 = ___value;
		__this->___m03_12 = L_14;
		goto IL_0118;
	}

IL_00e9:
	{
		float L_15 = ___value;
		__this->___m13_13 = L_15;
		goto IL_0118;
	}

IL_00f5:
	{
		float L_16 = ___value;
		__this->___m23_14 = L_16;
		goto IL_0118;
	}

IL_0101:
	{
		float L_17 = ___value;
		__this->___m33_15 = L_17;
		goto IL_0118;
	}

IL_010d:
	{
		IndexOutOfRangeException_t1476 * L_18 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_18, _stringLiteral1287, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_0118:
	{
		return;
	}
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C" int32_t Matrix4x4_GetHashCode_m7875 (Matrix4x4_t603 * __this, const MethodInfo* method)
{
	Vector4_t822  V_0 = {0};
	Vector4_t822  V_1 = {0};
	Vector4_t822  V_2 = {0};
	Vector4_t822  V_3 = {0};
	{
		Vector4_t822  L_0 = Matrix4x4_GetColumn_m7885(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m7925((&V_0), /*hidden argument*/NULL);
		Vector4_t822  L_2 = Matrix4x4_GetColumn_m7885(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m7925((&V_1), /*hidden argument*/NULL);
		Vector4_t822  L_4 = Matrix4x4_GetColumn_m7885(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m7925((&V_2), /*hidden argument*/NULL);
		Vector4_t822  L_6 = Matrix4x4_GetColumn_m7885(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m7925((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern TypeInfo* Matrix4x4_t603_il2cpp_TypeInfo_var;
extern TypeInfo* Vector4_t822_il2cpp_TypeInfo_var;
extern "C" bool Matrix4x4_Equals_m7876 (Matrix4x4_t603 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(976);
		Vector4_t822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(951);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t603  V_0 = {0};
	Vector4_t822  V_1 = {0};
	Vector4_t822  V_2 = {0};
	Vector4_t822  V_3 = {0};
	Vector4_t822  V_4 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Matrix4x4_t603_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Matrix4x4_t603 *)((Matrix4x4_t603 *)UnBox (L_1, Matrix4x4_t603_il2cpp_TypeInfo_var))));
		Vector4_t822  L_2 = Matrix4x4_GetColumn_m7885(__this, 0, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector4_t822  L_3 = Matrix4x4_GetColumn_m7885((&V_0), 0, /*hidden argument*/NULL);
		Vector4_t822  L_4 = L_3;
		Object_t * L_5 = Box(Vector4_t822_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m7926((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t822  L_7 = Matrix4x4_GetColumn_m7885(__this, 1, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector4_t822  L_8 = Matrix4x4_GetColumn_m7885((&V_0), 1, /*hidden argument*/NULL);
		Vector4_t822  L_9 = L_8;
		Object_t * L_10 = Box(Vector4_t822_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m7926((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t822  L_12 = Matrix4x4_GetColumn_m7885(__this, 2, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector4_t822  L_13 = Matrix4x4_GetColumn_m7885((&V_0), 2, /*hidden argument*/NULL);
		Vector4_t822  L_14 = L_13;
		Object_t * L_15 = Box(Vector4_t822_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m7926((&V_3), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t822  L_17 = Matrix4x4_GetColumn_m7885(__this, 3, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector4_t822  L_18 = Matrix4x4_GetColumn_m7885((&V_0), 3, /*hidden argument*/NULL);
		Vector4_t822  L_19 = L_18;
		Object_t * L_20 = Box(Vector4_t822_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m7926((&V_4), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_0097;
	}

IL_0096:
	{
		G_B7_0 = 0;
	}

IL_0097:
	{
		return G_B7_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Inverse(UnityEngine.Matrix4x4)
extern "C" Matrix4x4_t603  Matrix4x4_Inverse_m7877 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___m, const MethodInfo* method)
{
	{
		Matrix4x4_t603  L_0 = Matrix4x4_INTERNAL_CALL_Inverse_m7878(NULL /*static, unused*/, (&___m), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)
extern "C" Matrix4x4_t603  Matrix4x4_INTERNAL_CALL_Inverse_m7878 (Object_t * __this /* static, unused */, Matrix4x4_t603 * ___m, const MethodInfo* method)
{
	typedef Matrix4x4_t603  (*Matrix4x4_INTERNAL_CALL_Inverse_m7878_ftn) (Matrix4x4_t603 *);
	static Matrix4x4_INTERNAL_CALL_Inverse_m7878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Inverse_m7878_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___m);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Transpose(UnityEngine.Matrix4x4)
extern "C" Matrix4x4_t603  Matrix4x4_Transpose_m7879 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___m, const MethodInfo* method)
{
	{
		Matrix4x4_t603  L_0 = Matrix4x4_INTERNAL_CALL_Transpose_m7880(NULL /*static, unused*/, (&___m), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)
extern "C" Matrix4x4_t603  Matrix4x4_INTERNAL_CALL_Transpose_m7880 (Object_t * __this /* static, unused */, Matrix4x4_t603 * ___m, const MethodInfo* method)
{
	typedef Matrix4x4_t603  (*Matrix4x4_INTERNAL_CALL_Transpose_m7880_ftn) (Matrix4x4_t603 *);
	static Matrix4x4_INTERNAL_CALL_Transpose_m7880_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Transpose_m7880_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___m);
}
// System.Boolean UnityEngine.Matrix4x4::Invert(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4&)
extern "C" bool Matrix4x4_Invert_m7881 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___inMatrix, Matrix4x4_t603 * ___dest, const MethodInfo* method)
{
	{
		Matrix4x4_t603 * L_0 = ___dest;
		bool L_1 = Matrix4x4_INTERNAL_CALL_Invert_m7882(NULL /*static, unused*/, (&___inMatrix), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
extern "C" bool Matrix4x4_INTERNAL_CALL_Invert_m7882 (Object_t * __this /* static, unused */, Matrix4x4_t603 * ___inMatrix, Matrix4x4_t603 * ___dest, const MethodInfo* method)
{
	typedef bool (*Matrix4x4_INTERNAL_CALL_Invert_m7882_ftn) (Matrix4x4_t603 *, Matrix4x4_t603 *);
	static Matrix4x4_INTERNAL_CALL_Invert_m7882_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Invert_m7882_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___inMatrix, ___dest);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C" Matrix4x4_t603  Matrix4x4_get_inverse_m6466 (Matrix4x4_t603 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t603  L_0 = Matrix4x4_Inverse_m7877(NULL /*static, unused*/, (*(Matrix4x4_t603 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_transpose()
extern "C" Matrix4x4_t603  Matrix4x4_get_transpose_m7883 (Matrix4x4_t603 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t603  L_0 = Matrix4x4_Transpose_m7879(NULL /*static, unused*/, (*(Matrix4x4_t603 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Matrix4x4::get_isIdentity()
extern "C" bool Matrix4x4_get_isIdentity_m7884 (Matrix4x4_t603 * __this, const MethodInfo* method)
{
	typedef bool (*Matrix4x4_get_isIdentity_m7884_ftn) (Matrix4x4_t603 *);
	static Matrix4x4_get_isIdentity_m7884_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_get_isIdentity_m7884_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::get_isIdentity()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C" Vector4_t822  Matrix4x4_GetColumn_m7885 (Matrix4x4_t603 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = Matrix4x4_get_Item_m6840(__this, 0, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = Matrix4x4_get_Item_m6840(__this, 1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = Matrix4x4_get_Item_m6840(__this, 2, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = Matrix4x4_get_Item_m6840(__this, 3, L_6, /*hidden argument*/NULL);
		Vector4_t822  L_8 = {0};
		Vector4__ctor_m4143(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetRow(System.Int32)
extern "C" Vector4_t822  Matrix4x4_GetRow_m7886 (Matrix4x4_t603 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = Matrix4x4_get_Item_m6840(__this, L_0, 0, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = Matrix4x4_get_Item_m6840(__this, L_2, 1, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = Matrix4x4_get_Item_m6840(__this, L_4, 2, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = Matrix4x4_get_Item_m6840(__this, L_6, 3, /*hidden argument*/NULL);
		Vector4_t822  L_8 = {0};
		Vector4__ctor_m4143(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C" void Matrix4x4_SetColumn_m7887 (Matrix4x4_t603 * __this, int32_t ___i, Vector4_t822  ___v, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = ((&___v)->___x_1);
		Matrix4x4_set_Item_m6841(__this, 0, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = ((&___v)->___y_2);
		Matrix4x4_set_Item_m6841(__this, 1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = ((&___v)->___z_3);
		Matrix4x4_set_Item_m6841(__this, 2, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = ((&___v)->___w_4);
		Matrix4x4_set_Item_m6841(__this, 3, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Matrix4x4::SetRow(System.Int32,UnityEngine.Vector4)
extern "C" void Matrix4x4_SetRow_m7888 (Matrix4x4_t603 * __this, int32_t ___i, Vector4_t822  ___v, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = ((&___v)->___x_1);
		Matrix4x4_set_Item_m6841(__this, L_0, 0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = ((&___v)->___y_2);
		Matrix4x4_set_Item_m6841(__this, L_2, 1, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = ((&___v)->___z_3);
		Matrix4x4_set_Item_m6841(__this, L_4, 2, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = ((&___v)->___w_4);
		Matrix4x4_set_Item_m6841(__this, L_6, 3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C" Vector3_t6  Matrix4x4_MultiplyPoint_m7889 (Matrix4x4_t603 * __this, Vector3_t6  ___v, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	float V_1 = 0.0f;
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = (__this->___m03_12);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6));
		float L_7 = (__this->___m10_1);
		float L_8 = ((&___v)->___x_1);
		float L_9 = (__this->___m11_5);
		float L_10 = ((&___v)->___y_2);
		float L_11 = (__this->___m12_9);
		float L_12 = ((&___v)->___z_3);
		float L_13 = (__this->___m13_13);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13));
		float L_14 = (__this->___m20_2);
		float L_15 = ((&___v)->___x_1);
		float L_16 = (__this->___m21_6);
		float L_17 = ((&___v)->___y_2);
		float L_18 = (__this->___m22_10);
		float L_19 = ((&___v)->___z_3);
		float L_20 = (__this->___m23_14);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20));
		float L_21 = (__this->___m30_3);
		float L_22 = ((&___v)->___x_1);
		float L_23 = (__this->___m31_7);
		float L_24 = ((&___v)->___y_2);
		float L_25 = (__this->___m32_11);
		float L_26 = ((&___v)->___z_3);
		float L_27 = (__this->___m33_15);
		V_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_21*(float)L_22))+(float)((float)((float)L_23*(float)L_24))))+(float)((float)((float)L_25*(float)L_26))))+(float)L_27));
		float L_28 = V_1;
		V_1 = ((float)((float)(1.0f)/(float)L_28));
		Vector3_t6 * L_29 = (&V_0);
		float L_30 = (L_29->___x_1);
		float L_31 = V_1;
		L_29->___x_1 = ((float)((float)L_30*(float)L_31));
		Vector3_t6 * L_32 = (&V_0);
		float L_33 = (L_32->___y_2);
		float L_34 = V_1;
		L_32->___y_2 = ((float)((float)L_33*(float)L_34));
		Vector3_t6 * L_35 = (&V_0);
		float L_36 = (L_35->___z_3);
		float L_37 = V_1;
		L_35->___z_3 = ((float)((float)L_36*(float)L_37));
		Vector3_t6  L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C" Vector3_t6  Matrix4x4_MultiplyPoint3x4_m4334 (Matrix4x4_t603 * __this, Vector3_t6  ___v, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = (__this->___m03_12);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6));
		float L_7 = (__this->___m10_1);
		float L_8 = ((&___v)->___x_1);
		float L_9 = (__this->___m11_5);
		float L_10 = ((&___v)->___y_2);
		float L_11 = (__this->___m12_9);
		float L_12 = ((&___v)->___z_3);
		float L_13 = (__this->___m13_13);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13));
		float L_14 = (__this->___m20_2);
		float L_15 = ((&___v)->___x_1);
		float L_16 = (__this->___m21_6);
		float L_17 = ((&___v)->___y_2);
		float L_18 = (__this->___m22_10);
		float L_19 = ((&___v)->___z_3);
		float L_20 = (__this->___m23_14);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20));
		Vector3_t6  L_21 = V_0;
		return L_21;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C" Vector3_t6  Matrix4x4_MultiplyVector_m7890 (Matrix4x4_t603 * __this, Vector3_t6  ___v, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		float L_6 = (__this->___m10_1);
		float L_7 = ((&___v)->___x_1);
		float L_8 = (__this->___m11_5);
		float L_9 = ((&___v)->___y_2);
		float L_10 = (__this->___m12_9);
		float L_11 = ((&___v)->___z_3);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11))));
		float L_12 = (__this->___m20_2);
		float L_13 = ((&___v)->___x_1);
		float L_14 = (__this->___m21_6);
		float L_15 = ((&___v)->___y_2);
		float L_16 = (__this->___m22_10);
		float L_17 = ((&___v)->___z_3);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)L_12*(float)L_13))+(float)((float)((float)L_14*(float)L_15))))+(float)((float)((float)L_16*(float)L_17))));
		Vector3_t6  L_18 = V_0;
		return L_18;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Scale(UnityEngine.Vector3)
extern TypeInfo* Matrix4x4_t603_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t603  Matrix4x4_Scale_m7891 (Object_t * __this /* static, unused */, Vector3_t6  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(976);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t603  V_0 = {0};
	{
		Initobj (Matrix4x4_t603_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___v)->___x_1);
		(&V_0)->___m00_0 = L_0;
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		float L_1 = ((&___v)->___y_2);
		(&V_0)->___m11_5 = L_1;
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		float L_2 = ((&___v)->___z_3);
		(&V_0)->___m22_10 = L_2;
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (1.0f);
		Matrix4x4_t603  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern TypeInfo* Matrix4x4_t603_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t603  Matrix4x4_get_zero_m6848 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(976);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t603  V_0 = {0};
	{
		Initobj (Matrix4x4_t603_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___m00_0 = (0.0f);
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		(&V_0)->___m11_5 = (0.0f);
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		(&V_0)->___m22_10 = (0.0f);
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (0.0f);
		Matrix4x4_t603  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern TypeInfo* Matrix4x4_t603_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t603  Matrix4x4_get_identity_m6426 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(976);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t603  V_0 = {0};
	{
		Initobj (Matrix4x4_t603_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___m00_0 = (1.0f);
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		(&V_0)->___m11_5 = (1.0f);
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		(&V_0)->___m22_10 = (1.0f);
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (1.0f);
		Matrix4x4_t603  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::SetTRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" void Matrix4x4_SetTRS_m7892 (Matrix4x4_t603 * __this, Vector3_t6  ___pos, Quaternion_t51  ___q, Vector3_t6  ___s, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___pos;
		Quaternion_t51  L_1 = ___q;
		Vector3_t6  L_2 = ___s;
		Matrix4x4_t603  L_3 = Matrix4x4_TRS_m2791(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		(*(Matrix4x4_t603 *)__this) = L_3;
		return;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Matrix4x4_t603  Matrix4x4_TRS_m2791 (Object_t * __this /* static, unused */, Vector3_t6  ___pos, Quaternion_t51  ___q, Vector3_t6  ___s, const MethodInfo* method)
{
	{
		Matrix4x4_t603  L_0 = Matrix4x4_INTERNAL_CALL_TRS_m7893(NULL /*static, unused*/, (&___pos), (&___q), (&___s), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C" Matrix4x4_t603  Matrix4x4_INTERNAL_CALL_TRS_m7893 (Object_t * __this /* static, unused */, Vector3_t6 * ___pos, Quaternion_t51 * ___q, Vector3_t6 * ___s, const MethodInfo* method)
{
	typedef Matrix4x4_t603  (*Matrix4x4_INTERNAL_CALL_TRS_m7893_ftn) (Vector3_t6 *, Quaternion_t51 *, Vector3_t6 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m7893_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m7893_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___pos, ___q, ___s);
}
// System.String UnityEngine.Matrix4x4::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1288;
extern "C" String_t* Matrix4x4_ToString_m7894 (Matrix4x4_t603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1288 = il2cpp_codegen_string_literal_from_index(1288);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, ((int32_t)16)));
		float L_1 = (__this->___m00_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t36_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float L_5 = (__this->___m01_4);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t36_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		float L_9 = (__this->___m02_8);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t36_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		float L_13 = (__this->___m03_12);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t36_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t34* L_16 = L_12;
		float L_17 = (__this->___m10_1);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t36_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t34* L_20 = L_16;
		float L_21 = (__this->___m11_5);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t36_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t34* L_24 = L_20;
		float L_25 = (__this->___m12_9);
		float L_26 = L_25;
		Object_t * L_27 = Box(Single_t36_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 6, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t34* L_28 = L_24;
		float L_29 = (__this->___m13_13);
		float L_30 = L_29;
		Object_t * L_31 = Box(Single_t36_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 7, sizeof(Object_t *))) = (Object_t *)L_31;
		ObjectU5BU5D_t34* L_32 = L_28;
		float L_33 = (__this->___m20_2);
		float L_34 = L_33;
		Object_t * L_35 = Box(Single_t36_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 8, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t34* L_36 = L_32;
		float L_37 = (__this->___m21_6);
		float L_38 = L_37;
		Object_t * L_39 = Box(Single_t36_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_39;
		ObjectU5BU5D_t34* L_40 = L_36;
		float L_41 = (__this->___m22_10);
		float L_42 = L_41;
		Object_t * L_43 = Box(Single_t36_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_43;
		ObjectU5BU5D_t34* L_44 = L_40;
		float L_45 = (__this->___m23_14);
		float L_46 = L_45;
		Object_t * L_47 = Box(Single_t36_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_47;
		ObjectU5BU5D_t34* L_48 = L_44;
		float L_49 = (__this->___m30_3);
		float L_50 = L_49;
		Object_t * L_51 = Box(Single_t36_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)L_51;
		ObjectU5BU5D_t34* L_52 = L_48;
		float L_53 = (__this->___m31_7);
		float L_54 = L_53;
		Object_t * L_55 = Box(Single_t36_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t34* L_56 = L_52;
		float L_57 = (__this->___m32_11);
		float L_58 = L_57;
		Object_t * L_59 = Box(Single_t36_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)L_59;
		ObjectU5BU5D_t34* L_60 = L_56;
		float L_61 = (__this->___m33_15);
		float L_62 = L_61;
		Object_t * L_63 = Box(Single_t36_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_63;
		String_t* L_64 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1288, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
// System.String UnityEngine.Matrix4x4::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1289;
extern "C" String_t* Matrix4x4_ToString_m7895 (Matrix4x4_t603 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1289 = il2cpp_codegen_string_literal_from_index(1289);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, ((int32_t)16)));
		float* L_1 = &(__this->___m00_0);
		String_t* L_2 = ___format;
		String_t* L_3 = Single_ToString_m8200(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float* L_5 = &(__this->___m01_4);
		String_t* L_6 = ___format;
		String_t* L_7 = Single_ToString_m8200(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		float* L_9 = &(__this->___m02_8);
		String_t* L_10 = ___format;
		String_t* L_11 = Single_ToString_m8200(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		float* L_13 = &(__this->___m03_12);
		String_t* L_14 = ___format;
		String_t* L_15 = Single_ToString_m8200(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t34* L_16 = L_12;
		float* L_17 = &(__this->___m10_1);
		String_t* L_18 = ___format;
		String_t* L_19 = Single_ToString_m8200(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t34* L_20 = L_16;
		float* L_21 = &(__this->___m11_5);
		String_t* L_22 = ___format;
		String_t* L_23 = Single_ToString_m8200(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t34* L_24 = L_20;
		float* L_25 = &(__this->___m12_9);
		String_t* L_26 = ___format;
		String_t* L_27 = Single_ToString_m8200(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 6, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t34* L_28 = L_24;
		float* L_29 = &(__this->___m13_13);
		String_t* L_30 = ___format;
		String_t* L_31 = Single_ToString_m8200(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 7, sizeof(Object_t *))) = (Object_t *)L_31;
		ObjectU5BU5D_t34* L_32 = L_28;
		float* L_33 = &(__this->___m20_2);
		String_t* L_34 = ___format;
		String_t* L_35 = Single_ToString_m8200(L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 8, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t34* L_36 = L_32;
		float* L_37 = &(__this->___m21_6);
		String_t* L_38 = ___format;
		String_t* L_39 = Single_ToString_m8200(L_37, L_38, /*hidden argument*/NULL);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_39;
		ObjectU5BU5D_t34* L_40 = L_36;
		float* L_41 = &(__this->___m22_10);
		String_t* L_42 = ___format;
		String_t* L_43 = Single_ToString_m8200(L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_43;
		ObjectU5BU5D_t34* L_44 = L_40;
		float* L_45 = &(__this->___m23_14);
		String_t* L_46 = ___format;
		String_t* L_47 = Single_ToString_m8200(L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_47;
		ObjectU5BU5D_t34* L_48 = L_44;
		float* L_49 = &(__this->___m30_3);
		String_t* L_50 = ___format;
		String_t* L_51 = Single_ToString_m8200(L_49, L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)L_51;
		ObjectU5BU5D_t34* L_52 = L_48;
		float* L_53 = &(__this->___m31_7);
		String_t* L_54 = ___format;
		String_t* L_55 = Single_ToString_m8200(L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t34* L_56 = L_52;
		float* L_57 = &(__this->___m32_11);
		String_t* L_58 = ___format;
		String_t* L_59 = Single_ToString_m8200(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)L_59;
		ObjectU5BU5D_t34* L_60 = L_56;
		float* L_61 = &(__this->___m33_15);
		String_t* L_62 = ___format;
		String_t* L_63 = Single_ToString_m8200(L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_63;
		String_t* L_64 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1289, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t603  Matrix4x4_Ortho_m7896 (Object_t * __this /* static, unused */, float ___left, float ___right, float ___bottom, float ___top, float ___zNear, float ___zFar, const MethodInfo* method)
{
	typedef Matrix4x4_t603  (*Matrix4x4_Ortho_m7896_ftn) (float, float, float, float, float, float);
	static Matrix4x4_Ortho_m7896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_Ortho_m7896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)");
	return _il2cpp_icall_func(___left, ___right, ___bottom, ___top, ___zNear, ___zFar);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t603  Matrix4x4_Perspective_m7897 (Object_t * __this /* static, unused */, float ___fov, float ___aspect, float ___zNear, float ___zFar, const MethodInfo* method)
{
	typedef Matrix4x4_t603  (*Matrix4x4_Perspective_m7897_ftn) (float, float, float, float);
	static Matrix4x4_Perspective_m7897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_Perspective_m7897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)");
	return _il2cpp_icall_func(___fov, ___aspect, ___zNear, ___zFar);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern TypeInfo* Matrix4x4_t603_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t603  Matrix4x4_op_Multiply_m7898 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___lhs, Matrix4x4_t603  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(976);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t603  V_0 = {0};
	{
		Initobj (Matrix4x4_t603_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___m00_0);
		float L_1 = ((&___rhs)->___m00_0);
		float L_2 = ((&___lhs)->___m01_4);
		float L_3 = ((&___rhs)->___m10_1);
		float L_4 = ((&___lhs)->___m02_8);
		float L_5 = ((&___rhs)->___m20_2);
		float L_6 = ((&___lhs)->___m03_12);
		float L_7 = ((&___rhs)->___m30_3);
		(&V_0)->___m00_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		float L_8 = ((&___lhs)->___m00_0);
		float L_9 = ((&___rhs)->___m01_4);
		float L_10 = ((&___lhs)->___m01_4);
		float L_11 = ((&___rhs)->___m11_5);
		float L_12 = ((&___lhs)->___m02_8);
		float L_13 = ((&___rhs)->___m21_6);
		float L_14 = ((&___lhs)->___m03_12);
		float L_15 = ((&___rhs)->___m31_7);
		(&V_0)->___m01_4 = ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15))));
		float L_16 = ((&___lhs)->___m00_0);
		float L_17 = ((&___rhs)->___m02_8);
		float L_18 = ((&___lhs)->___m01_4);
		float L_19 = ((&___rhs)->___m12_9);
		float L_20 = ((&___lhs)->___m02_8);
		float L_21 = ((&___rhs)->___m22_10);
		float L_22 = ((&___lhs)->___m03_12);
		float L_23 = ((&___rhs)->___m32_11);
		(&V_0)->___m02_8 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = ((&___lhs)->___m00_0);
		float L_25 = ((&___rhs)->___m03_12);
		float L_26 = ((&___lhs)->___m01_4);
		float L_27 = ((&___rhs)->___m13_13);
		float L_28 = ((&___lhs)->___m02_8);
		float L_29 = ((&___rhs)->___m23_14);
		float L_30 = ((&___lhs)->___m03_12);
		float L_31 = ((&___rhs)->___m33_15);
		(&V_0)->___m03_12 = ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31))));
		float L_32 = ((&___lhs)->___m10_1);
		float L_33 = ((&___rhs)->___m00_0);
		float L_34 = ((&___lhs)->___m11_5);
		float L_35 = ((&___rhs)->___m10_1);
		float L_36 = ((&___lhs)->___m12_9);
		float L_37 = ((&___rhs)->___m20_2);
		float L_38 = ((&___lhs)->___m13_13);
		float L_39 = ((&___rhs)->___m30_3);
		(&V_0)->___m10_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_32*(float)L_33))+(float)((float)((float)L_34*(float)L_35))))+(float)((float)((float)L_36*(float)L_37))))+(float)((float)((float)L_38*(float)L_39))));
		float L_40 = ((&___lhs)->___m10_1);
		float L_41 = ((&___rhs)->___m01_4);
		float L_42 = ((&___lhs)->___m11_5);
		float L_43 = ((&___rhs)->___m11_5);
		float L_44 = ((&___lhs)->___m12_9);
		float L_45 = ((&___rhs)->___m21_6);
		float L_46 = ((&___lhs)->___m13_13);
		float L_47 = ((&___rhs)->___m31_7);
		(&V_0)->___m11_5 = ((float)((float)((float)((float)((float)((float)((float)((float)L_40*(float)L_41))+(float)((float)((float)L_42*(float)L_43))))+(float)((float)((float)L_44*(float)L_45))))+(float)((float)((float)L_46*(float)L_47))));
		float L_48 = ((&___lhs)->___m10_1);
		float L_49 = ((&___rhs)->___m02_8);
		float L_50 = ((&___lhs)->___m11_5);
		float L_51 = ((&___rhs)->___m12_9);
		float L_52 = ((&___lhs)->___m12_9);
		float L_53 = ((&___rhs)->___m22_10);
		float L_54 = ((&___lhs)->___m13_13);
		float L_55 = ((&___rhs)->___m32_11);
		(&V_0)->___m12_9 = ((float)((float)((float)((float)((float)((float)((float)((float)L_48*(float)L_49))+(float)((float)((float)L_50*(float)L_51))))+(float)((float)((float)L_52*(float)L_53))))+(float)((float)((float)L_54*(float)L_55))));
		float L_56 = ((&___lhs)->___m10_1);
		float L_57 = ((&___rhs)->___m03_12);
		float L_58 = ((&___lhs)->___m11_5);
		float L_59 = ((&___rhs)->___m13_13);
		float L_60 = ((&___lhs)->___m12_9);
		float L_61 = ((&___rhs)->___m23_14);
		float L_62 = ((&___lhs)->___m13_13);
		float L_63 = ((&___rhs)->___m33_15);
		(&V_0)->___m13_13 = ((float)((float)((float)((float)((float)((float)((float)((float)L_56*(float)L_57))+(float)((float)((float)L_58*(float)L_59))))+(float)((float)((float)L_60*(float)L_61))))+(float)((float)((float)L_62*(float)L_63))));
		float L_64 = ((&___lhs)->___m20_2);
		float L_65 = ((&___rhs)->___m00_0);
		float L_66 = ((&___lhs)->___m21_6);
		float L_67 = ((&___rhs)->___m10_1);
		float L_68 = ((&___lhs)->___m22_10);
		float L_69 = ((&___rhs)->___m20_2);
		float L_70 = ((&___lhs)->___m23_14);
		float L_71 = ((&___rhs)->___m30_3);
		(&V_0)->___m20_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_64*(float)L_65))+(float)((float)((float)L_66*(float)L_67))))+(float)((float)((float)L_68*(float)L_69))))+(float)((float)((float)L_70*(float)L_71))));
		float L_72 = ((&___lhs)->___m20_2);
		float L_73 = ((&___rhs)->___m01_4);
		float L_74 = ((&___lhs)->___m21_6);
		float L_75 = ((&___rhs)->___m11_5);
		float L_76 = ((&___lhs)->___m22_10);
		float L_77 = ((&___rhs)->___m21_6);
		float L_78 = ((&___lhs)->___m23_14);
		float L_79 = ((&___rhs)->___m31_7);
		(&V_0)->___m21_6 = ((float)((float)((float)((float)((float)((float)((float)((float)L_72*(float)L_73))+(float)((float)((float)L_74*(float)L_75))))+(float)((float)((float)L_76*(float)L_77))))+(float)((float)((float)L_78*(float)L_79))));
		float L_80 = ((&___lhs)->___m20_2);
		float L_81 = ((&___rhs)->___m02_8);
		float L_82 = ((&___lhs)->___m21_6);
		float L_83 = ((&___rhs)->___m12_9);
		float L_84 = ((&___lhs)->___m22_10);
		float L_85 = ((&___rhs)->___m22_10);
		float L_86 = ((&___lhs)->___m23_14);
		float L_87 = ((&___rhs)->___m32_11);
		(&V_0)->___m22_10 = ((float)((float)((float)((float)((float)((float)((float)((float)L_80*(float)L_81))+(float)((float)((float)L_82*(float)L_83))))+(float)((float)((float)L_84*(float)L_85))))+(float)((float)((float)L_86*(float)L_87))));
		float L_88 = ((&___lhs)->___m20_2);
		float L_89 = ((&___rhs)->___m03_12);
		float L_90 = ((&___lhs)->___m21_6);
		float L_91 = ((&___rhs)->___m13_13);
		float L_92 = ((&___lhs)->___m22_10);
		float L_93 = ((&___rhs)->___m23_14);
		float L_94 = ((&___lhs)->___m23_14);
		float L_95 = ((&___rhs)->___m33_15);
		(&V_0)->___m23_14 = ((float)((float)((float)((float)((float)((float)((float)((float)L_88*(float)L_89))+(float)((float)((float)L_90*(float)L_91))))+(float)((float)((float)L_92*(float)L_93))))+(float)((float)((float)L_94*(float)L_95))));
		float L_96 = ((&___lhs)->___m30_3);
		float L_97 = ((&___rhs)->___m00_0);
		float L_98 = ((&___lhs)->___m31_7);
		float L_99 = ((&___rhs)->___m10_1);
		float L_100 = ((&___lhs)->___m32_11);
		float L_101 = ((&___rhs)->___m20_2);
		float L_102 = ((&___lhs)->___m33_15);
		float L_103 = ((&___rhs)->___m30_3);
		(&V_0)->___m30_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_96*(float)L_97))+(float)((float)((float)L_98*(float)L_99))))+(float)((float)((float)L_100*(float)L_101))))+(float)((float)((float)L_102*(float)L_103))));
		float L_104 = ((&___lhs)->___m30_3);
		float L_105 = ((&___rhs)->___m01_4);
		float L_106 = ((&___lhs)->___m31_7);
		float L_107 = ((&___rhs)->___m11_5);
		float L_108 = ((&___lhs)->___m32_11);
		float L_109 = ((&___rhs)->___m21_6);
		float L_110 = ((&___lhs)->___m33_15);
		float L_111 = ((&___rhs)->___m31_7);
		(&V_0)->___m31_7 = ((float)((float)((float)((float)((float)((float)((float)((float)L_104*(float)L_105))+(float)((float)((float)L_106*(float)L_107))))+(float)((float)((float)L_108*(float)L_109))))+(float)((float)((float)L_110*(float)L_111))));
		float L_112 = ((&___lhs)->___m30_3);
		float L_113 = ((&___rhs)->___m02_8);
		float L_114 = ((&___lhs)->___m31_7);
		float L_115 = ((&___rhs)->___m12_9);
		float L_116 = ((&___lhs)->___m32_11);
		float L_117 = ((&___rhs)->___m22_10);
		float L_118 = ((&___lhs)->___m33_15);
		float L_119 = ((&___rhs)->___m32_11);
		(&V_0)->___m32_11 = ((float)((float)((float)((float)((float)((float)((float)((float)L_112*(float)L_113))+(float)((float)((float)L_114*(float)L_115))))+(float)((float)((float)L_116*(float)L_117))))+(float)((float)((float)L_118*(float)L_119))));
		float L_120 = ((&___lhs)->___m30_3);
		float L_121 = ((&___rhs)->___m03_12);
		float L_122 = ((&___lhs)->___m31_7);
		float L_123 = ((&___rhs)->___m13_13);
		float L_124 = ((&___lhs)->___m32_11);
		float L_125 = ((&___rhs)->___m23_14);
		float L_126 = ((&___lhs)->___m33_15);
		float L_127 = ((&___rhs)->___m33_15);
		(&V_0)->___m33_15 = ((float)((float)((float)((float)((float)((float)((float)((float)L_120*(float)L_121))+(float)((float)((float)L_122*(float)L_123))))+(float)((float)((float)L_124*(float)L_125))))+(float)((float)((float)L_126*(float)L_127))));
		Matrix4x4_t603  L_128 = V_0;
		return L_128;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C" Vector4_t822  Matrix4x4_op_Multiply_m6467 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___lhs, Vector4_t822  ___v, const MethodInfo* method)
{
	Vector4_t822  V_0 = {0};
	{
		float L_0 = ((&___lhs)->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = ((&___lhs)->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = ((&___lhs)->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = ((&___lhs)->___m03_12);
		float L_7 = ((&___v)->___w_4);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		float L_8 = ((&___lhs)->___m10_1);
		float L_9 = ((&___v)->___x_1);
		float L_10 = ((&___lhs)->___m11_5);
		float L_11 = ((&___v)->___y_2);
		float L_12 = ((&___lhs)->___m12_9);
		float L_13 = ((&___v)->___z_3);
		float L_14 = ((&___lhs)->___m13_13);
		float L_15 = ((&___v)->___w_4);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15))));
		float L_16 = ((&___lhs)->___m20_2);
		float L_17 = ((&___v)->___x_1);
		float L_18 = ((&___lhs)->___m21_6);
		float L_19 = ((&___v)->___y_2);
		float L_20 = ((&___lhs)->___m22_10);
		float L_21 = ((&___v)->___z_3);
		float L_22 = ((&___lhs)->___m23_14);
		float L_23 = ((&___v)->___w_4);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = ((&___lhs)->___m30_3);
		float L_25 = ((&___v)->___x_1);
		float L_26 = ((&___lhs)->___m31_7);
		float L_27 = ((&___v)->___y_2);
		float L_28 = ((&___lhs)->___m32_11);
		float L_29 = ((&___v)->___z_3);
		float L_30 = ((&___lhs)->___m33_15);
		float L_31 = ((&___v)->___w_4);
		(&V_0)->___w_4 = ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31))));
		Vector4_t822  L_32 = V_0;
		return L_32;
	}
}
// System.Boolean UnityEngine.Matrix4x4::op_Equality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" bool Matrix4x4_op_Equality_m7899 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___lhs, Matrix4x4_t603  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		Vector4_t822  L_0 = Matrix4x4_GetColumn_m7885((&___lhs), 0, /*hidden argument*/NULL);
		Vector4_t822  L_1 = Matrix4x4_GetColumn_m7885((&___rhs), 0, /*hidden argument*/NULL);
		bool L_2 = Vector4_op_Equality_m7931(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t822  L_3 = Matrix4x4_GetColumn_m7885((&___lhs), 1, /*hidden argument*/NULL);
		Vector4_t822  L_4 = Matrix4x4_GetColumn_m7885((&___rhs), 1, /*hidden argument*/NULL);
		bool L_5 = Vector4_op_Equality_m7931(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t822  L_6 = Matrix4x4_GetColumn_m7885((&___lhs), 2, /*hidden argument*/NULL);
		Vector4_t822  L_7 = Matrix4x4_GetColumn_m7885((&___rhs), 2, /*hidden argument*/NULL);
		bool L_8 = Vector4_op_Equality_m7931(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t822  L_9 = Matrix4x4_GetColumn_m7885((&___lhs), 3, /*hidden argument*/NULL);
		Vector4_t822  L_10 = Matrix4x4_GetColumn_m7885((&___rhs), 3, /*hidden argument*/NULL);
		bool L_11 = Vector4_op_Equality_m7931(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0066;
	}

IL_0065:
	{
		G_B5_0 = 0;
	}

IL_0066:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Matrix4x4::op_Inequality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" bool Matrix4x4_op_Inequality_m7900 (Object_t * __this /* static, unused */, Matrix4x4_t603  ___lhs, Matrix4x4_t603  ___rhs, const MethodInfo* method)
{
	{
		Matrix4x4_t603  L_0 = ___lhs;
		Matrix4x4_t603  L_1 = ___rhs;
		bool L_2 = Matrix4x4_op_Equality_m7899(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Bounds__ctor_m4328 (Bounds_t742 * __this, Vector3_t6  ___center, Vector3_t6  ___size, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___center;
		__this->___m_Center_0 = L_0;
		Vector3_t6  L_1 = ___size;
		Vector3_t6  L_2 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->___m_Extents_1 = L_2;
		return;
	}
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C" int32_t Bounds_GetHashCode_m7901 (Bounds_t742 * __this, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	Vector3_t6  V_1 = {0};
	{
		Vector3_t6  L_0 = Bounds_get_center_m4329(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m7839((&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_2 = Bounds_get_extents_m7903(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m7839((&V_1), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern TypeInfo* Bounds_t742_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_il2cpp_TypeInfo_var;
extern "C" bool Bounds_Equals_m7902 (Bounds_t742 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Bounds_t742_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(579);
		Vector3_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t742  V_0 = {0};
	Vector3_t6  V_1 = {0};
	Vector3_t6  V_2 = {0};
	int32_t G_B5_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Bounds_t742_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Bounds_t742 *)((Bounds_t742 *)UnBox (L_1, Bounds_t742_il2cpp_TypeInfo_var))));
		Vector3_t6  L_2 = Bounds_get_center_m4329(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t6  L_3 = Bounds_get_center_m4329((&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_4 = L_3;
		Object_t * L_5 = Box(Vector3_t6_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m7840((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		Vector3_t6  L_7 = Bounds_get_extents_m7903(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t6  L_8 = Bounds_get_extents_m7903((&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_9 = L_8;
		Object_t * L_10 = Box(Vector3_t6_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m7840((&V_2), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0050;
	}

IL_004f:
	{
		G_B5_0 = 0;
	}

IL_0050:
	{
		return G_B5_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C" Vector3_t6  Bounds_get_center_m4329 (Bounds_t742 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Center_0);
		return L_0;
	}
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C" void Bounds_set_center_m4331 (Bounds_t742 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___value;
		__this->___m_Center_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" Vector3_t6  Bounds_get_size_m4320 (Bounds_t742 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Extents_1);
		Vector3_t6  L_1 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C" void Bounds_set_size_m4330 (Bounds_t742 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___value;
		Vector3_t6  L_1 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->___m_Extents_1 = L_1;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C" Vector3_t6  Bounds_get_extents_m7903 (Bounds_t742 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Extents_1);
		return L_0;
	}
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C" void Bounds_set_extents_m7904 (Bounds_t742 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___value;
		__this->___m_Extents_1 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C" Vector3_t6  Bounds_get_min_m4324 (Bounds_t742 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Bounds_get_center_m4329(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = Bounds_get_extents_m7903(__this, /*hidden argument*/NULL);
		Vector3_t6  L_2 = Vector3_op_Subtraction_m292(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Bounds::set_min(UnityEngine.Vector3)
extern "C" void Bounds_set_min_m7905 (Bounds_t742 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___value;
		Vector3_t6  L_1 = Bounds_get_max_m4338(__this, /*hidden argument*/NULL);
		Bounds_SetMinMax_m7907(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C" Vector3_t6  Bounds_get_max_m4338 (Bounds_t742 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Bounds_get_center_m4329(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = Bounds_get_extents_m7903(__this, /*hidden argument*/NULL);
		Vector3_t6  L_2 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Bounds::set_max(UnityEngine.Vector3)
extern "C" void Bounds_set_max_m7906 (Bounds_t742 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Bounds_get_min_m4324(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = ___value;
		Bounds_SetMinMax_m7907(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Bounds_SetMinMax_m7907 (Bounds_t742 * __this, Vector3_t6  ___min, Vector3_t6  ___max, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___max;
		Vector3_t6  L_1 = ___min;
		Vector3_t6  L_2 = Vector3_op_Subtraction_m292(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t6  L_3 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m7904(__this, L_3, /*hidden argument*/NULL);
		Vector3_t6  L_4 = ___min;
		Vector3_t6  L_5 = Bounds_get_extents_m7903(__this, /*hidden argument*/NULL);
		Vector3_t6  L_6 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m4331(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C" void Bounds_Encapsulate_m4337 (Bounds_t742 * __this, Vector3_t6  ___point, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Bounds_get_min_m4324(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = ___point;
		Vector3_t6  L_2 = Vector3_Min_m4335(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t6  L_3 = Bounds_get_max_m4338(__this, /*hidden argument*/NULL);
		Vector3_t6  L_4 = ___point;
		Vector3_t6  L_5 = Vector3_Max_m4336(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m7907(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C" void Bounds_Encapsulate_m7908 (Bounds_t742 * __this, Bounds_t742  ___bounds, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Bounds_get_center_m4329((&___bounds), /*hidden argument*/NULL);
		Vector3_t6  L_1 = Bounds_get_extents_m7903((&___bounds), /*hidden argument*/NULL);
		Vector3_t6  L_2 = Vector3_op_Subtraction_m292(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Bounds_Encapsulate_m4337(__this, L_2, /*hidden argument*/NULL);
		Vector3_t6  L_3 = Bounds_get_center_m4329((&___bounds), /*hidden argument*/NULL);
		Vector3_t6  L_4 = Bounds_get_extents_m7903((&___bounds), /*hidden argument*/NULL);
		Vector3_t6  L_5 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_Encapsulate_m4337(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Expand(System.Single)
extern "C" void Bounds_Expand_m7909 (Bounds_t742 * __this, float ___amount, const MethodInfo* method)
{
	{
		float L_0 = ___amount;
		___amount = ((float)((float)L_0*(float)(0.5f)));
		Vector3_t6  L_1 = Bounds_get_extents_m7903(__this, /*hidden argument*/NULL);
		float L_2 = ___amount;
		float L_3 = ___amount;
		float L_4 = ___amount;
		Vector3_t6  L_5 = {0};
		Vector3__ctor_m335(&L_5, L_2, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t6  L_6 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_1, L_5, /*hidden argument*/NULL);
		Bounds_set_extents_m7904(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Expand(UnityEngine.Vector3)
extern "C" void Bounds_Expand_m7910 (Bounds_t742 * __this, Vector3_t6  ___amount, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Bounds_get_extents_m7903(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = ___amount;
		Vector3_t6  L_2 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		Vector3_t6  L_3 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Bounds_set_extents_m7904(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Bounds::Intersects(UnityEngine.Bounds)
extern "C" bool Bounds_Intersects_m7911 (Bounds_t742 * __this, Bounds_t742  ___bounds, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	Vector3_t6  V_1 = {0};
	Vector3_t6  V_2 = {0};
	Vector3_t6  V_3 = {0};
	Vector3_t6  V_4 = {0};
	Vector3_t6  V_5 = {0};
	Vector3_t6  V_6 = {0};
	Vector3_t6  V_7 = {0};
	Vector3_t6  V_8 = {0};
	Vector3_t6  V_9 = {0};
	Vector3_t6  V_10 = {0};
	Vector3_t6  V_11 = {0};
	int32_t G_B7_0 = 0;
	{
		Vector3_t6  L_0 = Bounds_get_min_m4324(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		Vector3_t6  L_2 = Bounds_get_max_m4338((&___bounds), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___x_1);
		if ((!(((float)L_1) <= ((float)L_3))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6  L_4 = Bounds_get_max_m4338(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___x_1);
		Vector3_t6  L_6 = Bounds_get_min_m4324((&___bounds), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___x_1);
		if ((!(((float)L_5) >= ((float)L_7))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6  L_8 = Bounds_get_min_m4324(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = ((&V_4)->___y_2);
		Vector3_t6  L_10 = Bounds_get_max_m4338((&___bounds), /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = ((&V_5)->___y_2);
		if ((!(((float)L_9) <= ((float)L_11))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6  L_12 = Bounds_get_max_m4338(__this, /*hidden argument*/NULL);
		V_6 = L_12;
		float L_13 = ((&V_6)->___y_2);
		Vector3_t6  L_14 = Bounds_get_min_m4324((&___bounds), /*hidden argument*/NULL);
		V_7 = L_14;
		float L_15 = ((&V_7)->___y_2);
		if ((!(((float)L_13) >= ((float)L_15))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6  L_16 = Bounds_get_min_m4324(__this, /*hidden argument*/NULL);
		V_8 = L_16;
		float L_17 = ((&V_8)->___z_3);
		Vector3_t6  L_18 = Bounds_get_max_m4338((&___bounds), /*hidden argument*/NULL);
		V_9 = L_18;
		float L_19 = ((&V_9)->___z_3);
		if ((!(((float)L_17) <= ((float)L_19))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6  L_20 = Bounds_get_max_m4338(__this, /*hidden argument*/NULL);
		V_10 = L_20;
		float L_21 = ((&V_10)->___z_3);
		Vector3_t6  L_22 = Bounds_get_min_m4324((&___bounds), /*hidden argument*/NULL);
		V_11 = L_22;
		float L_23 = ((&V_11)->___z_3);
		G_B7_0 = ((((int32_t)((!(((float)L_21) >= ((float)L_23)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00d7;
	}

IL_00d6:
	{
		G_B7_0 = 0;
	}

IL_00d7:
	{
		return G_B7_0;
	}
}
// System.Boolean UnityEngine.Bounds::Internal_Contains(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C" bool Bounds_Internal_Contains_m7912 (Object_t * __this /* static, unused */, Bounds_t742  ___m, Vector3_t6  ___point, const MethodInfo* method)
{
	{
		bool L_0 = Bounds_INTERNAL_CALL_Internal_Contains_m7913(NULL /*static, unused*/, (&___m), (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" bool Bounds_INTERNAL_CALL_Internal_Contains_m7913 (Object_t * __this /* static, unused */, Bounds_t742 * ___m, Vector3_t6 * ___point, const MethodInfo* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_Contains_m7913_ftn) (Bounds_t742 *, Vector3_t6 *);
	static Bounds_INTERNAL_CALL_Internal_Contains_m7913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_Contains_m7913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___m, ___point);
}
// System.Boolean UnityEngine.Bounds::Contains(UnityEngine.Vector3)
extern "C" bool Bounds_Contains_m7914 (Bounds_t742 * __this, Vector3_t6  ___point, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___point;
		bool L_1 = Bounds_Internal_Contains_m7912(NULL /*static, unused*/, (*(Bounds_t742 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Bounds::Internal_SqrDistance(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C" float Bounds_Internal_SqrDistance_m7915 (Object_t * __this /* static, unused */, Bounds_t742  ___m, Vector3_t6  ___point, const MethodInfo* method)
{
	{
		float L_0 = Bounds_INTERNAL_CALL_Internal_SqrDistance_m7916(NULL /*static, unused*/, (&___m), (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" float Bounds_INTERNAL_CALL_Internal_SqrDistance_m7916 (Object_t * __this /* static, unused */, Bounds_t742 * ___m, Vector3_t6 * ___point, const MethodInfo* method)
{
	typedef float (*Bounds_INTERNAL_CALL_Internal_SqrDistance_m7916_ftn) (Bounds_t742 *, Vector3_t6 *);
	static Bounds_INTERNAL_CALL_Internal_SqrDistance_m7916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_SqrDistance_m7916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___m, ___point);
}
// System.Single UnityEngine.Bounds::SqrDistance(UnityEngine.Vector3)
extern "C" float Bounds_SqrDistance_m7917 (Bounds_t742 * __this, Vector3_t6  ___point, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___point;
		float L_1 = Bounds_Internal_SqrDistance_m7915(NULL /*static, unused*/, (*(Bounds_t742 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Bounds::Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
extern "C" bool Bounds_Internal_IntersectRay_m7918 (Object_t * __this /* static, unused */, Ray_t567 * ___ray, Bounds_t742 * ___bounds, float* ___distance, const MethodInfo* method)
{
	{
		Ray_t567 * L_0 = ___ray;
		Bounds_t742 * L_1 = ___bounds;
		float* L_2 = ___distance;
		bool L_3 = Bounds_INTERNAL_CALL_Internal_IntersectRay_m7919(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
extern "C" bool Bounds_INTERNAL_CALL_Internal_IntersectRay_m7919 (Object_t * __this /* static, unused */, Ray_t567 * ___ray, Bounds_t742 * ___bounds, float* ___distance, const MethodInfo* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_IntersectRay_m7919_ftn) (Ray_t567 *, Bounds_t742 *, float*);
	static Bounds_INTERNAL_CALL_Internal_IntersectRay_m7919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_IntersectRay_m7919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)");
	return _il2cpp_icall_func(___ray, ___bounds, ___distance);
}
// System.Boolean UnityEngine.Bounds::IntersectRay(UnityEngine.Ray)
extern "C" bool Bounds_IntersectRay_m7920 (Bounds_t742 * __this, Ray_t567  ___ray, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		bool L_0 = Bounds_Internal_IntersectRay_m7918(NULL /*static, unused*/, (&___ray), __this, (&V_0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Bounds::IntersectRay(UnityEngine.Ray,System.Single&)
extern "C" bool Bounds_IntersectRay_m7921 (Bounds_t742 * __this, Ray_t567  ___ray, float* ___distance, const MethodInfo* method)
{
	{
		float* L_0 = ___distance;
		bool L_1 = Bounds_Internal_IntersectRay_m7918(NULL /*static, unused*/, (&___ray), __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String UnityEngine.Bounds::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1290;
extern "C" String_t* Bounds_ToString_m7922 (Bounds_t742 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Vector3_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral1290 = il2cpp_codegen_string_literal_from_index(1290);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 2));
		Vector3_t6  L_1 = (__this->___m_Center_0);
		Vector3_t6  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t6_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		Vector3_t6  L_5 = (__this->___m_Extents_1);
		Vector3_t6  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t6_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1290, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String UnityEngine.Bounds::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1290;
extern "C" String_t* Bounds_ToString_m7923 (Bounds_t742 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral1290 = il2cpp_codegen_string_literal_from_index(1290);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 2));
		Vector3_t6 * L_1 = &(__this->___m_Center_0);
		String_t* L_2 = ___format;
		String_t* L_3 = Vector3_ToString_m7844(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		Vector3_t6 * L_5 = &(__this->___m_Extents_1);
		String_t* L_6 = ___format;
		String_t* L_7 = Vector3_ToString_m7844(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1290, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool Bounds_op_Equality_m7924 (Object_t * __this /* static, unused */, Bounds_t742  ___lhs, Bounds_t742  ___rhs, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Vector3_t6  L_0 = Bounds_get_center_m4329((&___lhs), /*hidden argument*/NULL);
		Vector3_t6  L_1 = Bounds_get_center_m4329((&___rhs), /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m363(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Vector3_t6  L_3 = Bounds_get_extents_m7903((&___lhs), /*hidden argument*/NULL);
		Vector3_t6  L_4 = Bounds_get_extents_m7903((&___rhs), /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m363(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool Bounds_op_Inequality_m4322 (Object_t * __this /* static, unused */, Bounds_t742  ___lhs, Bounds_t742  ___rhs, const MethodInfo* method)
{
	{
		Bounds_t742  L_0 = ___lhs;
		Bounds_t742  L_1 = ___rhs;
		bool L_2 = Bounds_op_Equality_m7924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Vector4__ctor_m4143 (Vector4_t822 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		float L_3 = ___w;
		__this->___w_4 = L_3;
		return;
	}
}
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1291;
extern "C" float Vector4_get_Item_m4202 (Vector4_t822 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1291 = il2cpp_codegen_string_literal_from_index(1291);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0024;
		}
		if (L_1 == 2)
		{
			goto IL_002b;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0039;
	}

IL_001d:
	{
		float L_2 = (__this->___x_1);
		return L_2;
	}

IL_0024:
	{
		float L_3 = (__this->___y_2);
		return L_3;
	}

IL_002b:
	{
		float L_4 = (__this->___z_3);
		return L_4;
	}

IL_0032:
	{
		float L_5 = (__this->___w_4);
		return L_5;
	}

IL_0039:
	{
		IndexOutOfRangeException_t1476 * L_6 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_6, _stringLiteral1291, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}
}
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1291;
extern "C" void Vector4_set_Item_m4204 (Vector4_t822 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(974);
		_stringLiteral1291 = il2cpp_codegen_string_literal_from_index(1291);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0035;
		}
		if (L_1 == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_001d:
	{
		float L_2 = ___value;
		__this->___x_1 = L_2;
		goto IL_0058;
	}

IL_0029:
	{
		float L_3 = ___value;
		__this->___y_2 = L_3;
		goto IL_0058;
	}

IL_0035:
	{
		float L_4 = ___value;
		__this->___z_3 = L_4;
		goto IL_0058;
	}

IL_0041:
	{
		float L_5 = ___value;
		__this->___w_4 = L_5;
		goto IL_0058;
	}

IL_004d:
	{
		IndexOutOfRangeException_t1476 * L_6 = (IndexOutOfRangeException_t1476 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1476_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m8198(L_6, _stringLiteral1291, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0058:
	{
		return;
	}
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C" int32_t Vector4_GetHashCode_m7925 (Vector4_t822 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m8176(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m8176(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m8176(L_4, /*hidden argument*/NULL);
		float* L_6 = &(__this->___w_4);
		int32_t L_7 = Single_GetHashCode_m8176(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern TypeInfo* Vector4_t822_il2cpp_TypeInfo_var;
extern "C" bool Vector4_Equals_m7926 (Vector4_t822 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector4_t822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(951);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t822  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector4_t822_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector4_t822 *)((Vector4_t822 *)UnBox (L_1, Vector4_t822_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m8199(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m8199(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m8199(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___w_4);
		float L_12 = ((&V_0)->___w_4);
		bool L_13 = Single_Equals_m8199(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.Vector4::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1285;
extern "C" String_t* Vector4_ToString_m7927 (Vector4_t822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Single_t36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1285 = il2cpp_codegen_string_literal_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t36_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t36_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t34* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t36_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t34* L_12 = L_8;
		float L_13 = (__this->___w_4);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t36_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1285, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" float Vector4_Dot_m7928 (Object_t * __this /* static, unused */, Vector4_t822  ___a, Vector4_t822  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C" float Vector4_SqrMagnitude_m7929 (Object_t * __this /* static, unused */, Vector4_t822  ___a, const MethodInfo* method)
{
	{
		Vector4_t822  L_0 = ___a;
		Vector4_t822  L_1 = ___a;
		float L_2 = Vector4_Dot_m7928(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C" float Vector4_get_sqrMagnitude_m4185 (Vector4_t822 * __this, const MethodInfo* method)
{
	{
		float L_0 = Vector4_Dot_m7928(NULL /*static, unused*/, (*(Vector4_t822 *)__this), (*(Vector4_t822 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C" Vector4_t822  Vector4_get_zero_m4188 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector4_t822  L_0 = {0};
		Vector4__ctor_m4143(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" Vector4_t822  Vector4_op_Subtraction_m7930 (Object_t * __this /* static, unused */, Vector4_t822  ___a, Vector4_t822  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		Vector4_t822  L_8 = {0};
		Vector4__ctor_m4143(&L_8, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C" Vector4_t822  Vector4_op_Division_m4198 (Object_t * __this /* static, unused */, Vector4_t822  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		float L_6 = ((&___a)->___w_4);
		float L_7 = ___d;
		Vector4_t822  L_8 = {0};
		Vector4__ctor_m4143(&L_8, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" bool Vector4_op_Equality_m7931 (Object_t * __this /* static, unused */, Vector4_t822  ___lhs, Vector4_t822  ___rhs, const MethodInfo* method)
{
	{
		Vector4_t822  L_0 = ___lhs;
		Vector4_t822  L_1 = ___rhs;
		Vector4_t822  L_2 = Vector4_op_Subtraction_m7930(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m7929(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector4::op_Inequality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" bool Vector4_op_Inequality_m7932 (Object_t * __this /* static, unused */, Vector4_t822  ___lhs, Vector4_t822  ___rhs, const MethodInfo* method)
{
	{
		Vector4_t822  L_0 = ___lhs;
		Vector4_t822  L_1 = ___rhs;
		Vector4_t822  L_2 = Vector4_op_Subtraction_m7930(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m7929(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Ray__ctor_m7933 (Ray_t567 * __this, Vector3_t6  ___origin, Vector3_t6  ___direction, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___origin;
		__this->___m_Origin_0 = L_0;
		Vector3_t6  L_1 = Vector3_get_normalized_m2574((&___direction), /*hidden argument*/NULL);
		__this->___m_Direction_1 = L_1;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C" Vector3_t6  Ray_get_origin_m4085 (Ray_t567 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Origin_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C" Vector3_t6  Ray_get_direction_m4086 (Ray_t567 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Direction_1);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C" Vector3_t6  Ray_GetPoint_m4249 (Ray_t567 * __this, float ___distance, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Origin_0);
		Vector3_t6  L_1 = (__this->___m_Direction_1);
		float L_2 = ___distance;
		Vector3_t6  L_3 = Vector3_op_Multiply_m293(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t6  L_4 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.Ray::ToString()
extern TypeInfo* ObjectU5BU5D_t34_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1292;
extern "C" String_t* Ray_ToString_m7934 (Ray_t567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Vector3_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral1292 = il2cpp_codegen_string_literal_from_index(1292);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t34* L_0 = ((ObjectU5BU5D_t34*)SZArrayNew(ObjectU5BU5D_t34_il2cpp_TypeInfo_var, 2));
		Vector3_t6  L_1 = (__this->___m_Origin_0);
		Vector3_t6  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t6_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t34* L_4 = L_0;
		Vector3_t6  L_5 = (__this->___m_Direction_1);
		Vector3_t6  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t6_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m7985(NULL /*static, unused*/, _stringLiteral1292, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Plane__ctor_m4247 (Plane_t856 * __this, Vector3_t6  ___inNormal, Vector3_t6  ___inPoint, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___inNormal;
		Vector3_t6  L_1 = Vector3_Normalize_m7841(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___m_Normal_0 = L_1;
		Vector3_t6  L_2 = ___inNormal;
		Vector3_t6  L_3 = ___inPoint;
		float L_4 = Vector3_Dot_m4168(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->___m_Distance_1 = ((-L_4));
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C" Vector3_t6  Plane_get_normal_m7935 (Plane_t856 * __this, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = (__this->___m_Normal_0);
		return L_0;
	}
}
// System.Single UnityEngine.Plane::get_distance()
extern "C" float Plane_get_distance_m7936 (Plane_t856 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_1);
		return L_0;
	}
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C" bool Plane_Raycast_m4248 (Plane_t856 * __this, Ray_t567  ___ray, float* ___enter, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t6  L_0 = Ray_get_direction_m4086((&___ray), /*hidden argument*/NULL);
		Vector3_t6  L_1 = Plane_get_normal_m7935(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m4168(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t6  L_3 = Ray_get_origin_m4085((&___ray), /*hidden argument*/NULL);
		Vector3_t6  L_4 = Plane_get_normal_m7935(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m4168(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m7936(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		bool L_8 = Mathf_Approximately_m4062(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		float* L_9 = ___enter;
		*((float*)(L_9)) = (float)(0.0f);
		return 0;
	}

IL_0047:
	{
		float* L_10 = ___enter;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter;
		return ((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
	}
}
// System.Single UnityEngine.Mathf::Sin(System.Single)
extern "C" float Mathf_Sin_m7937 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = sin((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Cos(System.Single)
extern "C" float Mathf_Cos_m7938 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = cos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Asin(System.Single)
extern "C" float Mathf_Asin_m7939 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = asin((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Acos(System.Single)
extern "C" float Mathf_Acos_m7940 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = acos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C" float Mathf_Sqrt_m7941 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = sqrt((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C" float Mathf_Abs_m7942 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = fabsf(L_0);
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C" float Mathf_Min_m376 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C" int32_t Mathf_Min_m288 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a;
		int32_t L_1 = ___b;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C" float Mathf_Max_m4353 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single[])
extern "C" float Mathf_Max_m6755 (Object_t * __this /* static, unused */, SingleU5BU5D_t23* ___values, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		SingleU5BU5D_t23* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return (0.0f);
	}

IL_0010:
	{
		SingleU5BU5D_t23* L_2 = ___values;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		V_1 = (*(float*)(float*)SZArrayLdElema(L_2, L_3, sizeof(float)));
		V_2 = 1;
		goto IL_002c;
	}

IL_001b:
	{
		SingleU5BU5D_t23* L_4 = ___values;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		float L_7 = V_1;
		if ((!(((float)(*(float*)(float*)SZArrayLdElema(L_4, L_6, sizeof(float)))) > ((float)L_7))))
		{
			goto IL_0028;
		}
	}
	{
		SingleU5BU5D_t23* L_8 = ___values;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_1 = (*(float*)(float*)SZArrayLdElema(L_8, L_10, sizeof(float)));
	}

IL_0028:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_12 = V_2;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_001b;
		}
	}
	{
		float L_14 = V_1;
		return L_14;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C" int32_t Mathf_Max_m4262 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a;
		int32_t L_1 = ___b;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern "C" float Mathf_Pow_m7943 (Object_t * __this /* static, unused */, float ___f, float ___p, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = ___p;
		double L_2 = pow((((double)((double)L_0))), (((double)((double)L_1))));
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern "C" float Mathf_Log_m4386 (Object_t * __this /* static, unused */, float ___f, float ___p, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = ___p;
		double L_2 = Math_Log_m8201(NULL /*static, unused*/, (((double)((double)L_0))), (((double)((double)L_1))), /*hidden argument*/NULL);
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Floor(System.Single)
extern "C" float Mathf_Floor_m7944 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = floor((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C" float Mathf_Round_m7945 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C" int32_t Mathf_CeilToInt_m4404 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = ceil((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C" int32_t Mathf_FloorToInt_m287 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = floor((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C" int32_t Mathf_RoundToInt_m4191 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C" float Mathf_Sign_m4327 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001a;
	}

IL_0015:
	{
		G_B3_0 = (-1.0f);
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" float Mathf_Clamp_m375 (Object_t * __this /* static, unused */, float ___value, float ___min, float ___max, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = ___min;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		float L_2 = ___min;
		___value = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		float L_3 = ___value;
		float L_4 = ___max;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		float L_5 = ___max;
		___value = L_5;
	}

IL_0019:
	{
		float L_6 = ___value;
		return L_6;
	}
}
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C" int32_t Mathf_Clamp_m4120 (Object_t * __this /* static, unused */, int32_t ___value, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		int32_t L_1 = ___min;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_2 = ___min;
		___value = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		int32_t L_3 = ___value;
		int32_t L_4 = ___max;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___max;
		___value = L_5;
	}

IL_0019:
	{
		int32_t L_6 = ___value;
		return L_6;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C" float Mathf_Clamp01_m435 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (0.0f);
	}

IL_0011:
	{
		float L_1 = ___value;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (1.0f);
	}

IL_0022:
	{
		float L_2 = ___value;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C" float Mathf_Lerp_m434 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___from;
		float L_1 = ___to;
		float L_2 = ___from;
		float L_3 = ___t;
		float L_4 = Mathf_Clamp01_m435(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C" bool Mathf_Approximately_m4062 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ___b;
		float L_1 = ___a;
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a;
		float L_4 = fabsf(L_3);
		float L_5 = ___b;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m4353(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = Mathf_Max_m4353(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), (9.403955E-38f), /*hidden argument*/NULL);
		return ((((float)L_2) < ((float)L_8))? 1 : 0);
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern "C" float Mathf_SmoothDamp_m392 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current;
		float L_2 = ___target;
		float* L_3 = ___currentVelocity;
		float L_4 = ___smoothTime;
		float L_5 = V_1;
		float L_6 = V_0;
		float L_7 = Mathf_SmoothDamp_m4321(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern "C" float Mathf_SmoothDamp_m4321 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, float ___maxSpeed, float ___deltaTime, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		float L_0 = ___smoothTime;
		float L_1 = Mathf_Max_m4353(NULL /*static, unused*/, (0.0001f), L_0, /*hidden argument*/NULL);
		___smoothTime = L_1;
		float L_2 = ___smoothTime;
		V_0 = ((float)((float)(2.0f)/(float)L_2));
		float L_3 = V_0;
		float L_4 = ___deltaTime;
		V_1 = ((float)((float)L_3*(float)L_4));
		float L_5 = V_1;
		float L_6 = V_1;
		float L_7 = V_1;
		float L_8 = V_1;
		float L_9 = V_1;
		float L_10 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))+(float)((float)((float)((float)((float)(0.48f)*(float)L_6))*(float)L_7))))+(float)((float)((float)((float)((float)((float)((float)(0.235f)*(float)L_8))*(float)L_9))*(float)L_10))))));
		float L_11 = ___current;
		float L_12 = ___target;
		V_3 = ((float)((float)L_11-(float)L_12));
		float L_13 = ___target;
		V_4 = L_13;
		float L_14 = ___maxSpeed;
		float L_15 = ___smoothTime;
		V_5 = ((float)((float)L_14*(float)L_15));
		float L_16 = V_3;
		float L_17 = V_5;
		float L_18 = V_5;
		float L_19 = Mathf_Clamp_m375(NULL /*static, unused*/, L_16, ((-L_17)), L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = ___current;
		float L_21 = V_3;
		___target = ((float)((float)L_20-(float)L_21));
		float* L_22 = ___currentVelocity;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = ___deltaTime;
		V_6 = ((float)((float)((float)((float)(*((float*)L_22))+(float)((float)((float)L_23*(float)L_24))))*(float)L_25));
		float* L_26 = ___currentVelocity;
		float* L_27 = ___currentVelocity;
		float L_28 = V_0;
		float L_29 = V_6;
		float L_30 = V_2;
		*((float*)(L_26)) = (float)((float)((float)((float)((float)(*((float*)L_27))-(float)((float)((float)L_28*(float)L_29))))*(float)L_30));
		float L_31 = ___target;
		float L_32 = V_3;
		float L_33 = V_6;
		float L_34 = V_2;
		V_7 = ((float)((float)L_31+(float)((float)((float)((float)((float)L_32+(float)L_33))*(float)L_34))));
		float L_35 = V_4;
		float L_36 = ___current;
		float L_37 = V_7;
		float L_38 = V_4;
		if ((!(((uint32_t)((((float)((float)((float)L_35-(float)L_36))) > ((float)(0.0f)))? 1 : 0)) == ((uint32_t)((((float)L_37) > ((float)L_38))? 1 : 0)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_39 = V_4;
		V_7 = L_39;
		float* L_40 = ___currentVelocity;
		float L_41 = V_7;
		float L_42 = V_4;
		float L_43 = ___deltaTime;
		*((float*)(L_40)) = (float)((float)((float)((float)((float)L_41-(float)L_42))/(float)L_43));
	}

IL_00a0:
	{
		float L_44 = V_7;
		return L_44;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single)
extern "C" float Mathf_SmoothDampAngle_m393 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m388(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current;
		float L_2 = ___target;
		float* L_3 = ___currentVelocity;
		float L_4 = ___smoothTime;
		float L_5 = V_1;
		float L_6 = V_0;
		float L_7 = Mathf_SmoothDampAngle_m7946(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern "C" float Mathf_SmoothDampAngle_m7946 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, float ___maxSpeed, float ___deltaTime, const MethodInfo* method)
{
	{
		float L_0 = ___current;
		float L_1 = ___current;
		float L_2 = ___target;
		float L_3 = Mathf_DeltaAngle_m7947(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		___target = ((float)((float)L_0+(float)L_3));
		float L_4 = ___current;
		float L_5 = ___target;
		float* L_6 = ___currentVelocity;
		float L_7 = ___smoothTime;
		float L_8 = ___maxSpeed;
		float L_9 = ___deltaTime;
		float L_10 = Mathf_SmoothDamp_m4321(NULL /*static, unused*/, L_4, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern "C" float Mathf_Repeat_m4214 (Object_t * __this /* static, unused */, float ___t, float ___length, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		float L_1 = ___t;
		float L_2 = ___length;
		float L_3 = floorf(((float)((float)L_1/(float)L_2)));
		float L_4 = ___length;
		return ((float)((float)L_0-(float)((float)((float)L_3*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern "C" float Mathf_InverseLerp_m4213 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___from;
		float L_1 = ___to;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_002f;
		}
	}
	{
		float L_2 = ___value;
		float L_3 = ___from;
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0014;
		}
	}
	{
		return (0.0f);
	}

IL_0014:
	{
		float L_4 = ___value;
		float L_5 = ___to;
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0021;
		}
	}
	{
		return (1.0f);
	}

IL_0021:
	{
		float L_6 = ___value;
		float L_7 = ___from;
		___value = ((float)((float)L_6-(float)L_7));
		float L_8 = ___value;
		float L_9 = ___to;
		float L_10 = ___from;
		___value = ((float)((float)L_8/(float)((float)((float)L_9-(float)L_10))));
		float L_11 = ___value;
		return L_11;
	}

IL_002f:
	{
		float L_12 = ___from;
		float L_13 = ___to;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_005e;
		}
	}
	{
		float L_14 = ___value;
		float L_15 = ___to;
		if ((!(((float)L_14) < ((float)L_15))))
		{
			goto IL_0043;
		}
	}
	{
		return (1.0f);
	}

IL_0043:
	{
		float L_16 = ___value;
		float L_17 = ___from;
		if ((!(((float)L_16) > ((float)L_17))))
		{
			goto IL_0050;
		}
	}
	{
		return (0.0f);
	}

IL_0050:
	{
		float L_18 = ___value;
		float L_19 = ___to;
		float L_20 = ___from;
		float L_21 = ___to;
		return ((float)((float)(1.0f)-(float)((float)((float)((float)((float)L_18-(float)L_19))/(float)((float)((float)L_20-(float)L_21))))));
	}

IL_005e:
	{
		return (0.0f);
	}
}
// System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
extern "C" float Mathf_DeltaAngle_m7947 (Object_t * __this /* static, unused */, float ___current, float ___target, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___target;
		float L_1 = ___current;
		float L_2 = Mathf_Repeat_m4214(NULL /*static, unused*/, ((float)((float)L_0-(float)L_1)), (360.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) > ((float)(180.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4-(float)(360.0f)));
	}

IL_0021:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.ParticleSystem::get_loop()
extern "C" bool ParticleSystem_get_loop_m2563 (ParticleSystem_t332 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_loop_m2563_ftn) (ParticleSystem_t332 *);
	static ParticleSystem_get_loop_m2563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_loop_m2563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_loop()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)
extern "C" void ParticleSystem_set_enableEmission_m2583 (ParticleSystem_t332 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_enableEmission_m2583_ftn) (ParticleSystem_t332 *, bool);
	static ParticleSystem_set_enableEmission_m2583_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_enableEmission_m2583_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.ParticleSystem::Internal_Play()
extern "C" void ParticleSystem_Internal_Play_m7948 (ParticleSystem_t332 * __this, const MethodInfo* method)
{
	typedef void (*ParticleSystem_Internal_Play_m7948_ftn) (ParticleSystem_t332 *);
	static ParticleSystem_Internal_Play_m7948_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Play_m7948_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Play()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::Internal_Stop()
extern "C" void ParticleSystem_Internal_Stop_m7949 (ParticleSystem_t332 * __this, const MethodInfo* method)
{
	typedef void (*ParticleSystem_Internal_Stop_m7949_ftn) (ParticleSystem_t332 *);
	static ParticleSystem_Internal_Stop_m7949_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Stop_m7949_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Stop()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_IsAlive()
extern "C" bool ParticleSystem_Internal_IsAlive_m7950 (ParticleSystem_t332 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_IsAlive_m7950_ftn) (ParticleSystem_t332 *);
	static ParticleSystem_Internal_IsAlive_m7950_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_IsAlive_m7950_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_IsAlive()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::Play()
extern "C" void ParticleSystem_Play_m2604 (ParticleSystem_t332 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		bool L_0 = V_0;
		ParticleSystem_Play_m2584(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C" void ParticleSystem_Play_m2584 (ParticleSystem_t332 * __this, bool ___withChildren, const MethodInfo* method)
{
	ParticleSystemU5BU5D_t331* V_0 = {0};
	ParticleSystem_t332 * V_1 = {0};
	ParticleSystemU5BU5D_t331* V_2 = {0};
	int32_t V_3 = 0;
	{
		bool L_0 = ___withChildren;
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		ParticleSystemU5BU5D_t331* L_1 = ParticleSystem_GetParticleSystems_m7951(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		ParticleSystemU5BU5D_t331* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_0024;
	}

IL_0016:
	{
		ParticleSystemU5BU5D_t331* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(ParticleSystem_t332 **)(ParticleSystem_t332 **)SZArrayLdElema(L_3, L_5, sizeof(ParticleSystem_t332 *)));
		ParticleSystem_t332 * L_6 = V_1;
		NullCheck(L_6);
		ParticleSystem_Internal_Play_m7948(L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_8 = V_3;
		ParticleSystemU5BU5D_t331* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0038;
	}

IL_0032:
	{
		ParticleSystem_Internal_Play_m7948(__this, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C" void ParticleSystem_Stop_m2618 (ParticleSystem_t332 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		bool L_0 = V_0;
		ParticleSystem_Stop_m2559(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern "C" void ParticleSystem_Stop_m2559 (ParticleSystem_t332 * __this, bool ___withChildren, const MethodInfo* method)
{
	ParticleSystemU5BU5D_t331* V_0 = {0};
	ParticleSystem_t332 * V_1 = {0};
	ParticleSystemU5BU5D_t331* V_2 = {0};
	int32_t V_3 = 0;
	{
		bool L_0 = ___withChildren;
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		ParticleSystemU5BU5D_t331* L_1 = ParticleSystem_GetParticleSystems_m7951(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		ParticleSystemU5BU5D_t331* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_0024;
	}

IL_0016:
	{
		ParticleSystemU5BU5D_t331* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(ParticleSystem_t332 **)(ParticleSystem_t332 **)SZArrayLdElema(L_3, L_5, sizeof(ParticleSystem_t332 *)));
		ParticleSystem_t332 * L_6 = V_1;
		NullCheck(L_6);
		ParticleSystem_Internal_Stop_m7949(L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_8 = V_3;
		ParticleSystemU5BU5D_t331* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0038;
	}

IL_0032:
	{
		ParticleSystem_Internal_Stop_m7949(__this, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C" bool ParticleSystem_IsAlive_m2579 (ParticleSystem_t332 * __this, bool ___withChildren, const MethodInfo* method)
{
	ParticleSystemU5BU5D_t331* V_0 = {0};
	ParticleSystem_t332 * V_1 = {0};
	ParticleSystemU5BU5D_t331* V_2 = {0};
	int32_t V_3 = 0;
	{
		bool L_0 = ___withChildren;
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		ParticleSystemU5BU5D_t331* L_1 = ParticleSystem_GetParticleSystems_m7951(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		ParticleSystemU5BU5D_t331* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_002b;
	}

IL_0016:
	{
		ParticleSystemU5BU5D_t331* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(ParticleSystem_t332 **)(ParticleSystem_t332 **)SZArrayLdElema(L_3, L_5, sizeof(ParticleSystem_t332 *)));
		ParticleSystem_t332 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = ParticleSystem_Internal_IsAlive_m7950(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0027;
		}
	}
	{
		return 1;
	}

IL_0027:
	{
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_9 = V_3;
		ParticleSystemU5BU5D_t331* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_0016;
		}
	}
	{
		return 0;
	}

IL_0036:
	{
		bool L_11 = ParticleSystem_Internal_IsAlive_m7950(__this, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.ParticleSystem[] UnityEngine.ParticleSystem::GetParticleSystems(UnityEngine.ParticleSystem)
extern TypeInfo* List_1_t1446_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8202_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m8203_MethodInfo_var;
extern "C" ParticleSystemU5BU5D_t331* ParticleSystem_GetParticleSystems_m7951 (Object_t * __this /* static, unused */, ParticleSystem_t332 * ___root, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1446_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		List_1__ctor_m8202_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484731);
		List_1_ToArray_m8203_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484732);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1446 * V_0 = {0};
	{
		ParticleSystem_t332 * L_0 = ___root;
		bool L_1 = Object_op_Implicit_m301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (ParticleSystemU5BU5D_t331*)NULL;
	}

IL_000d:
	{
		List_1_t1446 * L_2 = (List_1_t1446 *)il2cpp_codegen_object_new (List_1_t1446_il2cpp_TypeInfo_var);
		List_1__ctor_m8202(L_2, /*hidden argument*/List_1__ctor_m8202_MethodInfo_var);
		V_0 = L_2;
		List_1_t1446 * L_3 = V_0;
		ParticleSystem_t332 * L_4 = ___root;
		NullCheck(L_3);
		VirtActionInvoker1< ParticleSystem_t332 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Add(!0) */, L_3, L_4);
		ParticleSystem_t332 * L_5 = ___root;
		NullCheck(L_5);
		Transform_t25 * L_6 = Component_get_transform_m416(L_5, /*hidden argument*/NULL);
		List_1_t1446 * L_7 = V_0;
		ParticleSystem_GetDirectParticleSystemChildrenRecursive_m7952(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		List_1_t1446 * L_8 = V_0;
		NullCheck(L_8);
		ParticleSystemU5BU5D_t331* L_9 = List_1_ToArray_m8203(L_8, /*hidden argument*/List_1_ToArray_m8203_MethodInfo_var);
		return L_9;
	}
}
// System.Void UnityEngine.ParticleSystem::GetDirectParticleSystemChildrenRecursive(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.ParticleSystem>)
extern TypeInfo* IEnumerator_t35_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t25_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t332_m2562_MethodInfo_var;
extern "C" void ParticleSystem_GetDirectParticleSystemChildrenRecursive_m7952 (Object_t * __this /* static, unused */, Transform_t25 * ___transform, List_1_t1446 * ___particleSystems, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Transform_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		GameObject_GetComponent_TisParticleSystem_t332_m2562_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484051);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t25 * V_0 = {0};
	Object_t * V_1 = {0};
	ParticleSystem_t332 * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t25 * L_0 = ___transform;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_0);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_000c:
		{
			Object_t * L_2 = V_1;
			NullCheck(L_2);
			Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t35_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Transform_t25 *)CastclassClass(L_3, Transform_t25_il2cpp_TypeInfo_var));
			Transform_t25 * L_4 = V_0;
			NullCheck(L_4);
			GameObject_t27 * L_5 = Component_get_gameObject_m306(L_4, /*hidden argument*/NULL);
			NullCheck(L_5);
			ParticleSystem_t332 * L_6 = GameObject_GetComponent_TisParticleSystem_t332_m2562(L_5, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t332_m2562_MethodInfo_var);
			V_2 = L_6;
			ParticleSystem_t332 * L_7 = V_2;
			bool L_8 = Object_op_Inequality_m395(NULL /*static, unused*/, L_7, (Object_t53 *)NULL, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_003e;
			}
		}

IL_0030:
		{
			List_1_t1446 * L_9 = ___particleSystems;
			ParticleSystem_t332 * L_10 = V_2;
			NullCheck(L_9);
			VirtActionInvoker1< ParticleSystem_t332 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Add(!0) */, L_9, L_10);
			Transform_t25 * L_11 = V_0;
			List_1_t1446 * L_12 = ___particleSystems;
			ParticleSystem_GetDirectParticleSystemChildrenRecursive_m7952(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		}

IL_003e:
		{
			Object_t * L_13 = V_1;
			NullCheck(L_13);
			bool L_14 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t35_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_000c;
			}
		}

IL_0049:
		{
			IL2CPP_LEAVE(0x60, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		{
			Object_t * L_15 = V_1;
			V_3 = ((Object_t *)IsInst(L_15, IDisposable_t42_il2cpp_TypeInfo_var));
			Object_t * L_16 = V_3;
			if (L_16)
			{
				goto IL_0059;
			}
		}

IL_0058:
		{
			IL2CPP_END_FINALLY(78)
		}

IL_0059:
		{
			Object_t * L_17 = V_3;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_17);
			IL2CPP_END_FINALLY(78)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0060:
	{
		return;
	}
}
// System.String UnityEngine.TextAsset::get_text()
extern "C" String_t* TextAsset_get_text_m2365 (TextAsset_t522 * __this, const MethodInfo* method)
{
	typedef String_t* (*TextAsset_get_text_m2365_ftn) (TextAsset_t522 *);
	static TextAsset_get_text_m2365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextAsset_get_text_m2365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextAsset::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.TextAsset::ToString()
extern "C" String_t* TextAsset_ToString_m7953 (TextAsset_t522 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TextAsset_get_text_m2365(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C" void SerializePrivateVariables__ctor_m7954 (SerializePrivateVariables_t1408 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m7955 (SerializeField_t1409 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C" Shader_t583 * Shader_Find_m2694 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef Shader_t583 * (*Shader_Find_m2694_ftn) (String_t*);
	static Shader_Find_m2694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m2694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C" int32_t Shader_PropertyToID_m7956 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m7956_ftn) (String_t*);
	static Shader_PropertyToID_m7956_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m7956_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Material::.ctor(System.String)
extern "C" void Material__ctor_m2774 (Material_t45 * __this, String_t* ___contents, const MethodInfo* method)
{
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___contents;
		Material_Internal_CreateWithString_m7967(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern "C" void Material__ctor_m6491 (Material_t45 * __this, Shader_t583 * ___shader, const MethodInfo* method)
{
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		Shader_t583 * L_0 = ___shader;
		Material_Internal_CreateWithShader_m7968(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C" void Material__ctor_m2325 (Material_t45 * __this, Material_t45 * ___source, const MethodInfo* method)
{
	{
		Object__ctor_m8054(__this, /*hidden argument*/NULL);
		Material_t45 * L_0 = ___source;
		Material_Internal_CreateWithMaterial_m7969(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Shader UnityEngine.Material::get_shader()
extern "C" Shader_t583 * Material_get_shader_m2692 (Material_t45 * __this, const MethodInfo* method)
{
	typedef Shader_t583 * (*Material_get_shader_m2692_ftn) (Material_t45 *);
	static Material_get_shader_m2692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_get_shader_m2692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::get_shader()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
extern "C" void Material_set_shader_m2693 (Material_t45 * __this, Shader_t583 * ___value, const MethodInfo* method)
{
	typedef void (*Material_set_shader_m2693_ftn) (Material_t45 *, Shader_t583 *);
	static Material_set_shader_m2693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_set_shader_m2693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::set_shader(UnityEngine.Shader)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Color UnityEngine.Material::get_color()
extern Il2CppCodeGenString* _stringLiteral753;
extern "C" Color_t5  Material_get_color_m313 (Material_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral753 = il2cpp_codegen_string_literal_from_index(753);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t5  L_0 = Material_GetColor_m351(__this, _stringLiteral753, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern Il2CppCodeGenString* _stringLiteral753;
extern "C" void Material_set_color_m319 (Material_t45 * __this, Color_t5  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral753 = il2cpp_codegen_string_literal_from_index(753);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t5  L_0 = ___value;
		Material_SetColor_m374(__this, _stringLiteral753, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern Il2CppCodeGenString* _stringLiteral1293;
extern "C" Texture_t731 * Material_get_mainTexture_m4368 (Material_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1293 = il2cpp_codegen_string_literal_from_index(1293);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t731 * L_0 = Material_GetTexture_m7962(__this, _stringLiteral1293, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern Il2CppCodeGenString* _stringLiteral1293;
extern "C" void Material_set_mainTexture_m2691 (Material_t45 * __this, Texture_t731 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1293 = il2cpp_codegen_string_literal_from_index(1293);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t731 * L_0 = ___value;
		Material_SetTexture_m7960(__this, _stringLiteral1293, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C" void Material_SetColor_m374 (Material_t45 * __this, String_t* ___propertyName, Color_t5  ___color, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m7956(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t5  L_2 = ___color;
		Material_SetColor_m7957(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C" void Material_SetColor_m7957 (Material_t45 * __this, int32_t ___nameID, Color_t5  ___color, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID;
		Material_INTERNAL_CALL_SetColor_m7958(NULL /*static, unused*/, __this, L_0, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C" void Material_INTERNAL_CALL_SetColor_m7958 (Object_t * __this /* static, unused */, Material_t45 * ___self, int32_t ___nameID, Color_t5 * ___color, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetColor_m7958_ftn) (Material_t45 *, int32_t, Color_t5 *);
	static Material_INTERNAL_CALL_SetColor_m7958_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetColor_m7958_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___nameID, ___color);
}
// UnityEngine.Color UnityEngine.Material::GetColor(System.String)
extern "C" Color_t5  Material_GetColor_m351 (Material_t45 * __this, String_t* ___propertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m7956(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t5  L_2 = Material_GetColor_m7959(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color UnityEngine.Material::GetColor(System.Int32)
extern "C" Color_t5  Material_GetColor_m7959 (Material_t45 * __this, int32_t ___nameID, const MethodInfo* method)
{
	typedef Color_t5  (*Material_GetColor_m7959_ftn) (Material_t45 *, int32_t);
	static Material_GetColor_m7959_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetColor_m7959_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetColor(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID);
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C" void Material_SetTexture_m7960 (Material_t45 * __this, String_t* ___propertyName, Texture_t731 * ___texture, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m7956(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t731 * L_2 = ___texture;
		Material_SetTexture_m7961(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C" void Material_SetTexture_m7961 (Material_t45 * __this, int32_t ___nameID, Texture_t731 * ___texture, const MethodInfo* method)
{
	typedef void (*Material_SetTexture_m7961_ftn) (Material_t45 *, int32_t, Texture_t731 *);
	static Material_SetTexture_m7961_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetTexture_m7961_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___nameID, ___texture);
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C" Texture_t731 * Material_GetTexture_m7962 (Material_t45 * __this, String_t* ___propertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m7956(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t731 * L_2 = Material_GetTexture_m7963(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C" Texture_t731 * Material_GetTexture_m7963 (Material_t45 * __this, int32_t ___nameID, const MethodInfo* method)
{
	typedef Texture_t731 * (*Material_GetTexture_m7963_ftn) (Material_t45 *, int32_t);
	static Material_GetTexture_m7963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetTexture_m7963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetTexture(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID);
}
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C" void Material_SetFloat_m7964 (Material_t45 * __this, String_t* ___propertyName, float ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m7956(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___value;
		Material_SetFloat_m7965(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern "C" void Material_SetFloat_m7965 (Material_t45 * __this, int32_t ___nameID, float ___value, const MethodInfo* method)
{
	typedef void (*Material_SetFloat_m7965_ftn) (Material_t45 *, int32_t, float);
	static Material_SetFloat_m7965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetFloat_m7965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___nameID, ___value);
}
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C" void Material_SetInt_m4364 (Material_t45 * __this, String_t* ___propertyName, int32_t ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = ___value;
		Material_SetFloat_m7964(__this, L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C" bool Material_HasProperty_m4361 (Material_t45 * __this, String_t* ___propertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m7956(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = Material_HasProperty_m7966(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C" bool Material_HasProperty_m7966 (Material_t45 * __this, int32_t ___nameID, const MethodInfo* method)
{
	typedef bool (*Material_HasProperty_m7966_ftn) (Material_t45 *, int32_t);
	static Material_HasProperty_m7966_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_HasProperty_m7966_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::HasProperty(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID);
}
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C" bool Material_SetPass_m2784 (Material_t45 * __this, int32_t ___pass, const MethodInfo* method)
{
	typedef bool (*Material_SetPass_m2784_ftn) (Material_t45 *, int32_t);
	static Material_SetPass_m2784_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetPass_m2784_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetPass(System.Int32)");
	return _il2cpp_icall_func(__this, ___pass);
}
// System.Void UnityEngine.Material::Internal_CreateWithString(UnityEngine.Material,System.String)
extern "C" void Material_Internal_CreateWithString_m7967 (Object_t * __this /* static, unused */, Material_t45 * ___mono, String_t* ___contents, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithString_m7967_ftn) (Material_t45 *, String_t*);
	static Material_Internal_CreateWithString_m7967_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithString_m7967_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithString(UnityEngine.Material,System.String)");
	_il2cpp_icall_func(___mono, ___contents);
}
// System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
extern "C" void Material_Internal_CreateWithShader_m7968 (Object_t * __this /* static, unused */, Material_t45 * ___mono, Shader_t583 * ___shader, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithShader_m7968_ftn) (Material_t45 *, Shader_t583 *);
	static Material_Internal_CreateWithShader_m7968_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithShader_m7968_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)");
	_il2cpp_icall_func(___mono, ___shader);
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C" void Material_Internal_CreateWithMaterial_m7969 (Object_t * __this /* static, unused */, Material_t45 * ___mono, Material_t45 * ___source, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m7969_ftn) (Material_t45 *, Material_t45 *);
	static Material_Internal_CreateWithMaterial_m7969_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m7969_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono, ___source);
}
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C" Rect_t30  Sprite_get_rect_m4190 (Sprite_t709 * __this, const MethodInfo* method)
{
	Rect_t30  V_0 = {0};
	{
		Sprite_INTERNAL_get_rect_m7970(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t30  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C" void Sprite_INTERNAL_get_rect_m7970 (Sprite_t709 * __this, Rect_t30 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_rect_m7970_ftn) (Sprite_t709 *, Rect_t30 *);
	static Sprite_INTERNAL_get_rect_m7970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_rect_m7970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C" float Sprite_get_pixelsPerUnit_m4186 (Sprite_t709 * __this, const MethodInfo* method)
{
	typedef float (*Sprite_get_pixelsPerUnit_m4186_ftn) (Sprite_t709 *);
	static Sprite_get_pixelsPerUnit_m4186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_pixelsPerUnit_m4186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_pixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C" Texture2D_t33 * Sprite_get_texture_m4183 (Sprite_t709 * __this, const MethodInfo* method)
{
	typedef Texture2D_t33 * (*Sprite_get_texture_m4183_ftn) (Sprite_t709 *);
	static Sprite_get_texture_m4183_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_texture_m4183_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_texture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C" Rect_t30  Sprite_get_textureRect_m4207 (Sprite_t709 * __this, const MethodInfo* method)
{
	Rect_t30  V_0 = {0};
	{
		Sprite_INTERNAL_get_textureRect_m7971(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t30  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C" void Sprite_INTERNAL_get_textureRect_m7971 (Sprite_t709 * __this, Rect_t30 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_textureRect_m7971_ftn) (Sprite_t709 *, Rect_t30 *);
	static Sprite_INTERNAL_get_textureRect_m7971_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_textureRect_m7971_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C" Vector4_t822  Sprite_get_border_m4184 (Sprite_t709 * __this, const MethodInfo* method)
{
	Vector4_t822  V_0 = {0};
	{
		Sprite_INTERNAL_get_border_m7972(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t822  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C" void Sprite_INTERNAL_get_border_m7972 (Sprite_t709 * __this, Vector4_t822 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_border_m7972_ftn) (Sprite_t709 *, Vector4_t822 *);
	static Sprite_INTERNAL_get_border_m7972_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_border_m7972_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C" Vector4_t822  DataUtility_GetInnerUV_m4197 (Object_t * __this /* static, unused */, Sprite_t709 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t822  (*DataUtility_GetInnerUV_m4197_ftn) (Sprite_t709 *);
	static DataUtility_GetInnerUV_m4197_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetInnerUV_m4197_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C" Vector4_t822  DataUtility_GetOuterUV_m4196 (Object_t * __this /* static, unused */, Sprite_t709 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t822  (*DataUtility_GetOuterUV_m4196_ftn) (Sprite_t709 *);
	static DataUtility_GetOuterUV_m4196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetOuterUV_m4196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C" Vector4_t822  DataUtility_GetPadding_m4189 (Object_t * __this /* static, unused */, Sprite_t709 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t822  (*DataUtility_GetPadding_m4189_ftn) (Sprite_t709 *);
	static DataUtility_GetPadding_m4189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetPadding_m4189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C" Vector2_t29  DataUtility_GetMinSize_m4205 (Object_t * __this /* static, unused */, Sprite_t709 * ___sprite, const MethodInfo* method)
{
	Vector2_t29  V_0 = {0};
	{
		Sprite_t709 * L_0 = ___sprite;
		DataUtility_Internal_GetMinSize_m7973(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t29  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C" void DataUtility_Internal_GetMinSize_m7973 (Object_t * __this /* static, unused */, Sprite_t709 * ___sprite, Vector2_t29 * ___output, const MethodInfo* method)
{
	typedef void (*DataUtility_Internal_GetMinSize_m7973_ftn) (Sprite_t709 *, Vector2_t29 *);
	static DataUtility_Internal_GetMinSize_m7973_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_Internal_GetMinSize_m7973_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___sprite, ___output);
}
// System.Void UnityEngine.WWWForm::.ctor()
extern TypeInfo* List_1_t1411_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t78_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t119_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m8204_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2160_MethodInfo_var;
extern "C" void WWWForm__ctor_m2406 (WWWForm_t293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(978);
		List_1_t78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		ByteU5BU5D_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(749);
		List_1__ctor_m8204_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484733);
		List_1__ctor_m2160_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483726);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		List_1_t1411 * L_0 = (List_1_t1411 *)il2cpp_codegen_object_new (List_1_t1411_il2cpp_TypeInfo_var);
		List_1__ctor_m8204(L_0, /*hidden argument*/List_1__ctor_m8204_MethodInfo_var);
		__this->___formData_0 = L_0;
		List_1_t78 * L_1 = (List_1_t78 *)il2cpp_codegen_object_new (List_1_t78_il2cpp_TypeInfo_var);
		List_1__ctor_m2160(L_1, /*hidden argument*/List_1__ctor_m2160_MethodInfo_var);
		__this->___fieldNames_1 = L_1;
		List_1_t78 * L_2 = (List_1_t78 *)il2cpp_codegen_object_new (List_1_t78_il2cpp_TypeInfo_var);
		List_1__ctor_m2160(L_2, /*hidden argument*/List_1__ctor_m2160_MethodInfo_var);
		__this->___fileNames_2 = L_2;
		List_1_t78 * L_3 = (List_1_t78 *)il2cpp_codegen_object_new (List_1_t78_il2cpp_TypeInfo_var);
		List_1__ctor_m2160(L_3, /*hidden argument*/List_1__ctor_m2160_MethodInfo_var);
		__this->___types_3 = L_3;
		__this->___boundary_4 = ((ByteU5BU5D_t119*)SZArrayNew(ByteU5BU5D_t119_il2cpp_TypeInfo_var, ((int32_t)40)));
		V_0 = 0;
		goto IL_0076;
	}

IL_0046:
	{
		int32_t L_4 = Random_Range_m8105(NULL /*static, unused*/, ((int32_t)48), ((int32_t)110), /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) <= ((int32_t)((int32_t)57))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)7));
	}

IL_005c:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)6));
	}

IL_0068:
	{
		ByteU5BU5D_t119* L_9 = (__this->___boundary_4);
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_10, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_11)));
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)((int32_t)40))))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern TypeInfo* Encoding_t1442_il2cpp_TypeInfo_var;
extern "C" void WWWForm_AddField_m2407 (WWWForm_t293 * __this, String_t* ___fieldName, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		s_Il2CppMethodIntialized = true;
	}
	Encoding_t1442 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
		Encoding_t1442 * L_0 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___fieldName;
		String_t* L_2 = ___value;
		Encoding_t1442 * L_3 = V_0;
		WWWForm_AddField_m7974(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String,System.Text.Encoding)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1294;
extern Il2CppCodeGenString* _stringLiteral1295;
extern "C" void WWWForm_AddField_m7974 (WWWForm_t293 * __this, String_t* ___fieldName, String_t* ___value, Encoding_t1442 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1294 = il2cpp_codegen_string_literal_from_index(1294);
		_stringLiteral1295 = il2cpp_codegen_string_literal_from_index(1295);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t78 * L_0 = (__this->___fieldNames_1);
		String_t* L_1 = ___fieldName;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_0, L_1);
		List_1_t78 * L_2 = (__this->___fileNames_2);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_2, (String_t*)NULL);
		List_1_t1411 * L_3 = (__this->___formData_0);
		Encoding_t1442 * L_4 = ___e;
		String_t* L_5 = ___value;
		NullCheck(L_4);
		ByteU5BU5D_t119* L_6 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, L_5);
		NullCheck(L_3);
		VirtActionInvoker1< ByteU5BU5D_t119* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Byte[]>::Add(!0) */, L_3, L_6);
		List_1_t78 * L_7 = (__this->___types_3);
		Encoding_t1442 * L_8 = ___e;
		NullCheck(L_8);
		String_t* L_9 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Text.Encoding::get_WebName() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2152(NULL /*static, unused*/, _stringLiteral1294, L_9, _stringLiteral1295, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_7, L_10);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String)
extern "C" void WWWForm_AddBinaryData_m2671 (WWWForm_t293 * __this, String_t* ___fieldName, ByteU5BU5D_t119* ___contents, String_t* ___fileName, const MethodInfo* method)
{
	String_t* V_0 = {0};
	{
		V_0 = (String_t*)NULL;
		String_t* L_0 = ___fieldName;
		ByteU5BU5D_t119* L_1 = ___contents;
		String_t* L_2 = ___fileName;
		String_t* L_3 = V_0;
		WWWForm_AddBinaryData_m7975(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1296;
extern Il2CppCodeGenString* _stringLiteral1297;
extern Il2CppCodeGenString* _stringLiteral1298;
extern Il2CppCodeGenString* _stringLiteral1299;
extern "C" void WWWForm_AddBinaryData_m7975 (WWWForm_t293 * __this, String_t* ___fieldName, ByteU5BU5D_t119* ___contents, String_t* ___fileName, String_t* ___mimeType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1296 = il2cpp_codegen_string_literal_from_index(1296);
		_stringLiteral1297 = il2cpp_codegen_string_literal_from_index(1297);
		_stringLiteral1298 = il2cpp_codegen_string_literal_from_index(1298);
		_stringLiteral1299 = il2cpp_codegen_string_literal_from_index(1299);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t G_B10_0 = 0;
	String_t* G_B13_0 = {0};
	String_t* G_B12_0 = {0};
	String_t* G_B14_0 = {0};
	String_t* G_B14_1 = {0};
	{
		__this->___containsFiles_5 = 1;
		ByteU5BU5D_t119* L_0 = ___contents;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))) <= ((int32_t)8)))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t119* L_1 = ___contents;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_1, L_2, sizeof(uint8_t)))) == ((uint32_t)((int32_t)137)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t119* L_3 = ___contents;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		int32_t L_4 = 1;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_4, sizeof(uint8_t)))) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t119* L_5 = ___contents;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		int32_t L_6 = 2;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_5, L_6, sizeof(uint8_t)))) == ((uint32_t)((int32_t)78)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t119* L_7 = ___contents;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		int32_t L_8 = 3;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_8, sizeof(uint8_t)))) == ((uint32_t)((int32_t)71)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t119* L_9 = ___contents;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		int32_t L_10 = 4;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_10, sizeof(uint8_t)))) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t119* L_11 = ___contents;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		int32_t L_12 = 5;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_11, L_12, sizeof(uint8_t)))) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t119* L_13 = ___contents;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		int32_t L_14 = 6;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_13, L_14, sizeof(uint8_t)))) == ((uint32_t)((int32_t)26)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t119* L_15 = ___contents;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 7);
		int32_t L_16 = 7;
		G_B10_0 = ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_15, L_16, sizeof(uint8_t)))) == ((int32_t)((int32_t)10)))? 1 : 0);
		goto IL_0063;
	}

IL_0062:
	{
		G_B10_0 = 0;
	}

IL_0063:
	{
		V_0 = G_B10_0;
		String_t* L_17 = ___fileName;
		if (L_17)
		{
			goto IL_0087;
		}
	}
	{
		String_t* L_18 = ___fieldName;
		bool L_19 = V_0;
		G_B12_0 = L_18;
		if (!L_19)
		{
			G_B13_0 = L_18;
			goto IL_007b;
		}
	}
	{
		G_B14_0 = _stringLiteral1296;
		G_B14_1 = G_B12_0;
		goto IL_0080;
	}

IL_007b:
	{
		G_B14_0 = _stringLiteral1297;
		G_B14_1 = G_B13_0;
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m409(NULL /*static, unused*/, G_B14_1, G_B14_0, /*hidden argument*/NULL);
		___fileName = L_20;
	}

IL_0087:
	{
		String_t* L_21 = ___mimeType;
		if (L_21)
		{
			goto IL_00a7;
		}
	}
	{
		bool L_22 = V_0;
		if (!L_22)
		{
			goto IL_00a0;
		}
	}
	{
		___mimeType = _stringLiteral1298;
		goto IL_00a7;
	}

IL_00a0:
	{
		___mimeType = _stringLiteral1299;
	}

IL_00a7:
	{
		List_1_t78 * L_23 = (__this->___fieldNames_1);
		String_t* L_24 = ___fieldName;
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_23, L_24);
		List_1_t78 * L_25 = (__this->___fileNames_2);
		String_t* L_26 = ___fileName;
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_25, L_26);
		List_1_t1411 * L_27 = (__this->___formData_0);
		ByteU5BU5D_t119* L_28 = ___contents;
		NullCheck(L_27);
		VirtActionInvoker1< ByteU5BU5D_t119* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Byte[]>::Add(!0) */, L_27, L_28);
		List_1_t78 * L_29 = (__this->___types_3);
		String_t* L_30 = ___mimeType;
		NullCheck(L_29);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_29, L_30);
		return;
	}
}
// System.Collections.Hashtable UnityEngine.WWWForm::get_headers()
extern TypeInfo* Hashtable_t19_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1300;
extern Il2CppCodeGenString* _stringLiteral1301;
extern Il2CppCodeGenString* _stringLiteral1295;
extern Il2CppCodeGenString* _stringLiteral1302;
extern "C" Hashtable_t19 * WWWForm_get_headers_m2517 (WWWForm_t293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Encoding_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral1300 = il2cpp_codegen_string_literal_from_index(1300);
		_stringLiteral1301 = il2cpp_codegen_string_literal_from_index(1301);
		_stringLiteral1295 = il2cpp_codegen_string_literal_from_index(1295);
		_stringLiteral1302 = il2cpp_codegen_string_literal_from_index(1302);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t19 * V_0 = {0};
	{
		Hashtable_t19 * L_0 = (Hashtable_t19 *)il2cpp_codegen_object_new (Hashtable_t19_il2cpp_TypeInfo_var);
		Hashtable__ctor_m8205(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = (__this->___containsFiles_5);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Hashtable_t19 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
		Encoding_t1442 * L_3 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t119* L_4 = (__this->___boundary_4);
		NullCheck(L_3);
		String_t* L_5 = (String_t*)VirtFuncInvoker1< String_t*, ByteU5BU5D_t119* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2152(NULL /*static, unused*/, _stringLiteral1301, L_5, _stringLiteral1295, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(25 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_2, _stringLiteral1300, L_6);
		goto IL_0050;
	}

IL_0040:
	{
		Hashtable_t19 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(25 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_7, _stringLiteral1300, _stringLiteral1302);
	}

IL_0050:
	{
		Hashtable_t19 * L_8 = V_0;
		return L_8;
	}
}
// System.Byte[] UnityEngine.WWWForm::get_data()
extern TypeInfo* MemoryStream_t200_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* WWWTranscoder_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t260_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1303;
extern Il2CppCodeGenString* _stringLiteral1304;
extern Il2CppCodeGenString* _stringLiteral1305;
extern Il2CppCodeGenString* _stringLiteral1306;
extern Il2CppCodeGenString* _stringLiteral1295;
extern Il2CppCodeGenString* _stringLiteral1307;
extern Il2CppCodeGenString* _stringLiteral1308;
extern Il2CppCodeGenString* _stringLiteral1309;
extern Il2CppCodeGenString* _stringLiteral1310;
extern Il2CppCodeGenString* _stringLiteral98;
extern Il2CppCodeGenString* _stringLiteral1311;
extern "C" ByteU5BU5D_t119* WWWForm_get_data_m7976 (WWWForm_t293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		Encoding_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		WWWTranscoder_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(979);
		StringU5BU5D_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		_stringLiteral1303 = il2cpp_codegen_string_literal_from_index(1303);
		_stringLiteral1304 = il2cpp_codegen_string_literal_from_index(1304);
		_stringLiteral1305 = il2cpp_codegen_string_literal_from_index(1305);
		_stringLiteral1306 = il2cpp_codegen_string_literal_from_index(1306);
		_stringLiteral1295 = il2cpp_codegen_string_literal_from_index(1295);
		_stringLiteral1307 = il2cpp_codegen_string_literal_from_index(1307);
		_stringLiteral1308 = il2cpp_codegen_string_literal_from_index(1308);
		_stringLiteral1309 = il2cpp_codegen_string_literal_from_index(1309);
		_stringLiteral1310 = il2cpp_codegen_string_literal_from_index(1310);
		_stringLiteral98 = il2cpp_codegen_string_literal_from_index(98);
		_stringLiteral1311 = il2cpp_codegen_string_literal_from_index(1311);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t119* V_0 = {0};
	ByteU5BU5D_t119* V_1 = {0};
	ByteU5BU5D_t119* V_2 = {0};
	ByteU5BU5D_t119* V_3 = {0};
	ByteU5BU5D_t119* V_4 = {0};
	ByteU5BU5D_t119* V_5 = {0};
	MemoryStream_t200 * V_6 = {0};
	int32_t V_7 = 0;
	ByteU5BU5D_t119* V_8 = {0};
	String_t* V_9 = {0};
	String_t* V_10 = {0};
	ByteU5BU5D_t119* V_11 = {0};
	String_t* V_12 = {0};
	ByteU5BU5D_t119* V_13 = {0};
	ByteU5BU5D_t119* V_14 = {0};
	ByteU5BU5D_t119* V_15 = {0};
	ByteU5BU5D_t119* V_16 = {0};
	MemoryStream_t200 * V_17 = {0};
	int32_t V_18 = 0;
	ByteU5BU5D_t119* V_19 = {0};
	ByteU5BU5D_t119* V_20 = {0};
	ByteU5BU5D_t119* V_21 = {0};
	ByteU5BU5D_t119* V_22 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (__this->___containsFiles_5);
		if (!L_0)
		{
			goto IL_0311;
		}
	}
	{
		Encoding_t1442 * L_1 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t119* L_2 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, _stringLiteral1303);
		V_0 = L_2;
		Encoding_t1442 * L_3 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		ByteU5BU5D_t119* L_4 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, _stringLiteral1304);
		V_1 = L_4;
		Encoding_t1442 * L_5 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t119* L_6 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, _stringLiteral1305);
		V_2 = L_6;
		Encoding_t1442 * L_7 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		ByteU5BU5D_t119* L_8 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_7, _stringLiteral1306);
		V_3 = L_8;
		Encoding_t1442 * L_9 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		ByteU5BU5D_t119* L_10 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, _stringLiteral1295);
		V_4 = L_10;
		Encoding_t1442 * L_11 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		ByteU5BU5D_t119* L_12 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_11, _stringLiteral1307);
		V_5 = L_12;
		MemoryStream_t200 * L_13 = (MemoryStream_t200 *)il2cpp_codegen_object_new (MemoryStream_t200_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m8206(L_13, ((int32_t)1024), /*hidden argument*/NULL);
		V_6 = L_13;
	}

IL_0079:
	try
	{ // begin try (depth: 1)
		{
			V_7 = 0;
			goto IL_0297;
		}

IL_0081:
		{
			MemoryStream_t200 * L_14 = V_6;
			ByteU5BU5D_t119* L_15 = V_1;
			ByteU5BU5D_t119* L_16 = V_1;
			NullCheck(L_16);
			NullCheck(L_14);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_14, L_15, 0, (((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))));
			MemoryStream_t200 * L_17 = V_6;
			ByteU5BU5D_t119* L_18 = V_0;
			ByteU5BU5D_t119* L_19 = V_0;
			NullCheck(L_19);
			NullCheck(L_17);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_17, L_18, 0, (((int32_t)((int32_t)(((Array_t *)L_19)->max_length)))));
			MemoryStream_t200 * L_20 = V_6;
			ByteU5BU5D_t119* L_21 = (__this->___boundary_4);
			ByteU5BU5D_t119* L_22 = (__this->___boundary_4);
			NullCheck(L_22);
			NullCheck(L_20);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_20, L_21, 0, (((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))));
			MemoryStream_t200 * L_23 = V_6;
			ByteU5BU5D_t119* L_24 = V_1;
			ByteU5BU5D_t119* L_25 = V_1;
			NullCheck(L_25);
			NullCheck(L_23);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_23, L_24, 0, (((int32_t)((int32_t)(((Array_t *)L_25)->max_length)))));
			MemoryStream_t200 * L_26 = V_6;
			ByteU5BU5D_t119* L_27 = V_2;
			ByteU5BU5D_t119* L_28 = V_2;
			NullCheck(L_28);
			NullCheck(L_26);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_26, L_27, 0, (((int32_t)((int32_t)(((Array_t *)L_28)->max_length)))));
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
			Encoding_t1442 * L_29 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t78 * L_30 = (__this->___types_3);
			int32_t L_31 = V_7;
			NullCheck(L_30);
			String_t* L_32 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_30, L_31);
			NullCheck(L_29);
			ByteU5BU5D_t119* L_33 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_29, L_32);
			V_8 = L_33;
			MemoryStream_t200 * L_34 = V_6;
			ByteU5BU5D_t119* L_35 = V_8;
			ByteU5BU5D_t119* L_36 = V_8;
			NullCheck(L_36);
			NullCheck(L_34);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_34, L_35, 0, (((int32_t)((int32_t)(((Array_t *)L_36)->max_length)))));
			MemoryStream_t200 * L_37 = V_6;
			ByteU5BU5D_t119* L_38 = V_1;
			ByteU5BU5D_t119* L_39 = V_1;
			NullCheck(L_39);
			NullCheck(L_37);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_37, L_38, 0, (((int32_t)((int32_t)(((Array_t *)L_39)->max_length)))));
			MemoryStream_t200 * L_40 = V_6;
			ByteU5BU5D_t119* L_41 = V_3;
			ByteU5BU5D_t119* L_42 = V_3;
			NullCheck(L_42);
			NullCheck(L_40);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_40, L_41, 0, (((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))));
			Encoding_t1442 * L_43 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_43);
			String_t* L_44 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(23 /* System.String System.Text.Encoding::get_HeaderName() */, L_43);
			V_9 = L_44;
			List_1_t78 * L_45 = (__this->___fieldNames_1);
			int32_t L_46 = V_7;
			NullCheck(L_45);
			String_t* L_47 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_45, L_46);
			V_10 = L_47;
			String_t* L_48 = V_10;
			Encoding_t1442 * L_49 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			bool L_50 = WWWTranscoder_SevenBitClean_m7983(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
			if (!L_50)
			{
				goto IL_0144;
			}
		}

IL_0132:
		{
			String_t* L_51 = V_10;
			NullCheck(L_51);
			int32_t L_52 = String_IndexOf_m2278(L_51, _stringLiteral1308, /*hidden argument*/NULL);
			if ((((int32_t)L_52) <= ((int32_t)(-1))))
			{
				goto IL_017d;
			}
		}

IL_0144:
		{
			StringU5BU5D_t260* L_53 = ((StringU5BU5D_t260*)SZArrayNew(StringU5BU5D_t260_il2cpp_TypeInfo_var, 5));
			NullCheck(L_53);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
			ArrayElementTypeCheck (L_53, _stringLiteral1308);
			*((String_t**)(String_t**)SZArrayLdElema(L_53, 0, sizeof(String_t*))) = (String_t*)_stringLiteral1308;
			StringU5BU5D_t260* L_54 = L_53;
			String_t* L_55 = V_9;
			NullCheck(L_54);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 1);
			ArrayElementTypeCheck (L_54, L_55);
			*((String_t**)(String_t**)SZArrayLdElema(L_54, 1, sizeof(String_t*))) = (String_t*)L_55;
			StringU5BU5D_t260* L_56 = L_54;
			NullCheck(L_56);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 2);
			ArrayElementTypeCheck (L_56, _stringLiteral1309);
			*((String_t**)(String_t**)SZArrayLdElema(L_56, 2, sizeof(String_t*))) = (String_t*)_stringLiteral1309;
			StringU5BU5D_t260* L_57 = L_56;
			String_t* L_58 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
			Encoding_t1442 * L_59 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			String_t* L_60 = WWWTranscoder_QPEncode_m7980(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
			NullCheck(L_57);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 3);
			ArrayElementTypeCheck (L_57, L_60);
			*((String_t**)(String_t**)SZArrayLdElema(L_57, 3, sizeof(String_t*))) = (String_t*)L_60;
			StringU5BU5D_t260* L_61 = L_57;
			NullCheck(L_61);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 4);
			ArrayElementTypeCheck (L_61, _stringLiteral1310);
			*((String_t**)(String_t**)SZArrayLdElema(L_61, 4, sizeof(String_t*))) = (String_t*)_stringLiteral1310;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_62 = String_Concat_m8207(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
			V_10 = L_62;
		}

IL_017d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
			Encoding_t1442 * L_63 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_64 = V_10;
			NullCheck(L_63);
			ByteU5BU5D_t119* L_65 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_63, L_64);
			V_11 = L_65;
			MemoryStream_t200 * L_66 = V_6;
			ByteU5BU5D_t119* L_67 = V_11;
			ByteU5BU5D_t119* L_68 = V_11;
			NullCheck(L_68);
			NullCheck(L_66);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_66, L_67, 0, (((int32_t)((int32_t)(((Array_t *)L_68)->max_length)))));
			MemoryStream_t200 * L_69 = V_6;
			ByteU5BU5D_t119* L_70 = V_4;
			ByteU5BU5D_t119* L_71 = V_4;
			NullCheck(L_71);
			NullCheck(L_69);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_69, L_70, 0, (((int32_t)((int32_t)(((Array_t *)L_71)->max_length)))));
			List_1_t78 * L_72 = (__this->___fileNames_2);
			int32_t L_73 = V_7;
			NullCheck(L_72);
			String_t* L_74 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_72, L_73);
			if (!L_74)
			{
				goto IL_025c;
			}
		}

IL_01b9:
		{
			List_1_t78 * L_75 = (__this->___fileNames_2);
			int32_t L_76 = V_7;
			NullCheck(L_75);
			String_t* L_77 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_75, L_76);
			V_12 = L_77;
			String_t* L_78 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
			Encoding_t1442 * L_79 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			bool L_80 = WWWTranscoder_SevenBitClean_m7983(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
			if (!L_80)
			{
				goto IL_01eb;
			}
		}

IL_01d9:
		{
			String_t* L_81 = V_12;
			NullCheck(L_81);
			int32_t L_82 = String_IndexOf_m2278(L_81, _stringLiteral1308, /*hidden argument*/NULL);
			if ((((int32_t)L_82) <= ((int32_t)(-1))))
			{
				goto IL_0224;
			}
		}

IL_01eb:
		{
			StringU5BU5D_t260* L_83 = ((StringU5BU5D_t260*)SZArrayNew(StringU5BU5D_t260_il2cpp_TypeInfo_var, 5));
			NullCheck(L_83);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 0);
			ArrayElementTypeCheck (L_83, _stringLiteral1308);
			*((String_t**)(String_t**)SZArrayLdElema(L_83, 0, sizeof(String_t*))) = (String_t*)_stringLiteral1308;
			StringU5BU5D_t260* L_84 = L_83;
			String_t* L_85 = V_9;
			NullCheck(L_84);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_84, 1);
			ArrayElementTypeCheck (L_84, L_85);
			*((String_t**)(String_t**)SZArrayLdElema(L_84, 1, sizeof(String_t*))) = (String_t*)L_85;
			StringU5BU5D_t260* L_86 = L_84;
			NullCheck(L_86);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_86, 2);
			ArrayElementTypeCheck (L_86, _stringLiteral1309);
			*((String_t**)(String_t**)SZArrayLdElema(L_86, 2, sizeof(String_t*))) = (String_t*)_stringLiteral1309;
			StringU5BU5D_t260* L_87 = L_86;
			String_t* L_88 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
			Encoding_t1442 * L_89 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			String_t* L_90 = WWWTranscoder_QPEncode_m7980(NULL /*static, unused*/, L_88, L_89, /*hidden argument*/NULL);
			NullCheck(L_87);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 3);
			ArrayElementTypeCheck (L_87, L_90);
			*((String_t**)(String_t**)SZArrayLdElema(L_87, 3, sizeof(String_t*))) = (String_t*)L_90;
			StringU5BU5D_t260* L_91 = L_87;
			NullCheck(L_91);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 4);
			ArrayElementTypeCheck (L_91, _stringLiteral1310);
			*((String_t**)(String_t**)SZArrayLdElema(L_91, 4, sizeof(String_t*))) = (String_t*)_stringLiteral1310;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_92 = String_Concat_m8207(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
			V_12 = L_92;
		}

IL_0224:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
			Encoding_t1442 * L_93 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_94 = V_12;
			NullCheck(L_93);
			ByteU5BU5D_t119* L_95 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_93, L_94);
			V_13 = L_95;
			MemoryStream_t200 * L_96 = V_6;
			ByteU5BU5D_t119* L_97 = V_5;
			ByteU5BU5D_t119* L_98 = V_5;
			NullCheck(L_98);
			NullCheck(L_96);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_96, L_97, 0, (((int32_t)((int32_t)(((Array_t *)L_98)->max_length)))));
			MemoryStream_t200 * L_99 = V_6;
			ByteU5BU5D_t119* L_100 = V_13;
			ByteU5BU5D_t119* L_101 = V_13;
			NullCheck(L_101);
			NullCheck(L_99);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_99, L_100, 0, (((int32_t)((int32_t)(((Array_t *)L_101)->max_length)))));
			MemoryStream_t200 * L_102 = V_6;
			ByteU5BU5D_t119* L_103 = V_4;
			ByteU5BU5D_t119* L_104 = V_4;
			NullCheck(L_104);
			NullCheck(L_102);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_102, L_103, 0, (((int32_t)((int32_t)(((Array_t *)L_104)->max_length)))));
		}

IL_025c:
		{
			MemoryStream_t200 * L_105 = V_6;
			ByteU5BU5D_t119* L_106 = V_1;
			ByteU5BU5D_t119* L_107 = V_1;
			NullCheck(L_107);
			NullCheck(L_105);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_105, L_106, 0, (((int32_t)((int32_t)(((Array_t *)L_107)->max_length)))));
			MemoryStream_t200 * L_108 = V_6;
			ByteU5BU5D_t119* L_109 = V_1;
			ByteU5BU5D_t119* L_110 = V_1;
			NullCheck(L_110);
			NullCheck(L_108);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_108, L_109, 0, (((int32_t)((int32_t)(((Array_t *)L_110)->max_length)))));
			List_1_t1411 * L_111 = (__this->___formData_0);
			int32_t L_112 = V_7;
			NullCheck(L_111);
			ByteU5BU5D_t119* L_113 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Byte[]>::get_Item(System.Int32) */, L_111, L_112);
			V_14 = L_113;
			MemoryStream_t200 * L_114 = V_6;
			ByteU5BU5D_t119* L_115 = V_14;
			ByteU5BU5D_t119* L_116 = V_14;
			NullCheck(L_116);
			NullCheck(L_114);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_114, L_115, 0, (((int32_t)((int32_t)(((Array_t *)L_116)->max_length)))));
			int32_t L_117 = V_7;
			V_7 = ((int32_t)((int32_t)L_117+(int32_t)1));
		}

IL_0297:
		{
			int32_t L_118 = V_7;
			List_1_t1411 * L_119 = (__this->___formData_0);
			NullCheck(L_119);
			int32_t L_120 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Count() */, L_119);
			if ((((int32_t)L_118) < ((int32_t)L_120)))
			{
				goto IL_0081;
			}
		}

IL_02a9:
		{
			MemoryStream_t200 * L_121 = V_6;
			ByteU5BU5D_t119* L_122 = V_1;
			ByteU5BU5D_t119* L_123 = V_1;
			NullCheck(L_123);
			NullCheck(L_121);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_121, L_122, 0, (((int32_t)((int32_t)(((Array_t *)L_123)->max_length)))));
			MemoryStream_t200 * L_124 = V_6;
			ByteU5BU5D_t119* L_125 = V_0;
			ByteU5BU5D_t119* L_126 = V_0;
			NullCheck(L_126);
			NullCheck(L_124);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_124, L_125, 0, (((int32_t)((int32_t)(((Array_t *)L_126)->max_length)))));
			MemoryStream_t200 * L_127 = V_6;
			ByteU5BU5D_t119* L_128 = (__this->___boundary_4);
			ByteU5BU5D_t119* L_129 = (__this->___boundary_4);
			NullCheck(L_129);
			NullCheck(L_127);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_127, L_128, 0, (((int32_t)((int32_t)(((Array_t *)L_129)->max_length)))));
			MemoryStream_t200 * L_130 = V_6;
			ByteU5BU5D_t119* L_131 = V_0;
			ByteU5BU5D_t119* L_132 = V_0;
			NullCheck(L_132);
			NullCheck(L_130);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_130, L_131, 0, (((int32_t)((int32_t)(((Array_t *)L_132)->max_length)))));
			MemoryStream_t200 * L_133 = V_6;
			ByteU5BU5D_t119* L_134 = V_1;
			ByteU5BU5D_t119* L_135 = V_1;
			NullCheck(L_135);
			NullCheck(L_133);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_133, L_134, 0, (((int32_t)((int32_t)(((Array_t *)L_135)->max_length)))));
			MemoryStream_t200 * L_136 = V_6;
			NullCheck(L_136);
			ByteU5BU5D_t119* L_137 = (ByteU5BU5D_t119*)VirtFuncInvoker0< ByteU5BU5D_t119* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_136);
			V_22 = L_137;
			IL2CPP_LEAVE(0x3F7, FINALLY_0302);
		}

IL_02fd:
		{
			; // IL_02fd: leave IL_0311
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_0302;
	}

FINALLY_0302:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t200 * L_138 = V_6;
			if (!L_138)
			{
				goto IL_0310;
			}
		}

IL_0309:
		{
			MemoryStream_t200 * L_139 = V_6;
			NullCheck(L_139);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_139);
		}

IL_0310:
		{
			IL2CPP_END_FINALLY(770)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(770)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0311:
	{
		Encoding_t1442 * L_140 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_140);
		ByteU5BU5D_t119* L_141 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_140, _stringLiteral98);
		V_15 = L_141;
		Encoding_t1442 * L_142 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_142);
		ByteU5BU5D_t119* L_143 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_142, _stringLiteral1311);
		V_16 = L_143;
		MemoryStream_t200 * L_144 = (MemoryStream_t200 *)il2cpp_codegen_object_new (MemoryStream_t200_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m8206(L_144, ((int32_t)1024), /*hidden argument*/NULL);
		V_17 = L_144;
	}

IL_033f:
	try
	{ // begin try (depth: 1)
		{
			V_18 = 0;
			goto IL_03c3;
		}

IL_0347:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1442_il2cpp_TypeInfo_var);
			Encoding_t1442 * L_145 = Encoding_get_UTF8_m8167(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t78 * L_146 = (__this->___fieldNames_1);
			int32_t L_147 = V_18;
			NullCheck(L_146);
			String_t* L_148 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_146, L_147);
			NullCheck(L_145);
			ByteU5BU5D_t119* L_149 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_145, L_148);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			ByteU5BU5D_t119* L_150 = WWWTranscoder_URLEncode_m7979(NULL /*static, unused*/, L_149, /*hidden argument*/NULL);
			V_19 = L_150;
			List_1_t1411 * L_151 = (__this->___formData_0);
			int32_t L_152 = V_18;
			NullCheck(L_151);
			ByteU5BU5D_t119* L_153 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Byte[]>::get_Item(System.Int32) */, L_151, L_152);
			V_20 = L_153;
			ByteU5BU5D_t119* L_154 = V_20;
			ByteU5BU5D_t119* L_155 = WWWTranscoder_URLEncode_m7979(NULL /*static, unused*/, L_154, /*hidden argument*/NULL);
			V_21 = L_155;
			int32_t L_156 = V_18;
			if ((((int32_t)L_156) <= ((int32_t)0)))
			{
				goto IL_0393;
			}
		}

IL_0385:
		{
			MemoryStream_t200 * L_157 = V_17;
			ByteU5BU5D_t119* L_158 = V_15;
			ByteU5BU5D_t119* L_159 = V_15;
			NullCheck(L_159);
			NullCheck(L_157);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_157, L_158, 0, (((int32_t)((int32_t)(((Array_t *)L_159)->max_length)))));
		}

IL_0393:
		{
			MemoryStream_t200 * L_160 = V_17;
			ByteU5BU5D_t119* L_161 = V_19;
			ByteU5BU5D_t119* L_162 = V_19;
			NullCheck(L_162);
			NullCheck(L_160);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_160, L_161, 0, (((int32_t)((int32_t)(((Array_t *)L_162)->max_length)))));
			MemoryStream_t200 * L_163 = V_17;
			ByteU5BU5D_t119* L_164 = V_16;
			ByteU5BU5D_t119* L_165 = V_16;
			NullCheck(L_165);
			NullCheck(L_163);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_163, L_164, 0, (((int32_t)((int32_t)(((Array_t *)L_165)->max_length)))));
			MemoryStream_t200 * L_166 = V_17;
			ByteU5BU5D_t119* L_167 = V_21;
			ByteU5BU5D_t119* L_168 = V_21;
			NullCheck(L_168);
			NullCheck(L_166);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_166, L_167, 0, (((int32_t)((int32_t)(((Array_t *)L_168)->max_length)))));
			int32_t L_169 = V_18;
			V_18 = ((int32_t)((int32_t)L_169+(int32_t)1));
		}

IL_03c3:
		{
			int32_t L_170 = V_18;
			List_1_t1411 * L_171 = (__this->___formData_0);
			NullCheck(L_171);
			int32_t L_172 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Count() */, L_171);
			if ((((int32_t)L_170) < ((int32_t)L_172)))
			{
				goto IL_0347;
			}
		}

IL_03d5:
		{
			MemoryStream_t200 * L_173 = V_17;
			NullCheck(L_173);
			ByteU5BU5D_t119* L_174 = (ByteU5BU5D_t119*)VirtFuncInvoker0< ByteU5BU5D_t119* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_173);
			V_22 = L_174;
			IL2CPP_LEAVE(0x3F7, FINALLY_03e8);
		}

IL_03e3:
		{
			; // IL_03e3: leave IL_03f7
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_03e8;
	}

FINALLY_03e8:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t200 * L_175 = V_17;
			if (!L_175)
			{
				goto IL_03f6;
			}
		}

IL_03ef:
		{
			MemoryStream_t200 * L_176 = V_17;
			NullCheck(L_176);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_176);
		}

IL_03f6:
		{
			IL2CPP_END_FINALLY(1000)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1000)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_03f7:
	{
		ByteU5BU5D_t119* L_177 = V_22;
		return L_177;
	}
}
// System.Void UnityEngine.WWWTranscoder::.cctor()
extern TypeInfo* WWWTranscoder_t1412_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1312;
extern Il2CppCodeGenString* _stringLiteral1313;
extern Il2CppCodeGenString* _stringLiteral1314;
extern Il2CppCodeGenString* _stringLiteral1315;
extern "C" void WWWTranscoder__cctor_m7977 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(979);
		_stringLiteral1312 = il2cpp_codegen_string_literal_from_index(1312);
		_stringLiteral1313 = il2cpp_codegen_string_literal_from_index(1313);
		_stringLiteral1314 = il2cpp_codegen_string_literal_from_index(1314);
		_stringLiteral1315 = il2cpp_codegen_string_literal_from_index(1315);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t1442 * L_0 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t119* L_1 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, _stringLiteral1312);
		((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___ucHexChars_0 = L_1;
		Encoding_t1442 * L_2 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		ByteU5BU5D_t119* L_3 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, _stringLiteral1313);
		((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___lcHexChars_1 = L_3;
		((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___urlEscapeChar_2 = ((int32_t)37);
		((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___urlSpace_3 = ((int32_t)43);
		Encoding_t1442 * L_4 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		ByteU5BU5D_t119* L_5 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, _stringLiteral1314);
		((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___urlForbidden_4 = L_5;
		((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___qpEscapeChar_5 = ((int32_t)61);
		((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___qpSpace_6 = ((int32_t)95);
		Encoding_t1442 * L_6 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		ByteU5BU5D_t119* L_7 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, _stringLiteral1315);
		((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___qpForbidden_7 = L_7;
		return;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Byte2Hex(System.Byte,System.Byte[])
extern TypeInfo* ByteU5BU5D_t119_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t119* WWWTranscoder_Byte2Hex_m7978 (Object_t * __this /* static, unused */, uint8_t ___b, ByteU5BU5D_t119* ___hexChars, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(749);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t119* V_0 = {0};
	{
		V_0 = ((ByteU5BU5D_t119*)SZArrayNew(ByteU5BU5D_t119_il2cpp_TypeInfo_var, 2));
		ByteU5BU5D_t119* L_0 = V_0;
		ByteU5BU5D_t119* L_1 = ___hexChars;
		uint8_t L_2 = ___b;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)L_2>>(int32_t)4)));
		int32_t L_3 = ((int32_t)((int32_t)L_2>>(int32_t)4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_0, 0, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_1, L_3, sizeof(uint8_t)));
		ByteU5BU5D_t119* L_4 = V_0;
		ByteU5BU5D_t119* L_5 = ___hexChars;
		uint8_t L_6 = ___b;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)L_6&(int32_t)((int32_t)15))));
		int32_t L_7 = ((int32_t)((int32_t)L_6&(int32_t)((int32_t)15)));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_4, 1, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_5, L_7, sizeof(uint8_t)));
		ByteU5BU5D_t119* L_8 = V_0;
		return L_8;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::URLEncode(System.Byte[])
extern TypeInfo* WWWTranscoder_t1412_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t119* WWWTranscoder_URLEncode_m7979 (Object_t * __this /* static, unused */, ByteU5BU5D_t119* ___toEncode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(979);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t119* L_0 = ___toEncode;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
		uint8_t L_1 = ((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___urlEscapeChar_2;
		uint8_t L_2 = ((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___urlSpace_3;
		ByteU5BU5D_t119* L_3 = ((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___urlForbidden_4;
		ByteU5BU5D_t119* L_4 = WWWTranscoder_Encode_m7981(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.WWWTranscoder::QPEncode(System.String,System.Text.Encoding)
extern TypeInfo* WWWTranscoder_t1412_il2cpp_TypeInfo_var;
extern "C" String_t* WWWTranscoder_QPEncode_m7980 (Object_t * __this /* static, unused */, String_t* ___toEncode, Encoding_t1442 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(979);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t119* V_0 = {0};
	{
		Encoding_t1442 * L_0 = ___e;
		String_t* L_1 = ___toEncode;
		NullCheck(L_0);
		ByteU5BU5D_t119* L_2 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
		uint8_t L_3 = ((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___qpEscapeChar_5;
		uint8_t L_4 = ((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___qpSpace_6;
		ByteU5BU5D_t119* L_5 = ((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___qpForbidden_7;
		ByteU5BU5D_t119* L_6 = WWWTranscoder_Encode_m7981(NULL /*static, unused*/, L_2, L_3, L_4, L_5, 1, /*hidden argument*/NULL);
		V_0 = L_6;
		Encoding_t1442 * L_7 = WWW_get_DefaultEncoding_m7112(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t119* L_8 = V_0;
		ByteU5BU5D_t119* L_9 = V_0;
		NullCheck(L_9);
		NullCheck(L_7);
		String_t* L_10 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, (((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))));
		return L_10;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Encode(System.Byte[],System.Byte,System.Byte,System.Byte[],System.Boolean)
extern TypeInfo* MemoryStream_t200_il2cpp_TypeInfo_var;
extern TypeInfo* WWWTranscoder_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t42_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t119* WWWTranscoder_Encode_m7981 (Object_t * __this /* static, unused */, ByteU5BU5D_t119* ___input, uint8_t ___escapeChar, uint8_t ___space, ByteU5BU5D_t119* ___forbidden, bool ___uppercase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		WWWTranscoder_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(979);
		IDisposable_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t200 * V_0 = {0};
	int32_t V_1 = 0;
	ByteU5BU5D_t119* V_2 = {0};
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B9_0 = 0;
	MemoryStream_t200 * G_B9_1 = {0};
	int32_t G_B8_0 = 0;
	MemoryStream_t200 * G_B8_1 = {0};
	ByteU5BU5D_t119* G_B10_0 = {0};
	int32_t G_B10_1 = 0;
	MemoryStream_t200 * G_B10_2 = {0};
	{
		ByteU5BU5D_t119* L_0 = ___input;
		NullCheck(L_0);
		MemoryStream_t200 * L_1 = (MemoryStream_t200 *)il2cpp_codegen_object_new (MemoryStream_t200_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m8206(L_1, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))*(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			V_1 = 0;
			goto IL_0089;
		}

IL_0012:
		{
			ByteU5BU5D_t119* L_2 = ___input;
			int32_t L_3 = V_1;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
			int32_t L_4 = L_3;
			if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_2, L_4, sizeof(uint8_t)))) == ((uint32_t)((int32_t)32)))))
			{
				goto IL_0028;
			}
		}

IL_001c:
		{
			MemoryStream_t200 * L_5 = V_0;
			uint8_t L_6 = ___space;
			NullCheck(L_5);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_5, L_6);
			goto IL_0085;
		}

IL_0028:
		{
			ByteU5BU5D_t119* L_7 = ___input;
			int32_t L_8 = V_1;
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
			int32_t L_9 = L_8;
			if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_9, sizeof(uint8_t)))) < ((int32_t)((int32_t)32))))
			{
				goto IL_004a;
			}
		}

IL_0032:
		{
			ByteU5BU5D_t119* L_10 = ___input;
			int32_t L_11 = V_1;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_10, L_12, sizeof(uint8_t)))) > ((int32_t)((int32_t)126))))
			{
				goto IL_004a;
			}
		}

IL_003c:
		{
			ByteU5BU5D_t119* L_13 = ___forbidden;
			ByteU5BU5D_t119* L_14 = ___input;
			int32_t L_15 = V_1;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
			int32_t L_16 = L_15;
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			bool L_17 = WWWTranscoder_ByteArrayContains_m7982(NULL /*static, unused*/, L_13, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_14, L_16, sizeof(uint8_t))), /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_007c;
			}
		}

IL_004a:
		{
			MemoryStream_t200 * L_18 = V_0;
			uint8_t L_19 = ___escapeChar;
			NullCheck(L_18);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_18, L_19);
			MemoryStream_t200 * L_20 = V_0;
			ByteU5BU5D_t119* L_21 = ___input;
			int32_t L_22 = V_1;
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
			int32_t L_23 = L_22;
			bool L_24 = ___uppercase;
			G_B8_0 = ((int32_t)((*(uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_23, sizeof(uint8_t)))));
			G_B8_1 = L_20;
			if (!L_24)
			{
				G_B9_0 = ((int32_t)((*(uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_23, sizeof(uint8_t)))));
				G_B9_1 = L_20;
				goto IL_0066;
			}
		}

IL_005c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			ByteU5BU5D_t119* L_25 = ((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___ucHexChars_0;
			G_B10_0 = L_25;
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			goto IL_006b;
		}

IL_0066:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			ByteU5BU5D_t119* L_26 = ((WWWTranscoder_t1412_StaticFields*)WWWTranscoder_t1412_il2cpp_TypeInfo_var->static_fields)->___lcHexChars_1;
			G_B10_0 = L_26;
			G_B10_1 = G_B9_0;
			G_B10_2 = G_B9_1;
		}

IL_006b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
			ByteU5BU5D_t119* L_27 = WWWTranscoder_Byte2Hex_m7978(NULL /*static, unused*/, G_B10_1, G_B10_0, /*hidden argument*/NULL);
			NullCheck(G_B10_2);
			VirtActionInvoker3< ByteU5BU5D_t119*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, G_B10_2, L_27, 0, 2);
			goto IL_0085;
		}

IL_007c:
		{
			MemoryStream_t200 * L_28 = V_0;
			ByteU5BU5D_t119* L_29 = ___input;
			int32_t L_30 = V_1;
			NullCheck(L_29);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
			int32_t L_31 = L_30;
			NullCheck(L_28);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_28, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_29, L_31, sizeof(uint8_t))));
		}

IL_0085:
		{
			int32_t L_32 = V_1;
			V_1 = ((int32_t)((int32_t)L_32+(int32_t)1));
		}

IL_0089:
		{
			int32_t L_33 = V_1;
			ByteU5BU5D_t119* L_34 = ___input;
			NullCheck(L_34);
			if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_34)->max_length)))))))
			{
				goto IL_0012;
			}
		}

IL_0092:
		{
			MemoryStream_t200 * L_35 = V_0;
			NullCheck(L_35);
			ByteU5BU5D_t119* L_36 = (ByteU5BU5D_t119*)VirtFuncInvoker0< ByteU5BU5D_t119* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_35);
			V_2 = L_36;
			IL2CPP_LEAVE(0xB0, FINALLY_00a3);
		}

IL_009e:
		{
			; // IL_009e: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_00a3;
	}

FINALLY_00a3:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t200 * L_37 = V_0;
			if (!L_37)
			{
				goto IL_00af;
			}
		}

IL_00a9:
		{
			MemoryStream_t200 * L_38 = V_0;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t42_il2cpp_TypeInfo_var, L_38);
		}

IL_00af:
		{
			IL2CPP_END_FINALLY(163)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(163)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_00b0:
	{
		ByteU5BU5D_t119* L_39 = V_2;
		return L_39;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::ByteArrayContains(System.Byte[],System.Byte)
extern "C" bool WWWTranscoder_ByteArrayContains_m7982 (Object_t * __this /* static, unused */, ByteU5BU5D_t119* ___array, uint8_t ___b, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t119* L_0 = ___array;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		V_1 = 0;
		goto IL_001a;
	}

IL_000b:
	{
		ByteU5BU5D_t119* L_1 = ___array;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		uint8_t L_4 = ___b;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_1, L_3, sizeof(uint8_t)))) == ((uint32_t)L_4))))
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001a:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.String,System.Text.Encoding)
extern TypeInfo* WWWTranscoder_t1412_il2cpp_TypeInfo_var;
extern "C" bool WWWTranscoder_SevenBitClean_m7983 (Object_t * __this /* static, unused */, String_t* ___s, Encoding_t1442 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(979);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t1442 * L_0 = ___e;
		String_t* L_1 = ___s;
		NullCheck(L_0);
		ByteU5BU5D_t119* L_2 = (ByteU5BU5D_t119*)VirtFuncInvoker1< ByteU5BU5D_t119*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1412_il2cpp_TypeInfo_var);
		bool L_3 = WWWTranscoder_SevenBitClean_m7984(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.Byte[])
extern "C" bool WWWTranscoder_SevenBitClean_m7984 (Object_t * __this /* static, unused */, ByteU5BU5D_t119* ___input, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		ByteU5BU5D_t119* L_0 = ___input;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_0, L_2, sizeof(uint8_t)))) < ((int32_t)((int32_t)32))))
		{
			goto IL_001b;
		}
	}
	{
		ByteU5BU5D_t119* L_3 = ___input;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_5, sizeof(uint8_t)))) <= ((int32_t)((int32_t)126))))
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return 0;
	}

IL_001d:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_0;
		ByteU5BU5D_t119* L_8 = ___input;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return 1;
	}
}
// Conversion methods for marshalling of: UnityEngine.CacheIndex
extern "C" void CacheIndex_t1413_marshal(const CacheIndex_t1413& unmarshaled, CacheIndex_t1413_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___bytesUsed_1 = unmarshaled.___bytesUsed_1;
	marshaled.___expires_2 = unmarshaled.___expires_2;
}
extern "C" void CacheIndex_t1413_marshal_back(const CacheIndex_t1413_marshaled& marshaled, CacheIndex_t1413& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___bytesUsed_1 = marshaled.___bytesUsed_1;
	unmarshaled.___expires_2 = marshaled.___expires_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.CacheIndex
extern "C" void CacheIndex_t1413_marshal_cleanup(CacheIndex_t1413_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* UnityString_Format_m7985 (Object_t * __this /* static, unused */, String_t* ___fmt, ObjectU5BU5D_t34* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fmt;
		ObjectU5BU5D_t34* L_1 = ___args;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2511(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m7986 (AsyncOperation_t1340 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m8107(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m7987 (AsyncOperation_t1340 * __this, const MethodInfo* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m7987_ftn) (AsyncOperation_t1340 *);
	static AsyncOperation_InternalDestroy_m7987_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m7987_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m7988 (AsyncOperation_t1340 * __this, const MethodInfo* method)
{
	Exception_t40 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t40 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m7987(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t40 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6527(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t40 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1340_marshal(const AsyncOperation_t1340& unmarshaled, AsyncOperation_t1340_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void AsyncOperation_t1340_marshal_back(const AsyncOperation_t1340_marshaled& marshaled, AsyncOperation_t1340& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1340_marshal_cleanup(AsyncOperation_t1340_marshaled& marshaled)
{
}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C" void LogCallback__ctor_m7989 (LogCallback_t1416 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C" void LogCallback_Invoke_m7990 (LogCallback_t1416 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LogCallback_Invoke_m7990((LogCallback_t1416 *)__this->___prev_9,___condition, ___stackTrace, ___type, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LogCallback_t1416(Il2CppObject* delegate, String_t* ___condition, String_t* ___stackTrace, int32_t ___type)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___condition' to native representation
	char* ____condition_marshaled = { 0 };
	____condition_marshaled = il2cpp_codegen_marshal_string(___condition);

	// Marshaling of parameter '___stackTrace' to native representation
	char* ____stackTrace_marshaled = { 0 };
	____stackTrace_marshaled = il2cpp_codegen_marshal_string(___stackTrace);

	// Marshaling of parameter '___type' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____condition_marshaled, ____stackTrace_marshaled, ___type);

	// Marshaling cleanup of parameter '___condition' native representation
	il2cpp_codegen_marshal_free(____condition_marshaled);
	____condition_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace' native representation
	il2cpp_codegen_marshal_free(____stackTrace_marshaled);
	____stackTrace_marshaled = NULL;

	// Marshaling cleanup of parameter '___type' native representation

}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern TypeInfo* LogType_t1348_il2cpp_TypeInfo_var;
extern "C" Object_t * LogCallback_BeginInvoke_m7991 (LogCallback_t1416 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogType_t1348_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(980);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition;
	__d_args[1] = ___stackTrace;
	__d_args[2] = Box(LogType_t1348_il2cpp_TypeInfo_var, &___type);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C" void LogCallback_EndInvoke_m7992 (LogCallback_t1416 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Application::Quit()
extern "C" void Application_Quit_m2703 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Application_Quit_m2703_ftn) ();
	static Application_Quit_m2703_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Quit_m2703_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Quit()");
	_il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_loadedLevelName()
extern "C" String_t* Application_get_loadedLevelName_m2285 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_loadedLevelName_m2285_ftn) ();
	static Application_get_loadedLevelName_m2285_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_loadedLevelName_m2285_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_loadedLevelName()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::LoadLevel(System.Int32)
extern "C" void Application_LoadLevel_m2591 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		Application_LoadLevelAsync_m7993(NULL /*static, unused*/, (String_t*)NULL, L_0, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C" void Application_LoadLevel_m2341 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Application_LoadLevelAsync_m7993(NULL /*static, unused*/, L_0, (-1), 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C" AsyncOperation_t1340 * Application_LoadLevelAsync_m7993 (Object_t * __this /* static, unused */, String_t* ___monoLevelName, int32_t ___index, bool ___additive, bool ___mustCompleteNextFrame, const MethodInfo* method)
{
	typedef AsyncOperation_t1340 * (*Application_LoadLevelAsync_m7993_ftn) (String_t*, int32_t, bool, bool);
	static Application_LoadLevelAsync_m7993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_LoadLevelAsync_m7993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___monoLevelName, ___index, ___additive, ___mustCompleteNextFrame);
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C" bool Application_get_isPlaying_m4301 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isPlaying_m4301_ftn) ();
	static Application_get_isPlaying_m4301_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m4301_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C" bool Application_get_isEditor_m4303 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isEditor_m4303_ftn) ();
	static Application_get_isEditor_m4303_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m4303_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	return _il2cpp_icall_func();
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" int32_t Application_get_platform_m2652 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_platform_m2652_ftn) ();
	static Application_get_platform_m2652_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m2652_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)
extern "C" void Application_CaptureScreenshot_m7994 (Object_t * __this /* static, unused */, String_t* ___filename, int32_t ___superSize, const MethodInfo* method)
{
	typedef void (*Application_CaptureScreenshot_m7994_ftn) (String_t*, int32_t);
	static Application_CaptureScreenshot_m7994_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_CaptureScreenshot_m7994_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)");
	_il2cpp_icall_func(___filename, ___superSize);
}
// System.Void UnityEngine.Application::CaptureScreenshot(System.String)
extern "C" void Application_CaptureScreenshot_m2653 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___filename;
		int32_t L_1 = V_0;
		Application_CaptureScreenshot_m7994(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C" void Application_set_runInBackground_m6855 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*Application_set_runInBackground_m6855_ftn) (bool);
	static Application_set_runInBackground_m6855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_runInBackground_m6855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_runInBackground(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.Application::get_dataPath()
extern "C" String_t* Application_get_dataPath_m2683 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_dataPath_m2683_ftn) ();
	static Application_get_dataPath_m2683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_dataPath_m2683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_dataPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C" String_t* Application_get_persistentDataPath_m2658 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_persistentDataPath_m2658_ftn) ();
	static Application_get_persistentDataPath_m2658_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_persistentDataPath_m2658_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_persistentDataPath()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::ExternalCall(System.String,System.Object[])
extern "C" void Application_ExternalCall_m2369 (Object_t * __this /* static, unused */, String_t* ___functionName, ObjectU5BU5D_t34* ___args, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Application::ExternalEval(System.String)
extern TypeInfo* Char_t556_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Application_ExternalEval_m2367 (Object_t * __this /* static, unused */, String_t* ___script, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___script;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m410(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_2 = ___script;
		String_t* L_3 = ___script;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m410(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		uint16_t L_5 = String_get_Chars_m2170(L_2, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)59))))
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_6 = ___script;
		uint16_t L_7 = ((int32_t)59);
		Object_t * L_8 = Box(Char_t556_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2203(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		___script = L_9;
	}

IL_0030:
	{
		String_t* L_10 = ___script;
		Application_Internal_ExternalCall_m7995(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::Internal_ExternalCall(System.String)
extern "C" void Application_Internal_ExternalCall_m7995 (Object_t * __this /* static, unused */, String_t* ___script, const MethodInfo* method)
{
	typedef void (*Application_Internal_ExternalCall_m7995_ftn) (String_t*);
	static Application_Internal_ExternalCall_m7995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Internal_ExternalCall_m7995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Internal_ExternalCall(System.String)");
	_il2cpp_icall_func(___script);
}
// System.String UnityEngine.Application::get_unityVersion()
extern "C" String_t* Application_get_unityVersion_m6598 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_unityVersion_m6598_ftn) ();
	static Application_get_unityVersion_m6598_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_unityVersion_m6598_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_unityVersion()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C" void Application_OpenURL_m2496 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	typedef void (*Application_OpenURL_m2496_ftn) (String_t*);
	static Application_OpenURL_m2496_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_OpenURL_m2496_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::OpenURL(System.String)");
	_il2cpp_icall_func(___url);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType)
extern TypeInfo* Application_t1417_il2cpp_TypeInfo_var;
extern "C" void Application_CallLogCallback_m7996 (Object_t * __this /* static, unused */, String_t* ___logString, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Application_t1417_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(981);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogCallback_t1416 * L_0 = ((Application_t1417_StaticFields*)Application_t1417_il2cpp_TypeInfo_var->static_fields)->___s_LogCallback_0;
		il2cpp_codegen_memory_barrier();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t1416 * L_1 = ((Application_t1417_StaticFields*)Application_t1417_il2cpp_TypeInfo_var->static_fields)->___s_LogCallback_0;
		il2cpp_codegen_memory_barrier();
		String_t* L_2 = ___logString;
		String_t* L_3 = ___stackTrace;
		int32_t L_4 = ___type;
		NullCheck(L_1);
		LogCallback_Invoke_m7990(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
extern "C" int32_t Application_get_internetReachability_m2196 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_internetReachability_m2196_ftn) ();
	static Application_get_internetReachability_m2196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_internetReachability_m2196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_internetReachability()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::ReplyToUserAuthorizationRequest(System.Boolean,System.Boolean)
extern "C" void Application_ReplyToUserAuthorizationRequest_m7997 (Object_t * __this /* static, unused */, bool ___reply, bool ___remember, const MethodInfo* method)
{
	typedef void (*Application_ReplyToUserAuthorizationRequest_m7997_ftn) (bool, bool);
	static Application_ReplyToUserAuthorizationRequest_m7997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_ReplyToUserAuthorizationRequest_m7997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::ReplyToUserAuthorizationRequest(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(___reply, ___remember);
}
// System.Void UnityEngine.Application::ReplyToUserAuthorizationRequest(System.Boolean)
extern "C" void Application_ReplyToUserAuthorizationRequest_m7998 (Object_t * __this /* static, unused */, bool ___reply, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		bool L_0 = ___reply;
		bool L_1 = V_0;
		Application_ReplyToUserAuthorizationRequest_m7997(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Application::GetUserAuthorizationRequestMode_Internal()
extern "C" int32_t Application_GetUserAuthorizationRequestMode_Internal_m7999 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_GetUserAuthorizationRequestMode_Internal_m7999_ftn) ();
	static Application_GetUserAuthorizationRequestMode_Internal_m7999_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_GetUserAuthorizationRequestMode_Internal_m7999_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::GetUserAuthorizationRequestMode_Internal()");
	return _il2cpp_icall_func();
}
// UnityEngine.UserAuthorization UnityEngine.Application::GetUserAuthorizationRequestMode()
extern "C" int32_t Application_GetUserAuthorizationRequestMode_m8000 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_GetUserAuthorizationRequestMode_Internal_m7999(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (int32_t)(L_0);
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C" bool Behaviour_get_enabled_m2789 (Behaviour_t875 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_enabled_m2789_ftn) (Behaviour_t875 *);
	static Behaviour_get_enabled_m2789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m2789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" void Behaviour_set_enabled_m408 (Behaviour_t875 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Behaviour_set_enabled_m408_ftn) (Behaviour_t875 *, bool);
	static Behaviour_set_enabled_m408_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m408_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C" bool Behaviour_get_isActiveAndEnabled_m4030 (Behaviour_t875 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m4030_ftn) (Behaviour_t875 *);
	static Behaviour_get_isActiveAndEnabled_m4030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m4030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C" float Camera_get_fieldOfView_m6460 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_fieldOfView_m6460_ftn) (Camera_t510 *);
	static Camera_get_fieldOfView_m6460_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_fieldOfView_m6460_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_fieldOfView()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C" float Camera_get_nearClipPlane_m4084 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m4084_ftn) (Camera_t510 *);
	static Camera_get_nearClipPlane_m4084_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m4084_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern "C" void Camera_set_nearClipPlane_m6772 (Camera_t510 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_nearClipPlane_m6772_ftn) (Camera_t510 *, float);
	static Camera_set_nearClipPlane_m6772_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_nearClipPlane_m6772_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_nearClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C" float Camera_get_farClipPlane_m4083 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m4083_ftn) (Camera_t510 *);
	static Camera_get_farClipPlane_m4083_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m4083_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern "C" void Camera_set_farClipPlane_m6773 (Camera_t510 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_farClipPlane_m6773_ftn) (Camera_t510 *, float);
	static Camera_set_farClipPlane_m6773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_farClipPlane_m6773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_farClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C" void Camera_set_orthographicSize_m6770 (Camera_t510 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographicSize_m6770_ftn) (Camera_t510 *, float);
	static Camera_set_orthographicSize_m6770_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographicSize_m6770_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographicSize(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C" void Camera_set_orthographic_m6769 (Camera_t510 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographic_m6769_ftn) (Camera_t510 *, bool);
	static Camera_set_orthographic_m6769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographic_m6769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Camera::get_depth()
extern "C" float Camera_get_depth_m3998 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_depth_m3998_ftn) (Camera_t510 *);
	static Camera_get_depth_m3998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m3998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_aspect(System.Single)
extern "C" void Camera_set_aspect_m6771 (Camera_t510 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_aspect_m6771_ftn) (Camera_t510 *, float);
	static Camera_set_aspect_m6771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_aspect_m6771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_aspect(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C" int32_t Camera_get_cullingMask_m4096 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m4096_ftn) (Camera_t510 *);
	static Camera_get_cullingMask_m4096_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m4096_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C" void Camera_set_cullingMask_m6774 (Camera_t510 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_cullingMask_m6774_ftn) (Camera_t510 *, int32_t);
	static Camera_set_cullingMask_m6774_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_cullingMask_m6774_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_cullingMask(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C" int32_t Camera_get_eventMask_m8001 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m8001_ftn) (Camera_t510 *);
	static Camera_get_eventMask_m8001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m8001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Camera::get_rect()
extern "C" Rect_t30  Camera_get_rect_m6818 (Camera_t510 * __this, const MethodInfo* method)
{
	Rect_t30  V_0 = {0};
	{
		Camera_INTERNAL_get_rect_m8002(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t30  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
extern "C" void Camera_set_rect_m6820 (Camera_t510 * __this, Rect_t30  ___value, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_rect_m8003(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_get_rect_m8002 (Camera_t510 * __this, Rect_t30 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_rect_m8002_ftn) (Camera_t510 *, Rect_t30 *);
	static Camera_INTERNAL_get_rect_m8002_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_rect_m8002_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_set_rect_m8003 (Camera_t510 * __this, Rect_t30 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_rect_m8003_ftn) (Camera_t510 *, Rect_t30 *);
	static Camera_INTERNAL_set_rect_m8003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_rect_m8003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C" Rect_t30  Camera_get_pixelRect_m6444 (Camera_t510 * __this, const MethodInfo* method)
{
	Rect_t30  V_0 = {0};
	{
		Camera_INTERNAL_get_pixelRect_m8004(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t30  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_get_pixelRect_m8004 (Camera_t510 * __this, Rect_t30 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m8004_ftn) (Camera_t510 *, Rect_t30 *);
	static Camera_INTERNAL_get_pixelRect_m8004_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m8004_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C" RenderTexture_t582 * Camera_get_targetTexture_m8005 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef RenderTexture_t582 * (*Camera_get_targetTexture_m8005_ftn) (Camera_t510 *);
	static Camera_get_targetTexture_m8005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m8005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern "C" void Camera_set_targetTexture_m2687 (Camera_t510 * __this, RenderTexture_t582 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_targetTexture_m2687_ftn) (Camera_t510 *, RenderTexture_t582 *);
	static Camera_set_targetTexture_m2687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_targetTexture_m2687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C" Matrix4x4_t603  Camera_get_projectionMatrix_m6445 (Camera_t510 * __this, const MethodInfo* method)
{
	Matrix4x4_t603  V_0 = {0};
	{
		Camera_INTERNAL_get_projectionMatrix_m8006(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t603  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C" void Camera_set_projectionMatrix_m6458 (Camera_t510 * __this, Matrix4x4_t603  ___value, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_projectionMatrix_m8007(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C" void Camera_INTERNAL_get_projectionMatrix_m8006 (Camera_t510 * __this, Matrix4x4_t603 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_projectionMatrix_m8006_ftn) (Camera_t510 *, Matrix4x4_t603 *);
	static Camera_INTERNAL_get_projectionMatrix_m8006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_projectionMatrix_m8006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C" void Camera_INTERNAL_set_projectionMatrix_m8007 (Camera_t510 * __this, Matrix4x4_t603 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_projectionMatrix_m8007_ftn) (Camera_t510 *, Matrix4x4_t603 *);
	static Camera_INTERNAL_set_projectionMatrix_m8007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_projectionMatrix_m8007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C" int32_t Camera_get_clearFlags_m8008 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m8008_ftn) (Camera_t510 *);
	static Camera_get_clearFlags_m8008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m8008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C" Vector3_t6  Camera_WorldToScreenPoint_m6501 (Camera_t510 * __this, Vector3_t6  ___position, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Camera_INTERNAL_CALL_WorldToScreenPoint_m8009(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t6  Camera_INTERNAL_CALL_WorldToScreenPoint_m8009 (Object_t * __this /* static, unused */, Camera_t510 * ___self, Vector3_t6 * ___position, const MethodInfo* method)
{
	typedef Vector3_t6  (*Camera_INTERNAL_CALL_WorldToScreenPoint_m8009_ftn) (Camera_t510 *, Vector3_t6 *);
	static Camera_INTERNAL_CALL_WorldToScreenPoint_m8009_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_WorldToScreenPoint_m8009_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C" Vector3_t6  Camera_ScreenToWorldPoint_m2317 (Camera_t510 * __this, Vector3_t6  ___position, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Camera_INTERNAL_CALL_ScreenToWorldPoint_m8010(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t6  Camera_INTERNAL_CALL_ScreenToWorldPoint_m8010 (Object_t * __this /* static, unused */, Camera_t510 * ___self, Vector3_t6 * ___position, const MethodInfo* method)
{
	typedef Vector3_t6  (*Camera_INTERNAL_CALL_ScreenToWorldPoint_m8010_ftn) (Camera_t510 *, Vector3_t6 *);
	static Camera_INTERNAL_CALL_ScreenToWorldPoint_m8010_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToWorldPoint_m8010_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C" Vector3_t6  Camera_ScreenToViewportPoint_m4163 (Camera_t510 * __this, Vector3_t6  ___position, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Camera_INTERNAL_CALL_ScreenToViewportPoint_m8011(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t6  Camera_INTERNAL_CALL_ScreenToViewportPoint_m8011 (Object_t * __this /* static, unused */, Camera_t510 * ___self, Vector3_t6 * ___position, const MethodInfo* method)
{
	typedef Vector3_t6  (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m8011_ftn) (Camera_t510 *, Vector3_t6 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m8011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m8011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C" Ray_t567  Camera_ScreenPointToRay_m2544 (Camera_t510 * __this, Vector3_t6  ___position, const MethodInfo* method)
{
	{
		Ray_t567  L_0 = Camera_INTERNAL_CALL_ScreenPointToRay_m8012(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Ray UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Ray_t567  Camera_INTERNAL_CALL_ScreenPointToRay_m8012 (Object_t * __this /* static, unused */, Camera_t510 * ___self, Vector3_t6 * ___position, const MethodInfo* method)
{
	typedef Ray_t567  (*Camera_INTERNAL_CALL_ScreenPointToRay_m8012_ftn) (Camera_t510 *, Vector3_t6 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m8012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m8012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" Camera_t510 * Camera_get_main_m2315 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t510 * (*Camera_get_main_m2315_ftn) ();
	static Camera_get_main_m2315_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m2315_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	return _il2cpp_icall_func();
}
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C" Camera_t510 * Camera_get_current_m2777 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t510 * (*Camera_get_current_m2777_ftn) ();
	static Camera_get_current_m2777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_current_m2777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_current()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C" int32_t Camera_get_allCamerasCount_m8013 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m8013_ftn) ();
	static Camera_get_allCamerasCount_m8013_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m8013_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C" int32_t Camera_GetAllCameras_m8014 (Object_t * __this /* static, unused */, CameraU5BU5D_t600* ___cameras, const MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m8014_ftn) (CameraU5BU5D_t600*);
	static Camera_GetAllCameras_m8014_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m8014_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras);
}
// System.Void UnityEngine.Camera::Render()
extern "C" void Camera_Render_m2688 (Camera_t510 * __this, const MethodInfo* method)
{
	typedef void (*Camera_Render_m2688_ftn) (Camera_t510 *);
	static Camera_Render_m2688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_Render_m2688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::Render()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)
extern "C" void Debug_Internal_Log_m8015 (Object_t * __this /* static, unused */, int32_t ___level, String_t* ___msg, Object_t53 * ___obj, const MethodInfo* method)
{
	typedef void (*Debug_Internal_Log_m8015_ftn) (int32_t, String_t*, Object_t53 *);
	static Debug_Internal_Log_m8015_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_Log_m8015_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level, ___msg, ___obj);
}
// System.Void UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C" void Debug_Internal_LogException_m8016 (Object_t * __this /* static, unused */, Exception_t40 * ___exception, Object_t53 * ___obj, const MethodInfo* method)
{
	typedef void (*Debug_Internal_LogException_m8016_ftn) (Exception_t40 *, Object_t53 *);
	static Debug_Internal_LogException_m8016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_LogException_m8016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception, ___obj);
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern Il2CppCodeGenString* _stringLiteral1316;
extern "C" void Debug_Log_m2193 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1316 = il2cpp_codegen_string_literal_from_index(1316);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 0;
		if (!L_0)
		{
			G_B2_0 = 0;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral1316;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m8015(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern Il2CppCodeGenString* _stringLiteral1316;
extern "C" void Debug_LogError_m302 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1316 = il2cpp_codegen_string_literal_from_index(1316);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 2;
		if (!L_0)
		{
			G_B2_0 = 2;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral1316;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m8015(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C" void Debug_LogError_m4211 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t53 * ___context, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t53 * L_2 = ___context;
		Debug_Internal_Log_m8015(NULL /*static, unused*/, 2, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C" void Debug_LogException_m8017 (Object_t * __this /* static, unused */, Exception_t40 * ___exception, const MethodInfo* method)
{
	{
		Exception_t40 * L_0 = ___exception;
		Debug_Internal_LogException_m8016(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern "C" void Debug_LogException_m4117 (Object_t * __this /* static, unused */, Exception_t40 * ___exception, Object_t53 * ___context, const MethodInfo* method)
{
	{
		Exception_t40 * L_0 = ___exception;
		Object_t53 * L_1 = ___context;
		Debug_Internal_LogException_m8016(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" void Debug_LogWarning_m430 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Debug_Internal_Log_m8015(NULL /*static, unused*/, 1, L_1, (Object_t53 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C" void Debug_LogWarning_m4362 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t53 * ___context, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t53 * L_2 = ___context;
		Debug_Internal_Log_m8015(NULL /*static, unused*/, 1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
extern "C" bool Debug_get_isDebugBuild_m2522 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Debug_get_isDebugBuild_m2522_ftn) ();
	static Debug_get_isDebugBuild_m2522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_get_isDebugBuild_m2522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::get_isDebugBuild()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void DisplaysUpdatedDelegate__ctor_m8018 (DisplaysUpdatedDelegate_t1420 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C" void DisplaysUpdatedDelegate_Invoke_m8019 (DisplaysUpdatedDelegate_t1420 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m8019((DisplaysUpdatedDelegate_t1420 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t1420(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DisplaysUpdatedDelegate_BeginInvoke_m8020 (DisplaysUpdatedDelegate_t1420 * __this, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m8021 (DisplaysUpdatedDelegate_t1420 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Display::.ctor()
extern "C" void Display__ctor_m8022 (Display_t1421 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = {0};
		IntPtr__ctor_m6666(&L_0, 0, /*hidden argument*/NULL);
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C" void Display__ctor_m8023 (Display_t1421 * __this, IntPtr_t ___nativeDisplay, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay;
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern TypeInfo* DisplayU5BU5D_t1422_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" void Display__cctor_m8024 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t1422_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisplayU5BU5D_t1422* L_0 = ((DisplayU5BU5D_t1422*)SZArrayNew(DisplayU5BU5D_t1422_il2cpp_TypeInfo_var, 1));
		Display_t1421 * L_1 = (Display_t1421 *)il2cpp_codegen_object_new (Display_t1421_il2cpp_TypeInfo_var);
		Display__ctor_m8022(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Display_t1421 **)(Display_t1421 **)SZArrayLdElema(L_0, 0, sizeof(Display_t1421 *))) = (Display_t1421 *)L_1;
		((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___displays_1 = L_0;
		DisplayU5BU5D_t1422* L_2 = ((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t1421 **)(Display_t1421 **)SZArrayLdElema(L_2, L_3, sizeof(Display_t1421 *)));
		((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = (DisplaysUpdatedDelegate_t1420 *)NULL;
		return;
	}
}
// System.Void UnityEngine.Display::add_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t1420_il2cpp_TypeInfo_var;
extern "C" void Display_add_onDisplaysUpdated_m8025 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t1420 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		DisplaysUpdatedDelegate_t1420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1420 * L_0 = ((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t1420 * L_1 = ___value;
		Delegate_t506 * L_2 = Delegate_Combine_m2300(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t1420 *)CastclassSealed(L_2, DisplaysUpdatedDelegate_t1420_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Display::remove_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t1420_il2cpp_TypeInfo_var;
extern "C" void Display_remove_onDisplaysUpdated_m8026 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t1420 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		DisplaysUpdatedDelegate_t1420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1420 * L_0 = ((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t1420 * L_1 = ___value;
		Delegate_t506 * L_2 = Delegate_Remove_m2302(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t1420 *)CastclassSealed(L_2, DisplaysUpdatedDelegate_t1420_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingWidth_m8027 (Display_t1421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m8039(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingHeight_m8028 (Display_t1421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m8039(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemWidth_m8029 (Display_t1421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m8038(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemHeight_m8030 (Display_t1421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m8038(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_colorBuffer()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t1363  Display_get_colorBuffer_m8031 (Display_t1421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t1363  V_0 = {0};
	RenderBuffer_t1363  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m8040(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t1363  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_depthBuffer()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t1363  Display_get_depthBuffer_m8032 (Display_t1421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t1363  V_0 = {0};
	RenderBuffer_t1363  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m8040(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t1363  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Display::Activate()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m8033 (Display_t1421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m8042(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetRenderingResolution(System.Int32,System.Int32)
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" void Display_SetRenderingResolution_m8034 (Display_t1421 * __this, int32_t ___w, int32_t ___h, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___w;
		int32_t L_2 = ___h;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_SetRenderingResolutionImpl_m8041(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Display UnityEngine.Display::get_main()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" Display_t1421 * Display_get_main_m8035 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		Display_t1421 * L_0 = ((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2;
		return L_0;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern TypeInfo* DisplayU5BU5D_t1422_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" void Display_RecreateDisplayList_m8036 (Object_t * __this /* static, unused */, IntPtrU5BU5D_t1447* ___nativeDisplay, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t1422_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t1447* L_0 = ___nativeDisplay;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___displays_1 = ((DisplayU5BU5D_t1422*)SZArrayNew(DisplayU5BU5D_t1422_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_0 = 0;
		goto IL_0027;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t1422* L_1 = ((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t1447* L_3 = ___nativeDisplay;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Display_t1421 * L_6 = (Display_t1421 *)il2cpp_codegen_object_new (Display_t1421_il2cpp_TypeInfo_var);
		Display__ctor_m8023(L_6, (*(IntPtr_t*)(IntPtr_t*)SZArrayLdElema(L_3, L_5, sizeof(IntPtr_t))), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_6);
		*((Display_t1421 **)(Display_t1421 **)SZArrayLdElema(L_1, L_2, sizeof(Display_t1421 *))) = (Display_t1421 *)L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_8 = V_0;
		IntPtrU5BU5D_t1447* L_9 = ___nativeDisplay;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t1422* L_10 = ((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t1421 **)(Display_t1421 **)SZArrayLdElema(L_10, L_11, sizeof(Display_t1421 *)));
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern TypeInfo* Display_t1421_il2cpp_TypeInfo_var;
extern "C" void Display_FireDisplaysUpdated_m8037 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1420 * L_0 = ((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1421_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1420 * L_1 = ((Display_t1421_StaticFields*)Display_t1421_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m8019(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetSystemExtImpl_m8038 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, const MethodInfo* method)
{
	typedef void (*Display_GetSystemExtImpl_m8038_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m8038_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m8038_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetRenderingExtImpl_m8039 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingExtImpl_m8039_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m8039_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m8039_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
extern "C" void Display_GetRenderingBuffersImpl_m8040 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, RenderBuffer_t1363 * ___color, RenderBuffer_t1363 * ___depth, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingBuffersImpl_m8040_ftn) (IntPtr_t, RenderBuffer_t1363 *, RenderBuffer_t1363 *);
	static Display_GetRenderingBuffersImpl_m8040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingBuffersImpl_m8040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(___nativeDisplay, ___color, ___depth);
}
// System.Void UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)
extern "C" void Display_SetRenderingResolutionImpl_m8041 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___w, int32_t ___h, const MethodInfo* method)
{
	typedef void (*Display_SetRenderingResolutionImpl_m8041_ftn) (IntPtr_t, int32_t, int32_t);
	static Display_SetRenderingResolutionImpl_m8041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetRenderingResolutionImpl_m8041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::ActivateDisplayImpl(System.IntPtr)
extern "C" void Display_ActivateDisplayImpl_m8042 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, const MethodInfo* method)
{
	typedef void (*Display_ActivateDisplayImpl_m8042_ftn) (IntPtr_t);
	static Display_ActivateDisplayImpl_m8042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_ActivateDisplayImpl_m8042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::ActivateDisplayImpl(System.IntPtr)");
	_il2cpp_icall_func(___nativeDisplay);
}
// System.Void UnityEngine.NotConvertedAttribute::.ctor()
extern "C" void NotConvertedAttribute__ctor_m8043 (NotConvertedAttribute_t1423 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NotRenamedAttribute::.ctor()
extern "C" void NotRenamedAttribute__ctor_m8044 (NotRenamedAttribute_t1424 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" void MonoBehaviour__ctor_m298 (MonoBehaviour_t18 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour__ctor_m298_ftn) (MonoBehaviour_t18 *);
	static MonoBehaviour__ctor_m298_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour__ctor_m298_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::.ctor()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C" void MonoBehaviour_Invoke_m2231 (MonoBehaviour_t18 * __this, String_t* ___methodName, float ___time, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m2231_ftn) (MonoBehaviour_t18 *, String_t*, float);
	static MonoBehaviour_Invoke_m2231_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m2231_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName, ___time);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" Coroutine_t39 * MonoBehaviour_StartCoroutine_m2199 (MonoBehaviour_t18 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		Coroutine_t39 * L_1 = MonoBehaviour_StartCoroutine_Auto_m8045(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C" Coroutine_t39 * MonoBehaviour_StartCoroutine_Auto_m8045 (MonoBehaviour_t18 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef Coroutine_t39 * (*MonoBehaviour_StartCoroutine_Auto_m8045_ftn) (MonoBehaviour_t18 *, Object_t *);
	static MonoBehaviour_StartCoroutine_Auto_m8045_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m8045_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C" Coroutine_t39 * MonoBehaviour_StartCoroutine_m8046 (MonoBehaviour_t18 * __this, String_t* ___methodName, Object_t * ___value, const MethodInfo* method)
{
	typedef Coroutine_t39 * (*MonoBehaviour_StartCoroutine_m8046_ftn) (MonoBehaviour_t18 *, String_t*, Object_t *);
	static MonoBehaviour_StartCoroutine_m8046_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m8046_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName, ___value);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C" Coroutine_t39 * MonoBehaviour_StartCoroutine_m297 (MonoBehaviour_t18 * __this, String_t* ___methodName, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = V_0;
		Coroutine_t39 * L_2 = MonoBehaviour_StartCoroutine_m8046(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C" void MonoBehaviour_StopCoroutine_m414 (MonoBehaviour_t18 * __this, String_t* ___methodName, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m414_ftn) (MonoBehaviour_t18 *, String_t*);
	static MonoBehaviour_StopCoroutine_m414_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m414_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutine_m2640 (MonoBehaviour_t18 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m8047(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_m4306 (MonoBehaviour_t18 * __this, Coroutine_t39 * ___routine, const MethodInfo* method)
{
	{
		Coroutine_t39 * L_0 = ___routine;
		MonoBehaviour_StopCoroutine_Auto_m8048(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m8047 (MonoBehaviour_t18 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m8047_ftn) (MonoBehaviour_t18 *, Object_t *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m8047_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m8047_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_Auto_m8048 (MonoBehaviour_t18 * __this, Coroutine_t39 * ___routine, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m8048_ftn) (MonoBehaviour_t18 *, Coroutine_t39 *);
	static MonoBehaviour_StopCoroutine_Auto_m8048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m8048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C" void MonoBehaviour_print_m2635 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		Debug_Log_m2193(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C" int32_t Touch_get_fingerId_m4039 (Touch_t818 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FingerId_0);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t29  Touch_get_position_m4041 (Touch_t818 * __this, const MethodInfo* method)
{
	{
		Vector2_t29  L_0 = (__this->___m_Position_1);
		return L_0;
	}
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m4040 (Touch_t818 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Phase_6);
		return L_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Touch
extern "C" void Touch_t818_marshal(const Touch_t818& unmarshaled, Touch_t818_marshaled& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.___m_FingerId_0;
	marshaled.___m_Position_1 = unmarshaled.___m_Position_1;
	marshaled.___m_RawPosition_2 = unmarshaled.___m_RawPosition_2;
	marshaled.___m_PositionDelta_3 = unmarshaled.___m_PositionDelta_3;
	marshaled.___m_TimeDelta_4 = unmarshaled.___m_TimeDelta_4;
	marshaled.___m_TapCount_5 = unmarshaled.___m_TapCount_5;
	marshaled.___m_Phase_6 = unmarshaled.___m_Phase_6;
}
extern "C" void Touch_t818_marshal_back(const Touch_t818_marshaled& marshaled, Touch_t818& unmarshaled)
{
	unmarshaled.___m_FingerId_0 = marshaled.___m_FingerId_0;
	unmarshaled.___m_Position_1 = marshaled.___m_Position_1;
	unmarshaled.___m_RawPosition_2 = marshaled.___m_RawPosition_2;
	unmarshaled.___m_PositionDelta_3 = marshaled.___m_PositionDelta_3;
	unmarshaled.___m_TimeDelta_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.___m_TapCount_5 = marshaled.___m_TapCount_5;
	unmarshaled.___m_Phase_6 = marshaled.___m_Phase_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
extern "C" void Touch_t818_marshal_cleanup(Touch_t818_marshaled& marshaled)
{
}
// System.Void UnityEngine.Input::.cctor()
extern "C" void Input__cctor_m8049 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
extern "C" bool Input_GetKeyDownInt_m8050 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	typedef bool (*Input_GetKeyDownInt_m8050_ftn) (int32_t);
	static Input_GetKeyDownInt_m8050_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyDownInt_m8050_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyDownInt(System.Int32)");
	return _il2cpp_icall_func(___key);
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C" float Input_GetAxisRaw_m4061 (Object_t * __this /* static, unused */, String_t* ___axisName, const MethodInfo* method)
{
	typedef float (*Input_GetAxisRaw_m4061_ftn) (String_t*);
	static Input_GetAxisRaw_m4061_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m4061_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	return _il2cpp_icall_func(___axisName);
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C" bool Input_GetButtonDown_m4060 (Object_t * __this /* static, unused */, String_t* ___buttonName, const MethodInfo* method)
{
	typedef bool (*Input_GetButtonDown_m4060_ftn) (String_t*);
	static Input_GetButtonDown_m4060_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m4060_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	return _il2cpp_icall_func(___buttonName);
}
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern "C" bool Input_GetKeyDown_m2557 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDownInt_m8050(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C" bool Input_GetMouseButton_m2612 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButton_m2612_ftn) (int32_t);
	static Input_GetMouseButton_m2612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m2612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C" bool Input_GetMouseButtonDown_m2314 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonDown_m2314_ftn) (int32_t);
	static Input_GetMouseButtonDown_m2314_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m2314_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C" bool Input_GetMouseButtonUp_m4042 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonUp_m4042_ftn) (int32_t);
	static Input_GetMouseButtonUp_m4042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m4042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern "C" Vector3_t6  Input_get_mousePosition_m2316 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m8051(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C" void Input_INTERNAL_get_mousePosition_m8051 (Object_t * __this /* static, unused */, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m8051_ftn) (Vector3_t6 *);
	static Input_INTERNAL_get_mousePosition_m8051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m8051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mouseScrollDelta()
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern "C" Vector3_t6  Input_get_mouseScrollDelta_m4044 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mouseScrollDelta_m8052(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector3&)
extern "C" void Input_INTERNAL_get_mouseScrollDelta_m8052 (Object_t * __this /* static, unused */, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mouseScrollDelta_m8052_ftn) (Vector3_t6 *);
	static Input_INTERNAL_get_mouseScrollDelta_m8052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mouseScrollDelta_m8052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Input::get_mousePresent()
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern "C" bool Input_get_mousePresent_m4059 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		bool L_0 = Input_get_touchSupported_m4077(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" Touch_t818  Input_GetTouch_m4078 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method)
{
	typedef Touch_t818  (*Input_GetTouch_m4078_ftn) (int32_t);
	static Input_GetTouch_m4078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetTouch_m4078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetTouch(System.Int32)");
	return _il2cpp_icall_func(___index);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" int32_t Input_get_touchCount_m4079 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Input_get_touchCount_m4079_ftn) ();
	static Input_get_touchCount_m4079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m4079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Input::get_touchSupported()
extern "C" bool Input_get_touchSupported_m4077 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern "C" void Input_set_imeCompositionMode_m4300 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Input_set_imeCompositionMode_m4300_ftn) (int32_t);
	static Input_set_imeCompositionMode_m4300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_set_imeCompositionMode_m4300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.Input::get_compositionString()
extern "C" String_t* Input_get_compositionString_m4235 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Input_get_compositionString_m4235_ftn) ();
	static Input_get_compositionString_m4235_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m4235_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern TypeInfo* Input_t509_il2cpp_TypeInfo_var;
extern "C" void Input_set_compositionCursorPos_m4290 (Object_t * __this /* static, unused */, Vector2_t29  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t509_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m8053(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_set_compositionCursorPos_m8053 (Object_t * __this /* static, unused */, Vector2_t29 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m8053_ftn) (Vector2_t29 *);
	static Input_INTERNAL_set_compositionCursorPos_m8053_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m8053_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Object::.ctor()
extern "C" void Object__ctor_m8054 (Object_t53 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern TypeInfo* Object_t53_il2cpp_TypeInfo_var;
extern "C" bool Object_Equals_m8055 (Object_t53 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = Object_CompareBaseObjects_m8057(NULL /*static, unused*/, __this, ((Object_t53 *)IsInstClass(L_0, Object_t53_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C" int32_t Object_GetHashCode_m8056 (Object_t53 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetInstanceID_m2588(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_CompareBaseObjects_m8057 (Object_t * __this /* static, unused */, Object_t53 * ___lhs, Object_t53 * ___rhs, const MethodInfo* method)
{
	{
		Object_t53 * L_0 = ___lhs;
		Object_t53 * L_1 = ___rhs;
		bool L_2 = Object_CompareBaseObjectsInternal_m8058(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjectsInternal(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_CompareBaseObjectsInternal_m8058 (Object_t * __this /* static, unused */, Object_t53 * ___lhs, Object_t53 * ___rhs, const MethodInfo* method)
{
	typedef bool (*Object_CompareBaseObjectsInternal_m8058_ftn) (Object_t53 *, Object_t53 *);
	static Object_CompareBaseObjectsInternal_m8058_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_CompareBaseObjectsInternal_m8058_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::CompareBaseObjectsInternal(UnityEngine.Object,UnityEngine.Object)");
	return _il2cpp_icall_func(___lhs, ___rhs);
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C" int32_t Object_GetInstanceID_m2588 (Object_t53 * __this, const MethodInfo* method)
{
	{
		ReferenceData_t1343 * L_0 = &(__this->___m_UnityRuntimeReferenceData_0);
		int32_t L_1 = (L_0->___instanceID_0);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C" Object_t53 * Object_Internal_CloneSingle_m8059 (Object_t * __this /* static, unused */, Object_t53 * ___data, const MethodInfo* method)
{
	typedef Object_t53 * (*Object_Internal_CloneSingle_m8059_ftn) (Object_t53 *);
	static Object_Internal_CloneSingle_m8059_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m8059_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" Object_t53 * Object_Internal_InstantiateSingle_m8060 (Object_t * __this /* static, unused */, Object_t53 * ___data, Vector3_t6  ___pos, Quaternion_t51  ___rot, const MethodInfo* method)
{
	{
		Object_t53 * L_0 = ___data;
		Object_t53 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m8061(NULL /*static, unused*/, L_0, (&___pos), (&___rot), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" Object_t53 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m8061 (Object_t * __this /* static, unused */, Object_t53 * ___data, Vector3_t6 * ___pos, Quaternion_t51 * ___rot, const MethodInfo* method)
{
	typedef Object_t53 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m8061_ftn) (Object_t53 *, Vector3_t6 *, Quaternion_t51 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m8061_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m8061_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data, ___pos, ___rot);
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppCodeGenString* _stringLiteral1317;
extern "C" Object_t53 * Object_Instantiate_m2641 (Object_t * __this /* static, unused */, Object_t53 * ___original, Vector3_t6  ___position, Quaternion_t51  ___rotation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1317 = il2cpp_codegen_string_literal_from_index(1317);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t53 * L_0 = ___original;
		Object_CheckNullArgument_m8062(NULL /*static, unused*/, L_0, _stringLiteral1317, /*hidden argument*/NULL);
		Object_t53 * L_1 = ___original;
		Vector3_t6  L_2 = ___position;
		Quaternion_t51  L_3 = ___rotation;
		Object_t53 * L_4 = Object_Internal_InstantiateSingle_m8060(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern Il2CppCodeGenString* _stringLiteral1318;
extern "C" Object_t53 * Object_Instantiate_m2322 (Object_t * __this /* static, unused */, Object_t53 * ___original, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1318 = il2cpp_codegen_string_literal_from_index(1318);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t53 * L_0 = ___original;
		Object_CheckNullArgument_m8062(NULL /*static, unused*/, L_0, _stringLiteral1318, /*hidden argument*/NULL);
		Object_t53 * L_1 = ___original;
		Object_t53 * L_2 = Object_Internal_CloneSingle_m8059(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern TypeInfo* ArgumentException_t513_il2cpp_TypeInfo_var;
extern "C" void Object_CheckNullArgument_m8062 (Object_t * __this /* static, unused */, Object_t * ___arg, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___arg;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message;
		ArgumentException_t513 * L_2 = (ArgumentException_t513 *)il2cpp_codegen_object_new (ArgumentException_t513_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2349(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_000d:
	{
		return;
	}
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" void Object_Destroy_m2617 (Object_t * __this /* static, unused */, Object_t53 * ___obj, float ___t, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m2617_ftn) (Object_t53 *, float);
	static Object_Destroy_m2617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m2617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj, ___t);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" void Object_Destroy_m401 (Object_t * __this /* static, unused */, Object_t53 * ___obj, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t53 * L_0 = ___obj;
		float L_1 = V_0;
		Object_Destroy_m2617(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C" void Object_DestroyImmediate_m8063 (Object_t * __this /* static, unused */, Object_t53 * ___obj, bool ___allowDestroyingAssets, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m8063_ftn) (Object_t53 *, bool);
	static Object_DestroyImmediate_m8063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m8063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj, ___allowDestroyingAssets);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" void Object_DestroyImmediate_m4302 (Object_t * __this /* static, unused */, Object_t53 * ___obj, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Object_t53 * L_0 = ___obj;
		bool L_1 = V_0;
		Object_DestroyImmediate_m8063(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C" ObjectU5BU5D_t1153* Object_FindObjectsOfType_m6583 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1153* (*Object_FindObjectsOfType_m6583_ftn) (Type_t *);
	static Object_FindObjectsOfType_m6583_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m6583_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C" Object_t53 * Object_FindObjectOfType_m2210 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	ObjectU5BU5D_t1153* V_0 = {0};
	{
		Type_t * L_0 = ___type;
		ObjectU5BU5D_t1153* L_1 = Object_FindObjectsOfType_m6583(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t1153* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		ObjectU5BU5D_t1153* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		return (*(Object_t53 **)(Object_t53 **)SZArrayLdElema(L_3, L_4, sizeof(Object_t53 *)));
	}

IL_0014:
	{
		return (Object_t53 *)NULL;
	}
}
// System.String UnityEngine.Object::get_name()
extern "C" String_t* Object_get_name_m2286 (Object_t53 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m2286_ftn) (Object_t53 *);
	static Object_get_name_m2286_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m2286_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" void Object_set_name_m4363 (Object_t53 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Object_set_name_m4363_ftn) (Object_t53 *, String_t*);
	static Object_set_name_m4363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m4363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" void Object_DontDestroyOnLoad_m2165 (Object_t * __this /* static, unused */, Object_t53 * ___target, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m2165_ftn) (Object_t53 *);
	static Object_DontDestroyOnLoad_m2165_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m2165_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" void Object_set_hideFlags_m2253 (Object_t53 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m2253_ftn) (Object_t53 *, int32_t);
	static Object_set_hideFlags_m2253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m2253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Object::ToString()
extern "C" String_t* Object_ToString_m8064 (Object_t53 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m8064_ftn) (Object_t53 *);
	static Object_ToString_m8064_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m8064_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" bool Object_op_Implicit_m301 (Object_t * __this /* static, unused */, Object_t53 * ___exists, const MethodInfo* method)
{
	{
		Object_t53 * L_0 = ___exists;
		bool L_1 = Object_CompareBaseObjects_m8057(NULL /*static, unused*/, L_0, (Object_t53 *)NULL, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Equality_m427 (Object_t * __this /* static, unused */, Object_t53 * ___x, Object_t53 * ___y, const MethodInfo* method)
{
	{
		Object_t53 * L_0 = ___x;
		Object_t53 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m8057(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Inequality_m395 (Object_t * __this /* static, unused */, Object_t53 * ___x, Object_t53 * ___y, const MethodInfo* method)
{
	{
		Object_t53 * L_0 = ___x;
		Object_t53 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m8057(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t53_marshal(const Object_t53& unmarshaled, Object_t53_marshaled& marshaled)
{
	marshaled.___m_UnityRuntimeReferenceData_0 = unmarshaled.___m_UnityRuntimeReferenceData_0;
}
extern "C" void Object_t53_marshal_back(const Object_t53_marshaled& marshaled, Object_t53& unmarshaled)
{
	unmarshaled.___m_UnityRuntimeReferenceData_0 = marshaled.___m_UnityRuntimeReferenceData_0;
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t53_marshal_cleanup(Object_t53_marshaled& marshaled)
{
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" Transform_t25 * Component_get_transform_m416 (Component_t56 * __this, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = Component_InternalGetTransform_m8065(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Component::InternalGetTransform()
extern "C" Transform_t25 * Component_InternalGetTransform_m8065 (Component_t56 * __this, const MethodInfo* method)
{
	typedef Transform_t25 * (*Component_InternalGetTransform_m8065_ftn) (Component_t56 *);
	static Component_InternalGetTransform_m8065_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_InternalGetTransform_m8065_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::InternalGetTransform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rigidbody UnityEngine.Component::get_rigidbody()
extern "C" Rigidbody_t50 * Component_get_rigidbody_m377 (Component_t56 * __this, const MethodInfo* method)
{
	typedef Rigidbody_t50 * (*Component_get_rigidbody_m377_ftn) (Component_t56 *);
	static Component_get_rigidbody_m377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_rigidbody_m377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_rigidbody()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Component::get_camera()
extern "C" Camera_t510 * Component_get_camera_m4029 (Component_t56 * __this, const MethodInfo* method)
{
	typedef Camera_t510 * (*Component_get_camera_m4029_ftn) (Component_t56 *);
	static Component_get_camera_m4029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_camera_m4029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_camera()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Light UnityEngine.Component::get_light()
extern "C" Light_t47 * Component_get_light_m352 (Component_t56 * __this, const MethodInfo* method)
{
	typedef Light_t47 * (*Component_get_light_m352_ftn) (Component_t56 *);
	static Component_get_light_m352_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_light_m352_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_light()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Renderer UnityEngine.Component::get_renderer()
extern "C" Renderer_t46 * Component_get_renderer_m349 (Component_t56 * __this, const MethodInfo* method)
{
	typedef Renderer_t46 * (*Component_get_renderer_m349_ftn) (Component_t56 *);
	static Component_get_renderer_m349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_renderer_m349_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_renderer()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.AudioSource UnityEngine.Component::get_audio()
extern "C" AudioSource_t20 * Component_get_audio_m355 (Component_t56 * __this, const MethodInfo* method)
{
	typedef AudioSource_t20 * (*Component_get_audio_m355_ftn) (Component_t56 *);
	static Component_get_audio_m355_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_audio_m355_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_audio()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GUIText UnityEngine.Component::get_guiText()
extern "C" GUIText_t44 * Component_get_guiText_m348 (Component_t56 * __this, const MethodInfo* method)
{
	typedef GUIText_t44 * (*Component_get_guiText_m348_ftn) (Component_t56 *);
	static Component_get_guiText_m348_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_guiText_m348_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_guiText()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GUITexture UnityEngine.Component::get_guiTexture()
extern "C" GUITexture_t43 * Component_get_guiTexture_m346 (Component_t56 * __this, const MethodInfo* method)
{
	typedef GUITexture_t43 * (*Component_get_guiTexture_m346_ftn) (Component_t56 *);
	static Component_get_guiTexture_m346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_guiTexture_m346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_guiTexture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" GameObject_t27 * Component_get_gameObject_m306 (Component_t56 * __this, const MethodInfo* method)
{
	{
		GameObject_t27 * L_0 = Component_InternalGetGameObject_m8066(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject UnityEngine.Component::InternalGetGameObject()
extern "C" GameObject_t27 * Component_InternalGetGameObject_m8066 (Component_t56 * __this, const MethodInfo* method)
{
	typedef GameObject_t27 * (*Component_InternalGetGameObject_m8066_ftn) (Component_t56 *);
	static Component_InternalGetGameObject_m8066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_InternalGetGameObject_m8066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::InternalGetGameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C" Component_t56 * Component_GetComponent_m4416 (Component_t56 * __this, Type_t * ___type, const MethodInfo* method)
{
	typedef Component_t56 * (*Component_GetComponent_m4416_ftn) (Component_t56 *, Type_t *);
	static Component_GetComponent_m4416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponent_m4416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C" Component_t56 * Component_GetComponentInChildren_m8067 (Component_t56 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		GameObject_t27 * L_0 = Component_get_gameObject_m306(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t56 * L_2 = GameObject_GetComponentInChildren_m7193(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsWithCorrectReturnType(System.Type)
extern "C" ComponentU5BU5D_t54* Component_GetComponentsWithCorrectReturnType_m8068 (Component_t56 * __this, Type_t * ___type, const MethodInfo* method)
{
	typedef ComponentU5BU5D_t54* (*Component_GetComponentsWithCorrectReturnType_m8068_ftn) (Component_t56 *, Type_t *);
	static Component_GetComponentsWithCorrectReturnType_m8068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsWithCorrectReturnType_m8068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsWithCorrectReturnType(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Type,System.Boolean,System.Boolean,System.Object)
extern "C" void Component_GetComponentsForListInternal_m8069 (Component_t56 * __this, Type_t * ___searchType, Type_t * ___listElementType, bool ___recursive, bool ___includeInactive, Object_t * ___resultList, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m8069_ftn) (Component_t56 *, Type_t *, Type_t *, bool, bool, Object_t *);
	static Component_GetComponentsForListInternal_m8069_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m8069_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Type,System.Boolean,System.Boolean,System.Object)");
	_il2cpp_icall_func(__this, ___searchType, ___listElementType, ___recursive, ___includeInactive, ___resultList);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern const Il2CppType* Component_t56_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void Component_GetComponents_m4135 (Component_t56 * __this, Type_t * ___type, List_1_t826 * ___results, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_t56_0_0_0_var = il2cpp_codegen_type_from_index(532);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m304(NULL /*static, unused*/, LoadTypeToken(Component_t56_0_0_0_var), /*hidden argument*/NULL);
		List_1_t826 * L_2 = ___results;
		Component_GetComponentsForListInternal_m8069(__this, L_0, L_1, 0, 1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void Component_SendMessage_m8070 (Component_t56 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method)
{
	typedef void (*Component_SendMessage_m8070_ftn) (Component_t56 *, String_t*, Object_t *, int32_t);
	static Component_SendMessage_m8070_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessage_m8070_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object)
extern "C" void Component_SendMessage_m2805 (Component_t56 * __this, String_t* ___methodName, Object_t * ___value, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = ___value;
		int32_t L_2 = V_0;
		Component_SendMessage_m8070(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String)
extern "C" void Component_SendMessage_m2804 (Component_t56 * __this, String_t* ___methodName, const MethodInfo* method)
{
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessage_m8070(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UnityEngine.Light::get_color()
extern "C" Color_t5  Light_get_color_m317 (Light_t47 * __this, const MethodInfo* method)
{
	Color_t5  V_0 = {0};
	{
		Light_INTERNAL_get_color_m8071(__this, (&V_0), /*hidden argument*/NULL);
		Color_t5  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern "C" void Light_set_color_m320 (Light_t47 * __this, Color_t5  ___value, const MethodInfo* method)
{
	{
		Light_INTERNAL_set_color_m8072(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void Light_INTERNAL_get_color_m8071 (Light_t47 * __this, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*Light_INTERNAL_get_color_m8071_ftn) (Light_t47 *, Color_t5 *);
	static Light_INTERNAL_get_color_m8071_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_INTERNAL_get_color_m8071_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Light_INTERNAL_set_color_m8072 (Light_t47 * __this, Color_t5 * ___value, const MethodInfo* method)
{
	typedef void (*Light_INTERNAL_set_color_m8072_ftn) (Light_t47 *, Color_t5 *);
	static Light_INTERNAL_set_color_m8072_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_INTERNAL_set_color_m8072_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Light::get_intensity()
extern "C" float Light_get_intensity_m2581 (Light_t47 * __this, const MethodInfo* method)
{
	typedef float (*Light_get_intensity_m2581_ftn) (Light_t47 *);
	static Light_get_intensity_m2581_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_get_intensity_m2581_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::get_intensity()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C" void Light_set_intensity_m2582 (Light_t47 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Light_set_intensity_m2582_ftn) (Light_t47 *, float);
	static Light_set_intensity_m2582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_intensity_m2582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_intensity(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C" void Enumerator__ctor_m8073 (Enumerator_t1427 * __this, Transform_t25 * ___outer, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		Transform_t25 * L_0 = ___outer;
		__this->___outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m8074 (Enumerator_t1427 * __this, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = (__this->___outer_0);
		int32_t L_1 = (__this->___currentIndex_1);
		NullCheck(L_0);
		Transform_t25 * L_2 = Transform_GetChild_m2547(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m8075 (Enumerator_t1427 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t25 * L_0 = (__this->___outer_0);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m2548(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___currentIndex_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->___currentIndex_1 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return ((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C" void Enumerator_Reset_m8076 (Enumerator_t1427 * __this, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" Vector3_t6  Transform_get_position_m334 (Transform_t25 * __this, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Transform_INTERNAL_get_position_m8077(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" void Transform_set_position_m340 (Transform_t25 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m8078(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_position_m8077 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m8077_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_get_position_m8077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m8077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_position_m8078 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m8078_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_set_position_m8078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m8078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" Vector3_t6  Transform_get_localPosition_m338 (Transform_t25 * __this, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Transform_INTERNAL_get_localPosition_m8079(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" void Transform_set_localPosition_m339 (Transform_t25 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m8080(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localPosition_m8079 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m8079_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_get_localPosition_m8079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m8079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localPosition_m8080 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m8080_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_set_localPosition_m8080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m8080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C" Vector3_t6  Transform_get_eulerAngles_m327 (Transform_t25 * __this, const MethodInfo* method)
{
	Quaternion_t51  V_0 = {0};
	{
		Quaternion_t51  L_0 = Transform_get_rotation_m2607(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t6  L_1 = Quaternion_get_eulerAngles_m7859((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C" void Transform_set_eulerAngles_m333 (Transform_t25 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = ___value;
		Quaternion_t51  L_1 = Quaternion_Euler_m379(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Transform_set_rotation_m381(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
extern "C" Vector3_t6  Transform_get_localEulerAngles_m342 (Transform_t25 * __this, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Transform_INTERNAL_get_localEulerAngles_m8081(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C" void Transform_set_localEulerAngles_m343 (Transform_t25 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localEulerAngles_m8082(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localEulerAngles_m8081 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localEulerAngles_m8081_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_get_localEulerAngles_m8081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localEulerAngles_m8081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localEulerAngles_m8082 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localEulerAngles_m8082_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_set_localEulerAngles_m8082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localEulerAngles_m8082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C" Vector3_t6  Transform_get_forward_m4169 (Transform_t25 * __this, const MethodInfo* method)
{
	{
		Quaternion_t51  L_0 = Transform_get_rotation_m2607(__this, /*hidden argument*/NULL);
		Vector3_t6  L_1 = Vector3_get_forward_m2566(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6  L_2 = Quaternion_op_Multiply_m4167(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" Quaternion_t51  Transform_get_rotation_m2607 (Transform_t25 * __this, const MethodInfo* method)
{
	Quaternion_t51  V_0 = {0};
	{
		Transform_INTERNAL_get_rotation_m8083(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" void Transform_set_rotation_m381 (Transform_t25 * __this, Quaternion_t51  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_rotation_m8084(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_rotation_m8083 (Transform_t25 * __this, Quaternion_t51 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m8083_ftn) (Transform_t25 *, Quaternion_t51 *);
	static Transform_INTERNAL_get_rotation_m8083_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m8083_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_rotation_m8084 (Transform_t25 * __this, Quaternion_t51 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m8084_ftn) (Transform_t25 *, Quaternion_t51 *);
	static Transform_INTERNAL_set_rotation_m8084_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m8084_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" Quaternion_t51  Transform_get_localRotation_m4279 (Transform_t25 * __this, const MethodInfo* method)
{
	Quaternion_t51  V_0 = {0};
	{
		Transform_INTERNAL_get_localRotation_m8085(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" void Transform_set_localRotation_m380 (Transform_t25 * __this, Quaternion_t51  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m8086(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_localRotation_m8085 (Transform_t25 * __this, Quaternion_t51 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m8085_ftn) (Transform_t25 *, Quaternion_t51 *);
	static Transform_INTERNAL_get_localRotation_m8085_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m8085_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_localRotation_m8086 (Transform_t25 * __this, Quaternion_t51 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m8086_ftn) (Transform_t25 *, Quaternion_t51 *);
	static Transform_INTERNAL_set_localRotation_m8086_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m8086_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" Vector3_t6  Transform_get_localScale_m336 (Transform_t25 * __this, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Transform_INTERNAL_get_localScale_m8087(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" void Transform_set_localScale_m341 (Transform_t25 * __this, Vector3_t6  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m8088(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localScale_m8087 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m8087_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_get_localScale_m8087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m8087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localScale_m8088 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m8088_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_set_localScale_m8088_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m8088_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" Transform_t25 * Transform_get_parent_m2605 (Transform_t25 * __this, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = Transform_get_parentInternal_m8089(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern TypeInfo* RectTransform_t688_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" void Transform_set_parent_m2395 (Transform_t25 * __this, Transform_t25 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t688_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(524);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t688 *)IsInstSealed(__this, RectTransform_t688_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogWarning_m4362(NULL /*static, unused*/, _stringLiteral1319, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t25 * L_0 = ___value;
		Transform_set_parentInternal_m8090(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C" Transform_t25 * Transform_get_parentInternal_m8089 (Transform_t25 * __this, const MethodInfo* method)
{
	typedef Transform_t25 * (*Transform_get_parentInternal_m8089_ftn) (Transform_t25 *);
	static Transform_get_parentInternal_m8089_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m8089_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C" void Transform_set_parentInternal_m8090 (Transform_t25 * __this, Transform_t25 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m8090_ftn) (Transform_t25 *, Transform_t25 *);
	static Transform_set_parentInternal_m8090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m8090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" void Transform_SetParent_m4271 (Transform_t25 * __this, Transform_t25 * ___parent, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = ___parent;
		Transform_SetParent_m8091(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C" void Transform_SetParent_m8091 (Transform_t25 * __this, Transform_t25 * ___parent, bool ___worldPositionStays, const MethodInfo* method)
{
	typedef void (*Transform_SetParent_m8091_ftn) (Transform_t25 *, Transform_t25 *, bool);
	static Transform_SetParent_m8091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m8091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent, ___worldPositionStays);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C" Matrix4x4_t603  Transform_get_worldToLocalMatrix_m4332 (Transform_t25 * __this, const MethodInfo* method)
{
	Matrix4x4_t603  V_0 = {0};
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m8092(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t603  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m8092 (Transform_t25 * __this, Matrix4x4_t603 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m8092_ftn) (Transform_t25 *, Matrix4x4_t603 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m8092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m8092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C" Matrix4x4_t603  Transform_get_localToWorldMatrix_m2782 (Transform_t25 * __this, const MethodInfo* method)
{
	Matrix4x4_t603  V_0 = {0};
	{
		Transform_INTERNAL_get_localToWorldMatrix_m8093(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t603  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C" void Transform_INTERNAL_get_localToWorldMatrix_m8093 (Transform_t25 * __this, Matrix4x4_t603 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localToWorldMatrix_m8093_ftn) (Transform_t25 *, Matrix4x4_t603 *);
	static Transform_INTERNAL_get_localToWorldMatrix_m8093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localToWorldMatrix_m8093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C" void Transform_Translate_m2575 (Transform_t25 * __this, Vector3_t6  ___translation, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		Vector3_t6  L_0 = ___translation;
		int32_t L_1 = V_0;
		Transform_Translate_m364(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C" void Transform_Translate_m364 (Transform_t25 * __this, Vector3_t6  ___translation, int32_t ___relativeTo, const MethodInfo* method)
{
	{
		int32_t L_0 = ___relativeTo;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t6  L_1 = Transform_get_position_m334(__this, /*hidden argument*/NULL);
		Vector3_t6  L_2 = ___translation;
		Vector3_t6  L_3 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Transform_set_position_m340(__this, L_3, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_001d:
	{
		Vector3_t6  L_4 = Transform_get_position_m334(__this, /*hidden argument*/NULL);
		Vector3_t6  L_5 = ___translation;
		Vector3_t6  L_6 = Transform_TransformDirection_m8098(__this, L_5, /*hidden argument*/NULL);
		Vector3_t6  L_7 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Transform_set_position_m340(__this, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C" void Transform_Translate_m2677 (Transform_t25 * __this, float ___x, float ___y, float ___z, int32_t ___relativeTo, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		float L_1 = ___y;
		float L_2 = ___z;
		Vector3_t6  L_3 = {0};
		Vector3__ctor_m335(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___relativeTo;
		Transform_Translate_m364(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C" void Transform_Rotate_m2578 (Transform_t25 * __this, Vector3_t6  ___eulerAngles, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		Vector3_t6  L_0 = ___eulerAngles;
		int32_t L_1 = V_0;
		Transform_Rotate_m383(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C" void Transform_Rotate_m383 (Transform_t25 * __this, Vector3_t6  ___eulerAngles, int32_t ___relativeTo, const MethodInfo* method)
{
	Quaternion_t51  V_0 = {0};
	{
		float L_0 = ((&___eulerAngles)->___x_1);
		float L_1 = ((&___eulerAngles)->___y_2);
		float L_2 = ((&___eulerAngles)->___z_3);
		Quaternion_t51  L_3 = Quaternion_Euler_m7860(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0039;
		}
	}
	{
		Quaternion_t51  L_5 = Transform_get_localRotation_m4279(__this, /*hidden argument*/NULL);
		Quaternion_t51  L_6 = V_0;
		Quaternion_t51  L_7 = Quaternion_op_Multiply_m6443(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m380(__this, L_7, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0039:
	{
		Quaternion_t51  L_8 = Transform_get_rotation_m2607(__this, /*hidden argument*/NULL);
		Quaternion_t51  L_9 = Transform_get_rotation_m2607(__this, /*hidden argument*/NULL);
		Quaternion_t51  L_10 = Quaternion_Inverse_m4350(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t51  L_11 = V_0;
		Quaternion_t51  L_12 = Quaternion_op_Multiply_m6443(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t51  L_13 = Transform_get_rotation_m2607(__this, /*hidden argument*/NULL);
		Quaternion_t51  L_14 = Quaternion_op_Multiply_m6443(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t51  L_15 = Quaternion_op_Multiply_m6443(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m381(__this, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C" void Transform_Rotate_m2608 (Transform_t25 * __this, float ___xAngle, float ___yAngle, float ___zAngle, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		float L_0 = ___xAngle;
		float L_1 = ___yAngle;
		float L_2 = ___zAngle;
		int32_t L_3 = V_0;
		Transform_Rotate_m8094(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C" void Transform_Rotate_m8094 (Transform_t25 * __this, float ___xAngle, float ___yAngle, float ___zAngle, int32_t ___relativeTo, const MethodInfo* method)
{
	{
		float L_0 = ___xAngle;
		float L_1 = ___yAngle;
		float L_2 = ___zAngle;
		Vector3_t6  L_3 = {0};
		Vector3__ctor_m335(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___relativeTo;
		Transform_Rotate_m383(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::RotateAroundInternal(UnityEngine.Vector3,System.Single)
extern "C" void Transform_RotateAroundInternal_m8095 (Transform_t25 * __this, Vector3_t6  ___axis, float ___angle, const MethodInfo* method)
{
	{
		float L_0 = ___angle;
		Transform_INTERNAL_CALL_RotateAroundInternal_m8096(NULL /*static, unused*/, __this, (&___axis), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)
extern "C" void Transform_INTERNAL_CALL_RotateAroundInternal_m8096 (Object_t * __this /* static, unused */, Transform_t25 * ___self, Vector3_t6 * ___axis, float ___angle, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_RotateAroundInternal_m8096_ftn) (Transform_t25 *, Vector3_t6 *, float);
	static Transform_INTERNAL_CALL_RotateAroundInternal_m8096_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_RotateAroundInternal_m8096_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___self, ___axis, ___angle);
}
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" void Transform_RotateAround_m2577 (Transform_t25 * __this, Vector3_t6  ___point, Vector3_t6  ___axis, float ___angle, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	Quaternion_t51  V_1 = {0};
	Vector3_t6  V_2 = {0};
	{
		Vector3_t6  L_0 = Transform_get_position_m334(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___angle;
		Vector3_t6  L_2 = ___axis;
		Quaternion_t51  L_3 = Quaternion_AngleAxis_m6432(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t6  L_4 = V_0;
		Vector3_t6  L_5 = ___point;
		Vector3_t6  L_6 = Vector3_op_Subtraction_m292(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Quaternion_t51  L_7 = V_1;
		Vector3_t6  L_8 = V_2;
		Vector3_t6  L_9 = Quaternion_op_Multiply_m4167(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector3_t6  L_10 = ___point;
		Vector3_t6  L_11 = V_2;
		Vector3_t6  L_12 = Vector3_op_Addition_m291(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Vector3_t6  L_13 = V_0;
		Transform_set_position_m340(__this, L_13, /*hidden argument*/NULL);
		Vector3_t6  L_14 = ___axis;
		float L_15 = ___angle;
		Transform_RotateAroundInternal_m8095(__this, L_14, ((float)((float)L_15*(float)(0.0174532924f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern "C" void Transform_LookAt_m330 (Transform_t25 * __this, Transform_t25 * ___target, Vector3_t6  ___worldUp, const MethodInfo* method)
{
	{
		Transform_t25 * L_0 = ___target;
		bool L_1 = Object_op_Implicit_m301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Transform_t25 * L_2 = ___target;
		NullCheck(L_2);
		Vector3_t6  L_3 = Transform_get_position_m334(L_2, /*hidden argument*/NULL);
		Vector3_t6  L_4 = ___worldUp;
		Transform_LookAt_m331(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Transform_LookAt_m331 (Transform_t25 * __this, Vector3_t6  ___worldPosition, Vector3_t6  ___worldUp, const MethodInfo* method)
{
	{
		Transform_INTERNAL_CALL_LookAt_m8097(NULL /*static, unused*/, __this, (&___worldPosition), (&___worldUp), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern "C" void Transform_LookAt_m2614 (Transform_t25 * __this, Vector3_t6  ___worldPosition, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Vector3_t6  L_0 = Vector3_get_up_m284(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_INTERNAL_CALL_LookAt_m8097(NULL /*static, unused*/, __this, (&___worldPosition), (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_CALL_LookAt_m8097 (Object_t * __this /* static, unused */, Transform_t25 * ___self, Vector3_t6 * ___worldPosition, Vector3_t6 * ___worldUp, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_LookAt_m8097_ftn) (Transform_t25 *, Vector3_t6 *, Vector3_t6 *);
	static Transform_INTERNAL_CALL_LookAt_m8097_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_LookAt_m8097_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___worldPosition, ___worldUp);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C" Vector3_t6  Transform_TransformDirection_m8098 (Transform_t25 * __this, Vector3_t6  ___direction, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Transform_INTERNAL_CALL_TransformDirection_m8099(NULL /*static, unused*/, __this, (&___direction), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t6  Transform_INTERNAL_CALL_TransformDirection_m8099 (Object_t * __this /* static, unused */, Transform_t25 * ___self, Vector3_t6 * ___direction, const MethodInfo* method)
{
	typedef Vector3_t6  (*Transform_INTERNAL_CALL_TransformDirection_m8099_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_CALL_TransformDirection_m8099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m8099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___direction);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t6  Transform_TransformPoint_m4351 (Transform_t25 * __this, Vector3_t6  ___position, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Transform_INTERNAL_CALL_TransformPoint_m8100(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t6  Transform_INTERNAL_CALL_TransformPoint_m8100 (Object_t * __this /* static, unused */, Transform_t25 * ___self, Vector3_t6 * ___position, const MethodInfo* method)
{
	typedef Vector3_t6  (*Transform_INTERNAL_CALL_TransformPoint_m8100_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_CALL_TransformPoint_m8100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m8100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t6  Transform_InverseTransformPoint_m4246 (Transform_t25 * __this, Vector3_t6  ___position, const MethodInfo* method)
{
	{
		Vector3_t6  L_0 = Transform_INTERNAL_CALL_InverseTransformPoint_m8101(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t6  Transform_INTERNAL_CALL_InverseTransformPoint_m8101 (Object_t * __this /* static, unused */, Transform_t25 * ___self, Vector3_t6 * ___position, const MethodInfo* method)
{
	typedef Vector3_t6  (*Transform_INTERNAL_CALL_InverseTransformPoint_m8101_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m8101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m8101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C" Transform_t25 * Transform_get_root_m6787 (Transform_t25 * __this, const MethodInfo* method)
{
	typedef Transform_t25 * (*Transform_get_root_m6787_ftn) (Transform_t25 *);
	static Transform_get_root_m6787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_root_m6787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_root()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" int32_t Transform_get_childCount_m2548 (Transform_t25 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m2548_ftn) (Transform_t25 *);
	static Transform_get_childCount_m2548_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m2548_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C" void Transform_SetAsFirstSibling_m4272 (Transform_t25 * __this, const MethodInfo* method)
{
	typedef void (*Transform_SetAsFirstSibling_m4272_ftn) (Transform_t25 *);
	static Transform_SetAsFirstSibling_m4272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m4272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" Transform_t25 * Transform_Find_m6792 (Transform_t25 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef Transform_t25 * (*Transform_Find_m6792_ftn) (Transform_t25 *, String_t*);
	static Transform_Find_m6792_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_Find_m6792_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::Find(System.String)");
	return _il2cpp_icall_func(__this, ___name);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C" Vector3_t6  Transform_get_lossyScale_m2790 (Transform_t25 * __this, const MethodInfo* method)
{
	Vector3_t6  V_0 = {0};
	{
		Transform_INTERNAL_get_lossyScale_m8102(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_lossyScale_m8102 (Transform_t25 * __this, Vector3_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_lossyScale_m8102_ftn) (Transform_t25 *, Vector3_t6 *);
	static Transform_INTERNAL_get_lossyScale_m8102_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_lossyScale_m8102_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Transform UnityEngine.Transform::FindChild(System.String)
extern "C" Transform_t25 * Transform_FindChild_m2761 (Transform_t25 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Transform_t25 * L_1 = Transform_Find_m6792(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern TypeInfo* Enumerator_t1427_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_GetEnumerator_m8103 (Transform_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1427_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1427 * L_0 = (Enumerator_t1427 *)il2cpp_codegen_object_new (Enumerator_t1427_il2cpp_TypeInfo_var);
		Enumerator__ctor_m8073(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" Transform_t25 * Transform_GetChild_m2547 (Transform_t25 * __this, int32_t ___index, const MethodInfo* method)
{
	typedef Transform_t25 * (*Transform_GetChild_m2547_ftn) (Transform_t25 *, int32_t);
	static Transform_GetChild_m2547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m2547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// System.Single UnityEngine.Time::get_time()
extern "C" float Time_get_time_m294 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_time_m294_ftn) ();
	static Time_get_time_m294_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m294_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" float Time_get_deltaTime_m388 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m388_ftn) ();
	static Time_get_deltaTime_m388_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m388_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C" float Time_get_unscaledTime_m4065 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledTime_m4065_ftn) ();
	static Time_get_unscaledTime_m4065_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m4065_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C" float Time_get_unscaledDeltaTime_m4108 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m4108_ftn) ();
	static Time_get_unscaledDeltaTime_m4108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m4108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeScale()
extern "C" float Time_get_timeScale_m8104 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeScale_m8104_ftn) ();
	static Time_get_timeScale_m8104_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeScale_m8104_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeScale()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C" void Time_set_timeScale_m2555 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	typedef void (*Time_set_timeScale_m2555_ftn) (float);
	static Time_set_timeScale_m2555_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_set_timeScale_m2555_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::set_timeScale(System.Single)");
	_il2cpp_icall_func(___value);
}
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C" int32_t Time_get_frameCount_m6808 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Time_get_frameCount_m6808_ftn) ();
	static Time_get_frameCount_m6808_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_frameCount_m6808_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_frameCount()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C" float Time_get_realtimeSinceStartup_m417 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m417_ftn) ();
	static Time_get_realtimeSinceStartup_m417_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m417_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" float Random_Range_m384 (Object_t * __this /* static, unused */, float ___min, float ___max, const MethodInfo* method)
{
	typedef float (*Random_Range_m384_ftn) (float, float);
	static Random_Range_m384_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m384_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" int32_t Random_Range_m8105 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min;
		int32_t L_1 = ___max;
		int32_t L_2 = Random_RandomRangeInt_m8106(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C" int32_t Random_RandomRangeInt_m8106 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m8106_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m8106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m8106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m8107 (YieldInstruction_t1349 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m285(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t1349_marshal(const YieldInstruction_t1349& unmarshaled, YieldInstruction_t1349_marshaled& marshaled)
{
}
extern "C" void YieldInstruction_t1349_marshal_back(const YieldInstruction_t1349_marshaled& marshaled, YieldInstruction_t1349& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t1349_marshal_cleanup(YieldInstruction_t1349_marshaled& marshaled)
{
}
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C" void PlayerPrefsException__ctor_m8108 (PlayerPrefsException_t1430 * __this, String_t* ___error, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error;
		Exception__ctor_m2366(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C" bool PlayerPrefs_TrySetInt_m8109 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m8109_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m8109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m8109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)
extern "C" bool PlayerPrefs_TrySetFloat_m8110 (Object_t * __this /* static, unused */, String_t* ___key, float ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetFloat_m8110_ftn) (String_t*, float);
	static PlayerPrefs_TrySetFloat_m8110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetFloat_m8110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C" bool PlayerPrefs_TrySetSetString_m8111 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m8111_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m8111_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m8111_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern TypeInfo* PlayerPrefsException_t1430_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1320;
extern "C" void PlayerPrefs_SetInt_m2242 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(986);
		_stringLiteral1320 = il2cpp_codegen_string_literal_from_index(1320);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		int32_t L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetInt_m8109(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t1430 * L_3 = (PlayerPrefsException_t1430 *)il2cpp_codegen_object_new (PlayerPrefsException_t1430_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m8108(L_3, _stringLiteral1320, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C" int32_t PlayerPrefs_GetInt_m8112 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___defaultValue, const MethodInfo* method)
{
	typedef int32_t (*PlayerPrefs_GetInt_m8112_ftn) (String_t*, int32_t);
	static PlayerPrefs_GetInt_m8112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetInt_m8112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C" int32_t PlayerPrefs_GetInt_m2241 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___key;
		int32_t L_1 = V_0;
		int32_t L_2 = PlayerPrefs_GetInt_m8112(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.PlayerPrefs::SetFloat(System.String,System.Single)
extern TypeInfo* PlayerPrefsException_t1430_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1320;
extern "C" void PlayerPrefs_SetFloat_m2205 (Object_t * __this /* static, unused */, String_t* ___key, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(986);
		_stringLiteral1320 = il2cpp_codegen_string_literal_from_index(1320);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		float L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetFloat_m8110(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t1430 * L_3 = (PlayerPrefsException_t1430 *)il2cpp_codegen_object_new (PlayerPrefsException_t1430_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m8108(L_3, _stringLiteral1320, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
extern "C" float PlayerPrefs_GetFloat_m8113 (Object_t * __this /* static, unused */, String_t* ___key, float ___defaultValue, const MethodInfo* method)
{
	typedef float (*PlayerPrefs_GetFloat_m8113_ftn) (String_t*, float);
	static PlayerPrefs_GetFloat_m8113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetFloat_m8113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String)
extern "C" float PlayerPrefs_GetFloat_m2206 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		String_t* L_0 = ___key;
		float L_1 = V_0;
		float L_2 = PlayerPrefs_GetFloat_m8113(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern TypeInfo* PlayerPrefsException_t1430_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1320;
extern "C" void PlayerPrefs_SetString_m2176 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(986);
		_stringLiteral1320 = il2cpp_codegen_string_literal_from_index(1320);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		String_t* L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetSetString_m8111(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t1430 * L_3 = (PlayerPrefsException_t1430 *)il2cpp_codegen_object_new (PlayerPrefsException_t1430_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m8108(L_3, _stringLiteral1320, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C" String_t* PlayerPrefs_GetString_m8114 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___defaultValue, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m8114_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m8114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m8114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayerPrefs_GetString_m2175 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ___key;
		String_t* L_2 = V_0;
		String_t* L_3 = PlayerPrefs_GetString_m8114(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C" bool PlayerPrefs_HasKey_m2174 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m2174_ftn) (String_t*);
	static PlayerPrefs_HasKey_m2174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m2174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key);
}
// System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
extern "C" void PlayerPrefs_DeleteKey_m2173 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteKey_m2173_ftn) (String_t*);
	static PlayerPrefs_DeleteKey_m2173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteKey_m2173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteKey(System.String)");
	_il2cpp_icall_func(___key);
}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C" void UnityAction__ctor_m4236 (UnityAction_t691 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C" void UnityAction_Invoke_m4130 (UnityAction_t691 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UnityAction_Invoke_m4130((UnityAction_t691 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_UnityAction_t691(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * UnityAction_BeginInvoke_m8115 (UnityAction_t691 * __this, AsyncCallback_t12 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C" void UnityAction_EndInvoke_m8116 (UnityAction_t691 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
