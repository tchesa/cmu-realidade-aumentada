﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// TesteTakeScreenshot
struct TesteTakeScreenshot_t394;

#include "mscorlib_System_Object.h"

// TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19
struct  U3CITakeScreenshotU3Ec__Iterator19_t393  : public Object_t
{
	// System.Int32 TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::$PC
	int32_t ___U24PC_0;
	// System.Object TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::$current
	Object_t * ___U24current_1;
	// TesteTakeScreenshot TesteTakeScreenshot/<ITakeScreenshot>c__Iterator19::<>f__this
	TesteTakeScreenshot_t394 * ___U3CU3Ef__this_2;
};
