﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3290;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__42.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_42.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m26677_gshared (Enumerator_t3297 * __this, Dictionary_2_t3290 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m26677(__this, ___dictionary, method) (( void (*) (Enumerator_t3297 *, Dictionary_2_t3290 *, const MethodInfo*))Enumerator__ctor_m26677_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26678_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26678(__this, method) (( Object_t * (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26678_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m26679_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m26679(__this, method) (( void (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m26679_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t58  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26680_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26680(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26680_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26681_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26681(__this, method) (( Object_t * (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26681_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26682_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26682(__this, method) (( Object_t * (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26682_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26683_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26683(__this, method) (( bool (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_MoveNext_m26683_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" KeyValuePair_2_t3292  Enumerator_get_Current_m26684_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26684(__this, method) (( KeyValuePair_2_t3292  (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_get_Current_m26684_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m26685_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m26685(__this, method) (( Object_t * (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_get_CurrentKey_m26685_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
extern "C" ProfileData_t1064  Enumerator_get_CurrentValue_m26686_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m26686(__this, method) (( ProfileData_t1064  (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_get_CurrentValue_m26686_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Reset()
extern "C" void Enumerator_Reset_m26687_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_Reset_m26687(__this, method) (( void (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_Reset_m26687_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyState()
extern "C" void Enumerator_VerifyState_m26688_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m26688(__this, method) (( void (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_VerifyState_m26688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m26689_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m26689(__this, method) (( void (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_VerifyCurrent_m26689_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m26690_gshared (Enumerator_t3297 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26690(__this, method) (( void (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_Dispose_m26690_gshared)(__this, method)
