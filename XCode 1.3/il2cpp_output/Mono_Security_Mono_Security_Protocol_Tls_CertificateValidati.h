﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1546;
// System.Int32[]
struct Int32U5BU5D_t335;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct  CertificateValidationCallback_t1776  : public MulticastDelegate_t10
{
};
