﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t387;
// System.Object
struct Object_t;
// StartUIController
struct StartUIController_t390;

#include "mscorlib_System_Object.h"

// StartUIController/<StartTransiction>c__Iterator18
struct  U3CStartTransictionU3Ec__Iterator18_t389  : public Object_t
{
	// UnityEngine.UI.Image StartUIController/<StartTransiction>c__Iterator18::<black>__0
	Image_t387 * ___U3CblackU3E__0_0;
	// System.Single StartUIController/<StartTransiction>c__Iterator18::<i>__1
	float ___U3CiU3E__1_1;
	// System.Int32 StartUIController/<StartTransiction>c__Iterator18::$PC
	int32_t ___U24PC_2;
	// System.Object StartUIController/<StartTransiction>c__Iterator18::$current
	Object_t * ___U24current_3;
	// StartUIController StartUIController/<StartTransiction>c__Iterator18::<>f__this
	StartUIController_t390 * ___U3CU3Ef__this_4;
};
