﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t1107;
// System.IAsyncResult
struct IAsyncResult_t11;
// System.AsyncCallback
struct AsyncCallback_t12;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Comparison`1<Vuforia.ILoadLevelEventHandler>
struct  Comparison_1_t3198  : public MulticastDelegate_t10
{
};
