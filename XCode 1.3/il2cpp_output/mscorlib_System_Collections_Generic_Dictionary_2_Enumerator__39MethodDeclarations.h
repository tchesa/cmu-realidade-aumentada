﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1209;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__39.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25996_gshared (Enumerator_t3244 * __this, Dictionary_2_t1209 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m25996(__this, ___dictionary, method) (( void (*) (Enumerator_t3244 *, Dictionary_2_t1209 *, const MethodInfo*))Enumerator__ctor_m25996_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25997_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25997(__this, method) (( Object_t * (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25997_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m25998_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m25998(__this, method) (( void (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m25998_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t58  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25999_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25999(__this, method) (( DictionaryEntry_t58  (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25999_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26000_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26000(__this, method) (( Object_t * (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26000_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26001_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26001(__this, method) (( Object_t * (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26001_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26002_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26002(__this, method) (( bool (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_MoveNext_m26002_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" KeyValuePair_2_t3240  Enumerator_get_Current_m26003_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26003(__this, method) (( KeyValuePair_2_t3240  (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_get_Current_m26003_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m26004_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m26004(__this, method) (( int32_t (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_get_CurrentKey_m26004_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentValue()
extern "C" TrackableResultData_t969  Enumerator_get_CurrentValue_m26005_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m26005(__this, method) (( TrackableResultData_t969  (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_get_CurrentValue_m26005_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Reset()
extern "C" void Enumerator_Reset_m26006_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_Reset_m26006(__this, method) (( void (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_Reset_m26006_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyState()
extern "C" void Enumerator_VerifyState_m26007_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m26007(__this, method) (( void (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_VerifyState_m26007_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m26008_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m26008(__this, method) (( void (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_VerifyCurrent_m26008_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m26009_gshared (Enumerator_t3244 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26009(__this, method) (( void (*) (Enumerator_t3244 *, const MethodInfo*))Enumerator_Dispose_m26009_gshared)(__this, method)
