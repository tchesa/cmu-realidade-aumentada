﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t331;
// UnityEngine.ParticleSystem
struct ParticleSystem_t332;
// System.Object
struct Object_t;
// CFX_ShurikenThreadFix
struct CFX_ShurikenThreadFix_t333;

#include "mscorlib_System_Object.h"

// CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC
struct  U3CWaitFrameU3Ec__IteratorC_t330  : public Object_t
{
	// UnityEngine.ParticleSystem[] CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::<$s_48>__0
	ParticleSystemU5BU5D_t331* ___U3CU24s_48U3E__0_0;
	// System.Int32 CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::<$s_49>__1
	int32_t ___U3CU24s_49U3E__1_1;
	// UnityEngine.ParticleSystem CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::<ps>__2
	ParticleSystem_t332 * ___U3CpsU3E__2_2;
	// System.Int32 CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::$PC
	int32_t ___U24PC_3;
	// System.Object CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::$current
	Object_t * ___U24current_4;
	// CFX_ShurikenThreadFix CFX_ShurikenThreadFix/<WaitFrame>c__IteratorC::<>f__this
	CFX_ShurikenThreadFix_t333 * ___U3CU3Ef__this_5;
};
