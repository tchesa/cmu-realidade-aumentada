﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Byte>
struct Collection_1_t2712;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t35;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t3629;
// System.Collections.Generic.IList`1<System.Byte>
struct IList_1_t2711;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::.ctor()
extern "C" void Collection_1__ctor_m17263_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17263(__this, method) (( void (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1__ctor_m17263_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17264_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17264(__this, method) (( bool (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17264_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17265_gshared (Collection_1_t2712 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17265(__this, ___array, ___index, method) (( void (*) (Collection_1_t2712 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17265_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17266_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17266(__this, method) (( Object_t * (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17266_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17267_gshared (Collection_1_t2712 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17267(__this, ___value, method) (( int32_t (*) (Collection_1_t2712 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17267_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17268_gshared (Collection_1_t2712 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17268(__this, ___value, method) (( bool (*) (Collection_1_t2712 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17268_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17269_gshared (Collection_1_t2712 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17269(__this, ___value, method) (( int32_t (*) (Collection_1_t2712 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17269_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17270_gshared (Collection_1_t2712 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17270(__this, ___index, ___value, method) (( void (*) (Collection_1_t2712 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17270_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17271_gshared (Collection_1_t2712 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17271(__this, ___value, method) (( void (*) (Collection_1_t2712 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17271_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17272_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17272(__this, method) (( bool (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17272_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17273_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17273(__this, method) (( Object_t * (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17273_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17274_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17274(__this, method) (( bool (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17274_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17275_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17275(__this, method) (( bool (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17275_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17276_gshared (Collection_1_t2712 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17276(__this, ___index, method) (( Object_t * (*) (Collection_1_t2712 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17276_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17277_gshared (Collection_1_t2712 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17277(__this, ___index, ___value, method) (( void (*) (Collection_1_t2712 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17277_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::Add(T)
extern "C" void Collection_1_Add_m17278_gshared (Collection_1_t2712 * __this, uint8_t ___item, const MethodInfo* method);
#define Collection_1_Add_m17278(__this, ___item, method) (( void (*) (Collection_1_t2712 *, uint8_t, const MethodInfo*))Collection_1_Add_m17278_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::Clear()
extern "C" void Collection_1_Clear_m17279_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17279(__this, method) (( void (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_Clear_m17279_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::ClearItems()
extern "C" void Collection_1_ClearItems_m17280_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17280(__this, method) (( void (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_ClearItems_m17280_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::Contains(T)
extern "C" bool Collection_1_Contains_m17281_gshared (Collection_1_t2712 * __this, uint8_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m17281(__this, ___item, method) (( bool (*) (Collection_1_t2712 *, uint8_t, const MethodInfo*))Collection_1_Contains_m17281_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17282_gshared (Collection_1_t2712 * __this, ByteU5BU5D_t119* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17282(__this, ___array, ___index, method) (( void (*) (Collection_1_t2712 *, ByteU5BU5D_t119*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17282_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Byte>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17283_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17283(__this, method) (( Object_t* (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_GetEnumerator_m17283_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Byte>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17284_gshared (Collection_1_t2712 * __this, uint8_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17284(__this, ___item, method) (( int32_t (*) (Collection_1_t2712 *, uint8_t, const MethodInfo*))Collection_1_IndexOf_m17284_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17285_gshared (Collection_1_t2712 * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m17285(__this, ___index, ___item, method) (( void (*) (Collection_1_t2712 *, int32_t, uint8_t, const MethodInfo*))Collection_1_Insert_m17285_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17286_gshared (Collection_1_t2712 * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17286(__this, ___index, ___item, method) (( void (*) (Collection_1_t2712 *, int32_t, uint8_t, const MethodInfo*))Collection_1_InsertItem_m17286_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::Remove(T)
extern "C" bool Collection_1_Remove_m17287_gshared (Collection_1_t2712 * __this, uint8_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m17287(__this, ___item, method) (( bool (*) (Collection_1_t2712 *, uint8_t, const MethodInfo*))Collection_1_Remove_m17287_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17288_gshared (Collection_1_t2712 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17288(__this, ___index, method) (( void (*) (Collection_1_t2712 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17288_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17289_gshared (Collection_1_t2712 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17289(__this, ___index, method) (( void (*) (Collection_1_t2712 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17289_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Byte>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17290_gshared (Collection_1_t2712 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17290(__this, method) (( int32_t (*) (Collection_1_t2712 *, const MethodInfo*))Collection_1_get_Count_m17290_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Byte>::get_Item(System.Int32)
extern "C" uint8_t Collection_1_get_Item_m17291_gshared (Collection_1_t2712 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17291(__this, ___index, method) (( uint8_t (*) (Collection_1_t2712 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17291_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17292_gshared (Collection_1_t2712 * __this, int32_t ___index, uint8_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17292(__this, ___index, ___value, method) (( void (*) (Collection_1_t2712 *, int32_t, uint8_t, const MethodInfo*))Collection_1_set_Item_m17292_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17293_gshared (Collection_1_t2712 * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17293(__this, ___index, ___item, method) (( void (*) (Collection_1_t2712 *, int32_t, uint8_t, const MethodInfo*))Collection_1_SetItem_m17293_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17294_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17294(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17294_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Byte>::ConvertItem(System.Object)
extern "C" uint8_t Collection_1_ConvertItem_m17295_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17295(__this /* static, unused */, ___item, method) (( uint8_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17295_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Byte>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17296_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17296(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17296_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17297_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17297(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17297_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Byte>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17298_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17298(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17298_gshared)(__this /* static, unused */, ___list, method)
