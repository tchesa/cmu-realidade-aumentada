﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t2170;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t2167;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator__ctor_m13187 (ContextLevelActivator_t2170 * __this, Object_t * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
