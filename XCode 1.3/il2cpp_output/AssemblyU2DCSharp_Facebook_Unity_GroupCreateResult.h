﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase.h"

// Facebook.Unity.GroupCreateResult
struct  GroupCreateResult_t280  : public ResultBase_t275
{
	// System.String Facebook.Unity.GroupCreateResult::<GroupId>k__BackingField
	String_t* ___U3CGroupIdU3Ek__BackingField_5;
};
