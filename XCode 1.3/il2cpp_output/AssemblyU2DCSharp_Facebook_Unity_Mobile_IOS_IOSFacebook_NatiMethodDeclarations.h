﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict
struct NativeDict_t259;
// System.String[]
struct StringU5BU5D_t260;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::.ctor()
extern "C" void NativeDict__ctor_m1443 (NativeDict_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::get_NumEntries()
extern "C" int32_t NativeDict_get_NumEntries_m1444 (NativeDict_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::set_NumEntries(System.Int32)
extern "C" void NativeDict_set_NumEntries_m1445 (NativeDict_t259 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::get_Keys()
extern "C" StringU5BU5D_t260* NativeDict_get_Keys_m1446 (NativeDict_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::set_Keys(System.String[])
extern "C" void NativeDict_set_Keys_m1447 (NativeDict_t259 * __this, StringU5BU5D_t260* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::get_Values()
extern "C" StringU5BU5D_t260* NativeDict_get_Values_m1448 (NativeDict_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::set_Values(System.String[])
extern "C" void NativeDict_set_Values_m1449 (NativeDict_t259 * __this, StringU5BU5D_t260* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
