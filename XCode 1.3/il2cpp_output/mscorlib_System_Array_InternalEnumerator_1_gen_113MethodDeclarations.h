﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m29522(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3530 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15544_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m29523(__this, method) (( void (*) (InternalEnumerator_1_t3530 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15545_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Globalization.Calendar>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29524(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3530 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15546_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::Dispose()
#define InternalEnumerator_1_Dispose_m29525(__this, method) (( void (*) (InternalEnumerator_1_t3530 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15547_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.Calendar>::MoveNext()
#define InternalEnumerator_1_MoveNext_m29526(__this, method) (( bool (*) (InternalEnumerator_1_t3530 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15548_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Globalization.Calendar>::get_Current()
#define InternalEnumerator_1_get_Current_m29527(__this, method) (( Calendar_t1988 * (*) (InternalEnumerator_1_t3530 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15549_gshared)(__this, method)
