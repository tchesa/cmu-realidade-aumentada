﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_SpawnSystem
struct CFX_SpawnSystem_t334;
// UnityEngine.GameObject
struct GameObject_t27;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_SpawnSystem::.ctor()
extern "C" void CFX_SpawnSystem__ctor_m1792 (CFX_SpawnSystem_t334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX_SpawnSystem::GetNextObject(UnityEngine.GameObject,System.Boolean)
extern "C" GameObject_t27 * CFX_SpawnSystem_GetNextObject_m1793 (Object_t * __this /* static, unused */, GameObject_t27 * ___sourceObj, bool ___activateObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::PreloadObject(UnityEngine.GameObject,System.Int32)
extern "C" void CFX_SpawnSystem_PreloadObject_m1794 (Object_t * __this /* static, unused */, GameObject_t27 * ___sourceObj, int32_t ___poolSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::UnloadObjects(UnityEngine.GameObject)
extern "C" void CFX_SpawnSystem_UnloadObjects_m1795 (Object_t * __this /* static, unused */, GameObject_t27 * ___sourceObj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_SpawnSystem::get_AllObjectsLoaded()
extern "C" bool CFX_SpawnSystem_get_AllObjectsLoaded_m1796 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::addObjectToPool(UnityEngine.GameObject,System.Int32)
extern "C" void CFX_SpawnSystem_addObjectToPool_m1797 (CFX_SpawnSystem_t334 * __this, GameObject_t27 * ___sourceObject, int32_t ___number, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::removeObjectsFromPool(UnityEngine.GameObject)
extern "C" void CFX_SpawnSystem_removeObjectsFromPool_m1798 (CFX_SpawnSystem_t334 * __this, GameObject_t27 * ___sourceObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::Awake()
extern "C" void CFX_SpawnSystem_Awake_m1799 (CFX_SpawnSystem_t334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::Start()
extern "C" void CFX_SpawnSystem_Start_m1800 (CFX_SpawnSystem_t334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
