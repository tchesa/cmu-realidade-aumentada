﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNotificationDeviceToken
struct IOSNotificationDeviceToken_t143;
// System.Byte[]
struct ByteU5BU5D_t119;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNotificationDeviceToken::.ctor(System.Byte[])
extern "C" void IOSNotificationDeviceToken__ctor_m811 (IOSNotificationDeviceToken_t143 * __this, ByteU5BU5D_t119* ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSNotificationDeviceToken::get_tokenString()
extern "C" String_t* IOSNotificationDeviceToken_get_tokenString_m812 (IOSNotificationDeviceToken_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] IOSNotificationDeviceToken::get_tokenBytes()
extern "C" ByteU5BU5D_t119* IOSNotificationDeviceToken_get_tokenBytes_m813 (IOSNotificationDeviceToken_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
