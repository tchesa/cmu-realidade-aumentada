﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.FBJavaClass
struct FBJavaClass_t257;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t34;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Mobile.Android.FBJavaClass::.ctor()
extern "C" void FBJavaClass__ctor_m1441 (FBJavaClass_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.Android.FBJavaClass::CallStatic(System.String,System.Object[])
extern "C" void FBJavaClass_CallStatic_m1442 (FBJavaClass_t257 * __this, String_t* ___methodName, ObjectU5BU5D_t34* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
