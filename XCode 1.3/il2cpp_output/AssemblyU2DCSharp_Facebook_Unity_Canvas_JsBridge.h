﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler
struct ICanvasFacebookCallbackHandler_t233;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Facebook.Unity.Canvas.JsBridge
struct  JsBridge_t232  : public MonoBehaviour_t18
{
	// Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler Facebook.Unity.Canvas.JsBridge::facebook
	Object_t * ___facebook_1;
};
