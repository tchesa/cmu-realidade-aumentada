﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIStyle
struct GUIStyle_t184;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// BaseIOSFeaturePreview
struct  BaseIOSFeaturePreview_t183  : public MonoBehaviour_t18
{
	// UnityEngine.GUIStyle BaseIOSFeaturePreview::style
	GUIStyle_t184 * ___style_1;
	// System.Int32 BaseIOSFeaturePreview::buttonWidth
	int32_t ___buttonWidth_2;
	// System.Int32 BaseIOSFeaturePreview::buttonHeight
	int32_t ___buttonHeight_3;
	// System.Single BaseIOSFeaturePreview::StartY
	float ___StartY_4;
	// System.Single BaseIOSFeaturePreview::StartX
	float ___StartX_5;
	// System.Single BaseIOSFeaturePreview::XStartPos
	float ___XStartPos_6;
	// System.Single BaseIOSFeaturePreview::YStartPos
	float ___YStartPos_7;
	// System.Single BaseIOSFeaturePreview::XButtonStep
	float ___XButtonStep_8;
	// System.Single BaseIOSFeaturePreview::YButtonStep
	float ___YButtonStep_9;
	// System.Single BaseIOSFeaturePreview::YLableStep
	float ___YLableStep_10;
};
