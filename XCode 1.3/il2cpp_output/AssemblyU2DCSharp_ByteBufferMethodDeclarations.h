﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ByteBuffer
struct ByteBuffer_t194;
// System.Byte[]
struct ByteU5BU5D_t119;

#include "codegen/il2cpp-codegen.h"

// System.Void ByteBuffer::.ctor(System.Byte[])
extern "C" void ByteBuffer__ctor_m1101 (ByteBuffer_t194 * __this, ByteU5BU5D_t119* ___buf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ByteBuffer::readInt()
extern "C" int32_t ByteBuffer_readInt_m1102 (ByteBuffer_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ByteBuffer::readFloat()
extern "C" float ByteBuffer_readFloat_m1103 (ByteBuffer_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
