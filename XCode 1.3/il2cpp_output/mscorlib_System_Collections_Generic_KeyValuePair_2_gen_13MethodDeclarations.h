﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m16721(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2681 *, int32_t, List_1_t461 *, const MethodInfo*))KeyValuePair_2__ctor_m16393_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::get_Key()
#define KeyValuePair_2_get_Key_m16722(__this, method) (( int32_t (*) (KeyValuePair_2_t2681 *, const MethodInfo*))KeyValuePair_2_get_Key_m16394_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16723(__this, ___value, method) (( void (*) (KeyValuePair_2_t2681 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16395_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::get_Value()
#define KeyValuePair_2_get_Value_m16724(__this, method) (( List_1_t461 * (*) (KeyValuePair_2_t2681 *, const MethodInfo*))KeyValuePair_2_get_Value_m16396_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16725(__this, ___value, method) (( void (*) (KeyValuePair_2_t2681 *, List_1_t461 *, const MethodInfo*))KeyValuePair_2_set_Value_m16397_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>::ToString()
#define KeyValuePair_2_ToString_m16726(__this, method) (( String_t* (*) (KeyValuePair_2_t2681 *, const MethodInfo*))KeyValuePair_2_ToString_m16398_gshared)(__this, method)
