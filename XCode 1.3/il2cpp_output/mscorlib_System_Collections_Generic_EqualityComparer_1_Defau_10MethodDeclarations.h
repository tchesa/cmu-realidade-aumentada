﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>
struct DefaultComparer_t3305;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::.ctor()
extern "C" void DefaultComparer__ctor_m26739_gshared (DefaultComparer_t3305 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m26739(__this, method) (( void (*) (DefaultComparer_t3305 *, const MethodInfo*))DefaultComparer__ctor_m26739_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26740_gshared (DefaultComparer_t3305 * __this, ProfileData_t1064  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m26740(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3305 *, ProfileData_t1064 , const MethodInfo*))DefaultComparer_GetHashCode_m26740_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26741_gshared (DefaultComparer_t3305 * __this, ProfileData_t1064  ___x, ProfileData_t1064  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m26741(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3305 *, ProfileData_t1064 , ProfileData_t1064 , const MethodInfo*))DefaultComparer_Equals_m26741_gshared)(__this, ___x, ___y, method)
