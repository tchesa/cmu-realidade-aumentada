﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_46.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23742_gshared (InternalEnumerator_1_t3120 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23742(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3120 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23742_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23743_gshared (InternalEnumerator_1_t3120 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23743(__this, method) (( void (*) (InternalEnumerator_1_t3120 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23743_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23744_gshared (InternalEnumerator_1_t3120 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23744(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3120 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23744_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23745_gshared (InternalEnumerator_1_t3120 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23745(__this, method) (( void (*) (InternalEnumerator_1_t3120 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23745_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23746_gshared (InternalEnumerator_1_t3120 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23746(__this, method) (( bool (*) (InternalEnumerator_1_t3120 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23746_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t969  InternalEnumerator_1_get_Current_m23747_gshared (InternalEnumerator_1_t3120 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23747(__this, method) (( TrackableResultData_t969  (*) (InternalEnumerator_1_t3120 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23747_gshared)(__this, method)
