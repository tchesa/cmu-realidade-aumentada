﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m27413(__this, ___t, method) (( void (*) (Enumerator_t3349 *, Stack_1_t1448 *, const MethodInfo*))Enumerator__ctor_m19757_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m27414(__this, method) (( void (*) (Enumerator_t3349 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m19758_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27415(__this, method) (( Object_t * (*) (Enumerator_t3349 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19759_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m27416(__this, method) (( void (*) (Enumerator_t3349 *, const MethodInfo*))Enumerator_Dispose_m19760_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m27417(__this, method) (( bool (*) (Enumerator_t3349 *, const MethodInfo*))Enumerator_MoveNext_m19761_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m27418(__this, method) (( Type_t * (*) (Enumerator_t3349 *, const MethodInfo*))Enumerator_get_Current_m19762_gshared)(__this, method)
