﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.EditorFacebookLoader
struct EditorFacebookLoader_t268;
// Facebook.Unity.FacebookGameObject
struct FacebookGameObject_t229;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Editor.EditorFacebookLoader::.ctor()
extern "C" void EditorFacebookLoader__ctor_m1549 (EditorFacebookLoader_t268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.FacebookGameObject Facebook.Unity.Editor.EditorFacebookLoader::get_FBGameObject()
extern "C" FacebookGameObject_t229 * EditorFacebookLoader_get_FBGameObject_m1550 (EditorFacebookLoader_t268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
