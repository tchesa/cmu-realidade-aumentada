﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativeUtility
struct IOSNativeUtility_t83;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNativeUtility::.ctor()
extern "C" void IOSNativeUtility__ctor_m534 (IOSNativeUtility_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_RedirectToAppStoreRatingPage(System.String)
extern "C" void IOSNativeUtility__ISN_RedirectToAppStoreRatingPage_m535 (Object_t * __this /* static, unused */, String_t* ___appId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_ShowPreloader()
extern "C" void IOSNativeUtility__ISN_ShowPreloader_m536 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_HidePreloader()
extern "C" void IOSNativeUtility__ISN_HidePreloader_m537 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::RedirectToAppStoreRatingPage()
extern "C" void IOSNativeUtility_RedirectToAppStoreRatingPage_m538 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::RedirectToAppStoreRatingPage(System.String)
extern "C" void IOSNativeUtility_RedirectToAppStoreRatingPage_m539 (Object_t * __this /* static, unused */, String_t* ___appleId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::ShowPreloader()
extern "C" void IOSNativeUtility_ShowPreloader_m540 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::HidePreloader()
extern "C" void IOSNativeUtility_HidePreloader_m541 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
