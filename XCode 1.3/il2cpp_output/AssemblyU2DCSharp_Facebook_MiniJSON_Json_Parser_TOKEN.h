﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_Facebook_MiniJSON_Json_Parser_TOKEN.h"

// Facebook.MiniJSON.Json/Parser/TOKEN
struct  TOKEN_t301 
{
	// System.Int32 Facebook.MiniJSON.Json/Parser/TOKEN::value__
	int32_t ___value___1;
};
