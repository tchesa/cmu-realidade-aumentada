﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t81;
// IOSNativeAppEvents
struct IOSNativeAppEvents_t80;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher.h"

// IOSNativeAppEvents
struct  IOSNativeAppEvents_t80  : public EventDispatcher_t69
{
	// System.Action IOSNativeAppEvents::OnApplicationDidEnterBackground
	Action_t81 * ___OnApplicationDidEnterBackground_8;
	// System.Action IOSNativeAppEvents::OnApplicationDidBecomeActive
	Action_t81 * ___OnApplicationDidBecomeActive_9;
	// System.Action IOSNativeAppEvents::OnApplicationDidReceiveMemoryWarning
	Action_t81 * ___OnApplicationDidReceiveMemoryWarning_10;
	// System.Action IOSNativeAppEvents::OnApplicationWillResignActive
	Action_t81 * ___OnApplicationWillResignActive_11;
	// System.Action IOSNativeAppEvents::OnApplicationWillTerminate
	Action_t81 * ___OnApplicationWillTerminate_12;
};
struct IOSNativeAppEvents_t80_StaticFields{
	// IOSNativeAppEvents IOSNativeAppEvents::_instance
	IOSNativeAppEvents_t80 * ____instance_13;
	// System.Action IOSNativeAppEvents::<>f__am$cache6
	Action_t81 * ___U3CU3Ef__amU24cache6_14;
	// System.Action IOSNativeAppEvents::<>f__am$cache7
	Action_t81 * ___U3CU3Ef__amU24cache7_15;
	// System.Action IOSNativeAppEvents::<>f__am$cache8
	Action_t81 * ___U3CU3Ef__amU24cache8_16;
	// System.Action IOSNativeAppEvents::<>f__am$cache9
	Action_t81 * ___U3CU3Ef__amU24cache9_17;
	// System.Action IOSNativeAppEvents::<>f__am$cacheA
	Action_t81 * ___U3CU3Ef__amU24cacheA_18;
};
