﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m20355(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2904 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15635_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Transform>::Invoke(T)
#define Predicate_1_Invoke_m20356(__this, ___obj, method) (( bool (*) (Predicate_1_t2904 *, Transform_t25 *, const MethodInfo*))Predicate_1_Invoke_m15636_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Transform>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m20357(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2904 *, Transform_t25 *, AsyncCallback_t12 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15637_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m20358(__this, ___result, method) (( bool (*) (Predicate_1_t2904 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15638_gshared)(__this, ___result, method)
