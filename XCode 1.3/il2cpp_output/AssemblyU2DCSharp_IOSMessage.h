﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t81;

#include "AssemblyU2DCSharp_BaseIOSPopup.h"

// IOSMessage
struct  IOSMessage_t166  : public BaseIOSPopup_t164
{
	// System.String IOSMessage::ok
	String_t* ___ok_5;
	// System.Action IOSMessage::OnComplete
	Action_t81 * ___OnComplete_6;
};
struct IOSMessage_t166_StaticFields{
	// System.Action IOSMessage::<>f__am$cache2
	Action_t81 * ___U3CU3Ef__amU24cache2_7;
};
