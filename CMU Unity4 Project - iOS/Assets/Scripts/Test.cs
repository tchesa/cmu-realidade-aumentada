﻿using UnityEngine;
using System;
using System.Collections;

public class Test : MonoBehaviour {

    private void Start()
    {
        Request(12, Callback);
    }

    public void Request(int param, Action<int> callback = null)
    { 
        // do things

        if (callback != null) callback(param);
    }

    public void Callback(int i)
    {
        print(string.Format("The callback reurned {0}", i));
    }
}
