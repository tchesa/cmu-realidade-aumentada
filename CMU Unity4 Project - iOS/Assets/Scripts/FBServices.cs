﻿/// <summary>
/// Author: Norton Pigozzo
/// Date: 05/28/2014
/// Description: Implement a few basic Facebook SDK operations, use it as start point for your application
/// Requirements: JSON.cs included in the package
/// </summary>

#define FACEBOOK

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
#if FACEBOOK
using Facebook.Unity;
#endif

public class FBServices : MonoBehaviour {
#if FACEBOOK
	#region SINGLETON
	private static FBServices current;

	public static FBServices Current {
		get {
            if (!current) current = new GameObject("FBServices").AddComponent<FBServices>();

			return current;
		}
	}

	private void Instantiate(){
		if(FBServices.current == null){
			current = this;
		}else{
			Destroy(this);
		}
	}
	#endregion //SINGLETON

	//Facebook requested permissions:
	//publish_actions, public_profile, email
	#region PRIVATE_VARIABLES
	//private const string requested_permissions = "public_profile, email, publish_actions";
	private List<string> requested_permissions;
	//Facebook Variables
	private bool isInit = false;
//	private string apiQuery = "";
	#endregion //PRIVATE_VARIABLES

	void Awake(){
		Instantiate();

		requested_permissions = new List<string> { "public_profile", "email", "publish_actions" };
	}

	#region PUBLIC_METHODS
	public void Init(){
        if(!FB.IsInitialized) FB.Init(Logout);
        //if (!FB.IsInitialized) FB.Init(null, OnHideUnity);
	}

	private void OnHideUnity(){ }

	public void Login(Action<bool, ILoginResult> callback = null){
        /*FB.Login(
                requested_permissions,
                (FBResult result) =>
                {
                    if (result.Error != null)
                    {
                        //Feed();
                        callback.Invoke(true, result);
                    }
                    else
                    {
                        callback.Invoke(false, result);
                    }
                }
            );*/

		FB.LogInWithPublishPermissions (requested_permissions, (ILoginResult result) => callback.Invoke (FB.IsLoggedIn, result));
	}

	private void  LoginCallback(ILoginResult result)
	{

	}

    public void Logout()
    {
        FB.LogOut();
        ArUiController.Instance.Share_OnClick();
    }

	#endregion //PUBLIC_METHODS

    #region Feed
	/*
    private void Feed()
    {

        Dictionary<string, string[]> feedProperties = null;
        FB.Feed(
            toId: "",
            //link: "https://play.google.com/store/apps/details?id=com.Meatballs.CataMoeda",
            link: "http://www.google.com/",
            //linkName: "Eba! Esses são os meus pontos no CATA MOEDA do Banco do Brasil.",
            linkName: "Linkname",
            linkCaption: "Linkcaption",
            linkDescription: "Link Description.",
            picture: "http://www.denubila.com.br/aprovacao/cata_moeda/imagem_facebook_cata_moeda.png",
            mediaSource: "",
            actionName: "",
            actionLink: "",
            reference: "",
            properties: feedProperties,
            callback: FeedCallback
        );

		FB.FeedShare(
			link: "https://example.com/myapp/?storyID=thelarch",
			linkName: "The Larch",
			linkCaption: "I thought up a witty tagline about larches",
			linkDescription: "There are a lot of larch trees around here, aren't there?",
			picture: "https://example.com/myapp/assets/1/larch.jpg",
			callback: FeedCallback
			);
    }

    private void FeedCallback(IShareResult result)
    {

    }*/
    #endregion

    #region API

    public void API(byte[] image, FacebookDelegate<IGraphResult> callback = null)
    {
        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", image, "testeImage.png");
        wwwForm.AddField("message", "");

        FB.API("me/photos", HttpMethod.POST, callback, wwwForm);
    }

    #endregion

	private void OnGUI()
    {
        /*
        if (GUI.Button(new Rect(10f, 40f, 100f, 100f), "Init")) FB.Init(null, OnHideUnity);

        if (GUI.Button(new Rect(120f, 40f, 100f, 100f), "Share")) Share();

        if (GUI.Button(new Rect(230f, 40f, 100f, 100f), "Log In")) FB.Login(
                requested_permissions,
                (FBResult result) =>
                {
                    if (result.Error != null)
                    {
                        Feed();
                    }

                    else if (!FB.IsLoggedIn)
                    {

                    }
                    else
                    {

                    }
                }
            ); ;

        if (GUI.Button(new Rect(340f, 40f, 100f, 100f), "Logoff")) FB.Logout();

        if (GUI.Button(new Rect(450f, 40f, 100f, 100f), "Reload")) Application.LoadLevel(Application.loadedLevel);

        GUI.Box(new Rect(10f, 150f, 150f, 20f), "");
        GUI.Label(new Rect(20f, 150f, 300f, 20f), "Is Initialized: " + FB.IsInitialized.ToString());
        GUI.Box(new Rect(10f, 180f, 150f, 20f), "");
        GUI.Label(new Rect(20f, 180f, 300f, 20f), "Is logged in: " + FB.IsLoggedIn);*/
    }
#endif
}