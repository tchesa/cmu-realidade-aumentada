﻿using UnityEngine;
using System.Collections;

public class SinoidalAnimation : MonoBehaviour {

    public float range;
    public float speed = 1f;

    private Vector3 initialPosition;

    private void Start()
    {
        initialPosition = transform.localPosition;
    }

    private void Update()
    {
        float sinPos = Mathf.Sin(Time.time * speed) * range;
        //transform.position = new Vector3(initialPosition.x, initialPosition.y + sinPos, initialPosition.z);
        transform.localPosition = initialPosition;
        transform.Translate(0f, sinPos, 0f, Space.Self);
    }
}
