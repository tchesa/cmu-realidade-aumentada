﻿using UnityEngine;
using UnityEngine.UI;

public class ShowHideSprite : MonoBehaviour {

    public bool showing;
    public float showSpeed;

    private Color color, transparent;
    private Image image;

    public void Show()
    {
        showing = true;
    }

    public void Hide()
    {
        showing = false;
    }

    private void Start()
    {
        image = GetComponent<Image>();
        color = image.color;
        transparent = new Color(color.r, color.g, color.b, 0f);
    }

    private void Update()
    {
        if(showing && image.color != color)
        {
            image.color = Color.Lerp(image.color, color, Time.deltaTime * showSpeed);
        }
        else if (!showing && image.color != transparent)
        {
            image.color = Color.Lerp(image.color, transparent, Time.deltaTime * showSpeed);
        }
    }
}
