﻿using UnityEngine;
using System.Collections;

public class RotateScale : MonoBehaviour
{
    public Vector3 rotation;
    public float scale;

    private Vector3 _rotation;
    private float _scale;
    private bool isPlaying;

    private void Awake()
    {
        _rotation = transform.localEulerAngles;
        _scale = transform.localScale.x;
    }

    private void Update()
    {
        if (isPlaying)
        {
            //transform.localEulerAngles += rotation * Time.deltaTime;
            transform.Rotate(rotation * Time.deltaTime);
            transform.localScale += Vector3.one * scale * Time.deltaTime;
        }
    }

    public void Play()
    {
        transform.localEulerAngles = _rotation;
        transform.localScale = Vector3.one * _scale;
        isPlaying = true;
    }

    public void Stop()
    {
        isPlaying = false;
        transform.localEulerAngles = _rotation;
        transform.localScale = Vector3.one * _scale;
    }
}
