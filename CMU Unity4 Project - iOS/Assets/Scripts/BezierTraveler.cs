﻿using UnityEngine;
using System.Collections;

public class BezierTraveler : MonoBehaviour
{

    public Bezier[] beziers;
    public float speed = 1f;
    private float progress = 0f;
    public bool teste = false;
    public bool _isPlaying;

    public float normalizedProgress
    {
        get
        {
            return progress / beziers.Length;
        }
    }

    public bool isPlaying
    {
        get
        {
            return progress < 1f * beziers.Length;
        }
    }

    private void Update()
    {
        if (teste)
        {
            teste = false;
            progress = 0f;
        }

        _isPlaying = isPlaying;

        if (progress < 1f * beziers.Length)
        {
            transform.position = beziers[Mathf.FloorToInt(progress)].GetBezierPosition(progress - Mathf.Floor(progress));
            float _progress = Mathf.Clamp(progress + 0.1f, 0f, 1f * beziers.Length);
            if (_progress < 1f * beziers.Length)
            {
                transform.LookAt(beziers[Mathf.FloorToInt(_progress)].GetBezierPosition(_progress - Mathf.Floor(_progress)));
            }
            progress += Time.deltaTime * speed;
        }
    }

    public void Play()
    {
        progress = 0f;
    }
}
