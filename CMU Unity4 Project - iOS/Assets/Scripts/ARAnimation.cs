﻿using UnityEngine;
using System.Collections.Generic;

public class ARAnimation : MonoBehaviour {

    [SerializeField] private TransparencyTrigger[] showingObjects;
    private List<TransparencyTrigger> showingList, hidingList;

    [SerializeField] private TrailTrigger[] trails;
    private List<TrailTrigger> trailList = new List<TrailTrigger>();

    [SerializeField]
    private ShineTrigger[] shines;
    private List<ShineTrigger> shineList = new List<ShineTrigger>();

    private bool isPlaying;
    private float startTime;

    public bool play, stop;

    private void Update()
    {
        if (play)
        {
            play = false;
            Play();
        }

        if (stop)
        {
            stop = false;
            Stop();
        }

        if (isPlaying)
        {
            if (showingList.Count == 0 && hidingList.Count == 0 && trailList.Count == 0 && shineList.Count == 0) isPlaying = false;
            else
            {
                if (showingList.Count > 0 && Time.time >= startTime + showingList[0].show)
                {
                    do {
                        if (Time.time >= startTime + showingList[0].show)
                        {
                            showingList[0].obj.Show();
                            showingList.RemoveAt(0);
                        }
                    } while (showingList.Count > 0 && Time.time >= startTime + showingList[0].show);
                }

                if (hidingList.Count > 0 && Time.time >= startTime + hidingList[0].hide)
                {
                    do
                    {
                        if (Time.time >= startTime + hidingList[0].hide)
                        {
                            hidingList[0].obj.Hide();
                            hidingList.RemoveAt(0);
                        }
                    } while (hidingList.Count > 0 && Time.time >= startTime + hidingList[0].hide);
                }

                if (trailList.Count > 0 && Time.time >= startTime + trailList[0].turnOn)
                {
                    do
                    {
                        if (Time.time >= startTime + trailList[0].turnOn)
                        {
                            trailList[0].obj.Play();
                            trailList.RemoveAt(0);
                        }
                    } while (trailList.Count > 0 && Time.time >= startTime + trailList[0].turnOn);
                }

                if (shineList.Count > 0 && Time.time >= startTime + shineList[0].turnOn)
                {
                    do
                    {
                        if (Time.time >= startTime + shineList[0].turnOn)
                        {
                            shineList[0].obj.Activate();
                            shineList.RemoveAt(0);
                        }
                    } while (shineList.Count > 0 && Time.time >= startTime + shineList[0].turnOn);
                }
            }
        }
    }

    public void Play()
    {
        isPlaying = true;
        startTime = Time.time;

        showingList = new List<TransparencyTrigger>(showingObjects);
        showingList.Sort((x, y) => x.show.CompareTo(y.show));

        hidingList = new List<TransparencyTrigger>(showingObjects);
        hidingList.Sort((x, y) => x.hide.CompareTo(y.hide));

        trailList = new List<TrailTrigger>(trails);
        trailList.Sort((x, y) => x.turnOn.CompareTo(y.turnOn));

        shineList = new List<ShineTrigger>(shines);
        shineList.Sort((x, y) => x.turnOn.CompareTo(y.turnOn));
    }

    public void Stop()
    {
        isPlaying = false;
        foreach (TransparencyTrigger t in showingObjects) t.obj.Reset();
        foreach (ShineTrigger b in shines) b.obj.activated = false;
    }

    [System.Serializable] private class TransparencyTrigger
    {
        public float show, hide;
        public TransparencyAnimation obj;
    }

    [System.Serializable]
    private class TrailTrigger
    {
        public float turnOn;
        public BezierTraveler obj;
    }

    [System.Serializable]
    private class ShineTrigger
    {
        public float turnOn;
        public BrilhoAnimation obj;
    }
}

