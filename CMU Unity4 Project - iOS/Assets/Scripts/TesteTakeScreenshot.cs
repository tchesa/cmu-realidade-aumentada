﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TesteTakeScreenshot : MonoBehaviour
{

    public int resWidth = 2550; 
    public int resHeight = 3300;
    public int length = 24;

    public bool takeHiResShot = false;
    private string lastTexture;

    public GameObject teste;

    private void Start()
    {
        resWidth = Screen.width;
        resHeight = Screen.height;
    }

    public static string ScreenShotName(int width, int height)
    {
        return string.Format("{0}/screenshots/screen_{1}x{2}_{3}.png",
        Application.dataPath,
        width, height,
        System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    public void TakeHiResShot()
    {
        takeHiResShot = true;
    }

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(ITakeScreenshot());
        }

        takeHiResShot |= Input.GetKeyDown(KeyCode.Space);
        if (takeHiResShot)
        {
            if(!string.IsNullOrEmpty(lastTexture)) File.Delete(lastTexture);

            RenderTexture rt = new RenderTexture(resWidth, resHeight, length);
            GetComponent<Camera>().targetTexture = rt;
            Texture2D screenshot = new Texture2D(resWidth, resHeight, TextureFormat.RGBA4444, false);
            GetComponent<Camera>().Render();
            RenderTexture.active = rt;
            screenshot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            GetComponent<Camera>().targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            Destroy(rt);
            byte[] bytes = screenshot.EncodeToPNG();
            string filename = ScreenShotName(resWidth, resHeight);
            System.IO.File.WriteAllBytes(filename, bytes);
            Debug.Log(string.Format("Took screenshot to: {0}", filename));
            takeHiResShot = false;

            Texture2D tex = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
            tex.LoadImage(File.ReadAllBytes(filename));
            teste.GetComponent<Renderer>().material.mainTexture = tex;

            lastTexture = filename;
        }
    }

    IEnumerator ITakeScreenshot()
    {
        ComponentTracker.Instance.GetObject("Main_Panel").SetActive(false);

        Application.CaptureScreenshot(ScreenShotName(resWidth, resHeight));

        yield return new WaitForEndOfFrame();
        ComponentTracker.Instance.GetObject("Main_Panel").SetActive(true);
    }
}
