﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuController : MonoBehaviour {

	ComponentTracker tracker;

	private void Start()
	{
		tracker = ComponentTracker.Instance;
		
		tracker.GetObject ("Info_Panel").SetActive (false);
	}

	public void Play_OnClick()
	{
		tracker.GetObject ("play_button").SetActive (false);
		Application.LoadLevel (1);
	}

	public void Info_OnClick()
	{
		tracker.GetObject ("Info_Panel").SetActive (true);
	}

	public void CMU_OnClick()
	{
		Application.OpenURL ("http://cmuenergia.com.br/nossaenergiavemdevoce/");
	}

	public void Info_Back_OnClick()
	{
		tracker.GetObject ("Info_Panel").SetActive (false);
	}
}
