﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartUIController : MonoBehaviour {

    public bool info = false;

    private ComponentTracker tracker;
    public Vector3 infoShowedPosition, infoHiddenPosition;
    public float infoSpeed;
    private bool infoDrag = false;

    private void Start()
    {
        tracker = ComponentTracker.Instance;

        tracker.GetObject("Black_Panel").SetActive(false);
    }

    private void Update()
    {
        //Transform infoGroup = tracker.GetObject("InfoGroup").transform;
        ScrollRect infoGroup = tracker.GetElement<ScrollRect>("InfoGroup");
        //RectTransform seta = tracker.GetElement<RectTransform>("SetaButton");

        if (!infoDrag)
        {
            if (!info)
            {
                infoGroup.transform.localPosition = Vector3.Lerp(infoGroup.transform.localPosition, infoHiddenPosition, Time.deltaTime * infoSpeed);
                //seta.localEulerAngles = new Vector3(0f, 0f, Mathf.Lerp(seta.localEulerAngles.z, 0f, Time.deltaTime * infoSpeed));
            }
            else
            {
                infoGroup.transform.localPosition = Vector3.Lerp(infoGroup.transform.localPosition, infoShowedPosition, Time.deltaTime * infoSpeed);
                //seta.localEulerAngles = new Vector3(0f, 0f, Mathf.Lerp(seta.localEulerAngles.z, 180f, Time.deltaTime * infoSpeed));
            }
        }

        if (infoGroup.transform.localPosition.y > infoShowedPosition.y) infoGroup.transform.localPosition = infoShowedPosition;
        if (infoGroup.transform.localPosition.y < infoHiddenPosition.y) infoGroup.transform.localPosition = infoHiddenPosition;
        tracker.GetElement<Text>("comousar_text").text = (info) ? "VOLTAR" : "INFO";
    }

    #region UI Messages
    public void Seta_OnClick()
    {
        info = !info;
    }

    public void InfoGroup_OnBeginDrag()
    {
        infoDrag = true;
    }

    public void InfoGroup_OnEndGrag()
    {
        infoDrag = false;

        if (info && tracker.GetObject("InfoGroup").transform.localPosition.y < -400f) info = false;
        else if (!info && tracker.GetObject("InfoGroup").transform.localPosition.y > -1500f)
        {
            print(tracker.GetObject("InfoGroup").transform.localPosition.y);
            info = true;
        }
    }

    public void Iniciar_OnClick()
    {
        //tracker.GetObject("Black_Panel").SetActive(true);
        //Application.LoadLevel(1);

        StartCoroutine(StartTransiction());
    }

    public void Vimeo_OnClick()
    {
        print("calling Application.OpenURL(\"www.vimeo.com/meatballsbrasil\")");
        Application.OpenURL("https://www.vimeo.com/meatballsbrasil");
    }
    #endregion

    private IEnumerator StartTransiction()
    {
        UnityEngine.UI.Image black = tracker.GetElement<UnityEngine.UI.Image>("Black_Panel");
        black.gameObject.SetActive(true);
        black.color = new Color(0f, 0f, 0f, 0f);
        for (float i = 0f; i < 1f; i += Time.deltaTime)
        {
            tracker.GetElement<UnityEngine.UI.Image>("Black_Panel").color = new Color(0f, 0f, 0f, i * 0.63f);
            yield return new WaitForEndOfFrame();
        }

        Application.LoadLevel(1);
    }

    public void test()
    {
        print(tracker.GetObject("Iniciar_Text").transform.localScale);
    }
}