﻿using UnityEngine;
using System.Collections;

public class TransparencyAnimation : MonoBehaviour {

    private Shader[] shaders;

    public bool show, hide;
    public float fade = 1f;
    private bool showing, hiding;
    private float startTime;

    public RotateScale rotateScale;

    private void Start()
    {
        shaders = new Shader[GetComponent<Renderer>().materials.Length];
        for (int i = 0; i < GetComponent<Renderer>().materials.Length; i++)
        {
            shaders[i] = GetComponent<Renderer>().materials[i].shader;
        }

        Reset();
    }

    private void Update()
    { 
        if(show)
        {
            show = false;
            Show();
        }

        if(hide)
        {
            hide = false;
            Hide();
        }

        if (showing)
        {
            if (Time.time - startTime <= fade)
            {
                foreach (Material m in GetComponent<Renderer>().materials)
                {
                    m.color = new Color(m.color.r, m.color.g, m.color.b, (Time.time - startTime) / fade);
                }
            }
            else
            {
                for (int i = 0; i < GetComponent<Renderer>().materials.Length; i++)
                {
                    GetComponent<Renderer>().materials[i].color = new Color(GetComponent<Renderer>().materials[i].color.r, GetComponent<Renderer>().materials[i].color.g, GetComponent<Renderer>().materials[i].color.b, 1f);
                    GetComponent<Renderer>().materials[i].shader = shaders[i];
                }

                showing = false;
            }
        }

        if (hiding)
        {
            if (Time.time - startTime <= fade)
            {
                foreach (Material m in GetComponent<Renderer>().materials)
                {
                    m.color = new Color(m.color.r, m.color.g, m.color.b, 1f - ((Time.time - startTime) / fade));
                }
            }
            else
            {
                for (int i = 0; i < GetComponent<Renderer>().materials.Length; i++)
                {
                    GetComponent<Renderer>().materials[i].color = new Color(GetComponent<Renderer>().materials[i].color.r, GetComponent<Renderer>().materials[i].color.g, GetComponent<Renderer>().materials[i].color.b, 0f);
                }

                if (rotateScale) rotateScale.Stop();
                hiding = false;
            }
        }
    }

    public void Show()
    {
        foreach (Material m in GetComponent<Renderer>().materials)
        {
            m.shader = Shader.Find("Transparent/Diffuse");
            m.color = new Color(m.color.r, m.color.g, m.color.b, 0f);
        }

        hiding = false;
        showing = true;
        startTime = Time.time;
        if (rotateScale) rotateScale.Play();
    }

    public void Hide()
    {
        foreach (Material m in GetComponent<Renderer>().materials)
        {
            m.shader = Shader.Find("Transparent/Diffuse");
            m.color = new Color(m.color.r, m.color.g, m.color.b, 1f);
        }

        showing = false;
        hiding = true;
        startTime = Time.time;
    }

    public void Reset()
    {
        showing = false;
        hiding = false;

        foreach (Material m in GetComponent<Renderer>().materials)
        {
            m.shader = Shader.Find("Transparent/Diffuse");
            m.color = new Color(m.color.r, m.color.g, m.color.b, 0f);
        }

        if (rotateScale) rotateScale.Stop();
    }
}
