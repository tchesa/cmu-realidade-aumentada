﻿using UnityEngine;
using System.Collections;

public class BrilhoAnimation : MonoBehaviour {

    private Material mat;

    public float fade = 1f, delay = 1f;
    public bool activated = false;
    private float maxAlpha;
    private bool _activated = false;
    private float startedTime;

    private void Start()
    {
        mat = GetComponent<Renderer>().material;
        maxAlpha = mat.GetColor("_TintColor").a;
        SetColorByAlpha(0f);
    }

    private void Update()
    {
        if (activated != _activated)
        {
            _activated = activated;
            SetColorByAlpha(0f);
            if(activated) startedTime = Time.time;
        }

        if (_activated)
        {
            if (Time.time - startedTime < fade) SetColorByAlpha(((Time.time - startedTime) / fade) * maxAlpha);
            else if (Time.time - startedTime < fade + delay) SetColorByAlpha(maxAlpha);
            else if (Time.time - startedTime < fade * 2f + delay) SetColorByAlpha((1f - ((Time.time - startedTime - delay - fade) / fade)) * maxAlpha);
            else activated = false;
        }
    }

    public void Activate()
    {
        activated = true;
    }

    private void SetColorByAlpha(float alpha)
    {
        mat.SetColor("_TintColor", new Color(mat.GetColor("_TintColor").r, mat.GetColor("_TintColor").g, mat.GetColor("_TintColor").b, alpha));
    }
}
