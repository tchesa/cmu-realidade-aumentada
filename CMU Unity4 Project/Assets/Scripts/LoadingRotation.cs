﻿using UnityEngine;
using System.Collections;

public class LoadingRotation : MonoBehaviour {

    public float rotationStep, speed = 1f;
    private float realRotation = 0f;

    private void Update()
    {
        realRotation += Time.deltaTime * speed * rotationStep;
        if (realRotation > 360f) realRotation -= 360f;

        transform.eulerAngles = new Vector3(0f, 0f, 360f - (int)(realRotation / rotationStep) * rotationStep);
    }
}
