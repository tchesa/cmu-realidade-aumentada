﻿using UnityEngine;
using System.Collections;

public class Bezier : MonoBehaviour {
    
    public int iterations;
    public Transform p1, p2, p3, p4;

    public Vector3 GetBezierPosition(float progress)
    {
        if (progress < 0 && progress > 1f)
        {
            print("error");
            return Vector3.zero;
        }
        else
        {
            float x = progress;

            Vector3 ap1 = Vector3.Lerp(p1.position, p2.position, x);
            Vector3 ap2 = Vector3.Lerp(p2.position, p3.position, x);
            Vector3 ap3 = Vector3.Lerp(p3.position, p4.position, x);

            Vector3 bp1 = Vector3.Lerp(ap1, ap2, x);
            Vector3 bp2 = Vector3.Lerp(ap2, ap3, x);

            Vector3 p = Vector3.Lerp(bp1, bp2, x);
            return p;
        }
    }

    private void OnDrawGizmos()
    {
        if (p1 == null || p2 == null || p3 == null || p4 == null) return;
        
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(p1.position, 0.1f);
        Gizmos.DrawSphere(p2.position, 0.1f);
        Gizmos.DrawLine(p1.position, p2.position);

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(p3.position, 0.1f);
        Gizmos.DrawSphere(p4.position, 0.1f);
        Gizmos.DrawLine(p3.position, p4.position);

        float stepping = 1.0f / iterations;
 
        for (float x = 0.0f; x <= 1.0f; x += stepping)
        {
            Gizmos.color = Color.Lerp(Color.red, Color.blue, x);
            Gizmos.DrawSphere(GetBezierPosition(x), 0.1f);
        }
    }
}
