﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AnimationControllerScript : MonoBehaviour {

    public bool on, off;
    private Animator anim;
    private float startTime;

    public GameObject electricityPoint, fourtain;
    public ParticleSystem particlePortal, vortexPortal, smoke;

    [SerializeField] private List<TimeEvent> events;
    private List<TimeEvent> eventQueue;

    private ComponentTracker tracker;

    private void Start()
    {
        tracker = ComponentTracker.Instance;

        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (on)
        {
            on = false;
            On();
        }

        if (off)
        {
            off = false;
            Off();
        }

        if (eventQueue != null && eventQueue.Count > 0)
        {
            if (eventQueue[0].timer < Time.time - startTime)
            {
                while (eventQueue.Count > 0 && eventQueue[0].timer < Time.time - startTime)
                {
                    //eventQueue[0].action();
                    StartCoroutine(eventQueue[0].function);
                    eventQueue.RemoveAt(0);
                }
            }
        }
    }

    public void On()
    {
        print("on");
        //anim.SetTrigger("On");
        anim.SetBool("On", true);

        ComponentTracker.Instance.GetElement<Renderer>("proveta").enabled = false;
        //tracker.GetElement<Renderer>("lampada").enabled = false;
        //ComponentTracker.Instance.GetElement<Renderer>("saleiro").enabled = false;

        eventQueue = new List<TimeEvent>(events.ToArray());
        eventQueue.Sort((TimeEvent a, TimeEvent b) => a.timer.CompareTo(b.timer));
        startTime = Time.time;
    }

    public void Off()
    {
        print("off");
        eventQueue = new List<TimeEvent>();
        anim.SetBool("On", false);
    }

    #region TimeEvent Actions

    private IEnumerator ApareceLampada()
    {
        /*GameObject lamp = ComponentTracker.Instance.GetObject("lampada");
        Vector3 size = lamp.transform.localScale = Vector3.one * 65.76576f;

        (Instantiate(electricityPoint, lamp.transform.position, lamp.transform.rotation) as GameObject).transform.parent = lamp.transform;

        lamp.renderer.enabled = true;
        iTween.ScaleFrom(lamp, iTween.Hash(
                "scale", Vector3.zero,
                "time", 1f,
                "easetype", iTween.EaseType.easeOutBounce
            ));

        yield return new WaitForSeconds(1f);

        float time2 = 1f;
        lamp.renderer.enabled = true;
        iTween.ScaleTo(lamp, iTween.Hash(
                "scale", Vector3.zero,
                "time", time2,
                "easetype", iTween.EaseType.easeInBack
            ));

        yield return new WaitForSeconds(time2);
        lamp.renderer.enabled = false;
        lamp.transform.localScale = size;*/

        GameObject lamp = tracker.GetObject("lampPosition");
        (Instantiate(electricityPoint, lamp.transform.position, lamp.transform.rotation) as GameObject).transform.parent = lamp.transform;
        yield return new WaitForEndOfFrame();
    }

    private IEnumerator ApareceProveta()
    {
        yield return null;
        ComponentTracker.Instance.GetElement<Renderer>("proveta").enabled = true;
    }

    private IEnumerator DesapareceProveta()
    {
        yield return null;
        ComponentTracker.Instance.GetElement<Renderer>("proveta").enabled = false;
    }

    private IEnumerator ApareceSaleiro()
    {
        yield return null;
        ComponentTracker.Instance.GetElement<Renderer>("saleiro").enabled = true;
    }

    private IEnumerator LiquidoProveta()
    {
        ParticleSystem liquid = tracker.GetElement<ParticleSystem>("liquido_proveta");

        GameObject f = Instantiate(fourtain) as GameObject;
        f.transform.parent = tracker.GetObject("target").transform;
        f.transform.localPosition = new Vector3(0f, 0.8020619f, 0f);
        f.transform.localEulerAngles = new Vector3(90f, 0f, 0f);
        f.transform.localScale = Vector3.one;

        liquid.Play();
        yield return new WaitForSeconds(1.2f);
        liquid.Stop();
    }

    private IEnumerator AtivaParticulaPortal()
    {
        particlePortal.Play();
        vortexPortal.Play();
        yield return new WaitForSeconds(8f);
        particlePortal.Stop();
        vortexPortal.Stop();
    }

    private IEnumerator EmmitSmoke()
    {
        smoke.Play();
        yield return null;
    }

    #endregion

    [System.Serializable]
    private class TimeEvent
    {
        private string name;
        public float timer;
        public string function;

        public TimeEvent(float timer, string function)
        {
            this.timer = timer;
            this.name = this.function = function;
        }
    }

    private void OnGUI()
    {
#if UNITY_EDITOR
        //GUI.Box(new Rect(10, 10, 100, 30), "");
        //GUI.Label(new Rect(15, 15, 100, 30), Time.time.ToString());
#endif
    }
}
