﻿using UnityEngine;
using System.Collections;

public class BoltAnimation : MonoBehaviour {

    public float delay;
    private float _time; 
    public bool play;
    private bool _play;

    public ParticleSystem particle;

    private void Update()
    {
        if(play != _play)
        {
            if (play)
            {
                _time = Time.time;
            }
        }
        _play = play;
        
        if(play)
        {
            if(Time.time >= _time)
            {
                _time += Time.time - _time + delay;
                transform.eulerAngles = new Vector3(Random.Range(0f, 360f), 270f, 0f);
                particle.Play();
            }
        }
    }
}
