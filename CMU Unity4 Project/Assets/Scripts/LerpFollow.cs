﻿using UnityEngine;
using System.Collections;

public class LerpFollow : MonoBehaviour {

    public Transform target;
    public float movementSpeed, rotationSpeed;

    private void Start()
    {
        transform.parent = null;
    }

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * movementSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime * rotationSpeed);
    }
}
